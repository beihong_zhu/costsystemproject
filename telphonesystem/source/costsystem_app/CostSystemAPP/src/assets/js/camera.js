let front = false;
const changeBtn = document.querySelector('#changeBtn');
changeBtn.addEventListener('click', function(){
  front = !front;
});

const msgbox = document.querySelector('#msgbox');
msgbox.innerHTML = '';
const supports = navigator.mediaDevices.getSupportedConstraints();
for(let a in supports){
  msgbox.innerHTML = msgbox.innerHTML + a + ': ' + supports[a] + '<br>';
}

navigator.mediaDevices.getUserMedia({
  audio: false,
  video: {
    width: 360,
    height: 300,
    facingMode: front ? 'user' : 'environment',
    // facingMode: 'user',
    // facingMode: { exact : 'environment' },
  }
})
.then(async mediaStream => {
  document.querySelector('#cameraVideo').srcObject = mediaStream;

  // Once crbug.com/711524 is fixed, we won't need to wait anymore. This is
  // currently needed because capabilities can only be retrieved after the
  // device starts streaming. This happens after and asynchronously w.r.t.
  // getUserMedia() returns.
  await sleep(1000);

  const track = mediaStream.getVideoTracks()[0];
  const capabilities = track.getCapabilities();
  const settings = track.getSettings();

  const zoomRange = document.querySelector('#zoomRange');
  // Check whether zoom is supported or not.
  if (!('zoom' in capabilities)) {
    return Promise.reject('Zoom is not supported by ' + track.label);
  }
  // Map zoom to a slider element.
  zoomRange.min = capabilities.zoom.min;
  zoomRange.max = capabilities.zoom.max;
  zoomRange.step = capabilities.zoom.step;
  zoomRange.value = settings.zoom;
  zoomRange.oninput = function(event) {
    track.applyConstraints({advanced: [{zoom: event.target.value}]});
  }

  const changeBtn = document.querySelector('#changeBtn');
  // Check whether torch is supported or not.
  if (!('torch' in capabilities)) {
    return Promise.reject('Torch is not supported by ' + track.label);
  }
  // Map torch to a button element.
  changeBtn.addEventListener('click', function(){
    const sts = capabilities.torch;
    track.applyConstraints({advanced: [{torch: !sts}]});
  });

  const flashBtn = document.querySelector('#flashBtn');
  // Check whether torch is supported or not.
  if (!('torch' in capabilities)) {
    return Promise.reject('Torch is not supported by ' + track.label);
  }
  // Map torch to a button element.
  flashBtn.addEventListener('click', function(){
    const sts = capabilities.torch;
    track.applyConstraints({advanced: [{torch: !sts}]});
  });
})
.catch(error => console.log('Argh!', error.name || error));

/* Utils */
function sleep(ms = 0) {
  return new Promise(r => setTimeout(r, ms));
}
