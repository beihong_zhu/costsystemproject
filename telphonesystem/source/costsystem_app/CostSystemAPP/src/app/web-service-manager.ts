import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';

import { ServiceBaseService } from './service-base-service';
import { ApiFaultDto, WebServiceManagerDto } from './app_photo/service/dto/web-service-manager-dto';
import { AlertController } from '@ionic/angular';
import { RoutingService } from '../app/app_photo/service/app-route.service';
import { AppLoadingStoreService, } from '@store';
import { ResultType } from './app_common/constant/common';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { ApiBaseOutDto } from '@service/dto/api-base-out-dto';

export abstract class WebServiceManager<M> extends ServiceBaseService<M> {

  // 送信情報格納キュー
  private webServiceQue: WebServiceManagerDto<M>[] = [];

  // 送信情報格納キューサイズ
  private webserviceQueSize = 9999;

  // 通信中区分
  private statusFlg = false;

  constructor(
    protected http: HttpClient,
    protected alertController: AlertController,
    protected routingService: RoutingService,
    private appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http);
  }

  /**
   * 送信要求処理
   * 渡されたWebサービスマネージャ用Dtoをキューに格納し、送信実行処理をコールする。
   *
   */
  protected exec(
    inDto: M,
    resultFunction: (outDto: ApiBaseOutDto) => void,
    faultFunction: (outDto?: ApiFaultDto) => void,
    isDownload: boolean = false,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {

    const serviceDto: WebServiceManagerDto<M> = {
      inDto,
      faultFunction,
      resultFunction,
      isDownload,
      isNeedMask,
      isNeedErrorHander,
      status: 'wait'
    };

    // 上限に到達している状態で発生したリクエストについてはエラー扱いとし、アプリが指定したFaultHandlerを呼び出す
    if (this.webServiceQue.length === this.webserviceQueSize) {
      if (serviceDto.faultFunction) {
        serviceDto.faultFunction();
      }
      return;
    }

    // 送信情報格納キューにdtoを格納
    this.webServiceQue.push(serviceDto);

    // ローダの表示処理
    if (isNeedMask) {
      this.appLoadingStoreService.isloading(true);
    }

    if (!this.statusFlg) {
      // 送信実行処理コール
      this.sendRequest();
    }
  }

  /**
   * 送信実行処理
   * キューよりWebサービスマネージャ用Dtoを取得し、順次送信する。
   * 同期処理の場合はアプリケーションのマスクを行う。
   * 送信中件数が最大件数に達している場合は送信制限を掛ける。
   *
   */
  private sendRequest() {
    let sendingQue: WebServiceManagerDto<M>;

    for (const sendQue of this.webServiceQue) {
      // 待ち場合は送信
      if (sendQue.status === 'wait') {
        sendingQue = sendQue;
        break;
      }
    }
    // 送信中件数0
    if (!sendingQue) {
      return;
    }
    // 送信中
    sendingQue.status = 'run';

    // delete
    let url: string = this.routingService.getUrl();

    this.send(
      sendingQue.inDto,
      sendingQue.isDownload,
      this.routingService.getUrl()
    ).subscribe(
      (res: any) => {
        // APIシステムエラー (download request，serve から jsonのデータを返す場合ある)
        if (res.resultCode === ResultType.SYSTEMERROR || (res.headers && res.headers.get('x-resultCode') === ResultType.SYSTEMERROR)) {
          const messageCode = !sendingQue.isDownload ? res.messageCode : res.headers.get('x-messageCode');
          this.webServiceFaultHandler(sendingQue, messageCode,
            { isTimeOut: false, resultCode: ResultType.SYSTEMERROR, status: '200', messageCode });
        } else {
          // 正常情况
          this.webServiceResultHandler(sendingQue, res);
        }
      },
      (err: any) => {
        // タイムアウトエラー
        if (err.name === 'TimeoutError') {
          this.timeOutHandler(sendingQue, { isTimeOut: true, resultCode: null, status: '0' });
        } else if (err.name === 'HttpErrorResponse') {
          // HTTPエラー
          this.webServiceFaultHandler(sendingQue, err.status, { isTimeOut: false, resultCode: null, status: err.status });
        } else {
          // そのたエラー
          this.webServiceFaultHandler(sendingQue, err.status || '', { isTimeOut: false, resultCode: null, status: err.status });
        }
      }
    );
  }


  /**
   * WSDLロード完了イベントハンドラー
   * WSDLのロードが完了した場合にコールされる。
   *
   */
  private webServiceResultHandler(
    sendingQue: WebServiceManagerDto<M>,
    res: any
  ): void {

    // 正常場合
    if (sendingQue.status !== 'error') {
      sendingQue.status = 'quit';
    }

    //  送信情報格納キューから終了タスクを削除
    this.removeCompleteQue();

    // 通信中区分を設定
    this.updateStatusFlg();

    // マスクまた通信完了
    if (sendingQue.isNeedMask && !this.statusFlg) {
      this.appLoadingStoreService.isloading(false);
    }

    // 残り送信実行
    this.sendRequest();

    sendingQue.resultFunction(res);
  }

  /**
   * WSDLロードエラーイベントハンドラー
   * WSDLロード時にエラーが発生した場合にコールされる。
   *
   */
  private async webServiceFaultHandler(sendingQue: WebServiceManagerDto<M>, errorStatus: string, faultDto: ApiFaultDto) {

    // エラーステータス
    sendingQue.status = 'error';

    // 通信中区分を設定
    this.updateStatusFlg();

    // // マスクまた通信完了
    if (sendingQue.isNeedMask && !this.statusFlg) {
      this.appLoadingStoreService.isloading(false);
    }

    // FaultFunctionが設定されている場合はコールする
    this.faultKakuninCloseHandler(sendingQue, faultDto);

    let messageCode: string = '';
    const strErrorStatus = errorStatus + '';
    if (sendingQue.isNeedErrorHander) {
      // AWS　WAF　攻撃として検知
      if (strErrorStatus === '403') {
        messageCode = 'E00112';
        if (document.getElementsByClassName(messageCode).length === 0) {
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            cssClass: messageCode,
            message: MessageConstants.E00112.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          alert.present();
        }
        // HTTP DEFAULT 4xx、5xx　エラー　と　corsためUnknown error処理
        // HTTPステータスコードが0 ※APサーバの返信なし(APサーバ断など)
      } else if (strErrorStatus === '0' || strErrorStatus.startsWith('4') || strErrorStatus.startsWith('5')) {
        messageCode = 'E00150';
        if (document.getElementsByClassName(messageCode).length === 0) {
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            cssClass: messageCode,
            message: MessageConstants[messageCode].replace('{1}', AlertWindowConstants.ADMINEMAIL).replace('{2}', strErrorStatus),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          alert.present();
        }
      } else {
        // 送信情報格納キューをクリア
        this.webServiceQue.length = 0;
        // エラー画面に遷移
        this.routingService.goRouteLink('/error/' + errorStatus);
      }
    } else {
      this.sendRequest();
    }
  }

  // エラーアラートがポップアップ
  private async errAlert(errCode: string, errMsg: string) {
    if (document.getElementsByClassName(errCode).length === 0) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        cssClass: errCode,
        message: errMsg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.present();
    }
  }

  // 送信情報格納キューから終了タスクを削除
  private removeCompleteQue(): void {
    _.remove(
      this.webServiceQue,
      (que: WebServiceManagerDto<M>) =>
        que.status === 'error' || que.status === 'quit'
    );
  }

  // タイムアウト
  private async timeOutHandler(sendingQue: WebServiceManagerDto<M>, faultDto: ApiFaultDto) {
    sendingQue.status = 'error';

    let messageCode: string = '';

    // ローダの削除処理呼び出し
    this.appLoadingStoreService.isloading(false);
    // 送信情報格納キューをクリア
    this.webServiceQue.length = 0;

    if (sendingQue.isNeedErrorHander) {
      // messageCode = 'E00068';
      messageCode = 'E00149';
      if (document.getElementsByClassName(messageCode).length === 0) {
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          cssClass: messageCode,
          message: MessageConstants[messageCode].replace('{1}', AlertWindowConstants.ADMINEMAIL),
          buttons: [AlertWindowConstants.OKENGLISHBUTTON],
          backdropDismiss: false
        });
        alert.present();
      }
    }

    this.faultKakuninCloseHandler(sendingQue, faultDto);
  }

  private faultKakuninCloseHandler(sendingQue: WebServiceManagerDto<M>, faultDto: ApiFaultDto): void {

    if (sendingQue && sendingQue.faultFunction) {
      // FaultFunctionが設定されている場合はコールする
      sendingQue.faultFunction(faultDto as ApiFaultDto);
    }
  }

  // 通信中区分を更新
  private updateStatusFlg(): void {
    this.statusFlg = false;
    for (const dto of this.webServiceQue) {
      if (
        dto.status === 'wait' ||
        dto.status === 'run'
      ) {
        this.statusFlg = true;
        break;
      }
    }
  }

  // 通信中区分
  public getStatusFlg(): boolean {
    return this.statusFlg;
  }
}
