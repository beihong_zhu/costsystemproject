import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, Route } from '@angular/router';
import { AuthGuard } from '@core/guard/auth.guard';
import { environment } from '@env';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  // tab画面
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  // regular info
  {
    path: 'regularinfo',
    data: { pageId: 'S-15-01' },
    loadChildren: () => import('./app_photo/pages/regularinfo/regularinfo.module').then( m => m.RegularinfoPageModule)
  },

  // product stock
  {
    path: 'productstock',
    data: { pageId: 'S-16-01' },
    loadChildren: () => import('./app_photo/pages/productstock/productstock.module').then( m => m.ProductstockPageModule)
  },

  // ログイン画面
  {
    path: 'login',
    data: { pageId: 'S-11-01' },
    loadChildren: () =>
      import('./app_common/pages/login/login.module').then(m => m.LoginModule)
  },
  // パスワード変更画面
  {
    path: 'change-pwd',
    data: { pageId: 'S-11-02' },
    loadChildren: () =>
      import('./app_common/pages/change-pwd/change-pwd.module').then(m => m.ChangePwdModule)
  },
  // 二段階認証デバイス管理画面
  {
    path: 'device-manage',
    data: { pageId: 'S-11-03' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_common/pages/device-manage/device-manage.module').then(m => m.DeviceManageModule)
  },
  // アカウントロック管理画面
  {
    path: 'lock-account',
    data: { pageId: 'S-11-04' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_common/pages/lock-account/lock-account.module').then(m => m.LockAccountModule)
  },
  // 物件表示画面
  {
    path: 'buildinglist',
    data: { pageId: 'S-01-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/building-project/building-list/building-list.module').then(m => m.BuildingListModule)
  },
  // 案件表示画面
  {
    path: 'project',
    data: { pageId: 'S-01-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/building-project/project/project.module').then(m => m.ProjectModule)
  },
  // 大工程選択画面
  {
    path: 'bigProcess',
    data: { pageId: 'S-02-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/big-process/big-process.module').then(m => m.BigProcessModule)
  },
  // 写真撮影画面
  {
    path: 'camera',
    data: { pageId: 'S-03-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/camera/camera.module').then(m => m.CameraModule)
  },
  // 機番撮影画面
  {
    path: 'barcode-scan',
    data: { pageId: 'S-04-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/camera/barcode-scan/barcode-scan.module').then(m => m.BarcodeScanModule)
  },
  // 写真一覧画面
  {
    path: 'photo-list',
    data: { pageId: 'S-05-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/photo-manage/photo-list/photo-list.module').then(m => m.PhotoListModule)
  },
  // 写真詳細画面
  {
    path: 'photo-detail',
    data: { pageId: 'S-05-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/photo-manage/photo-detail/photo-detail.module').then(m => m.PhotoDetailModule)
  },
  // 図面表示画面
  {
    path: 'draw-display',
    data: { pageId: 'S-06-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/draw-manage/draw-display/draw-display.module').then(m => m.DrawDisplayModule)
  },
  // 図面位置設定画面
  {
    path: 'draw-location',
    data: { pageId: 'S-06-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/draw-manage/draw-location/draw-location.module').then(m => m.DrawLocationModule)
  },
  // 工程登録画面
  {
    path: 'process-create',
    data: { pageId: 'S-08-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/process/process-create/process-create.module').then(m => m.ProcessCreateModule)
  },
  // 工程編集画面
  {
    path: 'process-edit',
    data: { pageId: 'S-08-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/process/process-edit/process-edit.module').then(m => m.ProcessEditModule)
  },
  // 写真帳出力情報設定画面
  {
    path: 'photobook-info-set',
    data: { pageId: 'S-09-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/photobook/photobook-info-set/photobook-info-set.module').then(m => m.PhotobookInfoSetModule)
  },
  // 写真帳レイアウト設定画面
  {
    path: 'photobook-view-set',
    data: { pageId: 'S-09-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/photobook/photobook-view-set/photobook-view-set.module').then(m => m.PhotobookViewSetModule)
  },
  // 写真帳プレビュー画面
  {
    path: 'photobook-preview',
    data: { pageId: 'S-09-03' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/photobook/photobook-preview/photobook-preview.module').then(m => m.PhotobookPreviewModule)
  },
  // チャットルーム一覧画面
  {
    path: 'chatroomlist',
    data: { pageId: 'S-12-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/chatroom/chatroom-list/chatroom-list.module').then(m => m.ChatroomListPageModule)
  },
  // チャットルーム作成画面
  {
    path: 'chatroom-create',
    data: { pageId: 'S-12-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/chatroom/chatroom-create/chatroom-create.module').then(m => m.ChatroomCreatePageModule)
  },
  // チャットルーム作成画面-メンバー選択
  {
    path: 'chatroom-create-user-select',
    data: { pageId: 'S-12-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/chatroom/chatroom-create-user-select/chatroom-create-user-select.module')
        .then(m => m.ChatroomCreateUserSelectPageModule)
  },
  // チャットルーム作成画面-作成完了
  {
    path: 'chatroom-create-complete',
    data: { pageId: 'S-12-02' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/chatroom/chatroom-create-complete/chatroom-create-complete.module')
      .then(m => m.ChatroomCreateCompletePageModule)
  },
  // チャットルーム編集画面
  {
    path: 'chatroom-edit',
    data: { pageId: 'S-12-03' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/chatroom/chatroom-edit/chatroom-edit.module').then(m => m.ChatroomEditPageModule)
  },
  // チャットルーム編集画面-メンバー選択
  {
    path: 'chatroom-edit-user-select',
    data: { pageId: 'S-12-03' },
    canActivate: [AuthGuard],
    loadChildren: () => import('./app_photo/pages/chatroom/chatroom-edit-user-select/chatroom-edit-user-select.module')
      .then(m => m.ChatroomEditUserSelectPageModule)
  },
  // チャットルーム編集画面-作成完了
  {
    path: 'chatroom-edit-complete',
    data: { pageId: 'S-12-03' },
    canActivate: [AuthGuard],
    loadChildren: () => import('./app_photo/pages/chatroom/chatroom-edit-complete/chatroom-edit-complete.module')
      .then(m => m.ChatroomEditCompletePageModule)
  },
  // チャット画面
  {
    path: 'chattalk',
    data: { pageId: 'S-12-04' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/chatroom/chattalk/chattalk.module').then(m => m.ChattalkPageModule)
  },
  // システムエラー画面
  {
    path: 'error/:errorStatus',
    data: { pageId: 'S-13-02' },
    loadChildren: () =>
      import('./app_common/pages/error/error.module').then(m => m.ErrorModule)
  },
  // プライバシーポリシー画面
  {
    path: 'privacy-policy',
    data: { pageId: 'S-13-03' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_common/pages/privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)
  },
  // 事前ログイン画面
  {
    path: 'presetting-login',
    data: { pageId: 'S-14-00' },
    loadChildren: () =>
      import('./app_common/pages/presetting-login/presetting-login.module').then(m => m.PresettingLoginModule)
  },
  // 事前設定情報登録画面
  {
    path: 'presetting-upload',
    data: { pageId: 'S-14-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/presetting-upload/upload-form/upload-form.module').then(m => m.UploadFormModule)
  },
  // 事前設定情報登録確認画面
  {
    path: 'presetting-confirm',
    data: { pageId: 'S-14-01' },
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./app_photo/pages/presetting-upload/upload-confirm/upload-confirm.module').then(m => m.UploadConfirmModule)
  },
  // agent summary
  {
    path: 'agent-summary',
    loadChildren: () => import('./app_photo/pages/agent-summary/agent-summary.module').then( m => m.AgentSummaryPageModule)
  },
  // authory summary
  {
    path: 'authory-summary',
    loadChildren: () => import('./app_photo/pages/authory-summary/authory-summary.module').then( m => m.AuthorySummaryPageModule)
  },
  {
    path: 'createstock',
    loadChildren: () => import('./app_photo/pages/createstock/createstock.module').then( m => m.CreatestockPageModule)
  },
  {
    path: 'onshelf',
    loadChildren: () => import('./app_photo/pages/onshelf/onshelf.module').then( m => m.OnshelfPageModule)
  },

  {
    path: 'agency',
    loadChildren: () => import('./app_photo/pages/agency/agency.module').then( m => m.AgencyPageModule)
  },
  {
    path: 'newagency',
    loadChildren: () => import('./app_photo/pages/newagency/newagency.module').then( m => m.NewagencyPageModule)
  },
  {
    path: 'fundrecorder',
    loadChildren: () => import('./app_photo/pages/fundrecorder/fundrecorder.module').then( m => m.FundrecorderPageModule)
  },
  {
    path: 'cush',
    loadChildren: () => import('./app_photo/pages/cush/cush.module').then( m => m.CushPageModule)
  },
  {
    path: 'addnewproduct',
    loadChildren: () => import('./app_photo/pages/addnewproduct/addnewproduct.module').then( m => m.AddnewproductPageModule)
  },
  {
    path: 'zeroaccount',
    loadChildren: () => import('./app_photo/pages/zeroaccount/zeroaccount.module').then( m => m.ZeroaccountPageModule)
  },
  {
    path: 'homepage',
    loadChildren: () => import('./app_photo/pages/homepage/homepage.module').then( m => m.HomepagePageModule)
  },
  {
    path: 'suppliedmanagement',
    loadChildren: () => import('./app_photo/pages/suppliedmanagement/suppliedmanagement.module').then( m => m.SuppliedmanagementPageModule)
  },
  {
    path: 'ordersmanagement',
    loadChildren: () => import('./app_photo/pages/ordersmanagement/ordersmanagement.module').then( m => m.OrdersmanagementPageModule)
  },
  {
    path: 'orderhistory',
    loadChildren: () => import('./app_photo/pages/orderhistory/orderhistory.module').then( m => m.OrderhistoryPageModule)
  },
  {
    path: 'mutex',
    loadChildren: () => import('./app_photo/pages/mutex/mutex.module').then( m => m.MutexPageModule)
  },
  {
    path: 'blacklist',
    loadChildren: () => import('./app_photo/pages/blacklist/blacklist.module').then( m => m.BlacklistPageModule)
  },  {
    path: 'newproductonshelf',
    loadChildren: () => import('./app_photo/pages/newproductonshelf/newproductonshelf.module').then( m => m.NewproductonshelfPageModule)
  }




];

const routesDebug: Routes = [
  ...routes,
  {
    path: 'logger',
    data: { },
    loadChildren: () =>
      import('./app_common/pages/tablet-debug-logger/logger.module').then(m => m.LoggerModule)
  }
];

const appRoutes = environment.debug ? routesDebug  : routes;

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {
    scrollPositionRestoration: 'enabled',
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
