import { Injectable } from '@angular/core';
import { Auth } from 'aws-amplify';
import { CognitoUser } from 'amazon-cognito-identity-js';
import { AppLoadingStoreService, CommonStore, CommonStoreService, DeviceTokenStore, AppStateStoreService } from '@store';
import { AuthConfig, ResultType } from '../constant/common';
import { environment } from '@env';
import { Platform } from '@ionic/angular';
import { Store, select } from '@ngrx/store';
import { ChatAPI } from '@graphql/chatApi.service';
import { API1113Service, API1114Service } from '@service/api';
import { API1113InDto, API1113OutDto, API1114InDto, API1114OutDto } from '@service/dto';
import * as moment from 'moment';

export enum AuthResult {
  IncorrectUsername = 'incorrectUsername',
  IncorrectUsernameOrPassword = 'incorrectUsernameOrPassword',
  IncorrectAnswer = 'incorrectAnswer',
  AccountLockedByStore = 'accountLockedByStore',
  LockAccountedToDB = 'lockAccountedToDB',
  InvalidSession = 'invalidSession',
  LoginSystemError = 'loginSystemError'
}

@Injectable()
export class AuthService {

  private commonStore: CommonStore;
  private deviceToken: string;

  constructor(
    private platform: Platform,
    private appLoadingStoreService: AppLoadingStoreService,
    private appStateStoreService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private storeSubscribe: Store<{ deviceTokenStore: DeviceTokenStore }>,
    private api1113Service: API1113Service,
    private api1114Service: API1114Service
  ) {
    this.commonStore = this.commonStoreService.commonStore;
    this.storeSubscribe.pipe(select('deviceTokenStore')).subscribe(store => {
      this.deviceToken = store.deviceToken;
    });
  }

  async signIn(username: string, password: string): Promise<CognitoUser> {
    return new Promise(async (resolve, reject) => {
      try {
        const outDto: API1114OutDto = await this.api1114Exec(username);
        const pwErrCnt = outDto.firstLoginFailCount ? outDto.firstLoginFailCount : 0;
        const pwLockedTime = outDto.firstLoginFailTime ? moment(outDto.firstLoginFailTime, 'YYYYMMDDHHmmss').toDate().getTime() : '';
        if (pwLockedTime && new Date().getTime() < (Number(pwLockedTime) + AuthConfig.PW_ERR_LOCK_TIME)) {
          // 一時的にログイン不可
          reject(AuthResult.AccountLockedByStore);
        } else {
          try {
            // ログイン
            const user: CognitoUser =
            await Auth.signIn({ username, password, validationData: { deviceKey: this.getDeviceKey({ username }) } });
            // console.log('[AuthService] Auth.signIn', user);
            // 認証成功、失敗回数をクリア
            if (pwErrCnt !== 0 || pwLockedTime !== '') {
              await this.api1113Exec(username, 0, '');
            }
            resolve(user);
          } catch (err) {
            if (err.message === 'Incorrect username or password.' || err.message === 'Password attempts exceeded') {
              // console.log('[AuthService] Auth.signIn catch', err);
              if (pwErrCnt + 1 < AuthConfig.PW_ERR_CNT_MAX) {
                // 認証失敗回数をカウント
                await this.api1113Exec(username, pwErrCnt + 1, '');
              } else {
                // 規定回数認証失敗となった場合、一時的にログイン不可
                await this.api1113Exec(username, 0, moment(new Date()).format('YYYYMMDDHHmmss'));
                reject(AuthResult.AccountLockedByStore);
                return;
              }
              reject(AuthResult.IncorrectUsernameOrPassword);
            } else {
              reject(AuthResult.LoginSystemError);
            }
          }
        }
      } catch (err) {
        // ユーザーIDが存在しない
        reject(AuthResult.IncorrectUsername);
      }
    });
  }

  async confirmSignIn(currentUser: CognitoUser, secretCode: string): Promise<CognitoUser> {
    return new Promise(async (resolve, reject) => {
      try {
        const user: CognitoUser = await Auth.sendCustomChallengeAnswer(currentUser, secretCode);
        // console.log('[AuthService] Auth.sendCustomChallengeAnswer', user);
        if (user.getSignInUserSession()) {
          // 二段階認証成功
          resolve(user);
        } else {
          // 二段階認証失敗
          reject(AuthResult.IncorrectAnswer);
        }
      } catch (err) {
        // console.log('[AuthService] Auth.sendCustomChallengeAnswer catch', err);
        if (err.message === 'Invalid session for the user.') {
          // パスコードの有効期限が切れています
          reject(AuthResult.InvalidSession);
        } else if (err.message === 'Incorrect username or password.' || err.message === 'Password attempts exceeded') {
          // 規定回数二段階認証失敗となった場合、アカウントロック
          reject(AuthResult.LockAccountedToDB);
        } else {
          reject(AuthResult.LoginSystemError);
        }
      }
    });
  }

  async signOut() {
    try {
      this.appLoadingStoreService.isloading(true);
      if (this.deviceToken && (this.platform.is('android') || this.platform.is('ios') || this.appStateStoreService.appState.isWinApp)) {
        const platform = this.appStateStoreService.appState.isWinApp ? 'windows' : this.platform.is('android') ? 'android' : 'ios';
        await ChatAPI.apiService.deleteUserDevice(this.deviceToken, platform, this.commonStore.userId);
      }
    } catch (err) {
      // logout失敗場合、処理中断できない
    } finally {
      await Auth.signOut();
      this.appLoadingStoreService.isloading(false);
    }
  }

  getDeviceKey(currentUser: any, noRegion: boolean = false) {
    let deviceKey = currentUser.deviceKey ? currentUser.deviceKey :
      localStorage.getItem(
        `CognitoIdentityServiceProvider.${environment.aws_user_pools_web_client_id}.${currentUser.username}.deviceKey`
      );
    if (noRegion && deviceKey) {
      deviceKey = deviceKey.replace(`${environment.aws_cognito_region}_`, '');
    }
    return deviceKey ? deviceKey : '';
  }

  private async api1113Exec(username: string, firstLoginFailCount: number, firstLoginFailTime: string): Promise<API1113OutDto> {
    // 一段階ログイン情報更新
    const inDto: API1113InDto = {
      userId: username,
      firstLoginFailCount,
      firstLoginFailTime
    };
    return new Promise((resolve, reject) => {
      this.api1113Service.postExec(inDto,
        (outDto: API1113OutDto) => resolve(outDto),
        () => { this.appLoadingStoreService.isloading(false); });
    });
  }

  private async api1114Exec(username: string): Promise<API1114OutDto> {
    // 一段階ログイン情報取得
    const inDto: API1114InDto = {
      userId: username
    };
    return new Promise((resolve, reject) => {
      this.api1114Service.postExec(inDto,
        (outDto: API1114OutDto) => {
          if (outDto.resultCode === ResultType.NORMAL) {
            resolve(outDto);
          } else {
            reject();
          }
        },
        () => { this.appLoadingStoreService.isloading(false); });
    });
  }
}
