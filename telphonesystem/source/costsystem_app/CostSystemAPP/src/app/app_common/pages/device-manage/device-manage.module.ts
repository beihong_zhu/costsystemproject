import { NgModule } from '@angular/core';
import { DeviceManagePage } from './device-manage.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DeviceSearchPage } from './device-search/device-search.page';

@NgModule({
  providers: [],
  declarations: [DeviceManagePage, DeviceSearchPage],
  entryComponents: [
    DeviceSearchPage
  ],
  imports: [
    SharedModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: DeviceManagePage
      }
    ])
  ]
})
export class DeviceManageModule { }
