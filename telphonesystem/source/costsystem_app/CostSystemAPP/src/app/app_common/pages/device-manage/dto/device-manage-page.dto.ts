import { DeviceDto } from './device.dto';

export class DeviceManagePageDto {
  deviceList?: Array<DeviceDto>;
  deviceListBK?: Array<DeviceDto>;
  selectedBigprocess?: string;
  outnumber?: boolean;
}
