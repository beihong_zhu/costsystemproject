import { NgModule } from '@angular/core';
import { PrivacyPolicyPage } from './privacy-policy.page';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [PrivacyPolicyPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PrivacyPolicyPage
      }
    ])
  ]
})
export class PrivacyPolicyModule { }
