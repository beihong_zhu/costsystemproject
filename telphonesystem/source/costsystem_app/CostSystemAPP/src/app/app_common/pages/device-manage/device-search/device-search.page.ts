import { Component, OnInit } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { API1111Service } from '@service/api';
import { API1111InDto, API1111OutDto } from '@service/dto';
import { DeviceDto } from '../dto';
import * as moment from 'moment';
import { SORTBY, CD0023Constants } from '@constant/code';
import { S1103Constants, AlertWindowConstants } from '@constant/constant.photo';
import { ResultType } from '../../../../app_common/constant/common';
import { TextCheck } from '../../../../app_photo/util/text-check';
import { MessageConstants } from '@constant/message-constants';

@Component({
  selector: 'app-device-search',
  templateUrl: './device-search.page.html',
  styleUrls: ['./device-search.page.scss'],
})
export class DeviceSearchPage {

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    title: S1103Constants.SEARCHTITLE,
    search: S1103Constants.SEARCH,
    userId: S1103Constants.USERID,
    dialogLabel: S1103Constants.DIALOGLABEL
  };

  deviceManageList: DeviceDto[];
  userId: string;
  inDto: API1111InDto;

  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    public api1111Service: API1111Service,
  ) {
  }

  isDisabled() {
    return !this.userId || !this.userId.trim();
  }

  /**
   * 「×」ボタン押下
   */
  onNoClick(): void {
    this.navParams.data.modal.dismiss(null);
  }

  /**
   * 「検索」ボタン押下
   */
  async searchClick() {

    if (!TextCheck.regCheckHalf(this.userId.trim())) {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00009.replace('{{value}}', this.gamenLable.userId),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }


    this.api1111Service.postExec(this.getApi1111InDto(), (outDto: API1111OutDto) => {
      this.apiSuccessed(outDto);
    },
      // fault
      () => {
        this.navParams.data.modal.dismiss(null);
      });
  }

  /**
   * api1111InDtoを作成
   * @returns API1111InDto
   */
  getApi1111InDto(): API1111InDto {
    const inDto: API1111InDto = {};
    // ユーザID
    inDto.userId = this.userId.trim();
    // ソートリスト
    inDto.sort = [{ sortItem: 'device_created_datetime', order: SORTBY.ESC }];
    this.inDto = inDto;
    return inDto;
  }

  /**
   * api呼出、戻り値を設定
   * @param outDto API1111OutDto
   */
  async apiSuccessed(outDto: API1111OutDto) {
    this.deviceManageList = new Array<DeviceDto>();
    if (outDto && outDto.deviceAccessInfoList) {
      Array.from({ length: outDto.deviceAccessInfoList.length }).map((_, i) => {
        this.deviceManageList[i] = {};
        // デバイス有効/無効
        this.deviceManageList[i].isAccessApproval = String(outDto.deviceAccessInfoList[i].isAccessApproval);
        // ユーザID
        this.deviceManageList[i].userId = outDto.deviceAccessInfoList[i].userId;
        // 端末コード
        this.deviceManageList[i].deviceCode = outDto.deviceAccessInfoList[i].deviceCode;
        // デバイス登録日時
        if (outDto.deviceAccessInfoList[i].deviceCreatedDatetime) {
          this.deviceManageList[i].dateMoment = moment(outDto.deviceAccessInfoList[i].deviceCreatedDatetime, 'YYYYMMDDHHmmss')
            .format('YYYY年MM月DD日  HH時mm分');
        } else {
          this.deviceManageList[i].dateMoment = '';
        }
      });
    }
    this.navParams.data.modal.dismiss([this.deviceManageList, this.inDto, outDto.resultCode === ResultType.INFO]);
  }

}
