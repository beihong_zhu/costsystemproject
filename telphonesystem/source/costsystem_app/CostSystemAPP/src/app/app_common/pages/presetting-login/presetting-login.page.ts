import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AmplifyService } from 'aws-amplify-angular';
import { CognitoUser } from 'amazon-cognito-identity-js';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService, AppStateStoreService, CommonStoreService, CommonStore } from '@store';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { S1101Constants } from '@constant/constant.photo';
import { API0701Service, API1107Service, API1109Service, API1110Service } from '@service/api';
import {
  API0701InDto, API0701OutDto, API1107InDto, API1107OutDto, API1109InDto, API1109OutDto, API1110InDto, API1110OutDto
} from '@service/dto';
import { AuthService, AuthResult } from '../../service/auth.service';
import { TextCheck } from '../../../app_photo/util/text-check';

@Component({
  selector: 'app-presetting-login',
  templateUrl: './presetting-login.page.html',
  styleUrls: ['./presetting-login.page.scss']
})
export class PresettingLoginPage {

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    loginMessage: S1101Constants.LOGINMESSAGE,
    passwordChangeMessage: S1101Constants.PASSWORDCHANGEMESSAGE,
    passwordChange: S1101Constants.PASSWORDCHANGE,
    useVersionSelectMessage: S1101Constants.USEVERSIONSELECTMESSAGE,
    pcEdition: S1101Constants.PCEDITION,
    smartphoneEdition: S1101Constants.SMARTPHONEEDITION,
    signIn: S1101Constants.SIGNIN,
    passcodeInput: S1101Constants.PASSCODEINPUT,
    confirmSignIn: S1101Constants.CONFIRMSIGNIN
  };

  isConfirmLogin = false;
  userName = null;
  userPwd = null;
  secretCode = null;

  private currentUser: CognitoUser;
  private commonStore: CommonStore;

  constructor(
    private alertController: AlertController,
    private amplifyService: AmplifyService,
    private routeService: RoutingService,
    private authService: AuthService,
    private appLoadingStoreService: AppLoadingStoreService,
    private appStateStoreService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private api0701Service: API0701Service,
    private api1107Service: API1107Service,
    private api1109Service: API1109Service,
    private api1110Service: API1110Service
  ) {
    this.commonStore = this.commonStoreService.commonStore;
    // this.amplifyService.authStateChange$.subscribe(async authState => {
    //   console.log('[PresettingLoginPage] authState:', authState.state);
    //   console.log('[PresettingLoginPage] user:', authState.user);
    // });
  }

  ionViewWillEnter() {
    this.isConfirmLogin = false;
    this.userName = '';
    this.userPwd = '';
    this.secretCode = null;
    localStorage.setItem('com.dkj.photo.presetting', '1');
  }

  async signIn() {
    if (!TextCheck.textCheck(this.userName.trim())) {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    if (!TextCheck.regCheckHalf(this.userName.trim())) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00009.replace('{{value}}', 'ユーザID'),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    if (!TextCheck.regCheckHalf(this.userPwd.trim())) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00009.replace('{{value}}', 'パスワード'),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    if (!this.userName || !this.userPwd) {
      // ユーザIDまたはパスワードが未入力の場合
      await this.showAlert(MessageConstants.E00012);
    } else {
      if (this.appStateStoreService.appState.isOffline) {
        // オフラインの場合
        await this.showAlert(MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL));
      } else {
        this.appLoadingStoreService.isloading(true);

        try {
          await this.signInSuccess(true);

          // const user: CognitoUser = await this.authService.signIn(this.userName, this.userPwd);
          // // console.log('[PresettingLoginPage] authService.signIn', user);
          // // 認証成功
          // this.currentUser = user;
          // if (!this.currentUser.getSignInUserSession()) {
          //   // 二段階認証
          //   this.isConfirmLogin = true;
          // } else {
          //   // ログイン済み
          //   await this.signInSuccess(false);
          // }
        } catch (err) {
          // console.log('[PresettingLoginPage] authService.signIn catch', err);
          if (err === AuthResult.IncorrectUsername) {
            // ユーザーIDが存在しない
            await this.showAlert(MessageConstants.E00110);
          } else if (err === AuthResult.IncorrectUsernameOrPassword) {
            // 認証失敗
            await this.showAlert(MessageConstants.E00002);
          } else if (err === AuthResult.AccountLockedByStore) {
            // 一時的にログイン不可
            await this.showAlert(MessageConstants.E00104);
          } else {
            // ログインに失敗しました。通信環境の良いところで再度ログインを行っても同じ異常が発生する場合は、システム管理者にお問い合わせください。
            await this.showAlert(MessageConstants.E00114.replace('{1}', AlertWindowConstants.ADMINEMAIL));
          }
        }
        this.appLoadingStoreService.isloading(false);
      }
    }
  }

  async confirmSignIn() {
    if (!this.secretCode) {
      // 認証コードが未入力の場合
      await this.showAlert(MessageConstants.E00001);
    } else if (this.appStateStoreService.appState.isOffline) {
      // オフラインの場合
      await this.showAlert(MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL));
    } else {
      this.appLoadingStoreService.isloading(true);
      try {
        this.currentUser = await this.authService.confirmSignIn(this.currentUser, this.secretCode);
        // console.log('[PresettingLoginPage] authService.confirmSignIn', user);
        // 二段階認証成功
        this.amplifyService.setAuthState({ state: 'signedIn', user: this.currentUser });
        await this.signInSuccess(true);
      } catch (err) {
        // console.log('[PresettingLoginPage] authService.confirmSignIn catch', err);
        if (err === AuthResult.IncorrectAnswer) {
          // 二段階認証失敗
          await this.showAlert(MessageConstants.E00026);
        } else if (err === AuthResult.InvalidSession) {
          // パスコードの有効期限が切れています
          await this.showAlert(MessageConstants.E00070);
          this.isConfirmLogin = false;
        } else if (err === AuthResult.LockAccountedToDB) {
          // 規定回数二段階認証失敗となった場合、アカウントロック
          await this.showAlertWarning(MessageConstants.W00003.replace('{{value}}', S1101Constants.ADMINEMAIL));
          this.isConfirmLogin = false;
        } else {
          // ログインに失敗しました。通信環境の良いところで再度ログインを行っても同じ異常が発生する場合は、システム管理者にお問い合わせください。
          await this.showAlert(MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL));
        }
      }
      this.secretCode = null;
      this.appLoadingStoreService.isloading(false);
    }
  }

  private async signInSuccess(confirmFlg: boolean) {
    try {
      // アカウントロック判定
      await this.api1107Exec();
      // アカウントがロックされない場合
      if (confirmFlg) {
        // デバイスアクセス情報登録
        await this.api1109Exec();
        // 権限利用機能情報取得
        await this.getAuthority();
      } else {
        try {
          // デバイスアクセス情報取得
          const kbn = await this.api1110Exec();
          if (kbn === 'NeedRegistDevice') {
            // デバイスアクセス情報登録
            await this.api1109Exec();
          }
          // 権限利用機能情報取得
          await this.getAuthority();
        } catch {
          // アクセス許可が無効
          await this.showAlert(MessageConstants.E00046.replace('{value}', AlertWindowConstants.ADMINEMAIL));
          this.isConfirmLogin = false;
        }
      }
    } catch {
      // アカウントがロックされた場合
      if (confirmFlg) {
        this.currentUser.forgetDevice({
          onSuccess: async (success: string) => {
            await this.authService.signOut();
            await this.showAlertWarning(MessageConstants.W00003.replace('{{value}}', S1101Constants.ADMINEMAIL));
            this.isConfirmLogin = false;
          },
          onFailure: async (err: Error) => {
            await this.authService.signOut();
            await this.showAlertWarning(MessageConstants.E00114.replace('{1}', AlertWindowConstants.ADMINEMAIL));
            this.isConfirmLogin = false;
          }
        });
      } else {
        await this.authService.signOut();
        await this.showAlertWarning(MessageConstants.W00003.replace('{{value}}', S1101Constants.ADMINEMAIL));
        this.isConfirmLogin = false;
      }
    }
  }

  private async api1107Exec(): Promise<void> {
    // アカウントロック対象者情報取得
    const username = this.currentUser.getUsername();
    const inDto: API1107InDto = {
      userId: username,
      isAccountlocked: '1'
    };
    return new Promise((resolve, reject) => {
      this.api1107Service.postExec(inDto, (outDto: API1107OutDto) => {
        if (outDto.accountLockList && outDto.accountLockList.filter(item => item.userId === username).length === 1) {
          reject();
        } else {
          resolve();
        }
      }, () => { this.appLoadingStoreService.isloading(false); }, false);
    });
  }

  private async api1109Exec(): Promise<void> {
    // デバイスアクセス情報登録
    const inDto: API1109InDto = {
      userId: this.currentUser.getUsername(),
      deviceCode: this.authService.getDeviceKey(this.currentUser, true)
    };
    return new Promise((resolve, reject) => {
      this.api1109Service.postExec(inDto,
        (outDto: API1109OutDto) => resolve(),
        () => { this.appLoadingStoreService.isloading(false); }
      );
    });
  }

  private async api1110Exec(): Promise<string> {
    // デバイスアクセス情報取得
    const username = this.currentUser.getUsername();
    const inDto: API1110InDto = {
      userId: username,
      deviceCode: this.authService.getDeviceKey(this.currentUser, true)
    };
    return new Promise((resolve, reject) => {
      this.api1110Service.postExec(inDto, (outDto: API1110OutDto) => {
        if (outDto.isAccessApproval && outDto.isAccessApproval === 1) {
          resolve('');
        } else if (outDto.resultCnt === 0) {
          resolve('NeedRegistDevice');
        } else {
          reject();
        }
      }, () => { this.appLoadingStoreService.isloading(false); });
    });
  }

  private async api0701Exec(): Promise<API0701OutDto> {
    // 権限利用機能情報取得
    const inDto: API0701InDto = {
      authorityId: this.commonStore.authorId
    };
    return new Promise((resolve, reject) => {
      this.api0701Service.postExec(inDto,
        (outDto: API0701OutDto) => resolve(outDto),
        () => { this.appLoadingStoreService.isloading(false); });
    });
  }

  private async getAuthority() {
    const sessionInfo = this.currentUser.getSignInUserSession().getIdToken().payload;
    this.commonStore.userId = sessionInfo['cognito:username'];
    this.commonStore.userName = sessionInfo.nickname;
    this.commonStore.companyId = sessionInfo['custom:companyId'];
    this.commonStore.authorId = sessionInfo['custom:authorId'];
    this.commonStore.availableFunctionIds = [];
    this.commonStoreService.setCommon(this.commonStore);
    const outDto: API0701OutDto = await this.api0701Exec();
    if (outDto.availableFunctions && outDto.availableFunctions.length > 0) {
      outDto.availableFunctions.forEach(af => {
        this.commonStore.availableFunctionIds.push(af.useFunctionId);
      });
    }
    this.commonStoreService.setCommon(this.commonStore);
    this.appStateStoreService.setIsPC(true);
    this.routeService.goRouteLink('/presetting-upload');
  }

  private async showAlert(msg: string) {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.ERRORTITLE,
      message: msg,
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    await alert.present();
  }

  private async showAlertWarning(msg: string) {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.WARNINGTITLE,
      message: msg,
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    await alert.present();
  }

}
