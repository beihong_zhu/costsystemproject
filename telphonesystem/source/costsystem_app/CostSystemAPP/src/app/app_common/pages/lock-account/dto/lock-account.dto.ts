import { AccountDto } from './account.dto';

export class LockAccountPageDto {
  accountList?: Array<AccountDto>;
  outnumber?: boolean;
}
