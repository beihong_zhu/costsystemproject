import { NgModule } from '@angular/core';
import { ChangePwdPage } from './change-pwd.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ChangePwdPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChangePwdPage
      }
    ])
  ],
  providers: []
})
export class ChangePwdModule { }
