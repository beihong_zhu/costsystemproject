import { NgModule } from '@angular/core';
import { ErrorPage } from './error.page';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [ErrorPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ErrorPage
      }
    ])
  ]
})
export class ErrorModule { }
