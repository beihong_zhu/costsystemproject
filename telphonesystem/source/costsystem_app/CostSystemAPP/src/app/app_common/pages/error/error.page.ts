import { Component, OnInit, OnDestroy } from '@angular/core';
import { MessageConstants } from '@constant/message-constants';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../service/auth.service';
import { LoggerStoreService } from '@store';
import { AppStateStoreService } from '@store';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-error',
  templateUrl: './error.page.html',
  styleUrls: ['./error.page.scss', './error.page.mobile.scss']
})
export class ErrorPage implements OnInit, OnDestroy {

  message1: string = MessageConstants.E00024;
  errorStatus: string = MessageConstants.E00025;
  loginLink: string;
  systemErrorInfo = '';
  platformtype: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private loggerStoreService: LoggerStoreService,
    private appStateService: AppStateStoreService,
    private modalController: ModalController
  ) {
    this.platformtype = 'error-' + this.appStateService.getPlatform();
  }

  async ngOnInit() {
    this.route.params.subscribe(params => {
      if (params && params.errorStatus) {
        this.errorStatus = this.errorStatus.replace('{{value}}', params.errorStatus);
      } else {
        this.errorStatus = this.errorStatus.replace('{{value}}', '');
      }
      if (params.errorStatus === 'SystemError') {
        this.systemErrorInfo = this.loggerStoreService.systemErrorInfo;
      } else {
        this.systemErrorInfo = '';
      }
    });
    await this.authService.signOut();
  }

  ngOnDestroy(): void {
  }

  async ionViewWillEnter() {
    const isPresetting = localStorage.getItem('com.dkj.photo.presetting');
    if (isPresetting && isPresetting === '1') {
      this.loginLink = '/presetting-login';
    } else {
      this.loginLink = '/login';
    }
    const modal: HTMLIonModalElement = await this.modalController.getTop();
    if (modal) {
      modal.dismiss();
    }
  }

}
