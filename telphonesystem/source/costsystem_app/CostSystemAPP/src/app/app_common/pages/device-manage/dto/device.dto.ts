export class DeviceDto {
  // アクセス許可フラグ
  isAccessApproval?: string;
  // ユーザID
  userId?: string;
  // 端末コード
  deviceCode?: string;
  // デバイス登録日時
  dateMoment?: string;
}
