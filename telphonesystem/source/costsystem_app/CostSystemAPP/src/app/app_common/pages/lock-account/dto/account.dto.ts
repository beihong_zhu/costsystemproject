export class AccountDto {
  // ロックフラグ
  // tslint:disable-next-line: no-inferrable-types
  accountLocked?: boolean = false;
  // ユーザID
  userId?: string;
  // ロック日時
  lockDatetime?: string;
}
