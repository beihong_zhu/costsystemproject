import { NgModule } from '@angular/core';
import { LoginPage } from './login.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { UploadPhotoService } from '@service/upload/upload-photo.service';
import { UploadMachineService } from '@service/upload/upload-machine.service';

@NgModule({
  declarations: [LoginPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginPage
      }
    ])
  ],
  providers: [
    UploadPhotoService,
    UploadMachineService
  ]
})
export class LoginModule { }
