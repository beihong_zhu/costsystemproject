import { Component, OnInit } from '@angular/core';
import { NavParams, AlertController } from '@ionic/angular';
import { API1107Service } from '@service/api';
import { API1107InDto, API1107OutDto } from '@service/dto';
import { AccountDto } from '../dto';
import { SORTBY, CD0018Constants } from '@constant/code';
import * as moment from 'moment';
import { S1104Constants, AlertWindowConstants } from '@constant/constant.photo';
import { ResultType } from '../../../../app_common/constant/common';
import { TextCheck } from '../../../../app_photo/util/text-check';
import { MessageConstants } from '@constant/message-constants';

@Component({
  selector: 'app-account-search',
  templateUrl: './account-search.page.html',
  styleUrls: ['./account-search.page.scss'],
})
export class AccountSearchPage {
  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    title: S1104Constants.SEARCHTITLE,
    search: S1104Constants.SEARCH,
    userId: S1104Constants.USERID,
    dialogLabel: S1104Constants.DIALOGLABEL
  };

  userId: string;
  inDto: API1107InDto;

  constructor(public navParams: NavParams,
              public api1107Service: API1107Service,
              public alertController: AlertController,
  ) {
  }

  isDisabled() {
    return !this.userId || !this.userId.trim();
  }

  /**
   * 「×」ボタン押下
   */
  onNoClick(): void {
    this.navParams.data.modal.dismiss(null);
  }

  /**
   * 「検索」ボタン押下
   */
  async searchClick() {


    if (!TextCheck.regCheckHalf(this.userId.trim())) {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00009.replace('{{value}}', this.gamenLable.userId),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    this.api1107Service.postExec(this.getApi1107InDto(), (outDto: API1107OutDto) => {
      this.apiSuccessed(outDto);
    },
      // fault
      () => {
        this.navParams.data.modal.dismiss(null);
      });
  }

  /**
   * API1107InDtoを作成
   * @returns API1107InDto
   */
  getApi1107InDto(): API1107InDto {
    const inDto: API1107InDto = {};
    // ユーザID
    inDto.userId = this.userId.trim();
    // アカウントロックフラグ
    inDto.isAccountlocked = CD0018Constants.LOCK_FLAG;
    // ソートリスト
    inDto.sort = [{ sortItem: 'lock_datetime', order: SORTBY.ESC }];
    this.inDto = inDto;
    return inDto;
  }

  /**
   * api呼出、戻り値を設定
   * @param outDto API1107OutDto
   */
  async apiSuccessed(outDto: API1107OutDto) {
    const accountList = new Array<AccountDto>();
    Array.from({ length: outDto.accountLockList.length }).map((_, i) => {
      accountList[i] = {};
      // ユーザID
      accountList[i].userId = outDto.accountLockList[i].userId;
      // ロック日時
      if (outDto.accountLockList[i].lockDatetime) {
        accountList[i].lockDatetime = moment(outDto.accountLockList[i].lockDatetime, 'YYYYMMDDHHmmss')
          .format('YYYY年MM月DD日  HH時mm分');
      } else {
        accountList[i].lockDatetime = '';
      }
    });
    this.navParams.data.modal.dismiss([accountList, this.inDto, outDto.resultCode === ResultType.INFO]);
  }
}
