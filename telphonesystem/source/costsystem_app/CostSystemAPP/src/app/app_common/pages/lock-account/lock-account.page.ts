import { Component, OnInit } from '@angular/core';
import { S1104Constants } from '@constant/constant.photo';
import { AlertController, ModalController } from '@ionic/angular';
import { AccountSearchPage } from './account-search/account-search.page';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { AccountDto, LockAccountPageDto } from './dto';
import { CommonStoreService } from '@store';
import { API1107Service, API1108Service } from '@service/api';
import { API1107InDto, API1107OutDto, API1108InDto, API1108OutDto } from '@service/dto';
import { SORTBY, CD0018Constants } from '@constant/code';
import { ResultType } from '../../constant/common';
import { RoutingService } from '@service/app-route.service';
import * as moment from 'moment';
import { AccountInfo } from '@service/dto/api-11-08-in-dto';

@Component({
  selector: 'app-lock-account',
  templateUrl: './lock-account.page.html',
  styleUrls: ['./lock-account.page.scss'],
})
export class LockAccountPage {
  title: string = S1104Constants.TITLE;
  search: string = S1104Constants.SEARCH;
  unLock: string = S1104Constants.UNLOCK;
  userId: string = S1104Constants.USERID;
  lockTime: string = S1104Constants.LOCKTIME;
  outnumber: string = MessageConstants.I00026.replace('{value}', '200');

  dialogflag: boolean = false;

  public gamenDto: LockAccountPageDto = {};
  public inDto: API1107InDto;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    public api1107Service: API1107Service,
    public api1108Service: API1108Service,
    public commonStoreService: CommonStoreService,
    private routerService: RoutingService,
  ) {
  }

  ionViewWillEnter() {
    const history = this.routerService.getHistory();
    if (history[history.length - 2] === '/privacy-policy' || history[history.length - 2] === '/device-manage') {
      // プライバシーポリシー、二段階認証デバイス管理から戻る時、アカウントロック管理画面の値を保持する
      return;
    }
    this.api1107Service.postExec(this.getApi1107InDto(), (outDto: API1107OutDto) => {
      this.api1107Successed(outDto);
    },
      // fault
      () => {
      });
  }

  /**
   * 「検索ボタン」押下
   */
  async openSearchDialog() {
    const modal = await this.modalController.create({
      component: AccountSearchPage,
      componentProps: [],
      cssClass: 'deviceSearchModal'
    });
    await modal.present();
    // apiから検索した内容を設定
    const { data } = await modal.onDidDismiss();
    if (data != null) {
      this.gamenDto.accountList = data[0];
      this.inDto = data[1];
      this.gamenDto.outnumber = data[2];
    }
  }

  /**
   * 「ロック解除ボタン」押下
   */
  async updateClick() {

    if (this.dialogflag) {
      return;
    }
    this.dialogflag = true;

    let isChange = false;
    for (const account of this.gamenDto.accountList) {
      if (account.accountLocked) {
        isChange = true;
        break;
      }
    }
    if (isChange) {
      // 更新チェックボックス選択済みの場合
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00012,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              this.dialogflag = false;
              this.api1108Service.postExec(this.getApi1108InDto(), (outDto: API1108OutDto) => {
                this.api1108Successed(outDto);
              },
                // fault
                () => {
                });

            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
              this.dialogflag = false;
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    } else {
      // 更新チェックボックス未選択の場合
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00039,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      const { data } = await alert.onDidDismiss();
      if (data) {
        this.dialogflag = false;
      }
    }
  }

  /**
   * API1107InDtoを作成
   * @returns API1107InDto
   */
  getApi1107InDto(): API1107InDto {
    const inDto: API1107InDto = {};
    // ユーザID
    inDto.userId = '';
    // アカウントロックフラグ
    inDto.isAccountlocked = CD0018Constants.LOCK_FLAG;

    // ソートリスト
    inDto.sort = [{ sortItem: 'lock_datetime', order: SORTBY.ESC }];
    this.inDto = inDto;
    return inDto;
  }

  /**
   * api呼出、戻り値を設定
   * @param outDto API1107OutDto
   */
  async api1107Successed(outDto: API1107OutDto) {
    this.gamenDto.accountList = new Array<AccountDto>();
    Array.from({ length: outDto.accountLockList.length }).map((_, i) => {
      this.gamenDto.accountList[i] = new AccountDto();
      // ユーザID
      this.gamenDto.accountList[i].userId = outDto.accountLockList[i].userId;
      // ロック日時
      if (outDto.accountLockList[i].lockDatetime) {
        this.gamenDto.accountList[i].lockDatetime = moment(outDto.accountLockList[i].lockDatetime, 'YYYYMMDDHHmmss')
          .format('YYYY年MM月DD日  HH時mm分');
      } else {
        this.gamenDto.accountList[i].lockDatetime = '';
      }
    });
    this.gamenDto.outnumber = outDto.resultCode === ResultType.INFO;
  }

  /**
   * API1108InDtoを作成
   * @returns API1108InDto
   */
  getApi1108InDto(): API1108InDto {
    const inDto: API1108InDto = {};
    inDto.accountInfoList = [];
    for (let i = 0; i <= this.gamenDto.accountList.length - 1; i++) {
      if (this.gamenDto.accountList[i].accountLocked) {
        let account: AccountInfo = null;
        account = {};
        // ユーザID
        account.userId = this.gamenDto.accountList[i].userId;
        inDto.accountInfoList.push(account);
      }
    }
    // アカウントロックフラグ
    inDto.isAccountlocked = CD0018Constants.UNLOCK_FLAG;
    return inDto;
  }

  /**
   * api呼出
   * @param outDto API1108OutDto
   */
  async api1108Successed(outDto: API1108OutDto) {
    // api処理正常終了の場合
    if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: MessageConstants.I00003,
        backdropDismiss: false,
        buttons: [{
          text: AlertWindowConstants.OKENGLISHBUTTON,
          // 更新完了後、直前の検索条件にて一覧を再表示する。
          handler: () => {
            this.api1107Service.postExec(this.inDto, (outDto1107: API1107OutDto) => {
              this.api1107Successed(outDto1107);
            },
              // fault
              () => {
              });
          }
        }]
      });
      await alert.present();
    }
  }

}

