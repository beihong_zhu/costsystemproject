import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoggerStore, LoggerStoreService } from '@store';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-logger',
  templateUrl: './logger.page.html',
  styleUrls: ['./logger.page.scss']
})
export class LoggerPage implements OnInit, OnDestroy {

  loggerStore: LoggerStore;
  messageArr: string[];
  loggerStoreSubscription: Subscription;

  constructor(private loggerStoreService: LoggerStoreService) {

    this.loggerStoreSubscription = this.loggerStoreService.loggerStoreSubscription(storeSubs => {
      this.loggerStore = storeSubs;

    });

  }

  ngOnDestroy(): void {
    this.loggerStoreSubscription.unsubscribe();
  }

  ngOnInit() {


  }

  ionViewDidEnter() {

    console.log('this.loggerStore', this.loggerStore);
    this.messageArr = this.loggerStore.messageArray;
    console.log('this.loggerStore messageArr', this.messageArr);

  }

}
