import { NgModule } from '@angular/core';
import { PresettingLoginPage } from './presetting-login.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [PresettingLoginPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PresettingLoginPage
      }
    ])
  ],
  providers: []
})
export class PresettingLoginModule { }
