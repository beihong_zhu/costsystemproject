import { NgModule } from '@angular/core';
import { LoggerPage } from './logger.page';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [LoggerPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoggerPage
      }
    ])
  ]
})
export class LoggerModule { }
