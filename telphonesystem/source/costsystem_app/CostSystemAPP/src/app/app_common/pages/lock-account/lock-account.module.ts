import { NgModule } from '@angular/core';
import { LockAccountPage } from './lock-account.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AccountSearchPage } from './account-search/account-search.page';

@NgModule({
  providers: [],
  declarations: [LockAccountPage, AccountSearchPage],
  entryComponents: [
    AccountSearchPage
  ],
  imports: [
    SharedModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: LockAccountPage
      }
    ])
  ]
})
export class LockAccountModule { }
