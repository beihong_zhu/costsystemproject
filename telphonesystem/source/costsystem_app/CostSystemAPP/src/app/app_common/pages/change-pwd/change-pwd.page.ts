import { Component } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { Auth } from 'aws-amplify';
import { AmplifyService } from 'aws-amplify-angular';
import { CognitoUser } from 'amazon-cognito-identity-js';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService, AppStateStoreService } from '@store';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { S1102Constants } from '@constant/constant.photo';
import { API1107Service, API1109Service, API1110Service } from '@service/api';
import { API1107InDto, API1107OutDto, API1109InDto, API1109OutDto, API1110InDto, API1110OutDto } from '@service/dto';
import { AuthService, AuthResult } from '../../service/auth.service';
import { AuthConfig } from '../../constant/common';
import { TextCheck } from '../../../app_photo/util/text-check';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-change-pwd',
  templateUrl: './change-pwd.page.html',
  styleUrls: ['./change-pwd.page.scss', './change-pwd.page.mobile.scss', './change-pwd.page.winapp.scss']
})
export class ChangePwdPage {

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    passwordChangeMessege: S1102Constants.PASSWORDCHANGEMESSEGE,
    passwordChangeDescription: S1102Constants.PASSWORDCHANGEDESCRIPTION,
    userIdMessege: S1102Constants.USERIDMESSEGE,
    currentPassword: S1102Constants.CURRENTPASSWORD,
    newPassword: S1102Constants.NEWPASSWORD,
    confirmationPassword: S1102Constants.CONFIRMATIONPASSWORD,
    passwordDescription: S1102Constants.PASSWORDDESCRIPTION,
    passwordChange: S1102Constants.PASSWORDCHANGE,
    passcodeInput: S1102Constants.PASSCODEINPUT,
    confirmSignIn: S1102Constants.CONFIRMSIGNIN,
    cancel: S1102Constants.CANCEL
  };
  isMobile = false;
  isPc = true;
  isConfirmLogin = false;
  userName = null;
  userPwd = null;
  newPwd = null;
  confirmPwd = null;
  secretCode = null;

  platformtype: string;

  private currentUser: CognitoUser;

  constructor(
    private alertController: AlertController,
    private amplifyService: AmplifyService,
    private routeService: RoutingService,
    private authService: AuthService,
    private appLoadingStoreService: AppLoadingStoreService,
    private appStateStoreService: AppStateStoreService,
    private api1107Service: API1107Service,
    private api1109Service: API1109Service,
    private api1110Service: API1110Service,
    private platform: Platform,
  ) {
    if (this.platform.is('android') || this.platform.is('ios')) {
      this.isMobile = true;
    }
    this.isPc = this.appStateStoreService.appState.isPC ? true : false;
    // this.amplifyService.authStateChange$.subscribe(async authState => {
    //   console.log('[ChangePwdPage] authState:', authState.state);
    //   console.log('[ChangePwdPage] user:', authState.user);
    // });
    this.platformtype = 'changePwd-' + this.appStateStoreService.getPlatform();
  }

  ionViewWillEnter() {
    this.isConfirmLogin = false;
    this.userName = '';
    this.userPwd = '';
    this.newPwd = '';
    this.confirmPwd = '';
    this.secretCode = null;
  }

  userfocus() {
    document.getElementById('user').style.color = '#0097E0';
  }

  userblur() {
    document.getElementById('user').style.color = '#000000';
  }

  pwdfocus() {
    document.getElementById('pwd').style.color = '#0097E0';
  }

  pwdblur() {
    document.getElementById('pwd').style.color = '#000000';
  }

  newpwdfocus() {
    document.getElementById('newpwd').style.color = '#0097E0';
  }

  newpwdblur() {
    document.getElementById('newpwd').style.color = '#000000';
  }

  conpwdfocus() {
    document.getElementById('conpwd').style.color = '#0097E0';
  }

  conpwdblur() {
    document.getElementById('conpwd').style.color = '#000000';
  }

  async chgPwd() {


    if (!TextCheck.textCheck(this.userName.trim())) {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }



    if (!this.userName || !this.userPwd || !this.newPwd || !this.confirmPwd) {
      // いずれかに入力が行われていない場合
      await this.showAlert(MessageConstants.E00018);
    } else if (!AuthConfig.PWD_ALLOWED_CHAR.test(this.newPwd)) {
      // 新しいパスワードが不正場合
      await this.showAlert(MessageConstants.E00115);
    } else if (!AuthConfig.PWD_ALLOWED_CHAR.test(this.confirmPwd)) {
      // 新しいパスワード(確認)が不正場合
      await this.showAlert(MessageConstants.E00115);
    } else if (this.userPwd === this.newPwd) {
      // 現在のパスワードと新しいパスワードが同一の場合
      await this.showAlert(MessageConstants.E00075);
    } else if (this.newPwd !== this.confirmPwd) {
      // 新しいパスワードと確認パスワードが一致しない場合
      await this.showAlert(MessageConstants.E00020);
    } else {
      if (this.appStateStoreService.appState.isOffline) {
        // オフラインの場合
        await this.showAlert(MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL));
      } else {
        this.appLoadingStoreService.isloading(true);
        try {
          const user: CognitoUser = await this.authService.signIn(this.userName, this.userPwd);
          // console.log('[ChangePwdPage] authService.signIn', user);
          // 認証成功
          this.currentUser = user;
          if (!this.currentUser.getSignInUserSession()) {
            // 二段階認証
            this.isConfirmLogin = true;
          } else {
            // ログイン済み
            await this.signInSuccess(false);
          }
        } catch (err) {
          // console.log('[ChangePwdPage] authService.signIn catch', err);
          if (err === AuthResult.IncorrectUsername) {
            // ユーザーIDが存在しない
            await this.showAlert(MessageConstants.E00002);
          } else if (err === AuthResult.IncorrectUsernameOrPassword) {
            // 認証失敗
            await this.showAlert(MessageConstants.E00002);
          } else if (err === AuthResult.AccountLockedByStore) {
            // 一時的にログイン不可
            await this.showAlert(MessageConstants.E00104);
          } else {
            // ログインに失敗しました。通信環境の良いところで再度ログインを行っても同じ異常が発生する場合は、システム管理者にお問い合わせください。
            await this.showAlert(MessageConstants.E00114.replace('{1}', AlertWindowConstants.ADMINEMAIL));
          }
        }
        this.appLoadingStoreService.isloading(false);
      }
    }
  }

  async confirmChgPwd() {
    if (!this.secretCode) {
      // 認証コードが未入力の場合
      await this.showAlert(MessageConstants.E00001);
    } else if (this.appStateStoreService.appState.isOffline) {
      // オフラインの場合
      await this.showAlert(MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL));
    } else {
      this.appLoadingStoreService.isloading(true);
      try {
        const user: CognitoUser = await this.authService.confirmSignIn(this.currentUser, this.secretCode);
        // console.log('[ChangePwdPage] authService.confirmSignIn', user);
        // 二段階認証成功
        this.currentUser = user;
        this.amplifyService.setAuthState({ state: 'signedIn', user: this.currentUser });
        await this.signInSuccess(true);
      } catch (err) {
        // console.log('[ChangePwdPage] authService.confirmSignIn catch', err);
        if (err === AuthResult.IncorrectAnswer) {
          // 二段階認証失敗
          await this.showAlert(MessageConstants.E00026);
        } else if (err === AuthResult.InvalidSession) {
          // パスコードの有効期限が切れています
          await this.showAlert(MessageConstants.E00070);
          this.toLogin();
        } else if (err === AuthResult.LockAccountedToDB) {
          // 規定回数二段階認証失敗となった場合、アカウントロック
          await this.showAlertWarning(MessageConstants.W00003.replace('{{value}}', S1102Constants.ADMINEMAIL));
          this.toLogin();
        } else {
          // ログインに失敗しました。通信環境の良いところで再度ログインを行っても同じ異常が発生する場合は、システム管理者にお問い合わせください。
          await this.showAlert(MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL));
        }
      }
      this.secretCode = null;
      this.appLoadingStoreService.isloading(false);
    }
  }

  private async signInSuccess(confirmFlg: boolean) {
    try {
      await this.api1107Exec();
      // アカウントがロックされない場合
      if (confirmFlg) {
        // デバイスアクセス情報登録
        await this.api1109Exec();
        // パスワード変更
        await this.changePassword();
      } else {
        try {
          const kbn = await this.api1110Exec();
          if (kbn === 'NeedRegistDevice') {
            // デバイスアクセス情報登録
            await this.api1109Exec();
          }
          // パスワード変更
          await this.changePassword();
        } catch (err) {
          // アクセス許可が無効
          await this.authService.signOut();
          await this.showAlert(MessageConstants.E00046.replace('{value}', AlertWindowConstants.ADMINEMAIL));
          this.toLogin();
        }
      }
    } catch (err) {
      // アカウントがロックされた場合
      if (confirmFlg) {
        this.currentUser.forgetDevice({
          onSuccess: async (success: string) => {
            await this.authService.signOut();
            await this.showAlertWarning(MessageConstants.W00003.replace('{{value}}', S1102Constants.ADMINEMAIL));
            this.toLogin();
          },
          onFailure: async (err: Error) => {
            await this.authService.signOut();
            await this.showAlertWarning(MessageConstants.E00114.replace('{1}', AlertWindowConstants.ADMINEMAIL));
            this.toLogin();
          }
        });
      } else {
        await this.authService.signOut();
        await this.showAlertWarning(MessageConstants.W00003.replace('{{value}}', S1102Constants.ADMINEMAIL));
        this.toLogin();
      }
    }
  }

  private async api1107Exec(): Promise<void> {
    // アカウントロック対象者情報取得
    const username = this.currentUser.getUsername();
    const inDto: API1107InDto = {
      userId: username,
      isAccountlocked: '1'
    };
    return new Promise((resolve, reject) => {
      this.api1107Service.postExec(inDto, (outDto: API1107OutDto) => {
        if (outDto.accountLockList && outDto.accountLockList.filter(item => item.userId === username).length === 1) {
          reject();
        } else {
          resolve();
        }
      }, () => { this.appLoadingStoreService.isloading(false); }, false);
    });
  }

  private async api1109Exec(): Promise<void> {
    // デバイスアクセス情報登録
    const inDto: API1109InDto = {
      userId: this.currentUser.getUsername(),
      deviceCode: this.authService.getDeviceKey(this.currentUser, true)
    };
    return new Promise((resolve, reject) => {
      this.api1109Service.postExec(inDto,
        (outDto: API1109OutDto) => resolve(),
        () => { this.appLoadingStoreService.isloading(false); }
      );
    });
  }

  private async api1110Exec(): Promise<string> {
    // デバイスアクセス情報取得
    const username = this.currentUser.getUsername();
    const inDto: API1110InDto = {
      userId: username,
      deviceCode: this.authService.getDeviceKey(this.currentUser, true)
    };
    return new Promise((resolve, reject) => {
      this.api1110Service.postExec(inDto, (outDto: API1110OutDto) => {
        if (outDto.isAccessApproval && outDto.isAccessApproval === 1) {
          resolve();
        } else if (outDto.resultCnt === 0) {
          resolve('NeedRegistDevice');
        } else {
          reject();
        }
      }, () => { this.appLoadingStoreService.isloading(false); });
    });
  }

  private async changePassword() {
    await Auth.changePassword(this.currentUser, this.userPwd, this.newPwd);
    await this.authService.signOut();
    await this.showAlertTip(MessageConstants.I00015);
    this.toLogin();
  }

  private async showAlert(msg: string) {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.ERRORTITLE,
      message: msg,
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    await alert.present();
  }

  private async showAlertTip(msg: string) {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.TIPSTITLE,
      message: msg,
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    await alert.present();
  }

  private async showAlertWarning(msg: string) {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.WARNINGTITLE,
      message: msg,
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    await alert.present();
  }

  private toLogin() {
    const isPresetting = localStorage.getItem('com.dkj.photo.presetting');
    if (isPresetting && isPresetting === '1') {
      this.routeService.goRouteLink('/presetting-login');
    } else {
      this.routeService.goRouteLink('/login');
    }
  }

}
