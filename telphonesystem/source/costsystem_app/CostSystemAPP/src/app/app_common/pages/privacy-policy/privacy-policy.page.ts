import { Component, OnInit } from '@angular/core';
import { PrivacyPolicyMessage } from '@constant/constant.photo';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.page.html',
  styleUrls: ['./privacy-policy.page.scss']
})
export class PrivacyPolicyPage implements OnInit {

  ppMessage: string = PrivacyPolicyMessage.PRIVACYPOLICYMSG;

  constructor(
  ) { }

  ngOnInit() {
  }

}
