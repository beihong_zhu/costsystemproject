import { Component, OnInit } from '@angular/core';
import { S1103Constants } from '@constant/constant.photo';
import { AlertController, ModalController } from '@ionic/angular';
import { DeviceSearchPage } from './device-search/device-search.page';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { DeviceDto, DeviceManagePageDto } from './dto';
import { API1112Service, API1111Service } from '@service/api';
import { API1112InDto, API1112OutDto, API1111InDto, API1111OutDto } from '@service/dto';
import { ResultType } from '../../constant/common';
import { CD0023Constants } from '@constant/code';
import * as _ from 'lodash';
import * as moment from 'moment';
import { UpdateUserList1112 } from '@service/dto/api-11-12-in-dto';

@Component({
  selector: 'app-device-manage',
  templateUrl: './device-manage.page.html',
  styleUrls: ['./device-manage.page.scss'],
})
export class DeviceManagePage {
  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    title: S1103Constants.TITLE,
    search: S1103Constants.SEARCH,
    update: S1103Constants.UPDATE,
    updateMessage: S1103Constants.UPDATEMESSAGE,
    effectiveNess: S1103Constants.EFFECTIVENESS,
    invalid: S1103Constants.INVALID,
    userId: S1103Constants.USERID,
    deviceCodeLabel: S1103Constants.DEVICECODE,
    dateMomentLabel: S1103Constants.DATEMOMENT,
    searchTitle: S1103Constants.SEARCHTITLE,
    dialogLabel: S1103Constants.DIALOGLABEL,
    outnumber: MessageConstants.I00026.replace('{value}', '200')
  };

  public gamenDto: DeviceManagePageDto = {};
  public inDto: API1111InDto;
  public validFlag = CD0023Constants.VALID_FLAG;
  public invalidFlag = CD0023Constants.INVALID_FLAG;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    public api1112Service: API1112Service,
    public api1111Service: API1111Service,
  ) {
    this.openSearchDialog();
  }

  async openSearchDialog() {
    const modal = await this.modalController.create({
      component: DeviceSearchPage,
      componentProps: [],
      cssClass: 'deviceSearchModal',
      backdropDismiss: false

    });
    await modal.present();
    // apiから検索した内容を設定
    const { data } = await modal.onDidDismiss();
    if (data != null) {
      this.gamenDto.deviceListBK = _.cloneDeep(data[0]);
      this.gamenDto.deviceList = data[0];
      this.inDto = data[1];
      this.gamenDto.outnumber = data[2];
    } else {
    }
  }

  ionViewWillEnter() {

  }

  async updateClick() {
    let isChange = false;
    const searchCount = this.gamenDto.deviceList ? this.gamenDto.deviceList.length : 0;
    for (let i = 0; i < searchCount; i++) {
      if (this.gamenDto.deviceList[i].isAccessApproval !== this.gamenDto.deviceListBK[i].isAccessApproval) {
        isChange = true;
        break;
      }
    }


    if (isChange) {
      // 画面「デバイス有効／無効」が更新する場合
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00016,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              // 工程削除API
              this.api1112Service.postExec(this.getApi1112InDto(), (outDto: API1112OutDto) => {
                this.apiSuccessed(outDto);
              },
                // fault
                () => {
                });

            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    } else {
      // 画面「デバイス有効／無効」が更新しない場合
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00044,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  /**
   * api1112InDtoを作成
   * @returns API1112InDto
   */
  getApi1112InDto(): API1112InDto {
    const inDto: API1112InDto = {};
    inDto.updateUserList = [];
    const deviceListLength: number = this.gamenDto.deviceList.length;
    for (let i = 0; i <= deviceListLength - 1; i++) {
      let device: UpdateUserList1112 = null;
      device = {};
      // inDto.updateUserList[i] = new UpdateUserList1112();
      // ユーザID
      device.userId = this.gamenDto.deviceList[i].userId;
      // 端末コード
      device.deviceCode = this.gamenDto.deviceList[i].deviceCode;
      // アクセス許可フラグ
      if (this.gamenDto.deviceList[i].isAccessApproval !== this.gamenDto.deviceListBK[i].isAccessApproval) {
        device.isAccessApproval = Number(this.gamenDto.deviceList[i].isAccessApproval);
        inDto.updateUserList.push(device);
      }
    }
    return inDto;
  }

  /**
   * api呼出
   * @param outDto API1112OutDto
   */
  async apiSuccessed(outDto: API1112OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: MessageConstants.I00003,
        buttons: [{
          text: AlertWindowConstants.OKBUTTON,
          // 更新完了後、直前の検索条件にて一覧を再表示する。
          handler: () => {
            this.api1111Service.postExec(this.inDto, (outDto1112: API1112OutDto) => {
              this.api1111Successed(outDto1112);
            },
              // fault
              () => {
              });
          }
        }]
      });
      await alert.present();
    }
  }

  /**
   * api呼出、戻り値を設定
   * @param outDto API1111OutDto
   */
  async api1111Successed(outDto: API1111OutDto) {
    const deviceManageList = new Array<DeviceDto>();
    Array.from({ length: outDto.deviceAccessInfoList.length }).map((item, i) => {
      deviceManageList[i] = {};
      // デバイス有効/無効
      deviceManageList[i].isAccessApproval = String(outDto.deviceAccessInfoList[i].isAccessApproval);
      // ユーザID
      deviceManageList[i].userId = outDto.deviceAccessInfoList[i].userId;
      // 端末コード
      deviceManageList[i].deviceCode = outDto.deviceAccessInfoList[i].deviceCode;
      // デバイス登録日時
      if (outDto.deviceAccessInfoList[i].deviceCreatedDatetime) {
        deviceManageList[i].dateMoment = moment(outDto.deviceAccessInfoList[i].deviceCreatedDatetime, 'YYYYMMDDHHmmss')
          .format('YYYY年MM月DD日  HH時mm分');
      } else {
        deviceManageList[i].dateMoment = '';
      }
    });
    this.gamenDto.outnumber = outDto.resultCode === ResultType.INFO;
    this.gamenDto.deviceList = deviceManageList;
    this.gamenDto.deviceListBK = _.cloneDeep(deviceManageList);
  }

}
