export abstract class ResultType {
  /**
   * 正常
   */
  static readonly NORMAL: string = '0';
  /**
   * 情報
   */
  static readonly INFO: string = '1';
  /**
   * 警告
   */
  static readonly WARN: string = '2';
  /**
   * 業務エラー
   */
  static readonly BUSINESSERROR: string = '3';
  /**
   * システムエラー
   */
  static readonly SYSTEMERROR: string = '9';
}

export abstract class AuthConfig {
  /**
   * パスワード認証失敗規定回数
   */
  static readonly PW_ERR_CNT_MAX: number = 10;
  /**
   * パスワード認証失敗制限時間
   */
  static readonly PW_ERR_LOCK_TIME: number = 10 * 60 * 1000;
  /**
   * 二段階認証失敗規定回数
   */
  static readonly MFA_ERR_CNT_MAX: number = 10;
  /**
   * 6~20文字のパスワードが大文字、小文字、数字、特殊文字のみ許可
   */
  static readonly PWD_ALLOWED_CHAR: RegExp = /^[0-9A-Za-z\^\$\*\.\[\]\{\}\(\)\?"!@#%&\/\\,><':;|_~`]{6,20}$/;
  /**
   * 6~20文字のパスワードが大文字、小文字、数字、特殊文字のみ許可
   */
  static readonly PWD_ALLOWED_CHAR_PRESETTING: RegExp = /^[0-9A-Za-z\^\$\*\.\[\]\{\}\(\)\?"!@#%&\/\\><':;|_~`]{6,20}$/;
  /**
   * 8~20文字のパスワードを大文字、小文字、数字、特殊文字で構成する
   */
  static readonly PWD_PATTERN: RegExp = /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[\^\$\*\.\[\]\{\}\(\)\?\-"!@#%&\/\\,><':;|_~`]).{8,20}/;
}

/**
 * エラーメッセージを更新
 */
export class UpdateMessage {

  /**
   * エラーメッセージを更新
   */
  public static setMessage(messageCode: string, message: string) {
    let msg: string = '';
    // エラーメッセージ設定
    msg = decodeURIComponent(message).replace(/%20/g, ' ');
    msg = msg + '（' + messageCode + '）';
    return msg;
  }
}
