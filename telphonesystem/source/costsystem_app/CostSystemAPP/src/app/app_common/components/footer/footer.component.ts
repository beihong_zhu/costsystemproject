import { Component, OnInit, Input } from '@angular/core';
import { MessageConstants } from '@constant/message-constants';
import { AppState, AppStateStoreService } from '@store';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() class = '';
  copyright = MessageConstants.I00009;
  appState: AppState;

  constructor(private appStateService: AppStateStoreService) {
    this.appState = this.appStateService.appState;

  }

  ngOnInit() { }

}
