import { Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { Location } from '@angular/common';
import { RoutingService } from '@service/app-route.service';
import { Subscription, from } from 'rxjs';
import {
  SlideMenuConstants, MoreMenuPcConstants, S0101Constants, S0102Constants, S0201Constants, S0801Constants, S0301Constants,
  S0901Constants, S0501Constants, S0601Constants, S1201Constants, S0401Constants, S0502Constants, S0602Constants,
  S0902Constants, S0903Constants, S1202Constants, S1203Constants, S1204Constants, S1205Constants, S1206Constants,
  S1207Constants, S1208Constants, S1103Constants, S1104Constants, S1401Constants, S1302Constants, S1303Constants, MoreMenuPhoneConstants
} from '@constant/constant.photo';
import { MenuController, PopoverController, Platform } from '@ionic/angular';
import { MoremenuComponent } from '@components/moremenu/moremenu.component';
import { Events } from '@ionic/angular';
import { HeadState, HeadStateService, MenuStateService, AppStateStoreService, State, CommonStoreService } from '@store';
import { environment } from '@env';
import { TextSinglePopoverComponent } from '../../../app_photo/pages/camera/text-popover/text-popover.component';
import { RouterReducerState } from '@ngrx/router-store';

/**
 * PC状態のメニュー
 */
const PcMenus = {
  logger: {
    title: 'Logger',
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: [],
    slideMenu: []
  },
  /**
   * PC物件表示画面
   */
  buildinglist: {
    title: S0101Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0101,
    slideMenu: []
  },
  /**
   * PC案件表示画面
   */
  project: {
    title: S0102Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '/buildinglist',
    moreMenu: MoreMenuPcConstants.S0102,
    slideMenu: []
  },
  /**
   * PC写真一覧画面
   */
  'photo-list': {
    title: S0501Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    isLoad: true,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0501,
    slideMenu: SlideMenuConstants.S0501
  },
  /**
   * PC写真詳細画面
   */
  'photo-detail': {
    title: S0502Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0502,
    slideMenu: []
  },
  /**
   * PC工程登録画面
   */
  'process-create': {
    title: S0801Constants.CREATETITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0801,
    slideMenu: SlideMenuConstants.S0801
  },
  /**
   * PC工程編集画面
   */
  'process-edit': {
    title: S0801Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '/process-create',
    moreMenu: MoreMenuPcConstants.S0802,
    slideMenu: []
  },
  /**
   * PC図面表示画面
   */
  'draw-display': {
    title: S0601Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0601,
    slideMenu: SlideMenuConstants.S0601
  },
  /**
   * PC図面表示設定画面
   */
  'draw-location': {
    title: S0602Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '/draw-display',
    moreMenu: MoreMenuPcConstants.S0602,
    slideMenu: []
  },
  /**
   * PC写真帳出力情報設定画面
   */
  'photobook-info-set': {
    title: S0901Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0901,
    slideMenu: SlideMenuConstants.S0901
  },
  /**
   * PC写真帳レイアウト設定画面
   */
  'photobook-view-set': {
    title: S0902Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0902,
    slideMenu: SlideMenuConstants.S0902
  },
  /**
   * PC写真帳プレビュー画面
   */
  'photobook-preview': {
    title: S0903Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S0903,
    slideMenu: SlideMenuConstants.S0903
  },
  /**
   * PCチャットルーム一覧画面
   */
  chatroomlist: {
    title: S1201Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    isAddMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S1201,
    slideMenu: SlideMenuConstants.S1201
  },
  /**
   * PCチャットルーム作成画面
   */
  'chatroom-create': {
    title: S1202Constants.TITLE,
    isBack: false,
    isCloseMenu: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '/chatroomlist',
    moreMenu: MoreMenuPcConstants.S1202,
    slideMenu: []
  },
  /**
   * PCチャットルーム編集画面
   */
  'chatroom-edit': {
    title: S1204Constants.TITLE,
    isBack: false,
    isCloseMenu: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '/chattalk',
    moreMenu: MoreMenuPcConstants.S1204,
    slideMenu: []
  },
  /**
   * PCチャット画面
   */
  chattalk: {
    title: '',
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    isSettingMenu: true,
    backRoute: '/chatroomlist',
    moreMenu: MoreMenuPcConstants.S1203,
    slideMenu: []
  },
  /**
   * PC二段階認証デバイス管理画面
   */
  'device-manage': {
    title: S1103Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S1103,
    slideMenu: SlideMenuConstants.S1103
  },
  /**
   * PCアカウントロック管理画面
   */
  'lock-account': {
    title: S1104Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S1104,
    slideMenu: SlideMenuConstants.S1104
  },
  /**
   * PC事前設定情報登録画面
   */
  'presetting-upload': {
    title: S1401Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S1401,
    slideMenu: []
  },
  /**
   * PC事前設定情報登録確認画面
   */
  'presetting-confirm': {
    title: S1401Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '/presetting-upload',
    moreMenu: MoreMenuPcConstants.S1401,
    slideMenu: []
  },
  /**
   * システムエラー画面
   */
  error: {
    title: S1302Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: [],
    slideMenu: []
  },
  /**
   * プライバシーポリシー画面
   */
  'privacy-policy': {
    title: S1303Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPcConstants.S1303,
    slideMenu: []
  }
};

/**
 * スマホ状態のメニュー
 */
const PhoneMenus = {

  logger: {
    title: 'Logger',
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: [],
    slideMenu: []
  },

  /**
   * スマホ物件表示画面
   */
  buildinglist: {
    title: S0101Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0101_PHONE,
    slideMenu: []
  },
  /**
   * スマホ案件表示画面
   */
  project: {
    title: S0102Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '/buildinglist',
    moreMenu: MoreMenuPhoneConstants.S0102_PHONE,
    slideMenu: []
  },
  /**
   * スマホ写真撮影画面
   */
  camera: {
    title: S0301Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0301_PHONE,
    slideMenu: SlideMenuConstants.S0301_PHONE,
  },
  /**
   * スマホ機番撮影画面
   */
  'barcode-scan': {
    title: S0401Constants.TITLE,
    isBack: false,
    isCloseMenu: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0401_PHONE,
    slideMenu: []
  },
  /**
   * スマホ写真一覧画面
   */
  'photo-list': {
    title: S0501Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    isLoad: true,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0501_PHONE,
    slideMenu: SlideMenuConstants.S0501_PHONE
  },
  /**
   * スマホ写真詳細画面
   */
  'photo-detail': {
    title: S0502Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0502_PHONE,
    slideMenu: []
  },
  /**
   * スマホ工程登録画面
   */
  'process-create': {
    title: S0801Constants.CREATETITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0801_PHONE,
    slideMenu: SlideMenuConstants.S0801_PHONE
  },
  /**
   * スマホ工程編集画面
   */
  'process-edit': {
    title: S0801Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '/process-create',
    moreMenu: MoreMenuPhoneConstants.S0802_PHONE,
    slideMenu: []
  },
  /**
   * スマホ図面表示画面
   */
  'draw-display': {
    title: S0601Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0601_PHONE,
    slideMenu: SlideMenuConstants.S0601_PHONE
  },
  /**
   * スマホ大工程選択画面
   */
  bigProcess: {
    title: S0201Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '/project',
    moreMenu: MoreMenuPhoneConstants.S0201_PHONE,
    slideMenu: []
  },
  /**
   * スマホ写真帳出力情報設定画面
   */
  'photobook-info-set': {
    title: S0901Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0901_PHONE,
    slideMenu: SlideMenuConstants.S0901_PHONE
  },
  /**
   * スマホ写真帳レイアウト設定画面
   */
  'photobook-view-set': {
    title: S0902Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0902_PHONE,
    slideMenu: SlideMenuConstants.S0902_PHONE
  },
  /**
   * スマホ写真帳プレビュー画面
   */
  'photobook-preview': {
    title: S0903Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S0903_PHONE,
    slideMenu: SlideMenuConstants.S0903_PHONE
  },
  /**
   * スマホチャット一覧画面
   */
  chatroomlist: {
    title: S1201Constants.TITLE,
    isBack: false,
    isMenu: true,
    isMoreMenu: true,
    isEditMenu: false,
    isAddMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S1201_PHONE,
    slideMenu: SlideMenuConstants.S1201_PHONE,
  },
  /**
   * スマホルーム作成メンバー選択画面
   */
  'chatroom-create-user-select': {
    title: S1205Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    isCloseMenu: true,
    backRoute: '/chatroomlist',
    moreMenu: MoreMenuPhoneConstants.S1202_PHONE,
    slideMenu: []
  },
  /**
   * スマホルーム作成完了画面
   */
  'chatroom-create-complete': {
    title: S1206Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    isCloseMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S1202_PHONE,
    slideMenu: []
  },
  /**
 * スマホルーム編集メンバー選択画面
 */
  'chatroom-edit-user-select': {
    title: S1207Constants.TITLE,
    isBack: false,
    isCloseMenu: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '/chattalk',
    moreMenu: MoreMenuPhoneConstants.S1202_PHONE,
    slideMenu: []
  },
  /**
   * スマホルーム編集完了画面
   */
  'chatroom-edit-complete': {
    title: S1208Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    isCloseMenu: true,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S1202_PHONE,
    slideMenu: []
  },
  /**
   * スマホチャット画面
   */
  chattalk: {
    title: '',
    isBack: true,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    isSettingMenu: true,
    backRoute: '/chatroomlist',
    moreMenu: MoreMenuPhoneConstants.S1203_PHONE,
    slideMenu: []
  },
  /**
   * システムエラー画面
   */
  error: {
    title: S1302Constants.TITLE,
    isBack: false,
    isMenu: false,
    isMoreMenu: false,
    isEditMenu: false,
    backRoute: '',
    moreMenu: [],
    slideMenu: []
  },
  /**
   * プライバシーポリシー画面
   */
  'privacy-policy': {
    title: S1303Constants.TITLE,
    isBack: true,
    isMenu: false,
    isMoreMenu: true,
    isEditMenu: false,
    backRoute: '',
    moreMenu: MoreMenuPhoneConstants.S1303_PHONE,
    slideMenu: []
  }
};

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.scss', './head.component.mobile.scss', './head.component.winapp.scss']
})
export class HeadComponent implements OnInit, OnDestroy {

  // @ViewChild('headTitile', { static: true })
  headTitile: ElementRef;

  isHandled = false;

  headState: HeadState;
  routeState: RouterReducerState;
  popover: HTMLIonPopoverElement = null;

  previousRoute: string;
  subscription: Subscription;
  isPc: boolean;
  platformtype: string;

  headStateSubscribe: Subscription;
  routersubscribe: Subscription;

  constructor(
    private routingService: RoutingService,
    private headStateService: HeadStateService,
    private menuStateService: MenuStateService,
    private menu: MenuController,
    private commonLocation: Location,
    public popoverController: PopoverController,
    public appStateService: AppStateStoreService,
    public events: Events,
    private commonStoreService: CommonStoreService,
    private platform: Platform
  ) {
    this.platformtype = 'header-' + this.appStateService.getPlatform();
    this.isPc = this.appStateService.appState.isPC ? true : false;

    this.headStateSubscribe = this.headStateService.headStateSubscribe((state) => {
      this.headState = state;
    });

    this.routersubscribe = this.routingService.routersubscribe((state) => {
      this.routeState = state;
      this.headSet(this.routeState.state.url);
    });
  }


  headSet(route: string) {
    const strMap = new Map();
    const Menus = this.appStateService.appState.isPC ? PcMenus : PhoneMenus;
    for (const k of Object.keys(Menus)) {
      strMap.set(k, Menus[k]);
    }
    const data = strMap.get(route.split('/')[1]);
    if (!data) {
      return;
    }

    // set head
    // 写真一覧画面 写真撮影画面 機番撮影画面 タイトル欄に案件名が表示
    if (!this.isPc &&
      (data.title === S0501Constants.TITLE || data.title === S0301Constants.TITLE || data.title === S0401Constants.TITLE || data.title === S0502Constants.TITLE)) {
      if (data.title != S0401Constants.TITLE || data.title === S0502Constants.TITLE) {
        this.headStateService.setTitle(this.commonStoreService.commonStore.projectName);

      }
    } else {
      this.headStateService.setTitle(data.title);
    }
    // 図面表示画面
    if (data.title === S0601Constants.TITLE) {
      this.headStateService.setTitle(this.commonStoreService.commonStore.projectName);
    }

    this.headStateService.setIsBack(data.isBack);
    this.headStateService.setIsMenu(data.isMenu);
    this.headStateService.setIsLoad(data.isLoad);

    this.headStateService.setIsMoreMenu(data.isMoreMenu);
    this.headStateService.setIsEditMenu(data.isEditMenu);
    this.headStateService.setIsAddMenu(data.isAddMenu);
    this.headStateService.setIsCloseMenu(data.isCloseMenu);
    this.headStateService.setIsSettingMenu(data.isSettingMenu);
    if (data.backRoute && data.backRoute.length > 0) {
      this.headStateService.setBackRoute(data.backRoute);
    }

    this.headStateService.setBackFunction(null);

    const menuFunction = () => {
      this.menu.enable(true, 'slideMenu');
      this.menu.open('slideMenu');
      this.events.publish('refreshMenu', true);
    };
    this.headStateService.setMenuFunction(menuFunction);

    // set slide Menu
    this.menuStateService.setSlideMenu(data.slideMenu);

    // set more Menu
    const moreFuction = async (ev: Event) => {
      if (this.popover === null) {
        this.popover = await this.popoverController.create({
          component: MoremenuComponent,
          event: ev,
          translucent: true,
          showBackdrop: true
        });
        this.popover.addEventListener('ionPopoverWillDismiss', () => {
          this.popover = null;
        });
        await this.popover.present();
      }
    };

    let moreMenu = data.moreMenu;
    if (false && environment.debug) {
      const debug = {
        id: '',
        title: 'logger',
        path: '/logger',
        switchFlg: false,
        logOutFlg: false,
        pageId: 'S-13-03'
      };
      moreMenu = moreMenu.concat(debug);
    }
    // console.log('moreMenu', moreMenu);
    this.menuStateService.setMoreMenu(moreMenu);
    this.headStateService.setMoreFunction(moreFuction);
    // document.getElementById('moreMenuButton').blur();

  }

  ngOnInit() {
    this.previousRoute = this.routingService.getPreviousUrl();
    this.isPc = this.appStateService.appState.isPC ? true : false;
  }

  // ngAfterViewChecked() {
  // if (this.headTitile !== null && !this.isHandled) {
  //   const element = this.headTitile.nativeElement;
  //   if (element.offsetHeight === 0) {
  //     return;
  //   }
  //   this.isHandled = true;
  //   for (let i = 18; i > 10; i--) {
  //     if (element.offsetHeight >= element.scrollHeight) {
  //       break;
  //     }
  //     element.style.fontSize = i + 'px';
  //   }
  // }
  // }

  backFunction() {
    // console.log('this.headState.title', this.headState.title, this.headState.backRoute);
    if (this.headState.backFunction) {
      this.headState.backFunction();
      return;
    }

    switch (this.headState.title) {
      // プライバシーポリシー画面の場合
      case S1303Constants.TITLE:
        this.routingService.goRouteLink(this.previousRoute);
        break;
      case S1103Constants.TITLE:
        this.routingService.goRouteLink(this.previousRoute);
        break;
      case S1104Constants.TITLE:
        this.routingService.goRouteLink(this.previousRoute);
        break;
      case 'Logger':
        this.routingService.goRouteLink(this.previousRoute);
        break;
      default:
        this.routingService.goRouteLink(this.headState.backRoute);
        break;
    }
  }

  ngOnDestroy() {
    console.log('head ----- ngOnDestroy');

    this.headStateSubscribe.unsubscribe();
    this.routersubscribe.unsubscribe();
  }

  moreMenuBlur(e: KeyboardEvent) {
    // enter space
    if (e.key === 'Enter' || e.key === ' ') {
      const length = document.getElementsByName('moreMenuButton').length;
      for (let i = 0; i < length; i++) {
        document.getElementsByName('moreMenuButton')[i].blur();
      }
    }
  }

  async onPress(ev: Event, message) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    if (this.isPc || (this.commonStoreService.commonStore.projectName !== this.headState.title &&
      this.routingService.getPreviousUrl() !== '/chattalk')) {
      return;
    }
    console.log('onPress', ev);
    const popover = await this.popoverController.create({
      component: TextSinglePopoverComponent,
      componentProps: { message },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

}
