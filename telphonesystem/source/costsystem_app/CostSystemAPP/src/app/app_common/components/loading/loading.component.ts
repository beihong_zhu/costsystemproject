import { Component, Input, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { environment } from '@env';
import { AppLoadingStoreService } from '@store';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  isLoading = false;
  @Input() title = '';
  @Input() cancel = '';

  outLog: OutLog[] = [];

  isLoadingSubscription: Subscription;

  constructor(
    public changeDetectorRef: ChangeDetectorRef,
    private appLoadingStoreService: AppLoadingStoreService
  ) {
    this.isLoadingSubscription = this.appLoadingStoreService.isLoadingSubscription(state => {
      if (state) {
        this.isLoading = state.isLoading;
      } else {
        this.isLoading = false;
      }
    });
  }

  ngOnDestroy(): void {
    this.isLoadingSubscription.unsubscribe();

  }


  ngOnInit() {
    if (environment.debug) {
      window.console.info = (info) => {
        const logDate = moment(new Date()).format('YYYY MM/DD HH:mm:ss SSS');
        this.outLog.push({ isError: false, msg: logDate + JSON.stringify(info) });
        if (this.outLog.length > 200) {
          this.outLog.shift();
        }
        this.changeDetectorRef.detectChanges();
      };
      window.console.error = (...values) => {
        const logDate = moment(new Date()).format('YYYY MM/DD HH:mm:ss SSS');
        if (Array.isArray(values)) {
          let errmsg = '';
          for (const e of values) {
            if (e instanceof Error) {
              errmsg += e.message;
            } else {
              errmsg += JSON.stringify(e);
            }
          }
          this.outLog.push({ isError: true, msg: logDate + errmsg });
          if (this.outLog.length > 200) {
            this.outLog.shift();
          }
        } else {
          this.outLog.push({ isError: true, msg: logDate + JSON.stringify(values) });
          if (this.outLog.length > 200) {
            this.outLog.shift();
          }
        }
        this.changeDetectorRef.detectChanges();
      };
    }
  }

  goBack() {
    this.isLoading = false;
  }


}

export class OutLog {
  isError: boolean;
  msg: string;
}
