import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Auth } from 'aws-amplify';
import { timeout } from 'rxjs/operators';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/mergeMap';
import { environment } from '@env';
// import { getDummyData as getDummyData } from 'src/assets/dummy/test-data';

export class ServiceBaseService<M> {

  protected http: HttpClient;
  protected wsUrl: string;
  protected api: string;
  protected outTime: number;

  constructor(
    http: HttpClient,
  ) {
    this.http = http;
    this.wsUrl = '';
    this.api = '';
    this.outTime = 1000 * 60 * 10;
  }

  protected genSendDto() {
    const sendDto: any = {};
    return sendDto;
  }

  protected send(inDto: any, isDownload: boolean, router: string) {
    if (environment.locadDummy) {
      return new Observable(subscriber => {
        // subscriber.next(getDummyData(this.api, router, inDto));
      });
    } else {
      if (this.wsUrl === environment.noauthurl) {
        return this.httpRequest(inDto, isDownload);
      } else {

        // delete
        let abc: any = Auth.currentSession();

        return Observable.fromPromise(Auth.currentSession()).mergeMap(session => {
          return this.httpRequest(inDto, isDownload, session);
        });
      }
    }
  }

  /**
   * ファイルをアップロードする
   * @param formData ファイル
   */
  protected sendFormData(formData: FormData) {
    return Observable.fromPromise(Auth.currentSession()).mergeMap(session => {
      const headerOptions = {
        Authorization: session.getIdToken().getJwtToken(),
        'X-IDTOKEN-JWT': session.getIdToken().getJwtToken()
      };
      const options = {
        headers: new HttpHeaders(headerOptions)
      };
      return this.http
        .post(this.wsUrl + this.api, formData, options)
        .pipe(timeout(this.outTime));
    });
  }

  private httpRequest(inDto: any, isDownload: boolean, session?: any) {
    let headerOptions = {};
    if (session) {
      headerOptions = {
        ...headerOptions,
        Authorization: session.getIdToken().getJwtToken(),
        'X-IDTOKEN-JWT': session.getIdToken().getJwtToken()
      };
    }
    if (!(inDto instanceof FormData)) {
      headerOptions = {
        ...headerOptions,
        'Content-Type': 'application/json'
      };
    }
    let options = {};
    options = {
      ...options,
      headers: new HttpHeaders(headerOptions)
    };
    if (isDownload) {
      options = {
        ...options,
        observe: 'response',
        responseType: 'blob'
      };
    }
    return this.http
      .post(this.wsUrl + this.api, inDto, options)
      .pipe(timeout(this.outTime));
  }
}
