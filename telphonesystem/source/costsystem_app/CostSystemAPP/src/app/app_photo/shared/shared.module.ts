import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from './material.module';
import { PhotoDrawFooterComponent } from '@components/photo-draw-footer/photo-draw-footer.component';

import { LoadingComponent } from '../../app_common/components/loading/loading.component';
import { HeadComponent } from '../../app_common/components/head/head.component';
import { FooterComponent } from '../../app_common/components/footer/footer.component';
import { MenuComponent } from '@components/menu/menu.component';
import { MoremenuComponent } from '@components/moremenu/moremenu.component';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@NgModule({
  declarations: [
    LoadingComponent,
    HeadComponent,
    FooterComponent,
    MenuComponent,
    MoremenuComponent,
    PhotoDrawFooterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MaterialModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MaterialModule,
    LoadingComponent,
    HeadComponent,
    FooterComponent,
    MenuComponent,
    MoremenuComponent,
    PhotoDrawFooterComponent
  ],
  entryComponents: [
    MoremenuComponent
  ],
  providers: [
    SQLite,
  ]

})
export class SharedModule { }
