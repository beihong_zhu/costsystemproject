
export class PhotoConf {

  /** ローカル写真 */
  static readonly LOCAL_POTO: number = 1;
  /** サーバの写真 */
  static readonly SERVER_POTO: number = 0;



  /** 自動同期 ON */
 static readonly AUTO_UPLOAD_ON: number = 1;

  /** 自動同期 OFF */
 static readonly AUTO_UPLOAD_OFF: number = 0;

 /** defaultリトライ回数 */
static readonly RETRY_DEFAULT: number = 0;

 /** リトライ回数の上限 */
static readonly RETRY_MAX_COUNT: number = 5;

  static readonly NOTIMING: string = '-';

}

