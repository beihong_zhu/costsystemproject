import { Pages } from '@conf/pages';

export const Menus = {
  home: {
    page: Pages.home,
    menus: [
      {
        ...Pages.home,
        url: '/home'
      },
      {
        ...Pages.list,
        url: '/list'
      },
      {
        ...Pages.buildingList,
        url: '/building'
      }
    ]
  },
  list: {
    page: Pages.list,
    menus: [
      {
        ...Pages.home,
        url: '/home'
      },
      {
        ...Pages.list,
        url: '/list'
      },
      {
        ...Pages.buildingList,
        url: '/building'
      }
    ]
  },
  buildingList: {
    page: Pages.buildingList,
    menus: [
      {
        ...Pages.home,
        url: '/home'
      },
      {
        ...Pages.buildingList,
        url: '/building'
      },
      {
        ...Pages.photograph,
        url: '/photograph'
      }
    ]
  },
  photograph: {
    page: Pages.photograph,
    menus: [
      {
        ...Pages.buildingList,
        url: '/building'
      },
      {
        ...Pages.photograph,
        url: '/photograph'
      }
    ]
  }
};
