export const Pages = {
  home: {
    pageId: 'home',
    title: 'ホーム',
    icon: 'home'
  },
  list: {
    pageId: 'list',
    title: 'リスト',
    icon: 'list'
  },
  buildingList: {
    pageId: 'buildinglist',
    title: '物件表示',
    icon: 'business'
  },
  photograph: {
    pageId: 'photograph',
    title: '写真撮影',
    icon: 'camera'
  }
};
