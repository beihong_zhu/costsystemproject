import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoutingService } from '@service/app-route.service';
import {
  ChatRoomStoreService, ChatRoomStore,
  AppState, CommonStore, CommonStoreService, AppStateStoreService, HeadStateService
} from '@store';
import { Store, select } from '@ngrx/store';
import { ChatAPI, escapeString } from '@graphql/chatApi.service';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { AlertController, PopoverController, Platform } from '@ionic/angular';
import { CD0015Constants } from '@constant/code';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';

@Component({
  selector: 'chatroom-edit-user-select',
  templateUrl: './chatroom-edit-user-select.page.html',
  styleUrls: ['./chatroom-edit-user-select.page.scss', './chatroom-edit-user-select.page.mobile.scss'],
})
export class ChatroomEditUserSelectPage implements OnInit, OnDestroy {
  platformtype: string;
  isPc: boolean;
  appState: AppState;
  commonStore: CommonStore;

  public userList; // 備考ユーザーリスト
  public selectedUserList;
  public initSelectUsers;

  private projectId;
  private chatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  constructor(
    public popoverController: PopoverController,
    public headStateService: HeadStateService,
    private routerService: RoutingService,
    private chatRoomStoreService: ChatRoomStoreService,
    public alertController: AlertController,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>,
    private platform: Platform
  ) {
    this.platformtype = 'chatroom-edit-user-select-' + this.appStateService.getPlatform();
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }

  ngOnInit() {
    console.log('ChatroomEditPage ngOnInit');
    this.projectId = this.commonStore.projectId;
    this.subscribeChatRoomStore();
  }

  ngOnDestroy() {
    this.chatRoomStoreSubscription.unsubscribe();

  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chatroom list chatRoomStore', store);
      this.chatRoomStore = store;
      this.userList = store.chatUsers;
      this.selectedUserList = store.chatSelectUsers;

    });

  }

  queryAssocaitedUser() {

    // APIから、チャット情報を取得
    const currentConversationId = this.chatRoomStore.conversationId;
    const successFunc = (associated) => {
      // console.log('associated list', associated);
      this.chatRoomStoreService.setChatAssociated(associated);
      this.getUserList();
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryAssocaitedUser(successFunc, failFunc, currentConversationId);

  }

  // ユーザリストを取得
  getUserList() {

    // const projectId = this.commonStore.projectId;
    const successFunc = (userList) => {
      const chatAssociated = this.chatRoomStore.chatAssociated;
      const chatUsers = [];
      const selectedUserList = [];
      const chatUsersSort = userList.filter((user) => {
        return user.usersInfo.authorId !== '1' && user.usersInfo.isInvalid === false;
      });

      // 氏名昇順で一覧表示する。
      userList = chatUsersSort.sort((a, b) => {
        if (a.userId === this.chatRoomStore.currentConversation.creatorId) { return -1; }
        if (b.userId === this.chatRoomStore.currentConversation.creatorId) { return 1; }
        const nameCompare = String(a.usersInfo.userName).localeCompare(String(b.usersInfo.userName));
        if (nameCompare === 0) {
          return String(a.usersInfo.companyName).localeCompare(String(b.usersInfo.companyName));
        } else {
          return nameCompare;
        }
      });

      if (!this.selectedUserList || this.selectedUserList.length === 0) {
        for (const user of userList) {
          const userInfo = user.usersInfo;
          userInfo.nameAndComp = userInfo.userName + '、' + userInfo.companyName;
          userInfo.isChecked = false;
          userInfo.disabled = false;
          const chatAssociatedNoInvalid = chatAssociated.filter((userA) => {
            return !userA.isInvalid;
          });
          chatAssociatedNoInvalid.map((userA) => {
            if (userA.userId === userInfo.userId) {
              userInfo.isChecked = true;
              // Lv.4の場合、すべての削除ボタンを非活性
              if (CD0015Constants.AUTHORITY04 === this.commonStore.authorId) {
                userInfo.disabled = true;
              } else if (userA.userId === this.chatRoomStore.currentConversation.creatorId) {
                // ルーム作成ユーザーは非活性
                userInfo.disabled = true;
              }
              selectedUserList.push(userInfo);
            }
          });
          chatUsers.push(user.usersInfo);
        }
        this.chatRoomStoreService.setChatUsers(chatUsers);
        this.chatRoomStoreService.setChatSelectUsers(selectedUserList);
        if (!this.initSelectUsers || this.initSelectUsers.length === 0) {
          const initSelect = JSON.stringify(selectedUserList);
          this.initSelectUsers = JSON.parse(initSelect);
          this.chatRoomStoreService.setInitSelectUsers(this.initSelectUsers);
        }
      }
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryConversationUser(successFunc, failFunc, this.projectId);

  }

  ionViewDidEnter() {
    console.log('ChatRoomEditPage ionViewDidEnter');
    this.gobackFunction();
    this.queryAssocaitedUser();

  }

  // 戻るボタン押下
  private gobackFunction() {

    const backFunction = async () => {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00013,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              const url = '/chattalk';
              this.headStateService.setBackFunction(null);
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
        ]
      });
      await alert.present();
    };

    this.headStateService.setBackFunction(backFunction);
  }

  nextButtonClick() {
    console.log('>>> cancel button click ...');
    const selectedUserList = [];
    for (const user of this.userList) {
      if (user.isChecked) {
        selectedUserList.push(user);
      }
    }
    this.chatRoomStoreService.setChatSelectUsers(selectedUserList);

    const url = '/chatroom-edit-complete';
    this.routerService.goRouteLink(url);
  }

  async onPress(ev: Event, user) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [user.nameAndComp];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


}
