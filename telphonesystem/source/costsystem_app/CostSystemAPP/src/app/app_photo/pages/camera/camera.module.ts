import { NgModule } from '@angular/core';
import { CameraPage } from './camera.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { CameraPopoverComponent } from '@pages/camera/popover/camera-popover.component';
import { CameraModalEditComponent } from '@pages/camera/modal-edit/camera-modal-edit.component';
import { CameraBlackboardComponent } from './blackboard/camera-blackboard.component';
import { CameraPreview, } from '@ionic-native/camera-preview/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Device } from '@ionic-native/device/ngx';

@NgModule({
  declarations: [
    CameraPage,
    CameraBlackboardComponent,
    CameraPopoverComponent,
    CameraModalEditComponent
  ],
  providers: [
    Device,
    Keyboard,
    UniqueDeviceID,
    CameraPreview,
  ],
  entryComponents: [
    CameraPopoverComponent,
    CameraModalEditComponent,
    CameraBlackboardComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CameraPage
      }
    ])
  ]
})
export class CameraModule { }
