import { WorkDto } from './work.dto';

export class ProcessDto {
  bigProcessId?: string;
  processId?: number;
  isOpen?: boolean;
  displayOrder?: number;
  processName?: string;
  workList?: Array<WorkDto>;
}
