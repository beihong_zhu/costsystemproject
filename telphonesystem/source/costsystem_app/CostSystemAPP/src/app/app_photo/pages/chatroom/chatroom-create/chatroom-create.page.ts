import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { ToastController, AlertController } from '@ionic/angular';
import { RoutingService } from '@service/app-route.service';
import {
  ChatRoomStore, ChatRoomStoreService,
  CommonStore, CommonStoreService, AppState, AppStateStoreService, HeadStateService, AppLoadingStoreService
} from '@store';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { ChatAPI, escapeString } from '@graphql/chatApi.service';
import { Store, select } from '@ngrx/store';
import { TextCheck } from '../../../util/text-check';

@Component({
  selector: 'app-chatroom-create',
  templateUrl: './chatroom-create.page.html',
  styleUrls: ['./chatroom-create.page.scss'],
})
export class ChatroomCreatePage implements OnInit, OnDestroy {

  isPc: boolean;
  chatroomName: string;
  appState: AppState;
  commonStore: CommonStore;

  isCreate: boolean = false;

  public userList; // 備考ユーザーリスト
  public selectedUserList; // 選択済みユーザーリスト

  private userId;
  private projectId;
  private chatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  constructor(
    private routerService: RoutingService,
    public toastController: ToastController,
    public headStateService: HeadStateService,
    public alertController: AlertController,
    private chatRoomStoreService: ChatRoomStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>
  ) {
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }


  ngOnInit() {
    console.log('ChatroomCreatePage ngOnInit');
    this.isPc = this.appState.isPC ? true : false;
    this.userId = this.commonStore.userId;
    this.projectId = this.commonStore.projectId;
    this.subscribeChatRoomStore();

  }

  ngOnDestroy() {
    this.chatRoomStoreSubscription.unsubscribe();

  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chatroom list chatRoomStore', store);
      this.chatRoomStore = store;
      this.userList = store.chatUsers;
      this.selectedUserList = store.chatSelectUsers;
      if (store.conversationName === '') {
        this.chatroomName = '';
        this.chatRoomStoreService.setChatConversationName('NONE');
      }

    });

  }

  // ユーザリストを取得
  getUserList() {
    // const projectId = this.commonStore.projectId;
    const successFunc = (userList) => {
      const chatUsers = [];
      const selectedUserList = [];
      const chatUsersSort = userList.filter((user) => {
        return user.usersInfo.authorId !== '1' && user.usersInfo.isInvalid === false;
      });

      // 氏名昇順で一覧表示する。
      userList = chatUsersSort.sort((a, b) => {
        if (a.userId === this.commonStore.userId) { return -1; }
        if (b.userId === this.commonStore.userId) { return 1; }
        const nameCompare = String(a.usersInfo.userName).localeCompare(String(b.usersInfo.userName));
        if (nameCompare === 0) {
          return String(a.usersInfo.companyName).localeCompare(String(b.usersInfo.companyName));
        } else {
          return nameCompare;
        }
      });

      for (const user of userList) {
        const userInfo = user.usersInfo;
        userInfo.nameAndComp = userInfo.userName + '、' + userInfo.companyName;
        // ログインユーザー(ルーム作成者)を「参加中」にする。
        // ログインユーザー(ルーム作成者)を画面右側に表示し、削除ボタンを非活性にする。
        if (userInfo.userId === this.commonStore.userId) {
          userInfo.isChecked = true;
          userInfo.disabled = true;
          selectedUserList.push(userInfo);
        } else {
          userInfo.isChecked = false;
        }
        chatUsers.push(user.usersInfo);
      }
      this.chatRoomStoreService.setChatUsers(chatUsers);
      this.chatRoomStoreService.setChatSelectUsers(selectedUserList);
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryConversationUser(successFunc, failFunc, this.projectId);

  }

  ionViewDidEnter() {
    console.log('ChatRoomCreatePage ionViewDidEnter');
    this.gobackFunction();
    this.getUserList();

  }

  // 戻るボタン押下
  private gobackFunction() {

    const backFunction = async () => {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00011,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              this.headStateService.setBackFunction(null);
              const url = '/chatroomlist';
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
        ]
      });
      await alert.present();
    };

    this.headStateService.setBackFunction(backFunction);
  }



  appendHandle(user) {

    const userList = this.userList.map((userA) => {
      if (userA.userId === user.userId) {
        userA.isChecked = true;
        return userA;
      }
      return userA;
    });
    this.chatRoomStoreService.setChatUsers(userList);
    console.log(`add userList `, this.userList);

    // 登録メンバーを追加
    user.disabled = false;
    this.selectedUserList.push(user);

    // 登録メンバー一覧に登録順で追加表示する。
    this.chatRoomStoreService.setChatSelectUsers(this.selectedUserList);
    console.log(`add selectedUserList `, this.selectedUserList);

  }

  deleteHandle(user) {

    const userList = this.userList.map((userA) => {
      if (userA.userId === user.userId) {
        userA.isChecked = false;
        return userA;
      }
      return userA;
    });
    this.chatRoomStoreService.setChatUsers(userList);
    console.log(`delete userList `, this.userList);

    const userSelectList = this.selectedUserList.filter((userA) => {
      if (userA.isChecked) {
        return userA;
      }
    });
    this.chatRoomStoreService.setChatSelectUsers(userSelectList);
    console.log(`delete selectedUserList `, this.selectedUserList);

  }

  async cancenButtonClick() {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.CONFIRMTITLE,
      message: MessageConstants.C00011,
      buttons: [
        {
          text: AlertWindowConstants.OKBUTTON,
          handler: () => {
            this.routerService.goRouteLink('/chatroomlist');
          }
        },
        {
          text: AlertWindowConstants.CANCELBUTTON,
          role: 'cancel',
          cssClass: 'primary',
          handler: (blah) => {
          }
        }
      ]
    });
    await alert.present();
  }

  goChatRoomList() {
    this.routerService.goRouteLink('/chatroomlist');

  }


  goChattalk(conversation) {
    // console.log('conversation', conversation);
    const conversationId = conversation.conversationId;
    const conversationName = conversation.conversationName;
    this.chatRoomStoreService.setChatCurrentConversation(conversation);
    this.chatRoomStoreService.setChatConversationId(conversationId);
    this.chatRoomStoreService.setChatConversationName(conversationName);

    this.chatRoomStoreService.setChatAssociated([]);

    this.chatRoomStoreService.setChatMessages([]);
    const url = '/chattalk';
    this.routerService.goRouteLink(url);

  }



  async completeButtonClick() {

    if (this.isCreate) {
      return;
    }

    this.isCreate = true;



    if (!this.chatroomName || this.chatroomName.trim().length === 0) {
      const message = `Input Chatroom Name`;
      // 文字が入力されていない場合
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00038,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      this.isCreate = false;
      return;
    }

    if (!TextCheck.textCheck(this.chatroomName)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      this.isCreate = false;
      return;
    }

    if (!this.selectedUserList || this.selectedUserList.length === 0) {
      const message = `Select User`;
      this.presentToast(message);
      this.isCreate = false;
      return;
    }

    this.appLoadingStoreService.isloading(true);
    // create Conversation
    const successFunc = (createConversation) => {
      console.log('createConversation', createConversation);
      this.appLoadingStoreService.isloading(false);
      // this.goChatRoomList();
      this.goChattalk(createConversation.data.createConversationDev);

    };
    const failFunc = async (err) => {
      console.log('err', err);
      this.isCreate = false;
      this.appLoadingStoreService.isloading(false);
      if (this.appStateService.appState.isOffline) {
        // オフライン可能以外画面の場合
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          message: MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL),
          buttons: [AlertWindowConstants.OKENGLISHBUTTON],
          backdropDismiss: false
        });
        await alert.present();
      } else {
        // 登録に失敗した場合
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          message: MessageConstants[err],
          buttons: [AlertWindowConstants.OKENGLISHBUTTON],
          backdropDismiss: false
        });
        await alert.present();
      }

    };
    ChatAPI.apiService.createConversation(successFunc, failFunc,
      this.selectedUserList, this.chatroomName.trim(), this.projectId, this.userId);

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  isDisabled() {
    return !this.chatroomName || this.chatroomName === null || this.chatroomName.trim() === '';
  }

  focusInName() {
    document.getElementById('name').style.color = '#0097E0';
  }
  focusOutName() {
    document.getElementById('name').style.color = '#333333';
  }

}
