import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductstockPageRoutingModule } from './productstock-routing.module';

import { ProductstockPage } from './productstock.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductstockPageRoutingModule
  ],
  declarations: [ProductstockPage]
})
export class ProductstockPageModule {}
