import { Component, OnInit } from '@angular/core';
import { S2011Constants } from '@constant/constant.photo';
import { AlertController, NavParams } from '@ionic/angular';
import { SysOrderManagementDto } from '../dto';

@Component({
  selector: 'app-order-edit',
  templateUrl: './ordersmanagement-edit.component.html',
  styleUrls: ['./ordersmanagement-edit.component.scss']
})

export class OrdermanagementEditComponent implements OnInit {
  /**
   * 获取画面項目名称
   */
  public ordermanagementEditLable: any = {
    title: S2011Constants.TITLE,
    agentName: S2011Constants.AGENTNAME,
    channelName: S2011Constants.CHANNELNAME,
    BillID: S2011Constants.BILLID,
    orderId: S2011Constants.ORDERID,
    requestId: S2011Constants.REQUESTID,
    number: S2011Constants.NUMBER,
    money: S2011Constants.MONEY,
    status: S2011Constants.STATUS,
    time: S2011Constants.TIME,
    change: S2011Constants.CHANGE,
    cancel: S2011Constants.CANCEL
  };


  public sysOrderManagement: SysOrderManagementDto = new SysOrderManagementDto();
  constructor(
    public alertController: AlertController,
    public navParams: NavParams) {
      let sysOrderManagement: SysOrderManagementDto = new SysOrderManagementDto();
      sysOrderManagement = navParams.data.value;
      this.sysOrderManagement.AgentReduceName = sysOrderManagement.AgentReduceName
      this.sysOrderManagement.abbreviation = sysOrderManagement.abbreviation;
      this.sysOrderManagement.reqId = sysOrderManagement.reqId;
      this.sysOrderManagement.orderId = sysOrderManagement.orderId;
      this.sysOrderManagement.billId = sysOrderManagement.billId;
      this.sysOrderManagement.number = sysOrderManagement.number;
      this.sysOrderManagement.money = sysOrderManagement.money;
      this.sysOrderManagement.status = sysOrderManagement.status;
      this.sysOrderManagement.ts = sysOrderManagement.ts;
      
  }

  ngOnInit() {

  }

  /**
   * 更新按钮点击事件
   */
  async updateClick() {

  }

  /**
   * 取消按钮点击事件
   */
  async cancelClick() {

  }

  /**
   * 关闭按钮点击事件
   */
  async closeClick() {
    this.navParams.data.modal.dismiss(null);
  }
}
