

import * as lodash from 'lodash';


export class Photosort {


  processBy(photos, ascvalue): any {
    const descOrasc = ascvalue === '1' ? 'asc' : 'desc';
    return lodash.orderBy(photos, [(ph) => ph.processOrder || 100,
    (ph) => ph.workOrder || 100,
    (ph) => ph.unitMark || '',
      'timingOrder',
    (ph) => ph.photoDate || ''], [descOrasc, descOrasc, 'asc', 'asc', 'asc']);

  }


  unitmarkBy(photos, ascvalue): any {
    const descOrasc = ascvalue === '1' ? 'asc' : 'desc';
    return lodash.orderBy(photos, [
      (ph) => ph.unitMark || '',
      (ph) => ph.processOrder || 100,
      (ph) => ph.workOrder || 100,
      'timingOrder',
      (ph) => ph.photoDate || ''], [descOrasc, 'asc', 'asc', 'asc', 'asc']);

  }

  floorkBy(photos, ascvalue): any {
    const descOrasc = ascvalue === '1' ? 'asc' : 'desc';
    return lodash.orderBy(photos, [
      (ph) => ph.floorOrder,
      (ph) => ph.processOrder || 100,
      (ph) => ph.workOrder || 100,
      (ph) => ph.unitMark || '',
      'timingOrder',
      (ph) => ph.photoDate || ''], [descOrasc, 'asc', 'asc', 'asc', 'asc', 'asc']);

  }

  datekBy(photos, ascvalue): any {
    const descOrasc = ascvalue === '1' ? 'asc' : 'desc';
    return lodash.orderBy(photos, [
      (ph) => ph.photoDate || ''], [descOrasc]);

  }


  detailprocessBy(photos): any {
    return lodash.orderBy(photos, [(ph) => ph.unitMark || '',
      'timingOrder',
    (ph) => ph.photoDate || ''], ['asc', 'asc', 'asc']);

  }

  detailunitmarkBy(photos): any {
    return lodash.orderBy(photos, [
      (ph) => ph.processOrder || 100,
      (ph) => ph.workOrder || 100,
      'timingOrder',
      (ph) => ph.photoDate || ''], ['asc', 'asc', 'asc', 'asc']);

  }

  detailfloorkBy(photos): any {
    return lodash.orderBy(photos, [
      (ph) => ph.processOrder || 100,
      (ph) => ph.workOrder || 100,
      (ph) => ph.unitMark || '',
      'timingOrder',
      (ph) => ph.photoDate || ''], ['asc', 'asc', 'asc', 'asc', 'asc']);

  }




  simplePhoto_timingOrderSort(photos): any {
    return lodash.orderBy(photos, [
      (ph) => ph.processOrder || 100,
      (ph) => ph.workOrder || 100,
      'timingOrder',
    ], ['asc', 'asc', 'asc']);

  }



}
