export declare interface FloorDto {
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 表示順
  displayOrder?: number;
  // 物件ID
  buildingId?: number;
}
