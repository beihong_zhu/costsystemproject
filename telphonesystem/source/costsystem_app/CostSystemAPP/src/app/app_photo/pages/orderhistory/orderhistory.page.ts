import { Component, OnInit } from '@angular/core';
import { API3009Service } from '@service/api';
import { API3009InDto, API3009OutDto } from '@service/dto';

import { SysOrderHistoryModel } from '@service/dto/api-30-09-out-dto';
import { AppState, CommonStore } from '@store';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysOrderManagementDto } from './dto';

@Component({
  selector: 'app-orderhistory',
  templateUrl: './orderhistory.page.html',
  styleUrls: ['./orderhistory.page.scss'],
})
export class OrderhistoryPage implements OnInit {

  public aggencyAccount: string;
  public aggecyName: string;

  //代理简称
  public aggecyBre: string;

  public aggencyLimited : string;
  public localLimited : string;
  public channelLimited : string ;
  public telNumber : string ;
  public ocrManufacturingDate : string;
  

  public sysOrderHistoryList: Array<SysOrderManagementDto> = new Array<SysOrderManagementDto>();


  constructor(private api3009Service: API3009Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api3009ServicePostExec();
  }

  public onSuccess() {
    this.api3009ServicePostExec();
  }

  private get3009InDto() {
    let ap3009InDto: API3009InDto = {};
    ap3009InDto.aggencyLimited = this.aggencyLimited;
    ap3009InDto.localLimited = this.localLimited;
    ap3009InDto.channelLimited = this.channelLimited;
    ap3009InDto.telNumber = this.telNumber;
    ap3009InDto.ocrManufacturingDate = this.channelLimited;
    return ap3009InDto;
  }

  private api3009ServicePostExec() {

    // 写真帳出力情報取得
    this.api3009Service.postExecNew(this.get3009InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3009successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api3009successed(outDto: API3009OutDto) {
    this.sysOrderHistoryList = new Array<SysOrderManagementDto>();

    const loadSysFundRecorderInfo = JSON.parse(JSON.stringify(outDto.resultCode));

    for (const sysUserInfoModel of loadSysFundRecorderInfo) {
      let sysOrderHistory: SysOrderManagementDto = new SysOrderManagementDto();
      sysOrderHistory.AgentReduceName = sysUserInfoModel.AgentReduceName;
      sysOrderHistory.abbreviation= sysUserInfoModel.abbreviation;
      sysOrderHistory.reqId= sysUserInfoModel.reqId;
      sysOrderHistory.orderId= sysUserInfoModel.orderId;
      sysOrderHistory.billId = sysUserInfoModel.billId;
      sysOrderHistory.number = sysUserInfoModel.number;
      sysOrderHistory.money = sysUserInfoModel.money;
      sysOrderHistory.status = sysUserInfoModel.status;
      sysOrderHistory.ts = sysUserInfoModel.ts;


      this.sysOrderHistoryList.push(sysOrderHistory);
    }
  }


}
