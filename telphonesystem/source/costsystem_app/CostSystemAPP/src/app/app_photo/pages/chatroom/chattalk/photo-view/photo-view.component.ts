import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { AppState, AppStateStoreService } from '@store';

@Component({
  selector: 'app-photo-view',
  templateUrl: './photo-view.component.html',
  styleUrls: ['./photo-view.component.scss'],
})
export class PhotoViewComponent implements OnInit {
  isPc: boolean;
  appState: AppState;

  photoPath: string;

  constructor(
    private params: NavParams,
    private appStateService: AppStateStoreService
  ) {
    this.appState = this.appStateService.appState;
    this.photoPath = params.data.formart;
  }

  ngOnInit() {
    this.isPc = this.appState.isPC ? true : false;
  }

  close(): void {
    this.params.data.modal.dismiss(null);
  }

}
