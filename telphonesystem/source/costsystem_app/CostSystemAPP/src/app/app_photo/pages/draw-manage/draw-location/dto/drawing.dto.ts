import { FloorDto } from './floor.dto';

export declare interface DrawingDto {
  // ファイル名称
  fileName?: string;
  // ファイルイメージ
  fileImage?: string;
  // フロア
  floor?: FloorDto;
  // フロアID
  floorId?: number;
  // 図面ID
  drawingId?: string;
}
