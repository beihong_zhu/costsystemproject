import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsshelfPage } from './productsshelf.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsshelfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsshelfPageRoutingModule {}
