import { Component, Input, OnInit } from '@angular/core';
import { RoutingService } from '@service/app-route.service';
import { AppStateStoreService } from '@store';

@Component({
  selector: 'app-photo-grid-item',
  templateUrl: './photo-grid-item.component.html',
  styleUrls: ['./photo-grid-item.component.scss', './photo-grid-item.component.mobile.scss'],
})
export class PhotoGridItemComponent implements OnInit {

  @Input() photo;
  @Input() selectp: (photo) => {};
  @Input() showp: (photo) => {};
  isPc: boolean;
  photoPath: string;
  platformtype: string;

  constructor(
    private appStateService: AppStateStoreService,
    public routingService: RoutingService
  ) {
    this.isPc = this.appStateService.appState.isPC;
    this.platformtype = 'photo-grid-item-' + this.appStateService.getPlatform();
  }

  ngOnInit() {
    this.photoPath = this.photo.chatTalkPictures;
  }

  imageclick(event) {
    this.selectp(this.photo);
  }

  onPreview(event) {
    this.showp(String(this.photo.chatTalkPictures));
  }
}
