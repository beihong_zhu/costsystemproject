import { NgModule } from '@angular/core';
import { PhotobookPreviewPage } from './photobook-preview.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [
  ],
  declarations: [PhotobookPreviewPage],
  entryComponents: [],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotobookPreviewPage
      }
    ])
  ]
})
export class PhotobookPreviewModule { }
