import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddcreditmanagementPage } from './addcreditmanagement.page';

describe('AddcreditmanagementPage', () => {
  let component: AddcreditmanagementPage;
  let fixture: ComponentFixture<AddcreditmanagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcreditmanagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddcreditmanagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
