import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FinancerecorderPage } from './financerecorder.page';

const routes: Routes = [
  {
    path: '',
    component: FinancerecorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinancerecorderPageRoutingModule {}
