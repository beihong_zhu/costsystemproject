export class SysAgencyInfoDto {
  
   // 代理账户
   accountagency?: string;
   // 简称
   agecnyAbbreviate?: string;
   // 省市
   province?: string; 
   // 类型
   type?: string;
   // 模式
   mode?: string;
   // 可用资金
   balance?: string;
    // 授信额度
    use?: string;
    //  状态
    status?: string;

}
