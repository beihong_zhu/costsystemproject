import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateregularPage } from './createregular.page';

describe('CreateregularPage', () => {
  let component: CreateregularPage;
  let fixture: ComponentFixture<CreateregularPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateregularPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateregularPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
