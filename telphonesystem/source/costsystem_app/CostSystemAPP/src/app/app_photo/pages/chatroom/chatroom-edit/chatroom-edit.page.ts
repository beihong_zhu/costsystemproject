import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoutingService } from '@service/app-route.service';
import {
  ChatRoomStoreService, ChatRoomStore,
  CommonStore, CommonStoreService, AppState, AppStateStoreService, HeadStateService, AppLoadingStoreService
} from '@store';
import { Store, select } from '@ngrx/store';
import { ChatAPI, escapeString } from '@graphql/chatApi.service';
import { CD0015Constants } from '@constant/code';
import { AlertController } from '@ionic/angular';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';

@Component({
  selector: 'app-chatroom-edit',
  templateUrl: './chatroom-edit.page.html',
  styleUrls: ['./chatroom-edit.page.scss'],
})

export class ChatroomEditPage implements OnInit, OnDestroy {

  isPc: boolean;
  appState: AppState;
  chatroomName: string;
  commonStore: CommonStore;

  isEdit: boolean = false;

  public userList; // 備考ユーザーリスト
  public selectedUserList; // 選択済みユーザーリスト

  public initSelectedUsetList;

  private projectId;
  private chatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  constructor(
    private routerService: RoutingService,
    private chatRoomStoreService: ChatRoomStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    public headStateService: HeadStateService,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    public alertController: AlertController,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>
  ) {
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }

  ngOnInit() {
    console.log('ChatroomEditPage ngOnInit');
    this.projectId = this.commonStore.projectId;
    this.subscribeChatRoomStore();

  }

  ngOnDestroy() {
    this.chatRoomStoreSubscription.unsubscribe();

  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chatroom list chatRoomStore', store);
      this.chatRoomStore = store;
      this.userList = store.chatUsers;
      this.selectedUserList = store.chatSelectUsers;
      this.chatroomName = store.conversationName;

    });

  }

  //
  queryAssocaitedUser() {

    // APIから、チャット情報を取得
    const currentConversationId = this.chatRoomStore.conversationId;
    const successFunc = (associated) => {
      // console.log('associated list', associated);
      this.chatRoomStoreService.setChatAssociated(associated);
      this.getUserList();
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryAssocaitedUser(successFunc, failFunc, currentConversationId);

  }

  // ユーザリストを取得
  getUserList() {
    // const projectId = this.commonStore.projectId;
    const successFunc = (userList) => {
      const chatAssociated = this.chatRoomStore.chatAssociated;
      const chatUsers = [];
      const selectedUserList = [];
      const chatUsersSort = userList.filter((user) => {
        return user.usersInfo.authorId !== '1' && user.usersInfo.isInvalid === false;
      });

      // 氏名昇順で一覧表示する。
      userList = chatUsersSort.sort((a, b) => {
        if (a.userId === this.chatRoomStore.currentConversation.creatorId) { return -1; }
        if (b.userId === this.chatRoomStore.currentConversation.creatorId) { return 1; }
        const nameCompare = String(a.usersInfo.userName).localeCompare(String(b.usersInfo.userName));
        if (nameCompare === 0) {
          return String(a.usersInfo.companyName).localeCompare(String(b.usersInfo.companyName));
        } else {
          return nameCompare;
        }
      });

      for (const user of userList) {
        const userInfo = user.usersInfo;
        userInfo.nameAndComp = userInfo.userName + '、' + userInfo.companyName;
        userInfo.isChecked = false;
        userInfo.disabled = false;
        const chatAssociatedNoInvalid = chatAssociated.filter((userA) => {
          return !userA.isInvalid;
        });
        chatAssociatedNoInvalid.map((userA) => {
          if (userA.userId === userInfo.userId) {
            userInfo.isChecked = true;
            // Lv.4の場合、すべての削除ボタンを非活性
            if (CD0015Constants.AUTHORITY04 === this.commonStore.authorId) {
              userInfo.disabled = true;
            } else if (userA.userId === this.chatRoomStore.currentConversation.creatorId) {
              // ルーム作成ユーザーは非活性
              userInfo.disabled = true;
            }
            selectedUserList.push(userInfo);
          }
        });
        chatUsers.push(user.usersInfo);
      }
      this.chatRoomStoreService.setChatUsers(chatUsers);
      this.chatRoomStoreService.setChatSelectUsers(selectedUserList);
      if (!this.initSelectedUsetList || this.initSelectedUsetList.length === 0) {
        const initSelect = JSON.stringify(selectedUserList);
        this.initSelectedUsetList = JSON.parse(initSelect);
      }
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryConversationUser(successFunc, failFunc, this.projectId);

  }


  ionViewDidEnter() {
    console.log('ChatRoomEditPage ionViewDidEnter');
    this.gobackFunction();
    this.queryAssocaitedUser();
  }

  // 戻るボタン押下
  private gobackFunction() {

    const backFunction = async () => {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00013,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              this.headStateService.setBackFunction(null);
              const url = '/chattalk';
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
        ]
      });
      await alert.present();
    };

    this.headStateService.setBackFunction(backFunction);
  }

  appendHandle(user) {
    console.log(`call API :: append handle. TODO `);
    const userList = this.userList.map((userA) => {
      if (userA.userId === user.userId) {
        userA.isChecked = true;
        return userA;
      }
      return userA;
    });
    this.chatRoomStoreService.setChatUsers(userList);
    console.log(`add userList `, this.userList);

    this.selectedUserList.push(user);
    this.chatRoomStoreService.setChatSelectUsers(this.selectedUserList);
    console.log(`add selectedUserList `, this.selectedUserList);

  }

  deleteHandle(user) {
    console.log(`call API :: delete handle. TODO `);
    const userList = this.userList.map((userA) => {
      if (userA.userId === user.userId) {
        userA.isChecked = false;
        return userA;
      }
      return userA;
    });
    this.chatRoomStoreService.setChatUsers(userList);
    console.log(`delete userList `, this.userList);

    const userSelectList = this.selectedUserList.filter((userA) => {
      if (userA.userId !== user.userId) {
        return userA;
      }
    });
    this.chatRoomStoreService.setChatSelectUsers(userSelectList);
    console.log(`delete selectedUserList `, this.selectedUserList);
  }

  async cancenButtonClick() {
    console.log('>>> cancel button click ...');

    const alert = await this.alertController.create({
      header: AlertWindowConstants.CONFIRMTITLE,
      message: MessageConstants.C00013,
      buttons: [
        {
          text: AlertWindowConstants.OKBUTTON,
          handler: () => {
            this.routerService.goRouteLink('/chattalk');
          }
        },
        {
          text: AlertWindowConstants.CANCELBUTTON,
          role: 'cancel',
          cssClass: 'primary',
          handler: (blah) => {
          }
        }
      ]
    });
    await alert.present();
  }

  async completeButtonClick() {

    if (this.isEdit) {
      return;
    }
    this.isEdit = true;

    console.log('>>> complete button click ...');
    const conversationId = this.chatRoomStore.conversationId;
    // 削除したメンバー
    const delUserList = this.initSelectedUsetList.filter(item1 => !this.selectedUserList.some(item2 => (item2.userId === item1.userId)));
    // 追加したメンバー
    const addUserList = this.selectedUserList.filter(item1 => !this.initSelectedUsetList.some(item2 => (item2.userId === item1.userId)));

    if ((!delUserList || delUserList.length === 0) && (!addUserList || addUserList.length === 0)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00014,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              const url = '/chattalk';
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
              this.isEdit = false;
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    } else {
      this.appLoadingStoreService.isloading(true);

      const successFunc = () => {
        console.log('createConversation');
        this.appLoadingStoreService.isloading(false);

        this.queryAssocaitedUser();
        const url = '/chattalk';
        this.routerService.goRouteLink(url);
      };
      const failFunc = async (err) => {
        console.log('err', err);
        this.isEdit = false;
        this.appLoadingStoreService.isloading(false);
        if (this.appStateService.appState.isOffline) {
          // オフライン可能以外画面の場合
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        } else if (err === 'EIF018' || err === 'EIF019') {
          // エラー画面に遷移
          this.routerService.goRouteLink('/error/' + err);
        } else {
          // 登録に失敗した場合
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants[err],
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        }
      };

      ChatAPI.apiService.createAndDeleteUserProjectConversation(successFunc, failFunc, conversationId,
        addUserList, delUserList, this.projectId);
    }
  }

}
