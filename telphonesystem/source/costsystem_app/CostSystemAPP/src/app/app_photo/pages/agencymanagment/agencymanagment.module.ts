import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgencymanagmentPageRoutingModule } from './agencymanagment-routing.module';

import { AgencymanagmentPage } from './agencymanagment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgencymanagmentPageRoutingModule
  ],
  declarations: [AgencymanagmentPage]
})
export class AgencymanagmentPageModule {}
