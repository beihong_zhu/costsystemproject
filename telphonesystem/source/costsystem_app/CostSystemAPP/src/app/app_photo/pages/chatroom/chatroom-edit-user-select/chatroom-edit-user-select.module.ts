import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomEditUserSelectPage } from './chatroom-edit-user-select.page';

@NgModule({
  providers: [],
  declarations: [ChatroomEditUserSelectPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomEditUserSelectPage
      }
    ])
  ]
})
export class ChatroomEditUserSelectPageModule { }
