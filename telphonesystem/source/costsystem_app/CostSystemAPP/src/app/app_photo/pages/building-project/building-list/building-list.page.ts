import { Component, OnInit } from '@angular/core';
import { S0101Constants, AddressSort } from '@constant/constant.photo';
import { RoutingService } from '@service/app-route.service';
import { IF0101Service } from '@service/api';
import { IF0101InDto, IF0101OutDto } from '@service/dto';
import { SORTBY } from '@constant/code';
import { AppState, CommonStore, CommonStoreService, AppStateStoreService } from '@store';
import { PopoverController, Platform } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { Building } from '../dto/Buildingdto';

@Component({
  selector: 'app-building-list',
  templateUrl: './building-list.page.html',
  styleUrls: ['./building-list.page.scss', './building-list.page.mobile.scss', './building-list.page.winapp.scss']
})

export class BuildingListPage {

  isPc: boolean;
  isWinApp: boolean;

  title: string = S0101Constants.TITLE;
  // 物件一覧件数レベル
  buildingCountLabel: string = S0101Constants.NUMBEROFPROPERTIES;
  // 物件一覧単位レベル
  buildingUnitLabel: string = S0101Constants.COUNTS;
  // 物件一覧物件IDレベル
  buildingIdTitle: string = S0101Constants.BUILDINGID;
  // 物件一覧物件名レベル
  buildingNameTitle: string = S0101Constants.BUILDINGNAME;
  // 物件一覧物件住所レベル
  buildingAddressTitle: string = S0101Constants.BUILDINGADDRESS;

  appState: AppState;
  commonStore: CommonStore;
  buildingList: Building[];
  buildingCount: number;
  // 区切り記号とアドレス
  ads: string[];
  // 言語別
  lang: string;
  // 言語別住所編集順
  addressSort: string[];
  // 言語別住所編集順JSON
  addressSortJa: string[] = AddressSort.ADDRESSSORTJA;
  addressSortUs: string[] = AddressSort.ADDRESSSORTUS;

  platformtype: string;

  /**
   * 物件一覧
   */
  constructor(
    private popoverController: PopoverController,
    private routerService: RoutingService,
    private commonStoreService: CommonStoreService,
    private appStateService: AppStateStoreService,
    private if0101Service: IF0101Service,
    private platform: Platform
  ) {

    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    this.platformtype = 'buildingList-' + this.appStateService.getPlatform();

  }

  ionViewWillEnter() {
    this.buildingList = new Array();
    this.isPc = this.appState.isPC ? true : false;
    this.isWinApp = this.appState.isWinApp ? true : false;
    // 画面情報を取得
    this.queryBuildingList(this.getCreateInDto());
  }

  clickItem(building) {
    // console.log('building', building);
    this.commonStore.buildingId = building.buildingId;
    this.commonStore.buildingName = building.buildingName;
    // 画面選択した物件情報は、ストアを保存します。
    this.commonStoreService.setCommon(this.commonStore);

    const url = '/project';

    this.routerService.goRouteLink(url);
  }

  async onPress(ev: Event, building) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [building.buildingId, building.buildingName, building.buildingAddress];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  // InDtoを作成
  getCreateInDto(): IF0101InDto {
    const inDto: IF0101InDto = {};
    // ユーザID
    inDto.userId = this.commonStore.userId;
    inDto.sort = [{ sortItem: 'building_id', order: SORTBY.ASC }];

    return inDto;
  }

  /**
   * 物件情報を取得
   */
  queryBuildingList(inDto: IF0101InDto): void {

    // APIから、物件情報を取得
    this.if0101Service.postExec(inDto, (outDto: IF0101OutDto) => {
      // レスポンス
      this.if0101successed(outDto);
    },
      // fault
      () => {
      });


  }

  async if0101successed(outDto: IF0101OutDto) {

    // 物件一覧情報
    this.buildingList = new Array();

    this.addressSort = this.addressSortJa;
    // API戻す情報から、画面情報を作成
    Array.from({ length: outDto.buildings.length }).map((_, i) => {
      this.ads = new Array();
      // 区切り記号をセット
      this.ads[0] = this.addressSort[3];
      // 住所１：物件住所(番地)
      this.ads[1] = outDto.buildings[i].addressNumber;
      // 住所２：物件住所(市区町村)
      this.ads[2] = outDto.buildings[i].addressCity;
      // 住所３：物件住所(都道府県)
      this.ads[3] = outDto.buildings[i].addressPrefecture;

      this.buildingList[i] = new Building();
      // 物件ID
      this.buildingList[i].buildingId = outDto.buildings[i].buildingId;
      // 物件名
      this.buildingList[i].buildingName = outDto.buildings[i].buildingName;
      // 物件住所
      this.buildingList[i].buildingAddress
        = this.ads[this.addressSort[0]]
        + this.ads[0] + this.ads[this.addressSort[1]]
        + this.ads[0] + this.ads[this.addressSort[2]];
    });

    // 物件一覧件数
    this.buildingCount = outDto.buildings.length;

  }

}

