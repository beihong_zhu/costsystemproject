export interface ChattalkMessage {
  chatTalkFlg?: string;
  chatTalkName?: string;
  chatTalkContent?: string;
  chatTalkPictures?: string[];
  createdAt?: string;
  readUsers?: string[];
}

export interface Message {
  id?: string;
  conversationId?: string;
  content?: string;
  identityId?: string;
  s3FilePath?: string;
  createdAt?: string;
  sender?: string;
  isSent?: boolean;
  isInvalid?: boolean;
  readUsers?: string[];
  s3FullPath?: string;
}


export interface User {
  userId?: string;
  projectId?: string;
  usersInfo?: UsersInfo;
  isInvalid?: boolean;
  createdAt?: string;
  updatedAt?: string;
}

export interface UsersInfo {
  authorId?: string;
  companyName?: string;
  createdAt?: string;
  isInvalid?: boolean;
  updatedAt?: string;
  userId?: string;
  userName?: string;
}

export interface SelectUser {
  authorId?: string;
  companyName?: string;
  createdAt?: string;
  isInvalid?: boolean;
  updatedAt?: string;
  userId?: string;
  userName?: string;
  isChecked?: boolean;
  disabled?: boolean;
}

export interface ProjectConversations {
  getUserProjectConversations?: GetUserProjectConversations;
}

export interface GetUserProjectConversations {
  userProjectConversations?: UserProjectConversations[];
}

export interface UserProjectConversations {
  associated?: AssociatedUserStoreConversation[];
  conversation?: Conversation;
  conversationId?: string;
  isInvalid?: boolean;
  userId?: string;
}
export interface AssociatedUserStoreConversation {
  conversationId?: string;
  isInvalid?: boolean;
  userId?: string;
  usersInfo?: UsersInfo;
}

export interface Conversation {
  conversationId?: string;
  conversationName?: string;
  createdAt?: string;
  creatorId?: string;
  isInvalid?: boolean;
  projectId?: string;
  updatedAt?: string;
}

export interface ChatRoomUsers {
  createdAt?: string;
  isInvalid?: boolean;
  projectId?: string;
  updatedAt?: string;
  userId?: string;
  usersInfo?: UsersInfo;
}

export interface InitSelectUsers {
  authorId?: string;
  companyName?: string;
  createdAt?: string;
  isInvalid?: boolean;
  updatedAt?: string;
  userId?: string;
  userName?: string;
  nameAndComp?: string;
  isChecked?: boolean;
  disabled?: boolean;
}

export interface GetInvitableUsers {
  invitableUsers?: InvitableUser[];
}

export interface InvitableUser {
  userId?: string;
  projectId?: string;
  usersInfo?: UsersInfo;
  isInvalid?: boolean;
  createdAt?: string;
  updatedAt?: string;
}

export interface Assoicated {
  conversationAssoicated?: ConversationAssoicatedUser[];
}

export interface ConversationAssoicatedUser {
  userId?: string;
  isInvalid?: boolean;
  usersInfo?: UsersInfo;
}

export interface CreateConversationDev {
  conversationId?: string;
  conversationName?: string;
  createdAt?: string;
  updatedAt?: string;
  creatorId?: string;
}

export interface GetConversationMessages {
  allMessageConnection?: AllMessageConnection;
}

export interface AllMessageConnection {
  messages?: Message[];
}

export interface CreateMessage {
  createMessage?: Message;
}

