import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZeroaccountPage } from './zeroaccount.page';

const routes: Routes = [
  {
    path: '',
    component: ZeroaccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZeroaccountPageRoutingModule {}
