import { MachineDto } from './machine.dto';

export declare interface PinDto {
  // ピンリスト
  pinId?: number;
  // 図面ID
  drawingId?: string;
  // 機器ID
  machine?: MachineDto;
  // 機種区分
  modeltypeDivision?: string;
  // X座標
  positionX?: number;
  // Y座標
  positionY?: number;
  // 配置フラグ
  isInstalled?: number;
  // X座標%
  positionXpercent?: string;
  // Y座標%
  positionYpercent?: string;
  // 移動可能
  isMoving?: boolean;
}
