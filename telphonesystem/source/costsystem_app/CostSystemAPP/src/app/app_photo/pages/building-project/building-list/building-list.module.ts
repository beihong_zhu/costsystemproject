import { NgModule } from '@angular/core';
import { BuildingListPage } from './building-list.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [],
  declarations: [BuildingListPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: BuildingListPage
      }
    ])
  ],
  entryComponents: [

  ],
})
export class BuildingListModule { }
