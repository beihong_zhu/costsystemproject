export class WorkDto {
  projectId?: number;
  bigProcessId?: string;
  processId?: number;
  workId?: number;
  workName?: string;
  displayOrder?: number;
}
