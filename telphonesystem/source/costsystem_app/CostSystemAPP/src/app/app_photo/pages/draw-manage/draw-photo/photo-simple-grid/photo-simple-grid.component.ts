import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController, AlertController, Platform } from '@ionic/angular';
import {
  AppStateStoreService, CommonStoreService, Photomanagestore, PhotomanagestoreService,
  CameraStoreService, InitInterfaceType, AppLoadingStoreService
} from '@store';
import { RoutingService } from '@service/app-route.service';
import { PhotoListPopoverComponent } from '@pages/photo-manage/photo-list-popover/photo-list-popover.component';
import { LookType, LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';
import { ListConstants } from '@constant/code';
import { DbPhotoService } from '@service/db/db-photoservice';
import { API0501InDto, API0501OutDtoHeader, API0501OutDto, PhotoList0501 } from '@service/dto';
import { API0501Service } from '@service/api';
import { RunExecutionService } from '@service/api/run-execution.service';
import { ResultType } from '../../../../../app_common/constant/common';
import { Photosort } from '../../../photo-manage/photo-list/photo-sort';
import { PhotoListComponentHelp } from '../../../photo-manage/photo-list/photo-list.component-help';
import { S0601Constants } from '../../../../constant/constant.photo';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-photo-simple-grid',
  templateUrl: './photo-simple-grid.component.html',
  styleUrls: [
    './photo-simple-grid.component.scss',
    './photo-simple-grid.component.mobile.scss',
    './photo-simple-grid.component.winapp.scss'
  ],
})
export class PhotoSimpleGridComponent implements OnInit {

  public photolist = S0601Constants.PHOTOLIST;
  public photograph = S0601Constants.PHOTOGRAPH;

  platformtype: string;
  title: string;
  isPc: boolean;
  bigProcessId: string;
  bigProcessselectedText: string;
  bigProcesses: LookType[];
  photoList: LocalPhoto[];
  needPhotoList: PhotoList0501[];

  datasource: [];

  // private dbPhotoservice: DbPhotoService;

  floorId: number; // フロアID
  processId: number; // 工程ID
  workId: number; // 作業ID
  machineId: number; // 機器ID
  roomId: number; // 部屋ID
  placeId: number; // 場所ID
  photomanageState: Photomanagestore;

  constructor(
    private popoverController: PopoverController,
    private appStateService: AppStateStoreService,
    private params: NavParams,
    private routingService: RoutingService,
    private commonStoreService: CommonStoreService,
    private api0501Serve: API0501Service,
    private photosort: Photosort,
    private photoListComponentHelp: PhotoListComponentHelp,
    private storeservice: PhotomanagestoreService,
    public cameraStoreService: CameraStoreService,
    private dbPhotoservice: DbPhotoService,
    private appLoadingStoreService: AppLoadingStoreService,
    public alertController: AlertController,
    public platform: Platform,
    protected httpClient: HttpClient,
    private runExecutionService: RunExecutionService<API0501InDto>
  ) {
    this.machineId = params.data.machineId;
    this.title = params.data.title;
    this.processId = params.data.processId;
    this.floorId = params.data.floorId;
    this.roomId = params.data.roomId;
    this.placeId = params.data.placeId;
    this.platformtype = 'photo-simple-grid-' + this.appStateService.getPlatform();
  }

  ngOnInit() {
    this.photomanageState = this.storeservice.photomanageState;
    this.isPc = this.appStateService.appState.isPC;

    if (this.isPc) {

      this.bigProcesses = ListConstants.CD0001_LIST;
      const bigP = new LookType();
      bigP.id = this.bigProcesses[0].id;
      bigP.value = this.bigProcesses[0].value;

      this.bigProcessId = bigP.id;
      this.bigProcessselectedText = '大工程：' + bigP.value;

      this.loadPhotos();

    } else {


      const bigP = new LookType();
      bigP.id = this.commonStoreService.commonStore.bigProcessId;
      bigP.value = this.commonStoreService.commonStore.bigProcessName;

      this.bigProcessId = bigP.id;
      this.bigProcessselectedText = '大工程：' + bigP.value;
      this.mobileGetPhotoList();
    }
  }

  async onPress(ev: Event, title) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    if (this.isPc) {
      return;
    }
    const messageArr1 = [title];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


  dismiss = () => {
    this.params.data.modal.dismiss();
  }

  gotoPhotoList(e) {
    e.stopPropagation();
    this.params.data.modal.dismiss();
    this.routingService.goRouteLink('/photo-list');
  }

  goCamera(e) {
    e.stopPropagation();
    this.params.data.modal.dismiss();
    this.cameraStoreService.setFromInitInterfaceType(InitInterfaceType.FromDrawing);
    this.cameraStoreService.setPhotoSimpleGridAction({
      machineId: this.machineId,
      floorId: this.floorId,
      roomId: this.roomId,
      placeId: this.placeId
    });
    this.routingService.goRouteLink('/camera');
  }

  selectBigprocess(event) {
    this.bigProcessId = event.detail.value;
    this.bigProcesses.forEach((bigProcess) => {
      if (bigProcess.id === this.bigProcessId) {
        this.bigProcessselectedText = '大工程：' + bigProcess.value;
        return;
      }
    });
    this.loadPhotos();
  }





  // 写真一覧提供API(API-05-01)
  getIndto(): API0501InDto {
    const api0501InDto: API0501InDto = {};
    api0501InDto.buildingId = this.commonStoreService.commonStore.buildingId;
    api0501InDto.projectId = this.commonStoreService.commonStore.projectId;
    api0501InDto.bigProcessId = this.bigProcessId;
    if (this.floorId != null) {
      api0501InDto.floorId = this.floorId;
    }
    if (this.processId != null) {
      api0501InDto.processId = this.processId;
    }
    if (this.workId != null) {
      api0501InDto.workId = this.workId;
    }
    if (this.machineId != null) {
      api0501InDto.machineId = this.machineId;
    }

    return api0501InDto;
  }

  // -----------------------------------------
  // download s3 json
  private async apiSuccessed(res: HttpResponse<Blob>, resHeader0501: API0501OutDtoHeader) {
    if (resHeader0501.resultCode === ResultType.NORMAL) {
      const fileReadResult = await this.runExecutionService.apiSuccessFileRead(res);
      if (fileReadResult) {
        this.api0501Success(fileReadResult);
      } else {
        this.appLoadingStoreService.isloading(false);
      }
    }
    return;
  }

  // 写真一覧提供API(API-05-01)
  async loadPhotos() {
    this.appLoadingStoreService.isloading(true);
    const executionAPI = await this.runExecutionService.runExecution('/API0501', this.getIndto(), 1000, false, false, true);
    if (executionAPI) {
      this.downloadJSON(JSON.parse(executionAPI));
    }
    this.appLoadingStoreService.isloading(false);
  }

  // 写真一覧提供API(API-05-01)
  async api0501Success(outDto: API0501OutDto) {
    this.photoList = outDto.photoList;
    this.needPhotoList = outDto.needPhotoList;

    console.log('-------photoList---------------needPhotoList------------', this.photoList.length, this.needPhotoList.length);

    this.showPhotos();
  }

  mobileGetPhotoList() {
    const photoList = this.photomanageState.photos;
    const needPhotoList = this.photomanageState.needPhotoList;
    this.photoList = photoList.filter((photo) => photo.machineId === this.machineId);
    this.needPhotoList = needPhotoList.filter((photo) => photo.machineId === this.machineId);
    this.showPhotos();
  }


  showPhotos() {
    let allPhotos = this.photoListComponentHelp.combineNeedPhotoList(
      this.commonStoreService.commonStore.bigProcessId, JSON.parse(JSON.stringify(this.needPhotoList)), this.photoList);

    allPhotos = this.photosort.simplePhoto_timingOrderSort(allPhotos);
    console.log('-------------simpleAllphotos----------------------', allPhotos);

    this.datasource = this.photoListComponentHelp.simplePhoto(allPhotos);
  }

  // 写真一覧URLでダウンロード
  private downloadJSON(resHeader0501: API0501OutDtoHeader) {
    this.httpClient.get(resHeader0501.photoAllListUrl, {
      observe: 'response',
      responseType: 'blob'
    }).subscribe(
      async (res) => {
        this.apiSuccessed(res, resHeader0501);
      },
      (err) => {
        console.error('Error: ' + err);
        this.appLoadingStoreService.isloading(false);
      }
    );
  }
}
