import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { AlertController, ModalController, PopoverController, Platform } from '@ionic/angular';
import { PhotoFilterSetComponent } from '../photo-filter-set/photo-filter-set.component';
import { PhotoSortSetComponent } from '../photo-sort-set/photo-sort-set.component';
import { PhotoViewSetComponent } from '../photo-view-set/photo-view-set.component';
import { LookType, LocalPhoto, SelectTitle } from '../dto/photo-manage-dto';
import {
  API0901Service, API0802Service, API0801Service, IF0102Service, IF0106Service,
  API0505Service, API0504Service, API0303Service, API0301Service
} from '@service/api';
import { RunExecutionService } from '@service/api/run-execution.service';
import {
  API0801InDto, API0801OutDto, IF0102InDto, IF0102OutDto,
  API0505InDto, API0505OutDto, IF0106InDto, IF0106OutDto,
  API0504OutDto, API0504InDto, API0501InDto
} from '@service/dto';
import { API0501OutDto } from '@service/dto/api-05-01-out-dto';
import { Photosort } from './photo-sort';
import { PhotoListComponentHelp } from '../photo-list/photo-list.component-help';
import {
  CommonStore, PhotomanagestoreService, AppStateStoreService, CommonStoreService,
  Photomanagestore, PhotoDetailStoreService, HeadStateService, OfflineStoreService,
  PhotoDownloadSuccessStore, AutoUploadPhotoStore, OfflineStore, OfflineUnitsStore, CameraStoreService,
  AppLoadingStoreService
} from '@store';
import { PhotoListPopoverComponent } from '../photo-list-popover/photo-list-popover.component';
import { Store, select } from '@ngrx/store';
import { RoutingService } from '@service/app-route.service';
import { API0501Service } from '@service/api/api-05-01.service';
import { DeletePhotoList } from '@service/dto/api-05-04-in-dto';
import { Photos0505 } from '@service/dto/api-05-05-in-dto';
import {
  ListConstants, CD0009Constants, CD0007Constants, CD0008Constants, CDFILMEDConstants,
  CD0024Constants, CD0006Constants, CD0005Constants, TEXT, CD0015Constants, CD0001Constants
} from '@constant/code';
import { DbPhotoService } from '@service/db/db-photoservice';
import { ResultType } from '../../../../app_common/constant/common';
import { MessageConstants } from '@constant/message-constants';
import { PhotoListConf } from './photo-list-conf';
import { SORTBY } from '@constant/code';
import { S0501Constants, AlertWindowConstants } from '../../../constant/constant.photo';
import { Machine } from '@service/dto/if-01-06-out-dto';
import { TextSinglePopoverComponent } from '@pages/camera/text-popover/text-popover.component';
import { OfflineAPIService } from '@service/offline/offline-api.service';
import { PhotoConf } from '@conf/photo-conf';
import { API0501OutDtoHeader } from '@service/dto/api-05-01-out-dto-header';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Unit } from '../../../../app_photo/store/offline-units/offline-units.store';
import { SpaceString } from '../../../../app_photo/util/space-string';

@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.page.html',
  styleUrls: ['./photo-list.page.scss', './photo-list.mobile.scss', './photo-list.winapp.scss'],
})
export class PhotoListComponent implements OnInit, OnDestroy {

  public cancleTitle = S0501Constants.CANCLE;
  public displayMethod = S0501Constants.DISPLAYMETHOD;
  public photodelete = S0501Constants.PHOTODELETE;
  // public multiselect = S0501Constants.MULTISELECT;
  public photobookselect = S0501Constants.PHOTOBOOKSELECT;
  public update = S0501Constants.UPDATE;
  public serialNumberPhotographTitle = S0501Constants.SERIALNUMBERPHOTOGRAPH;
  public message = '';
  public decision = '';

  isPc: boolean;
  isIos: boolean;
  isShowCheckBox: boolean;
  showType: string; // 表示形式1grid 2list
  sortby: string; // 1工程2機器  3日付  4フロア
  sortdes: string; // 1昇順  2降順
  filmeds: string[]; // 1撮影済み 2未撮影
  statu: string[]; // 1前 2中 3後

  multitypeselect: string; // 1pc-ダウンロード,mobile-アップロード 2削除 3写真帳選択
  multiSelectTitle: string;
  displaytype: LookType[];

  bigProcess: LookType; // 大工程選択
  bigProcesses: LookType[];

  // api-in
  buildingId: number; // 物件ID
  projectId: number; // 案件ID
  bigProcessId: string; // 大工程ID
  floorId: number; // フロアID
  processId: number; // 工程ID
  workId: number; // 作業ID
  machineId: number; // 機器ID
  photoDate: string; // 撮影日
  canDelete: boolean;
  // api-out
  photos: LocalPhoto[];
  needPhotoList: any[];
  photoKeyvalue: any = {};
  filterdAllPhotos: LocalPhoto[];

  unprocessphotos: LocalPhoto[];
  unmachinephotos: LocalPhoto[];
  unfloorphotos: LocalPhoto[];

  // 写真data
  datas: any;

  selectPhotos: LocalPhoto[];
  deletePhotos: LocalPhoto[];


  machine: Machine;
  machineList: Machine[];
  offlineUnits: Unit[];

  photomanageState: Photomanagestore;
  downloadphotoStoreSubscription: Subscription;
  uploadphotoStoreSubscription: Subscription;
  offlineStore: OfflineStore;
  offlineStoreSubscription: Subscription;
  offlineUnitsStoreSubscription: Subscription;
  // private dbPhotoservice: DbPhotoService;
  commonStore: CommonStore;

  // API Download flg
  API0801Download: boolean;
  IF0102Download: boolean;
  IF0106Download: boolean;

  // titles
  floorSortTitles: SelectTitle[] = [];
  processSortTitles: SelectTitle[] = [];
  machineSortTitles: SelectTitle[] = [];
  titles: SelectTitle[] = [];
  title: SelectTitle;

  // grid　或いは　list　データソース
  datasource: any = [];
  isPhotoChange: boolean = false;

  // code
  cd0009Constants = CD0009Constants;
  cd0007Constants = CD0007Constants;
  cD0005Constants = CD0005Constants;

  widthChange: () => void;

  platformtype: string;

  constructor(
    private popoverController: PopoverController,
    public modalController: ModalController,
    public alertController: AlertController,
    private api0301Serve: API0301Service,
    private api0303Serve: API0303Service,
    private api0501Serve: API0501Service,
    private api0504Serve: API0504Service,
    private api0505Serve: API0505Service,
    private api0901Serve: API0901Service,
    private api0802Serve: API0802Service,
    private dbPhotoservice: DbPhotoService,
    private headStateService: HeadStateService,
    private offlineStoreService: OfflineStoreService,
    private offlineAPIService: OfflineAPIService,
    private cameraStoreService: CameraStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    private runExecutionService: RunExecutionService<API0501InDto>,

    private routerService: RoutingService,
    private api0801Serve: API0801Service,
    private if0106Serve: IF0106Service,
    private if0102Serve: IF0102Service,
    private photomanagestoreService: PhotomanagestoreService,
    private photoDetailStoreService: PhotoDetailStoreService,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    public routingService: RoutingService,
    private photosort: Photosort,
    private photolistComponentHelp: PhotoListComponentHelp,
    private storeSubscribe: Store<{ photomanageState: Photomanagestore }>,
    private downloadphotoStoreSubscribe: Store<{ photoDownloadSuccessStore: PhotoDownloadSuccessStore }>,
    private uploadphotoStoreSubscribe: Store<{ autoUploadPhotoStore: AutoUploadPhotoStore }>,
    private storeOffline: Store<{ offlineStore: OfflineStore }>,
    private offlineUnitsStoreSubscribe: Store<{ offlineUnitsStore: OfflineUnitsStore }>,
    public platform: Platform,
    protected httpClient: HttpClient
  ) {

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
    this.platformtype = 'photolist-' + this.appStateService.getPlatform();
    this.initIsIos();
    this.setEditFunction();
  }

  async initIsIos() {
    await this.platform.ready();
    this.isIos = this.platform.is('ios');
  }

  private setEditFunction() {

    const editFunction = async () => {
      if (!this.isShowCheckBox) {

        if (this.appStateService.appState.isOffline) {
          // オフライン状態のため、実行できません。オンライン状態になってから、もう一度実行してください。
          this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00094);
          return;
        }



        if (this.isPc) {
          this.API0801Download = false;
          this.IF0102Download = false;
          this.IF0106Download = false;

          this.queryAPI0801(this.getAPI0801InDto()); // 工程・作業情報一覧 API-08-01
          this.queryIF0102(this.getIF0102InDto()); // 物件情報提供 IF-01-02
          this.queryIF0106(this.getIF0106InDto()); // 機器情報 IF-01-06
        } else {
          this.offlineAPIService.queryOfflineData();
        }
      }

    };

    this.headStateService.setEditFunction(editFunction);

  }

  // ==================================
  // 初期化
  // ==================================
  ngOnInit() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.selectPhotos = [];
    this.deletePhotos = [];
    this.isShowCheckBox = false;
    this.multitypeselect = PhotoListConf.NO_SELECT;
    this.multiSelectTitle = PhotoListConf.NO_SELECT_VALUE;
    this.stateinit();

    this.buildingId = this.commonStoreService.commonStore.buildingId;
    this.projectId = this.commonStoreService.commonStore.projectId;
    // 「削除」は協力店　職長(主)・作業者(副)のユーザは非表示
    this.canDelete = CD0015Constants.AUTHORITY04 !== this.commonStore.authorId;

    if (this.isPc) {

      this.bigProcesses = ListConstants.CD0001_LIST;
      this.bigProcess = this.bigProcesses[0];
      this.bigProcessId = this.bigProcesses[0].id;

      this.changeCommstore();

    } else {

      const bigP = new LookType();
      bigP.id = this.commonStoreService.commonStore.bigProcessId;
      bigP.value = this.commonStoreService.commonStore.bigProcessName;
      this.bigProcess = bigP;
      this.bigProcessId = this.commonStoreService.commonStore.bigProcessId;
    }


    this.displaytype = ListConstants.CD0006_LIST;


    // pc端 先工程・作業情報一覧， 再loadPhotos
    if (this.isPc) {

      this.queryAPI0801(this.getAPI0801InDto()); // 工程・作業情報一覧 API-08-01
      this.queryIF0102(this.getIF0102InDto()); // 物件情報提供 IF-01-02
      this.queryIF0106(this.getIF0106InDto()); // 機器情報 IF-01-06

      this.subscribeWindownWidthChange();
    } else {
      this.subscribeDownloadPhotoManage();
      this.subscribeUploadPhotoManage();
      this.subscribeOfflineData();
      this.subscribeOfflineUnitsStore();
    }

  }

  subscribeWindownWidthChange() {

    this.widthChange = async () => {
      if (this.sortby === CD0007Constants.DATE) {
        const key = JSON.stringify(this.title);
        this.datasource = this.photolistComponentHelp.groupBydateCount(
          this.datas.get(key) || [], this.photolistComponentHelp.pcWidth_count());

      } else {
        this.datasource = [...this.datasource];
      }

    };
    window.addEventListener('resize', this.widthChange);

  }



  ionViewWillEnter() {
    if (!this.isPc) {
      this.combinePhotoLogic();
    }
  }


  stateinit() {
    this.datasource = [];
    this.showType = CD0009Constants.GRID_SHOW;
    this.sortby = CD0007Constants.PROCESS;
    this.sortdes = CD0008Constants.ASC;
    this.filmeds = [CDFILMEDConstants.FILMED, CDFILMEDConstants.UNFILMED];
    this.statu = [CD0024Constants.BEFORE_CONSTRUCTION, CD0024Constants.IN_CONSTRUCTION, CD0024Constants.AFTER_CONSTRUCTION];


  }

  ionViewWillLeave() {
    // this.ngOnDestroy();
  }

  ngOnDestroy() {

    if (!this.isPc) {
      this.downloadphotoStoreSubscription.unsubscribe();
      this.uploadphotoStoreSubscription.unsubscribe();
      this.offlineStoreSubscription.unsubscribe();
      this.offlineUnitsStoreSubscription.unsubscribe();
    } else {
      window.removeEventListener('resize', this.widthChange);
    }
  }
  //  ソートの配列の初期化
  constructSortTitles(photomanageState) {

    if (this.commonStore.processList && this.commonStore.processList.length > 0) {

      this.processSortTitles = [];
      const processList = this.commonStore.processList.sort(this.compare('displayOrder', this.sortdes));
      processList.map((process) => {
        if (process && process.workList) {
          process.workList.sort(this.compare('displayOrder', this.sortdes)).map((work) => {
            const selectTitle: SelectTitle = {};
            selectTitle.title = process.processName + ' - ' + work.workName;
            selectTitle.processId = process.processId;
            selectTitle.id = work.workId;
            this.processSortTitles.push(selectTitle);
          });
        }
      });

      this.unprocessphotos = this.photos.filter((photo) => {
        return !photo.processId || !photo.processName || !photo.workId || !photo.workName;
      });

      if (this.unprocessphotos.length > 0) {
        const selectTitle: SelectTitle = {};
        selectTitle.title = '工程・作業名(不明)';
        selectTitle.id = 9999;
        selectTitle.processId = 9999;
        this.processSortTitles.push(selectTitle);
      }

      if ((this.title === undefined || this.title === null) && this.sortby === CD0007Constants.PROCESS) {
        this.title = (this.processSortTitles.length > 0 && this.processSortTitles[0]) || null;
      }

    }

    if (this.commonStore.machineList && this.commonStore.machineList.length > 0) {

      this.machineSortTitles = [];
      const machineList = this.commonStore.machineList.sort(this.compare('unitMark', this.sortdes));
      this.machineList = machineList;
      if (this.machine === undefined || this.machine === null) {
        this.machine = machineList[0];
      }

      machineList.map((machine) => {
        const selectTitle: SelectTitle = {};
        selectTitle.title = machine.unitMark + '(' + machine.machineModel + ')';
        selectTitle.id = machine.machineId;
        this.machineSortTitles.push(selectTitle);
        if (this.machine.machineId === machine.machineId && this.sortby === CD0007Constants.MACHINE) {
          this.title = selectTitle;
        }
      });


      this.unmachinephotos = this.photos.filter((photo) => {
        return !photo.machineId || !photo.modeltype || !photo.unitMark;
      });

      if (this.unmachinephotos.length > 0) {
        const selectTitle: SelectTitle = {};
        selectTitle.title = '機種(不明)';
        selectTitle.id = 9999;
        this.machineSortTitles.push(selectTitle);
      }

      if ((this.title === undefined || this.title === null) && this.sortby === CD0007Constants.MACHINE) {
        this.title = (this.machineSortTitles.length > 0 && this.machineSortTitles[0]) || null;
      }

    }

    if (this.commonStore.floorsList && this.commonStore.floorsList.length > 0) {

      this.floorSortTitles = [];
      const floorsList = this.commonStore.floorsList.sort(this.compare('displayOrder', this.sortdes));
      floorsList.map((floor) => {
        const selectTitle: SelectTitle = {};
        selectTitle.title = floor.floorName;
        selectTitle.id = floor.floorId;
        this.floorSortTitles.push(selectTitle);
      });


      this.unfloorphotos = this.photos.filter((photo) => {
        return !photo.floorId || !photo.floorName;
      });

      if (this.unfloorphotos.length > 0) {
        const selectTitle: SelectTitle = {};
        selectTitle.title = 'フロア(不明)';
        selectTitle.id = 9999;
        this.floorSortTitles.push(selectTitle);
      }


      if ((this.title === undefined || this.title === null) && this.sortby === CD0007Constants.FLOOR) {
        this.title = (this.floorSortTitles.length > 0 && this.floorSortTitles[0]) || null;
      }

    }

  }

  // needPhotoList takePhotoList結合，photosAll返す
  combineNeedPhotoList(needPhotoList, takePhotoList) {

    // noImageList 写真撮るべきまだ採ってない写真リスト
    const noImageList = needPhotoList.filter((neePhoto) => {

      // 写真撮るべきまだ採ってない写真 flg
      let needAndHasTakeFlg = false;
      needAndHasTakeFlg = takePhotoList.some((takePhoto) => {
        if (takePhoto.floorId === neePhoto.floorId &&
          takePhoto.roomId === neePhoto.roomId &&
          takePhoto.placeId === neePhoto.placeId &&
          takePhoto.machineId === neePhoto.machineId &&
          takePhoto.workId === neePhoto.workId &&
          takePhoto.processId === neePhoto.processId &&
          takePhoto.timingId === neePhoto.timingId) {
          return true;
        }

      });

      // 写真撮るべきまだ採ってない写真 flg === false
      if (!needAndHasTakeFlg) {
        // 写真撮るべきまだ採ってない写真
        return neePhoto;
      }
    });
    const photosAll = this.bigProcessId === CD0001Constants.CD0001_04
      ? takePhotoList.concat(needPhotoList) : takePhotoList.concat(noImageList);
    return photosAll;

  }

  subscribeOfflineData() {

    // offline data request完了しているか
    this.offlineStoreSubscription = this.storeOffline.pipe(select('offlineStore')).subscribe((subscribestore => {
      this.offlineStore = subscribestore;
      if (this.offlineStore.APIAllDownload && this.routerService.getUrl() === '/photo-list') {
        this.offlineStoreService.setAPIAllDownload(false);
        // pulldownの初期化
        this.title = null;
        this.machine = null;
        this.combinePhotoLogic();
      }

    }));

  }

  // 監視offlineUnitsStore
  subscribeOfflineUnitsStore() {

    this.offlineUnitsStoreSubscription = this.offlineUnitsStoreSubscribe.pipe(select('offlineUnitsStore')).subscribe((state => {
      this.offlineUnits = state.units;
      this.isPhotoChange = !this.isPhotoChange;
    }));

  }

  // 監視DownloadPhoto変化
  subscribeDownloadPhotoManage() {

    this.downloadphotoStoreSubscription = this.downloadphotoStoreSubscribe.pipe(select('photoDownloadSuccessStore')).subscribe((state => {

      const newP = state.photoDownloadSuccess;


      if (newP && JSON.stringify(this.photoKeyvalue) !== JSON.stringify({}) && JSON.stringify(newP) !== JSON.stringify({})) {
        const key = newP.photoTerminalNo + newP.photoDate;
        if (this.photoKeyvalue[key] && newP.photoPathLocal) {
          this.photoKeyvalue[key].photoPathLocal = newP.photoPathLocal;
          this.isPhotoChange = !this.isPhotoChange;

        }
      }


    }));

  }

  // 監視UploadPhoto変化
  subscribeUploadPhotoManage() {

    this.uploadphotoStoreSubscription = this.uploadphotoStoreSubscribe.pipe(select('autoUploadPhotoStore')).subscribe((state => {

      const newP = state.autoUploadPhoto;

      if (newP && JSON.stringify(this.photoKeyvalue) !== JSON.stringify({}) && JSON.stringify(newP) !== JSON.stringify({})) {

        const key = newP.photoTerminalNo + newP.photoDate;
        if (this.photoKeyvalue[key]) {
          this.photoKeyvalue[key].localPhotoFlag = newP.localPhotoFlag;
          this.photoKeyvalue[key].retryCount = newP.retryCount;
          this.isPhotoChange = !this.isPhotoChange;
        }
      }

    }));

  }

  // 画面初期化するとき、写真の結合処理
  combinePhotoLogic() {




    this.photomanageState = this.photomanagestoreService.photomanageState;
    this.photos = this.photomanageState.photos;
    // needPhotoList 全て採るべき写真
    this.needPhotoList = this.photomanageState.needPhotoList;

    this.setPhotoKey();
    this.constructSortTitles(this.photomanageState);
    this.filterSelectTitlePhotos();
  }

  // 写真ごとのkeyを設定する
  setPhotoKey() {
    this.photoKeyvalue = {};
    for (const photo of this.photos) {
      const key = photo.photoTerminalNo + photo.photoDate;
      this.photoKeyvalue[key] = photo;
    }
  }

  // ==================================
  // 大工程選択
  // ==================================
  bigprocessSelect(event): void {
    event.stopPropagation();

    if (!this.isShowCheckBox) {
      this.showBigProcesspopover(event);

    }

  }

  // sortとdisplayのpopover
  async showBigProcesspopover(ev: Event) {

    const modal = await this.popoverController.create({
      component: PhotoListPopoverComponent,
      componentProps: {
        datas: this.bigProcesses,
      },
      event: ev,
      showBackdrop: true,
      translucent: true


    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data != null) {

      this.bigProcess = data;
      this.bigProcessId = data.id;
      this.changeCommstore();
      this.stateinit();

      this.IF0106Download = true;
      this.IF0102Download = true;
      this.queryAPI0801(this.getAPI0801InDto()); // 工程・作業情報一覧 API-08-01


    }
  }

  // ==================================
  // 写真を更新
  // ==================================
  photoRefresh(): void {
    if (!this.isShowCheckBox) {

      if (this.appStateService.appState.isOffline) {
        // オフライン状態のため、実行できません。オンライン状態になってから、もう一度実行してください。
        this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00094);
        return;
      }



      if (this.isPc) {
        this.API0801Download = false;
        this.IF0102Download = false;
        this.IF0106Download = false;

        this.queryAPI0801(this.getAPI0801InDto()); // 工程・作業情報一覧 API-08-01
        this.queryIF0102(this.getIF0102InDto()); // 物件情報提供 IF-01-02
        this.queryIF0106(this.getIF0106InDto()); // 機器情報 IF-01-06
      } else {
        this.offlineAPIService.queryOfflineData();
      }
    }

  }

  // ==================================
  // 写真帳選択
  // ==================================
  photoBookSelect(): void {
    if (!this.isShowCheckBox) {

      this.isShowCheckBox = true;
      this.multitypeselect = CD0005Constants.PHOTO_BOOK;
      this.selectPhotos = this.photos.filter((ph) => ph.isOutputPhotoBook === PhotoListConf.OUT_PUT_PHOTO_BOOK) || [];
      this.message = '写真帳出力対象を選択';
      this.decision = '登録';
    }
  }

  // ==================================
  // 機番撮影
  // ==================================
  serialNumberPhotograph(event) {
    event.stopPropagation();

    if (this.isPc) {
      return;
    }


    sessionStorage.setItem('buildingName', this.commonStore.buildingName);
    sessionStorage.setItem('projectName', this.commonStore.projectName);
    sessionStorage.setItem('machineId', this.machine.machineId.toString());
    sessionStorage.setItem('machineModel', this.machine.machineModel);
    sessionStorage.setItem('unitMark', this.machine.unitMark);

    sessionStorage.setItem('floorName', this.machine.floorName);
    sessionStorage.setItem('roomName', this.machine.roomName);
    sessionStorage.setItem('placeName', this.machine.placeName);

    this.headStateService.setBackRoute('/photo-list');

    this.routingService.goRouteLink('/barcode-scan');
  }

  // ==================================
  // 選択削除
  // ==================================
  deletephotoclick(ev) {
    if (!this.isShowCheckBox) {
      this.isShowCheckBox = true;
      this.multitypeselect = CD0005Constants.DELETE;
      this.message = '削除する写真を選択';
      this.decision = '削除';
    }

  }
  // ==================================
  // 表示方法
  // ==================================
  changeShowType(event): void {
    event.stopPropagation();
    if (!this.isShowCheckBox) {
      this.showDisplaypopover(event);
    }

  }

  async showDisplaypopover(ev: Event) {

    const modal = await this.popoverController.create({
      component: PhotoListPopoverComponent,
      componentProps: {
        datas: this.displaytype,
      },
      event: ev,
      showBackdrop: true,
      translucent: true,
      cssClass: 'displaypop'


    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data != null) {
      // this.cancle();
      const item = data;
      if (item.id === CD0006Constants.FLITER) {
        this.showFilterDialog();
      } else if (item.id === CD0006Constants.SORT) {
        this.showSortDialog();
      } else {
        // 表示形式
        this.showDisplayDialog();
      }


    }
  }

  // ==================================
  //  写真一覧_フィルタ設定
  // ==================================
  async showFilterDialog() {
    const modal = await this.modalController.create({
      component: PhotoFilterSetComponent,
      componentProps: {
        filmeds: this.filmeds,
        status: this.statu,
        bigProcessId: this.bigProcessId
      },
      cssClass: 'glanceModal-' + this.appStateService.getPlatform()
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data != null) {
      // 撮影ステータス
      this.filmeds = data.filmed;
      // 状況
      this.statu = data.status;

      this.filterSelectTitlePhotos();
    }
  }

  // ==================================
  //  写真一覧_ソート設定
  // ==================================
  async showSortDialog() {
    const modal = await this.modalController.create({
      component: PhotoSortSetComponent,
      componentProps: {
        sortby: this.sortby,
        sortdes: this.sortdes,
      },
      cssClass: 'glanceModal-' + this.appStateService.getPlatform()
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data != null) {

      this.datasource = [];

      // ソート対象
      this.sortby = data.sortby;
      if (this.sortby === CD0007Constants.PROCESS) {
        this.title = (this.processSortTitles.length > 0 && this.processSortTitles[0]) || null;
      } else if (this.sortby === CD0007Constants.MACHINE) {
        this.title = (this.machineSortTitles.length > 0 && this.machineSortTitles[0] || null);
      } else if (this.sortby === CD0007Constants.FLOOR) {
        this.title = (this.floorSortTitles.length > 0 && this.floorSortTitles[0] || null);
      } else {
        this.title = null;
      }

      // 昇順、降順
      if (this.sortdes !== data.sortdes) {
        this.title = null;
        // 機器情報を初期化
        this.machine = null;
      }
      if (this.sortby !== CD0007Constants.MACHINE) {
        // 機器ソート以外の場合、機器情報を初期化
        this.machine = null;
      }
      this.sortdes = data.sortdes;
      this.constructSortTitles(this.photomanageState);
      this.filterSelectTitlePhotos();
    }
  }

  // ==================================
  //  写真一覧_表示設定
  // ==================================
  async showDisplayDialog() {
    const modal = await this.modalController.create({
      component: PhotoViewSetComponent,
      componentProps: {
        formart: this.showType,
      },
      cssClass: 'glanceModal-' + this.appStateService.getPlatform()
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data != null) {
      // 表示形式設定
      this.showType = data;

      this.filterSelectTitlePhotos();
    }
  }


  // ==================================
  // 選択ソートのtitle
  // ==================================
  selectTitle(e): void {
    if (e) {
      this.title = e.detail.value;
    }
    this.filterSelectTitlePhotos();


    if (this.sortby === CD0007Constants.MACHINE) {
      const machineList = this.machineList && this.machineList.filter((m) => m.machineId === this.title.id);
      this.machine = machineList.length > 0 && machineList[0];


    }

  }

  compareWithId(o1: SelectTitle, o2: SelectTitle) {
    if (o1 && o2) {
      return o1.id === o2.id;
    }
    return o1 === o2;
  }




  async onPress(ev: Event, message) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    const popover = await this.popoverController.create({
      component: TextSinglePopoverComponent,
      componentProps: { message },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  // ==================================
  // 决定
  // ==================================
  commit() {

    const selectp = this.selectPhotos;

    if (this.appStateService.appState.isOffline) {
      // オフライン状態のため、実行できません。オンライン状態になってから、もう一度実行してください。
      this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00094);
      return;
    }

    // 写真帳選択
    if (this.multitypeselect === CD0005Constants.PHOTO_BOOK) {
      this.photoBookSelectCommit();
    } else if (this.multitypeselect === CD0005Constants.DELETE) {// 写真削除

      if (selectp.length === 0) {
        // 写真が選択されていませんので、写真を選択後、もう一度実行してください。
        this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00095);
        return;
      }

      // 端末の写真
      const locP = this.selectPhotos.filter((photo) => {
        return photo.localPhotoFlag === PhotoListConf.LOCAL_POTO;
      });

      if (locP.length > 0 && this.commonStore.autoUpload) {
        // 同期予定の写真が含まれるため、削除できません。
        this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00092);
        return;
      }

      this.showAlert(AlertWindowConstants.CONFIRMTITLE, MessageConstants.C00002, [
        {
          text: TEXT.YES,
          handler: () => {
            this.deletePhotoCommit();
          }
        },
        {
          text: TEXT.NO,
          handler: () => {
          }
        }
      ]);
    }

  }

  // ==================================
  // 写真帳出力対象選択決定
  // ==================================
  photoBookSelectCommit() {

    const changePhoto = this.calculationBookPhotos();

    // 写真帳対象（更新予定）
    const photos: any[] = changePhoto[0];
    // 更新sqliteの写真帳
    const bookP: any[] = changePhoto[1];


    if (photos.length === 0) {
      // 写真帳の対象に変更がありませんでした。写真を１つ以上変更後、もう一度実行してください。
      this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00096);
      return;
    }

    this.api0505Exec(photos, bookP);
  }

  // ==================================
  // 写真削除決定
  // ==================================
  deletePhotoCommit() {

    const selectp = this.deletePhotos;


    // serverの写真
    const netP = selectp.filter((photo) => {
      return photo.localPhotoFlag === PhotoListConf.SERVER_POTO;
    });
    // 端末の写真
    const locP = selectp.filter((photo) => {
      return photo.localPhotoFlag === PhotoListConf.LOCAL_POTO;
    });


    if (netP.length > 0) {
      this.api0504Exec(netP, () => {
        this.deleteCameraThumbnail(netP);
        if (locP.length > 0) {
          this.deleteLocalPhotos(locP, false);
        } else {
          this.setPhtotToStore();
          this.clearSelectPhotos();
        }
      });


    } else {
      if (locP.length > 0) {
        this.deleteLocalPhotos(locP, true);
      }
    }

  }

  deleteLocalPhotos(locP, show) {
    if (locP.length) {
      for (const ph of locP) {
        const index = this.photos.indexOf(ph);
        if (index > -1) {
          this.photos.splice(index, 1);
        }
      }

      if (show) {
        this.showAlert(AlertWindowConstants.TIPSTITLE, MessageConstants.I00004);
      }

      this.multitypeselect = PhotoListConf.NO_SELECT;
      this.multiSelectTitle = PhotoListConf.NO_SELECT_VALUE;
      this.isShowCheckBox = false;
      this.clearSelectPhotos();
      this.setPhtotToStore();

      this.dbPhotoservice.deletephotos(this.buildingId, this.projectId, this.bigProcessId, locP, (photos) => {
        this.deleteCameraThumbnail(locP);
      });
    }
  }


  deleteCameraThumbnail = (photos) => {
    const thumbnail = this.cameraStoreService.cameraStore.thumbnail;
    if (!thumbnail) {
      return;
    }
    for (const photo of photos) {
      if (photo.photoTerminalNo === thumbnail.photoTerminalNo && photo.photoDate === thumbnail.photoDate) {
        this.cameraStoreService.setThumbnail(null);
      }
    }
  }

  // ==================================
  // cancle
  // ==================================
  cancle() {
    this.multitypeselect = PhotoListConf.NO_SELECT;
    this.multiSelectTitle = PhotoListConf.NO_SELECT_VALUE;
    this.isShowCheckBox = false;
    this.selectPhotos.splice(0, this.selectPhotos.length);
    this.deletePhotos.splice(0, this.deletePhotos.length);
  }

  selectphoto = (photo) => {

    if (this.multitypeselect === CD0005Constants.DELETE) {
      this.maxDeletePhotoJudge(photo);
    }
    if (this.multitypeselect === CD0005Constants.PHOTO_BOOK) {
      this.maxBookPhotoJudge(photo);
    }
  }

  goPhotoDetail = (photo) => {

    const photoList = this.setDetailPhotoList(JSON.parse(JSON.stringify(this.datasource)));


    this.photoDetailStoreService.setDetailPhotoList(photoList);


    this.photoDetailStoreService.setDetailPhoto(photo);

    this.headStateService.setBackRoute('/photo-list');
    this.routingService.goRouteLink('/photo-detail');
  }


  maxDeletePhotoJudge = (photo) => {

    const index = this.selectPhotos.indexOf(photo);

    if (index > -1) {
      this.selectPhotos.splice(index, 1);
    } else {
      if (this.deletePhotos.length >= PhotoListConf.MAX_PHOTO_COUNT) {
        this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00042.replace(
          '{value}', PhotoListConf.MAX_PHOTO_COUNT.toString()), null);
      } else {
        this.selectPhotos.push(photo);
        this.deletePhotos = this.selectPhotos;
      }
    }

  }

  maxBookPhotoJudge = (photo) => {

    // selectPhotosは全てcheckboxがcheckされる対象
    const index = this.selectPhotos.indexOf(photo);


    // checkした写真がもともと写真帳対象の場合、取消ということ
    if (index > -1) {
      this.selectPhotos.splice(index, 1);
    } else {
      this.selectPhotos.push(photo);
    }

    const bookphotos = this.calculationBookPhotos()[0];

    if (bookphotos.length > PhotoListConf.MAX_PHOTO_COUNT) {

      const index1 = this.selectPhotos.indexOf(photo);
      if (index1 > -1) {
        this.selectPhotos.splice(index1, 1);
      } else {
        this.selectPhotos.push(photo);
      }
      this.showAlert(AlertWindowConstants.ERRORTITLE, MessageConstants.E00042.replace(
        '{value}', PhotoListConf.MAX_PHOTO_COUNT.toString()), null);

    }



  }

  calculationBookPhotos() {
    const selectp = [...this.selectPhotos];

    // 画面で選択された写真を取得する
    const oldphotobook = this.photos.filter((ph) => ph.isOutputPhotoBook === 1);

    // 元写真帳出力対象で比較して取消された対象を集める
    for (const ph of selectp) {
      const index = oldphotobook.indexOf(ph);
      if (index > -1) {
        oldphotobook.splice(index, 1);
      }
    }

    const photos: any[] = [];
    const bookP: any[] = [];

    // 写真帳で取消一覧を作成
    for (const photo of oldphotobook) {
      const photo0505: Photos0505 = {};
      photo0505.photoDate = photo.photoDate.toString();
      photo0505.photoTerminalNumber = photo.photoTerminalNo;
      photo0505.isOutputPhotoBook = 0;
      photos.push(photo0505);
      bookP.push(photo);
    }

    // 今回写真帳に新しい追加する写真を取得
    for (const ph of this.photos) {
      const index = selectp.indexOf(ph);
      if (index > -1 && ph.isOutputPhotoBook === 1) {
        selectp.splice(index, 1);

      }

    }

    // 新しい追加する写真の一覧を作成
    for (const photo of selectp) {
      const photo0505: Photos0505 = {};
      photo0505.photoDate = photo.photoDate;
      photo0505.photoTerminalNumber = photo.photoTerminalNo;
      photo0505.isOutputPhotoBook = 1;
      photos.push(photo0505);
      bookP.push(photo);
    }

    return [photos, bookP];
  }


  filterSelectTitlePhotos() {
    // if (!this.title && this.isPc) {
    //   return;
    // }
    if (!this.title && this.sortby !== CD0007Constants.DATE) {
      return;
    }
    let filterphoto = [];
    let filterneedPhotoList = [];

    if (this.sortby === CD0007Constants.PROCESS) {
      const title = this.title as SelectTitle;
      filterphoto = this.photos.filter((photo) => {
        return photo.processId === title.processId && photo.workId === title.id;
      });
      filterneedPhotoList = this.needPhotoList.filter((photo) => {
        return photo.processId === title.processId && photo.workId === title.id;
      });
    } else if (this.sortby === CD0007Constants.MACHINE) {
      const title = this.title as SelectTitle;
      filterphoto = this.photos.filter((photo) => {
        return photo.machineId === title.id;
      });
      filterneedPhotoList = this.needPhotoList.filter((photo) => {
        return photo.machineId === title.id;
      });
    } else if (this.sortby === CD0007Constants.FLOOR) {
      const title = this.title as SelectTitle;
      filterphoto = this.photos.filter((photo) => {
        return photo.floorId === title.id;
      });
      filterneedPhotoList = this.needPhotoList.filter((photo) => {
        return photo.floorId === title.id;
      });
    } else {
      filterphoto = this.photos;
      filterneedPhotoList = [];
    }



    // photosAll 全て採るべき写真（noimage含まる）
    let photosAll = [];
    if (this.title && this.title.title.indexOf('不明') > -1) {
      if (this.sortby === CD0007Constants.PROCESS) {
        photosAll = this.unprocessphotos;
      } else if (this.sortby === CD0007Constants.MACHINE) {
        photosAll = this.unmachinephotos;
      } else if (this.sortby === CD0007Constants.FLOOR) {
        photosAll = this.unfloorphotos;
      }

    } else {
      photosAll = this.combineNeedPhotoList(JSON.parse(JSON.stringify(filterneedPhotoList)), filterphoto);
    }
    this.filterdAllPhotos = photosAll;
    this.showphotos(photosAll);
  }


  showphotos(photo?) {
    const filterPhotos = photo.filter(this.photofilter);


    // filter
    // 昇順・降順
    let resultPhotos = [];
    if (this.sortby === CD0007Constants.PROCESS) {
      resultPhotos = this.photosort.processBy(filterPhotos, this.sortdes);
    } else if (this.sortby === CD0007Constants.MACHINE) {
      resultPhotos = this.photosort.unitmarkBy(filterPhotos, this.sortdes);
    } else if (this.sortby === CD0007Constants.DATE) {
      resultPhotos = this.photosort.datekBy(filterPhotos, this.sortdes);

    } else {
      resultPhotos = this.photosort.floorkBy(filterPhotos, this.sortdes);
    }
    this.groupByPhoto(resultPhotos);
  }

  // photo-filter
  photofilter = (photo) => {

    if (this.bigProcessId === '03') {
      if (this.filmeds.length === 1) {
        if (this.filmeds.indexOf(CDFILMEDConstants.FILMED) > -1) {
          return this.statu.indexOf(photo.timingId.toString()) > -1 && ((photo.photoImage !== undefined && photo.photoImage != null)
            || (photo.photoPathLocal !== undefined && photo.photoPathLocal != null));
        } else {
          return this.statu.indexOf(photo.timingId.toString()) > -1 && ((photo.photoImage === undefined
            || photo.photoImage === null) && (photo.photoPathLocal === undefined || photo.photoPathLocal === null));
        }
      }
      return this.statu.indexOf(photo.timingId.toString()) > -1;
    } else {
      if (this.filmeds.length === 1) {
        if (this.filmeds.indexOf(CDFILMEDConstants.FILMED) > -1) {
          return (photo.photoImage !== undefined && photo.photoImage != null)
            || (photo.photoPathLocal !== undefined && photo.photoPathLocal != null);
        } else {

          return (photo.photoImage === undefined || photo.photoImage === null)
            && (photo.photoPathLocal === undefined || photo.photoPathLocal === null);
        }
      }
      return true;
    }

  }

  // 写真sort
  groupByPhoto(photos: Array<LocalPhoto>) {

    if (this.sortby === CD0007Constants.PROCESS) {
      // 工程作業sort

      this.titles = this.processSortTitles;
      this.datasource = this.photolistComponentHelp.groupByprocess(photos);

    } else if (this.sortby === CD0007Constants.MACHINE) {
      // 機器sort

      this.titles = this.machineSortTitles;
      this.datasource = this.photolistComponentHelp.groupBymachine(photos);

    } else if (this.sortby === CD0007Constants.DATE) {
      // 日付sort
      this.titles = [];
      this.datas = this.photolistComponentHelp.groupBydate(this.titles, photos);
      // 元の値を保存する
      if (!this.title) {

        if (this.titles.length > 0) {

          this.title = this.titles[0];

        } else {
          this.title = null;
        }
      }


      // 日付で全ての写真が削除された場合、初期値に設定する
      const key = JSON.stringify(this.title);
      if (!this.datas.get(key) || this.datas.get(key).length === 0) {
        if (this.titles.length > 0) {

          this.title = this.titles[0];

        } else {
          this.title = null;
        }
      }

      this.datasource = this.photolistComponentHelp.groupBydateCount(this.datas.get(key)
        || [], this.isPc ? this.photolistComponentHelp.pcWidth_count() : PhotoListConf.MOBILE_COUNT);

      // 写真詳細photolistを設定


    } else {
      //  フロアsort

      this.titles = this.floorSortTitles;
      this.datasource = this.photolistComponentHelp.groupByfloor(photos);
    }
    if (this.titles) {
      this.titles.forEach((item: SelectTitle) => {
        if (item.title) {
          item.title = SpaceString.spaceString(item.title);
        }
      });
    }
  }

  // 写真詳細photolistを設定
  setDetailPhotoList(photoList): any[] {

    let photos = [];
    if (photoList && photoList.length > 0) {

      // 写真結合

      this.concatList(photoList, photos);

      // noimages filter
      photos = photos.filter((photo) => {
        if (photo.photoTerminalNo && photo.photoTerminalNo.length > 0) {
          return photo;
        }
      });


    }
    return photos;
  }

  concatList(array: any[], resutlArr: any[]) {

    for (const item of array) {
      if (item instanceof Array) {
        this.concatList(item, resutlArr);
      } else {
        resutlArr.push(item);
      }
    }
  }

  // 工程・作業情報一覧API(API-08-01)
  private queryAPI0801(inDto: API0801InDto): void {
    // APIから、作業選択メニューリスト情報を取得
    this.api0801Serve.postExec(inDto, (outDto: API0801OutDto) => {
      // レスポンス
      this.api0801successed(outDto);
    },
      // fault
      () => {
      });

  }

  // 工程・作業情報一覧API(API-08-01)
  private getAPI0801InDto() {
    const inDto: API0801InDto = {};
    inDto.projectId = this.commonStore.projectId;
    inDto.bigProcessId = this.bigProcessId;
    return inDto;
  }

  // 工程・作業情報一覧API(API-08-01)
  private async api0801successed(outDto: API0801OutDto) {
    const processList = outDto.processList;
    this.commonStoreService.setProcessList(processList);
    this.processSortTitles = [];
    if (this.sortby === CD0007Constants.PROCESS) {
      this.title = (this.processSortTitles.length > 0 && this.processSortTitles[0] || null);
    }
    this.API0801Download = true;
    this.checkDownload();
  }

  // 物件情報提供API(IF-01-02)
  private queryIF0102(inDto: IF0102InDto): void {
    // APIから、物件情報を取得
    this.if0102Serve.postExec(inDto, (outDto: IF0102OutDto) => {
      // レスポンス
      this.if0102successed(outDto);
    },
      // fault
      () => {
      });

  }

  // 物件情報提供API(IF-01-02)
  private getIF0102InDto() {
    const inDto: IF0102InDto = {};
    inDto.buildingId = this.commonStore.buildingId;
    return inDto;
  }

  // 物件情報提供API(IF-01-02)
  private async if0102successed(outDto: IF0102OutDto) {
    const floorsList = outDto.floors;
    this.floorSortTitles = [];
    this.commonStoreService.setFloorsList(floorsList);
    this.IF0102Download = true;
    this.checkDownload();
  }

  // 機器情報
  private queryIF0106(inDto: IF0106InDto): void {
    // APIから、機器情報取得
    this.if0106Serve.postExec(inDto, (outDto: IF0106OutDto) => {
      // レスポンス
      this.if0106successed(outDto);
    },
      // fault
      () => {
      });

  }

  // 機器情報API(IF-01-06)
  private getIF0106InDto() {
    const inDto: IF0106InDto = {};
    inDto.buildingId = this.commonStore.buildingId;
    inDto.projectId = this.commonStore.projectId;
    inDto.sorts = [{ sortItem: 'machineId', order: SORTBY.ASC }];
    return inDto;
  }

  // 機器情報API(IF-01-06)
  private async if0106successed(outDto: IF0106OutDto) {
    const machineList = outDto.machineList;
    this.machineSortTitles = [];
    if (machineList !== null) {
      this.commonStoreService.setMachineList(machineList);
    }
    this.IF0106Download = true;
    this.checkDownload();
  }

  checkDownload() {
    if (this.API0801Download && this.IF0106Download && this.IF0102Download) {
      this.loadApi0501Photos();
    }
  }

  // 写真一覧提供API(API-05-01)
  getIndto(): API0501InDto {
    const api0501InDto: API0501InDto = {};
    api0501InDto.buildingId = this.buildingId;
    api0501InDto.projectId = this.projectId;
    api0501InDto.bigProcessId = this.bigProcessId;
    if (this.floorId != null) {
      api0501InDto.floorId = this.floorId;
    }
    if (this.processId != null) {
      api0501InDto.processId = this.processId;
    }
    if (this.workId != null) {
      api0501InDto.workId = this.workId;
    }
    if (this.machineId != null) {
      api0501InDto.machineId = this.machineId;
    }
    if (this.photoDate != null) {
      api0501InDto.photoDate = this.photoDate;
    }
    return api0501InDto;
  }

  // 写真一覧提供API(API-05-01)
  async loadApi0501Photos() {
    this.appLoadingStoreService.isloading(true);
    const executionAPI = await this.runExecutionService.runExecution('/API0501', this.getIndto(), 1000, false, false, true);
    if (executionAPI) {
      this.downloadJSON(JSON.parse(executionAPI));
    }
    this.appLoadingStoreService.isloading(false);
  }

  // 写真一覧提供API(API-05-01)
  async api0501Success(outDto: API0501OutDto) {
    const loadphotos = JSON.parse(JSON.stringify(outDto.photoList));
    const needPhotoList = outDto.needPhotoList;
    this.photomanagestoreService.setNeedPhotoList(needPhotoList);

    for (const photo of loadphotos) {
      photo.localPhotoFlag = PhotoListConf.SERVER_POTO;
      photo.retryCount = PhotoListConf.RETRY_DEFAULT;
    }

    if (this.isPc) {
      this.title = null;
      this.machine = null;
      this.photomanagestoreService.setPhotos(loadphotos);
      this.combinePhotoLogic();
    } else {
      await this.dbPhotoservice.savephotos(this.buildingId, this.projectId, this.bigProcessId, loadphotos);
      const allPhotos = await this.dbPhotoservice.selectphotos(this.buildingId, this.projectId, this.bigProcessId);
      this.photomanagestoreService.setPhotos(allPhotos);
      this.offlineStoreService.setPhotosDownload(loadphotos);
      this.combinePhotoLogic();
    }
  }

  async showAlert(header, message, buttons?) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: buttons != null ? buttons : [TEXT.OK]
    });

    await alert.present();
  }

  // 写真削除
  // 写真削除API(API-05-04)
  async api0504Exec(photos: any[], deleteSuccess) {

    const deletePhotos: any[] = [];
    for (const photo of photos) {
      const deletePhoto: DeletePhotoList = {};
      deletePhoto.photoDate = photo.photoDate.toString();
      deletePhoto.photoTerminalNumber = photo.photoTerminalNo;
      deletePhotos.push(deletePhoto);
    }

    const indto: API0504InDto = {};
    indto.deletePhotoList = deletePhotos;

    this.api0504Serve.postExec(indto, (outDto: API0504OutDto) => {
      let message = '';
      if (outDto.resultCode === ResultType.NORMAL) {
        this.api0504success();
        deleteSuccess();
        message = MessageConstants.I00004;

        if (!this.isPc) {

          this.dbPhotoservice.deletephotos(this.buildingId, this.projectId, this.bigProcessId, photos);
        }

        this.showAlert(AlertWindowConstants.TIPSTITLE, message);
      }
      // else {
      //   message = MessageConstants.E00004;
      //   this.showAlert(AlertWindowConstants.ERRORTITLE, message);
      // }
    },
      // fault
      () => {
      }, true, true);
    return;
  }

  // 写真削除API(API-05-04)
  async api0504success() {
    this.deletePhotoSuccess();
  }

  // 写真削除API(API-05-04)
  deletePhotoSuccess() {

    const selectPs = this.selectPhotos;
    for (const ph of selectPs) {
      const index = this.photos.indexOf(ph);
      if (index > -1) {
        this.photos.splice(index, 1);
      }
    }
    this.multitypeselect = PhotoListConf.NO_SELECT;
    this.multiSelectTitle = PhotoListConf.NO_SELECT_VALUE;
    this.isShowCheckBox = false;

  }

  // 10.写真帳出力対象選択
  // 写真帳出力対象変更API(API-05-05)
  async api0505Exec(photos: any, bookP: any) {

    const indto: API0505InDto = {};
    indto.photoList = photos;


    this.api0505Serve.postExec(indto, async (outDto: API0505OutDto) => {
      let message = '';
      if (outDto.resultCode === ResultType.NORMAL) {
        for (const photo of bookP) {
          photo.isOutputPhotoBook = photo.isOutputPhotoBook === PhotoListConf.OUT_PUT_PHOTO_BOOK ? 0 : PhotoListConf.OUT_PUT_PHOTO_BOOK;
        }
        await this.api0505success(bookP);
        message = MessageConstants.I00018;
        this.showAlert(AlertWindowConstants.TIPSTITLE, message);
      } else {
        // message = MessageConstants.E00064;
        message = MessageConstants[outDto.messageCode];
        this.showAlert(AlertWindowConstants.ERRORTITLE, message);
      }
    },
      // fault
      () => {
      }, true, true);
    return;
  }

  async api0505success(photos: any) {

    this.multitypeselect = PhotoListConf.NO_SELECT;
    this.multiSelectTitle = PhotoListConf.NO_SELECT_VALUE;
    this.isShowCheckBox = false;
    await this.updatePhotoToSql(photos);

    this.setPhtotToStore();
    this.clearSelectPhotos();
  }

  async updatePhotoToSql(photos: any[]) {
    if (!this.isPc) {
      await this.dbPhotoservice.updatephotos(photos);
    }
  }

  clearSelectPhotos() {
    this.selectPhotos.splice(0, this.selectPhotos.length);
  }

  setPhtotToStore() {
    this.photomanagestoreService.setPhotos(this.photos);
    this.combinePhotoLogic();
  }

  compare(pro, sort) {
    return (a, b) => {
      const value1 = a[pro];
      const value2 = b[pro];
      if (sort === '1') {
        return value1 >= value2 ? 1 : -1;
      } else {
        return value1 < value2 ? 1 : -1;
      }

    };
  }


  changeCommstore() {
    this.commonStore.bigProcessId = this.bigProcessId;
    this.commonStore.bigProcessName = this.bigProcess.value;
    this.commonStoreService.setCommon(this.commonStore);
  }

  qrstate() {
    if (!this.offlineUnits) {
      this.offlineUnits = [];
    }

    const storeUnit = this.offlineUnits.filter((unit) => {
      if (unit.unitId === this.machine.machineId) {
        return unit;
      }
    });

    // storeで存在している
    if (storeUnit.length > 0) {
      if (storeUnit[0].retryCount === PhotoConf.RETRY_MAX_COUNT) {
        return 'qrfailure';
      } else {
        // 登録待ち
        return 'qrdefault';
      }
    } else {
      if (this.machine.machineNumber) {
        // 登録済
        return 'qrok';
      } else {
        // 未登録
        return 'qrdefault';
      }
    }
  }

  // -----------------------------------------
  // download s3 json
  private async apiSuccessed(res: HttpResponse<Blob>, resHeader0501: API0501OutDtoHeader) {
    if (resHeader0501.resultCode === ResultType.NORMAL) {
      const fileReadResult = await this.runExecutionService.apiSuccessFileRead(res);
      if (fileReadResult) {
        this.api0501Success(fileReadResult);
      } else {
        this.appLoadingStoreService.isloading(false);
      }
    }
    return;
  }

  // 写真一覧URLでダウンロード
  private downloadJSON(resHeader0501: API0501OutDtoHeader) {
    this.httpClient.get(resHeader0501.photoAllListUrl, {
      observe: 'response',
      responseType: 'blob'
    }).subscribe(
      async (res: HttpResponse<Blob>) => {
        this.apiSuccessed(res, resHeader0501);
      },
      (err) => {
        console.error('Error: ' + err);
        this.appLoadingStoreService.isloading(false);
      }
    );
  }
}
