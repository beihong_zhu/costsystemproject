import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { CameraStoreService } from '@store';
import { CameraActionTypes } from '../../../../app_photo/store/camera/camera.store';
import { S0301Assets } from '@constant/assets';

interface FlashModl {
  name?: string;
  icon?: string;
}


@Component({
  selector: 'camera-popover',
  templateUrl: './camera-popover.component.html',
  styleUrls: ['./camera-popover.component.scss'],
})
export class CameraPopoverComponent implements OnInit {

  datas: FlashModl[];
  type: string;
  selected: string = null;

  private cameraFlashAuto = S0301Assets.CAMERAFLASHAUTO;
  private cameraFlashOff = S0301Assets.CAMERAFLASHOFF;
  private cameraFlashOn = S0301Assets.CAMERAFLASHON;

  constructor(
    private popoverController: PopoverController,
    private cameraStoreService: CameraStoreService) {
    this.selected = this.cameraStoreService.cameraStore.flashMode;
  }

  ngOnInit() {
    console.log('type', this.type);
    console.log('data', this.datas);

    // data 無い場合
    const noData = [{ name: '---', data: null }];
    if (!this.datas || (this.datas && this.datas.length === 0)) {
      this.datas = noData;
    }

  }

  click(data, type) {
    console.log('click type', type);
    console.log('click data', data);
    if (type === CameraActionTypes.SETFLASHMODE) {
      let flashImage;
      if (data.name === 'ON') {
        flashImage = this.cameraFlashOn;
      } else if (data.name === 'AUTO') {
        flashImage = this.cameraFlashAuto;
      } else if (data.name === 'OFF') {
        flashImage = this.cameraFlashOff;
      }
      this.cameraStoreService.setFlashMode(data.name);
      this.cameraStoreService.setFlashImage(flashImage);
      this.popoverController.dismiss(data);
    }
  }
}
