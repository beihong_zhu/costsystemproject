import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { S0102Constants } from '@constant/constant.photo';
import { RoutingService } from '@service/app-route.service';
import { IF0103Service } from '@service/api';
import { IF0103InDto, IF0103OutDto } from '@service/dto';
import { SORTBY } from '@constant/code';
import { AppState, CommonStore, CommonStoreService, AppStateStoreService } from '@store';
import { Project } from '../dto/projectdto';
import { PopoverController, Platform } from '@ionic/angular';
import { TextPopoverComponent } from '../popover/Text-popover.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.page.html',
  styleUrls: ['./project.page.scss', './project.page.mobile.scss', './project.page.winapp.scss']
})

export class ProjectPage {

  isPc: boolean;

  // 物件ID
  buildingId: string;
  // 物件名
  buildingName: string;

  title: string = S0102Constants.TITLE;
  // 案件IDレベル
  projectIdLabel: string = S0102Constants.PROJECTID;
  // 案件件数レベル
  projectCountLabel: string = S0102Constants.NUMBEROFCONSTRUCTIONS;
  // 案件単位レベル
  projectUnitLabel: string = S0102Constants.COUNTS;
  // 案件名レベル
  projectNameLabel: string = S0102Constants.PROJECTNAME;

  appState: AppState;
  commonStore: CommonStore;
  projectCount: number;
  projectList: Project[];

  platformtype: string;

  constructor(
    private popoverController: PopoverController,
    private routerService: RoutingService,
    private commonStoreService: CommonStoreService,
    private appStateService: AppStateStoreService,
    private if0103Service: IF0103Service,
    private platform: Platform
  ) {

    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;


    this.platformtype = 'projectPage-' + this.appStateService.getPlatform();

  }

  // 画面初期化
  ionViewWillEnter() {
    this.buildingName = this.commonStore.buildingName;
    this.isPc = this.appState.isPC ? true : false;
    // セッションから、物件IDと物件名を取得
    this.queryProjectList(this.getCreateInDto());
  }

  clickItem(project) {
    // console.log('project', project);
    this.commonStore.projectId = project.projectId;
    this.commonStore.projectName = project.projectName;
    // 画面選択した物件情報は、ストアを保存します。
    this.commonStoreService.setCommon(this.commonStore);

    let url = '';
    // PCの場合
    if (this.isPc) {
      // 写真一覧画面に遷移する。
      url = '/photo-list';
    } else {
      // スマホの場合
      // 大工程選択画面に遷移する。
      url = '/bigProcess';
    }

    this.routerService.goRouteLink(url);
  }

  async onPress(ev: Event, project) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [project.projectId, project.projectName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


  async onPressBuilding(ev: Event) {
    console.log('onPressBuilding', ev);
    const messageArr1 = [this.commonStore.buildingName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


  // InDtoを作成
  getCreateInDto(): IF0103InDto {
    const inDto: IF0103InDto = {};
    // ユーザID
    inDto.userId = this.commonStore.userId;
    inDto.buildingId = this.commonStore.buildingId;
    inDto.sort = [{ sortItem: 'project_id', order: SORTBY.ASC }];

    return inDto;
  }

  queryProjectList(inDto: IF0103InDto) {

    // APIから、物件情報を取得
    this.if0103Service.postExec(inDto, (outDto: IF0103OutDto) => {
      // レスポンス
      this.if0103successed(outDto);
    },
      // fault
      () => {
      });

  }

  async if0103successed(outDto: IF0103OutDto) {

    // 物件一覧情報
    this.projectList = new Array();

    // API戻す情報から、画面情報を作成
    Array.from({ length: outDto.projects.length }).map((_, i) => {

      this.projectList[i] = new Project();
      // 物件ID
      this.projectList[i].projectId = outDto.projects[i].projectId;
      // 物件名
      this.projectList[i].projectName = outDto.projects[i].projectName;
    });

    this.projectCount = outDto.projects.length;

  }

}
