import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatestockPage } from './createstock.page';

const routes: Routes = [
  {
    path: '',
    component: CreatestockPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatestockPageRoutingModule {}
