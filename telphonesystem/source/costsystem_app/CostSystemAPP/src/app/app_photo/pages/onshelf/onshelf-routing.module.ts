import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnshelfPage } from './onshelf.page';

const routes: Routes = [
  {
    path: '',
    component: OnshelfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnshelfPageRoutingModule {}
