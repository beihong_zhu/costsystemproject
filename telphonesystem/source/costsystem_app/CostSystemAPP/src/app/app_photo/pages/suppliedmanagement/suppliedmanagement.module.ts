import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuppliedmanagementPageRoutingModule } from './suppliedmanagement-routing.module';

import { SuppliedmanagementPage } from './suppliedmanagement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuppliedmanagementPageRoutingModule
  ],
  declarations: [SuppliedmanagementPage]
})
export class SuppliedmanagementPageModule {}
