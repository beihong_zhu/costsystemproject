import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgentSummaryPageRoutingModule } from './agent-summary-routing.module';

import { AgentSummaryPage } from './agent-summary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgentSummaryPageRoutingModule
  ],
  declarations: [AgentSummaryPage]
})
export class AgentSummaryPageModule {}
