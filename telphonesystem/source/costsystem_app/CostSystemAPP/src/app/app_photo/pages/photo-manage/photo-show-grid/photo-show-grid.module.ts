import { NgModule } from '@angular/core';
import { PhotoShowGridComponent } from './photo-show-grid.component';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { PhotoGridItemComponent } from '../photo-grid-item/photo-grid-item.component';

@NgModule({
  // declarations: [PhotoGridComponent, PhotoGridItemComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoShowGridComponent
      }
    ])
  ]
})
export class PhotoShowGridModule { }
