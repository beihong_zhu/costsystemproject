export class WorkDto {
  // 作業ID
  workId?: number;
  // 選択
  isChecked?: boolean;
  // 作業名
  workName?: string;
  // 更新ユーザ
  updatedBy?: string;
}
