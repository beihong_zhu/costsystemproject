import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { API3002Service } from '@service/api/api-30-02.service';
import { API3002InDto, API3002OutDto } from '@service/dto';
import { ResultType } from '../../../app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysFundRecorderDto } from './dto/recorderfund.dto';
import { FundrecorderEditComponent } from './fundrecorder-edit/fundrecorder-edit.component';

@Component({
  selector: 'app-fundrecorder',
  templateUrl: './fundrecorder.page.html',
  styleUrls: ['./fundrecorder.page.scss'],
})

export class FundrecorderPage implements OnInit {

  public bussinessInfo: any = {
    bussinessInfoList: ['账户', '支付宝', '微信'], 
    bussiness: '账户',
    bussiTypeList: ['充值', '提现', '支付', '退款', '即反佣金', '退款撤销', '充值撤销'],
    bussiType: '充值'
  }

  public ocrManufacturingDate: string;
  public corpId: string;
  public agentName: string;

  public agentRecordName: string;
  public reqId: string;
  public orderId: string;
  public billId: string;
  public orderForm: string;
  public orderType: string;
  public startTime: string;
  public endTime: string;

  public sysFundRecorderList: Array<SysFundRecorderDto> = new Array<SysFundRecorderDto>();
  private sysFundRecorderEdit: SysFundRecorderDto;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    private api3002Service: API3002Service
  ) { }

  ngOnInit() {
    this.clearCache();
  }

  ngOnDestroy() {
    this.clearCache();
  }

  /**
   * 资金记录画面重新表示
   */
  reInit(): void {
    this.clearCache();
  }

  public sysFundRecorderChkClick(sysFundRecorder: any, sysFundRecorderList: any) {
    if (sysFundRecorder.isChecked) {
      for (const getSysFundRecorder of sysFundRecorderList) {
        if (getSysFundRecorder !== sysFundRecorder) {
          getSysFundRecorder.isChecked = false;
        }
      }
      // 设置公共对象
      this.sysFundRecorderEdit = new SysFundRecorderDto();
      this.sysFundRecorderEdit = sysFundRecorder;
    } else {
      this.sysFundRecorderEdit = null;
    }
  }

  public onSearch() {
    this.clearCache();
    this.api3002ServicePostExec();
  }

  public onEdit() {
    if (this.sysFundRecorderEdit !== null && this.sysFundRecorderEdit !== undefined) {
      this.openEditPage(this.sysFundRecorderEdit);
    }
    this.sysFundRecorderEdit = null;
  }

  private get3002InDto() {
    let api3002InDto: API3002InDto = {};

    api3002InDto.corpId = this.corpId;
    api3002InDto.agentName = this.agentName;
    api3002InDto.agentRecordName = this.agentRecordName;
    api3002InDto.reqId = this.reqId;
    api3002InDto.orderId = this.orderId;
    api3002InDto.billId = this.billId;
    api3002InDto.orderForm = this.orderForm;
    api3002InDto.orderType = this.orderType;
    api3002InDto.startTime = this.startTime;
    api3002InDto.endTime = this.endTime;

    return api3002InDto;
  }

  private api3002ServicePostExec() {
    this.api3002Service.postExecNew(this.get3002InDto()).then((res) => {
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3002successed(response);
      }
    })
      .catch(ApiResponse.errorHandler);
  }

  private api3002successed(outDto: API3002OutDto) {
    this.sysFundRecorderList = new Array<SysFundRecorderDto>();
    const loadSysfondRecorder = JSON.parse(JSON.stringify(outDto.capitalRecordModelResList));
    for (const sysUserInfoModel of loadSysfondRecorder) {
      let sysFundrecorderDto: SysFundRecorderDto = new SysFundRecorderDto();
      sysFundrecorderDto.agentId = sysUserInfoModel.agentId;
      sysFundrecorderDto.agentName = sysUserInfoModel.agentName;
      sysFundrecorderDto.reqId = sysUserInfoModel.reqId;
      sysFundrecorderDto.orderId = sysUserInfoModel.orderId;
      sysFundrecorderDto.billId = sysUserInfoModel.billId;
      sysFundrecorderDto.agentReduceName = sysUserInfoModel.agentReduceName;
      sysFundrecorderDto.orderForm = sysUserInfoModel.orderForm;
      sysFundrecorderDto.orderType = sysUserInfoModel.orderType;
      sysFundrecorderDto.capital = sysUserInfoModel.capital;
      sysFundrecorderDto.price = sysUserInfoModel.price;
      sysFundrecorderDto.status = sysUserInfoModel.status;
      sysFundrecorderDto.ts = sysUserInfoModel.ts;
      this.sysFundRecorderList.push(sysFundrecorderDto);
    }
  }

  private async openEditPage(sysFundRecorder: SysFundRecorderDto) {
    const modal = await this.modalController.create({
      component: FundrecorderEditComponent,
      componentProps: {
        value: sysFundRecorder
      },
      cssClass: 'fundrecorderEditModal',
      backdropDismiss: false
    });
    await modal.present();
    const modalFundRecord: any = await modal.onDidDismiss();
    // 按钮按下的判断
    if (modalFundRecord.data !== null) {
      this.reInit();
    }
  }

  /**
   * 缓存清除
   */
  private clearCache(): void {
    this.sysFundRecorderList = null;
    this.sysFundRecorderEdit = null;
  }
}
