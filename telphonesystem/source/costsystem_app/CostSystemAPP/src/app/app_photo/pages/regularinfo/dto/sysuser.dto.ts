export class SysUserDto {
   // 用户ID
   userId?: string;
   // 用户名
   userName?: string;
   // 公司名
   companyName?: string;
   // 权限ID
   authorityId?: string;
   // 邮箱地址
   mailAddress?: string;
   // 登录flg
   isLogin?: number;
}
