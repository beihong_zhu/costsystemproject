import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductstockPage } from './productstock.page';

describe('ProductstockPage', () => {
  let component: ProductstockPage;
  let fixture: ComponentFixture<ProductstockPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductstockPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductstockPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
