export class SysAgentInfoDto {
   // 代理ID
   AgentId?: string;
   // 代理名字
   AgentName?: string;
   // 代理简称
   AgentReduceName?: string;
   //电话
   Tel?: string;
   // 邮件
   mail?: string;
   // 地址
   address?: string;
   // 商业模式
   businessMode ?:string;

}
