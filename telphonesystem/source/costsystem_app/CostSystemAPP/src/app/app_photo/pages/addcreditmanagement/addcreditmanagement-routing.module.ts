import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddcreditmanagementPage } from './addcreditmanagement.page';

const routes: Routes = [
  {
    path: '',
    component: AddcreditmanagementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddcreditmanagementPageRoutingModule {}
