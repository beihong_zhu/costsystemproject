import { Component, OnInit } from '@angular/core';
import { S2012Constants } from '@constant/constant.photo';
import { AlertController, NavParams } from '@ionic/angular';
import { SysOnShelfProductInfoDto } from '../dto';

@Component({
  selector: 'app-onshelf-edit',
  templateUrl: './onshelf-edit.component.html',
  styleUrls: ['./onshelf-edit.component.scss']
})

export class OnshelfEditComponent implements OnInit {
  /**
   * 获取画面項目名称
   */
  public onshelfEditLable: any = {
    title: S2012Constants.TITLE,
    agentName: S2012Constants.AGENTNAME,
    productName: S2012Constants.PRODUCTNAME,
    productType: S2012Constants.PRODUCTTYPE,
    serviceProvide: S2012Constants.SERVICEPROVIDE,
    money: S2012Constants.MONEY,
    price: S2012Constants.PRICE,
    disaccount: S2012Constants.DISACCOUNT,
    checkPrice: S2012Constants.CHECKPRICE,
    checkCost: S2012Constants.CHECKCOST,
    status: S2012Constants.STATUS,
    change: S2012Constants.CHANGE,
    cancel: S2012Constants.CANCEL,
  };


  public sysOnShelfProductInfo: SysOnShelfProductInfoDto = new SysOnShelfProductInfoDto();
  constructor(
    public alertController: AlertController,
    public navParams: NavParams) {
      let sysOnShelfProductInfo: SysOnShelfProductInfoDto = new SysOnShelfProductInfoDto();
      sysOnShelfProductInfo = navParams.data.value;
      this.sysOnShelfProductInfo.AgentReduceName = sysOnShelfProductInfo.AgentReduceName
      this.sysOnShelfProductInfo.productName = sysOnShelfProductInfo.productName;
      this.sysOnShelfProductInfo.productType = sysOnShelfProductInfo.productType;
      this.sysOnShelfProductInfo.spId = sysOnShelfProductInfo.spId;
      this.sysOnShelfProductInfo.value = sysOnShelfProductInfo.value;
      this.sysOnShelfProductInfo.price = sysOnShelfProductInfo.price;
      this.sysOnShelfProductInfo.disaccount = sysOnShelfProductInfo.disaccount;
      this.sysOnShelfProductInfo.checkPrice = sysOnShelfProductInfo.checkPrice;
      this.sysOnShelfProductInfo.checkCost = sysOnShelfProductInfo.checkCost;
      this.sysOnShelfProductInfo.status = sysOnShelfProductInfo.status;
      
  }

  ngOnInit() {

  }

  /**
   * 更新按钮点击事件
   */
  async updateClick() {

  }

  /**
   * 取消按钮点击事件
   */
  async cancelClick() {

  }

  /**
   * 关闭按钮点击事件
   */
  async closeClick() {
    this.navParams.data.modal.dismiss(null);
  }
}
