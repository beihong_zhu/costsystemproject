import { Component, OnInit, ViewChildren, QueryList, OnDestroy, HostListener, Injectable } from '@angular/core';
import { ModalController, AlertController, Events } from '@ionic/angular';
import { PdfViewerComponent } from '../../../components/ng2-pdf-viewer/pdf-viewer.component';
import { DrawDisplayGamenDto, RoomDto, MachineDto, FloorDto, DrawingDto, PinDto } from './dto';
import { RoutingService } from '@service/app-route.service';
import { DrawSelectComponent } from './draw-select/draw-select.component';
import { DrawDisplayStoreService, AppStateStoreService, CommonStoreService, DrawOfflineStoreService, AppLoadingStoreService } from '@store';
import { IF0108Service, IF0106Service, API0605Service, API0601Service } from '@service/api';
import {
  IF0108InDto, IF0108OutDto, IF0106InDto, IF0106OutDto,
  API0605InDto, API0605OutDto, API0601OutDto, API0601InDto
} from '@service/dto';
import { S0601Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MatDrawerContainer } from '@angular/material';
import { SORTBY, CD0019Constants } from '@constant/code';
import { MessageConstants } from '@constant/message-constants';
import * as _ from 'lodash';
import { PhotoSimpleGridComponent } from '../draw-photo/photo-simple-grid/photo-simple-grid.component';
import { DbDrawService } from '@service/db/db-drawservice';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Machine } from '../../../store/common/common.store';

/**
 * 図面表示画面
 */
@Component({
  selector: 'app-draw-display',
  templateUrl: './draw-display.page.html',
  styleUrls: ['./draw-display.page.scss', './draw-display.page.mobile.scss', './draw-display.page.winapp.scss'],
})
@Injectable()
export class DrawDisplayPage implements OnDestroy {

  /**
   * 図面
   */
  @ViewChildren(PdfViewerComponent) pdfview: QueryList<PdfViewerComponent>;
  @ViewChildren(MatDrawerContainer) matDrawerContainer: QueryList<MatDrawerContainer>;

  /**
   * 図面内をクリックしたままの状態でスクロールが可能
   */
  public screenX: number = null;
  public screenY: number = null;
  public matDrawerScroll: HTMLElement = null;

  /**
   * 画面DTO
   */
  public gamenDto: DrawDisplayGamenDto;
  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    title: S0601Constants.TITLE,
    setButton: S0601Constants.SETBUTTON,
  };
  /**
   * 部屋選択option
   */
  public optionRoomList?: RoomDto[];
  /**
   * APIから取得するの機器情報
   */
  public machineMap: Map<number, MachineDto>;
  /**
   * PDFの高さ、幅（百分率）
   */
  public pdfviewHeight: number;
  public pdfviewWidth: number;
  /**
   * PDFの高さ、幅（百分率、%含まる）
   */
  public pdfviewHeightPx: string;
  public pdfviewWidthPx: string;
  /**
   * PDFのsrc
   */
  public pdfFileImage = '';

  /**
   * isPc
   */
  public isPc: boolean;

  platformtype: string;

  constructor(private routerService: RoutingService,
    public alertController: AlertController,
    private drawDisplayStoreService: DrawDisplayStoreService,
    public appStateService: AppStateStoreService,
    public modalController: ModalController,
    public if0108Service: IF0108Service, // 図面情報
    public if0106Service: IF0106Service, // 機器情報
    public api0605Service: API0605Service, // フロア情報一覧
    public api0601Service: API0601Service, // ピン情報一覧
    public commonStoreService: CommonStoreService,
    public drawOfflineStoreService: DrawOfflineStoreService,
    public dbDrawService: DbDrawService,
    public file: File,
    public webview: WebView,
    public events: Events,
    private appLoadingStoreService: AppLoadingStoreService
  ) {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    (window as any).pdfWorkerSrc = './assets/js/pdf.worker.js';
    this.gamenDto = { drawingInfoDto: { drawings: [] }, pdfPinInfo: [], pinInfo: [], selectedDrawing: {} };
    events.subscribe('pdfDownloadSuccess', (buildingId: number, floorId: number) => {
      this.refreshPdfSrc(buildingId, floorId);
    });

    this.platformtype = 'drawDisplay-' + this.appStateService.getPlatform();
  }

  /**
   * 初期化する
   */
  ionViewWillEnter() {
    this.gamenDto = { drawingInfoDto: { drawings: [] }, pdfPinInfo: [], pinInfo: [], selectedDrawing: {} };
    this.pdfFileImage = '';
    if (!this.isPc) {
      this.if0108Successed(_.cloneDeep(this.drawDisplayStoreService.drawDisplayStore.drawingInfoDto));
    } else {
      // 図面情報の取得
      this.if0108Service.postExec(this.get0108InDto(), async (outDto: IF0108OutDto) => {
        this.if0108Successed(outDto);
      },
        // fault
        () => {
        });
    }

  }

  /**
   * if0108呼出、戻り値を設定
   */
  async if0108Successed(outDto: IF0108OutDto) {
    this.gamenDto.drawingInfoDto.drawings = outDto.drawings;
    this.gamenDto.drawingInfoDto.buildingId = outDto.buildingId;
    if (outDto && outDto.resultCnt > 0) {
      // 機器情報の取得
      this.getMachineInfo();

    } else {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.WARNINGTITLE,
        message: MessageConstants.W00004,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  /**
   * 機器情報取得
   */
  getMachineInfo() {
    if (!this.isPc) {
      this.if0106Offline(_.cloneDeep(this.commonStoreService.commonStore.machineList));
    } else {
      this.if0106Service.postExec(this.get0106InDto(), (outDto: IF0106OutDto) => {
        this.if0106Successed(outDto);
      },
        // fault
        () => {
        });
    }
  }

  if0106Offline(machineList: Machine[]) {
    this.machineMap = new Map();
    // 取得できる
    for (const machine of machineList) {
      this.machineMap.set(machine.machineId, machine);
    }
    // フロア情報の取得
    this.getFloorInfo();
  }

  /**
   * if0106呼出、戻り値を設定
   */
  async if0106Successed(outDto: IF0106OutDto) {
    this.machineMap = new Map();
    // 取得できる
    if (outDto && outDto.machineList) {
      for (const machine of outDto.machineList) {
        this.machineMap.set(machine.machineId, machine);
      }
    }
    // フロア情報の取得
    this.getFloorInfo();
  }

  /**
   * フロア情報の取得
   */
  getFloorInfo() {
    if (!this.isPc) {
      this.api0605Successed(_.cloneDeep(this.drawOfflineStoreService.drawOfflineStore.api0605OutDto));
    } else {
      this.api0605Service.postExec(this.get0605InDto(), (outDto: API0605OutDto) => {
        this.api0605Successed(outDto);
      },
        // fault
        () => {
        });
    }
  }

  /**
   * api0605呼出、戻り値を設定
   */
  async api0605Successed(outDto: API0605OutDto) {
    const floorMap: Map<number, FloorDto> = new Map();
    // 取得できる
    if (outDto.floorList) {
      for (const floor of outDto.floorList) {
        floorMap.set(floor.floorId, floor);
      }
      for (const drawing of this.gamenDto.drawingInfoDto.drawings) {
        drawing.floor = floorMap.get(drawing.floorId) || {};
      }
    }
    // ピン情報の取得
    this.getPinInfo();
  }

  /**
   * ピン情報の取得
   */
  getPinInfo() {
    if (!this.isPc) {
      this.api0601Successed(_.cloneDeep(this.drawDisplayStoreService.drawDisplayStore.pinInfo));
    } else {
      this.api0601Service.postExec(this.get0601InDto(), (outDto: API0601OutDto) => {
        this.api0601Successed(outDto.pinList);
      },
        // fault
        () => {
        });
    }
  }

  /**
   * api0601呼出、戻り値を設定
   */
  api0601Successed(pinList) {
    // 取得できる
    if (pinList) {
      for (const pin of pinList) {
        this.gamenDto.pinInfo.push({
          // ピンリスト
          pinId: pin.pinId,
          // 図面ID
          drawingId: pin.drawingId,
          // 機器IDより機情報取得
          machine: this.machineMap.get(pin.unitId) || {},
          // 機種区分
          // modeltypeDivision: pin.modeltypeDivision,
          // X座標
          positionX: pin.positionX,
          // Y座標
          positionY: pin.positionY,
          // 配置フラグ
          isInstalled: pin.isInstalled
        });
      }
    }
    this.gamenDto.selectedDrawingId = this.gamenDto.drawingInfoDto.drawings[0].drawingId;

    // 他画面からの復帰時、直前で選択していたフロアと図面を表示する。
    const storeDrawingId = this.drawDisplayStoreService.drawDisplayStore.selectedDrawingId;
    if (storeDrawingId !== null) {
      for (const drawingDto of this.gamenDto.drawingInfoDto.drawings) {
        if (drawingDto.drawingId === storeDrawingId) {
          this.gamenDto.selectedDrawingId = storeDrawingId;
        }
      }
    }
    this.selectedDrawingChange();
    this.gamenDto.zoomSize = 1;
  }

  /**
   * ピン情報を取得するAPIのinDtoの作成
   */
  get0601InDto(): API0601InDto {
    const inDto: API0601InDto = {};
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    return inDto;
  }

  /**
   * フロア情報を取得するAPIのinDtoの作成
   */
  get0605InDto(): API0605InDto {
    const inDto: API0605InDto = {};
    inDto.buildingId = this.commonStoreService.commonStore.buildingId;
    inDto.floorIdList = [];
    const floorId = 'floorId';
    for (const drawing of this.gamenDto.drawingInfoDto.drawings) {
      inDto.floorIdList.push({ floorId: drawing.floorId });
    }
    inDto.sort = [{ sortItem: 'displayOrder', order: SORTBY.ASC }];
    return inDto;
  }

  /**
   * 図面情報を取得するAPIのinDtoの作成
   */
  get0108InDto(): IF0108InDto {
    const inDto: IF0108InDto = {};
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.sort = [{ sortItem: 'drawingId', order: SORTBY.ASC }];
    return inDto;
  }

  /**
   * 機器情報を取得するAPIのinDtoの作成
   */
  get0106InDto(): IF0106InDto {
    const inDto: IF0106InDto = {};
    inDto.buildingId = this.commonStoreService.commonStore.buildingId;
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.sorts = [{ sortItem: 'machineId', order: SORTBY.ASC }];
    return inDto;
  }

  /**
   * 拡大ボタンについて
   */
  addZoom() {
    if (this.gamenDto.zoomSize < 10) {
      this.gamenDto.zoomSize = (this.gamenDto.zoomSize * 10 + 1) / 10;
    }
  }

  /**
   * 縮小ボタンについて
   */
  removeZoom() {
    if (this.gamenDto.zoomSize > 0.3) {
      this.gamenDto.zoomSize = (this.gamenDto.zoomSize * 10 - 1) / 10;
    }
  }

  /**
   * PDFの高さ、幅を取得すること
   */
  pdfviewSizeChange() {
    const pdfMultiPageViewer = 'pdfMultiPageViewer';
    if (this.pdfview && this.pdfview.first) {
      const pdfview: PdfViewerComponent = this.pdfview.first;
      this.pdfviewWidth = pdfview[pdfMultiPageViewer]._pages[0].viewport.width;
      this.pdfviewHeight = 0;

      const pageSize: number = pdfview[pdfMultiPageViewer]._pages.length;
      for (let i = 0; i < pageSize; i++) {
        this.pdfviewHeight += pdfview[pdfMultiPageViewer]._pages[i].viewport.height;
      }
      this.pdfviewWidthPx = this.pdfviewWidth + 'px';
      this.pdfviewHeightPx = this.pdfviewHeight + 'px';
    }
  }

  /**
   * PDF読み込み完了
   */
  pageRendered() {
    this.pdfviewSizeChange();
  }

  /**
   * 図面切替
   */
  async selectedDrawingChange() {
    this.gamenDto.pdfPinInfo = [];
    for (const drawingDto of this.gamenDto.drawingInfoDto.drawings) {
      if (drawingDto.drawingId === this.gamenDto.selectedDrawingId) {
        this.gamenDto.selectedDrawing = drawingDto;
      }
    }

    if (!this.isPc) {
      const selectedDrawing = this.gamenDto.selectedDrawing;
      const dbDraw = await this.dbDrawService.selectdraw(this.commonStoreService.commonStore.buildingId, selectedDrawing.floorId);
      if (dbDraw) {
        const filePath = this.file.dataDirectory + dbDraw[0].filePathLocal;
        this.pdfFileImage = this.webview.convertFileSrc(filePath);
      } else {
        this.pdfFileImage = selectedDrawing.fileImage;
      }
    } else {
      this.pdfFileImage = this.gamenDto.selectedDrawing.fileImage;
    }

    this.optionRoomList = [];
    // 図面でピンを追加
    for (const pinDto of this.gamenDto.pinInfo) {
      if (pinDto.drawingId === this.gamenDto.selectedDrawingId) {
        if (pinDto.isInstalled === CD0019Constants.SET_FLAG) {
          pinDto.positionXpercent = pinDto.positionX + '%';
          pinDto.positionYpercent = pinDto.positionY + '%';
          this.gamenDto.pdfPinInfo.push(pinDto);
        }
      }
    }
    this.drawDisplayStoreService.updateSelectedDrawing(this.gamenDto.selectedDrawingId);

  }

  /**
   * 図面一覧表示
   */
  async selectDraw() {
    const modal = await this.modalController.create({
      component: DrawSelectComponent,
      componentProps: {
        value: this.gamenDto.drawingInfoDto.drawings,
        selectedDrawingId: this.gamenDto.selectedDrawingId
      },
      cssClass: 'selectDrawModal-' + this.appStateService.getPlatform()
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // 図面はlabel出力選択可能な図面はlinkとする
    if (data != null) {
      this.gamenDto.selectedDrawingId = data;
      this.selectedDrawingChange();
    }
  }

  /**
   * 図面位置設定へ
   */
  goDrawLocation() {
    if (this.gamenDto.drawingInfoDto.drawings && this.gamenDto.drawingInfoDto.drawings.length > 0) {
      this.drawDisplayStoreService.updateList(this.gamenDto.drawingInfoDto, this.gamenDto.pinInfo, this.gamenDto.selectedDrawingId);
      this.pdfFileImage = '';
      this.routerService.goRouteLink('/draw-location');
    }
  }

  ngOnDestroy(): void {
    this.pdfFileImage = '';
    this.events.unsubscribe('pdfDownloadSuccess');
    console.log('drawdisplay----------- ngOnDestroy');
  }

  /**
   * PDFでDropされる
   */
  pinAllowDrop($event) {
    $event.stopPropagation();
  }

  /**
   * ピンクリック
   */
  async onPinClick(event, pin: PinDto) {
    event.stopPropagation();
    if (this.isPc) {
      this.appLoadingStoreService.isloading(true);
    }
    const floorName = this.gamenDto.selectedDrawing.floor.floorName;
    let titleName: string = (floorName ?  floorName + '・' : '')
                            + (pin.machine.roomName ? pin.machine.roomName + '・' : '')
                            + (pin.machine.placeName ? pin.machine.placeName + '・' : '')
                            + (pin.machine.unitMark || '');
    if (titleName && titleName.substr(titleName.length - 1, 1) === '・') {
      titleName = titleName.substring(0, titleName.length - 1);
    }
    titleName += pin.machine.machineModel ? '(' + pin.machine.machineModel + ')' : '';
    const modal = await this.modalController.create({
      component: PhotoSimpleGridComponent,
      componentProps: {
        title: titleName,
        machineId: pin.machine.machineId,
        floorId: this.gamenDto.selectedDrawing.floor.floorId,
        roomId: pin.machine.roomId,
        placeId: pin.machine.placeId,
      },
      cssClass: this.platformtype === 'drawDisplay-mobile' ? 'draw-photo' : 'draw-photo-pc'
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();

  }

  async refreshPdfSrc(buildingId: number, floorId: number) {
    const selectedDrawing = this.gamenDto.selectedDrawing;
    if (buildingId === this.commonStoreService.commonStore.buildingId && floorId === selectedDrawing.floorId) {
      const dbDraw = await this.dbDrawService.selectdraw(this.commonStoreService.commonStore.buildingId, selectedDrawing.floorId);
      if (dbDraw) {
        const filePath = this.file.dataDirectory + dbDraw[0].filePathLocal;
        this.pdfFileImage = this.webview.convertFileSrc(filePath);
      } else {
        this.pdfFileImage = selectedDrawing.fileImage;
      }
    }
  }

  drawerMousedown(event) {
    this.screenX = event.screenX;
    this.screenY = event.screenY;
    if (this.matDrawerContainer && this.matDrawerContainer.first) {
      const matDrawerContainer: MatDrawerContainer = this.matDrawerContainer.first;
      const elementRef = 'elementRef';
      this.matDrawerScroll = matDrawerContainer._content[elementRef].nativeElement;
    }
  }

  @HostListener('window:mouseup', ['$event'])
  onMouseup() {
    this.screenX = null;
    this.screenY = null;
    this.matDrawerScroll = null;
  }


  @HostListener('window:mousemove', ['$event'])
  onMouseMove(event) {
    if (this.screenX != null && this.screenY != null && this.matDrawerScroll) {
      this.matDrawerScroll.scrollLeft += this.screenX - event.screenX;
      this.matDrawerScroll.scrollTop += this.screenY - event.screenY;
      this.screenX = event.screenX;
      this.screenY = event.screenY;
    }
  }
}
