import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { S0201Constants } from '@constant/constant.photo';
import { RoutingService } from '@service/app-route.service';
import {
  AppState,
  AppStateStoreService,
  CommonStore,
  CommonStoreService,
  OfflineStore,
  OfflineStoreService,
  CameraStoreService
} from '@store';
import { OfflineAPIService } from '@service/offline/offline-api.service';
import { Store, select } from '@ngrx/store';
import { CD0001Constants } from '@constant/code';
import { PopoverController, Platform } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';

@Component({
  selector: 'app-big-process',
  templateUrl: './big-process.page.html',
  styleUrls: ['./big-process.page.mobile.scss', './big-process.page.winapp.scss']
})

export class BigProcessPage implements OnInit, OnDestroy {

  title: string = S0201Constants.TITLE;
  processes = [
    { name: CD0001Constants.CD0001_01_VALUE, code: CD0001Constants.CD0001_01, img: './assets/icon/search_icon.svg' },
    { name: CD0001Constants.CD0001_02_VALUE, code: CD0001Constants.CD0001_02, img: './assets/icon/location_icon.svg' },
    { name: CD0001Constants.CD0001_03_VALUE, code: CD0001Constants.CD0001_03, img: './assets/icon/install_icon.svg' },
    { name: CD0001Constants.CD0001_04_VALUE, code: CD0001Constants.CD0001_04, img: './assets/icon/service_icon.svg' },

  ];

  commonStore: CommonStore;
  commonStoreSubscription: Subscription;
  offlineStore: OfflineStore;
  offlineStoreSubscription: Subscription;
  apiStartDonwload = false;
  appState: AppState;
  platformtype: string;

  // 物件名
  buildingName: string;
  // 案件名
  projectName: string;

  constructor(
    private routerService: RoutingService,
    private commonStoreService: CommonStoreService,
    private cameraStoreService: CameraStoreService,
    private offlineStoreService: OfflineStoreService,
    private offlineAPIService: OfflineAPIService,
    public popoverController: PopoverController,
    private appStateService: AppStateStoreService,
    private storeCommon: Store<{ commonStore: CommonStore }>,
    private platform: Platform) {

    this.platformtype = 'bigProcess-' + this.appStateService.getPlatform();

  }

  ngOnInit() {

    console.log('bigProcesspage ngOnInit');
    this.commonStore = this.commonStoreService.commonStore;
    this.subscribeOfflineData();

  }

  ionViewWillEnter() {
    this.cameraStoreService.clearCameraStore();
    this.commonStoreService.resetAutoUpload();
  }

  ionViewDidEnter() {
    this.buildingName = this.commonStore.buildingName;
    this.projectName = this.commonStore.projectName;
  }

  subscribeOfflineData() {

    // offline data request完了しているか
    this.offlineStoreSubscription = this.offlineStoreService.offlineStoreSubscription(subscribestore => {
      this.offlineStore = subscribestore;
      console.log('this.offlineStore', this.offlineStore);
      if (this.offlineStore.APIAllDownload && this.routerService.getUrl() === '/bigProcess') {
        this.offlineStoreService.setAPIAllDownload(false);
        this.routerService.goRouteLink('/photo-list');
      }

    });

    // commostore 大工程bigProcessId
    this.commonStoreSubscription = this.commonStoreService.commonStoreSubscription(subscribestore => {
      this.commonStore = subscribestore;
      console.log('this.commonStore', this.commonStore, this.apiStartDonwload);
      if (this.commonStore.bigProcessId && this.apiStartDonwload) {
        this.apiStartDonwload = false;
        console.log('queryOfflineData', this.commonStore, this.apiStartDonwload);
        this.offlineAPIService.queryOfflineData();
      }

    });



  }

  ngOnDestroy() {
    console.log('bigProcesspage ngOnDestroy');
    this.offlineStoreSubscription.unsubscribe();
    this.commonStoreSubscription.unsubscribe();
  }


  clickItem(bigCode: string, bigName: string): void {
    // セッションを大工程コードに設定
    // セッションを大工程名に設定
    this.apiStartDonwload = true;
    this.commonStore.bigProcessId = bigCode;
    this.commonStore.bigProcessName = bigName;
    this.commonStoreService.setCommon(this.commonStore);
    console.log('----------------------------bigCode---------------------------', bigCode);

  }

  async onPressBuilding(ev: Event) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPressBuilding', ev);
    const messageArr1 = [this.commonStore.buildingName, this.commonStore.projectName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

}
