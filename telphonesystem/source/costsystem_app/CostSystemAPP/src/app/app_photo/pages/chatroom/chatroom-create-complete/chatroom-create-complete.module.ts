import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomCreateCompletePage } from './chatroom-create-complete.page';

@NgModule({
  providers: [],
  declarations: [ChatroomCreateCompletePage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomCreateCompletePage
      }
    ])
  ]
})
export class ChatroomCreateCompletePageModule { }
