import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthorySummaryPageRoutingModule } from './authory-summary-routing.module';

import { AuthorySummaryPage } from './authory-summary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthorySummaryPageRoutingModule
  ],
  declarations: [AuthorySummaryPage]
})
export class AuthorySummaryPageModule {}
