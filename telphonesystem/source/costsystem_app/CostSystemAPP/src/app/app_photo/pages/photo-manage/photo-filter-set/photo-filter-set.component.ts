import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ListConstants } from '@constant/code';
import { S0501Constants } from '../../../constant/constant.photo';
import { from } from 'rxjs';
import { AppStateStoreService } from '@store';

@Component({
  selector: 'app-photo-filter-set',
  templateUrl: './photo-filter-set.component.html',
  styleUrls: ['./photo-filter-set.component.scss', './photo-filter-set.mobile.scss', './photo-filter-set.winapp.scss'],
})
export class PhotoFilterSetComponent implements OnInit {

  public filmingstatus = S0501Constants.FILMINGSTATUS;
  public processtiming = S0501Constants.PROCESSTIMING;
  public ok = S0501Constants.DECISION;
  public cancleTitle = S0501Constants.CANCLE;

  title: string;
  status: any[];
  filmeds: any[];
  bigProcessId: string;

  selectfilmeds: string[];
  selectstatus: string[];

  changefilmeds: string[];
  changestatus: string[];

  platformtype: string;

  constructor(private params: NavParams, private appStateService: AppStateStoreService) {

    this.title = S0501Constants.FILTERSETTING;

    this.selectfilmeds = params.data.filmeds;
    this.selectstatus = params.data.status;
    this.bigProcessId = params.data.bigProcessId;
    console.log(this.selectfilmeds);
    console.log(this.selectstatus);
    this.platformtype = 'filterset-' + this.appStateService.getPlatform();
  }

  ngOnInit() {
    const sta = [...ListConstants.CD0024_LIST];
    sta.splice(3, 3);
    this.status = sta;
    console.log(this.status);

    this.filmeds = ListConstants.CDFILMED_LIST;

    this.changefilmeds = [...this.selectfilmeds];
    this.changestatus = [...this.selectstatus];
  }

  timingClick(ev) {

    const id = ev.detail.value;

    const index = this.changestatus.indexOf(id);
    if (index > -1) {
      this.changestatus.splice(index, 1);
    } else {
      this.changestatus.push(id);
    }
    console.log(this.changestatus);
  }

  photoStatusClick(ev) {
    console.log(ev);
    const id = ev.detail.value;
    const index = this.changefilmeds.indexOf(id);
    if (index > -1) {
      this.changefilmeds.splice(index, 1);
    } else {
      this.changefilmeds.push(id);
    }
    console.log(this.changefilmeds);
  }



  commit(): void {
    this.params.data.modal.dismiss({ filmed: this.changefilmeds, status: this.changestatus });
  }

  cancle(): void {
    this.params.data.modal.dismiss(null);
  }

}

