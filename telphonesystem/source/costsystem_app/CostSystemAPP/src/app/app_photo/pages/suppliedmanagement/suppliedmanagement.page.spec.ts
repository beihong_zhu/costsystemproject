import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuppliedmanagementPage } from './suppliedmanagement.page';

describe('SuppliedmanagementPage', () => {
  let component: SuppliedmanagementPage;
  let fixture: ComponentFixture<SuppliedmanagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuppliedmanagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuppliedmanagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
