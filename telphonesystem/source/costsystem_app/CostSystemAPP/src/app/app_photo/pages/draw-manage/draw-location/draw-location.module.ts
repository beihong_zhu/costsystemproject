import { NgModule } from '@angular/core';
import { DrawLocationPage } from './draw-location.page';
import { PinContextMenuComponent } from './pin-context-menu/pin-context-menu.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { PdfViewerModule } from '../../../components/ng2-pdf-viewer/pdf-viewer.module';

@NgModule({
  providers: [],
  declarations: [DrawLocationPage, PinContextMenuComponent],
  imports: [
    SharedModule,
    PdfViewerModule,
    RouterModule.forChild([
      {
        path: '',
        component: DrawLocationPage
      }
    ])
  ],
  entryComponents: [PinContextMenuComponent]
})
export class DrawLocationModule { }
