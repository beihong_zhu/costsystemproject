import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegularinfoPageRoutingModule } from './regularinfo-routing.module';

import { RegularinfoPage } from './regularinfo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegularinfoPageRoutingModule
  ],
  declarations: [RegularinfoPage]
})
export class RegularinfoPageModule {}
