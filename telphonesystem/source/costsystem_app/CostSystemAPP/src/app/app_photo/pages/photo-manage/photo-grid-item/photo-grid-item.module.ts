import { NgModule } from '@angular/core';
import { PhotoGridItemComponent } from './photo-grid-item.component';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { AppStateStoreService, CommonStoreService } from '@store';



@NgModule({
  providers: [
    AppStateStoreService,
    CommonStoreService,

  ],
  declarations: [

    // PhotoGridItemComponent,

  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoGridItemComponent
      }
    ])
  ],
  entryComponents: [],
})
export class PhotoGridItemModule { }
