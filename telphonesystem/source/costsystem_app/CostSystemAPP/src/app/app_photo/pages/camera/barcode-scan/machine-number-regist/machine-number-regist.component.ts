import { Component, OnInit, Input } from '@angular/core';
import { S0401Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { RoutingService } from '@service/app-route.service';
import { NavParams, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { IF0110InDto, IF0110OutDto } from '@service/dto';
import { AppStateStoreService, DrawOfflineStoreService, CommonStoreService, PhotomanagestoreService, OfflineUnitsService } from '@store';
import { IF0110Service } from '@service/api';
import { ResultType } from '../../../../../app_common/constant/common';
import { DbPhotoService } from '@service/db/db-photoservice';
import { TextCheck } from '../../../../util/text-check';
import { DbUnitsService } from '@service/db/db-unitsservice';

/**
 * 図面選択ダイアログ（図面一覧表示）
 */
@Component({
  selector: 'app-machine-number-regist',
  templateUrl: './machine-number-regist.component.html',
  styleUrls: ['./machine-number-regist.component.scss', './machine-number-regist.winapp.scss'],
})
export class MachineNumberRegistComponent implements OnInit {

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    regist: S0401Constants.REGIST,
    retakePhoto: S0401Constants.RETAKEPHOTO,
    productionNumber: S0401Constants.PRODUCTIONNUMBER,
    productionYearMonth: S0401Constants.PRODUCTIONYEARMONTH,
    machineName: S0401Constants.MACINENAME,
    buildingName: S0401Constants.BUILDINGNAME,
    constructionName: S0401Constants.CONSTRUCTIONNAME,
    floorTitle: S0401Constants.FLOORTITLE,
    roomName: S0401Constants.ROOMNAME,
    placeTitle: S0401Constants.PLACETITLE,
    unitMark: S0401Constants.UNITMARK,
    machineNameTitle: S0401Constants.MACINENAMETITLE
  };

  /**
   * 画面項目情報取得
   */
  public gamenValue = {
    ocrReadingModelName: sessionStorage.getItem('machineModel'),
    orcUnitMark: sessionStorage.getItem('unitMark'),
    ocrPropertyName: sessionStorage.getItem('buildingName'),
    ocrConstructionName: sessionStorage.getItem('projectName'),
    ocrFloor: sessionStorage.getItem('floorName'),
    ocrRoomName: sessionStorage.getItem('roomName') === 'null' ? '' : sessionStorage.getItem('roomName'),
    ocrPlace: sessionStorage.getItem('placeName') === 'null' ? '' : sessionStorage.getItem('placeName')
  };

  /**
   * 登録情報項目
   */
  public ocrSerialNumber = '';
  public ocrModelName = '';
  public ocrUnitId = Number(sessionStorage.getItem('machineId'));
  public ocrManufacturingDate = sessionStorage.getItem('ocrManufacturingDate');

  // エラーメッセージ
  public errorMessage: string;

  // 読み込み確認メッセージ
  public messageShow: string;

  platformtype: string;

  // true：QR読取、false：手動登録
  @Input() public isQR = true;
  @Input() public qrCodeValue = '';

  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    public storage: Storage,
    public routingService: RoutingService,
    public appStateStoreService: AppStateStoreService,
    private dbUnitsService: DbUnitsService,
    public if0110Service: IF0110Service,
    public drawOfflineStoreService: DrawOfflineStoreService,
    public commonStoreService: CommonStoreService,
    public photomanagestoreService: PhotomanagestoreService,
    public dbPhotoService: DbPhotoService,
    public offlineUnitsService: OfflineUnitsService) {
    this.platformtype = 'machine-number-regist-' + this.appStateStoreService.getPlatform();
  }

  ngOnInit() {
    if (this.isQR) {
      // QR読み取り場合
      const pramList = this.splitQR(this.qrCodeValue);

      if (pramList.length === 2) {
        this.ocrModelName = pramList[0];
        this.ocrSerialNumber = pramList[1];
        if (pramList[0] !== '' && pramList[1] !== '') {
          // すべての項目を認識できた場合
          this.messageShow = MessageConstants.I00010;
        } else {
          // 一部の項目を認識できた場合
          this.messageShow = MessageConstants.I00012;
        }
      } else {
        // すべての項目を認識できなかった場合
        this.messageShow = MessageConstants.I00012;
      }
    } else {
      // 手動登録場合
      this.messageShow = MessageConstants.I00013;
      this.ocrManufacturingDate = '';
    }
  }

  /**
   * 登録ボタンをクリックすること。
   */
  async regist() {
    // 製造年月の入力値整形
    if (this.ocrManufacturingDate != null && this.ocrManufacturingDate.length > 5) {
      const str = this.ocrManufacturingDate.substr(2, 5);
      this.ocrManufacturingDate = str.replace('-', '/');
    }

    // 入力チェック処理
    this.errorMessage = this.inputCheck();
    if (this.errorMessage !== '') {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: this.errorMessage,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    // オンラインの場合
    if (!this.appStateStoreService.appState.isOffline) {
      if (this.ocrModelName !== this.gamenValue.ocrReadingModelName) {
        // セッションの機種名と入力された機種名が異なる場合
        const alert = await this.alertController.create({
          header: AlertWindowConstants.CONFIRMTITLE,
          message: MessageConstants.C00007,
          buttons: [{
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              // 登録処理を行う。
              this.updateIF0110(this.getIF0110InDto());
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
          ],
          backdropDismiss: false
        });
        await alert.present();
      } else {
        // 登録処理
        this.updateIF0110(this.getIF0110InDto());
      }
    } else {
      // offlineの場合、sqlliteに保存
      const inDto = this.getIF0110InDto();
      this.dbUnitsService.saveunits(inDto.unitId, inDto.modeltype, inDto.serialNumber, inDto.productionDate, 0);
      // storeで更新か新規
      this.offlineUnitsService.pushUnit(inDto);
      this.updateStoreMachineInfo(inDto);
      this.gamenValue.ocrReadingModelName = inDto.modeltype;
      sessionStorage.setItem('machineModel', this.gamenValue.ocrReadingModelName);
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: MessageConstants.I00027,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }
  }

  /**
   * 撮り直しボタンをクリックすること。
   */
  retakePhoto(): void {
    if (this.isQR) {
      if (this.ocrManufacturingDate != null) {
        sessionStorage.setItem('ocrManufacturingDate', this.ocrManufacturingDate);
      } else {
        sessionStorage.removeItem('ocrManufacturingDate');
      }
    }
    this.navParams.data.modal.dismiss({ close: false });
  }

  /**
   *  入力チェック
   */
  inputCheck(): string {
    let errorE00001 = false;
    let error = '';

    if (this.ocrSerialNumber !== null && this.ocrSerialNumber.trim() !== '') {
      if (this.ocrSerialNumber.length > 10) {
        // 桁数チェック
        error = MessageConstants.E00007.replace('{{vaule}}', '10');
      } else if (!TextCheck.textCheck(this.ocrSerialNumber)) {
        if (error !== '') {
          error = error + '\r\n' + MessageConstants.E00008;
        } else {
          error = MessageConstants.E00008;
        }
      } else if (!this.ocrSerialNumber.match('^[ -~]+$') || !TextCheck.regCheckHalf(this.ocrSerialNumber)) {
        // 形式チェック
        error = MessageConstants.E00009.replace('{{value}}', this.gamenLable.productionNumber);
      }


    } else {
      // 必須チェック
      error = MessageConstants.E00001;
      errorE00001 = true;
    }

    if (this.ocrModelName != null && this.ocrModelName.trim() !== '') {
      if (this.ocrModelName.length > 100) {
        // 桁数チェック
        if (error !== '') {
          error = error + '\r\n' + MessageConstants.E00007.replace('{{vaule}}', '100');
        } else {
          error = MessageConstants.E00007.replace('{{vaule}}', '100');
        }
      } else if (!TextCheck.textCheck(this.ocrModelName)) {
        if (error !== '') {
          error = error + '\r\n' + MessageConstants.E00008;
        } else {
          error = MessageConstants.E00008;
        }
      } else if (!this.ocrModelName.match('^[ -~]+$') || !TextCheck.regCheckHalf(this.ocrModelName)) {
        // 形式チェック
        if (error !== '') {
          error = error + '\r\n' + MessageConstants.E00009.replace('{{value}}', this.gamenLable.machineName);
        } else {
          error = MessageConstants.E00009.replace('{{value}}', this.gamenLable.machineName);
        }
      }





    } else if (!errorE00001) {
      // 必須チェック
      if (error !== '') {
        error = error + '\r\n' + MessageConstants.E00001;
      } else {
        error = MessageConstants.E00001;
      }
      errorE00001 = true;
    }

    if (this.ocrManufacturingDate != null && this.ocrManufacturingDate !== '') {
      if (this.ocrManufacturingDate.length > 5) {
        // 桁数チェック
        if (error !== '') {
          error = error + '\r\n' + MessageConstants.E00007.replace('{{vaule}}', '5');
        } else {
          error = MessageConstants.E00007.replace('{{vaule}}', '5');
        }
      } else if (!this.ocrManufacturingDate.match('^([0-9]{2})/(0[1-9]|1[0-2])$')) {
        // 形式チェック
        if (error !== '') {
          error = error + '\r\n' + MessageConstants.E00009.replace('{{value}}', this.gamenLable.productionYearMonth);
        } else {
          error = MessageConstants.E00009.replace('{{value}}', this.gamenLable.productionYearMonth);
        }
      }
    } else if (!errorE00001) {
      // 必須チェック
      if (error !== '') {
        error = error + '\r\n' + MessageConstants.E00001;
      } else {
        error = MessageConstants.E00001;
      }
      errorE00001 = true;
    }

    return error;
  }

  private splitQR(qrCodeValue): string[] {
    const array = qrCodeValue.split('/?id1=');

    let idList = [];
    if (array.length === 2) {
      const tmpIdList = array[1].split('&id2=');
      if (tmpIdList.length === 2) {
        idList = tmpIdList;
      } else {
        idList[0] = tmpIdList[0];
        idList[1] = '';
      }
    } else {
      const tmpIdList = qrCodeValue.split('/?id2=');
      if (tmpIdList.length === 2) {
        idList[0] = '';
        idList[1] = tmpIdList[1];
      }
    }

    return idList;
  }

  // 機器情報登録・更新処理
  private async updateIF0110(inDto: IF0110InDto) {
    this.if0110Service.postExec(inDto, (outDto: IF0110OutDto) => {
      this.if0110Successed(inDto, outDto);
    },
      () => {
        this.navParams.data.modal.dismiss();
      });
  }

  // リクエストデータ
  private getIF0110InDto() {
    const inDto: IF0110InDto = {};
    inDto.unitId = this.ocrUnitId;
    inDto.modeltype = this.ocrModelName.trim();
    inDto.serialNumber = this.ocrSerialNumber.trim();
    inDto.productionDate = this.ocrManufacturingDate;
    return inDto;
  }

  private async if0110Successed(inDto: IF0110InDto, outDto: IF0110OutDto) {

    if (outDto.resultCode === ResultType.NORMAL) {
      this.updateStoreMachineInfo(inDto);

      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: MessageConstants.I00014,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss({ close: true });
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    } else {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00083,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss({ close: false });
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  updateStoreMachineInfo(inDto: IF0110InDto) {

    this.commonStoreService.updateMachineInfo({
      machineId: inDto.unitId,
      machineNumber: inDto.serialNumber,
      machineModel: inDto.modeltype
    });

    this.photomanagestoreService.updateMachineInfo({
      machineId: inDto.unitId,
      machineNumber: inDto.serialNumber,
      machineModel: inDto.modeltype
    });

    this.dbPhotoService.updatePhotoModeltype(inDto.unitId, inDto.modeltype);
  }

}
