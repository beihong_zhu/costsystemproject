import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { CameraStoreService, CameraStore,AppStateStoreService } from '@store';
import { AlertWindowConstants, S0301Constants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { TextCheck } from '../../../util/text-check';

@Component({
  selector: 'camera-modal-edit',
  templateUrl: './camera-modal-edit.component.html',
  styleUrls: ['./camera-modal-edit.component.scss'],
})
export class CameraModalEditComponent implements OnInit {

  note = '';
  currentWitness = null;
  cameraStore: CameraStore;
  nullcurrentWitness = null;
  platformtype: string;

  public defaultWitnessName = S0301Constants.DEFAULTWITNESSNAME;

  constructor(public alertController: AlertController,
              public modalController: ModalController,
              private cameraStoreService: CameraStoreService,
              private appStateService: AppStateStoreService) {
    this.cameraStore = this.cameraStoreService.cameraStore;
    this.note = this.cameraStoreService.cameraStore.free;
    this.currentWitness = this.cameraStoreService.cameraStore.currentWitness;
    this.platformtype = 'camera-modal-edit-' + this.appStateService.getPlatform();
  }
  selectWitness(event) {
    const data = event.detail.value;
    this.currentWitness = data || null;

  }

  ngOnInit() {
  }
  notefocus() {
    document.getElementById('input-title').style.color = '#0097E0';
    document.getElementById('div-input').style.borderColor = '#0097E0';
  }
  noteblur() {
    document.getElementById('input-title').style.color = '#333333';
    document.getElementById('div-input').style.borderColor = '#cccccc';
  }
  cancel() {
    this.modalController.dismiss();
  }

  async ok() {
    if (this.note !== null && this.note.length > 100) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00007.replace('{{vaule}}', '100'),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }


    if (this.note !== null && !TextCheck.textCheck(this.note)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }


    this.cameraStoreService.setFree(this.note);
    this.cameraStoreService.setCurrentWitness(this.currentWitness);
    this.modalController.dismiss();
  }

}
