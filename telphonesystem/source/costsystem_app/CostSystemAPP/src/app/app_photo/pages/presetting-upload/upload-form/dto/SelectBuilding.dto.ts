export class SelectBuildingDto {
  // ID
  buildingId?: string;
  // 名称
  buildingName?: string;
  addressPrefecture?: string;
  addressCity?: string;
  addressNumber?: string;
  code?: string;
  name?: string;
}
