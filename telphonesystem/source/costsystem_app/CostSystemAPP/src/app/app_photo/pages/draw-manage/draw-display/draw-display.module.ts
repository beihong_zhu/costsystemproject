import { NgModule } from '@angular/core';
import { DrawDisplayPage } from './draw-display.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { PdfViewerModule } from '../../../components/ng2-pdf-viewer/pdf-viewer.module';
import { DrawSelectComponent } from './draw-select/draw-select.component';
import { DrawOfflineReducer } from '@store';
import { StoreModule } from '@ngrx/store';
import { PhotoSimpleGridComponent } from '../draw-photo/photo-simple-grid/photo-simple-grid.component';
import { PhotoSimpleGridItemComponent } from '../draw-photo/photo-simple-grid-item/photo-simple-grid-item.component';

@NgModule({
  providers: [],
  declarations: [DrawDisplayPage, DrawSelectComponent, PhotoSimpleGridComponent, PhotoSimpleGridItemComponent],
  imports: [
    SharedModule,
    PdfViewerModule,
    RouterModule.forChild([
      {
        path: '',
        component: DrawDisplayPage
      }
    ]),
    StoreModule.forFeature('drawOfflineStore', DrawOfflineReducer)
  ],
  entryComponents: [DrawSelectComponent, PhotoSimpleGridComponent],
})
export class DrawDisplayModule { }
