import { Component, OnInit } from '@angular/core';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { API3008Service } from '@service/api';
import { API3008InDto, API3008OutDto } from '@service/dto';
import { SysZeroInfoDto } from './dto';

@Component({
  selector: 'app-zeroaccount',
  templateUrl: './zeroaccount.page.html',
  styleUrls: ['./zeroaccount.page.scss'],
})
export class ZeroaccountPage implements OnInit {

  public  zeroInfo:any = {
    zeroninfoList:['授信' ,'实付'],
    zero:'授信',
    statusList:['成功' ,'拒绝','撤销','待处理'],
    status : '成功'
   }

   public agencyname : string;

   public sysZeroInfoList: Array<SysZeroInfoDto> = new Array<SysZeroInfoDto>();

   constructor( private api3008Service: API3008Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api3008ServicePostExec();
  }

  
  private get3008InDto() {
    let api3008InDto: API3008InDto = {};
    api3008InDto.agencyname = this.agencyname;

    return api3008InDto;
  }

  private api3008ServicePostExec() {


    // 写真帳出力情報取得
    this.api3008Service.postExecNew(this.get3008InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3008successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api3008successed(outDto: API3008OutDto) {
    this.sysZeroInfoList = new Array<SysZeroInfoDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.zeroInfoModelResList));

    for (const sysZeroInfoModel of loadSysusers) {
      let sysZeroInfoDto: SysZeroInfoDto = new SysZeroInfoDto();
      sysZeroInfoDto.agencyname = sysZeroInfoModel.agencyname;
      sysZeroInfoDto.capital = sysZeroInfoModel.capital;
      sysZeroInfoDto.capitalStatus = sysZeroInfoModel.capitalStatus;
      sysZeroInfoDto.createdDatetime= sysZeroInfoModel.createdDatetime;
    
      this.sysZeroInfoList.push(sysZeroInfoDto);
    }
  }



  

}
