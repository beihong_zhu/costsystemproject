import { Component, OnInit } from '@angular/core';
import { S0901Constants, AlertWindowConstants, PageId } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { ListConstants, CD0011Constants, CD0012Constants, SORTBY } from '@constant/code';
import { Platform, AlertController } from '@ionic/angular';
import { API0801Service, API0903Service } from '@service/api';
import { API0801InDto, API0801OutDto, API0903InDto, API0903OutDto } from '@service/dto';
import { RoutingService } from '@service/app-route.service';
import {
  CommonStore,
  PhotobookInfoStore,
  CommonStoreService,
  AppStateStoreService,
  PhotobookInfoSetStoreService,
  PhotobookViewSetStoreService
} from '@store';
import { PhotobookInfo } from '../../../../app_photo/store/photobook-info-set/photobook-info-set.store';
import { ResultType, UpdateMessage } from '../../../../app_common/constant/common';
import * as _ from 'lodash';
import { TextCheck } from '../../../../app_photo/util/text-check';
import { ProcessIdDto } from '@service/dto/common/dto/processid-dto';

@Component({
  selector: 'app-photobook-info-set',
  templateUrl: './photobook-info-set.page.html',
  styleUrls: ['./photobook-info-set.page.scss', './photobook-info-set.page.mobile.scss', './photobook-info-set.page.winapp.scss'],
})
export class PhotobookInfoSetPage implements OnInit {
  platformtype: string;
  // Pc又はスマホ
  isPc: boolean;

  // 画面タイトル
  title: string = S0901Constants.TITLE;
  // 大工程
  bigProcesses: CodeDto[];
  // 共通ストア
  commonStore: CommonStore;
  // 出力情報ストア
  photobookInfoStore: PhotobookInfoStore;
  // 選択した工程
  processChks: ProcessChk[] = [];
  // 選択物件名
  chooseBuildingName: string;
  // 選択工事名称
  chooseConstructionName: string;
  // 選択大工程名
  chooseLargeProcessName: string;
  // ページの出力数
  pageCnt: string;
  // 大工程選択
  selectBigProcess: string;
  // 全体出力順
  outPutOrder: string;
  // 不明
  unknownProcessOutput: boolean;
  // 全て
  selectedAll: boolean;

  /**
   * 画面項目名称取得
   */
  gamenLable = {
    screenGuide: S0901Constants.SCREENGUIDE,
    basicInformation: S0901Constants.BASICINFORMATION,
    buildingName: S0901Constants.BUILDINGNAME,
    constructionName: S0901Constants.CONSTRUCTIONNAME,
    largeProcessName: S0901Constants.LARGEPROCESSNAME,
    outputProcess: S0901Constants.OUTPUTPROCESS,
    outputProcessGuide1: S0901Constants.OUTPUTPROCESSGUIDE1,
    outputProcessGuide2: S0901Constants.OUTPUTPROCESSGUIDE2,
    overallOutputOrder: S0901Constants.OVERALLOUTPUTORDER,
    overallOutputOrderGuide1: S0901Constants.OVERALLOUTPUTORDERGUIDE1,
    overallOutputOrderGuide2: S0901Constants.OVERALLOUTPUTORDERGUIDE2,
    processPriority: S0901Constants.PROCESSPRIORITY,
    floorPriority: S0901Constants.FLOORPRIORITY,
    outputPerPage: S0901Constants.OUTPUTPERPAGE,
    isMobile: S0901Constants.ISMOBILE,
    moveOn: S0901Constants.MOVEON,
    moveOnArrow: S0901Constants.MOVEONARROW,
  };

  constructor(
    private platform: Platform,
    private routerService: RoutingService,
    private commonStoreService: CommonStoreService,
    public alertController: AlertController,
    public appStateService: AppStateStoreService,
    private photobookInfoSetStoreService: PhotobookInfoSetStoreService,
    public photobookViewSetStoreService: PhotobookViewSetStoreService,
    public api0801Service: API0801Service,
    public api0903Service: API0903Service,
  ) {
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
    this.platformtype = 'photobook-info-set-' + this.appStateService.getPlatform();

  }

  ngOnInit() {
    // APPストアに保存した写真帳出力情報を取得
    this.photobookInfoStore = this.photobookInfoSetStoreService.photobookInfoStore;

  }

  /**
   * 初期化する。
   */
  ionViewWillEnter() {
    const history = this.routerService.getHistory();
    if (history[history.length - 2] === '/privacy-policy') {
      // プライバシーポリシーから戻る時、写真帳画面の値を保持する
      return;
    }

    // 全体出力順
    this.outPutOrder = CD0011Constants.LAYOUT_ORDER1;
    // 1ページの表示数
    this.pageCnt = CD0012Constants.LAYOUT_NUMBER2;

    // 大工程選択
    this.bigProcesses = _.cloneDeep(ListConstants.CD0001_LIST);

    // PC又はスマホ
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.initData();
  }

  ionViewWillLeave() {
  }

  /**
   * 画面データを取得すること。
   */
  initData(): void {

    // 写真帳レイアウト設定画面からの復帰時は初期化は行わず、編集済み名称と選択済みの情報を出力する。
    if (this.photobookInfoStore && this.photobookInfoStore.pageId === PageId.S0902) {
      this.photobookInfoSetStoreService.setPhotobookInfo(this.photobookInfoStore);
      this.selectBigProcess = this.photobookInfoStore.chooseLargeProcessId;
      this.chooseBuildingName = this.photobookInfoStore.chooseBuildingName;
      this.chooseConstructionName = this.photobookInfoStore.chooseConstructionName;
      this.chooseLargeProcessName = this.photobookInfoStore.chooseLargeProcessName;
      this.processChks = this.photobookInfoStore.photobookInfos;
      this.outPutOrder = this.photobookInfoStore.outputOrder;
      this.pageCnt = this.photobookInfoStore.pageCnt;
      this.unknownProcessOutput = this.photobookInfoStore.unknownProcessOutput;
      this.initSelectedAll();
    } else {
      // その他画面より遷移した場合は初期表示とする。
      // 基本情報初期表示
      this.chooseBuildingName = this.commonStore.buildingName;
      this.chooseConstructionName = this.commonStore.projectName;
      if (this.isPc) {
        this.chooseLargeProcessName = this.bigProcesses[0].value;
        this.selectBigProcess = this.bigProcesses[0].id;
        this.processChks = [];
      } else {
        this.chooseLargeProcessName = this.commonStore.bigProcessName;
        this.selectBigProcess = this.commonStore.bigProcessId;
        this.processChks = [];
      }
      this.unknownProcessOutput = true;
      this.selectedAll = true;


      // 工程情報一覧(API-08-01)
      this.api0801Service.postExec(this.getProcessInDto(), (outDto: API0801OutDto) => {
        this.api0801successed(outDto);
      },
        // fault
        () => {
        });
    }
  }

  // 選択されている工程・作業に紐付く出力対象となる写真数を取得する処理
  async api0903successed(outDto: API0903OutDto) {
    let msg: string = '';
    msg = UpdateMessage.setMessage(outDto.messageCode, outDto.message);
    if (outDto.resultCode === ResultType.BUSINESSERROR) {
      await this.alertCall(msg);
    }
  }

  // 工程作業一覧取得処理
  async api0801successed(outDto: API0801OutDto) {
    // 工程情報取得
    const photobookInfos: PhotobookInfo[] = new Array();
    this.processChks = new Array();

    if (outDto.processList) {
      Array.from({ length: outDto.processList.length }).map((item, i) => {
        photobookInfos[i] = new PhotobookInfo();
        this.processChks[i] = {};

        photobookInfos[i].processId = outDto.processList[i].processId;
        photobookInfos[i].processName = outDto.processList[i].processName;
        photobookInfos[i].displayOrder = outDto.processList[i].displayOrder;

        // 出力する工程
        this.processChks[i].processId = outDto.processList[i].processId;
        this.processChks[i].processName = outDto.processList[i].processName;
        this.processChks[i].displayOrder = outDto.processList[i].displayOrder;
        this.processChks[i].workList = outDto.processList[i].workList;
        this.processChks[i].ischeck = true;
        this.processChks[i].unknownWorkOutput = true;

        if (this.processChks[i].workList) {
          this.processChks[i].workList.map((item => {
            item.ischeck = true;
          }));
        }
      });
    }
  }

  // アラートポップアップ
  async alertCall(msg: string) {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.ERRORTITLE,
      message: msg,
      buttons: [AlertWindowConstants.OKENGLISHBUTTON]
    });
    await alert.present();
  }

  // 大工程選択
  codeChange(selectBigProcess: string) {

    Array.from({ length: this.bigProcesses.length }).map((item, i) => {
      if (selectBigProcess === this.bigProcesses[i].id) {
        this.chooseLargeProcessName = this.bigProcesses[i].value;
      }
    });

    this.unknownProcessOutput = true;
    this.selectedAll = true;


    // 工程情報一覧(API-08-01)
    this.api0801Service.postExec(this.getProcessInDto(), (outDto: API0801OutDto) => {
      this.api0801successed(outDto);
    },
      // fault
      () => {
      });
  }

  // 工程情報一覧InDto内容を作成
  getProcessInDto(): API0801InDto {

    const inDto: API0801InDto = {};
    inDto.projectId = this.commonStore.projectId;
    inDto.bigProcessId = this.selectBigProcess;

    return inDto;
  }

  // 写真帳出力対象数取得InDto内容を作成
  getPhotoInDto(processIdList: Array<ProcessIdDto>): API0903InDto {

    const inDto: API0903InDto = {};
    inDto.buildingId = this.commonStore.buildingId;
    inDto.projectId = this.commonStore.projectId;
    inDto.bigProcessId = this.selectBigProcess;
    inDto.unknownProcessOutput = this.unknownProcessOutput ? 1 : 0;
    inDto.processIdList = processIdList;

    return inDto;
  }

  // 写真帳レイアウト設定画面に遷移
  async nextPage() {
    // 写真帳出力情報は、ストアに保存する
    if (this.chooseBuildingName.trim().length <= 0 || this.chooseConstructionName.trim().length <= 0
      || this.chooseLargeProcessName.trim().length <= 0) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00001,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON]
      });
      await alert.present();
      return;
    }

    if (!TextCheck.textCheck(this.chooseBuildingName) || !TextCheck.textCheck(this.chooseConstructionName)
      || !TextCheck.textCheck(this.chooseLargeProcessName)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON]
      });
      await alert.present();
      return;
    }

    const photobookInfos = new Array();
    for (const processItem of this.processChks) {
      if (processItem.ischeck) {
          photobookInfos.push(processItem.processId);
      }
    }

    // 出力する工程が選択されていない場合は、エラーメッセージ(E00036)をポップアップ出力し、画面遷移を行わない
    if (photobookInfos.length === 0 && !this.unknownProcessOutput) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00036,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON]
      });
      await alert.present();
      return;
    }

    // 選択されている工程・作業に紐付く出力対象となる写真数を取得し、写真数が2000枚を超える場合、または写真数が0枚の場合はエラーとしエラーメッセージを出力し、画面遷移を行わない
    // 工程IDリスト設定
    const processIdList: Array<ProcessIdDto> = new Array();
    let j: number = 0;
    Array.from({ length: this.processChks.length }).map((item, i) => {
      if (this.processChks[i]) {
        if (this.processChks[i].ischeck) {
          processIdList[j] = new ProcessIdDto();
          processIdList[j].processId = this.processChks[i].processId;
          processIdList[j].unknownWorkOutput = this.processChks[i].unknownWorkOutput ? 1 : 0;
          if (this.processChks[i].workList) {
            processIdList[j].workIdList = new Array();
            for (const work of this.processChks[i].workList) {
              if (work.ischeck) {
                processIdList[j].workIdList.push({ workId: work.workId });
              }
            }
          }
          j++;
        }
      }
    });

    // 工程と作業に紐付く写真帳出力数の取得(写真帳出力対象数取得(API-09-03))
    this.api0903Service.postExec(this.getPhotoInDto(processIdList), (outDto: API0903OutDto) => {
      this.api0903successed(outDto);
      if (outDto.resultCode !== ResultType.NORMAL) {
        return;
      } else {
        this.photobookInfoStore.unknownProcessOutput = this.unknownProcessOutput;
        this.photobookInfoStore.photobookInfos = this.processChks;
        this.photobookInfoStore.chooseBuildingName = this.chooseBuildingName.trim();
        this.photobookInfoStore.chooseConstructionName = this.chooseConstructionName.trim();
        this.photobookInfoStore.chooseLargeProcessId = this.selectBigProcess;
        this.photobookInfoStore.chooseLargeProcessName = this.chooseLargeProcessName.trim();
        this.photobookInfoStore.outputOrder = this.outPutOrder;
        this.photobookInfoStore.pageCnt = this.pageCnt;
        this.photobookInfoStore.pageId = PageId.S0902;
        // 画面情報を作成
        this.photobookInfoSetStoreService.setPhotobookInfo(this.photobookInfoStore);

        const url = '/photobook-view-set';
        this.routerService.goRouteLink(url);
      }
    },
      // fault
      () => {
      });
  }

  /**
   * 工程チェックボックスがクリックされること。
   * @param process 工程DTO
   */
  processChkClick(process: ProcessChk, isCheckbox): void {
    if (!isCheckbox) {
      process.ischeck = !process.ischeck;
    }
    process.unknownWorkOutput = process.ischeck;
    const works = process.workList;
    // 該当工程で作業の存在すること判断
    if (works) {
      works.map((item => {
        item.ischeck = process.ischeck;
      }));
    }
    this.initSelectedAll();
  }

  /**
   * 工程内容展開すること。
   * @param process 工程DTO
   */
  expandChange(process: ProcessChk): void {
    process.isOpen = !process.isOpen;
  }

  /**
   * 作業チェックボックスがクリックされること。
   */
  workChkClick(process: ProcessChk): void {
    process.ischeck = false;
    for (const work of process.workList) {
      if (work.ischeck) {
        process.ischeck = true;
        break;
      }
    }
    process.ischeck = process.ischeck || process.unknownWorkOutput;
    this.initSelectedAll();
  }

  /**
   * 選択クリアボタンをクリックされること。
   */
  selectedAllClick(): void {
    if (this.processChks && this.processChks.length > 0) {
      this.processChks.forEach((item) => {
        item.ischeck = this.selectedAll;
        item.unknownWorkOutput = this.selectedAll;
        if (item.workList && item.workList.length > 0) {
          item.workList.forEach((work) => {
            work.ischeck = this.selectedAll;
          });
        }
      });
    }
    this.unknownProcessOutput = this.selectedAll;
  }

  initSelectedAll() {
    let all: boolean = true;
    if (this.processChks && this.processChks.length > 0) {
      this.processChks.forEach((item) => {
        all = all && item.ischeck && item.unknownWorkOutput;
        if (item.workList && item.workList.length > 0) {
          item.workList.forEach((work) => {
            all = all && work.ischeck;
          });
        }
      });
    }
    this.selectedAll = all && this.unknownProcessOutput;
  }
}

export class CodeDto {
  // コードID
  id?: string;
  // コード名
  value?: string;
}

export class ProcessChk {
  processId?: number;
  processName?: string;
  displayOrder?: number;
  ischeck?: boolean;
  isOpen?: boolean;
  // 作業リスト
  workList?: Array<{
    // 作業ID
    workId?: number;
    // 作業名
    workName?: string;
    // 表示順
    displayOrder?: number;
    ischeck?: boolean;
  }>;
  // 不明作業出力フラグ
  unknownWorkOutput?: boolean;
}
