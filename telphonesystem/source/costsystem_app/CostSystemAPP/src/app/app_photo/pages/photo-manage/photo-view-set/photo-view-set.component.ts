import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ListConstants, CD0009Constants } from '@constant/code';
import { S0501Constants } from '../../../constant/constant.photo';
import { AppStateStoreService } from '@store';


@Component({
  selector: 'app-photo-view-set',
  templateUrl: './photo-view-set.component.html',
  styleUrls: ['./photo-view-set.component.scss', './photo-view-set.mobile.scss', './photo-view-set.winapp.scss'],
})
export class PhotoViewSetComponent implements OnInit {

  public ok = S0501Constants.DECISION;
  public cancleTitle = S0501Constants.CANCLE;
  public grid = S0501Constants.GRID;
  public list = S0501Constants.LIST;

  title: string;
  formart: string;
  display: any[];

  platformtype: string;

  constructor(private params: NavParams, private appStateService: AppStateStoreService) {

    this.title = S0501Constants.DISPLAYFORMATSETTING;
    this.formart = params.data.formart;
    this.display = ListConstants.CD0009_LIST;
    this.platformtype = 'viewset-' + this.appStateService.getPlatform();
  }

  ngOnInit() {
  }

  select1(): void {
    this.formart = CD0009Constants.GRID_SHOW;
  }
  select2(): void {
    this.formart = CD0009Constants.LIST_SHOW;
  }

  select(ev) {
    console.log(ev);
    this.formart = ev.detail.value;
  }




  commit(): void {

    this.params.data.modal.dismiss(this.formart);

  }

  cancle(): void {
    this.params.data.modal.dismiss(null);
  }

}
