import { Component, OnInit } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { WorkDto } from '@pages/process/process-edit/dto';
import { S0801Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0809Service } from '@service/api';
import { API0809InDto, API0809OutDto } from '@service/dto';
import { ResultType, UpdateMessage } from '../../../../../app_common/constant/common';
import { TextCheck } from '../../../../util/text-check';
import { AppStateStoreService, CommonStoreService } from '@store';


@Component({
  selector: 'app-work-rename',
  templateUrl: './work-rename.component.html',
  styleUrls: ['./work-rename.component.scss', './work-rename.component.mobile.scss'],
})

/**
 * 作業名変更ダイアログ
 */
export class WorkRenameComponent implements OnInit {
  isPc: boolean;
  public gamenDto: WorkDto = {};

  bigProcessId: string;
  processId: number;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    workChangeTitle: S0801Constants.WORKCHANGETITLE,
    changeName: S0801Constants.CHANGENAME,
    change: S0801Constants.CHANGE,
    cancel: S0801Constants.CANCEL,
    workMsg: S0801Constants.WORKMSG
  };

  /**
   * コンストラクタ
   * @param navParams 作業DTO
   * @param alertController AlertController
   * @param api0809Service 作業名変更API(API-08-09)
   */
  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    public api0809Service: API0809Service,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService
  ) {
    const workDto: WorkDto = navParams.data.value;
    this.gamenDto.workId = workDto.workId;
    this.gamenDto.workName = workDto.workName;
    this.isPc = this.appStateService.appState.isPC;

    this.bigProcessId = navParams.data.bigProcessId;
    this.processId = navParams.data.projectId;

    this.platformtype = 'workRename-' + this.appStateService.getPlatform();
  }

  /**
   * 初期化する。
   */
  ngOnInit() {
  }

  /**
   * クローズボタンをクリックすること。
   */
  closeClick() {
    this.navParams.data.modal.dismiss(null);
  }

  /**
   * 変更ボタンをクリックすること。
   */
  async updateClick() {

    if (!TextCheck.textCheck(this.gamenDto.workName)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    this.api0809Service.postExec(this.getEditInDto(), (outDto: API0809OutDto) => {
      this.editSuccessed(outDto);
    },
      // fault
      () => {
        this.navParams.data.modal.dismiss(null);
      });
  }
  /**
   * 変更成功、API0809返信。
   * @param outDto API0809OutDto
   */
  async editSuccessed(outDto: API0809OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: this.isPc ? MessageConstants.I00016 : MessageConstants.I00023,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss(this.gamenDto);
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else if (outDto.resultCode === ResultType.BUSINESSERROR) {
      // エラーメッセージ設定
      let msg: string = '';
      msg = UpdateMessage.setMessage(
        outDto.messageCode,
        outDto.message);
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss(this.gamenDto);
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    }
  }
  /**
   * API0809送信
   */
  getEditInDto(): API0809InDto {
    const workEditInDto: API0809InDto = {};
    workEditInDto.projectId = this.commonStoreService.commonStore.projectId;
    workEditInDto.bigProcessId = this.bigProcessId;
    workEditInDto.processId = this.processId;
    workEditInDto.workId = this.gamenDto.workId;
    workEditInDto.workName = this.gamenDto.workName.trim();
    // TODO processEditInDto.更新ユーザ
    return workEditInDto;
  }

  /**
   * キャンセルボタンをクリックすること。
   */
  cancelClick() {
    this.navParams.data.modal.dismiss(null);
  }

  /**
   * 作業名入力欄内に値が入力された場合、活性化。
   */
  isUpdated() {
    return this.gamenDto.workName === null || this.gamenDto.workName.trim() === '';
  }

  focusInWorkName() {
    document.getElementById('workName').style.color = '#0097E0';
  }
  focusOutWorkName() {
    document.getElementById('workName').style.color = '#333333';
  }
}
