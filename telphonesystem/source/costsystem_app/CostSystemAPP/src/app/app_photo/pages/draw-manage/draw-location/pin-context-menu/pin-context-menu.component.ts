import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { S0602Constants } from '@constant/constant.photo';
import { CommonStoreService } from '@store';
import { CD0015Constants } from '@constant/code';

/**
 * ピン編集メニュー　※図面上のピンをクリックする事で表示
 */
@Component({
  selector: 'app-pin-context-menu',
  templateUrl: './pin-context-menu.component.html',
  styleUrls: ['./pin-context-menu.component.scss'],
})
export class PinContextMenuComponent implements OnInit {

  canDelete = true;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    pinMove: S0602Constants.PINMOVE,
    pinDelete: S0602Constants.PINDELETE,
  };
  constructor(public popoverController: PopoverController,
              public commonStoreService: CommonStoreService) {
    // TODO※利用者(協力店 職長(主)・作業者(副))は非表示 ピンの削除
    if (this.commonStoreService.commonStore.authorId) {
      if (this.commonStoreService.commonStore.authorId === CD0015Constants.AUTHORITY04) {
        this.canDelete = false;
      }
    }
  }

  ngOnInit() { }

  /**
   * ピンの移動
   */
  movePin() {
    this.popoverController.dismiss('move');
  }

  /**
   * ピンの削除
   */
  deletePin() {
    this.popoverController.dismiss('delete');
  }

}
