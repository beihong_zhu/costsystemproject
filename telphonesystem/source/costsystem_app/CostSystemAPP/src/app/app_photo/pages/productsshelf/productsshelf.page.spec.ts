import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductsshelfPage } from './productsshelf.page';

describe('ProductsshelfPage', () => {
  let component: ProductsshelfPage;
  let fixture: ComponentFixture<ProductsshelfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsshelfPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsshelfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
