import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnshelfPageRoutingModule } from './onshelf-routing.module';

import { OnshelfPage } from './onshelf.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnshelfPageRoutingModule
  ],
  declarations: [OnshelfPage]
})
export class OnshelfPageModule {}
