import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { AlertWindowConstants } from '@constant/constant.photo';
import { AlertController, ModalController, Platform, NavController } from '@ionic/angular';
import { API1201Service } from '@service/api';
import { API1201InDto, API1201OutDto } from '@service/dto';
import {
  CommonStore, CommonStoreService, AppState, AppStateStoreService, HeadStateService,
  ChatRoomStore, ChatRoomStoreService, AppLoadingStoreService
} from '@store';
import { RoutingService } from '@service/app-route.service';
import { PhotoViewComponent } from './photo-view/photo-view.component';
import { ChatAPI, escapeString } from '@graphql/chatApi.service';
import { Store, select } from '@ngrx/store';
import * as moment from 'moment';
import { MessageConstants } from '@constant/message-constants';
import { TextCheck } from '../../../util/text-check';
import { Photopath } from 'src/app/app_photo/store/chatroom/chatroom.store';
import { ChattalkMessage } from '../dto/chatroomdto';


@Component({
  selector: 'app-chattalk',
  templateUrl: './chattalk.page.html',
  styleUrls: ['./chattalk.page.scss', 'chattalk.page.mobile.scss', 'chattalk.page.winapp.scss'],
})

export class ChattalkPage implements OnInit, OnDestroy {

  @ViewChild('chatbottom', { static: true }) chatbottom: ElementRef;
  @ViewChild('myInput', { static: true }) myInput: ElementRef;
  platformtype: string;
  isPc: boolean;
  isSend: boolean = false;
  appState: AppState;
  inputHeight: number;

  commonStore: CommonStore;
  messages = [];
  chatBox = '';
  photoList = [];
  photoSelectList = [];
  isShowPicture = false;

  // userId
  private userId;
  private projectId;
  private bigProcessId;
  private conversationId;
  private userNameMap;

  private chatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  keyboardShowCallback: EventListenerOrEventListenerObject;
  keyboardHideCallback: EventListenerOrEventListenerObject;
  platformPause: Subscription;

  /**
   * チャット情報
   *
   */
  constructor(
    private platform: Platform,
    private routerService: RoutingService,
    private commonStoreService: CommonStoreService,
    private appStateService: AppStateStoreService,
    private api1201Service: API1201Service,
    private alertController: AlertController,
    private headStateService: HeadStateService,
    private modalController: ModalController,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>,
    private chatRoomStoreService: ChatRoomStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    private navCtrl: NavController,
  ) {
    this.platformtype = 'chattalk-' + this.appStateService.getPlatform();
    // APPストアに保存した共通内容を取得
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    this.commonStore = this.commonStoreService.commonStore;

    if (!this.appState.isPC) {
      this.platformPause = this.platform.pause.subscribe(() => {// background
        this.unsubscribeNewMessage();
        this.unsubscribeReadMessage();
        const textArea = this.myInput.nativeElement.getElementsByTagName('textarea')[0] as HTMLInputElement;
        textArea.blur();
        if (this.routerService.getUrl() === '/chattalk') {
          // this.routerService.goRouteLink('/chatroomlist');
          this.navCtrl.navigateForward('/chatroomlist', { animated: false });
        }
      });
    }
  }

  ngOnInit() {
    // console.log('ChatTalkListPage ngOnInit');
    this.isPc = this.appState.isPC ? true : false;
    this.userId = this.commonStore.userId;
    this.projectId = this.commonStore.projectId;
    this.bigProcessId = this.commonStore.bigProcessId;
    this.subscribeChatRoomStore();
    this.subscribeKeyboard();
    // titleの設定するについて、ngOnInit方法に必要な
    const conversationName = this.chatRoomStore.conversationName;
    this.headStateService.setTitle(conversationName);
    window.onoffline = async () => {
      window.onoffline = null;
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: 'チャット通信中断\r\n一覧画面に戻りますか？',
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              this.unsubscribeNewMessage();
              this.unsubscribeReadMessage();
              const textArea = this.myInput.nativeElement.getElementsByTagName('textarea')[0] as HTMLInputElement;
              if (textArea) {
                textArea.blur();
              }
              this.navCtrl.navigateForward('/chatroomlist', { animated: false });
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
              this.unsubscribeNewMessage();
              this.unsubscribeReadMessage();
            }
          }
        ],
        backdropDismiss: false
      });
      alert.present();
    }
  }

  ionViewWillEnter() {
    const conversationName = this.chatRoomStore.conversationName;
    this.headStateService.setTitle(conversationName);
    this.photoSelectList = [];
  }

  ngOnDestroy() {
    console.log('ngOnDestroy--------------');
    window.onoffline = null;
    if (!this.appState.isPC) {
      this.platformPause.unsubscribe();
    }
    this.chatRoomStoreSubscription.unsubscribe();
    window.removeEventListener('keyboardWillShow', this.keyboardShowCallback);
    window.removeEventListener('keyboardWillHide', this.keyboardHideCallback);
  }

  resize() {
    const textArea = this.myInput.nativeElement.getElementsByTagName('textarea')[0] as HTMLInputElement;
    // textArea.style.height = 'auto';
    // textArea.scrollTop = 0;
    if (textArea.scrollHeight > 120) {
      textArea.style.height = '120px';
    } else {
      textArea.style.height = textArea.scrollHeight + 'px';
    }

  }

  resizeNormal() {
    const textArea = this.myInput.nativeElement.getElementsByTagName('textarea')[0];
    textArea.style.overflow = 'auto';
    textArea.style.height = 'auto';
    textArea.style.height = this.inputHeight + 'px';
  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chattalk chatRoomStore', store);
      this.chatRoomStore = store;
      this.conversationId = store.conversationId;

      // メッセージsenderとuserinfoの関連
      const chatAssociated = store.chatAssociated;
      this.userNameMap = new Map();
      for (const associate of chatAssociated) {
        this.userNameMap.set(associate.userId, associate.usersInfo);
      }
      console.log('this.userNameMap', this.userNameMap);

      // メッセージデータのフォーマット変換
      const chatMessages = store.chatMessages;

      this.messages = chatMessages.map((message) => {
        const messageNew: ChattalkMessage = {};

        if (this.userId === message.sender) {
          messageNew.chatTalkFlg = 'R';
        } else {
          messageNew.chatTalkFlg = 'L';
          messageNew.chatTalkName = this.userNameMap.get(message.sender) && this.userNameMap.get(message.sender).userName;
        }

        messageNew.chatTalkContent = message.content !== 'NONE' ? message.content.replace(/(&lt;br&gt;)/g, '\n') : 'NONE';
        messageNew.chatTalkPictures = message.s3FullPath !== 'NONE' ? message.s3FullPath.split(',') : [];
        messageNew.createdAt = message.createdAt;
        messageNew.readUsers = message.readUsers;
        return messageNew;

      });
      // console.log('this.messages', this.messages);
      setTimeout(() => {
        this.chatbottom.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
      }, 500);

    });

  }

  subscribeNewMessage() {

    const currentConversationId = this.conversationId;
    const callBackFunc = (newMessage) => {
      // console.log('new message', newMessage);
      if (!this.userNameMap.get(newMessage.sender)) {
        this.queryAssocaitedUser();
      }

      ChatAPI.apiService.markAsRead([newMessage], this.userId, currentConversationId);
      this.chatRoomStoreService.addNewMessage(newMessage);
    };
    ChatAPI.apiService.newMessageSubscibe(this.conversationId, callBackFunc);

  }

  subscribeReadMessage() {

    const callBackFunc = (readMessage) => {
      console.log('read message', readMessage);
      this.chatRoomStoreService.addReadUsers(readMessage);
    };
    ChatAPI.apiService.markAsReadSubscribe(this.conversationId, callBackFunc);

  }

  // 監視keyboard popup 隠す
  subscribeKeyboard() {

    this.keyboardShowCallback = async () => {
      setTimeout(() => {
        this.chatbottom.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
      }, 500);
    };
    window.addEventListener('keyboardWillShow', this.keyboardShowCallback);

    this.keyboardHideCallback = async () => {
      setTimeout(() => {
        this.chatbottom.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'start' });
      }, 500);
    };

    window.addEventListener('keyboardWillHide', this.keyboardHideCallback);

  }

  ionViewDidEnter() {

    this.chatBox = '';
    const textArea = this.myInput.nativeElement.getElementsByTagName('textarea')[0] as HTMLInputElement;
    this.inputHeight = textArea.scrollHeight;
    this.subscribeNewMessage(); // 新メッセージを監視
    this.subscribeReadMessage(); // 既読メッセージを監視
    this.setEditFunction();    // 編集ボタン機能を設置
    this.queryAllMessages();  // 全部メッセージを取得する。
    // this.chatChange(this.chatBox);



  }

  ionViewDidLeave() {
    // console.log('ChatTalk ionViewDidLeave');
    this.unsubscribeNewMessage(); // 新メッセージ監視を解除
    this.unsubscribeReadMessage(); // 既読メッセージ監視を解除

  }

  unsubscribeNewMessage() {
    ChatAPI.apiService.unsubscribeNewMessage();
  }

  unsubscribeReadMessage() {
    ChatAPI.apiService.unsubscribeReadMessage();
  }

  queryAssocaitedUser() {

    // APIから、チャット情報を取得
    const currentConversationId = this.conversationId;
    const successFunc = (associated) => {
      // console.log('associated list', associated);
      this.chatRoomStoreService.setChatAssociated(associated);
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryAssocaitedUser(successFunc, failFunc, currentConversationId);

  }

  /**
   * チャット情報を取得
   *
   */
  queryAllMessages(): void {

    // APIから、チャット情報を取得
    const currentConversationId = this.conversationId;
    const successFunc = (messages) => {
      console.log('messages list', messages);
      ChatAPI.apiService.markAsRead(messages, this.userId, currentConversationId);
      this.chatRoomStoreService.setChatMessages(messages.reverse());

    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryConversationChatMessage(successFunc, failFunc, currentConversationId);

  }

  /**
   * メッセージ送信
   *
   */
  async send() {

    if (this.isSend) {
      return;
    }

    this.isSend = true;
    // send photoList
    if (this.photoSelectList && this.photoSelectList.length > 0 && this.isShowPicture) {
      this.sendPhotos();
      return;
    }

    // send text
    const escapeChatBox = escapeString(this.chatBox);
    if (escapeChatBox.length > 0) {



      if (this.chatBox !== null && !TextCheck.textCheck(this.chatBox)) {
        this.isSend = false;
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          message: MessageConstants.E00008,
          buttons: [AlertWindowConstants.OKENGLISHBUTTON],
          backdropDismiss: false
        });
        await alert.present();
        return;
      }

      this.sendText();
      this.resizeNormal();
    }

  }

  /**
   * テキストメッセージ送信
   *
   */
  sendText() {

    const successFunc = (createMessage) => {
      // console.log('createMessage', createMessage);
      this.chatBox = '';
      // this.chatChange(this.chatBox);
      this.isSend = false;
    };
    const failFunc = async (err) => {
      this.isSend = false;
      console.log('err', err);
      if (err === 'EIF018' || err === 'EIF019') {
        // エラー画面に遷移
        this.routerService.goRouteLink('/error/' + err);
      } else {
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          message: MessageConstants[err].replace('{1}', AlertWindowConstants.ADMINEMAIL),
          buttons: [AlertWindowConstants.OKBUTTON]
        });
        await alert.present();
      }
    };
    ChatAPI.apiService.createMessage(successFunc, failFunc, this.conversationId, this.userId, this.chatBox.trim());

  }

  // 日付を表示するか
  showDay(messages, message, index) {
    //  console.log('messages --- ', messages, message, index );
    if (messages.length !== index + 1) {
      const isSameDay = moment(message.createdAt).isSame(moment(messages[index + 1].createdAt), 'days');
      return !isSameDay;
    }

    return false;
  }

  dateformat(date) {
    const dateDay = moment(date).format('YYYY/MM/DD');
    return dateDay;
  }

  timeformat(date) {
    const dateTime = moment(date).format('HH:mm');
    return dateTime;

  }

  /**
   * 写真メッセージ送信
   *
   */
  sendPhotos() {

    let s3FilePath = '', count = 0;
    for (const photo of this.photoSelectList) {
      count = count + 1;
      if (count === this.photoSelectList.length) {
        s3FilePath = s3FilePath + photo.relativePath;
      } else {
        s3FilePath = s3FilePath + photo.relativePath + ',';
      }

    }

    // console.log('s3FilePath', s3FilePath);
    const successFunc = (createMessage) => {
      // console.log('createMessage', createMessage);
      this.chatBox = '';
      this.photoSelectList = [];
      this.isShowPicture = false;
      // this.chatChange(this.chatBox);
      this.isSend = false;
    };
    const failFunc = async (err) => {
      this.isSend = false;
      console.log('err', err);
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants[err].replace('{1}', AlertWindowConstants.ADMINEMAIL),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON]
      });
      await alert.present();
    };
    ChatAPI.apiService.createPhotosMessage(successFunc, failFunc, this.conversationId, this.userId, s3FilePath);

  }

  textareafocus() {
    this.isShowPicture = false;
  }

  // 写真を取得する。
  getPicture() {

    // display photoList
    if (!this.isShowPicture) {
      this.queryChatPictureList(this.getCreateInDto());
      return;
    }
    this.chatBox = '';
    // dismiss photoList
    this.isShowPicture = false;

  }

  // InDtoを作成
  getCreateInDto(): API1201InDto {
    const inDto: API1201InDto = {};
    inDto.projectId = this.projectId;
    if (!this.isPc) {
      inDto.bigProcessId = this.bigProcessId;
    }
    return inDto;
  }

  /**
   * キャプチャー情報を取得
   *
   */
  queryChatPictureList(inDto: API1201InDto): void {
    this.api1201Service.postExec(inDto, (outDto: API1201OutDto) => {
      this.isShowPicture = true;
      console.log('API1201OutDto', outDto);
      const photoPaths = outDto.photoPaths;
      this.photoList = photoPaths.map((photo) => {
        const newPic: Photopath = {};
        newPic.chatTalkPictures = photo.photoPath;
        newPic.isChecked = false;
        newPic.pictureID = photo.photoDate;
        newPic.relativePath = photo.photoRelativePath;
        return newPic;

      });
    },
      // fault
      () => { }
    );
  }

  // チャットルーム編集ボタン押下
  private async setEditFunction() {
    const editFunction = async () => {

      if (this.chatBox && this.chatBox.trim().length > 0) {
        const alert = await this.alertController.create({
          header: AlertWindowConstants.CONFIRMTITLE,
          message: MessageConstants.C00015,
          buttons: [
            {
              text: AlertWindowConstants.OKBUTTON,
              handler: () => {
                let url = '';
                if (this.isPc) {
                  url = '/chatroom-edit';
                } else {
                  url = '/chatroom-edit-user-select';
                }
                // console.log(' this.chatRoomStore',  this.chatRoomStore);
                this.chatRoomStoreService.setChatSelectUsers([]);
                this.chatRoomStoreService.setChatUsers([]);
                this.routerService.goRouteLink(url);
              }
            },
            {
              text: AlertWindowConstants.CANCELBUTTON,
              role: 'cancel',
              cssClass: 'primary',
              handler: (blah) => {
              }
            }
          ]
        });
        await alert.present();
      } else {
        let url = '';
        if (this.isPc) {
          url = '/chatroom-edit';
        } else {
          url = '/chatroom-edit-user-select';
        }
        // console.log(' this.chatRoomStore',  this.chatRoomStore);
        this.chatRoomStoreService.setChatSelectUsers([]);
        this.chatRoomStoreService.setChatUsers([]);
        this.routerService.goRouteLink(url);
      }
    };

    this.headStateService.setEditFunction(editFunction);
  }

  /**
   * トーク内のトーク写真リンク押下、写真表示画面をホップアップ表示
   * @param picture　当前の写真
   */
  onPreview(picture) {
    this.showPhoto(picture);
  }

  // 写真を選択
  selectphoto = async (photo) => {

    // console.log('select photo', photo, this.photoList);
    let count = 0;
    this.photoList.map((photoA) => {
      if (photoA.isChecked) {
        count = count + 1;
      }
    });

    // max number check
    // チェックされた写真枚数が「処理概要 制約」で定めた枚数以上の場合
    if (count > 21) {

      // console.log('select photo', photo, this.photoList);

      // チェックをつけずエラーメッセージ(E00042)をポップアップ出力する。
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00042.replace('{value}', '21'),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      photo.isChecked = false;
    }

    // return check photo
    const photoSelectList = this.photoList.filter((photoA) => {
      if (photoA.isChecked) {
        return photoA;
      }
    });
    this.photoSelectList = photoSelectList;

  }

  // popup
  showPhoto = async (picture: string) => {

    const setcomp = PhotoViewComponent;
    let css = 'previewModalPc';
    if (this.isPc) {
      css = 'previewModalPc';
    } else {
      css = 'previewModalModel';
    }
    const modal = await this.modalController.create({
      component: setcomp,
      componentProps: {
        formart: picture
      },
      cssClass: css
    });
    await modal.present();
  }

  textCheck(text: string): boolean {
    if (text && text.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  // 戻るボタン押下
  chatChange(chatBox: string) {

    this.resize();

    const backFunction = async () => {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00015,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              this.headStateService.setBackFunction(null);
              const url = '/chatroomlist';
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
        ]
      });
      await alert.present();
    };

    if (chatBox && chatBox.trim().length > 0) {
      this.headStateService.setBackFunction(backFunction);
    } else {
      this.headStateService.setBackFunction(null);
    }
  }

  sendCheck(): boolean {
    if ((!this.chatBox || this.chatBox.trim().length === 0) && this.photoSelectList.length === 0) {
      return false;
    } else {
      return true;
    }
  }

}
