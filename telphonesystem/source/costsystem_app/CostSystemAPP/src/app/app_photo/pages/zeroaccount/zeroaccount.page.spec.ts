import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ZeroaccountPage } from './zeroaccount.page';

describe('ZeroaccountPage', () => {
  let component: ZeroaccountPage;
  let fixture: ComponentFixture<ZeroaccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZeroaccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ZeroaccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
