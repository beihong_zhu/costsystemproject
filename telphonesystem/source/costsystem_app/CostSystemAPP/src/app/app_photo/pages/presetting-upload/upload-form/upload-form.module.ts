import { NgModule } from '@angular/core';
import { UploadFormPage } from './upload-form.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { BuildingSearchPage } from './building-search/building-search.page';

@NgModule({
  providers: [],
  declarations: [UploadFormPage, BuildingSearchPage],
  entryComponents: [
    BuildingSearchPage
  ],
  imports: [
    SharedModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: UploadFormPage
      }
    ])
  ]
})
export class UploadFormModule { }
