import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateregularPage } from './createregular.page';

const routes: Routes = [
  {
    path: '',
    component: CreateregularPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateregularPageRoutingModule {}
