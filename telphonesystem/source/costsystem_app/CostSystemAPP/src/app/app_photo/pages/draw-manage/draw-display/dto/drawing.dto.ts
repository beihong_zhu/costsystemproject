import { FloorDto } from './floor.dto';

export declare interface DrawingDto {
  // ファイル名称
  fileName?: string;
  // ファイルイメージ
  fileImage?: string;
  // フロアID
  floorId?: number;
  // フロア
  floor?: FloorDto;
  // 図面Id
  drawingId?: string;
}
