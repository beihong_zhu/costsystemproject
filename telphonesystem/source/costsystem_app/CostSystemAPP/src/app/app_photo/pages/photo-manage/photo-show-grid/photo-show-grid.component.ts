import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppStateStoreService } from '@store';
import { LookType } from '../dto/photo-manage-dto';
import { CD0007Constants } from '@constant/code';
import { PhotoListConf } from '../photo-list/photo-list-conf';
import { PhotoListComponentHelp } from '../photo-list/photo-list.component-help';

@Component({
  selector: 'app-photo-show-grid',
  templateUrl: './photo-show-grid.component.html',
  styleUrls: ['./photo-show-grid.component.scss', './photo-show-grid.mobile.scss', './photo-show-grid.winapp.scss'],
})
export class PhotoShowGridComponent implements OnInit {
  isPc: boolean;
  isWinApp: boolean;
  @Input() selectPhotos: any[];
  @Input() isPhotoChange: boolean;
  @Input() datas: any[];
  @Input() sortby: string;
  @Input() mulitype: string;
  @Input() bigProcess: LookType;
  @Input() isShowCheckBox: boolean;
  @Output() selectp = new EventEmitter<any>();
  @Output() goPhotoDetail = new EventEmitter<any>();

  cd0007Constants = CD0007Constants;
  platformtype: string;


  approxItemHeight: string;

  constructor(
    private appStateService: AppStateStoreService,
    private photolistComponentHelp: PhotoListComponentHelp) {
    this.isPc = this.appStateService.appState.isPC;
    this.isWinApp = this.appStateService.appState.isWinApp;
    this.platformtype = 'photogrid-' + this.appStateService.getPlatform();
    this.approxItemHeight = this.isPc || this.isWinApp ? 292 * 0.75 + 'px' : 156 + 'px';
  }

  ngOnInit() {
  }

  selectphoto(ph) {
    this.selectp.emit(ph);
  }
  goPhotoDetailpage(ph) {
    this.goPhotoDetail.emit(ph);
  }

  pcWidth_count() {
    const w = document.getElementById('photo-div').offsetWidth;
    const count = Math.floor((w - 20) / 128);
    return count;
  }


  myitemHeight = (item, index) => {

    let headH = this.isPc || this.isWinApp ? 70 : 40;
    if (this.sortby && this.sortby === this.cd0007Constants.DATE) {
      headH = 0;
    }

    const cloum = this.isPc || this.isWinApp ? this.photolistComponentHelp.pcWidth_count() : PhotoListConf.MOBILE_COUNT;
    const height = this.isPc || this.isWinApp ? 292 * 0.75 : 116;
    const spaceheight = this.isPc || this.isWinApp ? 10 : 5;
    const count = (item as any[]).length;

    return Math.ceil(count / cloum) * height + headH + (Math.ceil(count / cloum) - 1) * spaceheight;
  }

  myHeaderFn = (item, index, items) => {
    if (index === 0) {
      return this.headerTitle(item[0]);
    } else {
      const title = this.headerTitle(items[index - 1][0]);

      const title1 = this.headerTitle(item[0]);
      if (title !== title1) {
        return title1;
      }
    }
    return null;
  }

  headerTitle(photo) {
    return (photo.processName === null || photo.processId === null || photo.workName === null
      || photo.workId === null) ? '工程・作業名(不明)' : photo.processName + ' - ' + photo.workName;
  }


}
