import { Component, OnInit } from '@angular/core';
import { CommonConstants, HeaderConstants, S1501Constants } from '@constant/constant.photo';
import { API3014Service } from '@service/api';

import { API3014InDto, API3014OutDto } from '@service/dto';
import { AppState, CommonStore } from '@store';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysMutexDto } from './dto';


@Component({
  selector: 'app-mutex',
  templateUrl: './mutex.page.html',
  styleUrls: ['./mutex.page.scss'],
})
export class MutexPage implements OnInit {


  public agencyname: string ;
  // ids
  public channelname: string ;

  public mutexInfo:any = { 
    typesList:['仅仅是','不是'],
    type:'仅仅是'
  }
  public sysUserList: Array<SysMutexDto> = new Array<SysMutexDto>();

  constructor(private api3014Service: API3014Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api3014ServicePostExec();
  }

  private get3014InDto() {
    let api3014InDto: API3014InDto = {};
    api3014InDto.agencyname = this.agencyname;
    api3014InDto.channelname = this.channelname;
    api3014InDto.type = this.mutexInfo.type;
    return api3014InDto;
  }


  private api3014ServicePostExec() {
    // // 写真帳出力情報取得
    // this.api0102Service.postExec(this.get0102InDto(), (outDto: API0102OutDto) => {
    //   this.api01f02successed(outDto);
    // },
    //   // fault
    //   () => {
    //   });

    // 写真帳出力情報取得
    this.api3014Service.postExecNew(this.get3014InDto()).then((res) => {
      // response の型は any ではなく class で型を定義した方が良いが
      // ここでは簡便さから any としておく

      // @angular/http では json() でパースする必要があったが､ @angular/common/http では不要となった
      //const response: any = res.json();
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3014successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }
  private api3014successed(outDto: API3014OutDto) {
    this.sysUserList = new Array<SysMutexDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.sysMutexInfoModels));

    for (const sysUserInfoModel of loadSysusers) {
      let sysUserDto: SysMutexDto = new SysMutexDto();
      sysUserDto.channelID = sysUserInfoModel.userId;
      sysUserDto.channelID = sysUserInfoModel.userName;
      sysUserDto.productType = sysUserInfoModel.companyName;

      

      this.sysUserList.push(sysUserDto);
    }
  }



}
