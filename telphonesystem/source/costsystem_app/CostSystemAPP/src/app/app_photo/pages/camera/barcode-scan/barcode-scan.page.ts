import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { S0401Constants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { MachineNumberRegistComponent } from './machine-number-regist/machine-number-regist.component';
import { RoutingService } from '@service/app-route.service';
import { AppState, HeadState, HeadStateService, LoggerStoreService, AppStateStoreService } from '@store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-barcode-scan',
  templateUrl: './barcode-scan.page.html',
  styleUrls: ['./barcode-scan.page.scss', './barcode-scan.winapp.scss'],
  providers: [QRScanner]
})

export class BarcodeScanPage implements OnInit, OnDestroy {

  headStateSubscription: Subscription;

  isWinApp: boolean;

  appState: AppState;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    manualImport: S0401Constants.MANUALIMPORT,
    manipulationDescription: MessageConstants.I00008
  };

  // QRコード読取データ
  public qrCodeValue = '';

  // 画面クローズ判定
  public isScreenClose = false;

  headState: HeadState;

  private scanStatus: QRScannerStatus;
  private scanSubscription: Subscription;

  constructor(
    private headStateService: HeadStateService,
    private loggerStoreService: LoggerStoreService,
    private qrScanner: QRScanner,
    public modalController: ModalController,
    public routingService: RoutingService,
    private appStateService: AppStateStoreService,
  ) {
    this.appState = this.appStateService.appState;
    this.isWinApp = this.appState.isWinApp ? true : false;
    this.platformtype = 'barcode-scan-' + this.appStateService.getPlatform();
    this.headStateSubscription = this.headStateService.headStateSubscribe(state => {
      this.headState = state;
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.scan();
  }

  async ionViewWillLeave() {
    await this.qrScanner.hide();
    // qrコードスキャナ破棄
    await this.qrScanner.destroy();
  }

  ngOnDestroy() {
    this.isScreenClose = true;
    sessionStorage.removeItem('ocrManufacturingDate');
    this.headStateSubscription.unsubscribe();
  }

  async scan() {
    try {
      this.scanStatus = await this.qrScanner.prepare();
      this.loggerStoreService.addLogger('barcodesca page status', status);

      if (this.scanStatus.authorized) {
        this.loggerStoreService.addLogger('barcodesca page', 'authorized');
        this.scanSubscription = this.qrScanner.scan().subscribe((text: string) => {
          this.loggerStoreService.addLogger('barcodesca page qrcode', text);
          this.qrCodeValue = text;
          this.scanSubscription.unsubscribe();
          this.registMachineNumber();
        });

        this.qrScanner.show();
        this.loggerStoreService.addLogger('barcodesca page qrcode', 'show');

      } else if (this.scanStatus.denied) {
        // カメラが使用できない場合（denied)
        this.loggerStoreService.addLogger('barcodesca page', 'premanently denied');
        console.warn('camera feature is premanently denied.');
        alert(MessageConstants.E00102);
      } else {
        // E00102以外のエラー（authrizedでもなく、かつdeniedでもない）
        this.loggerStoreService.addLogger('barcodesca page', 'initial denied');
        console.warn('camera feature is instantly denied.');
        alert(MessageConstants.E00101);
      }
    } catch (err) {
      this.loggerStoreService.addLogger('barcodesca page error', err);
    }
  }

  close = () => {
    this.routingService.goRouteLink(this.headState.backRoute);
  }

  /**
   * 読み取りダイアログ表示（QR読込）
   */
  async registMachineNumber() {
    const modal = await this.modalController.create({
      component: MachineNumberRegistComponent,
      componentProps: { isQR: true, qrCodeValue: this.qrCodeValue },
      cssClass: this.isWinApp ? 'machineNumberRegistModalWinApp' : 'machineNumberRegistModal'
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      if (data.close) {
        this.close();
      } else {
        if (this.scanStatus.authorized) {
          this.scanSubscription = this.qrScanner.scan().subscribe((text: string) => {
            this.loggerStoreService.addLogger('barcodesca page qrcode', text);
            this.qrCodeValue = text;
            this.scanSubscription.unsubscribe();
            this.registMachineNumber();
          });
        }
      }
    }
  }

  /**
   * 読み取りダイアログ表示（手動）
   */
  async mabualImport() {
    const modal = await this.modalController.create({
      component: MachineNumberRegistComponent,
      componentProps: { isQR: false },
      cssClass: this.isWinApp ? 'machineNumberRegistModalWinApp' : 'machineNumberRegistModal'
    });
    // stop scanning
    if (this.scanSubscription) {
      this.scanSubscription.unsubscribe();
    }
    await modal.present();
    const { data } = await modal.onDidDismiss();
    if (data) {
      if (data.close) {
        this.close();
      } else {
        if (this.scanStatus.authorized) {
          this.scanSubscription = this.qrScanner.scan().subscribe((text: string) => {
            this.loggerStoreService.addLogger('barcodesca page qrcode', text);
            this.qrCodeValue = text;
            this.scanSubscription.unsubscribe();
            this.registMachineNumber();
          });
        }
      }
    }
  }

}
