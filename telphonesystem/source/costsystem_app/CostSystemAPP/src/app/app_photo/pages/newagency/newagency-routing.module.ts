import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewagencyPage } from './newagency.page';

const routes: Routes = [
  {
    path: '',
    component: NewagencyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewagencyPageRoutingModule {}
