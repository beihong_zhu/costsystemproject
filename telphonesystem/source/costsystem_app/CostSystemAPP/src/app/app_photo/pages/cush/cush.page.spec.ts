import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CushPage } from './cush.page';

describe('CushPage', () => {
  let component: CushPage;
  let fixture: ComponentFixture<CushPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CushPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CushPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
