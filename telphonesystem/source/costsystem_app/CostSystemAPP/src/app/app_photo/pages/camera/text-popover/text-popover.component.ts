import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'text-popover',
    templateUrl: './text-popover.component.html',
    styleUrls: ['./text-popover.component.scss'],
})
export class TextSinglePopoverComponent implements OnInit {

  message: string;

    constructor() {
    }

    ngOnInit() {
        console.log('messageArr', this.message);
    }

}
