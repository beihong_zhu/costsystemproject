import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { AppStateStoreService, CommonStoreService } from '@store';
import { PhotoGridItemComponent } from './photo-grid-item.component';

@NgModule({
  providers: [
    AppStateStoreService,
    CommonStoreService,

  ],
  declarations: [
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoGridItemComponent
      }
    ])
  ],
  entryComponents: [],
})
export class PhotoGridItemModule { }
