import { Component, OnInit } from '@angular/core';
import { AppState, CommonStore } from '@store';
import { API0202Service } from '@service/api/api-02-02.service';
import { API0202InDto, API0202OutDto } from '@service/dto';
import { CommonConstants, HeaderConstants, S1601Constants } from '@constant/constant.photo';
import { ApiResponse } from '../../util/api-response';
import { ResultType } from 'src/app/app_common/constant/common';
import { SysStockDto } from './dto';
import { AlertController, ModalController } from '@ionic/angular';
@Component({
  selector: 'app-productstock',
  templateUrl: './productstock.page.html',
  styleUrls: ['./productstock.page.scss'],
})
export class ProductstockPage implements OnInit {

  private appState: AppState;
  private commonStore: CommonStore;

 // public sysUserList: Array<SysUserDto> = new Array<SysUserDto>();

  // title
  //public title: string = S1601Constants.TITLE;
  // name
  //public channelName: string = S1601Constants.channelName;
  // space
 // public space: string = '           ';
  // search
  //public productType: string = S1601Constants.productType;
  //  status
  //public productStatus: string = S1601Constants.productStatus;
  //  status
  //public value: string = S1601Constants.value;
  //  status
  //public status: string = S1601Constants.status;
  //  level
  //public level: string = S1601Constants.level;
  // split
  //public split: string = S1601Constants.split;
  //block
 // public block: string = S1601Constants.block;
  //SUCCESS

  public sysstockList: Array<SysStockDto> = new Array<SysStockDto>();

  public channelName : string;
  public value : string;


    // user list header name
      productListHead: string[] = [
      S1601Constants.channelID,
     // S1601Constants.channelName,
      S1601Constants.productType,
      S1601Constants.productStatus,
     // S1601Constants.value,
      S1601Constants.level,
      S1601Constants.split,
      S1601Constants.block 
    ];
  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;
  
  public peopleInfo:any = { 
  typesList:['话费快充','话费慢充'],
  type:'话费快充',
  statusList:['上架','下架'],
  status:'上架',
  provinceList:[{title : '北京',
                  checked :false},
                  {title : '上海',
                  checked :false},
                  {title : '天津',
                  checked :false},
                  {title : '辽宁',
                  checked :false},
                  {title : '吉林',
                  checked :false},
                  {title : '河北',
                  checked :false},
                  {title : '山西',
                  checked :false},
                  {title : '江苏',
                  checked :false},
                  {title : '浙江',
                  checked :false},
                  {title : '安徽',
                  checked :false},
                  {title : '福建',
                  checked :false},
                  {title : '江西',
                  checked :false},
                  {title : '山东',
                  checked :false},
                  {title : '湖南',
                  checked :false},
                  {title : '湖北',
                  checked :false},
                  {title : '广东',
                  checked :false},
                  {title : '海南',
                  checked :false},
                  {title : '四川',
                  checked :false},
                  {title : '贵州',
                  checked :false},
                  {title : '云南',
                  checked :false},
                  {title : '陕西',
                  checked :false},
                  {title : '甘肃',
                  checked :false},
                  {title : '青海',
                  checked :false},
                  {title : '重庆',
                  checked :false},
                  {title : '新疆',
                  checked :false},
                  {title : '内蒙古',
                  checked :false},
                  {title : '西藏',
                  checked :false},
                  {title : '移动',
                  checked :false},
                  {title : '电信',
                  checked :false},
                  {title : '网通',
                  checked :false}],
  province:'北京'
}
                  
  constructor(
    //public alertController: AlertController,
    //public modalController: ModalController,
    private api0202Service: API0202Service) { }

  ngOnInit() {}

  public onSearch() {
    this.api0202ServicePostExec();
  }

  private get0202InDto() {
    let api0202InDto: API0202InDto = {};
    api0202InDto.channelname= this.channelName;
    api0202InDto.value= this.value;
    api0202InDto.producttype= this.peopleInfo.type;
    api0202InDto.productstatus= this.peopleInfo.status;
    return api0202InDto;

  }
  private api0202ServicePostExec() {

    this.api0202Service.postExecNew(this.get0202InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api0202successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api0202successed(outDto: API0202OutDto) {
    this.sysstockList = new Array<SysStockDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.stockInfoModelResList));

    for (const sysUserInfoModel of loadSysusers) {
      let sysStockDto: SysStockDto = new SysStockDto();
      sysStockDto.abbreviation = sysUserInfoModel.abbreviation;
      sysStockDto.productName = sysUserInfoModel.productName;
      sysStockDto.productMode = sysUserInfoModel.productMode;
      sysStockDto.value = sysUserInfoModel.value;
      sysStockDto.price = sysUserInfoModel.price;
      sysStockDto.disaccount = sysUserInfoModel.disaccount;
      sysStockDto.priority = sysUserInfoModel.priority;
      sysStockDto.status = sysUserInfoModel.status;
    
      this.sysstockList.push(sysStockDto);
    }
  }
  

  }

