import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MutexPageRoutingModule } from './mutex-routing.module';

import { MutexPage } from './mutex.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MutexPageRoutingModule
  ],
  declarations: [MutexPage]
})
export class MutexPageModule {}
