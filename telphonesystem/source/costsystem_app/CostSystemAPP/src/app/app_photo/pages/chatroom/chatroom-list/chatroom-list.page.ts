import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoutingService } from '@service/app-route.service';
import {
  AppState, ChatRoomStore, ChatRoomStoreService,
  CommonStore, CommonStoreService, AppStateStoreService, HeadStateService
} from '@store';
import { ChatAPI } from '@graphql/chatApi.service';
import { Store, select } from '@ngrx/store';
import * as moment from 'moment';
import { PopoverController, Platform } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';

@Component({
  selector: 'app-chatroom',
  templateUrl: './chatroom-list.page.html',
  styleUrls: ['./chatroom-list.page.scss', './chatroom-list.page.mobile.scss', './chatroom-list.page.winapp.scss'],
})

export class ChatroomListPage implements OnInit {
  platformtype: string;
  isPc: boolean;
  commonStore: CommonStore;
  appState: AppState;

  // チャットルームリスト
  public chatConversations;
  public conversations;
  public projectId;
  public userId;


  constructor(
    public popoverController: PopoverController,
    public headStateService: HeadStateService,
    private appStateService: AppStateStoreService,
    private routerService: RoutingService,
    private chatRoomStoreService: ChatRoomStoreService,
    private commonStoreService: CommonStoreService,
    private platform: Platform
  ) {
    this.platformtype = 'chatroom-list-' + this.appStateService.getPlatform();
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }

  ngOnInit() {
    // console.log('ChatroomListPage ngOnInit');
    this.isPc = this.appState.isPC ? true : false;
    this.projectId = this.commonStore.projectId;
    this.userId = this.commonStore.userId;

  }





  // 当日の場合、hh:mi(24時間表示)
  // 当日以外の場合、yyyy/mm/dd
  dateFormat(date) {
    // 当日の場合、hh:mi(24時間表示)
    if (this.sameDay(date)) {
      return this.dateformatTime(date);
    } else {
      // 当日以外の場合、yyyy/mm/dd
      return this.dateformatYear(date);
    }

  }

  // 当日の判定
  sameDay(chatroom) {
    const isSameDay = moment(chatroom).isSame(new Date(), 'days');
    return isSameDay;
  }

  dateformat(date) {
    const dateDay = moment(date).format('MM/DD HH:mm');
    return dateDay;
  }

  // yyyy/mm/dd表示
  dateformatYear(date) {
    const dateDay = moment(date).format('YYYY/MM/DD');
    return dateDay;
  }

  // hh:mi(24時間表示)
  dateformatTime(date) {
    const dateDay = moment(date).format('HH:mm');
    return dateDay;
  }

  // チャットロームリストを取得
  getListConversation() {

    const successFunc = (conversations) => {
      // console.log('conversations list', conversations);
      this.chatConversations = conversations;
      const conversationsSort = [];
      for (const conv of conversations) {
        const convs = conv.conversation;
        if (convs) {
          conversationsSort.push(convs);
        }
      }

      // 直前の送受信時刻をソート
      this.conversations = conversationsSort.sort((a, b) => {
        if (!a.updatedAt) { return -1; }
        if (!b.updatedAt) { return 1; }
        return (a.updatedAt > b.updatedAt ? -1 : 1);
      });
    };
    const failFunc = (err) => {
      //  console.log('err', err);
    };
    ChatAPI.apiService.queryListConversation(successFunc, failFunc, this.userId, this.projectId);

  }

  ionViewDidEnter() {
    this.setEditFunction();
    this.getListConversation();
    // console.log('ChatroomListPage ionViewDidEnter');
  }

  // enter converstaion
  clickItem(conversation) {
    // console.log('conversation', conversation);
    const conversationId = conversation.conversationId;
    const conversationName = conversation.conversationName;
    this.chatRoomStoreService.setChatCurrentConversation(conversation);
    this.chatRoomStoreService.setChatConversationId(conversationId);
    this.chatRoomStoreService.setChatConversationName(conversationName);

    for (const conv of this.chatConversations) {
      if (conv.conversationId === conversationId) {
        this.chatRoomStoreService.setChatAssociated(conv.associated);
      }
    }

    this.chatRoomStoreService.setChatMessages([]);
    const url = '/chattalk';
    this.routerService.goRouteLink(url);

  }

  async onPress(ev: Event, conversation) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [conversation.conversationName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  // チャットルーム作成ボタン押下
  private setEditFunction() {
    const editFunction = async () => {

      this.chatRoomStoreService.setChatSelectUsers([]);
      this.chatRoomStoreService.setChatUsers([]);
      this.chatRoomStoreService.setChatConversationName('');

      let url = '';
      if (this.isPc) {
        url = '/chatroom-create';
      } else {
        url = '/chatroom-create-user-select';
      }
      this.routerService.goRouteLink(url);
    };

    this.headStateService.setEditFunction(editFunction);
  }

}
