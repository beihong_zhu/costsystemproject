import { NgModule } from '@angular/core';
import { PhotoListComponent } from './photo-list.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { PhotoShowListComponent } from '../photo-show-list/photo-show-list.component';
import { PhotoListItemComponent } from '../photo-list-item/photo-list-item.component';
import { PhotoShowGridComponent } from '../photo-show-grid/photo-show-grid.component';
import { PhotoGridItemComponent } from '../photo-grid-item/photo-grid-item.component';
import { PhotoFilterSetComponent } from '../photo-filter-set/photo-filter-set.component';
import { PhotoSortSetComponent } from '../photo-sort-set/photo-sort-set.component';
import { PhotoViewSetComponent } from '../photo-view-set/photo-view-set.component';
import {
  PhotoDownloadSuccessStore,
  AutoUploadPhotoStore,
} from '@store';
import { HttpClient } from '@angular/common/http';

@NgModule({
  providers: [
    PhotoDownloadSuccessStore,
    AutoUploadPhotoStore,
    HttpClient,
  ],
  declarations: [
    PhotoListComponent,
    PhotoShowListComponent,
    PhotoListItemComponent,
    PhotoShowGridComponent,
    PhotoGridItemComponent,
    PhotoFilterSetComponent,
    PhotoSortSetComponent,
    PhotoViewSetComponent,
    // PhotoDrawFooterComponent

  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoListComponent
      }
    ])
  ],
  entryComponents: [PhotoFilterSetComponent, PhotoSortSetComponent, PhotoViewSetComponent],
})
export class PhotoListModule { }
