import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlacklistPage } from './blacklist.page';

describe('BlacklistPage', () => {
  let component: BlacklistPage;
  let fixture: ComponentFixture<BlacklistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlacklistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlacklistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
