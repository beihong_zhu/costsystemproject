import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CD0001Constants } from '@constant/code';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { RoutingService } from '@service/app-route.service';
import { LocalPhoto, LookType } from '@pages/photo-manage/dto/photo-manage-dto';
import { AppStateStoreService, CameraStoreService, PhotoDetailStoreService, InitInterfaceType } from '@store';
import { File } from '@ionic-native/file/ngx';
import { PhotoListConf } from '@pages/photo-manage/photo-list/photo-list-conf';
import { PhotoConf } from '@conf/photo-conf';

@Component({
  selector: 'app-photo-simple-grid-item',
  templateUrl: './photo-simple-grid-item.component.html',
  styleUrls: ['./photo-simple-grid-item.component.scss', './photo-simple-grid-item.component.mobile.scss'],
})
export class PhotoSimpleGridItemComponent implements OnInit {

  @Input() photo: LocalPhoto;
  @Input() bigProcessId: string;
  @Output() dismiss = new EventEmitter<void>();

  isPc: boolean;
  photoPath: string;
  isShowPhoto: boolean;
  isShowNoimageIcon: boolean;
  isShowTiming: boolean;
  platformtype: string;
  showBottomview: boolean;
  // server のicon
  isShowServerPhotoIcon: boolean;
  // 写真帳選択のicon
  isShowPhotobookIcon: boolean;

  constructor(
    private appStateService: AppStateStoreService,
    private cameraStoreService: CameraStoreService,
    public routingService: RoutingService,
    private file: File,
    private webview: WebView,
  ) {
    this.isPc = this.appStateService.appState.isPC;
    this.platformtype = 'photo-simple-grid-item-' + this.appStateService.getPlatform();
  }


  ngOnInit() {

    // console.log('photo grid item', this.photo);
    if (this.photo.photoPathLocal) {

      const filePath = this.file.dataDirectory + this.photo.photoPathLocal;
      this.photoPath = this.webview.convertFileSrc(filePath);

    } else {

      this.photoPath = this.photo.photoImage;

    }
    // console.log('photo grid item photoPath', this.photoPath);


    this.isShowPhoto = this.photoPath !== undefined && this.photoPath != null;
    this.isShowNoimageIcon = this.photoPath === undefined || this.photoPath === null;
    this.isShowTiming = this.bigProcessId !== CD0001Constants.CD0001_04 && this.photo.timingAbbreviation !== PhotoConf.NOTIMING;
    // 写真あり&serve から取得
    this.isShowServerPhotoIcon = this.photo.localPhotoFlag === PhotoListConf.SERVER_POTO && this.photoPath != null;
    //  写真あり&&写真帳
    this.isShowPhotobookIcon = this.photo.isOutputPhotoBook === PhotoListConf.OUT_PUT_PHOTO_BOOK && this.photoPath != null;

    this.showBottomview = this.isShowTiming || this.isShowPhotobookIcon || this.isShowServerPhotoIcon;
  }

  gotocamera() {
    console.log(this.photo);

    if (!this.isPc) {
      this.cameraStoreService.setFromInitInterfaceType(InitInterfaceType.FromDrawingNoIMage);
      this.cameraStoreService.setCurrentPhoto(this.photo);

      this.routingService.goRouteLink('/camera');
      this.dismiss.emit();
    }
  }

  imageclick(event) {
    event.stopPropagation();
    console.log('imageclick');

    this.gotodetail(this.photo);

  }

  gotodetail(photo) {
    console.log('detail page', photo);
  }
}
