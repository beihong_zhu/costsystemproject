import { Component, OnInit } from '@angular/core';
import { AppState } from '../../store/app-state/app-state.store';
import { CommonStore } from '../../store/common/common.store';
import { CommonConstants, HeaderConstants, S1901Constants } from '@constant/constant.photo';
import { API3004Service } from '@service/api';
import { ApiResponse } from '../../util/api-response';
import { API3004InDto, API3004OutDto } from '@service/dto';
import { ResultType } from 'src/app/app_common/constant/common';
import { SysAgencyInfoDto } from './dto';

@Component({
  selector: 'app-agency',
  templateUrl: './agency.page.html',
  styleUrls: ['./agency.page.scss'],
})
export class AgencyPage implements OnInit {

  private appState: AppState;
  private commonStore: CommonStore;
  public accountagency: string;
  public agencyname: string;
  public agecnyabbreviate :string;
 
  public sysAgencyList: Array<SysAgencyInfoDto> = new Array<SysAgencyInfoDto>();
   

  public agencyList : any = {
    agencyStatusList:['开启' ,'关闭'],
    agency:'开启'
  };

    // user list header name
    agencyListHead: string[] = [
      S1901Constants.USERID,
      S1901Constants.USERNAME,
      S1901Constants.COMPANYNAME,
      S1901Constants.AUTHORITYID,
      S1901Constants.MAILADDRESS,
      S1901Constants.ISLOGIN
    ];
 

  constructor(private api3004Service: API3004Service) {

   }

  ngOnInit() {
  }

  public onSearch() {
    this.api3004ServicePostExec();
  }

  private get3004InDto() {
    let api3004InDto: API3004InDto = {};
    api3004InDto.accountagency= this.accountagency;
    api3004InDto.agencyname = this.agencyname;
    api3004InDto.agecnyabbreviate =  this.agecnyabbreviate;
    api3004InDto.agency = this.agencyList.agency;
    
    return api3004InDto;
  }

  private api3004ServicePostExec() {

    this.api3004Service.postExecNew(this.get3004InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3004successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);

  }
  private api3004successed(outDto: API3004OutDto) {
    this.sysAgencyList = new Array<SysAgencyInfoDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.sysAgencyInfoModels));

    for (const sysUserInfoModel of loadSysusers) {
      let sysUserDto: SysAgencyInfoDto = new SysAgencyInfoDto();
      sysUserDto.accountagency= sysUserInfoModel.accountagency;
      sysUserDto.agecnyAbbreviate = sysUserInfoModel.agecnyAbbreviate;
      sysUserDto.province = sysUserInfoModel.province;
      sysUserDto.type = sysUserInfoModel.type;
      sysUserDto.mode = sysUserInfoModel.mode;
      sysUserDto.balance = sysUserInfoModel.balance;
      sysUserDto.use = sysUserInfoModel.use;
      sysUserDto.status = sysUserInfoModel.status;
     
      this.sysAgencyList.push(sysUserDto);
    }
  }

}
