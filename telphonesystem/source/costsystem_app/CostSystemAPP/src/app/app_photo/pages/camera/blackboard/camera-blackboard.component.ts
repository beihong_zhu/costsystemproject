import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { PopoverController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs/internal/Subscription';
import { Store, select } from '@ngrx/store';
import { CommonStore, CameraStore, CommonStoreService, AppStateStoreService } from '@store';
import { S0301Constants } from '@constant/constant.photo';
import * as moment from 'moment';
import { CameraStoreService } from '../../../store/camera/camera.store.service';

@Component({
  selector: 'camera-blackboard',
  templateUrl: './camera-blackboard.component.html',
})
export class CameraBlackboardComponent implements OnInit, OnDestroy {

  @Input() blackBoardWidth = 0;

  public floorRoomPlace: string;
  public witnessInfo: string;
  cameraStoreSubscription: Subscription;

  public BdProjectTitle = S0301Constants.BDPROJECTTITLE;
  public BdPlaceTitle = S0301Constants.BDPLACETITLE;
  public BdLineTitle = S0301Constants.BDLINETITLE;
  public BdMachineTitle = S0301Constants.BDMACHINETITLE;
  public BdProcessTitle = S0301Constants.BDPROCESSTITLE;
  public BdWorkTitle = S0301Constants.BDWORKTITLE;

  public defaultProcess = S0301Constants.DEFAULTPROCESS;
  public defaultWork = S0301Constants.DEFAULTWORK;
  public defaultTiming = S0301Constants.DEFAULTTIMING;

  public defaultFloor = S0301Constants.DEFAULTFLOOR;
  public defaultRoom = S0301Constants.DEFAULTROOM;
  public defaultPlace = S0301Constants.DEFAULTPLACE;
  public defaultMachine = S0301Constants.DEFAULTMACHINE;
  public defaultLine = S0301Constants.DEFAULTLINE;
  public defaultProject = S0301Constants.DEFAULTPROJECT;
  public defaultFree = S0301Constants.DEFAULTFREE;
  public defaultFloorRoomPlace = S0301Constants.DEFAULTFLOOR + ' ' +
    S0301Constants.DEFAULTROOM + ' ' +
    S0301Constants.DEFAULTPLACE;
  public defaultWitnessInfo = S0301Constants.DEFAULTCOMPANYNAME + ' ' +
    S0301Constants.DEFAULTWITNESSNAME + ' ' +
    S0301Constants.DEFAULTPHOTOTIME;

  cameraStore: CameraStore;
  commonStore: CommonStore;
  isTablet: boolean;

  constructor(
    public platform: Platform,
    public popoverController: PopoverController,
    private appStateStoreService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private cameraStoreService: CameraStoreService,
  ) {
    this.commonStore = this.commonStoreService.commonStore;
    this.subscribeCameraStoreBlackBoard();
    this.isTablet = this.appStateStoreService.appState.isWinApp;
  }

  subscribeCameraStoreBlackBoard() {
    this.cameraStoreSubscription = this.cameraStoreService.cameraStoreSubscription(subscribestore => {
      this.cameraStore = subscribestore;
      this.floorRoomPlace = '';
      this.witnessInfo = '';

      if (this.cameraStore.isBlackboardDisplay) {
        const blackboardDisplay = this.cameraStore.blackboardDisplay;
        if (blackboardDisplay.isFloorDisplay === '1') {
          this.floorRoomPlace += (this.cameraStore.currentFloor && this.cameraStore.currentFloor.floorName) || S0301Constants.DEFAULTFLOOR;
          this.floorRoomPlace += ' ';
        }
        if (blackboardDisplay.isRoomDisplay === '1') {
          this.floorRoomPlace += (this.cameraStore.currentRoom && this.cameraStore.currentRoom.roomName) || S0301Constants.DEFAULTROOM;
          this.floorRoomPlace += ' ';
        }
        if (blackboardDisplay.isPlaceDisplay === '1') {
          this.floorRoomPlace += (this.cameraStore.currentPlace && this.cameraStore.currentPlace.placeName) || S0301Constants.DEFAULTPLACE;
        }
        if (blackboardDisplay.isConstructionCompanyDisplay === '1') {
          this.witnessInfo += (this.cameraStore.currentWitness && this.cameraStore.currentWitness.constructionCompanyName)
            || S0301Constants.DEFAULTCOMPANYNAME;
          this.witnessInfo += ' ';
        }
        if (blackboardDisplay.isWitnessDisplay === '1') {
          this.witnessInfo += (this.cameraStore.currentWitness && this.cameraStore.currentWitness.witnessName)
            || S0301Constants.DEFAULTWITNESSNAME;
          this.witnessInfo += ' ';
        }
        if (blackboardDisplay.isDateDisplay === '1') {
          this.witnessInfo += this.cameraStore.photoDate.length > 0 ? this.dateform(
            this.cameraStore.photoDate).split(' ')[0] : S0301Constants.DEFAULTPHOTOTIME;
          this.witnessInfo += ' ';
        }
        if (blackboardDisplay.isTimeDisplay === '1') {
          this.witnessInfo += this.cameraStore.photoDate.length > 0 ? this.dateform(
            this.cameraStore.photoDate).split(' ')[1] : S0301Constants.DEFAULTPHOTOTIME;
        }
      }
    });
  }

  ngOnInit() { }

  dateform(dataStr: string) {
    if (dataStr === undefined || dataStr === null || dataStr.length === 0) {
      return '';
    } else {
      const date = moment(dataStr, 'YYYYMMDDHHmmss');
      if (date === undefined || date === null) {
        return '';
      } else {
        return date.format('YYYY/MM/DD HH:mm');
      }
    }
  }
  ngOnDestroy() {
    this.cameraStoreSubscription.unsubscribe();
  }

}
