import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoutingService } from '@service/app-route.service';
import {
  ChatRoomStoreService, ChatRoomStore,
  AppState, CommonStore, CommonStoreService, AppStateStoreService, HeadStateService, AppLoadingStoreService
} from '@store';
import { Store, select } from '@ngrx/store';
import { ChatAPI } from '@graphql/chatApi.service';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { AlertController, PopoverController, Platform } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';

@Component({
  selector: 'app-chatroom-edit-complete',
  templateUrl: './chatroom-edit-complete.page.html',
  styleUrls: ['./chatroom-edit-complete.page.scss', './chatroom-edit-complete.page.mobile.scss'],
})
export class ChatroomEditCompletePage implements OnInit, OnDestroy {
  platformtype: string;
  isPc: boolean;
  appState: AppState;
  commonStore: CommonStore;
  chatroomName: string;

  isEdit: boolean = false;

  public selectedUserList; // 選択済みユーザーリスト
  public initSelectedUsetList;

  private projectId;
  private chatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  constructor(
    public popoverController: PopoverController,
    public headStateService: HeadStateService,
    private routerService: RoutingService,
    private chatRoomStoreService: ChatRoomStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    public alertController: AlertController,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>,
    private platform: Platform
  ) {
    this.platformtype = 'chatroom-edit-complete-' + this.appStateService.getPlatform();
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }

  ngOnInit() {
    console.log('ChatroomEditPage ngOnInit');
    this.projectId = this.commonStore.projectId;
    this.subscribeChatRoomStore();
  }

  ngOnDestroy() {
    this.chatRoomStoreSubscription.unsubscribe();

  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chatroom list chatRoomStore', store);
      this.chatRoomStore = store;
      this.initSelectedUsetList = store.initSelectUsers;
      this.selectedUserList = store.chatSelectUsers;
      this.chatroomName = store.conversationName;

    });

  }

  ionViewDidEnter() {
    console.log('ChatRoomEditPage ionViewDidEnter');
  }

  //
  queryAssocaitedUser() {

    // APIから、チャット情報を取得
    const currentConversationId = this.chatRoomStore.conversationId;
    const successFunc = (associated) => {
      // console.log('associated list', associated);
      this.chatRoomStoreService.setChatAssociated(associated);
    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryAssocaitedUser(successFunc, failFunc, currentConversationId);

  }


  cancenButtonClick() {
    console.log('>>> cancel button click ...');
    const url = '/chatroom-edit-user-select';
    this.routerService.goRouteLink(url);
  }

  async commitButtonClick() {

    if (this.isEdit) {
      return;
    }
    this.isEdit = true;

    const conversationId = this.chatRoomStore.conversationId;

    // 削除したメンバー
    const delUserList = this.initSelectedUsetList.filter(item1 => !this.selectedUserList.some(item2 => (item2.userId === item1.userId)));
    // 追加したメンバー
    const addUserList = this.selectedUserList.filter(item1 => !this.initSelectedUsetList.some(item2 => (item2.userId === item1.userId)));

    if ((!delUserList || delUserList.length === 0) && (!addUserList || addUserList.length === 0)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00014,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              const url = '/chattalk';
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
              this.isEdit = false;
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    } else {
      this.appLoadingStoreService.isloading(true);
      const successFunc = () => {
        console.log('createConversation');
        this.appLoadingStoreService.isloading(false);
        this.queryAssocaitedUser();
        const url = '/chattalk';
        this.routerService.goRouteLink(url);
      };
      const failFunc = async (err) => {
        console.log('err', err);
        this.isEdit = false;
        this.appLoadingStoreService.isloading(false);
        if (this.appStateService.appState.isOffline) {
          // オフライン可能以外画面の場合
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        } else {
          // 登録に失敗した場合
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants[err],
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        }
      };

      const selectedUserList = [];
      for (const user of this.selectedUserList) {
        if (user.isChecked && user.isDisabled) {
          selectedUserList.push(user);
        }
      }

      ChatAPI.apiService.createAndDeleteUserProjectConversation(successFunc, failFunc, conversationId,
        addUserList, delUserList, this.projectId);
    }

  }

  async onPress(ev: Event, user) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [user.nameAndComp];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();
  }

  async onPressUser(ev: Event, user) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPressUser', ev);
    const messageArr1 = [user.nameAndComp];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  async onPressName(ev: Event, chatroomName) {
    console.log('onPressName', ev);
    const messageArr1 = [chatroomName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }



}
