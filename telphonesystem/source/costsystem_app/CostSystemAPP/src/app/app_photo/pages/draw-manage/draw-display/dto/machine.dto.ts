import { PlaceDto } from './place.dto';
export declare interface MachineDto {
  // 機器ID
  machineId?: number;
  // 機種名
  machineModel?: string;
  // 機番
  machineNumber?: string;
  // 記号
  unitMark?: string;
  // 系統名
  lineName?: string;
  // 機種区分
  modeltypeDivision?: string;
  // フロアID
  floorId?: number;

  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 部屋の場所一覧
  places?: PlaceDto[];

  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
}
