import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsshelfPageRoutingModule } from './productsshelf-routing.module';

import { ProductsshelfPage } from './productsshelf.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsshelfPageRoutingModule
  ],
  declarations: [ProductsshelfPage]
})
export class ProductsshelfPageModule {}
