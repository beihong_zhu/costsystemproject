import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FundrecorderPageRoutingModule } from './fundrecorder-routing.module';

import { FundrecorderPage } from './fundrecorder.page';
import { FundrecorderEditComponent } from './fundrecorder-edit/fundrecorder-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FundrecorderPageRoutingModule
  ],
  declarations: [
    FundrecorderPage,
    FundrecorderEditComponent],
  entryComponents: [
    FundrecorderEditComponent
  ]
})
export class FundrecorderPageModule { }
