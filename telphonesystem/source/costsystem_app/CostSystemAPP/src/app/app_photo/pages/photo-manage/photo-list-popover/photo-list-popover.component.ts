import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-photo-list-popover',
  templateUrl: './photo-list-popover.component.html',
  styleUrls: ['./photo-list-popover.component.scss'],
})
export class PhotoListPopoverComponent implements OnInit {
  datas: any;

  constructor(private params: NavParams) {
    this.datas = params.data.datas;
  }

  ngOnInit() { }

  click(data, type) {
    console.log('data', data);
    this.params.data.popover.dismiss(data);
  }

}
