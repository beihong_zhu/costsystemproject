import { NgModule } from '@angular/core';
import { PhotoListItemComponent } from './photo-list-item.component';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { AppStateStoreService, CommonStoreService } from '@store';



@NgModule({
  providers: [

    AppStateStoreService,
    CommonStoreService,

  ],
  declarations: [

    // PhotoListItemComponent,

  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoListItemComponent
      }
    ])
  ],
  entryComponents: [],
})
export class PhotoListItemModule { }
