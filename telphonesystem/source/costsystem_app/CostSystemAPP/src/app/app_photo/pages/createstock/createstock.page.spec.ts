import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreatestockPage } from './createstock.page';

describe('CreatestockPage', () => {
  let component: CreatestockPage;
  let fixture: ComponentFixture<CreatestockPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatestockPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreatestockPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
