import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, Platform } from '@ionic/angular';
import { WorkAddPage } from './work-add/work-add.page';
import { ProcessAddPage } from './process-add/process-add.page';
import { ProcessProcessGamenDto, ProcessDto, WorkDto } from './dto';
import { ProcessStoreService, CommonStoreService, AppStateStoreService, HeadStateService } from '@store';
import { API0801Service, API0802Service, API0805Service } from '@service/api';
import { API0801InDto, API0801OutDto, API0802InDto, API0802OutDto, API0805InDto, API0805OutDto } from '@service/dto';
import { S0801Constants, AlertWindowConstants } from '@constant/constant.photo';
import { RoutingService } from '@service/app-route.service';
import { CD0001Constants, ListConstants } from '@constant/code';
import { MessageConstants } from '@constant/message-constants';
import { PopoverController } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { ResultType } from '../../../../app_common/constant/common';
import { Process } from '@service/dto/api-08-01-out-dto';

@Component({
  selector: 'app-process-create',
  templateUrl: './process-create.page.html',
  styleUrls: ['./process-create.page.scss', './process-create.page.mobile.scss', './process-create.page.winapp.scss'],
})
/**
 * 工程登録画面
 */
export class ProcessCreatePage {

  public isPc = false;

  // 物件
  buildingName: string;
  // 案件
  selectProject: number;
  projectName: string;
  // 大工程
  bigprocessName: string;
  bigprocessList: Array<CodeDto>;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    edit: S0801Constants.EDIT,
    addProcessLabel: S0801Constants.ADDPROCESSLABEL,
    addWorkLabel: S0801Constants.ADDWORKLABEL,
    bigprocess03: CD0001Constants.CD0001_03,
    bigprocess03Value: CD0001Constants.CD0001_03_VALUE,
    bigprocess04: CD0001Constants.CD0001_04,
    bigprocess04Value: CD0001Constants.CD0001_04_VALUE
  };
  /**
   * 画面DTO
   */
  public gamenDto: ProcessProcessGamenDto = {};
  /**
   * コンストラクタ
   * @param modalController ダイアログコントローラ
   * @param platform Platform
   * @param workService ProcessStoreService
   * @param api0811Service 工程・作業情報一覧API
   * @param api0812Service テンプレート工程・作業情報取得API
   * @param api0813Service 工程・作業情報一括登録API
   * @param routerService RoutingService
   * @param alertController AlertController
   */
  constructor(public modalController: ModalController,
              private workService: ProcessStoreService,
              public api0801Service: API0801Service,
              public api0802Service: API0802Service,
              public api0805Service: API0805Service,
              private routerService: RoutingService,
              private commonStoreService: CommonStoreService,
              private appStateService: AppStateStoreService,
              public alertController: AlertController,
              public popoverController: PopoverController,
              public headStateService: HeadStateService,
              private platform: Platform
  ) {
    this.isPc = this.appStateService.appState.isPC;

    this.platformtype = 'processCreate-' + this.appStateService.getPlatform();
  }

  // ------------------EVENT------------------

  /**
   * 初期化する。
   */
  ionViewWillEnter() {
    this.gamenDto.selectedBigprocess = CD0001Constants.CD0001_03;

    this.buildingName = this.commonStoreService.commonStore.buildingName;
    this.selectProject = this.commonStoreService.commonStore.projectId;
    this.projectName = this.commonStoreService.commonStore.projectName;
    if (this.isPc) {
      this.bigprocessName = CD0001Constants.CD0001_03_VALUE;
    } else {
      this.bigprocessName = this.commonStoreService.commonStore.bigProcessName;
    }
    this.bigprocessList = ListConstants.CD0001_LIST;
    this.initData();

    // 編集方法
    this.headStateService.setEditFunction(this.editButtonClick);
  }

  /**
   * 大工程選択
   */
  comboxChange(bigProcess) {
    const bigprocessName = this.bigprocessName;
    Array.from({ length: this.bigprocessList.length }).map(async (_, i) => {
      if (this.bigprocessList[i].id === bigProcess) {
        this.bigprocessName = this.bigprocessList[i].value;
      }
    });
    // ionViewWillEnterとionChangeでinitDataをする
    if (this.bigprocessName !== bigprocessName) {
      this.initData();
    }
  }


  /**
   * 編集ボタン
   */
  editButtonClick = () => {
    if (this.isPc) {
      this.workService.updateProcessList(this.gamenDto.processList, this.selectProject, this.gamenDto.selectedBigprocess);
    } else {
      this.workService.updateProcessList(this.gamenDto.processList, this.selectProject, this.commonStoreService.commonStore.bigProcessId);
    }
    this.routerService.goRouteLink('/process-edit');
  }

  /**
   * 工程内容展開すること.
   * @param process 工程DTO
   */
  expandChange(process: ProcessDto): void {
    process.isOpen = !process.isOpen;
  }

  /**
   * 新規工程追加ダイアログ
   */
  async openAddProcessDialog() {
    const processDto: ProcessDto = {};
    if (this.gamenDto.processList) {
      processDto.displayOrder = this.gamenDto.processList.length + 1;
    } else {
      processDto.displayOrder = 1;
    }
    if (this.isPc) {
      processDto.bigProcessId = this.gamenDto.selectedBigprocess;
    } else {
      processDto.bigProcessId = this.commonStoreService.commonStore.bigProcessId;
    }
    if (this.gamenDto.processList && this.gamenDto.processList.length >= 99) {
      let msg = '';
      msg = MessageConstants.E00045.replace('{0}', '工程');
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else {
      const modal = await this.modalController.create({
        component: ProcessAddPage,
        componentProps: {
          value: processDto
        },
        cssClass: 'processAddPageModal-' + this.appStateService.getPlatform()
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
      // 追加ボタンを押下するか判断
      if (data != null) {
        this.initData();
      }
    }

  }

  /**
   * 新規作業追加ダイアログ
   * @param process 工程DTO
   */
  async openAddWorkDialog(process: ProcessDto) {
    const workDto: WorkDto = {};
    if (process.workList) {
      workDto.displayOrder = process.workList.length + 1;
    } else {
      workDto.displayOrder = 1;
    }
    workDto.processId = process.processId;
    workDto.projectId = this.commonStoreService.commonStore.projectId;
    if (this.isPc) {
      workDto.bigProcessId = this.gamenDto.selectedBigprocess;
    } else {
      workDto.bigProcessId = this.commonStoreService.commonStore.bigProcessId;
    }
    if (process.workList && process.workList.length >= 99) {
      let msg = '';
      msg = MessageConstants.E00045.replace('{0}', '作業');
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else {
      const modal = await this.modalController.create({
        component: WorkAddPage,
        componentProps: {
          value: workDto
        },
        cssClass: 'workAddPageModal-' + this.appStateService.getPlatform()
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
      // 変更ボタンを押下するか判断
      if (data != null) {
        this.initData();
      }
    }
  }

  // ------------------InDto------------------
  /**
   * 画面データを取得すること。
   */
  initData(): void {
    const inDto: API0801InDto = this.getCreate0801InDto();
    this.gamenDto.processList = [];
    this.api0801Service.postExec(inDto, (outDto: API0801OutDto) => {
      this.create0801Successed(outDto);
    },
      // fault
      () => {
      });
  }

  /**
   * API0811送信
   */
  getCreate0801InDto(): API0801InDto {
    const inDto: API0801InDto = {};
    inDto.projectId = this.selectProject;
    if (this.isPc) {
      inDto.bigProcessId = this.gamenDto.selectedBigprocess;
    } else {
      inDto.bigProcessId = this.commonStoreService.commonStore.bigProcessId;
    }
    return inDto;
  }

  /**
   * API0812送信
   */
  getCreate0802InDto(): API0802InDto {
    const inDto: API0802InDto = {};
    return inDto;
  }

  /**
   * API0813送信
   * @param outDto0802 API0812OutDto
   */
  getCreate0805InDto(outDto0802: API0802OutDto): API0805InDto {
    const inDto: API0805InDto = {};
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    if (this.isPc) {
      inDto.bigProcessId = this.gamenDto.selectedBigprocess;
    } else {
      inDto.bigProcessId = this.commonStoreService.commonStore.bigProcessId;
    }
    const processDisplayOrder = 'processDisplayOrder';
    const workDisplayOrder = 'workDisplayOrder';
    for (const process of outDto0802.processList) {
      process[processDisplayOrder] = process.displayProcessOrder;
      for (const work of process.workList) {
        work[workDisplayOrder] = work.displayWorkOrder;
      }
    }
    inDto.processList = outDto0802.processList;
    return inDto;
  }

  // ------------------OutDto------------------
  /**
   * 工程・作業情報一覧取得成功、API0801返信。
   * @param outDto API0801OutDto
   */
  async create0801Successed(outDto: API0801OutDto) {
    //  1-1-1.大工程情報に紐づく工程・作業情報を取得する
    if (outDto.processList && outDto.processList.length > 0) {
      this.gamenDto.processList = outDto.processList;
    } else if (outDto.resultCode === ResultType.WARN) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.WARNINGTITLE,
        message: MessageConstants.W00006,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else {
      // 1-1-2.大工程情報に紐づく工程・作業情報を取得できなかった場合
      this.api0802Service.postExec(this.getCreate0802InDto(), (outDto0802: API0802OutDto) => {
        this.create0802Successed(outDto0802);
      },
        // fault
        () => {
        });
    }
  }

  /**
   * テンプレート工程・作業情報取得成功、API0812返信。
   * @param outDto0802 API0802OutDto
   */
  async create0802Successed(outDto0802: API0802OutDto) {
    const processList: Process[] = outDto0802.processList;

    // 工程・作業情報一括登録API(API-08-13)を使用して、工程・作業情報を登録し、工程登録画面に表示する。
    this.api0805Service.postExec(this.getCreate0805InDto(outDto0802), (outDto0805: API0805OutDto) => {
      this.gamenDto.processList = processList;
    },
      // fault
      () => {
      });
  }

  async onPressBuilding(ev: Event) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPressBuilding', ev);
    const messageArr1 = [this.buildingName, this.projectName, this.bigprocessName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();
  }

  async onPress(ev: Event, title) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    if (this.isPc) {
      return;
    }
    const messageArr1 = [title];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();
  }
}

export class Project {
  projectId: number;
  projectName: string;
}

export class CodeDto {
  // コードID
  id?: string;
  // コード名
  value?: string;
}
