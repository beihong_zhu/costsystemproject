import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoutingService } from '@service/app-route.service';
import {
  AppState, ChatRoomStoreService, CommonStore, CommonStoreService, ChatRoomStore,
  AppStateStoreService, HeadStateService
} from '@store';
import { ChatAPI } from '@graphql/chatApi.service';
import { Store, select } from '@ngrx/store';
import { AlertController, PopoverController, Platform } from '@ionic/angular';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';

@Component({
  selector: 'chatroom-create-user-select',
  templateUrl: './chatroom-create-user-select.page.html',
  styleUrls: ['./chatroom-create-user-select.page.scss', './chatroom-create-user-select.page.mobile.scss'],
})
export class ChatroomCreateUserSelectPage implements OnInit, OnDestroy {
  platformtype: string;
  isPc: boolean;
  commonStore: CommonStore;
  userList = [];
  selectedUserList = [];
  appState: AppState;

  private projectId;
  private chatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  constructor(
    public popoverController: PopoverController,
    public headStateService: HeadStateService,
    private appStateService: AppStateStoreService,
    private routerService: RoutingService,
    public alertController: AlertController,
    private chatRoomStoreService: ChatRoomStoreService,
    private commonStoreService: CommonStoreService,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>,
    private platform: Platform
  ) {
    this.platformtype = 'chatroom-create-user-select-' + this.appStateService.getPlatform();
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }

  ngOnInit() {
    console.log('ChatroomCreateUserSelectPage ngOnInit');
    this.isPc = this.appState.isPC ? true : false;
    this.projectId = this.commonStore.projectId;
    this.subscribeChatRoomStore();

  }

  ngOnDestroy() {
    this.chatRoomStoreSubscription.unsubscribe();

  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chatroom list chatRoomStore', store);
      this.chatRoomStore = store;
      const userList = store.chatUsers;
      this.selectedUserList = store.chatSelectUsers;
      this.userList = [];
      for (const user of userList) {
        this.userList.push(user.usersInfo);
      }

    });

  }

  ionViewDidEnter() {
    console.log('ChatRoomCreatePage ionViewDidEnter');
    this.gobackFunction();
    this.getUserList();

  }

  // ユーザリストを取得
  getUserList() {
    // const projectId = this.commonStore.projectId;
    const successFunc = (userList) => {

      const chatUsers = [];
      const selectedUserList = [];
      const chatUsersSort = userList.filter((user) => {
        return user.usersInfo.authorId !== '1' && user.usersInfo.isInvalid === false;
      });

      // 氏名昇順で一覧表示する。
      userList = chatUsersSort.sort((a, b) => {
        if (a.usersInfo.userId === this.commonStore.userId) { return -1; }
        if (b.usersInfo.userId === this.commonStore.userId) { return 1; }
        const nameCompare = String(a.usersInfo.userName).localeCompare(String(b.usersInfo.userName));
        if (nameCompare === 0) {
          return String(a.usersInfo.companyName).localeCompare(String(b.usersInfo.companyName));
        } else {
          return nameCompare;
        }
      });

      for (const user of userList) {
        const userInfo = user.usersInfo;
        userInfo.nameAndComp = userInfo.userName + '、' + userInfo.companyName;
        // ログインユーザー(ルーム作成者)を「参加中」にする。
        // ログインユーザー(ルーム作成者)を画面右側に表示し、削除ボタンを非活性にする。
        if (userInfo.userId === this.commonStore.userId) {
          userInfo.isChecked = true;
          userInfo.disabled = true;
          selectedUserList.push(userInfo);
        } else {
          if (this.selectedUserList && this.selectedUserList.length > 0) {
            for (const selectedUser of this.selectedUserList) {
              if (userInfo.userId === selectedUser.userId) {
                userInfo.isChecked = selectedUser.isChecked;
                userInfo.disabled = false;
                selectedUserList.push(userInfo);
                break;
              } else {
                userInfo.isChecked = false;
                userInfo.disabled = false;
              }
            }
          } else {
            userInfo.isChecked = false;
            userInfo.disabled = false;
          }
        }
        chatUsers.push(user);
      }

      this.chatRoomStoreService.setChatUsers(chatUsers);
      this.chatRoomStoreService.setChatSelectUsers(selectedUserList);
      console.log('userList', chatUsers);

    };
    const failFunc = (err) => {
      console.log('err', err);
    };
    ChatAPI.apiService.queryConversationUser(successFunc, failFunc, this.projectId);

  }

  // 戻るボタン押下
  private gobackFunction() {

    const backFunction = async () => {

      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00011,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              this.headStateService.setBackFunction(null);
              const url = '/chatroomlist';
              this.routerService.goRouteLink(url);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
        ]
      });
      await alert.present();
    };

    this.headStateService.setBackFunction(backFunction);
  }

  nextButtonClick() {
    console.log('>>> cancel button click ...');
    const isCheckedList = [];
    for (const user of this.userList) {
      if (user.isChecked) {
        isCheckedList.push(user);
      }
    }

    this.chatRoomStoreService.setChatSelectUsers(isCheckedList);
    const url = '/chatroom-create-complete';
    this.routerService.goRouteLink(url);
  }

  async onPress(ev: Event, user) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [user.nameAndComp];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

}
