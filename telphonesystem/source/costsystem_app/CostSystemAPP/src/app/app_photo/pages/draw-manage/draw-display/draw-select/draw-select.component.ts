import { Component, OnInit } from '@angular/core';
import { S0601Constants } from '@constant/constant.photo';
import { NavParams } from '@ionic/angular';
import { DrawingDto } from '../dto';
import { AppStateStoreService } from '@store';

/**
 * 図面選択ダイアログ（図面一覧表示）
 */
@Component({
  selector: 'app-draw-select',
  templateUrl: './draw-select.component.html',
  styleUrls: ['./draw-select.component.scss', './draw-select.component.mobile.scss'],
})
export class DrawSelectComponent implements OnInit {

  public drawings: Array<DrawingDto>;
  public selectedDrawingId: number;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    ok: S0601Constants.DECISION,
    cancel: S0601Constants.CANCEL,
    title: S0601Constants.POPUPTITLE,
  };

  constructor(public navParams: NavParams, public appStateService: AppStateStoreService) {

    this.drawings = navParams.data.value;
    this.selectedDrawingId = navParams.data.selectedDrawingId;
    this.platformtype = 'drawSelectComponent-' + this.appStateService.getPlatform();
  }

  ngOnInit() { }

  selectedDrawing(serialNo: number) {
    this.selectedDrawingId = serialNo;

  }

  /**
   * クローズボタンをクリックすること。
   */
  closeClick(): void {
    this.navParams.data.modal.dismiss(null);
  }

  commitDrawing() {
    this.navParams.data.modal.dismiss(this.selectedDrawingId);

  }

}
