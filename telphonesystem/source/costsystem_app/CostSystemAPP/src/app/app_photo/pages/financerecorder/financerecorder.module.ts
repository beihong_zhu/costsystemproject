import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinancerecorderPageRoutingModule } from './financerecorder-routing.module';

import { FinancerecorderPage } from './financerecorder.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinancerecorderPageRoutingModule
  ],
  declarations: [FinancerecorderPage]
})
export class FinancerecorderPageModule {}
