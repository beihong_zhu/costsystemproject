import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CushPage } from './cush.page';

const routes: Routes = [
  {
    path: '',
    component: CushPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CushPageRoutingModule {}
