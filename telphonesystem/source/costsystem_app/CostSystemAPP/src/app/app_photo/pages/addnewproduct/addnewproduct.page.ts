import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addnewproduct',
  templateUrl: './addnewproduct.page.html',
  styleUrls: ['./addnewproduct.page.scss'],
})
export class AddnewproductPage implements OnInit {
  
  public  addProductInfo:any = {
    originalProductList:['代理平台' ,'淘宝平台'],
    original:'代理平台',
    costtypeList:['话费快充','话费慢充'],
    costType:'话费快充',
    provideSerivceList:[ '电信','移动',' 网通'],
    provideSevice:'移动',
    teletypeList: ['携带电话','固定电话'],
    teletype:'携带电话',
    provinceList:['北京','天津','上海','重庆','黑龙江','吉林','辽宁','内蒙古','江西 ','河南','湖南','湖北','河北','云南','贵州','浙江','江西','宁夏','青海','西藏','贵州','新疆','山西','山西','四川','重庆'],
    province:'北京',
    priceCostList:
    [{title :'核查价格',
    checked :false}, 
    {title :'核查成本',
    checked :false}],
    productStatusList: ['上架','下架'],
    productStatus : '上架'
   }
  constructor() { }

  ngOnInit() {
  }

}
