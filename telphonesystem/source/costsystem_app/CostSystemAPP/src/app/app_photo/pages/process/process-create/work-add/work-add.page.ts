import { Component, Inject } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { S0801Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0806Service } from '@service/api';
import { API0806InDto, API0806OutDto } from '@service/dto';
import { WorkDto } from '../dto';
import { ResultType } from '../../../../../app_common/constant/common';
import { TextCheck } from '../../../../util/text-check';
import { AppStateStoreService } from '@store';

@Component({
  selector: 'app-work-add',
  templateUrl: './work-add.page.html',
  styleUrls: ['./work-add.page.scss', './work-add.page.mobile.scss'],
})
/**
 * 作業追加ダイアログ
 */
export class WorkAddPage {
  isPc: boolean;
  public gamenDto: WorkDto = {};

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    addWorkTitle: S0801Constants.ADDWORKTITLE,
    displayOrder: S0801Constants.DISPLAYORDER,
    addName: S0801Constants.ADDNAME,
    message: S0801Constants.MESSAGE,
    register: S0801Constants.REGISTER,
    cancel: S0801Constants.CANCEL,
    ok: S0801Constants.CHANGE,
    workMsg: S0801Constants.WORKMSG
  };
  /**
   * コンストラクタ
   * @param alertController AlertController
   * @param navParams 作業DTO
   * @param api0806Service 作業追加API(API-08-06)
   */
  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    public api0806Service: API0806Service,
    private appStateService: AppStateStoreService
  ) {
    const workDto: WorkDto = navParams.data.value;
    this.gamenDto.workName = '';
    this.gamenDto.displayOrder = workDto.displayOrder;
    this.gamenDto.processId = workDto.processId;
    this.gamenDto.bigProcessId = workDto.bigProcessId;
    this.gamenDto.projectId = workDto.projectId;
    this.isPc = this.appStateService.appState.isPC;
    this.platformtype = 'workAdd-' + this.appStateService.getPlatform();
  }
  /**
   * クローズボタンをクリックすること。
   */
  onNoClick(): void {
    this.navParams.data.modal.dismiss(null);
  }
  /**
   * 追加ボタンをクリックすること。
   */
  async onAddClick() {



    if (!TextCheck.textCheck(this.gamenDto.workName)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    this.api0806Service.postExec(this.getAddInDto(), (outDto: API0806OutDto) => {
      this.addSuccessed(outDto);
    },
      // fault
      () => {
        this.navParams.data.modal.dismiss(null);
      });
  }
  /**
   * 追加成功、API0806返信。
   * @param outDto API0806OutDto
   */
  async addSuccessed(outDto: API0806OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: this.isPc ? MessageConstants.I00002 : MessageConstants.I00022,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss(this.gamenDto);
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else if (outDto.resultCode === ResultType.BUSINESSERROR) {
      // エラーメッセージ設定
      let msg = '';
      if (outDto.messageCode === 'E00129') {
        msg = MessageConstants[outDto.messageCode].replace('{0}', this.gamenDto.workName.trim());
      } else if (outDto.messageCode === 'E00045') {
        msg = MessageConstants[outDto.messageCode].replace('{0}', this.gamenLable.workMsg.trim());
      }
      // エラーメッセージポップアップ
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    }
  }
  /**
   * API0806送信
   */
  getAddInDto(): API0806InDto {
    const workEditInDto: API0806InDto = {};
    workEditInDto.workName = this.gamenDto.workName.trim();
    workEditInDto.processId = this.gamenDto.processId;
    workEditInDto.bigProcessId = this.gamenDto.bigProcessId;
    workEditInDto.projectId = this.gamenDto.projectId;
    return workEditInDto;
  }

  /**
   * 作業名入力欄内に値が入力された場合、活性化。
   */
  isUpdated() {
    return this.gamenDto.workName === null || this.gamenDto.workName.trim() === '';
  }

  focusInWorkName() {
    document.getElementById('workName').style.color = '#0097E0';
  }
  focusOutWorkName() {
    document.getElementById('workName').style.color = '#333333';
  }
}
