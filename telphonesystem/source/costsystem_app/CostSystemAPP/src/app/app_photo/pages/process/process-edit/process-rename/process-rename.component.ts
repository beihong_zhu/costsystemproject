import { Component, OnInit } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { ProcessDto } from '@pages/process/process-edit/dto';
import { S0801Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0808Service } from '@service/api';
import { API0808InDto, API0808OutDto } from '@service/dto';
import { ResultType, UpdateMessage } from '../../../../../app_common/constant/common';
import { TextCheck } from '../../../../util/text-check';
import { AppStateStoreService, CommonStoreService } from '@store';


@Component({
  selector: 'app-process-rename',
  templateUrl: './process-rename.component.html',
  styleUrls: ['./process-rename.component.scss', './process-rename.component.mobile.scss'],
})
/**
 * 工程名変更ダイアログ
 */
export class ProcessRenameComponent implements OnInit {
  isPc: boolean;
  /**
   * 画面DTO
   */
  public gamenDto: ProcessDto = {};

  bigProcessId: string;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    processChangeTitle: S0801Constants.PROCESSCHANGETITLE,
    displayOrder: S0801Constants.DISPLAYORDER,
    changeName: S0801Constants.CHANGENAME,
    message: S0801Constants.MESSAGE,
    change: S0801Constants.CHANGE,
    cancel: S0801Constants.CANCEL,
    processMsg: S0801Constants.PROCESSMSG
  };

  /**
   * コンストラクタ
   * @param navParams 工程DTO
   * @param alertController AlertController
   * @param API0808Service 工程名変更API(API-08-08)
   */
  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    public api0808Service: API0808Service,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService
  ) {
    const processDto: ProcessDto = navParams.data.value;
    this.gamenDto.processId = processDto.processId;
    this.gamenDto.processName = processDto.processName;
    this.gamenDto.displayOrder = processDto.displayOrder;
    this.isPc = this.appStateService.appState.isPC;
    this.bigProcessId = navParams.data.bigProcessId;
    this.platformtype = 'processRename-' + this.appStateService.getPlatform();
  }

  /**
   * 初期化する。
   */
  ngOnInit() {
  }

  /**
   * クローズボタンをクリックすること。
   */
  closeClick() {
    this.navParams.data.modal.dismiss(null);
  }

  /**
   * 表示順入力欄内、工程名入力欄内のどちらにも値が入力された場合、活性化。
   */
  isUpdated(): boolean {
    return this.gamenDto.displayOrder === null ||
      this.gamenDto.processName === null ||
      this.gamenDto.processName.trim() === '';
  }

  /**
   * 変更ボタンをクリックすること。
   */
  async updateClick() {
    const gamenDto = this.gamenDto;
    const reg = /^[0-9]{1,2}$/;


    if (!TextCheck.textCheck(gamenDto.processName)) {
      const alertWindow = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alertWindow.present();
      return;
    }

    if (gamenDto.displayOrder !== null && gamenDto.displayOrder > 0 && gamenDto.displayOrder <= 99
      && reg.test(gamenDto.displayOrder.toString())) {
      this.updateData();
      return;
    }
    const alert = await this.alertController.create({
      header: AlertWindowConstants.ERRORTITLE,
      message: MessageConstants.E00009.replace('{{value}}', this.gamenLable.displayOrder),
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    alert.addEventListener('ionAlertDidPresent', () => {
      const alertButton = alert.getElementsByClassName('alert-button');
      if (alertButton && alertButton.length > 0) {
        (alertButton[0] as HTMLElement).focus();
      }
    });
    await alert.present();
  }
  /**
   * データを変更すること。
   */
  async updateData() {
    this.api0808Service.postExec(this.getEditInDto(), (outDto: API0808OutDto) => {
      this.editSuccessed(outDto);
    },
      // fault
      () => {
        this.navParams.data.modal.dismiss(null);
      });
  }

  /**
   * 変更成功、API0808返信。
   * @param outDto API0808OutDto
   */
  async editSuccessed(outDto: API0808OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: this.isPc ? MessageConstants.I00016 : MessageConstants.I00023,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss(this.gamenDto);
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else if (outDto.resultCode === ResultType.BUSINESSERROR) {
      // エラーメッセージ設定
      let msg: string = '';
      msg = UpdateMessage.setMessage(
        outDto.messageCode,
        outDto.message);
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss(this.gamenDto);
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    }
  }
  /**
   * API0808送信
   */
  getEditInDto(): API0808InDto {
    const processEditInDto: API0808InDto = {};
    processEditInDto.projectId = this.commonStoreService.commonStore.projectId;
    processEditInDto.bigProcessId = this.bigProcessId;
    processEditInDto.processId = this.gamenDto.processId;
    processEditInDto.processName = this.gamenDto.processName.trim();
    processEditInDto.displayOrder = this.gamenDto.displayOrder;
    // TODO processEditInDto.更新ユーザ
    return processEditInDto;
  }

  /**
   * キャンセルボタンをクリックすること。
   */
  cancelClick() {
    this.navParams.data.modal.dismiss(null);
  }

  focusInSort() {
    document.getElementById('sort').style.color = '#0097E0';
  }
  focusOutSort() {
    document.getElementById('sort').style.color = '#333333';
  }

  focusInProcessName() {
    document.getElementById('processName').style.color = '#0097E0';
  }
  focusOutProcessName() {
    document.getElementById('processName').style.color = '#333333';
  }
}
