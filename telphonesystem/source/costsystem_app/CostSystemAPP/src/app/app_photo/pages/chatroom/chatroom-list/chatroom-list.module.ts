import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomListPage } from './chatroom-list.page';

@NgModule({
  providers: [],
  declarations: [ChatroomListPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomListPage
      }
    ])
  ]
})
export class ChatroomListPageModule { }
