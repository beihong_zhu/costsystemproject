import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CushPageRoutingModule } from './cush-routing.module';

import { CushPage } from './cush.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CushPageRoutingModule
  ],
  declarations: [CushPage]
})
export class CushPageModule {}
