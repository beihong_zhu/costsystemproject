import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FinancerecorderPage } from './financerecorder.page';

describe('FinancerecorderPage', () => {
  let component: FinancerecorderPage;
  let fixture: ComponentFixture<FinancerecorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinancerecorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FinancerecorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
