import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FundrecorderPage } from './fundrecorder.page';

const routes: Routes = [
  {
    path: '',
    component: FundrecorderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FundrecorderPageRoutingModule {}
