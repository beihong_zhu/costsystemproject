import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewproductonshelfPage } from './newproductonshelf.page';

describe('NewproductonshelfPage', () => {
  let component: NewproductonshelfPage;
  let fixture: ComponentFixture<NewproductonshelfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewproductonshelfPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewproductonshelfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
