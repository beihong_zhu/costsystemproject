import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { API3007Service } from '@service/api';
import { API3007InDto, API3007OutDto } from '@service/dto';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysOnShelfProductInfoDto } from './dto';
import { OnshelfEditComponent } from './onshelf-edit/onshelf-edit.component';

@Component({
  selector: 'app-onshelf',
  templateUrl: './onshelf.page.html',
  styleUrls: ['./onshelf.page.scss'],
})
export class OnshelfPage implements OnInit {


  public onshelfInfo: any = {
   /* bussinessInfoList: ['账户', '支付宝', '微信'],
    bussiness: '账户',*/
    /*bussiTypeList: ['充值', '提现', '支付', '退款', '即反佣金', '退款撤销', '充值撤销'],
    bussiType: '充值',*/
 /*   provinceList[
      {
        title: '北京',
        checked: false
      },
      {
        title: '上海',
        checked: false
      },
      {
        title: '天津',
        checked: false
      },
      {
        title: '辽宁',
        checked: false
      },
      {
        title: '吉林',
        checked: false
      },
      {
        title: '河北',
        checked: false
      },
      {
        title: '山西',
        checked: false
      },
      {
        title: '江苏',
        checked: false
      },
      {
        title: '浙江',
        checked: false
      },
      {
        title: '安徽',
        checked: false
      },
      {
        title: '福建',
        checked: false
      },
      {
        title: '江西',
        checked: false
      },
      {
        title: '山东',
        checked: false
      },
      {
        title: '湖南',
        checked: false
      },
      {
        title: '湖北',
        checked: false
      },
      {
        title: '广东',
        checked: false
      },
      {
        title: '海南',
        checked: false
      },
      {
        title: '四川',
        checked: false
      },
      {
        title: '贵州',
        checked: false
      },
      {
        title: '云南',
        checked: false
      },
      {
        title: '陕西',
        checked: false
      },
      {
        title: '甘肃',
        checked: false
      },
      {
        title: '青海',
        checked: false
      },
      {
        title: '重庆',
        checked: false
      },
      {
        title: '新疆',
        checked: false
      },
      {
        title: '西藏',
        checked: false
      },
      {
        title: '移动',
        checked: false
      },
      {
        title: '电信',
        checked: false
      },
      {
        title: '网通',
        checked: false
      }
    ] */
    
     agencylist: ['淘宝商城', '代理'],
     agency: '淘宝商城',
     producttypelist: ['话充快充', '话费慢充'],
     producttype: '话充快充',
     productstatuslist: ['上架', '下架'],
     productstatus: '上架',
     numbertypelist: ['手机', '固定电话'],
     numbertype: '手机',
     provideservicelist: ['移动', '网通', '联通'],
     provideservice: '移动'
   
  };

  public agencyname: string;
  public channelname: string;
  public productname: string;
  public value: string;
  public i: number;
  public sysOnShelfProductInfoList: Array<SysOnShelfProductInfoDto> = new Array<SysOnShelfProductInfoDto>();

  private sysOnShelfProductInfoEdit: SysOnShelfProductInfoDto;

  constructor(   
    public alertController: AlertController,
    public modalController: ModalController,
    private api3007Service: API3007Service) { }

  ngOnInit() {


  }

    /**
   *  商品画面重新展示
   */
     reInit(): void {
      this.clearCache();
    }
  

  public onSearch() {
    this.api3007ServicePostExec();
  }

    public onEdit() {
    if (this.sysOnShelfProductInfoEdit !== null && this.sysOnShelfProductInfoEdit !== undefined) {
      this.openEditPage(this.sysOnShelfProductInfoEdit);
    }
    this.sysOnShelfProductInfoEdit = null;
  }

  private get3007InDto() {
    let api3007InDto: API3007InDto = {};

    //let arr = new Array();
    //arr[0] = "a";
    // arr[1] = "b";
    // let values: string = "";
    // for (const value of arr) {
    //  values += value + ",";
    // }
    //values = values.substring(0, values.length - 1);

     api3007InDto.agencyName = this.agencyname;
     api3007InDto.value = this.value;
     api3007InDto.productType = this.onshelfInfo.producttype;
     api3007InDto.productStatus = this.onshelfInfo.productstatus;

    
    return api3007InDto;
  }

  private api3007ServicePostExec() {

    this.api3007Service.postExecNew(this.get3007InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3007successed(response);
      }
    })
      .catch(ApiResponse.errorHandler);
  }

  private api3007successed(outDto: API3007OutDto) {
    this.sysOnShelfProductInfoList = new Array<SysOnShelfProductInfoDto>();

    const loadSysOnShelfProductInfo = JSON.parse(JSON.stringify(outDto.onShelfProductModelResList));

    for (const sysOnShelfProductInfoModel of loadSysOnShelfProductInfo) {
      let sysOnShelfProductInfoDto: SysOnShelfProductInfoDto = new SysOnShelfProductInfoDto();
      sysOnShelfProductInfoDto.AgentReduceName = sysOnShelfProductInfoModel.AgentReduceName;
      sysOnShelfProductInfoDto.productName = sysOnShelfProductInfoModel.productName;
      sysOnShelfProductInfoDto.productType = sysOnShelfProductInfoModel.productType;
      sysOnShelfProductInfoDto.spId = sysOnShelfProductInfoModel.spId;
      sysOnShelfProductInfoDto.value = sysOnShelfProductInfoModel.value;
      sysOnShelfProductInfoDto.price = sysOnShelfProductInfoModel.price;
      sysOnShelfProductInfoDto.disaccount = sysOnShelfProductInfoModel.disaccount;
      sysOnShelfProductInfoDto.checkPrice = sysOnShelfProductInfoModel.checkPrice;
      sysOnShelfProductInfoDto.checkCost = sysOnShelfProductInfoModel.checkCost;
      sysOnShelfProductInfoDto.status = sysOnShelfProductInfoModel.status;
      

      this.sysOnShelfProductInfoList.push(sysOnShelfProductInfoDto);
    }
  }

  private async openEditPage(sysOnShelfProductInfo: SysOnShelfProductInfoDto) {
    const modal = await this.modalController.create({
      component: OnshelfEditComponent,
      componentProps: {
        value: sysOnShelfProductInfo
      },
      cssClass: 'onshelfEditModal',
      backdropDismiss: false
    });
    await modal.present();
    const modalOnShelf: any = await modal.onDidDismiss();
    // 按钮按下的判断
    if (modalOnShelf.data !== null) {
      this.reInit();
    }
  }

  /**
   * 缓存清除
   */
   private clearCache(): void {
    this.sysOnShelfProductInfoList = null;
    this.sysOnShelfProductInfoList = null;
  }

}
