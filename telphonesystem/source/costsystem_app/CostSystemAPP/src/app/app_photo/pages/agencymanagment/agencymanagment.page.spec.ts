import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgencymanagmentPage } from './agencymanagment.page';

describe('AgencymanagmentPage', () => {
  let component: AgencymanagmentPage;
  let fixture: ComponentFixture<AgencymanagmentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencymanagmentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgencymanagmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
