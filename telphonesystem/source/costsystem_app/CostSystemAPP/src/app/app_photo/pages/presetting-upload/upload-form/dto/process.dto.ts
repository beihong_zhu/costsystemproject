export class ProcessDto {
  processId?: number;
  isOpen?: boolean;
  displayOrder?: number;
  processName?: string;
}
