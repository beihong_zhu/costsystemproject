import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZeroaccountPageRoutingModule } from './zeroaccount-routing.module';

import { ZeroaccountPage } from './zeroaccount.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ZeroaccountPageRoutingModule
  ],
  declarations: [ZeroaccountPage]
})
export class ZeroaccountPageModule {}
