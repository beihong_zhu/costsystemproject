import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoutingService } from '@service/app-route.service';
import {
  ChatRoomStoreService,
  ChatRoomStore,
  CommonStore,
  CommonStoreService,
  AppState,
  AppStateStoreService,
  AppLoadingStoreService
} from '@store';
import { ChatAPI, } from '@graphql/chatApi.service';
import { Store, select } from '@ngrx/store';
import { AlertController, PopoverController, Platform } from '@ionic/angular';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { TextCheck } from '../../../util/text-check';


@Component({
  selector: 'chatroom-create-complete',
  templateUrl: './chatroom-create-complete.page.html',
  styleUrls: ['./chatroom-create-complete.page.scss', './chatroom-create-complete.page.mobile.scss'],
})
export class ChatroomCreateCompletePage implements OnInit, OnDestroy {
  platformtype: string;
  isPc: boolean;
  appState: AppState;
  commonStore: CommonStore;
  selectedUserList = [];
  chatroomName: string;

  isCreate: boolean = false;


  private userId;
  private projectId;
  private chatRoomStore: ChatRoomStore;
  private chatRoomStoreSubscription: Subscription;

  constructor(
    public popoverController: PopoverController,
    private routerService: RoutingService,
    private chatRoomStoreService: ChatRoomStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    public alertController: AlertController,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>,
    private platform: Platform
  ) {
    this.platformtype = 'chatroom-create-complete-' + this.appStateService.getPlatform();
    // APPストアに保存した端末情報を取得
    this.appState = this.appStateService.appState;
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;
  }

  ngOnInit() {
    console.log('ChatroomCreateCompletePage ngOnInit');
    this.isPc = this.appState.isPC ? true : false;
    this.userId = this.commonStore.userId;
    this.projectId = this.commonStore.projectId;
    this.subscribeChatRoomStore();

  }

  ngOnDestroy() {
    this.chatRoomStoreSubscription.unsubscribe();

  }

  // chatRoomStore subscribe
  subscribeChatRoomStore() {

    this.chatRoomStoreSubscription = this.chatRoomStoreService.chatRoomStoreSubscription(store => {
      console.log('chatroom list chatRoomStore', store);
      this.chatRoomStore = store;
      this.selectedUserList = store.chatSelectUsers;
    });

  }

  ionViewDidEnter() {
    console.log('ChatRoomCreatePage ionViewDidEnter');
  }

  cancenButtonClick() {
    console.log('>>> cancel button click ...');
    this.commonStoreService.setCommon(this.commonStore);
    const url = '/chatroom-create-user-select';
    this.routerService.goRouteLink(url);
  }

  async commitButtonClick() {
    if (this.isCreate) {
      return;
    }

    this.isCreate = true;

    if (!this.chatroomName || this.chatroomName.trim().length === 0) {
      const message = `Input Chatroom Name`;
      // 文字が入力されていない場合
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00038,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      this.isCreate = false;
      return;
    }

    if (!TextCheck.textCheck(this.chatroomName)) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      this.isCreate = false;
      return;
    }

    this.appLoadingStoreService.isloading(true);
    // create Conversation
    const successFunc = (createConversation) => {
      console.log('createConversation', createConversation);
      this.appLoadingStoreService.isloading(false);
      // this.goChatRoomList();
      this.goChattalk(createConversation.data.createConversationDev);
    };
    const failFunc = async (err) => {
      console.log('err', err);
      // 登録に失敗した場合
      this.isCreate = false;
      this.appLoadingStoreService.isloading(false);
      if (this.appStateService.appState.isOffline) {
        // オフライン可能以外画面の場合
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          message: MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL),
          buttons: [AlertWindowConstants.OKENGLISHBUTTON],
          backdropDismiss: false
        });
        await alert.present();
      } else {
        // 登録に失敗した場合
        const alert = await this.alertController.create({
          header: AlertWindowConstants.ERRORTITLE,
          message: MessageConstants[err],
          buttons: [AlertWindowConstants.OKENGLISHBUTTON],
          backdropDismiss: false
        });
        await alert.present();
      }
    };
    ChatAPI.apiService.createConversation(successFunc, failFunc,
      this.selectedUserList, this.chatroomName.trim(), this.projectId, this.userId);

  }

  async onPress(ev: Event, user) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [user.nameAndComp];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


  goChatRoomList() {

    this.routerService.goRouteLink('/chatroomlist');

  }
  goChattalk(conversation) {
    // console.log('conversation', conversation);
    const conversationId = conversation.conversationId;
    const conversationName = conversation.conversationName;
    this.chatRoomStoreService.setChatCurrentConversation(conversation);
    this.chatRoomStoreService.setChatConversationId(conversationId);
    this.chatRoomStoreService.setChatConversationName(conversationName);

    this.chatRoomStoreService.setChatAssociated([]);

    this.chatRoomStoreService.setChatMessages([]);
    const url = '/chattalk';
    this.routerService.goRouteLink(url);

  }

  isDisabled() {
    return !this.chatroomName || this.chatroomName === null || this.chatroomName.trim() === '';
  }

  focusInName() {
    document.getElementById('name').style.color = '#0097E0';
  }
  focusOutName() {
    document.getElementById('name').style.color = '#333333';
  }

}
