import { Component, OnInit } from '@angular/core';
import { CommonConstants, ManagerConstants, MoneyConstants, S1801Constants } from '@constant/constant.photo';
import { API0112Service } from '@service/api/api-01-12.service';
import { AgentSummaryDto } from './dto';

@Component({
  selector: 'app-agent-summary',
  templateUrl: './agent-summary.page.html',
  styleUrls: ['./agent-summary.page.scss'],
})
export class AgentSummaryPage implements OnInit {

  // title
  public title: string = S1801Constants.TITLE;
  // manager
  public manager: string = S1801Constants.MANAGER;
  // select money
  public selectMoney: string = S1801Constants.SELECTMONEY;
  // space
  public space: string = '           ';
  // money space
  public moneySpace: string = ' ';
  // create time
  public createTime: string = S1801Constants.CREATETIME;
  // agent name
  public agentName: string = S1801Constants.AGENTNAME;
  // agent reduce name
  public agentReduceName: string = S1801Constants.AGENTREDUCENAME;
  // order money
  public orderMoney: string = S1801Constants.ORDERMONEY;
  // order success money
  public orderSuccessMoney: string = S1801Constants.ORDERSUCCESSMONEY;
  // order total summary
  public orderTotalSummary: string = S1801Constants.CREATETIME;
  // order running summary
  public orderRunningSummary: string = S1801Constants.ORDERRUNNINGSUMMARY;
  // order success summary
  public orderSuccessSummary: string = S1801Constants.ORDERSUCCESSSUMMARY;
  // order success rate
  public orderSuccessRate: string = S1801Constants.ORDERSUCCESSRATE;
  // order success time
  public orderSuccessTime: string = S1801Constants.ORDERSUCCESSTIME;
  // order failure time
  public orderFailureTime: string = S1801Constants.ORDERFAILURETIME;
  // order three mins rate
  public orderThreeMinsRate: string = S1801Constants.ORDERTHREEMINSRATE;
  // order ten mins rate
  public orderTenMinsRate: string = S1801Constants.ORDERTENMINSRATE;

  // agent summary list
  public agentSummaryList: Array<AgentSummaryDto> = new Array<AgentSummaryDto>();

  // agent summary title
  public agentSummaryTitle: string[] = [
    this.agentName,
    this.agentReduceName,
    this.orderMoney,
    this.orderSuccessMoney,
    this.orderTotalSummary,
    this.orderRunningSummary,
    this.orderSuccessSummary,
    this.orderSuccessRate,
    this.orderSuccessTime,
    this.orderFailureTime,
    this.orderThreeMinsRate,
    this.orderTenMinsRate
  ];

  // manager list
  public managerList: string[] = [
    ManagerConstants.MOBILE,
    ManagerConstants.TELECOM,
    ManagerConstants.UNICOM
  ];

  // money list
  public MoneyList: string[] = [
    MoneyConstants.TENYUAN,
    MoneyConstants.TWENTYYUAN,
    MoneyConstants.THIRTYYUAN,
    MoneyConstants.FIFTYYUAN,
    MoneyConstants.ONEHUNDREDYUAN,
    MoneyConstants.TWOHUNDREDYUAN,
    MoneyConstants.THREEHUNDREDYUAN,
    MoneyConstants.FIVEHUNDREDYUAN
  ];

  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;

  constructor(
    private api0112Service: API0112Service
  ) { }

  ngOnInit() {

  }
}
