import { Component, OnInit } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { AppStateStoreService } from '@store';



@Component({
  selector: 'app-process-add',
  templateUrl: './download-confirm.page.html',
  styleUrls: ['./download-confirm.page.scss'],
})

export class DownloadConfirmPage implements OnInit {
  public isPc = false;
  public fileName: string = '';
  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    private appStateService: AppStateStoreService,
  ) {
  }

  ngOnInit() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.fileName = this.navParams.data.value;
  }

  onDownLoadClick() {
    this.navParams.data.modal.dismiss(true);
  }


}
