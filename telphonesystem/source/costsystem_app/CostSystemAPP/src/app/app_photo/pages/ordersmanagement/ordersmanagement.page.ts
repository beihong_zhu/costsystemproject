import { Component, OnInit } from '@angular/core';
import { API3011Service } from '@service/api';
import { API3011InDto, API3011OutDto } from '@service/dto';
import { AppState, CommonStore } from '@store';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysOrderManagementDto } from './dto';
import { AlertController, ModalController } from '@ionic/angular';


@Component({
  selector: 'app-ordersmanagement',
  templateUrl: './ordersmanagement.page.html',
  styleUrls: ['./ordersmanagement.page.scss'],
})
export class OrdersmanagementPage implements OnInit {

  public orderorigin :string;
  public channelname : string;
  public agencyname : string;
  public agecnylimited : string;
  public locallimited : string;
  public channellimited : string;
  public  telnumber : string ;
  public  productname : string;
  
  public ocrManufacturingDate: string;

  public sysUserList: Array<SysOrderManagementDto> = new Array<SysOrderManagementDto>();

  public  ordermanagerInfo:any = {
      orderoriginList:['天猫' ,'拼多多'],
      origin:'天猫',
      provideseviceList:['电信' ,'网通','移动'],
      providesevice : '移动',
      telphonetypeList:['手机' ,'固话','宽带'],
      telphonetype : '手机',
      statusList:['成功' ,'处理中','失败'],
      status:'成功',
      provinceList :[
        {title : '北京',
        checked :false},
        {title : '上海',
        checked :false},
        {title : '天津',
        checked :false},
        {title : '辽宁',
        checked :false},
        {title : '吉林',
        checked :false},
        {title : '河北',
        checked :false},
        {title : '山西',
        checked :false},
        {title : '江苏',
        checked :false},
        {title : '浙江',
        checked :false},
        {title : '安徽',
        checked :false},
        {title : '福建',
        checked :false},
        {title : '江西',
        checked :false},
        {title : '山东',
        checked :false},
        {title : '湖南',
        checked :false},
        {title : '湖北',
        checked :false},
        {title : '广东',
        checked :false},
        {title : '海南',
        checked :false},
        {title : '四川',
        checked :false},
        {title : '贵州',
        checked :false},
        {title : '云南',
        checked :false},
        {title : '陕西',
        checked :false},
        {title : '甘肃',
        checked :false},
        {title : '青海',
        checked :false},
        {title : '重庆',
        checked :false},
        {title : '新疆',
        checked :false},
        {title : '西藏',
        checked :false},
        {title : '移动',
        checked :false},
        {title : '电信',
        checked :false},
        {title : '网通',
        checked :false}, 
      ]
   }

   public sysOdermanagementList: Array<SysOrderManagementDto> = new Array<SysOrderManagementDto>();
   private sysOdermanagementEdit: SysOrderManagementDto;

  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    private api3011Service: API3011Service) { }

  ngOnInit() {
  }

  public sysUserChkClick(sysFundRecorder: any, sysFundRecorderList: any) {
    if (sysFundRecorder.isChecked) {
      for (const getSysFundRecorder of sysFundRecorderList) {
        if (getSysFundRecorder !== sysFundRecorder) {
          getSysFundRecorder.isChecked = false;
        }
      }
      // 设置公共对象
      this.sysOdermanagementEdit = new SysOrderManagementDto();
      this.sysOdermanagementEdit = sysFundRecorder;
    } else {
      this.sysOdermanagementEdit = null;
    }
  }

  public onSearch() {
    // this.api3011ServicePostExec();

  }

  private get3011InDto() {
    let api3011InDto: API3011InDto = {};
    //api3011InDto.orderorigin = this.orderorigin;
    api3011InDto.locallimited  = this.locallimited;
    api3011InDto.channellimited = this.channellimited;
    api3011InDto.agecnylimited  =  this.agecnylimited;
    api3011InDto.channelname  = this.channelname;
    api3011InDto.agencyname =  this.agencyname;
    api3011InDto.telnumber = this.telnumber;
    //api3011InDto.productname = this.productname;
    //api3011InDto.providesevice  = this.ordermanagerInfo.providesevice;
    //api3011InDto.teletype =  this.ordermanagerInfo.telphonetype;
    api3011InDto.status = this.ordermanagerInfo.status;
    //api3011InDto.ocrManufacturingDate = this.ocrManufacturingDate;

    return api3011InDto;
  }

  private api3011ServicePostExec() {

    this.api3011Service.postExecNew(this.get3011InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3011successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api3011successed(outDto: API3011OutDto) {
    this.sysUserList = new Array<SysOrderManagementDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.orderManagementModelResList));

    for (const sysUserInfoModel of loadSysusers) {
      let sysUserDto: SysOrderManagementDto = new SysOrderManagementDto();
      sysUserDto.AgentReduceName = sysUserInfoModel.AgentReduceName;
      sysUserDto.abbreviation = sysUserInfoModel.abbreviation;
      sysUserDto.reqId = sysUserInfoModel.reqId;
      sysUserDto.orderId = sysUserInfoModel.orderId;
      sysUserDto.billId = sysUserInfoModel.billId;
      sysUserDto.number = sysUserInfoModel.number;
      sysUserDto.money = sysUserInfoModel.money;
      sysUserDto.status = sysUserInfoModel.status;
      sysUserDto.ts = sysUserInfoModel.ts;

      this.sysUserList.push(sysUserDto);
    }
  }
}
