import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MutexPage } from './mutex.page';

const routes: Routes = [
  {
    path: '',
    component: MutexPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MutexPageRoutingModule {}
