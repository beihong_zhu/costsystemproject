import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnshelfPage } from './onshelf.page';

describe('OnshelfPage', () => {
  let component: OnshelfPage;
  let fixture: ComponentFixture<OnshelfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnshelfPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnshelfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
