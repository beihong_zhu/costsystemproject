import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewagencyPageRoutingModule } from './newagency-routing.module';

import { NewagencyPage } from './newagency.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewagencyPageRoutingModule
  ],
  declarations: [NewagencyPage]
})
export class NewagencyPageModule {}
