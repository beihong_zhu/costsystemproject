import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateregularPageRoutingModule } from './createregular-routing.module';

import { CreateregularPage } from './createregular.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateregularPageRoutingModule
  ],
  declarations: [CreateregularPage]
})
export class CreateregularPageModule {}
