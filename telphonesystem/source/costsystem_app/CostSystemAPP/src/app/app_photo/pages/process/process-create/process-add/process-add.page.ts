import { Component } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { ProcessDto } from '../dto';
import { S0801Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0803Service } from '@service/api';
import { API0803InDto, API0803OutDto } from '@service/dto';
import { CommonStoreService, AppStateStoreService } from '@store';
import { ResultType } from '../../../../../app_common/constant/common';
import { TextCheck } from '../../../../util/text-check';

@Component({
  selector: 'app-process-add',
  templateUrl: './process-add.page.html',
  styleUrls: ['./process-add.page.scss', './process-add.page.mobile.scss'],
})
/**
 * 工程名追加ダイアログ
 */
export class ProcessAddPage {
  isPc: boolean;
  /**
   * 画面DTO
   */
  public gamenDto: ProcessDto = {};

  /**
   * 工程件数
   */
  public processCount: number;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    addProcessTitle: S0801Constants.ADDPROCESSTITLE,
    displayOrder: S0801Constants.DISPLAYORDER,
    addName: S0801Constants.ADDNAME,
    message: S0801Constants.MESSAGE,
    register: S0801Constants.REGISTER,
    cancel: S0801Constants.CANCEL,
    ok: S0801Constants.CHANGE,
    processMsg: S0801Constants.PROCESSMSG
  };
  /**
   * コンストラクタ
   * @param alertController AlertController
   * @param navParams 工程DTO
   * @param api0803Service 工程名追加API(API-08-05)
   */
  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    public api0803Service: API0803Service,
    private commonStoreService: CommonStoreService,
    private appStateService: AppStateStoreService
  ) {
    const processDto: ProcessDto = navParams.data.value;
    this.gamenDto.displayOrder = processDto.displayOrder;
    this.gamenDto.bigProcessId = processDto.bigProcessId;
    this.gamenDto.processName = null;
    this.processCount = processDto.displayOrder;
    this.isPc = this.appStateService.appState.isPC;
    this.platformtype = 'processAdd-' + this.appStateService.getPlatform();
  }
  /**
   * クローズボタンをクリックすること。
   */
  onNoClick(): void {
    this.navParams.data.modal.dismiss(null);
  }

  /**
   * 表示順入力欄内、工程名入力欄内のどちらにも値が入力された場合、活性化。
   */
  isUpdated(): boolean {
    return this.gamenDto.displayOrder === null ||
      this.gamenDto.processName === null ||
      this.gamenDto.processName.trim() === '';
  }
  /**
   * 追加ボタンをクリックすること。
   */
  async onAddClick() {
    const gamenDto = this.gamenDto;
    const reg = /^[0-9]{1,2}$/;


    if (!TextCheck.textCheck(gamenDto.processName)) {
      const alertWindow = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alertWindow.present();
      return;
    }

    if (gamenDto.displayOrder !== null && gamenDto.displayOrder > 0 && gamenDto.displayOrder <= 99
      && reg.test(gamenDto.displayOrder.toString())) {
      this.updateData();
      return;
    }
    const alert = await this.alertController.create({
      header: AlertWindowConstants.ERRORTITLE,
      message: MessageConstants.E00009.replace('{{value}}', this.gamenLable.displayOrder),
      buttons: [AlertWindowConstants.OKENGLISHBUTTON],
      backdropDismiss: false
    });
    alert.addEventListener('ionAlertDidPresent', () => {
      const alertButton = alert.getElementsByClassName('alert-button');
      if (alertButton && alertButton.length > 0) {
        (alertButton[0] as HTMLElement).focus();
      }
    });
    await alert.present();
  }
  /**
   * データを追加すること。
   */
  async updateData() {
    this.api0803Service.processAdd(this.getAddProcessInDto(), (outDto: API0803OutDto) => {
      this.addSuccessed(outDto);
    },
      // fault
      () => {
        this.navParams.data.modal.dismiss(null);
      });
  }
  /**
   * 追加成功、API0803返信。
   * @param outDto API0803OutDto
   */
  async addSuccessed(outDto: API0803OutDto) {
    if (outDto.resultCode === ResultType.BUSINESSERROR) {
      // エラーメッセージ設定
      let msg = '';
      if (outDto.messageCode === 'E00129') {
        msg = MessageConstants[outDto.messageCode].replace('{0}', this.gamenDto.processName.trim());
      } else if (outDto.messageCode === 'E00045') {
        msg = MessageConstants[outDto.messageCode].replace('{0}', this.gamenLable.processMsg.trim());
      }
      // エラーメッセージポップアップ
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: this.isPc ? MessageConstants.I00002 : MessageConstants.I00022,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.navParams.data.modal.dismiss(this.gamenDto);
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    }
  }
  /**
   * API0803送信。
   */
  getAddProcessInDto(): API0803InDto {
    const api0803InDto: API0803InDto = {};
    api0803InDto.processName = this.gamenDto.processName.trim();
    api0803InDto.displayOrder = this.gamenDto.displayOrder;
    api0803InDto.projectId = this.commonStoreService.commonStore.projectId;
    api0803InDto.bigProcessId = this.gamenDto.bigProcessId;
    return api0803InDto;
  }

  focusInSort() {
    document.getElementById('sort').style.color = '#0097E0';
  }
  focusOutSort() {
    document.getElementById('sort').style.color = '#333333';
  }

  focusInProcessName() {
    document.getElementById('processName').style.color = '#0097E0';
  }
  focusOutProcessName() {
    document.getElementById('processName').style.color = '#333333';
  }
}
