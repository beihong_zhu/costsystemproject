import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { PhotoFilterSetComponent } from './photo-filter-set.component';


@NgModule({
  // declarations: [PhotoFilterSetComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoFilterSetComponent
      }
    ])
  ]
})
export class PhotoFilterSetModule { }
