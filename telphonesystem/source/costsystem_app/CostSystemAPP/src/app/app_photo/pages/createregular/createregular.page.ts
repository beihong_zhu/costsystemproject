import { Component, OnInit } from '@angular/core';
import { S1701Constants } from '@constant/constant.photo';

@Component({
  selector: 'app-createregular',
  templateUrl: './createregular.page.html',
  styleUrls: ['./createregular.page.scss'],
})
export class CreateregularPage implements OnInit {

  public channelID: string;

    // id
    public channelIDLabel: string = S1701Constants.channelID; 

  constructor() { }

  ngOnInit() {
  }

}
