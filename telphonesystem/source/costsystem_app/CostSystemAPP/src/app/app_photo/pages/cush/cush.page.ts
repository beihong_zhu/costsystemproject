import { Component, OnInit } from '@angular/core';
import { API3003InDto, API3003OutDto } from '@service/dto';
import { API3003Service } from '@service/api/api-30-03.service';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysAddfundDto } from './dto/addfund.dto';
import { SysUserDto } from '@pages/regularinfo/dto';



@Component({
  selector: 'app-cush',
  templateUrl: './cush.page.html',
  styleUrls: ['./cush.page.scss'],
})
export class CushPage implements OnInit {


   public ocrManufacturingDate: string ;
   public agencyacount : string;
   //代理简称
   public agencyname : string ;
   public applymode : string;
   public bussinessMode : string;
   public status : string;
   public bussinessName : string;
   public sysAddfundDtoList: Array<SysAddfundDto> = new Array<SysAddfundDto>();

  public  cushInfo:any = {
    cushInfoList:['账户提现' ,'平台加款'],
    cush:'账户提现',
    statusList:['成功' ,'拒绝','撤销','待处理'],
    status : '成功'
   }

  constructor(private api3003Service: API3003Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api3003ServicePostExec();
  }

  private get3003InDto() {
    let api3003InDto: API3003InDto = {};
    //api3003InDto.agencyacount= this.agencyacount;
    api3003InDto.agencyname = this.agencyname;
  
    //api3003InDto.ocrManufacturingDate = this.ocrManufacturingDate;

    return api3003InDto;
  }

  private api3003ServicePostExec() {
   
    // 写真帳出力情報取得
    this.api3003Service.postExecNew(this.get3003InDto()).then((res) => {
     
      //const response: any = res.json();
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3003successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api3003successed(outDto: API3003OutDto) {
    this.sysAddfundDtoList = new Array<SysAddfundDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.capitalReducePlusModelResList));

    for (const sysUserInfoModel of loadSysusers) {
      let sysAddfundDto: SysAddfundDto = new SysAddfundDto();
      sysAddfundDto.AgentReduceName = sysUserInfoModel.AgentReduceName;
      sysAddfundDto.plusCapital = sysUserInfoModel.plusCapital;
      sysAddfundDto.createdDatetime = sysUserInfoModel.createdDatetime;
      sysAddfundDto.capital = sysUserInfoModel.capital;
      this.sysAddfundDtoList.push(sysAddfundDto);
    }
  }

}
