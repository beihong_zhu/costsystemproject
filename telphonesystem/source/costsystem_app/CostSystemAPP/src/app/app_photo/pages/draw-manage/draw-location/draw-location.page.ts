import { Component, OnInit, ViewChildren, QueryList, OnDestroy, HostListener } from '@angular/core';
import { PdfViewerComponent } from '../../../components/ng2-pdf-viewer/pdf-viewer.component';
import { PopoverController, AlertController } from '@ionic/angular';
import { PinContextMenuComponent } from './pin-context-menu/pin-context-menu.component';
import { DrawLocationGamenDto, PinDto, RoomDto, PlaceDto } from './dto';
import { DrawDisplayStoreService, CommonStoreService } from '@store';
import { AlertWindowConstants, S0602Constants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0602Service, API0603Service, API0604Service } from '@service/api';
import { API0602OutDto, API0602InDto, API0603OutDto, API0603InDto, API0604OutDto, API0604InDto } from '@service/dto';
import { MatDrawerContainer } from '@angular/material';
import { ResultType } from '../../../../app_common/constant/common';
import { CD0019Constants } from '@constant/code';
import * as lodash from 'lodash';

@Component({
  selector: 'app-draw-location',
  templateUrl: './draw-location.page.html',
  styleUrls: ['./draw-location.page.scss'],
})
export class DrawLocationPage implements OnDestroy {

  @ViewChildren(PdfViewerComponent) pdfview: QueryList<PdfViewerComponent>;
  @ViewChildren(MatDrawerContainer) matDrawerContainer: QueryList<MatDrawerContainer>;

  /**
   * 画面DTO
   */
  public gamenDto: DrawLocationGamenDto;
  /**
   * 図面内をクリックしたままの状態でスクロールが可能
   */
  public screenX: number = null;
  public screenY: number = null;
  public matDrawerScroll: HTMLElement = null;
  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    title: S0602Constants.TITLE,
    drawingList: S0602Constants.DRAWINGLIST,
    photographPlacelist: S0602Constants.PHOTOGRAPHPLACELIST,
    floorSelection: S0602Constants.FLOORSELECTION,
    roomSelection: S0602Constants.ROOMSELECTION,
    placeSelection: S0602Constants.PLACESELECTION,
    pacement1: S0602Constants.PACEMENT1,
    pacement0: S0602Constants.PACEMENT0,
    pinDeleteline: S0602Constants.PINDELETELINE,
    modelType: S0602Constants.MODELTYPE,
    setFlag: CD0019Constants.SET_FLAG
  };
  /**
   * 図面一覧にて選択している図面のピン
   */
  public selectedDrawingPinInfo?: Array<PinDto>;
  /**
   * 図面一覧にて選択している図面の場所情報
   */
  public selectedPlaceId?: string = '';
  /**
   * 撮影場所一覧にて選択しているの部屋情報の場所一覧
   */
  public optionRoomList?: RoomDto[] = [];
  /**
   * 撮影場所一覧にて選択しているの部屋情報
   */
  public selectedRoom?: RoomDto = {};
  public selectedRoomId?: string = '';
  /**
   * PDFの高さ、幅（百分率）
   */
  public pdfviewHeight: number;
  public pdfviewWidth: number;
  /**
   * PDFの高さ、幅（百分率、%含まる）
   */
  public pdfviewHeightPx: string;
  public pdfviewWidthPx: string;
  /**
   * PDFで移動されているピン
   */
  public movePin: PinDto;
  /**
   * ピン一覧からPDFに移動されているピン
   */
  public newPin: PinDto;

  /**
   * コンストラクタ
   * @param api0602Service ピン情報登録
   * @param api0603Service ピン情報座標更新
   * @param api0604Service ピン情報配置フラグ更新
   * @param popoverController ダイアログコントローラ
   * @param alertController ダイアログコントローラ
   */
  constructor(
    private api0602Service: API0602Service,
    private api0603Service: API0603Service,
    private api0604Service: API0604Service,
    private popoverController: PopoverController,
    private drawDisplayStoreService: DrawDisplayStoreService,
    public alertController: AlertController,
    public commonStoreService: CommonStoreService) {
    (window as any).pdfWorkerSrc = './assets/js/pdf.worker.js';
    this.gamenDto = { drawingInfoDto: { drawings: [] }, pdfPinInfo: [], pinInfo: [], selectedDrawing: {} };
  }

  ionViewWillEnter() {
    const gamenDto = this.gamenDto;
    gamenDto.drawingInfoDto = this.drawDisplayStoreService.drawDisplayStore.drawingInfoDto;
    gamenDto.pinInfo = this.drawDisplayStoreService.drawDisplayStore.pinInfo;
    gamenDto.selectedDrawingId = this.drawDisplayStoreService.drawDisplayStore.selectedDrawingId;
    this.selectedDrawingChange();
    // 初期表示時には、画面に対して図面を100%で表示する。
    gamenDto.zoomSize = 1;


  }

  /**
   * PDFで移動する方法
   */
  movePinDrag(movePin: PinDto, $event) {
    this.screenX = null;
    this.screenY = null;
    this.matDrawerScroll = null;
    this.movePin = movePin;
    this.newPin = null;
  }

  /**
   * PDFでのピンのDropされるのを取消
   */
  stopDrop($event) {
    $event.stopPropagation();
  }

  /**
   * ピン一覧からPDFに移動する方法
   */
  newPinDrag(newPin: PinDto, $event) {
    this.screenX = null;
    this.screenY = null;
    this.matDrawerScroll = null;
    this.movePin = null;
    this.newPin = newPin;
  }

  /**
   * PDFにDrop事件
   */
  async pinDrop($event) {
    const positionY = Number(($event.layerY / this.pdfviewHeight * 100.0).toFixed(2));
    const positionX = Number(($event.layerX / this.pdfviewWidth * 100.0).toFixed(2));
    // PDFで移動する場合
    if (this.movePin != null) {
      // 元の設置場所
      const bkPositionX = this.movePin.positionX;
      const bkPositionY = this.movePin.positionY;
      // 最新な設置
      this.movePin.positionY = positionY;
      this.movePin.positionX = positionX;
      this.movePin.positionYpercent = this.movePin.positionY + '%';
      this.movePin.positionXpercent = this.movePin.positionX + '%';
      this.movePin.isMoving = false;
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00009,
        buttons: [
          {
            text: AlertWindowConstants.OKBUTTON,
            handler: () => {
              // OKボタンを押下、APIをを行う。
              this.api0603Service.postExec(this.get0603Indto(this.movePin), (outDto: API0603OutDto) => { },
                // fault
                () => { });
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
              // ドラッグで移動していたピンを、元の設置場所へ戻す。
              this.movePin.positionX = bkPositionX;
              this.movePin.positionY = bkPositionY;
              this.movePin.positionYpercent = this.movePin.positionY + '%';
              this.movePin.positionXpercent = this.movePin.positionX + '%';
            }
          }
        ]
      });
      await alert.present();
    } else if (this.newPin != null) {
      // ピン一覧からPDFで移動する場合
      this.newPin.positionY = positionY;
      this.newPin.positionX = positionX;
      this.newPin.positionYpercent = this.newPin.positionY + '%';
      this.newPin.positionXpercent = this.newPin.positionX + '%';
      this.newPin.isMoving = false;
      this.gamenDto.pdfPinInfo.push(this.newPin);
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00008,
        buttons: [{
          text: AlertWindowConstants.OKBUTTON,
          handler: () => {
            // OKボタンを押下、APIをを行う。
            if (this.newPin.pinId === null) {
              this.api0602Service.postExec(this.get0602Indto(this.newPin), (outDto: API0602OutDto) => {
                this.api0602Success(this.newPin, outDto);
              },
                // fault
                () => {
                });

            } else {
              this.api0603Service.postExec(this.get0603IndtoInstalled(this.newPin), (outDto: API0603OutDto) => {
                this.newPin.isInstalled = CD0019Constants.SET_FLAG;
              },
                // fault
                () => { });

            }

          }
        },
        {
          text: AlertWindowConstants.CANCELBUTTON,
          role: 'cancel',
          cssClass: 'primary',
          handler: (blah) => {
            // ドラッグしたピンを図面上から消去する。
            const index = this.gamenDto.pdfPinInfo.indexOf(this.newPin);
            this.gamenDto.pdfPinInfo.splice(index, 1);
            this.newPin.isInstalled = null;
            this.newPin.positionX = null;
            this.newPin.positionY = null;
          }
        }
        ]
      });
      await alert.present();
    }
  }

  /**
   * API0605のIndtoを取得すること
   */
  get0602Indto(pin: PinDto): API0602InDto {
    const inDto: API0602InDto = {};
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.drawingId = pin.drawingId;
    inDto.unitId = pin.machine.machineId;
    inDto.positionX = pin.positionX;
    inDto.positionY = pin.positionY;
    inDto.isInstalled = CD0019Constants.SET_FLAG;
    // inDto.登録ユーザ
    return inDto;
  }

  /**
   * API0606のIndtoを取得すること
   */
  get0603Indto(pin: PinDto): API0603InDto {
    const inDto: API0603InDto = {};
    inDto.pinId = pin.pinId;
    inDto.positionX = pin.positionX;
    inDto.positionY = pin.positionY;
    // inDto.更新ユーザ
    return inDto;
  }

  /**
   * API0606のIndtoを取得すること
   */
  get0603IndtoInstalled(pin: PinDto): API0603InDto {
    const inDto: API0603InDto = {};
    inDto.pinId = pin.pinId;
    inDto.positionX = pin.positionX;
    inDto.positionY = pin.positionY;
    inDto.isInstalled = CD0019Constants.SET_FLAG;
    // inDto.更新ユーザ
    return inDto;
  }

  /**
   * API0607のIndtoを取得すること
   */
  get0604Indto(pin: PinDto): API0604InDto {
    const inDto: API0604InDto = {};
    inDto.pinId = pin.pinId;
    inDto.isInstalled = CD0019Constants.UNSET_FLAG;
    // inDto.更新ユーザ
    return inDto;
  }

  async api0602Success(pin: PinDto, outDto: API0602OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      pin.isInstalled = CD0019Constants.SET_FLAG;
      pin.pinId = outDto.pinId;
    } else if (outDto.resultCode === ResultType.INFO) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE, // I00020
        message: MessageConstants[outDto.messageCode] || outDto.message,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              if (CD0019Constants.UNSET_FLAG === outDto.isInstalled) {
                const index = this.gamenDto.pdfPinInfo.indexOf(pin);
                this.gamenDto.pdfPinInfo.splice(index, 1);
              }
              pin.isInstalled = outDto.isInstalled;
              pin.pinId = outDto.pinId;
              pin.positionY = outDto.positionY;
              pin.positionX = outDto.positionX;
              pin.positionYpercent = outDto.positionY + '%';
              pin.positionXpercent = outDto.positionX + '%';
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  /**
   * PDFでDropされる
   */
  pinAllowDrop($event) {
    $event.preventDefault();
  }

  /**
   * 拡大ボタンについて
   */
  addZoom() {
    if (this.gamenDto.zoomSize < 10) {
      this.gamenDto.zoomSize = (this.gamenDto.zoomSize * 10 + 1) / 10;
    }
  }

  /**
   * 縮小ボタンについて
   */
  removeZoom() {
    if (this.gamenDto.zoomSize > 0.3) {
      this.gamenDto.zoomSize = (this.gamenDto.zoomSize * 10 - 1) / 10;
    }
  }

  /**
   * PDFの高さ、幅を取得すること
   */
  pdfviewSizeChange() {
    const pdfMultiPageViewer = 'pdfMultiPageViewer';
    if (this.pdfview && this.pdfview.first) {
      const pdfview: PdfViewerComponent = this.pdfview.first;
      this.pdfviewWidth = pdfview[pdfMultiPageViewer]._pages[0].viewport.width;
      this.pdfviewHeight = 0;

      const pageSize: number = pdfview[pdfMultiPageViewer]._pages.length;
      for (let i = 0; i < pageSize; i++) {
        this.pdfviewHeight += pdfview[pdfMultiPageViewer]._pages[i].viewport.height;
      }
      this.pdfviewWidthPx = this.pdfviewWidth + 'px';
      this.pdfviewHeightPx = this.pdfviewHeight + 'px';
    }
  }

  /**
   * PDF読み込み完了
   */
  pageRendered() {
    this.pdfviewSizeChange();
  }

  mouseOverPin(pin: PinDto, event) {

    const pinpop = document.getElementById(pin.pinId.toString());
    const x = pinpop.offsetLeft + (pinpop.offsetParent as HTMLElement).offsetLeft + pinpop.offsetWidth;
    if (x > this.pdfviewWidth) {
      pinpop.style.right = '100%';
    }
    const y = pinpop.offsetTop + (pinpop.offsetParent as HTMLElement).offsetTop + pinpop.offsetHeight;
    if (y > this.pdfviewHeight) {
      pinpop.style.bottom = 'calc(100% + 40px)';
    }
    pinpop.style.visibility = 'visible';
  }
  mouseOutPin(pin: PinDto, event) {

    const pinpop = document.getElementById(pin.pinId.toString());
    pinpop.style.removeProperty('right');
    pinpop.style.removeProperty('bottom');
    pinpop.style.visibility = 'hidden';
  }



  /**
   * ピン編集メニュー　※図面上のピンをクリックする事で表示
   */
  async openMenu(pin: PinDto, event) {
    event.preventDefault();
    const menu = await this.popoverController.create({
      component: PinContextMenuComponent,
      event,
      translucent: true,
      showBackdrop: true
    });
    await menu.present();
    const { data } = await menu.onDidDismiss();
    // 変更ボタンを押下するか判断
    if (data === 'move') {
      pin.isMoving = true;
    } else if (data === 'delete') {
      // ピンの削除
      const alert = await this.alertController.create({
        header: AlertWindowConstants.CONFIRMTITLE,
        message: MessageConstants.C00010,
        buttons: [{
          text: AlertWindowConstants.OKBUTTON,
          handler: () => {
            // apiで削除
            this.api0604Service.postExec(this.get0604Indto(pin), (outDto: API0604OutDto) => {
              // 画面のPDFで削除
              const index = this.gamenDto.pdfPinInfo.indexOf(pin);
              this.gamenDto.pdfPinInfo.splice(index, 1);
              pin.isInstalled = null;
              pin.positionX = null;
              pin.positionY = null;
            },
              // fault
              () => {
              });
          }
        },
        {
          text: AlertWindowConstants.CANCELBUTTON,
          role: 'cancel',
          cssClass: 'primary',
          handler: (blah) => {
          }
        }
        ]
      });
      await alert.present();
    }
  }

  /**
   * 図面を一覧に表示(サムネイル)し選択する
   */
  selectedDrawingChange() {
    this.gamenDto.pdfPinInfo = [];
    this.selectedDrawingPinInfo = [];
    for (const drawingDto of this.gamenDto.drawingInfoDto.drawings) {
      if (drawingDto.drawingId === this.gamenDto.selectedDrawingId) {
        // 選択されたPDFを表示する
        this.gamenDto.selectedDrawing = drawingDto;
      }
    }

    this.optionRoomList = [];
    this.selectedRoom = {};
    this.selectedRoomId = '';
    this.selectedPlaceId = '';
    for (const pinDto of this.gamenDto.pinInfo) {
      if (pinDto.drawingId === this.gamenDto.selectedDrawingId) {
        if (pinDto.isInstalled === CD0019Constants.SET_FLAG) {
          pinDto.positionXpercent = pinDto.positionX + '%';
          pinDto.positionYpercent = pinDto.positionY + '%';
          // PDFにピンを追加
          this.gamenDto.pdfPinInfo.push(pinDto);
        }
        // ピン一覧にを追加
        this.selectedDrawingPinInfo.push(pinDto);
      }
    }

    const sortFloorRoomPlace = lodash.orderBy(this.gamenDto.pinInfo, [
      (pin) => pin.drawingId || 0,
      (pin) => pin.machine.roomId || 0,
      (pin) => pin.machine.placeId || 0,
    ], ['asc', 'asc', 'asc']);

    for (const pinDto of sortFloorRoomPlace) {
      if (pinDto.drawingId === this.gamenDto.selectedDrawingId) {
        // 撮影場所一覧に設置する
        const room: RoomDto = { roomId: pinDto.machine.roomId, roomName: pinDto.machine.roomName };
        const place: PlaceDto = { placeId: pinDto.machine.placeId, placeName: pinDto.machine.placeName };
        this.pushRoomTree(room, place);
      }
    }
  }

  /**
   * 部屋選択、場所選択の初期化
   */
  pushRoomTree(room: RoomDto, place: PlaceDto) {
    if (room.roomId === null) {
      return;
    }
    const optionRoomListLength = this.optionRoomList.length;
    for (let i = 0; i < optionRoomListLength; i++) {
      if (this.optionRoomList[i].roomId === room.roomId) {
        if (this.optionRoomList[i].places) {
          this.pushPlaceTree(this.optionRoomList[i].places, place);
        } else if (place.placeId !== null) {
          this.optionRoomList[i].places = [place];
        }
        return;
      }
    }
    this.optionRoomList.push(room);
    if (place.placeId !== null) {
      room.places = [place];
    }
  }

  pushPlaceTree(places: PlaceDto[], place: PlaceDto) {
    if (place.placeId === null) {
      return;
    }
    const placesLength = places.length;
    for (let j = 0; j < placesLength; j++) {
      if (places[j].placeId === place.placeId) {
        return;
      }
    }
    places.push(place);
  }


  /**
   * 部屋選択変更
   */
  roomChanged() {
    this.selectedRoom = {};
    const optionRoomListLength = this.optionRoomList.length;
    for (let i = 0; i < optionRoomListLength; i++) {
      if (this.optionRoomList[i].roomId + '' === this.selectedRoomId) {
        this.selectedRoom = this.optionRoomList[i];
        break;
      }
    }
    this.selectedPlaceId = '';
  }

  drawerMousedown(event) {
    this.screenX = event.screenX;
    this.screenY = event.screenY;
    if (this.matDrawerContainer && this.matDrawerContainer.first) {
      const matDrawerContainer: MatDrawerContainer = this.matDrawerContainer.first;
      const elementRef = 'elementRef';
      this.matDrawerScroll = matDrawerContainer._content[elementRef].nativeElement;
    }
  }

  @HostListener('window:mouseup', ['$event'])
  onMouseup() {
    this.screenX = null;
    this.screenY = null;
    this.matDrawerScroll = null;
  }


  @HostListener('window:mousemove', ['$event'])
  onMouseMove(event) {
    if (this.screenX != null && this.screenY != null && this.matDrawerScroll) {
      this.matDrawerScroll.scrollLeft += this.screenX - event.screenX;
      this.matDrawerScroll.scrollTop += this.screenY - event.screenY;
      this.screenX = event.screenX;
      this.screenY = event.screenY;
    }
  }

  ngOnDestroy(): void {
    this.drawDisplayStoreService.updateSelectedDrawing(this.gamenDto.selectedDrawingId);
    this.gamenDto = null;
  }

  spaceString(title: string) {
    if (title) {
      return decodeURI(title.replace(/ /g, '%C2%A0'));
    } else {
      return '';
    }
  }
}
