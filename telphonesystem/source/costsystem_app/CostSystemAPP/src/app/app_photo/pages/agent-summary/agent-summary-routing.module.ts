import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgentSummaryPage } from './agent-summary.page';

const routes: Routes = [
  {
    path: '',
    component: AgentSummaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgentSummaryPageRoutingModule {}
