import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { PhotoViewComponent } from './photo-view.component';


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoViewComponent
      }
    ])
  ]
})
export class PhotoViewModule { }
