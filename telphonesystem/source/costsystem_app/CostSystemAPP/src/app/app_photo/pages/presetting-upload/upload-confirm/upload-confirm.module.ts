import { NgModule } from '@angular/core';
import { UploadConfirmPage } from './upload-confirm.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { DownloadConfirmPage } from './download-confirm/download-confirm.page';

@NgModule({
  providers: [],
  declarations: [UploadConfirmPage, DownloadConfirmPage],
  entryComponents: [
    DownloadConfirmPage
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: UploadConfirmPage
      }
    ])
  ]
})
export class UploadConfirmModule { }
