import { Component } from '@angular/core';
import { S0902Constants, S0903Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0901Service, StartExecutionService, DescribeExecutionService, IF0106Service, IF0109Service } from '@service/api';
import {
  API0901InDto, API0901OutDto, API0902InDto, API0902OutDto,
  IF0106InDto, IF0109OutDto, StartExecutionOutDto, DescribeExecutionInDto, DescribeExecutionOutDto
} from '@service/dto';
import {
  PhotobookInfoSetStoreService, PhotobookInfoStore, PhotobookViewStore, AppLoadingStoreService,
  CommonStoreService, AppStateStoreService, PhotobookViewSetStoreService
} from '@store';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { RoutingService } from '@service/app-route.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { ResultType } from '../../../../app_common/constant/common';
import * as moment from 'moment';
import { SORTBY } from '@constant/code';
import { File } from '@ionic-native/file/ngx';
import { ProcessIdDto } from '@service/dto/common/dto/processid-dto';

@Component({
  selector: 'app-photobook-preview',
  templateUrl: './photobook-preview.page.html',
  styleUrls: ['./photobook-preview.page.scss', './photobook-preview.page.winapp.scss', './photobook-preview.page.mobile.scss'],
})

export class PhotobookPreviewPage {
  platformtype: string;
  isPc: boolean;

  title: string = S0903Constants.TITLE;
  photobookViewStore: PhotobookViewStore;
  photobookInfoStore: PhotobookInfoStore;
  buildingName: string;
  projectName: string;
  photobooks: PhotoBook[];
  prevBooks: PrevBook[];
  // 配置内容
  itemValue1: string;
  itemValue2: string;
  itemValue3: string;
  itemValue4: string;
  itemValue5: string;
  itemValue6: string;
  itemValue7: string;
  itemValue8: string;
  // セレクトボックス内容
  selectBuilding: number;
  selectFloor: number;
  selectRoom: number;
  selectPlace: number;
  selectLargeProcess: number;
  selectProcess: number;
  selectWork: number;
  selectSituation: number;
  selectDate: number;
  selectTime: number;
  selectUnitMarkModelType: number;
  selectMachineNumber: number;
  selectWitness: number;
  selectConstructionCompany: number;
  selectFreeColumn: number;
  pageCnt: number;
  isDownloaded: boolean;
  downloadIsDisabled: boolean = true;

  /**
   * 画面項目名称取得
   */
  gamenLable = {
    downloadGuide: S0903Constants.DOWNLOADGUIDE,
    download: S0903Constants.DOWNLOAD,
    return: S0903Constants.RETURN,
    returnArrow: S0903Constants.RETURNARROW,
  };

  constructor(
    private platform: Platform,
    private routerService: RoutingService,
    public alertController: AlertController,
    public modalController: ModalController,
    public appStateService: AppStateStoreService,
    public commonStoreService: CommonStoreService,
    private photobookViewSetStoreService: PhotobookViewSetStoreService,
    private photobookInfoSetStoreService: PhotobookInfoSetStoreService,
    public api0901Service: API0901Service,
    public startExecutionService: StartExecutionService<API0902InDto>,
    public describeExecutionService: DescribeExecutionService,
    public if0106Service: IF0106Service,
    public if0109Service: IF0109Service,
    protected httpClient: HttpClient,
    private appLoadingStoreService: AppLoadingStoreService,
    private file: File,
  ) {

    // APPストアに保存した写真帳出力情報を取得
    this.photobookInfoStore = this.photobookInfoSetStoreService.photobookInfoStore;

    // APPストアに保存した写真帳レイアウトを取得
    this.photobookViewStore = this.photobookViewSetStoreService.photobookViewStore;
    this.platformtype = 'photobook-preview-' + this.appStateService.getPlatform();

  }

  /**
   * 初期化する。
   */
  ionViewWillEnter() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.pageCnt = Number(this.photobookInfoStore.pageCnt);
    this.isDownloaded = false;
    this.downloadIsDisabled = true;
    this.initData();
  }

  ionViewWillLeave() {
  }

  /**
   * 画面データを取得すること。
   */
  async initData() {

    // 物件案件名
    this.buildingName = this.photobookInfoStore.chooseBuildingName;
    this.projectName = this.photobookInfoStore.chooseConstructionName;

    // 配置情報初期表示
    this.itemValue1 = this.photobookViewStore.item1;
    this.itemValue2 = this.photobookViewStore.item2;
    this.itemValue3 = this.photobookViewStore.item3;
    this.itemValue4 = this.photobookViewStore.item4;
    this.itemValue5 = this.photobookViewStore.item5;
    this.itemValue6 = this.photobookViewStore.item6;
    this.itemValue7 = this.photobookViewStore.item7;
    this.itemValue8 = this.photobookViewStore.item8;
    // セレクトボックス情報初期表示
    this.selectBuilding = this.photobookViewStore.building;
    this.selectFloor = this.photobookViewStore.floor;
    this.selectRoom = this.photobookViewStore.room;
    this.selectPlace = this.photobookViewStore.place;
    this.selectLargeProcess = this.photobookViewStore.largeProcess;
    this.selectProcess = this.photobookViewStore.process;
    this.selectWork = this.photobookViewStore.work;
    this.selectSituation = this.photobookViewStore.situation;
    this.selectDate = this.photobookViewStore.date;
    this.selectTime = this.photobookViewStore.time;
    this.selectUnitMarkModelType = this.photobookViewStore.unitMarkModelType;
    this.selectMachineNumber = this.photobookViewStore.machineNumber;
    this.selectWitness = this.photobookViewStore.witness;
    this.selectConstructionCompany = this.photobookViewStore.constructionCompany;
    this.selectFreeColumn = this.photobookViewStore.freeColumn;

    // 写真帳出力情報取得
    this.api0901Service.postExec(this.get0901InDto(), (outDto: API0901OutDto) => {
      this.api0901successed(outDto);
      if (outDto.resultCnt > 0) {
        this.downloadIsDisabled = false;
        // プレビュー表示用データを作成
        if (this.photobooks && this.photobooks.length > 0) {
          this.prevBooksCreate();
        }
      }
    },
      // fault
      () => {
      });
  }

  async api0901successed(outDto: API0901OutDto) {
    // 工程情報取得。
    this.photobooks = new Array();

    if (outDto.photoInfomationList !== null && outDto.photoInfomationList.length > 0) {
      Array.from({ length: outDto.photoInfomationList.length }).map((_, i) => {

        const photoBook: PhotoBook = {};
        this.photobooks.push(photoBook);

        photoBook.photoPath = outDto.photoInfomationList[i].photoPath;
        photoBook.photoRelativePath = outDto.photoInfomationList[i].photoRelativePath;
        photoBook.buildingId = this.commonStoreService.commonStore.buildingId;
        photoBook.buildingName = this.photobookInfoStore.chooseBuildingName;
        photoBook.projectId = this.commonStoreService.commonStore.projectId;
        photoBook.projectName = this.photobookInfoStore.chooseConstructionName;
        photoBook.largeProcessId = this.photobookInfoStore.chooseLargeProcessId;
        photoBook.largeProcessName = this.photobookInfoStore.chooseLargeProcessName;
        photoBook.processId = outDto.photoInfomationList[i].processId;
        photoBook.processName = outDto.photoInfomationList[i].processName;
        photoBook.processOrder = outDto.photoInfomationList[i].processDisplayOrder;
        photoBook.floorId = outDto.photoInfomationList[i].floorId;
        photoBook.floorName = outDto.photoInfomationList[i].floorName;
        photoBook.floorOrder = outDto.photoInfomationList[i].floorDisplayOrder;
        photoBook.roomId = outDto.photoInfomationList[i].roomId;
        photoBook.roomName = outDto.photoInfomationList[i].roomName;
        photoBook.roomOrder = outDto.photoInfomationList[i].roomDisplayOrder;
        photoBook.placeId = outDto.photoInfomationList[i].placeId;
        photoBook.placeName = outDto.photoInfomationList[i].placeName;
        photoBook.placeOrder = outDto.photoInfomationList[i].placeDisplayOrder;
        photoBook.machineId = outDto.photoInfomationList[i].machineId;
        photoBook.machineNumber = outDto.photoInfomationList[i].machineNumber;
        photoBook.unitMarkModelType = outDto.photoInfomationList[i].unitMarkModelType;
        photoBook.workId = outDto.photoInfomationList[i].workId;
        photoBook.workName = outDto.photoInfomationList[i].workName;
        photoBook.workOrder = outDto.photoInfomationList[i].workDisplayOrder;
        photoBook.timingId = outDto.photoInfomationList[i].timingId;
        photoBook.timingName = outDto.photoInfomationList[i].timingName;
        photoBook.timingOrder = outDto.photoInfomationList[i].timingDisplayOrder;
        photoBook.photoDate = outDto.photoInfomationList[i].photoDate;
        if (outDto.photoInfomationList[i].photoDate) {
          photoBook.date = moment(outDto.photoInfomationList[i].photoDate, 'YYYYMMDDHHmmss').format('YYYY/MM/DD');
          photoBook.time = moment(outDto.photoInfomationList[i].photoDate, 'YYYYMMDDHHmmss').format('HH:mm');
        } else {
          photoBook.date = '';
          photoBook.time = '';
        }
        photoBook.witness = outDto.photoInfomationList[i].witness;
        photoBook.constructionCompany = outDto.photoInfomationList[i].constructionCompany;
        photoBook.freeColumn = outDto.photoInfomationList[i].free;
        photoBook.outFlg = outDto.photoInfomationList[i].isOutputPhotoBook;
      });
    } else {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00074,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
            }
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  // InDto内容を作成
  get0901InDto(): API0901InDto {
    const processIdList: Array<ProcessIdDto> = new Array();
    Array.from({ length: this.photobookInfoStore.photobookInfos.length }).map((_, i) => {
      const photobookInfo = this.photobookInfoStore.photobookInfos[i];
      if (photobookInfo.ischeck) {
        const processId = new ProcessIdDto();
        processId.processId = photobookInfo.processId;
        processId.unknownWorkOutput = photobookInfo.unknownWorkOutput ? 1 : 0;
        processId.workIdList = [];
        if (photobookInfo.workList) {
          for (const work of photobookInfo.workList) {
            if (work.ischeck) {
              processId.workIdList.push({ workId: work.workId });
            }
          }
        }
        processIdList.push(processId);
      }
    });

    const inDto: API0901InDto = {};
    inDto.outputOrderPattern = this.photobookInfoStore.outputOrder;
    inDto.buildingId = this.commonStoreService.commonStore.buildingId;
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.bigProcessId = this.photobookInfoStore.chooseLargeProcessId;
    inDto.processIdList = processIdList;
    inDto.unknownProcessOutput = this.photobookInfoStore.unknownProcessOutput ? 1 : 0;

    return inDto;
  }

  // InDto内容を作成
  get0106InDto(): IF0106InDto {

    const inDto: IF0106InDto = {};

    inDto.buildingId = this.commonStoreService.commonStore.buildingId;
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.sorts = [{ sortItem: 'unit_id', order: SORTBY.ASC }];

    return inDto;
  }

  // 画面情報を作成
  prevBooksCreate(): void {
    this.prevBooks = new Array<PrevBook>();

    Array.from({ length: this.photobooks.length }).map((_, i) => {
      this.prevBooks[i] = {};
      this.prevBooks[i].item1 = new Array();
      this.prevBooks[i].item2 = new Array();
      this.prevBooks[i].item3 = new Array();
      this.prevBooks[i].item4 = new Array();
      this.prevBooks[i].item5 = new Array();
      this.prevBooks[i].item6 = new Array();
      this.prevBooks[i].item7 = new Array();
      this.prevBooks[i].item8 = new Array();
      this.prevBooks[i].value1 = new Array();
      this.prevBooks[i].value2 = new Array();
      this.prevBooks[i].value3 = new Array();
      this.prevBooks[i].value4 = new Array();
      this.prevBooks[i].value5 = new Array();
      this.prevBooks[i].value6 = new Array();
      this.prevBooks[i].value7 = new Array();
      this.prevBooks[i].value8 = new Array();
      this.prevBooks[i].photoPath = this.photobooks[i].photoPath;
      this.prevValueCreate(this.selectBuilding, this.photobooks[i].buildingName, this.prevBooks[i], S0902Constants.BUILDING);
      this.prevValueCreate(this.selectFloor, this.photobooks[i].floorName, this.prevBooks[i], S0902Constants.FLOOR);
      this.prevValueCreate(this.selectRoom, this.photobooks[i].roomName, this.prevBooks[i], S0902Constants.ROOM);
      this.prevValueCreate(this.selectPlace, this.photobooks[i].placeName, this.prevBooks[i], S0902Constants.PLACE);
      this.prevValueCreate(this.selectLargeProcess, this.photobooks[i].largeProcessName, this.prevBooks[i],
        S0902Constants.LARGEPROCESS);
      this.prevValueCreate(this.selectProcess, this.photobooks[i].processName, this.prevBooks[i], S0902Constants.PROCESS);
      this.prevValueCreate(this.selectWork, this.photobooks[i].workName, this.prevBooks[i], S0902Constants.WORK);
      this.prevValueCreate(this.selectSituation, this.photobooks[i].timingName, this.prevBooks[i], S0902Constants.SITUATION);
      this.prevValueCreate(this.selectDate, this.photobooks[i].date, this.prevBooks[i], S0902Constants.DATE);
      this.prevValueCreate(this.selectTime, this.photobooks[i].time, this.prevBooks[i], S0902Constants.TIME);
      this.prevValueCreate(this.selectUnitMarkModelType, this.photobooks[i].unitMarkModelType,
        this.prevBooks[i], S0902Constants.unitMarkModelType);
      this.prevValueCreate(this.selectMachineNumber, this.photobooks[i].machineNumber, this.prevBooks[i], S0902Constants.MACHINENUMBER);
      this.prevValueCreate(this.selectWitness, this.photobooks[i].witness, this.prevBooks[i], S0902Constants.WITNESS);
      this.prevValueCreate(this.selectConstructionCompany, this.photobooks[i].constructionCompany, this.prevBooks[i],
        S0902Constants.CONSTRUCTIONCOMPANY);
      this.prevValueCreate(this.selectFreeColumn, this.photobooks[i].freeColumn, this.prevBooks[i], S0902Constants.FREECOLUMN);
    });
  }

  // 画面表示用情報を作成
  prevValueCreate(item: number, value: string, prevBookCreate: PrevBook, itemName: string): void {
    switch (String(item)) {
      case '1':
        prevBookCreate.item1.push(itemName);
        prevBookCreate.value1.push(value);
        break;
      case '2':
        prevBookCreate.item2.push(itemName);
        prevBookCreate.value2.push(value);
        break;
      case '3':
        prevBookCreate.item3.push(itemName);
        prevBookCreate.value3.push(value);
        break;
      case '4':
        prevBookCreate.item4.push(itemName);
        prevBookCreate.value4.push(value);
        break;
      case '5':
        prevBookCreate.item5.push(itemName);
        prevBookCreate.value5.push(value);
        break;
      case '6':
        prevBookCreate.item6.push(itemName);
        prevBookCreate.value6.push(value);
        break;
      case '7':
        prevBookCreate.item7.push(itemName);
        prevBookCreate.value7.push(value);
        break;
      case '8':
        prevBookCreate.item8.push(itemName);
        prevBookCreate.value8.push(value);
        break;
    }
  }

  // 写真帳レイアウト設定画面に遷移
  prevPage() {
    const url = '/photobook-view-set';
    this.routerService.goRouteLink(url);
  }

  // 写真帳ダウンロードのクリックイベント
  async downloadClick() {
    const alert = await this.alertController.create({
      header: AlertWindowConstants.CONFIRMTITLE,
      message: MessageConstants.C00004,
      buttons: [
        {
          text: AlertWindowConstants.OKBUTTON,
          handler: () => this.startExecutionAPI0902()
        },
        {
          text: AlertWindowConstants.CANCELBUTTON,
          role: 'cancel',
          cssClass: 'primary',
          handler: (blah) => {
          }
        }
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  // 写真帳帳票生成(API-09-02)のタスクを起動します
  private async startExecutionAPI0902() {
    this.appLoadingStoreService.isloading(true);
    await this.startExecutionService.postExec(
      '/API0902',
      this.get0902InDto(),
      async (outDto: StartExecutionOutDto) => {
        if (outDto.executionArn) {
          // タスクの起動が成功しました
          const describeExecutionInDto: DescribeExecutionInDto = {
            executionArn: outDto.executionArn
          };
          this.describeExecutionAPI0902(describeExecutionInDto);
        } else {
          // タスクの起動が失敗しました
          this.appLoadingStoreService.isloading(false);
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        }
      },
      () => this.appLoadingStoreService.isloading(false)
    );
  }

  // 写真帳帳票生成(API-09-02)のタスクを監視します
  private describeExecutionAPI0902(inDto: DescribeExecutionInDto) {
    let timer: NodeJS.Timer;
    this.describeExecutionService.postExec(
      inDto,
      async (outDto: DescribeExecutionOutDto) => {
        if (outDto.status === 'RUNNING') {
          timer = setTimeout(() => {
            this.describeExecutionAPI0902(inDto);
          }, 5000);
        } else {
          clearTimeout(timer);
          if (outDto.status === 'SUCCEEDED') {
            // タスクの実施が成功しました
            this.downloadPDF(JSON.parse(outDto.output));
          } else {
            // タスクの実施が失敗しました
            this.appLoadingStoreService.isloading(false);
            const alert = await this.alertController.create({
              header: AlertWindowConstants.ERRORTITLE,
              message: MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL),
              buttons: [AlertWindowConstants.OKENGLISHBUTTON],
              backdropDismiss: false
            });
            await alert.present();
          }
        }
      },
      () => {
        // 監視ときに通信エラーがある場合、または、APIにシステムエラーがある場合、監視終了
        clearTimeout(timer);
        this.appLoadingStoreService.isloading(false);
      }
    );
  }

  // 写真帳帳票ダウンロード
  private downloadPDF(res0902: API0902OutDto) {
    const fileName = res0902.photoBookFileName;
    this.httpClient.get(res0902.presignedUrl, {
      observe: 'response',
      responseType: 'blob'
    }).subscribe(
      async (res) => {
        if (this.isPc) {
          this.exportFile(res, fileName);
        } else {
          let message = MessageConstants.I00025.replace('{{value1}}', fileName);
          if (this.platform.is('android')) {
            // android
            const de = await this.file.createDir(this.file.externalRootDirectory, 'Android/D-Site', true);
            this.file.writeFile(de.toURL(), fileName, res.body, { replace: true });
            message = message.replace('{{value2}}', 'デバイス管理/Android/D-Site');
          }
          if (this.platform.is('ios')) {
            // ios
            this.file.writeFile(this.file.documentsDirectory, fileName, res.body, { replace: true });
            message = message.replace('{{value2}}', 'ファイル/D-Site');
          }
          const alert = await this.alertController.create({
            header: AlertWindowConstants.TIPSTITLE,
            message,
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        }
        this.appLoadingStoreService.isloading(false);
        if (this.isPc) {
          this.photobookInfoSetStoreService.clearBookInfoset();
          this.photobookViewSetStoreService.claerPhotobookView();
          const url = '/photobook-info-set';
          this.routerService.goRouteLink(url);
        } else {
          this.isDownloaded = true;
        }
      },
      (err) => {
        console.error('Error download file: ' + err);
        this.appLoadingStoreService.isloading(false);
      }
    );
  }

  // InDto内容を作成
  get0902InDto(): API0902InDto {

    const inDto: API0902InDto = {};
    inDto.buildingId = this.commonStoreService.commonStore.buildingId;
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.bigProcessId = this.photobookInfoStore.chooseLargeProcessId;
    inDto.outputPerPage = this.photobookInfoStore.pageCnt;
    inDto.isMobile = this.isPc ? '0' : '1';
    inDto.buildingProjectName = this.buildingName + this.projectName;
    if (this.photobooks.length > 0) {
      inDto.buildingName = this.photobooks[0].buildingName;
      inDto.bigProcessName = this.photobooks[0].largeProcessName;
    } else {
      inDto.buildingName = '';
      inDto.bigProcessName = '';
    }

    inDto.outputRayout = {
      arrangement1List: [],
      arrangement2List: [],
      arrangement3List: [],
      arrangement4List: [],
      arrangement5List: [],
      arrangement6List: [],
      arrangement7List: [],
      arrangement8List: []
    };
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.building, 'building');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.floor, 'floor');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.room, 'room');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.place, 'place');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.largeProcess, 'largeProcess');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.process, 'process');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.work, 'work');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.situation, 'situation');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.date, 'dateTime');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.time, 'time');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.unitMarkModelType, 'unitMarkModelType');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.machineNumber, 'machineNumber');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.witness, 'witness');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.constructionCompany, 'constructionCompany');
    this.convertArrangement(inDto.outputRayout, this.photobookViewStore.freeColumn, 'freeColumn');

    inDto.outputOrderPattern = this.photobookInfoStore.outputOrder;
    const processIdList: Array<ProcessIdDto> = new Array();
    Array.from({ length: this.photobookInfoStore.photobookInfos.length }).map((_, i) => {
      const photobookInfo = this.photobookInfoStore.photobookInfos[i];
      if (photobookInfo.ischeck) {
        const processId = new ProcessIdDto();
        processId.processId = photobookInfo.processId;
        processId.unknownWorkOutput = photobookInfo.unknownWorkOutput ? 1 : 0;
        processId.workIdList = [];
        if (photobookInfo.workList) {
          for (const work of photobookInfo.workList) {
            if (work.ischeck) {
              processId.workIdList.push({ workId: work.workId });
            }
          }
        }
        processIdList.push(processId);
      }
    });
    inDto.processIdList = processIdList;
    inDto.unknownProcessOutput = this.photobookInfoStore.unknownProcessOutput ? 1 : 0;

    return inDto;
  }

  convertArrangement(outputRayout: object, index: number, arrangementName: string) {
    if (index > 0 && index <= 8) {
      outputRayout['arrangement' + index + 'List'].push({ arrangement: arrangementName });
    }
  }

  exportFile(res: HttpResponse<Blob>, fileName: string) {
    const aLink = document.createElement('a');
    aLink.download = fileName;
    aLink.href = URL.createObjectURL(res.body);
    document.body.appendChild(aLink);
    aLink.click();
    document.body.removeChild(aLink);
  }

  async if0109successed(outDto: IF0109OutDto) {
    if (outDto.resultCode === ResultType.BUSINESSERROR) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants[outDto.resultCode],
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  filterValue(list: [], items: string[]) {

    if (!list || list.length === 0) {
      return '';
    }

    const filterValues = list.filter((value) => {
      if (value && value !== '') {
        return true;
      }
      return false;
    });

    if (filterValues.length === 0) {
      return '-';
    }

    const values = list.map((str) => {
      if (!str || str === '') {
        return '-';
      }
      return str;
    });

    const joinstr = this.platformtype === 'photobook-preview-web' || this.platformtype === 'photobook-preview-winapp' ? '，' : '/';

    const index1 = items.indexOf('日付');
    const index2 = items.indexOf('時刻');

    if (index1 > -1 && index2 > -1) {
      const replaceString = list[index1] + joinstr;
      return values.join(joinstr).replace(replaceString, list[index1] + ' ');
    }

    return values.join(joinstr);
  }



}

export class ItemDto {
  // key
  key: string;
  // value
  value: string;
}



export class PhotoBook {
  // 写真パス
  photoRelativePath?: string;
  // 物件ID
  buildingId?: number;
  // 物件名
  buildingName?: string;
  // 案件ID
  projectId?: number;
  // 案件名
  projectName?: string;
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // フロア表示順
  floorOrder?: number;
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 部屋表示順
  roomOrder?: number;
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
  // 場所表示順
  placeOrder?: number;
  // 大工程ID
  largeProcessId?: string;
  // 大工程名
  largeProcessName?: string;
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 工程表示順
  processOrder?: number;
  // 機器ID
  machineId?: number;
  // 記号(機種名)
  unitMarkModelType?: string;
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 作業表示順
  workOrder?: number;
  // 状況ID
  timingId?: number;
  // 状況名
  timingName?: string;
  // 状況表示順
  timingOrder?: number;
  // 日付
  photoDate?: string;
  // 日付
  date?: string;
  // 時間
  time?: string;
  // 機番
  machineNumber?: string;
  // 立会人
  witness?: string;
  // 施工会社
  constructionCompany?: string;
  // 自由欄
  freeColumn?: string;
  // 写真帳出力フラグ
  outFlg?: number;
  // 写真S3Path
  photoPath?: string;
}

export class PrevBook {
  photoPath?: string;
  item1?: Array<string>;
  item2?: Array<string>;
  item3?: Array<string>;
  item4?: Array<string>;
  item5?: Array<string>;
  item6?: Array<string>;
  item7?: Array<string>;
  item8?: Array<string>;
  value1?: Array<string>;
  value2?: Array<string>;
  value3?: Array<string>;
  value4?: Array<string>;
  value5?: Array<string>;
  value6?: Array<string>;
  value7?: Array<string>;
  value8?: Array<string>;
}

export class OutputInfos {
  // 写真
  photo?: string;
  // 物件
  building?: string;
  // フロア
  floor?: string;
  // 部屋
  room?: string;
  // 場所
  place?: string;
  // 大工程
  largeProcess?: string;
  // 工程
  process?: string;
  // 作業
  work?: string;
  // 状況
  situation?: string;
  // 日時
  dateTime?: string;
  // 時間
  time?: string;
  // 機種
  model?: string;
  // 記号
  unitMark?: string;
  // 機番
  machineNumber?: string;
  // 立会人
  witness?: string;
  // 施工会社
  constructionCompany?: string;
  // 自由欄
  freeColumn?: string;
}
