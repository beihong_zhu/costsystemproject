import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatestockPageRoutingModule } from './createstock-routing.module';

import { CreatestockPage } from './createstock.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreatestockPageRoutingModule
  ],
  declarations: [CreatestockPage]
})
export class CreatestockPageModule {}
