import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewproductonshelfPage } from './newproductonshelf.page';

const routes: Routes = [
  {
    path: '',
    component: NewproductonshelfPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewproductonshelfPageRoutingModule {}
