import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomCreateUserSelectPage } from './chatroom-create-user-select.page';

@NgModule({
  providers: [],
  declarations: [ChatroomCreateUserSelectPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomCreateUserSelectPage
      }
    ])
  ]
})
export class ChatroomCreateUserSelectPageModule { }
