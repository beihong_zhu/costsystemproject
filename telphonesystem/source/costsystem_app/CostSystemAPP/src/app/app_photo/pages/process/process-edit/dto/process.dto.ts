import { WorkDto } from './work.dto';

export class ProcessDto {
  // 工程Id
  processId?: number;
  // 選択
  isChecked?: boolean;
  // 工程内容展開
  isOpen?: boolean;
  // 表示順入力
  displayOrder?: number;
  // 工程名
  processName?: string;
  // 更新ユーザ
  updatedBy?: string;
  // 作業
  workList?: Array<WorkDto>;
}
