import { NgModule } from '@angular/core';
import { BarcodeScanPage } from './barcode-scan.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { MachineNumberRegistComponent } from './machine-number-regist/machine-number-regist.component';

@NgModule({
  declarations: [BarcodeScanPage, MachineNumberRegistComponent],
  providers: [],
  entryComponents: [MachineNumberRegistComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: BarcodeScanPage
      }
    ])
  ]
})
export class BarcodeScanModule { }
