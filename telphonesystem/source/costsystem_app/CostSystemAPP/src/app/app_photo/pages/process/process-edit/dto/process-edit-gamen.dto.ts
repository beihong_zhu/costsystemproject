import { ProcessDto } from './process.dto';

export class ProcessEditGamenDto {
  // 選択数表示
  selectCnt?: number;
  // 工程名一覧
  processList?: Array<ProcessDto>;
  // 更新ユーザ
  updatedBy?: string;
}
