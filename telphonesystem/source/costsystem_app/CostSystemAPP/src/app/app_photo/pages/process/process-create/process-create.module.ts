import { NgModule } from '@angular/core';
import { ProcessCreatePage } from './process-create.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { WorkAddPage } from './work-add/work-add.page';
import { ProcessAddPage } from './process-add/process-add.page';

@NgModule({
  providers: [],
  declarations: [ProcessCreatePage, ProcessAddPage, WorkAddPage],
  entryComponents: [
    WorkAddPage,
    ProcessAddPage
  ],
  imports: [
    SharedModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProcessCreatePage
      }
    ])
  ]
})
export class ProcessCreateModule { }
