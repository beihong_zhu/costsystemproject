import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersmanagementPageRoutingModule } from './ordersmanagement-routing.module';

import { OrdersmanagementPage } from './ordersmanagement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersmanagementPageRoutingModule
  ],
  declarations: [OrdersmanagementPage]
})
export class OrdersmanagementPageModule {}
