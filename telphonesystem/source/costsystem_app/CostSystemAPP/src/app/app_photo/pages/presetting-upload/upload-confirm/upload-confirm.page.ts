import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, AlertController, Platform } from '@ionic/angular';
import { S1401Constants, AlertWindowConstants } from '@constant/constant.photo';
import {
  API1401Service,
  API1402Service,
  API1403Service,
  API1404Service,
  API1407Service,
  API1408Service,
  StartExecutionService,
  DescribeExecutionService
} from '@service/api';
import {
  API1407OutDto,
  API1408OutDto,
  APIPresettingInDto,
  APIPresettingOutDto,
  StartExecutionOutDto,
  DescribeExecutionInDto,
  DescribeExecutionOutDto
} from '@service/dto';
import { RoutingService } from '@service/app-route.service';
import { MessageConstants } from '@constant/message-constants';
import { CD0014Constants } from '@constant/code';
import {
  AppState,
  CommonStore,
  CommonStoreService,
  AppStateStoreService,
  PreSettingStoreService,
  PreSettingStore,
  AppLoadingStoreService
} from '@store';
import {
  UserCsv,
  BukenCsv,
  BukenHeyaBasyoCsv,
  AnkenTotalCsv,
  AnkenCsv,
  UserKanliCsv,
  BukenZumenInfoCsv,
  MachineInfoCsv,
  ProjInfoCsv,
  PeopleInfoCsv,
  BlackboardProjInfoCsv
} from '../../../../app_photo/store/presetting-upload/presetting.store';
import { ResultType, UpdateMessage } from '../../../../app_common/constant/common';
import { DownloadConfirmPage } from './download-confirm/download-confirm.page';
import { HttpClient, HttpResponse } from '@angular/common/http';
@Component({
  selector: 'app-upload-confirm',
  templateUrl: './upload-confirm.page.html',
  styleUrls: ['./upload-confirm.page.scss']
})

// 事前設定情報登録確認画面
export class UploadConfirmPage implements OnInit, OnDestroy {

  isPc: boolean;

  appState: AppState;
  commonStore: CommonStore;
  preSettingStore: PreSettingStore;
  // 対象物件レベル
  targetBuildingLabel: string = S1401Constants.TARGET_BUILDING;
  // 対象案件レベル
  targetProjectLabel: string = S1401Constants.TARGET_PROJECT;
  // 設定対象レベル
  settingTargetLabel: string = S1401Constants.SETTING_TARGET;
  // 入力CSVレベル
  inputCsvLabel: string = S1401Constants.INPUT_CSV;
  // 入力PDFレベル
  inputPdfLabel: string = S1401Constants.INPUT_PDF;
  // 登録内容レベル
  registerDetailLabel: string = S1401Constants.UPLOAD_CONTENTS;
  // 登録button
  registerButtonLabel: string = S1401Constants.UPLOAD_LABEL;
  // 選択した設定対象名称を格納
  settingTargetName = '';
  // 選択した対象物件名称を格納
  targetBuildingName = '';
  // 選択した対象案件名称を格納
  targetProjectName = '';
  // 入力CSV値を格納
  inputCsvPath = '';
  // 入力PDF値を格納
  inputPdfPath = '';
  file: File = null;
  // CSV種類
  csvKbn = '';
  // ユーザCSV
  userCsvData: UserCsv[];
  // ユーザCSV画面表示
  userCsvDisplayData: UserCsv[];
  // 物件CSV
  bukenCsvData: BukenCsv[];
  // 物件CSV画面表示
  bukenCsvDisplayData: BukenCsv[];
  // 物件フロア・部屋・場所CSV
  bukenHeyaBasyoCsvData: BukenHeyaBasyoCsv[];
  // 物件フロア・部屋・場所CSV画面表示
  bukenHeyaBasyoDisplayCsvData: BukenHeyaBasyoCsv[];
  // 案件情報CSV
  ankenTotalCsvData: AnkenTotalCsv;
  ankenTotalCsvDisplayData: AnkenTotalCsv;
  // 案件CSV
  ankenCsvData: AnkenCsv[];
  // 案件CSV画面表示
  ankenDisplayCsvData: AnkenCsv[];
  // ユーザ担当管理CSV
  userKanliCsvData: UserKanliCsv[];
  // ユーザ担当管理CSV画面表示
  userKanliDisplayCsvData: UserKanliCsv[];
  // 物件図面情報CSV
  bukenZumenInfoCsvData: BukenZumenInfoCsv[];
  // 物件図面情報CSV画面表示
  bukenZumenInfoDisplayCsvData: BukenZumenInfoCsv[];
  // 機器情報CSV
  machineInfoCsvData: MachineInfoCsv[];
  // 機器情報CSV画面表示
  machineInfoDisplayCsvData: MachineInfoCsv[];
  // 工程作業情報CSV
  projInfoCsvData: ProjInfoCsv[];
  // 工程作業情報CSV画面表示
  projInfoDisplayCsvData: ProjInfoCsv[];
  // 立会人情報CSV
  peopleInfoCsvData: PeopleInfoCsv[];
  // 立会人情報CSV画面表示
  peopleInfoDisplayCsvData: PeopleInfoCsv[];
  // 黒板表示項目情報CSV
  blackboardProjInfoCsvData: BlackboardProjInfoCsv[];
  // 黒板表示項目情報CSV画面表示
  blackboardProjInfoDisplayCsvData: BlackboardProjInfoCsv[];
  // message
  message = '';
  // formdata
  formdata: FormData;
  // ダウンロード完了の判断
  isDownloaded: boolean;
  // ユーザCSVヘッダ名
  userCsvHead: string[] = [
    S1401Constants.USERCSV_USERKBN,
    S1401Constants.USERCSV_USERID,
    S1401Constants.USERCSV_BEARERSNAME,
    S1401Constants.USERCSV_MAILADD,
    S1401Constants.USERCSV_PASSWORD,
    S1401Constants.USERCSV_KAISHANAME,
    S1401Constants.USERCSV_AUTHOR,
    S1401Constants.USERCSV_MESSAGE
  ];
  // 物件CSVヘッダ名
  bukenCsvHead: string[] = [
    S1401Constants.BUKENCSV_BUKENKBN,
    S1401Constants.BUKENCSV_BUKENID,
    S1401Constants.BUKENCSV_BUKENNAME,
    S1401Constants.BUKENCSV_BUKENPOST,
    S1401Constants.BUKENCSV_ADD1,
    S1401Constants.BUKENCSV_ADD2,
    S1401Constants.BUKENCSV_ADD3,
    S1401Constants.BUKENCSV_TELEPHONE,
    S1401Constants.BUKENCSV_MESSAGE
  ];
  // 物件フロア・部屋・場所CSVヘッダ名
  bukenHeyaBasyoCsvHead: string[] = [
    S1401Constants.BUKENHEYABASYOCSV_BUKENID,
    S1401Constants.BUKENHEYABASYOCSV_BUKENNAME,
    S1401Constants.BUKENHEYABASYOCSV_FROAKBN,
    S1401Constants.BUKENHEYABASYOCSV_FROAID,
    S1401Constants.BUKENHEYABASYOCSV_FROANAME,
    S1401Constants.BUKENHEYABASYOCSV_HEYAKBN,
    S1401Constants.BUKENHEYABASYOCSV_HEYAID,
    S1401Constants.BUKENHEYABASYOCSV_HEYANAME,
    S1401Constants.BUKENHEYABASYOCSV_BASYOKBN,
    S1401Constants.BUKENHEYABASYOCSV_BASYOID,
    S1401Constants.BUKENHEYABASYOCSV_BASYONAME,
    S1401Constants.BUKENHEYABASYOCSV_MESSAGE
  ];
  // 案件CSVヘッダ名
  ankenCsvHead: string[] = [
    S1401Constants.ANKENCSV_ANKENID,
    S1401Constants.ANKENCSV_ANKENNAME,
    S1401Constants.ANKENCSV_MESSAGE
  ];
  // ユーザ担当管理CSVヘッダ名
  userKanliCsvHead: string[] = [
    S1401Constants.USERKANLICSV_USERKBN,
    S1401Constants.USERKANLICSV_USERID,
    S1401Constants.USERKANLICSV_MESSAGE
  ];
  // 物件図面情報CSVヘッダ名
  bukenZumenInfoCsvHead: string[] = [
    S1401Constants.BUKENZUMENINFOCSV_ZUMENKBN,
    S1401Constants.BUKENZUMENINFOCSV_FROANAME,
    S1401Constants.BUKENZUMENINFOCSV_ZUMENPATH,
    S1401Constants.BUKENZUMENINFOCSV_ZUMENNAME,
    S1401Constants.BUKENZUMENINFOCSV_MESSAGE
  ];
  // 機器情報CSVヘッダ名
  machineInfoCsvHead: string[] = [
    S1401Constants.MACHINEINFOCSV_MACHINEKBN,
    S1401Constants.MACHINEINFOCSV_SYSNAME,
    S1401Constants.MACHINEINFOCSV_SIGNAL,
    S1401Constants.MACHINEINFOCSV_OUTMACHINENAME,
    S1401Constants.MACHINEINFOCSV_INMACHINENAME,
    S1401Constants.MACHINEINFOCSV_FROANAME,
    S1401Constants.MACHINEINFOCSV_HEYANAME,
    S1401Constants.MACHINEINFOCSV_BASYONAME,
    S1401Constants.MACHINEINFOCSV_MESSAGE
  ];
  // 工程作業情報CSVヘッダ名
  projInfoCsvHead: string[] = [
    S1401Constants.PROJINFOCSV_BIGPROJ,
    S1401Constants.PROJINFOCSV_PROJID,
    S1401Constants.PROJINFOCSV_PROJNAME,
    S1401Constants.PROJINFOCSV_WORKID,
    S1401Constants.PROJINFOCSV_WORKNAME,
    S1401Constants.PROJINFOCSV_DOBEFORE,
    S1401Constants.PROJINFOCSV_DOING,
    S1401Constants.PROJINFOCSV_DOAFTER,
    S1401Constants.PROJINFOCSV_MESSAGE
  ];
  // 立会人情報CSVヘッダ名
  peopleInfoCsvHead: string[] = [
    S1401Constants.PEOPLEINFOCSV_PEOPLEKBN,
    S1401Constants.PEOPLEINFOCSV_PEOPLEID,
    S1401Constants.PEOPLEINFOCSV_PEOPLENAME,
    S1401Constants.PEOPLEINFOCSV_DOKAISHAID,
    S1401Constants.PEOPLEINFOCSV_DOKAISHANAME,
    S1401Constants.PEOPLEINFOCSV_WORKNAME
  ];
  // 黒板表示項目情報CSVヘッダ名
  blackboardProjInfoCsvHead: string[] = [
    S1401Constants.BLACKBOARDPROJINFOCSV_BIGPROJ,
    S1401Constants.BLACKBOARDPROJINFOCSV_ANKENFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_FROAFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_HEYAFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_BASYOFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_LINEFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_MACHINEFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_PROFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_WORKFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_FREEDOMEARFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_PEOPLEFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_DOKAISHAFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_DATEFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_TIMEFLG,
    S1401Constants.BLACKBOARDPROJINFOCSV_MESSAGE
  ];

  /**
   * 事前設定情報登録確認画面の　コンストラクタ
   * @param alertController AlertController
   * @param routerService  ルーターサービスオブジェクト
   * @param modalController  モデル画面コントローラーオブジェクト
   * @param commonStoreService 共通ストアサービスオブジェクト
   * @param appStateService アプリステータスサービスオブジェクト
   * @param preSettingStoreService 事前設定ストアサービスオブジェクト
   * @param api0101Service ０１０１APIサービスオブジェクト
   * @param api1401Service 1401APIサービスオブジェクト
   * @param api1402Service 1402APIサービスオブジェクト
   * @param api1403Service 1403APIサービスオブジェクト
   * @param api1404Service 1404APIサービスオブジェクト
   * @param api1407Service 1407APIサービスオブジェクト
   */
  constructor(
    private routerService: RoutingService,
    public alertController: AlertController,
    public modalController: ModalController,
    private commonStoreService: CommonStoreService,
    private appStateService: AppStateStoreService,
    private preSettingStoreService: PreSettingStoreService,
    private api1401Service: API1401Service,
    private api1402Service: API1402Service,
    private api1403Service: API1403Service,
    private api1404Service: API1404Service,
    private api1407Service: API1407Service,
    private api1408Service: API1408Service,
    private appLoadingStoreService: AppLoadingStoreService,
    public startExecutionService: StartExecutionService<APIPresettingInDto>,
    public describeExecutionService: DescribeExecutionService,
    protected httpClient: HttpClient,
    private platform: Platform) {

    // APPストアに保存した物件一覧を取得
    this.appState = this.appStateService.appState;

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    // APPストアに保存した写真帳出力情報を取得
    this.preSettingStore = this.preSettingStoreService.preSettingStore;
  }

  ngOnDestroy(): void {
    this.preSettingStoreService.preSettingStore.pdfFiles = null;
    this.preSettingStoreService.setPreSetting(this.preSettingStore);
  }

  /**
   * 初期化
   */
  ngOnInit() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.initData();
  }

  /**
   * 画面データ初期設定
   */
  initData(): void {
    // 選択した設定対象名称を設定
    this.settingTargetName = this.preSettingStore.settingTargetName;
    // 選択した対象物件名称を設定
    this.targetBuildingName = this.preSettingStore.targetBuildingName;
    // 選択した対象案件名称を設定
    this.targetProjectName = this.preSettingStore.targetProjectName;
    // 入力CSVファイル名を設定
    this.inputCsvPath = this.preSettingStore.inputCsvPath;
    // 入力PDFファイル名を設定
    this.inputPdfPath = this.preSettingStore.pdfNames;
    // 入力CSVファイル名を設定
    this.file = this.preSettingStore.file;
    // 入力CSVファイル名を設定
    this.csvKbn = this.preSettingStore.csvKbn;
    // 案件CSVの場合
    if (this.csvKbn === CD0014Constants.CSVKIND_ANKEN) {
      // this.message = MessageConstants.I00018;
      this.message = '登録内容の各CSV上位50行のみ表示されます。';
    } else {
      this.message = MessageConstants.I00017;
    }

    this.userCsvData = this.preSettingStore.userCsvData;
    this.bukenCsvData = this.preSettingStore.bukenCsvData;
    this.bukenHeyaBasyoCsvData = this.preSettingStore.bukenHeyaBasyoCsvData;
    this.ankenTotalCsvData = this.preSettingStore.ankenTotalCsvData;
    this.ankenCsvData = this.ankenTotalCsvData.anken;
    this.userKanliCsvData = this.ankenTotalCsvData.userKanli;
    this.bukenZumenInfoCsvData = this.ankenTotalCsvData.bukenInfo;
    this.machineInfoCsvData = this.ankenTotalCsvData.machineInfo;
    this.projInfoCsvData = this.ankenTotalCsvData.projInfo;
    this.peopleInfoCsvData = this.ankenTotalCsvData.peopleInfo;
    this.blackboardProjInfoCsvData = this.ankenTotalCsvData.blackProjInfo;

    // 上位200行表示される。
    // ユーザCSV
    if (this.csvKbn === CD0014Constants.CSVKIND_USER) {
      if (this.userCsvData && this.userCsvData.length > 200) {
        console.log(this.userCsvData);
        this.userCsvDisplayData = this.userCsvData.slice(0, 200);
        // let i = 0;
        // while (i < 202) {
        //   console.log(1111)
        //   this.userCsvDisplayData.push(this.userCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.userCsvDisplayData = this.userCsvData;
      }
      // 物件CSV
    } else if (this.csvKbn === CD0014Constants.CSVKIND_OBJECT) {
      if (this.bukenCsvData && this.bukenCsvData.length > 200) {
        this.bukenCsvDisplayData = this.bukenCsvData.slice(0, 200);
        // let i = 0;
        // while (i < 202) {
        //   this.bukenCsvDisplayData.push(this.bukenCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.bukenCsvDisplayData = this.bukenCsvData;
      }
      // 物件フロア・部屋・場所CSV
    } else if (this.csvKbn === CD0014Constants.CSVKIND_OBJ_ROOM_PLACE) {
      if (this.bukenHeyaBasyoCsvData && this.bukenHeyaBasyoCsvData.length > 202) {
        this.bukenHeyaBasyoDisplayCsvData = this.bukenHeyaBasyoCsvData.slice(0, 200);
        // let i = 0;
        // while (i < 202) {
        //   this.bukenHeyaBasyoDisplayCsvData.push(this.bukenHeyaBasyoCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.bukenHeyaBasyoDisplayCsvData = this.bukenHeyaBasyoCsvData;
      }
      // 案件情報CSV
    } else {
      // 案件CSV
      if (this.ankenCsvData && this.ankenCsvData.length > 50) {
        this.ankenDisplayCsvData = this.ankenCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.ankenDisplayCsvData.push(this.ankenCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.ankenDisplayCsvData = this.ankenCsvData;
      }
      // ユーザ担当管理CSV
      if (this.userKanliCsvData && this.userKanliCsvData.length > 50) {
        this.userKanliDisplayCsvData = this.userKanliCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.userKanliDisplayCsvData.push(this.userKanliCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.userKanliDisplayCsvData = this.userKanliCsvData;
      }
      // 物件図面情報CSV
      if (this.bukenZumenInfoCsvData && this.bukenZumenInfoCsvData.length > 50) {
        this.bukenZumenInfoDisplayCsvData = this.bukenZumenInfoCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.bukenZumenInfoDisplayCsvData.push(this.bukenZumenInfoCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.bukenZumenInfoDisplayCsvData = this.bukenZumenInfoCsvData;
      }
      // 機器情報CSV
      if (this.machineInfoCsvData && this.machineInfoCsvData.length > 50) {
        this.machineInfoDisplayCsvData = this.machineInfoCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.machineInfoDisplayCsvData.push(this.machineInfoCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.machineInfoDisplayCsvData = this.machineInfoCsvData;
      }
      // 工程作業情報CSV
      if (this.projInfoCsvData && this.projInfoCsvData.length > 50) {
        this.projInfoDisplayCsvData = this.projInfoCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.projInfoDisplayCsvData.push(this.projInfoCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.projInfoDisplayCsvData = this.projInfoCsvData;
      }
      // 立会人情報CSV
      if (this.peopleInfoCsvData && this.peopleInfoCsvData.length > 50) {
        this.peopleInfoDisplayCsvData = this.peopleInfoCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.peopleInfoDisplayCsvData.push(this.peopleInfoCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.peopleInfoDisplayCsvData = this.peopleInfoCsvData;
      }
      // 黒板表示項目情報CSV
      if (this.blackboardProjInfoCsvData && this.blackboardProjInfoCsvData.length > 52) {
        this.blackboardProjInfoDisplayCsvData = this.blackboardProjInfoCsvData.slice(0, 50);
        // let i = 0;
        // while (i < 52) {
        //   this.blackboardProjInfoDisplayCsvData.push(this.blackboardProjInfoCsvData[i]);
        //   i = i + 1;
        // }
      } else {
        this.blackboardProjInfoDisplayCsvData = this.blackboardProjInfoCsvData;
      }
    }
  }

  private getPresettingInDto(csvInputFileRelativePath: string): APIPresettingInDto {
    const inDto: APIPresettingInDto = {};
    if (this.preSettingStore.targetBuildingId !== null && this.preSettingStore.targetBuildingId !== '') {
      const targetBuildingId = this.preSettingStore.targetBuildingId;
      inDto.buildingId = targetBuildingId;
    }
    inDto.csvInputFileName = this.preSettingStore.file.name;
    inDto.csvInputFileRelativePath = csvInputFileRelativePath;
    return inDto;
  }

  private getCreateInDto(apiName: string): FormData {
    const formData = new FormData();
    const targetBuildingId = this.preSettingStore.targetBuildingId;
    formData.append('buildingId', targetBuildingId);
    formData.append('apiName', apiName);
    formData.append('csvInputFile', this.preSettingStore.file);
    return formData;
  }

  private getCreatePDFInDto(pdfFileName: string, pdfFile: File): FormData {
    const formData = new FormData();
    const targetBuildingId = this.preSettingStore.targetBuildingId;
    formData.append('buildingId', targetBuildingId);
    formData.append('pdfFileName', pdfFileName);
    formData.append('pdfFile', pdfFile);
    return formData;
  }

  private exportFile(res: HttpResponse<Blob>, fileName: string) {
    const aLink = document.createElement('a');
    aLink.download = fileName;
    aLink.href = URL.createObjectURL(res.body);
    document.body.appendChild(aLink);
    aLink.click();
    document.body.removeChild(aLink);
  }

  private async apiSuccessed(res: HttpResponse<Blob>, apiPresettingOutDto: APIPresettingOutDto) {
    this.preSettingStore.message = apiPresettingOutDto.messageCode;
    const resultCode = apiPresettingOutDto.resultCode;
    let msg: string = '';
    if (resultCode === ResultType.NORMAL || resultCode === ResultType.BUSINESSERROR) {
      if (resultCode === ResultType.NORMAL) {
        this.preSettingStore.message = '0|I00002|';
      } else {
        msg = UpdateMessage.setMessage(
          apiPresettingOutDto.messageCode,
          apiPresettingOutDto.message);
        this.preSettingStore.message = resultCode
          + '|'
          + apiPresettingOutDto.messageCode
          + '|'
          + msg;
      }
      this.openDownLoadDialog(res, apiPresettingOutDto.csvOutputFileName);
    }
    if (resultCode === ResultType.SYSTEMERROR) {
      msg = UpdateMessage.setMessage(
        apiPresettingOutDto.messageCode,
        apiPresettingOutDto.message);
      console.log(msg);
    }
  }

  private async apiFailed(apiPresettingOutDto: APIPresettingOutDto) {
    this.preSettingStore.message = apiPresettingOutDto.messageCode;
    const resultCode = apiPresettingOutDto.resultCode;
    let msg: string = '';
    if (resultCode === ResultType.NORMAL || resultCode === ResultType.BUSINESSERROR) {
      if (resultCode === ResultType.NORMAL) {
        this.preSettingStore.message = '0|I00002|';
      } else {
        msg = UpdateMessage.setMessage(
          apiPresettingOutDto.messageCode,
          apiPresettingOutDto.message);
        this.preSettingStore.message = resultCode
          + '|'
          + apiPresettingOutDto.messageCode
          + '|'
          + msg;
      }
      this.routerService.goRouteLink('/presetting-upload');
    }
    if (resultCode === ResultType.SYSTEMERROR) {
      msg = UpdateMessage.setMessage(
        apiPresettingOutDto.messageCode,
        apiPresettingOutDto.message);
      console.log(msg);
    }
  }

  private async openDownLoadDialog(res: HttpResponse<Blob>, fileName: string) {
    const modal = await this.modalController.create({
      component: DownloadConfirmPage,
      componentProps: {
        value: fileName
      },
      cssClass: 'downloadConfirmModal'
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // 追加ボタンを押下するか判断
    if (data != null) {
      if (data) {
        this.exportFile(res, fileName);
      }
      this.preSettingStore.confirmSuccess = true;
      this.routerService.goRouteLink('/presetting-upload');
    }
  }

  private async api1407ServicePostExec(pdfFileName: string, pdfFile: File) {
    return new Promise((resolve, reject) => {
      this.api1407Service.postExec(this.getCreatePDFInDto(pdfFileName, pdfFile),
        async (outDto1407: API1407OutDto) => {
          resolve(outDto1407);
        },
        () => {
          reject(new Error('1407'));
        }
      );
    });
  }

  private async api1408ServicePostExec(apiName: string) {
    return new Promise((resolve, reject) => {
      this.api1408Service.postExec(this.getCreateInDto(apiName),
        async (outDto1408: API1408OutDto) => {
          resolve(outDto1408);
        },
        () => {
          reject(new Error('1408'));
        }
      );
    });
  }

  // outdto情報を取得
  private async getOutDtoBusinessError(resultCode: string, messageCode: string, message: string) {
    let msg: string = '';
    msg = UpdateMessage.setMessage(
      messageCode,
      message);
    this.preSettingStore.message = resultCode
      + '|'
      + messageCode
      + '|'
      + msg;
    this.preSettingStore.pdfFiles = null;
    this.routerService.goRouteLink('/presetting-upload');
  }

  // stepfunction実行を行う
  private async cd0014ConstantsExecution(
    resultCode: string,
    messageCode: string,
    message: string,
    csvInputFileRelativePath: string,
    csvKind: string) {
    this.preSettingStore.message = messageCode;
    if (resultCode === ResultType.BUSINESSERROR) {
      this.getOutDtoBusinessError(resultCode, messageCode, message);
      this.preSettingStoreService.setPreSetting(this.preSettingStore);
      return;
    } else if (resultCode === ResultType.NORMAL) {
      this.preSettingStore.csvInputFileRelativePath = csvInputFileRelativePath;
    }
    await this.startExecution(this.preSettingStore.csvInputFileRelativePath, csvKind);
  }

  // step functionでAPIにデータを送る
  async onRegister() {
    try {
      if (this.csvKbn === CD0014Constants.CSVKIND_USER) {
        const outDto1408: API1408OutDto = await this.api1408ServicePostExec(CD0014Constants.CSVKIND_1401);
        this.cd0014ConstantsExecution(
          outDto1408.resultCode,
          outDto1408.messageCode,
          outDto1408.message,
          outDto1408.csvInputFileRelativePath,
          CD0014Constants.CSVKIND_1401);
      } else if (this.csvKbn === CD0014Constants.CSVKIND_OBJECT) {
        const outDto1408: API1408OutDto = await this.api1408ServicePostExec(CD0014Constants.CSVKIND_1402);
        this.cd0014ConstantsExecution(
          outDto1408.resultCode,
          outDto1408.messageCode,
          outDto1408.message,
          outDto1408.csvInputFileRelativePath,
          CD0014Constants.CSVKIND_1402);
      } else if (this.csvKbn === CD0014Constants.CSVKIND_OBJ_ROOM_PLACE) {
        const outDto1408: API1408OutDto = await this.api1408ServicePostExec(CD0014Constants.CSVKIND_1403);
        this.cd0014ConstantsExecution(
          outDto1408.resultCode,
          outDto1408.messageCode,
          outDto1408.message,
          outDto1408.csvInputFileRelativePath,
          CD0014Constants.CSVKIND_1403);
      } else {
        const pdfNamesArr = this.preSettingStore.pdfNames.split(',');
        if (this.preSettingStore.pdfFiles !== null) {
          if (this.preSettingStore.pdfFiles.length > 0) {
            for (const pdfFile of this.preSettingStore.pdfFiles) {
              if (pdfNamesArr.includes(pdfFile.name)) {
                let outDto1407: API1407OutDto = null;
                outDto1407 = await this.api1407ServicePostExec(pdfFile.name, pdfFile);
                this.preSettingStore.message = outDto1407.messageCode;
                if (outDto1407.resultCode === ResultType.BUSINESSERROR) {
                  this.getOutDtoBusinessError(outDto1407.resultCode, outDto1407.messageCode, outDto1407.message);
                  this.preSettingStoreService.setPreSetting(this.preSettingStore);
                  return;
                }
              }
            }
          }
        }
        const outDto1408: API1408OutDto = await this.api1408ServicePostExec(CD0014Constants.CSVKIND_1404);
        this.cd0014ConstantsExecution(
          outDto1408.resultCode,
          outDto1408.messageCode,
          outDto1408.message,
          outDto1408.csvInputFileRelativePath,
          CD0014Constants.CSVKIND_1404);
      }
      this.preSettingStoreService.setPreSetting(this.preSettingStore);
    } catch (err) {
      if (err.message !== '1407' && err.message !== '1408') {
        // 共通処理
        throw err;
      }
    }
  }

  // step functionタスクを起動します
  private async startExecution(csvInputFileRelativePath: string, apiName: string) {
    this.appLoadingStoreService.isloading(true);
    await this.startExecutionService.postExec(
      '/' + apiName,
      this.getPresettingInDto(csvInputFileRelativePath),
      async (outDto: StartExecutionOutDto) => {
        if (outDto.executionArn) {
          // step functionタスクの起動が成功しました
          const describeExecutionInDto: DescribeExecutionInDto = {
            executionArn: outDto.executionArn
          };
          this.describeExecution(describeExecutionInDto);
        } else {
          // step functionタスクの起動が失敗しました
          this.appLoadingStoreService.isloading(false);
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
        }
      },
      () => this.appLoadingStoreService.isloading(false)
    );
  }

  // step functionタスクを監視します
  private describeExecution(inDto: DescribeExecutionInDto) {
    let timer: NodeJS.Timer;
    this.describeExecutionService.postExec(
      inDto,
      async (outDto: DescribeExecutionOutDto) => {
        if (outDto.status === 'RUNNING') {
          timer = setTimeout(() => {
            this.describeExecution(inDto);
          }, 5000);
        } else {
          clearTimeout(timer);
          if (outDto.status === 'SUCCEEDED') {
            // タスクの実施が成功しました
            this.downloadCSV(JSON.parse(outDto.output));
          } else {
            // タスクの実施が失敗しました
            this.appLoadingStoreService.isloading(false);
            const alert = await this.alertController.create({
              header: AlertWindowConstants.ERRORTITLE,
              message: MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL),
              buttons: [AlertWindowConstants.OKENGLISHBUTTON],
              backdropDismiss: false
            });
            await alert.present();
          }
        }
      },
      () => {
        // 監視ときに通信エラーがある場合、または、APIにシステムエラーがある場合、監視終了
        clearTimeout(timer);
        this.appLoadingStoreService.isloading(false);
      }
    );
  }

  // step function結果CSVファイルダウンロード
  private downloadCSV(apiPresettingOutDto: APIPresettingOutDto) {
    if (apiPresettingOutDto.outputFilePresignedUrl !== null && apiPresettingOutDto.outputFilePresignedUrl !== '') {
      this.httpClient.get(apiPresettingOutDto.outputFilePresignedUrl, {
        observe: 'response',
        responseType: 'blob'
      }).subscribe(
        async (res: HttpResponse<Blob>) => {
          this.apiSuccessed(res, apiPresettingOutDto);
          this.appLoadingStoreService.isloading(false);
        },
        async (err) => {
          // ログに出力
          console.log('Error download file: ' + err.message);
          // タスクの実施が失敗しました
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            message: MessageConstants.E00111.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          await alert.present();
          this.appLoadingStoreService.isloading(false);
        }
      );
    } else {
      this.apiFailed(apiPresettingOutDto);
      this.appLoadingStoreService.isloading(false);
    }
  }
}
