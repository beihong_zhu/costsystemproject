import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChattalkPage } from './chattalk.page';
import { PhotoGridItemComponent } from './photo-grid-item/photo-grid-item.component';
import { PhotoViewComponent } from './photo-view/photo-view.component';

@NgModule({
  providers: [],
  declarations: [ChattalkPage,
    PhotoGridItemComponent,
    PhotoViewComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChattalkPage
      }
    ])
  ],
  entryComponents: [PhotoViewComponent]
})
export class ChattalkPageModule { }
