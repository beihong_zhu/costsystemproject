export class SysOnShelfProductInfoDto {
   
   AgentReduceName?: string;
   productName?: string;
   productType ?:string;
   spId?:string;
   value ?: string ;
   price ?: string ; 
   disaccount ?: string ;
   checkPrice ?: string;
   checkCost ?: string ;
   status ?: string ;

}
