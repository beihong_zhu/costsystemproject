

export class Building {
  buildingId: number;
  buildingName: string;
  buildingAddress: string;
}
