import { Component, OnInit } from '@angular/core';
import { API3012Service } from '@service/api';
import { API3012InDto, API3012OutDto } from '@service/dto';
import { AppState, CommonStore } from '@store';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysChannelInfoDto } from './dto';


@Component({
  selector: 'app-suppliedmanagement',
  templateUrl: './suppliedmanagement.page.html',
  styleUrls: ['./suppliedmanagement.page.scss'],
})
export class SuppliedmanagementPage implements OnInit {


  public  channelbre : string;
  public  supplyInfo:any = {
    bussinessInfoList:['其它' ,'支付宝','微信'],
    bussiness:'支付宝'}

    public sysChannelInfoList: Array<SysChannelInfoDto> = new Array<SysChannelInfoDto>();


  constructor(private api3012Service: API3012Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api3012ServicePostExec();
  }

  private get3012InDto() {
    let api3012InDto: API3012InDto = {};

    api3012InDto.abbreviation = this.channelbre;
  

    return api3012InDto;
  }

  private api3012ServicePostExec() {


    // 写真帳出力情報取得
    this.api3012Service.postExecNew(this.get3012InDto()).then((res) => {

      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3012successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }


  private api3012successed(outDto: API3012OutDto) {
    this.sysChannelInfoList = new Array<SysChannelInfoDto>();

    if (!(outDto.authoryInfoResList === undefined || outDto.authoryInfoResList === null)) {
      const loadSysusers = JSON.parse(JSON.stringify(outDto.authoryInfoResList));
      for (const sysUserInfoModel of loadSysusers) {
        let sysUserDto: SysChannelInfoDto = new SysChannelInfoDto();
        sysUserDto.authoryName = sysUserInfoModel.authoryName;
        sysUserDto.abbreviation = sysUserInfoModel.abbreviation;
        this.sysChannelInfoList.push(sysUserDto);
      }
    }
  }
}
