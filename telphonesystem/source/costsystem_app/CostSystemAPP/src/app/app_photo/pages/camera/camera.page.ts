import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { PhotoConf } from '@conf/photo-conf';
import { S0301Assets } from '@constant/assets';
import { S0301Constants, AlertWindowConstants } from '@constant/constant.photo';
import { CameraPreview, CameraPreviewOptions, CameraPreviewPictureOptions } from '@ionic-native/camera-preview/ngx';
import { Device } from '@ionic-native/device/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { AlertController, Events, ModalController, Platform, PopoverController } from '@ionic/angular';
import { select } from '@ngrx/store';
import { CameraModalEditComponent } from '@pages/camera/modal-edit/camera-modal-edit.component';
import { CameraPopoverComponent } from '@pages/camera/popover/camera-popover.component';
import { TextSinglePopoverComponent } from '@pages/camera/text-popover/text-popover.component';
import { RoutingService } from '@service/app-route.service';
import { TbPhoto } from '@service/db/db-manage';
import { DbPhotoService } from '@service/db/db-photoservice';
import {
  AppLoadingStoreService,
  CameraActionTypes,
  CameraStore,
  CameraStoreService,
  CommonStore,
  CommonStoreService,
  HeadStateService,
  InitInterfaceType,
  LoggerStoreService,
  PhotoDetailStoreService,
  PhotomanagestoreService,
  AppStateStoreService
} from '@store';
import * as moment from 'moment';
import { Machine, Floor, Room, Place } from '../../store/camera/camera.store';
import { MessageConstants } from '@constant/message-constants';
import * as rasterizeHTML from 'rasterizehtml';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

declare const Keyboard: any;

@Component({
  selector: 'app-camera',
  templateUrl: 'camera.page.html',
  styleUrls: ['camera.page.mobile.scss', 'camera.page.winapp.scss'],
  encapsulation: ViewEncapsulation.None
})


export class CameraPage implements OnInit, OnDestroy {

  // 撮影関連
  private photoRatio = 4 / 3;
  private liveviewLeft = 0;
  private liveviewTop = 0;
  private liveviewWidth = 0;
  private liveviewHeight = 0;
  cameraStartSuccess = false;

  private initZoom = { min: 1, max: 10, step: 1, value: 1 };
  zoom = { ...this.initZoom };
  dragPosition = { x: 0, y: 0 };
  private offset = { ...this.dragPosition };
  public dragAreaStyle = {
    marginLeft: 0,
    marginTop: 0,
    width: 0,
    height: 0,
    blackBoardWidth: 0
  };

  // ボタンのimage
  private defaultImage = S0301Assets.DEFAULTIMAGE;
  private cameraImage = S0301Assets.CAMERAIMAGE;

  public photoPath: string | SafeResourceUrl = this.defaultImage;
  public cameraShoot = this.cameraImage;

  // deafult 文言
  public eyeName = 'eye';

  public defaultProcess = S0301Constants.DEFAULTPROCESS;
  public defaultWork = S0301Constants.DEFAULTWORK;
  public defaultTiming = S0301Constants.DEFAULTTIMING;

  public defaultFloor = S0301Constants.DEFAULTFLOOR;
  public defaultRoom = S0301Constants.DEFAULTROOM;
  public defaultPlace = S0301Constants.DEFAULTPLACE;
  public defaultMachine = S0301Constants.DEFAULTMACHINE;

  // preview is show
  public showCamera = false;

  commonStore: CommonStore;
  cameraStore: CameraStore;
  cameraStoreSubscription: Subscription;

  keyboardShowCallback: EventListenerOrEventListenerObject;
  keyboardHideCallback: EventListenerOrEventListenerObject;

  // 写真情報
  takePhoto: TbPhoto;

  isMobile = false;

  photoDate: string;

  // ionicのselectに対して、データが変更する場合でも、onchangeイベント発生する
  // selectは手動で選択するときのみselectのイベントを発生するように制御する
  isHandSelect = false;

  nullSelectOption = null;
  isTablet: boolean;
  platformtype: string = null;

  constructor(
    public popoverController: PopoverController,
    public modalController: ModalController,
    public alertController: AlertController,
    private cameraPreview: CameraPreview,
    public headStateService: HeadStateService,
    public routeService: RoutingService,
    private cameraStoreService: CameraStoreService,
    private commonStoreService: CommonStoreService,
    private file: File,
    private photoService: DbPhotoService,
    private photoDetailStoreService: PhotoDetailStoreService,
    // private uniqueDeviceID: UniqueDeviceID,
    private device: Device,
    private photomanagestoreService: PhotomanagestoreService,
    private platform: Platform,
    private webview: WebView,
    private loggerStoreService: LoggerStoreService,
    private appLoadingStoreService: AppLoadingStoreService,
    public events: Events,
    private appStateStoreService: AppStateStoreService,
    private sanitizer: DomSanitizer,
  ) {
    if (this.platform.is('android') || this.platform.is('ios')) {
      this.isMobile = true;
    }
    if (!this.isMobile) {
      this.events.subscribe('foreground', () => {
        if (this.routeService.getPreviousUrl() === '/camera') {
          this.startCamera();
        }
      });
    }
    this.isTablet = this.appStateStoreService.appState.isWinApp;
    this.platformtype = 'camera-' + this.appStateStoreService.getPlatform();
  }

  // 監視keyboard popup 隠す
  subscribeKeyboard() {

    this.keyboardShowCallback = async () => {
      const footer = document.querySelector('#camera-footer');
      footer.setAttribute('style', 'display:none;');

    };
    window.addEventListener('keyboardWillShow', this.keyboardShowCallback);

    this.keyboardHideCallback = async () => {
      const footer = document.querySelector('#camera-footer');
      setTimeout(() => {
        footer.removeAttribute('style');
      }, 100);
    };

    window.addEventListener('keyboardWillHide', this.keyboardHideCallback);

  }

  // 監視cameraStore
  subscribeCameraStore() {

    // 初期化 carmera store
    this.cameraStore = this.cameraStoreService.cameraStore;
    this.cameraStoreSubscription = this.cameraStoreService.cameraStoreSubscription(subscribestore => {
      this.cameraStore = subscribestore;
      // 黒板見えるか
      if (!this.cameraStore.isBlackboardDisplay) {
        this.eyeName = 'assets/images/eye.svg';
        this.dragAreaStyle.width = 0;
        this.dragAreaStyle.height = 0;
      } else {
        this.eyeName = 'assets/images/eye-off.svg';
        this.dragAreaStyle.width = this.liveviewWidth;
        this.dragAreaStyle.height = this.liveviewHeight;
      }
    });
  }

  // flash　設定
  refreshFlashMode(flashMode: string) {
    if (flashMode === 'ON' && this.showCamera && this.cameraPreview) {
      this.cameraPreview.setFlashMode('on').catch(err => console.warn(err));
    } else if (flashMode === 'AUTO' && this.showCamera && this.cameraPreview) {
      this.cameraPreview.setFlashMode('auto').catch(err => console.warn(err));
    } else if (flashMode === 'OFF' && this.showCamera && this.cameraPreview) {
      this.cameraPreview.setFlashMode('off').catch(err => console.warn(err));
    }
  }

  // CAMERAPAGE Init
  ngOnInit() {

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    // 監視cameraStore
    this.subscribeCameraStore();

    // 監視keyboard popup 隠す
    this.subscribeKeyboard();

    // ios keyboard hide
    if (this.isMobile) {
      Keyboard.hideFormAccessoryBar(false);
    }

    this.setEditFunction();
    let currentWitness = null;
    if (this.cameraStore.currentWitness) {
      for (const witness of this.cameraStore.witnessList) {
        if (witness.witnessId === this.cameraStore.currentWitness.witnessId) {
          currentWitness = witness;
          break;
        }
      }
    }
    this.cameraStoreService.setCurrentWitness(currentWitness);
  }


  // CAMERAPAGE Destroy
  ngOnDestroy() {
    this.cameraStoreSubscription.unsubscribe();
    this.cameraStoreService.setCurrentPhoto(null);
    console.log('CameraPage----------------- ngOnDestroy');

    window.removeEventListener('keyboardWillShow', this.keyboardShowCallback);
    window.removeEventListener('keyboardWillHide', this.keyboardHideCallback);
    if (!this.isMobile) {
      this.events.unsubscribe('foreground');
    }
    this.clearPage();
  }

  clearPage() {
    this.photoRatio = null;
    this.liveviewLeft = null;
    this.liveviewTop = null;
    this.liveviewWidth = null;
    this.liveviewHeight = null;
    this.cameraStartSuccess = null;
    this.initZoom = null;
    this.zoom = null;
    this.dragPosition = null;
    this.offset = null;
    this.dragAreaStyle = null;
    this.defaultImage = null;
    this.cameraImage = null;
    this.photoPath = null;
    this.cameraShoot = null;
    this.eyeName = null;
    this.defaultProcess = null;
    this.defaultWork = null;
    this.defaultTiming = null;
    this.defaultFloor = null;
    this.defaultRoom = null;
    this.defaultPlace = null;
    this.defaultMachine = null;
    this.showCamera = null;
    this.commonStore = null;
    this.cameraStore = null;
    this.cameraStoreSubscription = null;
    this.keyboardShowCallback = null;
    this.keyboardHideCallback = null;
    this.takePhoto = null;
    this.isMobile = null;
    this.photoDate = null;
    this.isHandSelect = null;
    this.nullSelectOption = null;
    this.isTablet = null;
  }

  // 設備uuid
  getDeviceUUID() {

    const uuid = localStorage.getItem('terminalNo');
    if (uuid) {
      console.log('second uuid', uuid);
      this.cameraStoreService.setTerminalNo(uuid);
      return uuid;
    }

    try {
      // const uuid = await this.uniqueDeviceID.get();
      const uuidA = this.device.uuid;
      localStorage.setItem('terminalNo', uuidA);
      this.cameraStoreService.setTerminalNo(uuidA);
      console.log('first uuid', uuidA);
    } catch (error) {
      throw error;
    }

  }

  // 初期化 NoImage => camera
  initInterfaceFromNoIMage() {
    if (this.cameraStore.currentPhoto && this.commonStore.floorsList) {

      // current floorId
      const floorId = this.cameraStore.currentPhoto.floorId;
      const floors = this.commonStore.floorsList;
      let currentFloor = null;
      for (const floor of floors) {
        if (floor.floorId === floorId) {
          currentFloor = floor;
          break;
        }

      }
      this.cameraStoreService.setCurrentFloor(currentFloor);


      // current roomId
      const roomId = this.cameraStore.currentPhoto.roomId;
      const rooms = (currentFloor && currentFloor.rooms) || [];
      let currentRoom = null;
      for (const room of rooms) {
        if (room.roomId === roomId) {
          currentRoom = room;
          break;
        }

      }
      this.cameraStoreService.setCurrentRoom(currentRoom);
      this.cameraStoreService.setRoomsList(rooms);


      // current placeId
      const placeId = this.cameraStore.currentPhoto.placeId;
      const places = (currentRoom && currentRoom.places) || [];
      let currentPlace = null;
      for (const place of places) {
        if (place.placeId === placeId) {
          currentPlace = place;
          break;
        }

      }
      this.cameraStoreService.setCurrentPlace(currentPlace);
      this.cameraStoreService.setPlacesList(places);

      // current floor room place  machinelist
      let machinesList = [];
      if (this.commonStore.machineList && this.commonStore.machineList.length > 0) {

        machinesList = this.commonStore.machineList.filter((item) => {
          if (item.floorId === floorId &&
            (!roomId || item.roomId === roomId) &&
            (!placeId || item.placeId === placeId)) {
            return item;
          }
        });
      }

      // current machine
      let currentMachine = null;
      const machineId = this.cameraStore.currentPhoto.machineId;
      for (const machine of machinesList) {
        if (machine.machineId === machineId) {
          currentMachine = machine;
          break;
        }

      }
      this.cameraStoreService.setCurrentMachine(currentMachine);
      this.cameraStoreService.setDisplayMachinesList(machinesList);
      this.cameraStoreService.setIsBlackboardDisplay(this.cameraStore.isBlackboardDisplay);

      // currentprocessId
      const processList = this.commonStore.processList;
      const processId = this.cameraStore.currentPhoto.processId;
      let currentProcess = null;
      for (const process of processList) {
        if (process.processId === processId) {
          currentProcess = process;
          break;
        }
      }
      this.cameraStoreService.setCurrentProcess(currentProcess);

      // current workId
      if (currentProcess) {
        const workId = this.cameraStore.currentPhoto.workId;
        const works = currentProcess.workList;
        let currentWork = null;
        for (const work of works) {
          if (work.workId === workId) {
            currentWork = work;
            break;
          }
        }
        this.cameraStoreService.setCurrentWork(currentWork);
        this.cameraStoreService.setWorksList(works);
      }


      // current timingId
      if (this.cameraStore.timimgsList) {
        const timingId = this.cameraStore.currentPhoto.timingId;
        const timingsList = this.cameraStore.timimgsList;
        let currentTiming = null;
        for (const timing of timingsList) {
          if (timing.timingId === timingId) {
            currentTiming = timing;
            break;
          }
        }
        this.cameraStoreService.setCurrentTiming(currentTiming);
      }
    }

  }

  // 初期化 写真一覧tab => camera
  initInterfaceFromPhotolist() {
    if (this.commonStore.floorsList && this.commonStore.floorsList.length > 0) {

      // その他画面に遷移する以前の状態を復元する。初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
      let currentFloor = this.commonStore.floorsList[0];
      if (this.cameraStore.currentFloor) {
        for (const floor of this.commonStore.floorsList) {
          if (floor.floorId === this.cameraStore.currentFloor.floorId) {
            currentFloor = floor;
            break;
          }
        }
      }
      this.cameraStoreService.setCurrentFloor(currentFloor);

      // その他画面に遷移する以前の状態を復元する。初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
      let currentRoom = null;

      if (this.cameraStore.currentRoom && currentFloor.rooms) {
        for (const room of currentFloor.rooms) {
          if (room.roomId === this.cameraStore.currentRoom.roomId) {
            currentRoom = room;
            break;
          }
        }
      }
      this.cameraStoreService.setCurrentRoom(currentRoom);

      // その他画面に遷移する以前の状態を復元する。初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
      let currentPlace = null;

      if (this.cameraStore.currentPlace && currentRoom && currentRoom.places) {
        for (const place of currentRoom.places) {
          if (place.placeId === this.cameraStore.currentPlace.placeId) {
            currentPlace = place;
            break;
          }
        }
      }
      this.cameraStoreService.setCurrentPlace(currentPlace);
      this.cameraStoreService.setRoomsList(currentFloor.rooms);
      this.cameraStoreService.setPlacesList(currentRoom && currentRoom.places);

      const currentRoomId = currentRoom && currentRoom.roomId;
      const currentPlaceId = currentPlace && currentPlace.placeId;
      // current floor room place的 machinelist
      let machinesList = [];
      if (this.commonStore.machineList && this.commonStore.machineList.length > 0 && this.cameraStore.currentFloor) {
        machinesList = this.commonStore.machineList.filter((item) => {
          return item.floorId === currentFloor.floorId &&
            (!currentRoomId || item.roomId === currentRoomId) &&
            (!currentPlaceId || item.placeId === currentPlaceId);
        });

      }

      // その他画面に遷移する以前の状態を復元する。初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
      let currentMachine = null;

      if (this.cameraStore.currentMachine) {
        for (const machine of machinesList) {
          if (machine.machineId === this.cameraStore.currentMachine.machineId) {
            currentMachine = machine;
            break;
          }
        }
      }
      this.cameraStoreService.setCurrentMachine(currentMachine);
      this.cameraStoreService.setDisplayMachinesList(machinesList);
      this.cameraStoreService.setIsBlackboardDisplay(this.cameraStore.isBlackboardDisplay);
      this.initProcessWorkTiming();
    }

  }

  // 初期化 図面表示画面 => camera
  initInterfaceFromDrawing() {

    if (this.cameraStore.photoSimpleGrid) {

      // current floorId
      const floorId = this.cameraStore.photoSimpleGrid.floorId;
      const floors = this.commonStore.floorsList;
      let currentFloor = null;
      for (const floor of floors) {
        if (floor.floorId === floorId) {
          currentFloor = floor;
          break;
        }

      }
      this.cameraStoreService.setCurrentFloor(currentFloor);


      // current roomId
      const roomId = this.cameraStore.photoSimpleGrid.roomId;
      const rooms = (currentFloor && currentFloor.rooms) || [];
      let currentRoom = null;
      for (const room of rooms) {
        if (room.roomId === roomId) {
          currentRoom = room;
          break;
        }

      }
      this.cameraStoreService.setCurrentRoom(currentRoom);
      this.cameraStoreService.setRoomsList(rooms);


      // current placeId
      const placeId = this.cameraStore.photoSimpleGrid.placeId;
      const places = (currentRoom && currentRoom.places) || [];
      let currentPlace = null;
      for (const place of places) {
        if (place.placeId === placeId) {
          currentPlace = place;
          break;
        }

      }
      this.cameraStoreService.setCurrentPlace(currentPlace);
      this.cameraStoreService.setPlacesList(places);

      // current floor room place  machinelist
      let machinesList = [];
      if (this.commonStore.machineList && this.commonStore.machineList.length > 0) {
        machinesList = this.commonStore.machineList.filter((item) => {
          if (item.floorId === floorId &&
            (!roomId || item.roomId === roomId) &&
            (!placeId || item.placeId === placeId)) {
            return item;
          }
        });
      }

      // current machine
      let currentMachine = null;
      const machineId = this.cameraStore.photoSimpleGrid.machineId;
      for (const machine of machinesList) {
        if (machine.machineId === machineId) {
          currentMachine = machine;
          break;
        }

      }
      this.cameraStoreService.setCurrentMachine(currentMachine);
      this.cameraStoreService.setDisplayMachinesList(machinesList);
      this.cameraStoreService.setIsBlackboardDisplay(this.cameraStore.isBlackboardDisplay);
      this.initProcessWorkTiming();
    }
  }

  initProcessWorkTiming() {
    // 初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
    let currentProcess = this.commonStore.processList
      && this.commonStore.processList.length > 0 && this.commonStore.processList[0] || null;

    if (this.cameraStore.currentProcess && this.commonStore.processList) {
      for (const process of this.commonStore.processList) {
        if (process.processId === this.cameraStore.currentProcess.processId) {
          currentProcess = process;
          break;
        }
      }
    }
    this.cameraStoreService.setCurrentProcess(currentProcess);

    // 初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
    const workList = currentProcess && currentProcess.workList && currentProcess.workList || null;

    let currentWork = workList && workList[0] || null;
    if (this.cameraStore.currentWork && workList) {
      for (const work of workList) {
        if (work.workId === this.cameraStore.currentWork.workId) {
          currentWork = work;
          break;
        }
      }
    }
    this.cameraStoreService.setWorksList(workList);
    this.cameraStoreService.setCurrentWork(currentWork);

    // 初期値：事前登録したフロア名リストの先頭の値(データが存在しない場合「---」)
    let currentTimimg = this.cameraStore.timimgsList
      && this.cameraStore.timimgsList.length > 0 && this.cameraStore.timimgsList[0] || null;

    if (this.cameraStore.currentTiming && this.cameraStore.timimgsList) {
      for (const timing of this.cameraStore.timimgsList) {
        if (timing.timingId === this.cameraStore.currentTiming.timingId) {
          currentTimimg = timing;
          break;
        }
      }
    }
    this.cameraStoreService.setCurrentTiming(currentTimimg);
  }

  // 初期化 camreapage interface
  initInterface() {
    // const isFirstTimeIn = this.cameraStore.currentInitInterfaceType === null;
    if (this.cameraStore.fromInitInterfaceType !== null) {
      switch (this.cameraStore.fromInitInterfaceType) {
        case InitInterfaceType.FromDrawingNoIMage:
          this.fromNoIMageInitBikou();
          this.initInterfaceFromNoIMage();
          break;
        case InitInterfaceType.FromPhotoListNoIMage:
          this.fromNoIMageInitBikou();
          this.initInterfaceFromNoIMage();
          break;
        case InitInterfaceType.FromPhotoList:
          this.initInterfaceFromPhotolist();
          break;
        case InitInterfaceType.FromDrawing:
          this.fromDrawingInitBikou();
          this.initInterfaceFromDrawing();
          break;
        default:
          this.initInterfaceFromPhotolist();
          break;
      }
    } else {
      this.initInterfaceFromPhotolist();
    }
    this.cameraStoreService.setCurrentInitInterfaceType(this.cameraStore.fromInitInterfaceType);
    this.cameraStoreService.setFromInitInterfaceType(null);
    this.cameraStoreService.setPhotoTime(moment(new Date()).format('YYYYMMDDHHmmssSSS'));
  }

  private fromNoIMageInitBikou() {
    if ((this.cameraStore.currentFloor && this.cameraStore.currentFloor.floorId !== this.cameraStore.currentPhoto.floorId)
      || (this.cameraStore.currentRoom && this.cameraStore.currentRoom.roomId !== this.cameraStore.currentPhoto.roomId)
      || (this.cameraStore.currentPlace && this.cameraStore.currentPlace.placeId !== this.cameraStore.currentPhoto.placeId)
      || (this.cameraStore.currentProcess && this.cameraStore.currentProcess.processId !== this.cameraStore.currentPhoto.processId)
      || (this.cameraStore.currentWork && this.cameraStore.currentWork.workId !== this.cameraStore.currentPhoto.workId)
      || (this.cameraStore.currentTiming && this.cameraStore.currentTiming.timingId !== this.cameraStore.currentPhoto.timingId)) {
      this.cameraStoreService.setFree('');
      const currentWitness = null;
      // this.cameraStoreService.setCurrentWitness(currentWitness);
    }
  }

  private fromDrawingInitBikou() {
    if (this.cameraStore.photoSimpleGrid) {
      if ((this.cameraStore.currentFloor && this.cameraStore.currentFloor.floorId !== this.cameraStore.photoSimpleGrid.floorId)
        || (this.cameraStore.currentRoom && this.cameraStore.currentRoom.roomId !== this.cameraStore.photoSimpleGrid.roomId)
        || (this.cameraStore.currentPlace && this.cameraStore.currentPlace.placeId !== this.cameraStore.photoSimpleGrid.placeId)) {
        this.cameraStoreService.setFree('');
        const currentWitness = null;
        // this.cameraStoreService.setCurrentWitness(currentWitness);
      }
    }
  }

  // camera liveview preview の大きさと位置
  calcCameraLiveView() {

    // 計算liveview top
    const appheaderHeight = document.querySelector('#app-header').clientHeight;
    const gridheaderHeight = document.querySelector('#grid-header').clientHeight;
    const headerHeight = appheaderHeight + gridheaderHeight;
    this.liveviewTop = headerHeight;

    // 計算liveview width 和 height
    const contentWidth = document.querySelector('#preview-content').clientWidth;
    this.liveviewWidth = contentWidth;
    const contentHeight = document.querySelector('#camera-content').clientHeight - gridheaderHeight;
    this.liveviewHeight = contentHeight;

    // 計算liveview ratio
    const liveviewRatio = this.liveviewWidth / this.liveviewHeight;

    // 計算黒板dragarea的偏移
    if (liveviewRatio > this.photoRatio) {
      this.liveviewWidth = this.liveviewHeight * this.photoRatio;
      this.dragAreaStyle.marginLeft = (contentWidth - this.liveviewWidth) / 2;
    } else if (liveviewRatio < this.photoRatio) {
      this.liveviewHeight = this.liveviewWidth / this.photoRatio;
      const marginTop = (contentHeight - this.liveviewHeight) / 2;
      this.liveviewTop = this.liveviewTop + marginTop;
      this.dragAreaStyle.marginTop = marginTop;
    }
    this.liveviewLeft = (window.screen.width - this.liveviewWidth) / 2;

    // 計算黒板dragareaの大きさ
    this.dragAreaStyle.width = this.liveviewWidth;
    this.dragAreaStyle.height = this.liveviewHeight;
    this.dragAreaStyle.blackBoardWidth = this.liveviewWidth / 3 > 150 ? this.liveviewWidth / 3 : 150;
  }

  ionViewWillEnter() {
    this.takePhoto = this.cameraStore.thumbnail;
    this.photoPath = this.takePhoto && this.sanitizer.bypassSecurityTrustResourceUrl(
      this.webview.convertFileSrc(this.file.dataDirectory + this.takePhoto.photoPathLocal)) || this.defaultImage;
  }

  // current view
  async ionViewDidEnter() {

    console.log('CameraPage---------------ionViewDidEnter');

    // 初期化 camreapage
    this.initInterface();

    // camera liveview  の大きさと位置
    this.calcCameraLiveView();
    this.getDeviceUUID();

    // this.cameraPreview.stopCamera();

    // start preview
    await this.startCamera();

  }

  // current view leave
  async ionViewWillLeave() {
    // mobileの場合
    this.cameraStartSuccess = false;
    try {
      await this.cameraPreview.stopCamera();
    } catch (error) {
      console.error('stopCamera Error: ', error);
    }
    // this.photoPath = this.defaultImage;
    this.zoom = { ...this.initZoom };
    this.dragPosition = { ...this.dragPosition };
    this.offset = { ...this.dragPosition };
    this.showCamera = false;
  }

  // 編集方法
  private setEditFunction() {

    const editFunction = async () => {
      const modal = await this.modalController.create({
        component: CameraModalEditComponent,
        componentProps: {},
        cssClass: 'camera-Modal'
      });
      await modal.present();
    };

    this.headStateService.setEditFunction(editFunction);

  }

  // 機種名
  refreshSelectMachine(currentFloor: Floor, currentRoom: Room, currentPlace: Place) {
    let machinesFilter = [];
    if (this.commonStore.machineList && this.commonStore.machineList.length > 0 && currentFloor) {
      machinesFilter = this.commonStore.machineList.filter((item: Machine) => {
        if (item.floorId === currentFloor.floorId) {
          if (!currentRoom || currentRoom.roomId === item.roomId) {
            if (!currentPlace || currentPlace.placeId === item.placeId) {
              return item;
            }
          }
        }
      });
    }
    this.cameraStoreService.setDisplayMachinesList(machinesFilter);
  }

  // flashMode
  async flashModeChange(ev: Event) {
    const flashModeDatas = [
      { name: 'AUTO', icon: 'flash_auto.svg' },
      { name: 'ON', icon: 'flash_on.svg' },
      { name: 'OFF', icon: 'flash_off.svg' }
    ];
    const popover = await this.popoverController.create({
      component: CameraPopoverComponent,
      componentProps: { datas: flashModeDatas, type: CameraActionTypes.SETFLASHMODE },
      event: ev,
      translucent: true,
      showBackdrop: true,
      cssClass: 'flashModepop'
    });
    await popover.present();
    const { data } = await popover.onDidDismiss();
    if (data) {
      this.refreshFlashMode(data.name);
    }
    return;
  }

  // 黒板見えるか
  eyeModeChange() {
    this.cameraStoreService.setPhotoTime(moment(new Date()).format('YYYYMMDDHHmmssSSS'));
    this.cameraStoreService.setIsBlackboardDisplay(!this.cameraStore.isBlackboardDisplay);
  }

  // 機番撮影
  qrScanOpen() {

    const machineId = this.cameraStore.currentMachine && this.cameraStore.currentMachine.machineId.toString();
    const machineModel = this.cameraStore.currentMachine && this.cameraStore.currentMachine.machineModel;
    const unitMark = this.cameraStore.currentMachine && this.cameraStore.currentMachine.unitMark;
    const floorName = this.cameraStore.currentMachine && this.cameraStore.currentMachine.floorName;
    const roomName = this.cameraStore.currentMachine && this.cameraStore.currentMachine.roomName;
    const placeName = this.cameraStore.currentMachine && this.cameraStore.currentMachine.placeName;
    const buildingName = this.commonStore && this.commonStore.buildingName;
    const projectName = this.commonStore && this.commonStore.projectName;

    sessionStorage.setItem('buildingName', buildingName);
    sessionStorage.setItem('projectName', projectName);
    sessionStorage.setItem('machineId', machineId);
    sessionStorage.setItem('machineModel', machineModel);
    sessionStorage.setItem('unitMark', unitMark);

    sessionStorage.setItem('floorName', floorName);
    sessionStorage.setItem('roomName', roomName);
    sessionStorage.setItem('placeName', placeName);

    this.headStateService.setBackRoute('/camera');
    this.routeService.goRouteLink('/barcode-scan');

  }

  // Zoom調整
  changeZoom(): void {
    if (this.showCamera) {
      this.cameraPreview.setZoom(this.zoom.value).catch(err => console.warn(err));
    }
  }

  dragEnd(event: CdkDragEnd) {
    this.offset = { ...event.source.getFreeDragPosition() };
  }

  // start preview
  private async startCamera() {
    const cameraPreviewOpts: CameraPreviewOptions = {
      x: this.liveviewLeft,
      y: this.liveviewTop,
      width: this.liveviewWidth,
      height: this.liveviewHeight,
      camera: 'rear',
      tapPhoto: false,
      toBack: true,
    };

    try {

      await this.cameraPreview.startCamera(cameraPreviewOpts);
      this.cameraStartSuccess = true;
      this.showCamera = true;
      let maxZoom = await this.cameraPreview.getMaxZoom().catch(err => console.warn(err));
      if (!maxZoom) {
        maxZoom = 1;
      }
      let maxZoomLimit: number;
      if (this.platform.is('ios')) {
        maxZoomLimit = 10;
      } else if (this.platform.is('android')) {
        maxZoomLimit = 99;
      } else {
        maxZoomLimit = 40;
      }
      this.zoom.max = maxZoom > maxZoomLimit ? maxZoomLimit : maxZoom;
      this.zoom.step = parseInt(String(Number(this.zoom.max) / 10), 10);
      this.refreshFlashMode(this.cameraStore.flashMode);

    } catch (error) {
      console.error('startCamera Error: ', error);

    }

  }

  // 撮影button押下
  async camera() {

    if (!this.cameraStartSuccess) {
      return;
    }


    if (!this.cameraStore.currentProcess || !this.cameraStore.currentWork) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00001,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }


    this.appLoadingStoreService.isloading(true);
    this.photoDate = moment(new Date()).format('YYYYMMDDHHmmssSSS');
    await this.cameraStoreService.setPhotoTime(this.photoDate);

    const self = this;
    try {
      const imageData = await this.cameraPreview.takePicture({
        width: this.liveviewWidth,
        height: this.liveviewHeight,
        quality: 85
      } as CameraPreviewPictureOptions);
      const byteString = atob(imageData);
      const ua = new Uint8Array(byteString.length);
      for (let i = 0; i < byteString.length; i++) {
        ua[i] = byteString.charCodeAt(i);
      }
      const blob = new Blob([ua.buffer], { type: 'image/jpeg' });
      await self.composeWithCut(blob);
      this.appLoadingStoreService.isloading(false);
    } catch (error) {
      this.photoPath = this.defaultImage;
      this.appLoadingStoreService.isloading(false);
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00128.replace('{1}', AlertWindowConstants.ADMINEMAIL),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alert.present();
    }
  }

  private getPixelRatio(context) {
    const backingStore = context.backingStorePixelRatio ||
      context.webkitBackingStorePixelRatio ||
      context.mozBackingStorePixelRatio ||
      context.msBackingStorePixelRatio ||
      context.oBackingStorePixelRatio ||
      context.backingStorePixelRatio || 1;
    return (window.devicePixelRatio || 1) / backingStore;
  }

  // 黒板と写真結合
  private async composeWithCut(photoDataBlob) {
    const c: HTMLCanvasElement = document.getElementById('photoCanvas') as HTMLCanvasElement;
    const ctx = c.getContext('2d');

    // canvasをクリア
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    // High DPIの対応
    const pixelRatio = this.getPixelRatio(ctx);
    c.style.width = this.liveviewWidth + 'px';
    c.style.height = this.liveviewHeight + 'px';
    c.width = this.liveviewWidth * pixelRatio;
    c.height = this.liveviewHeight * pixelRatio;

    // 写真がbase64からimageへ変換してcanvasで描画
    const objectUrl = window.URL.createObjectURL(photoDataBlob);
    const image = await this.loadImage(objectUrl);
    let iWidth = image.width;
    let iheight = image.height;
    let iOffX = 0;
    let iOffY = 0;
    const ratio = image.width / image.height;
    if (ratio > this.photoRatio) {
      iWidth = image.height * this.photoRatio;
      iOffX = (image.width - iWidth) / 2;
    } else {
      iheight = image.width / this.photoRatio;
      iOffY = (image.height - iheight) / 2;
    }
    ctx.drawImage(image, iOffX, iOffY, iWidth, iheight, 0, 0, this.liveviewWidth * pixelRatio, this.liveviewHeight * pixelRatio);

    window.URL.revokeObjectURL(objectUrl);
    // 黒板を描画
    if (this.cameraStore.isBlackboardDisplay && this.isHaveDisplayItem()) {
      const blackBoardEle = document.getElementById('blackBoard');
      const blackBoardHtml = '<style>body {margin: 0px;}</style>' + blackBoardEle.innerHTML;
      const blackBoardImg = await rasterizeHTML.drawHTML(blackBoardHtml, null,
        { width: blackBoardEle.offsetWidth, height: blackBoardEle.offsetHeight });
      ctx.drawImage(blackBoardImg.image, this.offset.x * pixelRatio, this.offset.y * pixelRatio,
        blackBoardEle.offsetWidth * pixelRatio, blackBoardEle.offsetHeight * pixelRatio);
    }

    const newImageData = c.toDataURL('image/jpeg', 0.85);

    // canvasをクリア
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, c.width, c.height);

    // blob変換，ファイルに保存
    await this.base64ToBlob(newImageData.split(',')[1]);
  }

  private isHaveDisplayItem() {

    const dis = this.cameraStore.blackboardDisplay;
    return dis.isProjectDisplay === '1'
      || dis.isFloorDisplay === '1'
      || dis.isRoomDisplay === '1'
      || dis.isPlaceDisplay === '1'
      || dis.isLineDisplay === '1'
      || dis.isUnitDisplay === '1'
      || dis.isProcessDisplay === '1'
      || dis.isWorkDisplay === '1'
      || dis.isFreeDisplay === '1'
      || dis.isWitnessDisplay === '1'
      || dis.isConstructionCompanyDisplay === '1'
      || dis.isDateDisplay === '1'
      || dis.isTimeDisplay === '1';
  }

  // base64 写真変換
  private loadImage(src: string) {
    return new Promise<HTMLImageElement>((resolve, reject) => {
      const img = new Image();
      img.src = src;
      img.onload = () => resolve(img);
      img.onerror = (e) => reject(e);
    });
  }

  // blob変換，ファイルに保存
  private async base64ToBlob(imageData: string) {
    const byteString = atob(imageData);
    const ua = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ua[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([ua.buffer], { type: 'image/jpeg' });
    // console.warn('blob size: ', blob.size / 1000);

    try {
      const photoName = this.photoDate + '.jpg';
      const photoPath = 'Download/' + photoName;
      await this.cameraStoreService.setPhotoPathLocal(photoPath);

      const de = await this.file.createDir(this.file.dataDirectory, 'Download', true);
      const fe = await this.file.writeFile(de.toURL(), photoName, blob, { replace: true });

      const filePath = this.file.dataDirectory + photoPath;
      this.photoPath = this.sanitizer.bypassSecurityTrustResourceUrl(this.webview.convertFileSrc(filePath));
      this.loggerStoreService.addLogger('camerapage', 'this.filepath' + filePath);
      this.loggerStoreService.addLogger('camerapage', 'this.photopath' + this.photoPath);
      // sqlite保存
      this.save2Sqlite();
    } catch (error) {
      throw error;
    }
  }

  // 写真詳細遷移
  async goPhotoDetail() {

    if (this.photoPath !== S0301Assets.DEFAULTIMAGE && this.takePhoto) {

      const buildingId = this.commonStore.buildingId;
      const projectId = this.commonStore.projectId;
      const bigProcessId = this.commonStore.bigProcessId;

      this.photoDetailStoreService.setDetailPhoto(this.takePhoto);
      const photos = await this.photoService.selectphotos(buildingId, projectId, bigProcessId);
      // 当日写真filter
      const photoDetailList = photos.filter((photo) => {
        if (this.takePhoto.photoDate.toString().substring(0, 8) === photo.photoDate.toString().substring(0, 8)) {
          return photo;
        }
      });
      this.photoDetailStoreService.setDetailPhotoList(photoDetailList.sort(this.sortByDate('photoDate')));
      this.headStateService.setBackRoute('/camera');
      this.routeService.goRouteLink('/photo-detail');
    }

  }

  // 保存sqlite
  async save2Sqlite() {

    console.log('-------------this.takePhoto--------------');

    const photoItem: TbPhoto = {};
    photoItem.constructionCompany = this.cameraStore.currentWitness && this.cameraStore.currentWitness.constructionCompanyName;
    photoItem.constructionCompanyId = this.cameraStore.currentWitness && this.cameraStore.currentWitness.constructionCompanyId;
    photoItem.floorId = this.cameraStore.currentFloor && this.cameraStore.currentFloor.floorId;
    photoItem.floorName = this.cameraStore.currentFloor && this.cameraStore.currentFloor.floorName;
    photoItem.floorOrder = this.cameraStore.currentFloor && this.cameraStore.currentFloor.displayOrder;
    photoItem.free = this.cameraStore.free;
    photoItem.isBlackBoardDisplay = this.cameraStore.isBlackboardDisplay ? PhotoConf.AUTO_UPLOAD_ON : PhotoConf.AUTO_UPLOAD_OFF;
    photoItem.isOutputPhotoBook = 0;
    photoItem.machineId = this.cameraStore.currentMachine && this.cameraStore.currentMachine.machineId;
    photoItem.modeltype = this.cameraStore.currentMachine && this.cameraStore.currentMachine.machineModel;
    photoItem.photoDate = this.cameraStore.photoDate;
    photoItem.photoImage = null;
    photoItem.photoTerminalNo = this.cameraStore.terminalNo;
    photoItem.photoUser = this.commonStore.userName;
    photoItem.placeId = this.cameraStore.currentPlace && this.cameraStore.currentPlace.placeId;
    photoItem.placeName = this.cameraStore.currentPlace && this.cameraStore.currentPlace.placeName;
    photoItem.placeOrder = 1;
    photoItem.processId = this.cameraStore.currentProcess && this.cameraStore.currentProcess.processId;
    photoItem.processName = this.cameraStore.currentProcess && this.cameraStore.currentProcess.processName;
    photoItem.processOrder = this.cameraStore.currentProcess && this.cameraStore.currentProcess.displayOrder;
    photoItem.roomId = this.cameraStore.currentRoom && this.cameraStore.currentRoom.roomId;
    photoItem.roomName = this.cameraStore.currentRoom && this.cameraStore.currentRoom.roomName;
    photoItem.roomOrder = 1;
    photoItem.timingAbbreviation = this.cameraStore.currentTiming && this.cameraStore.currentTiming.timingAbbreviation;
    photoItem.timingId = this.cameraStore.currentTiming && this.cameraStore.currentTiming.timingId;
    photoItem.timingName = this.cameraStore.currentTiming && this.cameraStore.currentTiming.timingName;
    photoItem.timingOrder = this.cameraStore.currentTiming && Number(this.cameraStore.currentTiming.displayOrder);
    photoItem.unitMark = this.cameraStore.currentMachine && this.cameraStore.currentMachine.unitMark;
    photoItem.witness = this.cameraStore.currentWitness && this.cameraStore.currentWitness.witnessName;
    photoItem.witnessId = this.cameraStore.currentWitness && this.cameraStore.currentWitness.witnessId;
    photoItem.workId = this.cameraStore.currentWork && Number(this.cameraStore.currentWork.workId);
    photoItem.workName = this.cameraStore.currentWork && this.cameraStore.currentWork.workName;
    photoItem.workOrder = this.cameraStore.currentWork && this.cameraStore.currentWork.displayOrder;
    photoItem.photoPathLocal = this.cameraStore.photoPathLocal;
    photoItem.localPhotoFlag = PhotoConf.LOCAL_POTO;
    photoItem.retryCount = PhotoConf.RETRY_DEFAULT;
    photoItem.photoPath = null;
    photoItem.uploadFlag = this.commonStore.autoUpload ? PhotoConf.AUTO_UPLOAD_ON : PhotoConf.AUTO_UPLOAD_OFF;
    this.takePhoto = photoItem;

    this.cameraStoreService.setThumbnail(photoItem);

    console.log('-------------this.takePhoto--------------', this.takePhoto);
    this.loggerStoreService.addLogger('camerapage', this.takePhoto);
    this.loggerStoreService.addLogger('camerapage', 'start save singlephoto');

    const buildingId = this.commonStore.buildingId;
    const projectId = this.commonStore.projectId;
    const bigProcessId = this.commonStore.bigProcessId;

    await this.photoService.saveSinglePhoto(buildingId, projectId, bigProcessId, this.takePhoto);
    const photoAll = await this.photoService.selectphotos(buildingId, projectId, bigProcessId);
    this.photomanagestoreService.setPhotos(photoAll);
  }

  async onPress(ev: Event, message) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const popover = await this.popoverController.create({
      component: TextSinglePopoverComponent,
      componentProps: { message },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  sortByDate(pro) {
    return (a, b) => {
      const value1 = a[pro];
      const value2 = b[pro];
      return value2 - value1;
    };
  }
  // フロア選択
  selectFloor(event) {
    // 手動でselect場合のみ実行するように制御
    if (!this.isHandSelect) {
      return;
    }
    const data = event.detail.value;
    this.cameraStoreService.setCurrentFloor(data);
    this.cameraStoreService.setRoomsList(data.rooms);
    this.cameraStoreService.setCurrentRoom(null);
    this.cameraStoreService.setPlacesList(null);
    this.cameraStoreService.setCurrentPlace(null);
    this.cameraStoreService.setCurrentMachine(null);
    // this.cameraStoreService.setCurrentWitness(null);
    this.cameraStoreService.setFree('');
    this.refreshSelectMachine(data, null, null);


  }

  getCurrentMachine(floorId: number, currentRoomId: number, currentPlaceId: number): Machine | null {
    let machinesList = [];
    if (this.commonStore.machineList && this.commonStore.machineList.length > 0) {
      machinesList = this.commonStore.machineList.filter((item) => {
        return item.floorId === floorId &&
          (!currentRoomId || item.roomId === currentRoomId) &&
          (!currentPlaceId || item.placeId === currentPlaceId);
      });
    }
    return machinesList && machinesList.length > 0 ? machinesList[0] : null;
  }

  selectRoom(event) {
    if (!this.isHandSelect) {
      return;
    }
    const data = event.detail.value;
    this.cameraStoreService.setCurrentRoom(data || null);
    this.cameraStoreService.setPlacesList((data && data.places) || []);
    this.cameraStoreService.setCurrentPlace(null);
    this.cameraStoreService.setCurrentMachine(null);
    // this.cameraStoreService.setCurrentWitness(null);
    this.cameraStoreService.setFree('');
    this.refreshSelectMachine(this.cameraStoreService.cameraStore.currentFloor, data, null);


  }

  selectPlace(event) {
    if (!this.isHandSelect) {
      return;
    }
    const data = event.detail.value;
    this.cameraStoreService.setCurrentPlace(data);
    this.cameraStoreService.setCurrentMachine(null);
    // this.cameraStoreService.setCurrentWitness(null);
    this.cameraStoreService.setFree('');
    this.refreshSelectMachine(this.cameraStoreService.cameraStore.currentFloor, this.cameraStoreService.cameraStore.currentRoom, data);


  }

  selectBlur() {
    this.isHandSelect = true;
  }

  selectFocus() {
    this.isHandSelect = false;
  }

  selectMachine(event) {
    const data = event.detail.value;
    this.cameraStoreService.setCurrentMachine(data || null);
  }

  selectWork(event) {
    if (!this.isHandSelect) {
      return;
    }
    const data = event.detail.value;
    this.cameraStoreService.setCurrentWork(data);
    // this.cameraStoreService.setCurrentWitness(null);
    this.cameraStoreService.setFree('');
  }

  selectTiming(event) {
    if (!this.isHandSelect) {
      return;
    }
    const data = event.detail.value;
    this.cameraStoreService.setCurrentTiming(data);
    // this.cameraStoreService.setCurrentWitness(null);
    this.cameraStoreService.setFree('');
  }

  selectProcess(event) {
    if (!this.isHandSelect) {
      return;
    }
    const data = event.detail.value;
    const currentWork = data && data.workList && data.workList.length > 0 ? data.workList[0] : null;
    this.cameraStoreService.setCurrentProcess(data);
    this.cameraStoreService.setWorksList(data.workList);
    this.cameraStoreService.setCurrentWork(currentWork);
    // this.cameraStoreService.setCurrentWitness(null);
    this.cameraStoreService.setFree('');
  }

  spaceString(title: string) {
    if (title) {
      return decodeURI(title.replace(/ /g, '%C2%A0'));
    } else {
      return '';
    }
  }
}
