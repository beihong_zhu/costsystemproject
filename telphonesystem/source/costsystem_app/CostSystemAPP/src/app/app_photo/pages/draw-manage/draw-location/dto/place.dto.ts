export declare interface PlaceDto {
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
}
