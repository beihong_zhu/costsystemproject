import { PhotoList0501 } from '@service/dto/api-05-01-out-dto';


export class LookType {
  id: any;
  value: string;
}

export interface LocalPhoto extends PhotoList0501 {

  buildingId?: number;
  projectId?: number;
  bigProcessId?: string;
  photoPathLocal?: string;
  isSelect?: boolean;

  // 1 local 0 server
  localPhotoFlag?: number;

  uploadFlag?: number;

  retryCount?: number;
}

export interface SelectTitle {
  title?: string;

  processId?: number;
  id?: number;


}
