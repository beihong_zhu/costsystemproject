import { Component, OnInit } from '@angular/core';
import { AlertController, NavParams } from '@ionic/angular';
import { ProcessDto } from '../dto';
import { S0101Constants, AlertWindowConstants } from '@constant/constant.photo';
import { AppStateStoreService } from '@store';
import { API1406Service } from '@service/api';
import { API1406InDto, API1406OutDto } from '@service/dto';
import { TextCheck } from '../../../../../app_photo/util/text-check';
import { MessageConstants } from '@constant/message-constants';
import { ResultType } from '../../../../../app_common/constant/common';
import { Building1406 } from '../../../../service/dto/api-14-06-out-dto';

@Component({
  selector: 'app-process-add',
  templateUrl: './building-search.page.html',
  styleUrls: ['./building-search.page.scss'],
})
/**
 * 工程名追加ダイアログ
 */
export class BuildingSearchPage implements OnInit {
  /**
   * 画面DTO
   */
  public gamenDto: ProcessDto = {};
  public searchValue: string;
  /**
   * 工程件数
   */
  public processCount: number;
  searchBuildingList: Building1406[];
  // 物件一覧物件IDレベル
  buildingIdTitle: string = S0101Constants.BUILDINGID;
  // 物件一覧物件名レベル
  buildingNameTitle: string = S0101Constants.BUILDINGNAME;
  // 物件一覧物件住所レベル
  buildingAddressTitle: string = S0101Constants.BUILDINGADDRESS;

  public isPc = false;
  constructor(
    public alertController: AlertController,
    public navParams: NavParams,
    private appStateService: AppStateStoreService,
    private api1406Service: API1406Service
  ) {
  }

  onNoClick(): void {
    this.navParams.data.modal.dismiss(null);
  }

  async onSearchClick() {
    // this.searchBuildingList = [];
    // let allBuildingList = this.navParams.data.value;
    // if(allBuildingList){
    //   if(this.searchValue){
    //     this.searchBuildingList = allBuildingList.filter((item, index) => {
    //       return item.name.indexOf(this.searchValue) > -1
    //     })
    //   }else{
    //     this.searchBuildingList =allBuildingList
    //   }
    // }

    // 入力項目が空白チェック
    if (this.searchValue.trim() === '' || this.searchValue.trim() === null) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00001,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
    }

    // 特殊な文字列をチェック
    if (!TextCheck.textCheck(this.searchValue.trim())) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00008,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
      return;
    }

    // 検索処理
    if (this.searchValue != null && this.searchValue.trim() !== '') {
      // 物件情報取得APIから、対象物件情報を取得
      this.api1406Service.postExec(this.ge1406InDto(), (outDto: API1406OutDto) => {
        // レスポンス
        this.api1406success(outDto);
      },
        () => { });
    }
  }

  ge1406InDto(): API1406InDto {
    const inDto: API1406InDto = {};
    inDto.buildingName = this.searchValue.trim();
    return inDto;
  }

  async api1406success(outDto: API1406OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      this.searchBuildingList = outDto.buildings;
    } else if (outDto.resultCode === ResultType.BUSINESSERROR) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00113.replace('{1}', AlertWindowConstants.ADMINEMAIL),
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  ngOnInit() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.searchValue = '';
  }

  clickItem(building) {
    this.navParams.data.modal.dismiss(building);
  }

  isDisabled() {
    return !this.searchValue || this.searchValue.trim() === '';
  }
}
