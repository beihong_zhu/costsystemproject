import { NgModule } from '@angular/core';
import { BigProcessPage } from './big-process.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [],
  declarations: [BigProcessPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: BigProcessPage
      }
    ])
  ]
})
export class BigProcessModule { }
