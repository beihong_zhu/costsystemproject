import { Component, Inject, OnInit, ViewChild, ElementRef, Renderer } from '@angular/core';
import { ModalController, PopoverController, Platform } from '@ionic/angular';
import { BuildingSearchPage } from './building-search/building-search.page';
import { SelectDto } from './dto';
import { CommonStoreService, AppStateStoreService, AppState, CommonStore, PreSettingStoreService, PreSettingStore } from '@store';
import { API1405Service } from '@service/api';
import { API1405InDto, API1405OutDto } from '@service/dto';
import { S1401Constants } from '@constant/constant.photo';
import { RoutingService } from '@service/app-route.service';
import { CSVUtil } from './csvChecker';
import { MessageConstants } from '@constant/message-constants';
import * as targetListJson from './targetList.json';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { Building1406 } from '../../../service/dto/api-14-06-out-dto';
const targetLists = targetListJson.default;

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.page.html',
  styleUrls: ['./upload-form.page.scss'],
})
/**
 * 事前登録画面
 */
export class UploadFormPage implements OnInit {

  public isPc = false;
  appState: AppState;
  commonStore: CommonStore;
  preSettingStore: PreSettingStore;
  // 対象物件レベル
  targetBuildingLabel: string = S1401Constants.TARGET_BUILDING;
  // 対象案件レベル
  targetProjectLabel: string = S1401Constants.TARGET_PROJECT;
  // 設定対象レベル
  settingTargetLabel: string = S1401Constants.SETTING_TARGET;
  // 入力CSVレベル
  inputCsvLabel: string = S1401Constants.INPUT_CSV;
  // 入力PDFレベル
  inputPdfLabel: string = S1401Constants.INPUT_PDF;

  // 対象物件選択リスト
  // targetBuildingList: SelectDto[];
  // 対象案件選択リスト
  targetProjectList: SelectDto[];
  // 設定対象選択リスト
  settingTargetList: SelectDto[];
  // 対象物件選択済値を格納
  targetBuildingID: string = '';
  targetBuilding: Building1406 = null;
  // 対象案件選択済値を格納
  targetProject = '';
  // 設定対象選択済値を格納
  settingTarget = '0';
  // 入力CSV値を格納
  inputCsvPath = '';
  file: File | null = null;
  readText: string = null;
  csvData: [];
  errorMsg: string = null;
  pdfFiles: File[] | null = null;
  pdfNames: string = null;
  pdfNamesCheck: string = null;
  pdfInputFlag: boolean = true;
  csvInputFlag: boolean = true;

  /**
   * 画面DTO
   */
  /**
   * コンストラクタ
   * @param modalController ダイアログコントローラ
   * @param platform Platform
   * @param api0811Service 工程・作業情報一覧API
   * @param api0812Service テンプレート工程・作業情報取得API
   * @param api0813Service 工程・作業情報一括登録API
   * @param routerService RoutingService
   */
  constructor(
    public modalController: ModalController,
    private routerService: RoutingService,
    private commonStoreService: CommonStoreService,
    private appStateService: AppStateStoreService,
    private preSettingStoreService: PreSettingStoreService,
    private api1405Service: API1405Service,
    // tslint:disable-next-line: deprecation
    private renderer: Renderer,
    public popoverController: PopoverController,
    private platform: Platform
  ) {
    this.isPc = this.appStateService.appState.isPC;
    // APPストアに保存した物件一覧を取得
    this.appState = this.appStateService.appState;

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    // AP
    // Pストアに保存した写真帳出力情報を取得
    this.preSettingStore = this.preSettingStoreService.preSettingStore;
  }

  /**
   * 初期化
   */
  ngOnInit() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.querySelectTargetList();

  }
  ionViewWillEnter() {
    this.pdfInputFlag = true;
    this.csvInputFlag = true;
    if (this.preSettingStore.message) {
    const allMsg = this.preSettingStore.message.split('|');
    const resCode = allMsg[0];
    const messageCode = allMsg[1];
    const message = allMsg[2];
    if (messageCode) {
      if (resCode === '3') {
        this.errorMsg = message;
      } else {
        this.errorMsg = MessageConstants[messageCode];
        }
      }
    }
    this.settingTarget = this.preSettingStore.settingTargetId ? this.preSettingStore.settingTargetId : '0';
    if (this.preSettingStore.confirmSuccess) {
      this.settingTarget = '0';
      this.preSettingStore.confirmSuccess = false;
    }

    this.targetBuildingID = this.preSettingStore.targetBuildingId ? this.preSettingStore.targetBuildingId : this.targetBuildingID;
  }
  // 設定対象を取得
  querySelectTargetList() {
    this.settingTargetList = new Array<SelectDto>(targetLists.settingTargets.length);
    Array.from({ length: targetLists.settingTargets.length }).map((_, i) => {
      this.settingTargetList[i] = new SelectDto();
      // 設定対象ID
      this.settingTargetList[i].code = targetLists.settingTargets[i].settingTargetId;
      // 設定対象名
      this.settingTargetList[i].name = targetLists.settingTargets[i].settingTargetName;
    });
  }

  /**
   * 1405InDtoを作成
   */
  getCreate1405InDto(): API1405InDto {
    const inDto: API1405InDto = {};
    return inDto;
  }


  /**
   * １、対象物件選択肢情報を設定
   * ２．対象案件選択肢情報を取得
   * @param outDto 1405物件DTO
   */
  async setTargetBuildingMastData(out1405Dto: API1405OutDto) {

  }

  /**
   * 検索画面を開く
   */
  async openAddProcessDialog() {
    const modal = await this.modalController.create({
      component: BuildingSearchPage,
      componentProps: {
        // value: this.targetBuildingList,
      },
      cssClass: 'buildingSearchModal',

    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // 追加ボタンを押下するか判断
    if (data != null) {
      this.targetBuildingID = data.buildingId;
      this.targetBuilding = data;
    }
  }

  /**
   * CSVファイルを選択イベント
   * @param evt ファイル選択イベントオブジェクト
   */
  onChangeInput(files: File[]) {
    this.preSettingStore.message = '';
    this.errorMsg = '';
    this.file = files[0];
    this.readText = this.file.name;
    // CSVファイル以外は処理を止める
    if (!this.file.name.toUpperCase().match('.CSV$')) {
      this.errorMsg = MessageConstants.E00043;
      this.inputCsvPath = '';
      this.readText = '';
      this.file = null;
      return;
    }
  }

  /**
   * PDFファイルを選択イベント
   * @param evt ファイル選択イベントオブジェクト
   */
  onChangeInputPdf(files: File[]) {
    this.preSettingStore.message = '';
    this.errorMsg = '';
    this.pdfFiles = files;
    const nameArr: string[] = [];
    const nameArrCheck: string[] = [];
    for (const pdfItem of this.pdfFiles) {
      const itemMatchName = pdfItem.name.toUpperCase();
      const itemName = pdfItem.name;
      // PDF選択するかどうかのチェック
      if (!itemMatchName.match('.PDF$')) {
        this.errorMsg = MessageConstants.E00071;
        this.pdfNames = '';
        this.pdfNamesCheck = '';
        return;
      }
      // 選択した図面ファイルサイズの制限チェック
      // byteをKBやMBに換算
      // 1024byte -> 1KB、1024KB -> 1MBで、8MB -> 1.024 * 1.024 * 8000000 = 8388608byte
      if (pdfItem.size > 8388608) {
        this.errorMsg = MessageConstants.E00080;
        this.pdfNames = '';
        this.pdfNamesCheck = '';
        return;
      }
      nameArr.push(itemName);
      nameArrCheck.push(itemMatchName);
    }
    this.pdfNames = nameArr.join(',');
    this.pdfNamesCheck = nameArrCheck.join(',');
  }

  /**
   * CSVファイルを読込処理
   */
  async onRead() {
    if (this.targetBuilding === null && this.settingTarget === '3') {
      this.errorMsg = MessageConstants.E00078;
      return false;
    }

    if (this.file === null) {
      this.errorMsg = MessageConstants.E00079;
      return false;
    }
    // if (this.settingTarget == '3') {
    //   if (this.pdfFiles === null) {
    //     this.errorMsg = 'PDFファイルを選択してください';
    //     return false;
    //   }
    // }
    this.pdfInputFlag = !this.pdfInputFlag;
    this.csvInputFlag = !this.csvInputFlag;

    try {
      const csvText: string = await this.fileToText(this.file);
      const res = CSVUtil.check(this.settingTarget, csvText, this.pdfNamesCheck);
      // this.file = null;
      // this.readText = '';
      // this.pdfNames = '';
      if (res.resultCode === '-1') {
        this.targetBuilding = null;
        this.inputCsvPath = '';
        this.csvData = null;
        this.file = null;
        this.errorMsg = res.errorMsg;
      } else {
        this.errorMsg = res.errorMsg;
        this.pdfNames = res.pdfNames;
        // 事前設定情報登録確認画面へ遷移
        if (this.settingTarget !== '3') {
          this.goNextPage(res.csvData);
        } else {
          this.goNextPage(res.bukenCsvData);
        }
      }
      this.targetBuilding = null;
      this.file = null;
      this.readText = '';
      this.pdfNames = '';
    } catch (err) {
      throw err;
    }
  }

  /**
   * ファイルを読込処理詳細
   * @param file 選択済みファイル
   */
  fileToText(file: Blob): Promise<string> {
    const reader = new FileReader();
    reader.readAsText(file);
    return new Promise((resolve, reject) => {
      reader.onload = () => {
        const csvData: string = reader.result as string;
        resolve(csvData);
      };
      reader.onerror = () => {
        reject(reader.error);
      };
    });
  }

  /**
   * 事前設定情報登録確認画面へ遷移
   */
  goNextPage(value): void {
    // 選択した設定対象名称を設定
    this.preSettingStore.settingTargetName = this.getSelectName(this.settingTarget, this.settingTargetList);
    if (this.settingTarget === '3') {
      // 選択した対象案件名称を設定
      this.preSettingStore.targetBuildingName = this.targetBuilding.buildingName;
      this.preSettingStore.pdfNames = this.pdfNames;
    } else {
      this.preSettingStore.targetBuildingName = '';
      this.preSettingStore.pdfNames = '';
    }
    // 選択した対象案件idを設定
    this.preSettingStore.targetBuildingId = this.targetBuildingID.toString();
    // 入力CSVファイル名を設定
    this.preSettingStore.inputCsvPath = this.file.name;
    // 入力CSVファイル設定
    this.preSettingStore.file = this.file;
    this.preSettingStore.pdfFiles = this.pdfFiles;
    this.preSettingStore.settingTargetId = this.settingTarget;

    // 入力CSV設定
    switch (this.settingTarget) {
      case '0':
        this.preSettingStore.userCsvData = value;
        this.preSettingStore.csvKbn = '01';
        break;
      case '1':
        this.preSettingStore.bukenCsvData = value;
        this.preSettingStore.csvKbn = '02';
        break;
      case '2':
        this.preSettingStore.bukenHeyaBasyoCsvData = value;
        this.preSettingStore.csvKbn = '03';
        break;
      case '3':
        this.preSettingStore.ankenTotalCsvData = value;
        this.preSettingStore.csvKbn = '04';
        break;
    }

    this.targetBuilding = null;
    this.targetProject = '';
    this.settingTarget = '0';
    this.inputCsvPath = '';
    this.csvData = null;
    this.file = null;
    this.pdfNames = null;
    this.pdfFiles = null;
    this.pdfNamesCheck = null;

    this.preSettingStoreService.setPreSetting(this.preSettingStore);
    const url = '/presetting-confirm';
    this.routerService.goRouteLink(url);
  }

  /**
   *  選択肢のコードより、コード対する名称を取得
   * @param selectedCode 画面選択肢の選択したコード
   * @param masterArray 画面選択肢のマスターデータ
   * @returns 選択肢名称
   */
  getSelectName(selectedCode: string, masterArray: Array<SelectDto>): string {
    let codeName = '';
    for (const selectItem of masterArray) {
      if (String(selectItem.code) === String(selectedCode)) {
        codeName = selectItem.name;
        break;
      }
    }
    return codeName;
  }

  settingTargetChange() {
    this.targetBuilding = null;
    this.targetBuildingID = '';
    this.file = null;
    this.readText = '';
    this.pdfNames = '';
    this.errorMsg = null;
  }

  myInputClick() {
    const event = new MouseEvent('click', { bubbles: true });


    if (this.csvInputFlag) {
      const input = document.getElementById('myInput') as HTMLInputElement;
      input.value = null;

      this.renderer.invokeElementMethod(document.getElementById('myInput'), 'dispatchEvent', [event]);
    } else {
      const input1 = document.getElementById('myInput1') as HTMLInputElement;
      input1.value = null;
      this.renderer.invokeElementMethod(document.getElementById('myInput1'), 'dispatchEvent', [event]);
    }
  }

  myPdfInputClick() {
    const event = new MouseEvent('click', { bubbles: true });
    if (this.pdfInputFlag) {
      const input = document.getElementById('myPdfInput') as HTMLInputElement;
      input.value = null;
      this.renderer.invokeElementMethod(document.getElementById('myPdfInput'), 'dispatchEvent', [event]);
    } else {
      const input1 = document.getElementById('myPdfInput1') as HTMLInputElement;
      input1.value = null;
      this.renderer.invokeElementMethod(document.getElementById('myPdfInput1'), 'dispatchEvent', [event]);
    }
  }

  async onPress(ev: Event, info) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    const messageArr1 = [info];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();
  }
}
