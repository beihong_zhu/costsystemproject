import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FundrecorderPage } from './fundrecorder.page';

describe('FundrecorderPage', () => {
  let component: FundrecorderPage;
  let fixture: ComponentFixture<FundrecorderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FundrecorderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FundrecorderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
