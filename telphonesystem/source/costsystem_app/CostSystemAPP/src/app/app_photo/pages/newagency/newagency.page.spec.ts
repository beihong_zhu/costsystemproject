import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewagencyPage } from './newagency.page';

describe('NewagencyPage', () => {
  let component: NewagencyPage;
  let fixture: ComponentFixture<NewagencyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewagencyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewagencyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
