import { NgModule } from '@angular/core';
import { PhotoDetailPage } from './photo-detail.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';


@NgModule({
  providers: [],
  declarations: [
    PhotoDetailPage
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoDetailPage
      }
    ])
  ],
  entryComponents: [],
})
export class PhotoDetailModule { }
