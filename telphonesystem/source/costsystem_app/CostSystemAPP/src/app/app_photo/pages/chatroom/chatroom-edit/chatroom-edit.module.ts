import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomEditPage } from './chatroom-edit.page';

@NgModule({
  providers: [],
  declarations: [ChatroomEditPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomEditPage
      }
    ])
  ]
})
export class ChatroomEditPageModule { }
