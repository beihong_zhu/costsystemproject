import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorySummaryPage } from './authory-summary.page';

const routes: Routes = [
  {
    path: '',
    component: AuthorySummaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthorySummaryPageRoutingModule {}
