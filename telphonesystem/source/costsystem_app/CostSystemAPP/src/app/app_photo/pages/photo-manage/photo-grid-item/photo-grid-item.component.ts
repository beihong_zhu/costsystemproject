import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppStateStoreService, CameraStoreService, PhotoDetailStoreService, HeadStateService, InitInterfaceType } from '@store';
import { RoutingService } from '@service/app-route.service';
import { LookType, LocalPhoto } from '../dto/photo-manage-dto';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { CD0005Constants, CD0001Constants } from '@constant/code';
import { PhotoListConf } from '../photo-list/photo-list-conf';
import { PhotoConf } from '@conf/photo-conf';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageAssets } from '@constant/assets';

@Component({
  selector: 'app-photo-grid-item',
  templateUrl: './photo-grid-item.component.html',
  styleUrls: ['./photo-grid-item.component.scss', './photo-grid-item.mobile.scss'],
})
export class PhotoGridItemComponent implements OnInit {
  @Input() selectPhotos: LocalPhoto[];
  @Input() photo: LocalPhoto;
  @Input() mulitype: string;
  @Input() bigProcess: LookType;
  @Input() isShowCheckBox: boolean;
  @Output() selectp = new EventEmitter<LocalPhoto>();
  @Output() goPhotoDetail = new EventEmitter<LocalPhoto>();
  @Input() isPhotoChange: boolean;
  isPc: boolean;
  platformtype: string;
  photobookimage: string;
  photocloudimage: string;
  cd0001Constants = CD0001Constants;

  get photoPath(): string {
    if (this.photo.photoPathLocal) {

      const filePath = this.file.dataDirectory + this.photo.photoPathLocal;
      return this.webview.convertFileSrc(filePath);

    } else {

      return this.photo.photoImage;

    }
  }

  get showcheckbox(): boolean {
    return this.showCheckBox();
  }

  get checked(): boolean {
    return this.selectPhotos.indexOf(this.photo) > -1;
  }
  // 写真表示
  get isShowPhoto(): boolean {
    return this.photoPath !== undefined && this.photoPath != null;
  }
  // server のicon
  get isShowServerPhotoIcon(): boolean {
    return this.photo.localPhotoFlag === PhotoListConf.SERVER_POTO && this.photoPath != null;
  }
  // 写真帳選択のicon
  get isShowPhotobookIcon(): boolean {
    return this.photo.isOutputPhotoBook === PhotoListConf.OUT_PUT_PHOTO_BOOK && this.photoPath != null;
  }
  // no image icon
  get isShowNoimageIcon(): boolean {
    return this.photoPath === undefined || this.photoPath === null;
  }

  get isShowTiming(): boolean {
    return this.bigProcess.id !== this.cd0001Constants.CD0001_04 && this.photo.timingAbbreviation !== PhotoConf.NOTIMING;
  }

  get isSelect(): boolean {
    return this.selectPhotos.indexOf(this.photo) > -1;
  }

  get uploadFail(): boolean {
    return this.photo.retryCount && this.photo.retryCount >= PhotoConf.RETRY_MAX_COUNT;
  }

  get showBottomview(): boolean {
    return this.isShowTiming || this.isShowPhotobookIcon || this.isShowServerPhotoIcon;
  }

  constructor(
    private appStateService: AppStateStoreService,
    private cameraStoreService: CameraStoreService,
    private photoDetailStoreService: PhotoDetailStoreService,
    private headStateService: HeadStateService,
    public routingService: RoutingService,
    private file: File,
    private webview: WebView,
    private sanitizer: DomSanitizer,
  ) {
    this.isPc = this.appStateService.appState.isPC;
    this.platformtype = 'photogriditem-' + this.appStateService.getPlatform();
    this.photobookimage = this.isPc ? ImageAssets.PHOTOBOOK_W : ImageAssets.PHOTOBOOK_M;
    this.photocloudimage = this.isPc ? ImageAssets.PHOTOCLOUD_W : ImageAssets.PHOTOCLOUD_M;
  }

  // ngOnChanges(simpleChange: SimpleChange) {

  //   this.statesChange();

  // }

  ngOnInit() { }


  selectphoto(event) {
    event.stopPropagation();
    this.selectp.emit(this.photo);
  }
  // checkbox 編集可能か
  showCheckBox() {
    // 写真なし&&編集状態&&!（（写真帳選択&&端末写真）||（端末&&手動同期&&serve 写真））
    return this.photoPath != null
      && this.isShowCheckBox
      && !((this.mulitype === CD0005Constants.PHOTO_BOOK && this.photo.localPhotoFlag === PhotoListConf.LOCAL_POTO)
        || (this.isPc === false && this.mulitype === CD0005Constants.HAND_UPLOAD
          && this.photo.localPhotoFlag === PhotoListConf.SERVER_POTO));
  }


  // no imageの場合、写真撮影遷移
  gotocamera() {

    if (!this.isPc) {
      this.cameraStoreService.setFromInitInterfaceType(InitInterfaceType.FromPhotoListNoIMage);
      this.cameraStoreService.setCurrentPhoto(this.photo);
      this.routingService.goRouteLink('/camera');
    }
  }

  // 写真click
  imageclick(event) {
    event.stopPropagation();
    // checkbox表示の場合
    if (this.isShowCheckBox) {
      this.selectp.emit(this.photo);
    } else {
      // checkbox非表示の場合，写真詳細へ遷移
      this.gotodetail(this.photo);
    }
  }

  // 写真詳細へ遷移
  gotodetail(photo) {
    this.goPhotoDetail.emit(photo);

  }
}
