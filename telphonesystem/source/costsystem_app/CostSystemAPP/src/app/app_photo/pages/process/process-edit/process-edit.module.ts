import { NgModule } from '@angular/core';
import { ProcessEditComponent } from './process-edit.page';
import { ProcessRenameComponent } from './process-rename/process-rename.component';
import { WorkRenameComponent } from './work-rename/work-rename.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProcessReducer } from '@store';
import { StoreModule } from '@ngrx/store';

@NgModule({
  providers: [],
  declarations: [ProcessEditComponent, WorkRenameComponent, ProcessRenameComponent],
  imports: [
    SharedModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProcessEditComponent
      }
    ]),
    StoreModule.forFeature('processes', ProcessReducer),
  ],
  entryComponents: [WorkRenameComponent, ProcessRenameComponent]
})
export class ProcessEditModule { }
