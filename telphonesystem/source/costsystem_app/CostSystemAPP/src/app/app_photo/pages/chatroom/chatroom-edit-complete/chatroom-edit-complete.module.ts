import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomEditCompletePage } from './chatroom-edit-complete.page';

@NgModule({
  providers: [],
  declarations: [ChatroomEditCompletePage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomEditCompletePage
      }
    ])
  ]
})
export class ChatroomEditCompletePageModule { }
