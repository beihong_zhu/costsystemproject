import { Component, OnInit } from '@angular/core';
import { API3010InDto, API3010OutDto } from '@service/dto';
import { API3010Service } from '@service/api';

import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';



@Component({
  selector: 'app-createstock',
  templateUrl: './createstock.page.html',
  styleUrls: ['./createstock.page.scss'],
})
export class CreatestockPage implements OnInit {

    // title

    public channelAccount: string;
    public address: string; 
    public productValue:string;
    public productCode:string;
    public productName:string;
    

    public  costInfo:any = {
     costTypeList:['话费慢充' ,'话费快充'],
     costType:'话费快充' ,
     provideSerivceList:[ '电信','移动',' 网通'],
     provideSevice:'移动',
     teletypeList: ['携带电话','固定电话'],
     teletype:'携带电话',
     provinceList:['北京','天津','上海','重庆','黑龙江','吉林','辽宁','内蒙古','江西 ','河南','湖南','湖北','河北','云南','贵州','浙江','江西','宁夏','青海','西藏','贵州','新疆','山西','山西','四川','重庆'],
     province:'北京'
    }
  
  
  constructor(private api3010Service: API3010Service) {
   }

  ngOnInit() {

  }


  public onSearch() {
    this.api3010ServicePostExec();
  }

  private get3010InDto() {
    let api3010InDto: API3010InDto = {};

    api3010InDto.channelAccount = this.channelAccount;
    api3010InDto.costType = this.costInfo.costType;
    api3010InDto.provideSevice = this.costInfo.provideSevice;
    api3010InDto.teletype = this.costInfo.teletype;
    api3010InDto.province =  this.costInfo.province;
    api3010InDto.address = this.address;
    api3010InDto.productValue = this.productValue;
    api3010InDto.productCode = this.productCode;
    api3010InDto.productName = this.productName;

    return api3010InDto;
  }


  private api3010ServicePostExec() {
    // // 写真帳出力情報取得
    // this.api0102Service.postExec(this.get0102InDto(), (outDto: API0102OutDto) => {
    //   this.api01f02successed(outDto);
    // },
    //   // fault
    //   () => {
    //   });

    // 写真帳出力情報取得
    this.api3010Service.postExecNew(this.get3010InDto()).then((res) => {
      // response の型は any ではなく class で型を定義した方が良いが
      // ここでは簡便さから any としておく

      // @angular/http では json() でパースする必要があったが､ @angular/common/http では不要となった
      //const response: any = res.json();
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3010successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }


  private api3010successed(outDto: API3010OutDto) {
    
  }







  

  

}
