import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { LookType } from '../dto/photo-manage-dto';
import { ListConstants } from '@constant/code';
import { S0501Constants } from '../../../constant/constant.photo';
import { AppStateStoreService } from '@store';


@Component({
  selector: 'app-photo-sort-set',
  templateUrl: './photo-sort-set.component.html',
  styleUrls: ['./photo-sort-set.component.scss', './photo-sort-set.mobile.scss', './photo-sort-set.winapp.scss'],
})
export class PhotoSortSetComponent implements OnInit {

  public ok = S0501Constants.DECISION;
  public cancleTitle = S0501Constants.CANCLE;
  public target = S0501Constants.TARGET;
  public asc = S0501Constants.ASCENDINGDES;

  title: string;

  sortby: string;
  sortdes: string;

  sorttypes: LookType[];
  sortdestypes: LookType[];

  platformtype: string;

  constructor(private params: NavParams, private appStateService: AppStateStoreService) {

    this.sorttypes = ListConstants.CD0007_LIST;

    this.sortdestypes = ListConstants.CD0008_LIST;



    this.title = S0501Constants.SORTSETTING;
    this.sortby = params.data.sortby;
    this.sortdes = params.data.sortdes;

    this.platformtype = 'sortset-' + this.appStateService.getPlatform();

  }

  ngOnInit() {
  }




  selectsort(ev): void {
    console.log(ev);
    this.sortby = ev.detail.value;

  }
  selectsortdes(ev): void {
    console.log(ev);
    this.sortdes = ev.detail.value;
  }

  commit(): void {

    this.params.data.modal.dismiss({ sortby: this.sortby, sortdes: this.sortdes });

  }

  cancle(): void {
    this.params.data.modal.dismiss(null);
  }

}
