import { ProcessDto } from './process.dto';

export class ProcessProcessGamenDto {
  processList?: Array<ProcessDto>;
  selectedBigprocess?: string;
}
