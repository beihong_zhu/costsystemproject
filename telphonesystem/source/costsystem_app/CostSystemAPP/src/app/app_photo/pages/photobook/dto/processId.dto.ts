export class ProcessId {
    // 工程ID
    processId: number;
    // 作業IDリスト
    workIdList?: {
        // 作業ID
        workId?: number;
    }[];
}
