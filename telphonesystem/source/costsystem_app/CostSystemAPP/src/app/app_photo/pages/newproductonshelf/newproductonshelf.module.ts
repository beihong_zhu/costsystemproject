import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewproductonshelfPageRoutingModule } from './newproductonshelf-routing.module';

import { NewproductonshelfPage } from './newproductonshelf.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewproductonshelfPageRoutingModule
  ],
  declarations: [NewproductonshelfPage]
})
export class NewproductonshelfPageModule {}
