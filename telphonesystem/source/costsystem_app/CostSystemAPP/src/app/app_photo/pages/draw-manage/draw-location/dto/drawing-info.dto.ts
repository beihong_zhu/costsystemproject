import { DrawingDto } from './drawing.dto';

export declare interface DrawingInfoDto {
  // 物件ID
  buildingId?: number;
  // 図面リスト
  drawings?: Array<DrawingDto>;
}
