import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { S0902Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { AlertController } from '@ionic/angular';
import { RoutingService } from '@service/app-route.service';
import {
  PhotobookViewStore, CommonStoreService,
  AppStateStoreService, PhotobookViewSetStoreService, PhotobookInfoSetStoreService
} from '@store';
import { Store } from '@ngrx/store';
import { GamenLable } from '../dto/gamen-lable.dto';

@Component({
  selector: 'app-photobook-view-set',
  templateUrl: './photobook-view-set.page.html',
  styleUrls: ['./photobook-view-set.page.scss', './photobook-view-set.page.mobile.scss', './photobook-view-set.page.winapp.scss'],
})
export class PhotobookViewSetPage implements OnInit {

  @ViewChild('itemValueTd1', { static: true })
  itemValueTd1: ElementRef;
  @ViewChild('itemValueTd2', { static: true })
  itemValueTd2: ElementRef;
  @ViewChild('itemValueTd3', { static: true })
  itemValueTd3: ElementRef;
  @ViewChild('itemValueTd4', { static: true })
  itemValueTd4: ElementRef;
  @ViewChild('itemValueTd5', { static: true })
  itemValueTd5: ElementRef;
  @ViewChild('itemValueTd6', { static: true })
  itemValueTd6: ElementRef;
  @ViewChild('itemValueTd7', { static: true })
  itemValueTd7: ElementRef;
  @ViewChild('itemValueTd8', { static: true })
  itemValueTd8: ElementRef;
  itemFontSize1: number = 16;
  itemFontSize2: number = 16;
  itemFontSize3: number = 16;
  itemFontSize4: number = 16;
  itemFontSize5: number = 16;
  itemFontSize6: number = 16;
  itemFontSize7: number = 16;
  itemFontSize8: number = 16;
  platformtype: string;
  // PC又はスマホ
  isPc: boolean;

  // 画面タイトル
  title: string = S0902Constants.TITLE;
  // 物件名
  buildingName: string;
  // 案件名
  projectName: string;
  // 配置内容
  itemValue1: string;
  itemValue2: string;
  itemValue3: string;
  itemValue4: string;
  itemValue5: string;
  itemValue6: string;
  itemValue7: string;
  itemValue8: string;
  // 配置件数
  itemCnt1: number;
  itemCnt2: number;
  itemCnt3: number;
  itemCnt4: number;
  itemCnt5: number;
  itemCnt6: number;
  itemCnt7: number;
  itemCnt8: number;
  // セレクトボックス内容
  selectBuilding: number;
  selectFloor: number;
  selectRoom: number;
  selectPlace: number;
  selectLargeProcess: number;
  selectProcess: number;
  selectWork: number;
  selectSituation: number;
  selectDate: number;
  selectTime: number;
  selectUnitMarkModelType: number;
  selectMachineNumber: number;
  selectWitness: number;
  selectConstructionCompany: number;
  selectFreeColumn: number;

  public photobookViewStore: PhotobookViewStore;

  /**
   * 画面項目名称取得
   */
  gamenLable: GamenLable = {
    buildingProjectName1: S0902Constants.BUILDINGPROJECTNAME1,
    buildingProjectName2: S0902Constants.BUILDINGPROJECTNAME2,
    photoTaken: S0902Constants.PHOTOTAKEN,
    arrangement1: S0902Constants.ARRANGEMENT1,
    arrangement2: S0902Constants.ARRANGEMENT2,
    arrangement3: S0902Constants.ARRANGEMENT3,
    arrangement4: S0902Constants.ARRANGEMENT4,
    arrangement5: S0902Constants.ARRANGEMENT5,
    arrangement6: S0902Constants.ARRANGEMENT6,
    arrangement7: S0902Constants.ARRANGEMENT7,
    arrangement8: S0902Constants.ARRANGEMENT8,
    photoBookOrderGuide: S0902Constants.PHOTOBOOKORDERGUIDE,
    buildingInformation: S0902Constants.BUILDINGINFORMATION,
    building: S0902Constants.BUILDING,
    floor: S0902Constants.FLOOR,
    room: S0902Constants.ROOM,
    place: S0902Constants.PLACE,
    processInformation: S0902Constants.PROCESSINFORMATION,
    largeProcess: S0902Constants.LARGEPROCESS,
    process: S0902Constants.PROCESS,
    work: S0902Constants.WORK,
    situation: S0902Constants.SITUATION,
    dateTimeInformation: S0902Constants.DATETIMEINFORMATION,
    date: S0902Constants.DATE,
    time: S0902Constants.TIME,
    equipmentInformation: S0902Constants.EQUIPMENTINFORMATION,
    unitMarkModelType: S0902Constants.unitMarkModelType,
    machineNumber: S0902Constants.MACHINENUMBER,
    other: S0902Constants.OTHER,
    witness: S0902Constants.WITNESS,
    constructionCompany: S0902Constants.CONSTRUCTIONCOMPANY,
    freeColumn: S0902Constants.FREECOLUMN,
    return: S0902Constants.RETURN,
    moveOn: S0902Constants.MOVEON,
    returnArrow: S0902Constants.RETURNARROW,
    moveOnArrow: S0902Constants.MOVEONARROW,
    items: JSON.parse(JSON.stringify(S0902Constants.ITEMS)),
    buildingItems: S0902Constants.BUILDINGITEM.concat(S0902Constants.ITEMS),
    floorItems: S0902Constants.FLOORITEM.concat(S0902Constants.ITEMS),
    roomItems: S0902Constants.ROOMITEM.concat(S0902Constants.ITEMS),
    placeItems: S0902Constants.PLACEITEM.concat(S0902Constants.ITEMS),
    largeProcessItems: S0902Constants.LARGEPROCESSITEM.concat(S0902Constants.ITEMS),
    processItems: S0902Constants.PROCESSITEM.concat(S0902Constants.ITEMS),
    workItems: S0902Constants.WORKITEM.concat(S0902Constants.ITEMS),
    situationItems: S0902Constants.SITUATIONITEM.concat(S0902Constants.ITEMS),
    dateItems: S0902Constants.DATEITEM.concat(S0902Constants.ITEMS),
    timeItems: S0902Constants.TIMEITEM.concat(S0902Constants.ITEMS),
    unitMarkModelTypeItems: S0902Constants.unitMarkModelTypeITEM.concat(S0902Constants.ITEMS),
    machineNumberItems: S0902Constants.MACHINENUMBERITEM.concat(S0902Constants.ITEMS),
    otherItems: S0902Constants.OTHERITEM.concat(S0902Constants.ITEMS),
    witnesslItems: S0902Constants.WITNESSITEM.concat(S0902Constants.ITEMS),
    constructionCompanyItems: S0902Constants.CONSTRUCTIONCOMPANYITEM.concat(S0902Constants.ITEMS),
    freeColumnItems: S0902Constants.FREECOLUMNITEM.concat(S0902Constants.ITEMS),
    layoutItems: JSON.parse(JSON.stringify(S0902Constants.LAYOUTITEMS)),
  };

  constructor(
    private routerService: RoutingService,
    public alertController: AlertController,
    public commonStoreService: CommonStoreService,
    public appStateService: AppStateStoreService,
    public photobookViewSetStoreService: PhotobookViewSetStoreService,
    public photobookInfoSetSotreService: PhotobookInfoSetStoreService,
    private storeSubscribe: Store<{ photobookViewStore: PhotobookViewStore }>
  ) {
    this.platformtype = 'photobook-view-set-' + this.appStateService.getPlatform();
    this.isPc = this.appStateService.appState.isPC ? true : false;



    // セレクトボックス初期化
    this.selectBuilding = 0;
    this.selectFloor = 0;
    this.selectRoom = 0;
    this.selectPlace = 0;
    this.selectLargeProcess = 0;
    this.selectProcess = 0;
    this.selectWork = 0;
    this.selectSituation = 0;
    this.selectDate = 0;
    this.selectTime = 0;
    this.selectUnitMarkModelType = 0;
    this.selectMachineNumber = 0;
    this.selectWitness = 0;
    this.selectConstructionCompany = 0;
    this.selectFreeColumn = 0;

  }

  ngOnInit() {

  }

  /**
   * 初期化する。
   */
  ionViewWillEnter() {
    const history = this.routerService.getHistory();
    if (history[history.length - 2] === '/privacy-policy') {
      // プライバシーポリシーから戻る時、写真帳画面の値を保持する
      return;
    }

    this.initData();
  }

  ionViewWillLeave() {
  }

  /**
   * 画面データを取得すること。
   */
  initData(): void {
    const photobookInfoStore = this.photobookInfoSetSotreService.photobookInfoStore;
    // 物件案件名
    this.buildingName = photobookInfoStore.chooseBuildingName;
    this.projectName = photobookInfoStore.chooseConstructionName;


    const photobookViewSetStore = this.photobookViewSetStoreService.photobookViewStore;
    // 配置情報初期表示
    this.itemValue1 = photobookViewSetStore.item1;
    this.itemValue2 = photobookViewSetStore.item2;
    this.itemValue3 = photobookViewSetStore.item3;
    this.itemValue4 = photobookViewSetStore.item4;
    this.itemValue5 = photobookViewSetStore.item5;
    this.itemValue6 = photobookViewSetStore.item6;
    this.itemValue7 = photobookViewSetStore.item7;
    this.itemValue8 = photobookViewSetStore.item8;
    this.itemCnt1 = photobookViewSetStore.itemCnt1;
    this.itemCnt2 = photobookViewSetStore.itemCnt2;
    this.itemCnt3 = photobookViewSetStore.itemCnt3;
    this.itemCnt4 = photobookViewSetStore.itemCnt4;
    this.itemCnt5 = photobookViewSetStore.itemCnt5;
    this.itemCnt6 = photobookViewSetStore.itemCnt6;
    this.itemCnt7 = photobookViewSetStore.itemCnt7;
    this.itemCnt8 = photobookViewSetStore.itemCnt8;
    // セレクトボックス情報初期表示
    this.selectBuilding = photobookViewSetStore.building;
    this.selectFloor = photobookViewSetStore.floor;
    this.selectRoom = photobookViewSetStore.room;
    this.selectPlace = photobookViewSetStore.place;
    this.selectLargeProcess = photobookViewSetStore.largeProcess;
    this.selectProcess = photobookViewSetStore.process;
    this.selectWork = photobookViewSetStore.work;
    this.selectSituation = photobookViewSetStore.situation;
    this.selectDate = photobookViewSetStore.date;
    this.selectTime = photobookViewSetStore.time;
    this.selectUnitMarkModelType = photobookViewSetStore.unitMarkModelType;
    this.selectMachineNumber = photobookViewSetStore.machineNumber;
    this.selectWitness = photobookViewSetStore.witness;
    this.selectConstructionCompany = photobookViewSetStore.constructionCompany;
    this.selectFreeColumn = photobookViewSetStore.freeColumn;
    [this.selectBuilding, this.selectFloor, this.selectRoom, this.selectPlace, this.selectLargeProcess, this.selectProcess,
      this.selectWork, this.selectSituation, this.selectDate, this.selectTime, this.selectUnitMarkModelType,
      this.selectMachineNumber, this.selectWitness, this.selectConstructionCompany, this.selectFreeColumn].forEach((item, index) => {
        this.gamenLable.layoutItems[index].oldItem = item;
        this.gamenLable.layoutItems[index].item = item;
      });

    this.changeItemFontSize();
  }

  // セレクトボックス変更
  async itemChange(name: string, itemNum: number): Promise<void> {

    // セレクト件数をチェック
    let selectCntFlg = true;
    switch (itemNum) {
      case 1:
        if (this.itemCnt1 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 2:
        if (this.itemCnt2 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 3:
        if (this.itemCnt3 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 4:
        if (this.itemCnt4 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 5:
        if (this.itemCnt5 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 6:
        if (this.itemCnt6 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 7:
        if (this.itemCnt7 >= 4) {
          selectCntFlg = false;
        }
        break;
      case 8:
        if (this.itemCnt8 >= 4) {
          selectCntFlg = false;
        }
        break;
    }
    if (!selectCntFlg) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00047,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false,
      });
      await alert.present();
      this.selectReturn(name);
      this.changeItemFontSize();
      return;
    }

    // 配置情報クリア
    this.itemValue1 = '';
    this.itemValue2 = '';
    this.itemValue3 = '';
    this.itemValue4 = '';
    this.itemValue5 = '';
    this.itemValue6 = '';
    this.itemValue7 = '';
    this.itemValue8 = '';
    // 配置件数クリア
    this.itemCnt1 = 0;
    this.itemCnt2 = 0;
    this.itemCnt3 = 0;
    this.itemCnt4 = 0;
    this.itemCnt5 = 0;
    this.itemCnt6 = 0;
    this.itemCnt7 = 0;
    this.itemCnt8 = 0;

    // 配置情報を自動設定する
    Array.from({ length: this.gamenLable.layoutItems.length }).map(async (_, i) => {

      if (name === this.gamenLable.layoutItems[i].name) {
        this.gamenLable.layoutItems[i].item = itemNum;
      }

      switch (this.gamenLable.layoutItems[i].item) {
        case this.gamenLable.items[0].key:
          if (this.itemValue1 === null || this.itemValue1 === '') {
            this.itemValue1 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue1 = this.itemValue1 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.gamenLable.layoutItems[i].oldItem = this.gamenLable.layoutItems[i].item;
          this.itemCnt1++;
          break;

        case this.gamenLable.items[1].key:
          if (this.itemValue2 === null || this.itemValue2 === '') {
            this.itemValue2 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue2 = this.itemValue2 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt2++;
          break;

        case this.gamenLable.items[2].key:
          if (this.itemValue3 === null || this.itemValue3 === '') {
            this.itemValue3 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue3 = this.itemValue3 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt3++;
          break;

        case this.gamenLable.items[3].key:
          if (this.itemValue4 === null || this.itemValue4 === '') {
            this.itemValue4 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue4 = this.itemValue4 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt4++;
          break;

        case this.gamenLable.items[4].key:
          if (this.itemValue5 === null || this.itemValue5 === '') {
            this.itemValue5 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue5 = this.itemValue5 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt5++;
          break;

        case this.gamenLable.items[5].key:
          if (this.itemValue6 === null || this.itemValue6 === '') {
            this.itemValue6 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue6 = this.itemValue6 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt6++;
          break;

        case this.gamenLable.items[6].key:
          if (this.itemValue7 === null || this.itemValue7 === '') {
            this.itemValue7 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue7 = this.itemValue7 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt7++;
          break;

        case this.gamenLable.items[7].key:
          if (this.itemValue8 === null || this.itemValue8 === '') {
            this.itemValue8 = this.gamenLable.layoutItems[i].name;
          } else {
            this.itemValue8 = this.itemValue8 + ', ' + this.gamenLable.layoutItems[i].name;
          }
          this.itemCnt8++;
          break;
      }
      this.gamenLable.layoutItems[i].oldItem = this.gamenLable.layoutItems[i].item;
    });

    this.changeItemFontSize();
  }

  // 選択前の内容を戻る
  selectReturn(name: string): void {
    switch (name) {
      case S0902Constants.BUILDING:
        this.selectBuilding = Number(this.gamenLable.layoutItems[0].oldItem);
        break;
      case S0902Constants.FLOOR:
        this.selectFloor = Number(this.gamenLable.layoutItems[1].oldItem);
        break;
      case S0902Constants.ROOM:
        this.selectRoom = Number(this.gamenLable.layoutItems[2].oldItem);
        break;
      case S0902Constants.PLACE:
        this.selectPlace = Number(this.gamenLable.layoutItems[3].oldItem);
        break;
      case S0902Constants.LARGEPROCESS:
        this.selectLargeProcess = Number(this.gamenLable.layoutItems[4].oldItem);
        break;
      case S0902Constants.PROCESS:
        this.selectProcess = Number(this.gamenLable.layoutItems[5].oldItem);
        break;
      case S0902Constants.WORK:
        this.selectWork = Number(this.gamenLable.layoutItems[6].oldItem);
        break;
      case S0902Constants.SITUATION:
        this.selectSituation = Number(this.gamenLable.layoutItems[7].oldItem);
        break;
      case S0902Constants.DATE:
        this.selectDate = Number(this.gamenLable.layoutItems[8].oldItem);
        break;
      case S0902Constants.TIME:
        this.selectTime = Number(this.gamenLable.layoutItems[9].oldItem);
        break;
      case S0902Constants.unitMarkModelType:
        this.selectUnitMarkModelType = Number(this.gamenLable.layoutItems[10].oldItem);
        break;
      case S0902Constants.MACHINENUMBER:
        this.selectMachineNumber = Number(this.gamenLable.layoutItems[11].oldItem);
        break;
      case S0902Constants.WITNESS:
        this.selectWitness = Number(this.gamenLable.layoutItems[12].oldItem);
        break;
      case S0902Constants.CONSTRUCTIONCOMPANY:
        this.selectConstructionCompany = Number(this.gamenLable.layoutItems[13].oldItem);
        break;
      case S0902Constants.FREECOLUMN:
        this.selectFreeColumn = Number(this.gamenLable.layoutItems[14].oldItem);
        break;
    }
  }

  // プレビュー表示画面に遷移
  nextPage() {
    this.saveGamenSetting();
    const url = '/photobook-preview';
    this.routerService.goRouteLink(url);
  }

  // 写真帳出力情報設定画面に遷移
  prevPage() {
    this.saveGamenSetting();
    const url = '/photobook-info-set';
    this.routerService.goRouteLink(url);
  }

  saveGamenSetting() {
    const photobookViewSetStore = this.photobookViewSetStoreService.photobookViewStore;
    // 配置情報は、ストアに保存する
    photobookViewSetStore.item1 = this.itemValue1;
    photobookViewSetStore.item2 = this.itemValue2;
    photobookViewSetStore.item3 = this.itemValue3;
    photobookViewSetStore.item4 = this.itemValue4;
    photobookViewSetStore.item5 = this.itemValue5;
    photobookViewSetStore.item6 = this.itemValue6;
    photobookViewSetStore.item7 = this.itemValue7;
    photobookViewSetStore.item8 = this.itemValue8;
    photobookViewSetStore.itemCnt1 = this.itemCnt1;
    photobookViewSetStore.itemCnt2 = this.itemCnt2;
    photobookViewSetStore.itemCnt3 = this.itemCnt3;
    photobookViewSetStore.itemCnt4 = this.itemCnt4;
    photobookViewSetStore.itemCnt5 = this.itemCnt5;
    photobookViewSetStore.itemCnt6 = this.itemCnt6;
    photobookViewSetStore.itemCnt7 = this.itemCnt7;
    photobookViewSetStore.itemCnt8 = this.itemCnt8;
    // セレクトボックス情報は、ストアに保存する
    photobookViewSetStore.building = this.selectBuilding;
    photobookViewSetStore.floor = this.selectFloor;
    photobookViewSetStore.room = this.selectRoom;
    photobookViewSetStore.place = this.selectPlace;
    photobookViewSetStore.largeProcess = this.selectLargeProcess;
    photobookViewSetStore.process = this.selectProcess;
    photobookViewSetStore.work = this.selectWork;
    photobookViewSetStore.situation = this.selectSituation;
    photobookViewSetStore.date = this.selectDate;
    photobookViewSetStore.time = this.selectTime;
    photobookViewSetStore.unitMarkModelType = this.selectUnitMarkModelType;
    photobookViewSetStore.machineNumber = this.selectMachineNumber;
    photobookViewSetStore.witness = this.selectWitness;
    photobookViewSetStore.constructionCompany = this.selectConstructionCompany;
    photobookViewSetStore.freeColumn = this.selectFreeColumn;
    // 画面情報を作成
    this.photobookViewSetStoreService.setPhotobookView(photobookViewSetStore);
  }

  changeItemFontSize() {
    const defaultSize = 16;
    const minSize = 3;
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');

    this.itemFontSize1 = minSize;
    const itemOffsetWidth1 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue1, i, context) <= itemOffsetWidth1) {
        this.itemFontSize1 = i;
        break;
      }
    }
    this.itemFontSize2 = minSize;
    const itemOffsetWidth2 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue2, i, context) <= itemOffsetWidth2) {
        this.itemFontSize2 = i;
        break;
      }
    }
    this.itemFontSize3 = minSize;
    const itemOffsetWidth3 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue3, i, context) <= itemOffsetWidth3) {
        this.itemFontSize3 = i;
        break;
      }
    }
    this.itemFontSize4 = minSize;
    const itemOffsetWidth4 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue4, i, context) <= itemOffsetWidth4) {
        this.itemFontSize4 = i;
        break;
      }
    }
    this.itemFontSize5 = minSize;
    const itemOffsetWidth5 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue5, i, context) <= itemOffsetWidth5) {
        this.itemFontSize5 = i;
        break;
      }
    }
    this.itemFontSize6 = minSize;
    const itemOffsetWidth6 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue6, i, context) <= itemOffsetWidth6) {
        this.itemFontSize6 = i;
        break;
      }
    }
    this.itemFontSize7 = minSize;
    const itemOffsetWidth7 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue7, i, context) <= itemOffsetWidth7) {
        this.itemFontSize7 = i;
        break;
      }
    }
    this.itemFontSize8 = minSize;
    const itemOffsetWidth8 = this.itemValueTd1.nativeElement.offsetWidth;
    for (let i = defaultSize; i > minSize; i--) {
      if (this.getStrPxLength(this.itemValue8, i, context) <= itemOffsetWidth8) {
        this.itemFontSize8 = i;
        break;
      }
    }
  }

  getStrPxLength(str: string, fontSize: number, context) {
    context.font = fontSize + 'px Roboto, "Helvetica Neue", sans-serif';
    return context.measureText(str || '').width;
  }
}

export class ItemDto {
  // key
  key: string;
  // value
  value: string;
}

export class LayoutDto {
  // key
  item: string;
  // value
  name: string;

  value: string;
}
