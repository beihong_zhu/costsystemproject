import { Component, OnInit } from '@angular/core';
import { S2010Constants } from '@constant/constant.photo';
import { AlertController, NavParams } from '@ionic/angular';
import { SysFundRecorderDto } from '../dto';

@Component({
  selector: 'app-fundrecorde-edit',
  templateUrl: './fundrecorder-edit.component.html',
  styleUrls: ['./fundrecorder-edit.component.scss']
})

export class FundrecorderEditComponent implements OnInit {
  /**
   * 获取画面項目名称
   */
  public fundrecorderEditLable: any = {
    title: S2010Constants.TITLE,
    agentName: S2010Constants.AGENTNAME,
    reqId: S2010Constants.REQID,
    orderId: S2010Constants.ORDERID,
    billId: S2010Constants.BILLID,
    agentReduceName: S2010Constants.CANCEL,
    orderForm: S2010Constants.ORDERFORM,
    orderType: S2010Constants.ORDERTYPE,
    capital: S2010Constants.CAPITAL,
    price: S2010Constants.PRICE,
    status: S2010Constants.STATUS,
    createdDatetime: S2010Constants.CREATEDDATETIME,
    change: S2010Constants.CHANGE,
    cancel: S2010Constants.CANCEL
  };

  public sysFundRecorder: SysFundRecorderDto = new SysFundRecorderDto();

  constructor(
    public alertController: AlertController,
    public navParams: NavParams) {
      let sysFundRecorder: SysFundRecorderDto = new SysFundRecorderDto();
      sysFundRecorder = navParams.data.value;
      this.sysFundRecorder.agentName = sysFundRecorder.agentName;
      this.sysFundRecorder.reqId = sysFundRecorder.reqId;
      this.sysFundRecorder.orderId = sysFundRecorder.orderId;
      this.sysFundRecorder.billId = sysFundRecorder.billId;
      this.sysFundRecorder.agentReduceName = sysFundRecorder.agentReduceName;
      this.sysFundRecorder.orderForm = sysFundRecorder.orderForm;
      this.sysFundRecorder.orderType = sysFundRecorder.orderType;
      this.sysFundRecorder.capital = sysFundRecorder.capital;
      this.sysFundRecorder.price = sysFundRecorder.price;
      this.sysFundRecorder.status = sysFundRecorder.status;
      this.sysFundRecorder.ts = sysFundRecorder.ts;
  }

  ngOnInit() {

  }

  /**
   * 更新按钮点击事件
   */
  async updateClick() {

  }

  /**
   * 取消按钮点击事件
   */
  async cancelClick() {

  }

  /**
   * 关闭按钮点击事件
   */
  async closeClick() {
    this.navParams.data.modal.dismiss(null);
  }
}
