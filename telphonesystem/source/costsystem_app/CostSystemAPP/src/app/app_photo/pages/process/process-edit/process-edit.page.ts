import { Component, OnInit } from '@angular/core';
import { ProcessEditGamenDto, ProcessDto, WorkDto } from './dto';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { ProcessRenameComponent } from './process-rename/process-rename.component';
import { WorkRenameComponent } from './work-rename/work-rename.component';
import { S0801Constants, AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { API0810Service, API0801Service } from '@service/api';
import { API0810InDto, API0810OutDto, API0801InDto, API0801OutDto } from '@service/dto';
import { AppStateStoreService, ProcessStoreService, CommonStoreService } from '@store';
import { ResultType } from '../../../../app_common/constant/common';
import { ListConstants } from '@constant/code';
import { CD0015Constants } from '@constant/code';
import { PopoverController } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { ProcessList0810 } from '../../../service/dto/api-08-10-in-dto';

@Component({
  selector: 'app-process-edit',
  templateUrl: './process-edit.page.html',
  styleUrls: ['./process-edit.page.scss', './process-edit.page.mobile.scss', './process-edit.page.winapp.scss'],
})
/**
 * 工程編集画面
 */
export class ProcessEditComponent {

  /**
   * 画面DTO
   */
  public gamenDto: ProcessEditGamenDto = {};

  /**
   * 選択された工程のID
   */
  public selectedProcessId: string[];
  /**
   * 選択された作業のID
   */
  public selectedWorkId: string[];

  /**
   * isPc
   */
  public isPc: boolean;

  public isDisabledDelete = true;

  public canDelete: boolean = true;

  // 物件
  buildingName: string;
  // 案件
  selectProject: number;
  projectName: string;
  // 大工程
  bigprocessName: string;
  selectBigprocess: string;
  bigprocessList: Array<CodeDto>;

  platformtype: string;

  /**
   * 画面項目名称取得
   */
  public gamenLable = {
    headerLable: S0801Constants.HEADER,
    selectCntLabel: S0801Constants.SELECTCNTLABEL
  };

  /**
   * コンストラクタ
   * @param modalController ダイアログコントローラ
   * @param alertController ダイアログコントローラ
   * @param API0804Service 工程削除サービス
   * @param appStateService AppStateService
   * @param store Store
   */
  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    public api0810Service: API0810Service,
    public api0801Service: API0801Service,
    public appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    public processStoreService: ProcessStoreService,
    public popoverController: PopoverController,
    private platform: Platform
  ) {
    if (this.commonStoreService.commonStore.authorId) {
      if (this.commonStoreService.commonStore.authorId === CD0015Constants.AUTHORITY04) {
        this.canDelete = false;
      }
    }
    this.platformtype = 'processEdit-' + this.appStateService.getPlatform();
  }


  /**
   * 初期化する。
   */
  ionViewWillEnter() {
    this.isPc = this.appStateService.appState.isPC ? true : false;

    this.buildingName = this.commonStoreService.commonStore.buildingName;
    this.selectProject = this.processStoreService.processCreateState.selectProject;
    this.projectName = this.commonStoreService.commonStore.projectName;
    this.selectBigprocess = this.processStoreService.processCreateState.bigProcessId;
    this.bigprocessList = ListConstants.CD0001_LIST;
    Array.from({ length: this.bigprocessList.length }).map(async (_, i) => {
      if (this.bigprocessList[i].id === this.selectBigprocess) {
        this.bigprocessName = this.bigprocessList[i].value;
      }
    });

    this.initData();
  }

  /**
   * 画面データを取得すること。
   */
  initData(): void {
    this.gamenDto.selectCnt = 0;
    this.selectedProcessId = [];
    this.selectedWorkId = [];
    this.gamenDto.processList = [];
    this.isDisabledDelete = true;

    if (this.processStoreService.processCreateState) {
      const stateProcesses: ProcessDto[] = this.processStoreService.processCreateState.processes || [];
      this.gamenDto.processList = stateProcesses;
    }
  }

  /**
   * 工程チェックボックスがクリックされること。
   * @param process 工程DTO
   */
  processChkClick(process: ProcessDto): void {
    process.isOpen = true;
    const works: WorkDto[] = process.workList;
    // 該当工程で作業の存在すること判断
    if (works) {
      const worksLength: number = works.length;
      for (let i = 0; i < worksLength; i++) {
        works[i].isChecked = process.isChecked;
      }
    }
    this.workChkClick();
  }

  /**
   * 作業チェックボックスがクリックされること。
   */
  workChkClick(): void {
    const gamenDto = this.gamenDto;
    let selectedWork = 0;
    const selectedProcessId = [];
    const selectedWorkId = [];


    const processesLength: number = gamenDto.processList.length;
    for (let i = 0; i < processesLength; i++) {
      const works: WorkDto[] = gamenDto.processList[i].workList;
      // 該当工程で作業の存在すること判断
      if (works) {
        const worksLength: number = works.length;
        let processAllSelected = true;
        for (let j = 0; j < worksLength; j++) {
          // 工程isChecked
          if (works[j].isChecked) {
            selectedWork++;
            selectedWorkId.push(works[j].workId);
          } else {
            processAllSelected = false;
          }
        }
        // 工程が全部Checked
        if ((worksLength > 0 && processAllSelected)
          || (worksLength === 0 && gamenDto.processList[i].isChecked)) {
          selectedProcessId.push(gamenDto.processList[i].processId);
          // gamenDto.processList[i].isChecked = true;
        } else {
          gamenDto.processList[i].isChecked = false;
        }
      } else {
        if (gamenDto.processList[i].isChecked) {
          selectedProcessId.push(gamenDto.processList[i].processId);
        }
      }
    }
    this.selectedWorkId = selectedWorkId;
    this.selectedProcessId = selectedProcessId;
    this.gamenDto.selectCnt = selectedWork;
    this.isDisabledDelete = selectedProcessId.length === 0 && selectedWorkId.length === 0;
  }

  /**
   * 工程内容展開すること。
   * @param process 工程DTO
   */
  expandChange(process: ProcessDto): void {
    process.isOpen = !process.isOpen;
  }

  /**
   * 工程、作業情報の削除、変更に基づく再表示。
   */
  reInit(): void {
    this.api0801Service.postExec(this.get0801Indto(), (outDto: API0801OutDto) => {
      this.gamenDto.selectCnt = 0;
      this.selectedProcessId = [];
      this.selectedWorkId = [];
      this.gamenDto.processList = [];
      this.isDisabledDelete = true;

      if (outDto.processList) {
        this.gamenDto.processList = outDto.processList;
      }
    },
      // fault
      () => {
      });

  }

  /**
   * API0804送信
   */
  get0801Indto(): API0801InDto {
    const inDto: API0801InDto = {};
    inDto.projectId = this.selectProject;
    inDto.bigProcessId = this.processStoreService.processCreateState.bigProcessId;
    return inDto;
  }

  /**
   * 工程名変更ダイアログを開く
   * @param process 工程DTO
   */
  async updateProcess(process: ProcessDto) {
    const modal = await this.modalController.create({
      component: ProcessRenameComponent,
      componentProps: {
        value: process,
        bigProcessId: this.selectBigprocess
      },
      cssClass: 'processRenameModal-' + this.appStateService.getPlatform()
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // 変更ボタンを押下するか判断
    if (data != null) {
      this.reInit();
    }
  }

  /**
   * 作業名変更ダイアログを開く
   * @param process 作業DTO
   */
  async updateWork(work: WorkDto, processId) {
    const modal = await this.modalController.create({
      component: WorkRenameComponent,
      componentProps: {
        value: work,
        bigProcessId: this.selectBigprocess,
        processId
      },
      cssClass: 'workRenameModal-' + this.appStateService.getPlatform()

    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    // 変更ボタンを押下するか判断
    if (data != null) {
      this.reInit();
    }
  }

  /**
   * 削除ダイアログを開く
   */
  async deleteClick() {
    if (this.selectedProcessId.length === 0 && this.selectedWorkId.length === 0) {
      return;
    }
    const alert = await this.alertController.create({
      header: AlertWindowConstants.CONFIRMTITLE,
      message: MessageConstants.C00002,
      buttons: [{
        text: AlertWindowConstants.OKBUTTON,
        handler: () => {
          // 工程削除API
          this.api0810Service.postExec(this.getDeleteInDto(), (outDto: API0810OutDto) => {
            this.api0810Successed(outDto);
          },
            // fault
            () => {
            });

        }
      },
      {
        text: AlertWindowConstants.CANCELBUTTON,
        role: 'cancel',
        cssClass: 'primary',
        handler: (blah) => {
        }
      }
      ],
      backdropDismiss: false
    });
    alert.addEventListener('ionAlertDidPresent', () => {
      const alertButton = alert.getElementsByClassName('alert-button');
      if (alertButton && alertButton.length > 0) {
        (alertButton[0] as HTMLElement).focus();
      }
    });
    await alert.present();
  }

  /**
   * 削除成功、API0810返信。
   * @param outDto API0810OutDto
   */
  async api0810Successed(outDto: API0810OutDto) {
    if (outDto.resultCode === ResultType.NORMAL) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.TIPSTITLE,
        message: this.isPc ? MessageConstants.I00004 : MessageConstants.I00024,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.reInit();
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    } else if (outDto.resultCode === ResultType.BUSINESSERROR) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants[outDto.messageCode] || outDto.message,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON,
            handler: () => {
              this.reInit();
            }
          }
        ],
        backdropDismiss: false
      });
      alert.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alert.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alert.present();
    }
  }
  /**
   * API0804送信
   */
  getDeleteInDto(): API0810InDto {
    const inDto: API0810InDto = {};
    inDto.processList = [];
    inDto.projectId = this.commonStoreService.commonStore.projectId;
    inDto.bigProcessId = this.isPc ? this.selectBigprocess : this.commonStoreService.commonStore.bigProcessId;
    const gamenDto = this.gamenDto;
    const processesLength: number = gamenDto.processList.length;
    for (let i = processesLength - 1; i >= 0; i--) {
      let deleteProcess: ProcessList0810 = null;
      // 工程を削除するかどうか判断
      if (gamenDto.processList[i].isChecked) {
        deleteProcess = {};
        deleteProcess.processId = gamenDto.processList[i].processId;
        deleteProcess.isProcessDelete = '1';

        inDto.processList.push(deleteProcess);
        continue;
      }
      const works: WorkDto[] = gamenDto.processList[i].workList;
      // 該当工程で作業の存在すること判断
      if (works) {
        const worksLength: number = works.length;
        for (let j = worksLength - 1; j >= 0; j--) {
          // 工程isChecked
          if (works[j].isChecked) {
            if (!deleteProcess) {
              deleteProcess = {};
            }
            deleteProcess.processId = gamenDto.processList[i].processId;
            if (!deleteProcess.workList) {
              deleteProcess.workList = [];
            }
            deleteProcess.isProcessDelete = '0';
            deleteProcess.workList.push({ workId: works[j].workId });
          }
        }
      }
      if (deleteProcess) {
        inDto.processList.push(deleteProcess);
      }
    }
    return inDto;
  }

  async onPressBuilding(ev: Event) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPressBuilding', ev);
    const messageArr1 = [this.buildingName, this.projectName, this.bigprocessName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();
  }
}

export class Project {
  projectId: number;
  projectName: string;
}

export class CodeDto {
  // コードID
  id?: string;
  // コード名
  value?: string;
}
