import { NgModule } from '@angular/core';
import { PhotoShowListComponent } from './photo-show-list.component';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { PhotoListItemComponent } from '../photo-list-item/photo-list-item.component';
import { AppStateStoreService, CommonStoreService } from '@store';

@NgModule({
  providers: [
    AppStateStoreService,
    CommonStoreService,
  ],
  declarations: [
    // PhotoListComponent,
    // PhotoListItemComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoShowListComponent
      }
    ])
  ]
})
export class PhotoShowListModule { }
