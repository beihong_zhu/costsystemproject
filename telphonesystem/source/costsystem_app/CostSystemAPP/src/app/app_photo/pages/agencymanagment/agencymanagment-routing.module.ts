import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgencymanagmentPage } from './agencymanagment.page';

const routes: Routes = [
  {
    path: '',
    component: AgencymanagmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgencymanagmentPageRoutingModule {}
