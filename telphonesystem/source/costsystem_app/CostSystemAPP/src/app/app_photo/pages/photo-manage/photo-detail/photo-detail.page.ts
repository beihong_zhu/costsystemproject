import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { LocalPhoto } from '../dto/photo-manage-dto';
import { AppStateStoreService, CommonStore, CommonStoreService, PhotoDetailStore, PhotoDetailStoreService, HeadStateService } from '@store';
import { Store, select } from '@ngrx/store';
import { S0502Constants } from '@constant/constant.photo';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import * as moment from 'moment';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { PopoverController, Platform, IonSlides } from '@ionic/angular';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-photo-detail',
  templateUrl: './photo-detail.page.html',
  styleUrls: ['./photo-detail.page.scss',
    './swiper.css', './photo-detail.mobile.scss', './photo-detail.winapp.scss'],
})

// 写真詳細画面
export class PhotoDetailPage implements OnInit, OnDestroy {

  // npm install --save swiper
  // npm install --save @types/swiper
  @ViewChild('swiperSlide', { static: false }) swiperSlide: IonSlides;

  photoList: Array<LocalPhoto>;

  // galleryThumbs: Swiper;
  selectPhoto: LocalPhoto;

  isPc: boolean;
  isWinApp: boolean;

  thumbsCnt: number;

  photoDetailStore: PhotoDetailStore;

  photoDetailStoreSubscription: Subscription;

  commonStore: CommonStore;
  // 物件名
  buildingName: string;
  // 案件名
  projectName: string;
  // 大工程名
  bigProcessName: string;

  // 当前選択したindex
  selectedIndex: number;
  sliderConfig: any;

  photoPath: string | SafeResourceUrl;

  platformtype: string;

  public bdTitle = S0502Constants.BDTITLE;
  public bdBuilingTitle = S0502Constants.BDBUILDINGTITLE;
  public bdProjectTitle = S0502Constants.BDPROJECTTITLE;
  public bdFloorTitle = S0502Constants.BDFLOORTITLE;
  public bdRoomTitle = S0502Constants.BDROOMTITLE;
  public bdPlaceTitle = S0502Constants.BDPLACETITLE;
  public bdBigProcessTitle = S0502Constants.BDBIGPROCESSTITLE;
  public bdMachineTitle = S0502Constants.BDMACHINETITLE;
  public bdProcessTitle = S0502Constants.BDPROCESSTITLE;
  public bdWorkTitle = S0502Constants.BDWORKTITLE;
  public bdTimingTitle = S0502Constants.BDTIMINGTITLE;


  constructor(
    private headStateService: HeadStateService,
    private sanitizer: DomSanitizer,
    private popoverController: PopoverController,
    private file: File,
    private webview: WebView,
    private appStateService: AppStateStoreService,
    private commonStoreService: CommonStoreService,
    private photoDetailStoreService: PhotoDetailStoreService,
    private platform: Platform) {

    this.commonStore = this.commonStoreService.commonStore;
    this.buildingName = this.commonStore.buildingName;
    this.projectName = this.commonStore.projectName;
    this.bigProcessName = this.commonStore.bigProcessName;

    this.platformtype = 'photodetail-' + this.appStateService.getPlatform();
  }

  ngOnInit() {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.isWinApp = this.appStateService.appState.isWinApp ? true : false;

    // 写真管理ストアから写真一覧画面で設定した写真リストを取得する。
    // ソート済み、フィルター済の写真一覧？
    this.selectedIndex = 0;
    this.subscribePhotoManage();

    if (this.isPc || this.isWinApp) {
      this.thumbsCnt = 1;
    } else {
      this.thumbsCnt = 3;
    }


    this.sliderConfig = {
      slidesPerView: this.thumbsCnt,
      loop: false,
      centeredSlides: false,
      spaceBetween: this.isPc || this.isWinApp ? 0 : 10,
      simulateTouch: false // 禁掉手指滑动事件
    };

  }

  ngOnDestroy() {
    console.log('PhotoDetailPage------------------ ngOnDestroy');
    this.photoDetailStoreSubscription.unsubscribe();

  }

  ionViewDidEnter() {
    console.log('ChatRoomEditPage ionViewDidEnter', this.selectedIndex);
    this.selectIndex();
    this.swiperSlide.slideTo(this.selectedIndex, 500);
  }

  // 選択した写真のindex
  selectIndex() {

    let index = 0;
    for (const photo of this.photoList) {
      if (photo.photoTerminalNo === this.selectPhoto.photoTerminalNo &&
        Number(photo.photoDate) === Number(this.selectPhoto.photoDate)) {
        this.selectedIndex = index;
        console.log('this.selectedIndex', this.selectedIndex);
        break;
      }
      index = index + 1;
    }

  }

  /**
   * 写真管理ストアから写真一覧画面で設定した写真リストを取得する
   */
  subscribePhotoManage() {

    this.photoDetailStore = this.photoDetailStoreService.photoDetailStore;
    this.photoList = this.photoDetailStore.detailPhotoList;
    this.selectPhoto = this.photoDetailStore.detailPhoto;

    console.log('phototDetailPage service subscribePhotoManage', this.photoDetailStore);
    this.photoDetailStoreSubscription = this.photoDetailStoreService.photoDetailStoreSubscription((state => {
      console.log('phototDetailPage photomanageState', state);
      this.photoDetailStore = state;
      this.photoList = this.photoDetailStore.detailPhotoList;
      this.selectPhoto = this.photoDetailStore.detailPhoto;

      this.setHeadTitle(this.selectPhoto);

      // 変換photopath
      if (this.selectPhoto.photoPathLocal) {
        this.photoPath = this.convertFileSrc(this.selectPhoto.photoPathLocal);
      } else {
        this.photoPath = this.selectPhoto.photoImage;
      }

    }));

  }

  setHeadTitle(photo) {
    const title = photo.photoUser + '，' + moment(photo.photoDate.toString(), 'YYYYMMDDHHmmss').format('YYYY/MM/DD HH:mm');
    this.headStateService.setTitle(title);
  }

  convertFileSrc(path) {
    const filePath = this.file.dataDirectory + path;
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.webview.convertFileSrc(filePath));
  }


  /**
   * 写真のサムネイルをタップイベント
   * @photo 選択した写真オブジェクト
   */
  onSelectPhoto(photo: LocalPhoto, index) {
    this.photoDetailStoreService.setDetailPhoto(photo);
    this.selectedIndex = index;
  }

  selectPrevious() {
    // 最初のページではない
    if (this.selectedIndex > 0) {
      const index = this.selectedIndex - 1;
      this.selectedIndex = null;
      this.selectedIndex = index;
      const photo = this.photoList[this.selectedIndex];
      this.photoDetailStoreService.setDetailPhoto(photo);
      this.swiperSlide.slideTo(this.selectedIndex, 500);
    }

  }

  selectNext() {
    // 最後のページではない
    if (this.selectedIndex < this.photoList.length - 1) {
      this.selectedIndex = this.selectedIndex + 1;
      const photo = this.photoList[this.selectedIndex];
      this.photoDetailStoreService.setDetailPhoto(photo);
      this.swiperSlide.slideTo(this.selectedIndex, 500);

    }

  }


  async onPress(ev: Event, title) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }
    if (this.isPc) {
      return;
    }
    const messageArr1 = [title];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  // yyyy年mm月dd日　hh時mi分
  dateform(date) {

    const year = moment(date, 'YYYYMMDDHHmmss').format('YYYY');
    const month = moment(date, 'YYYYMMDDHHmmss').format('MM');
    const day = moment(date, 'YYYYMMDDHHmmss').format('DD');
    const hour = moment(date, 'YYYYMMDDHHmmss').format('HH');
    const min = moment(date, 'YYYYMMDDHHmmss').format('mm');

    return year + '年' + month + '月' + day + '日  ' + hour + '時' + min + '分';

  }

  sortByDate(pro) {
    return (a, b) => {
      const value1 = a[pro];
      const value2 = b[pro];
      return value2 - value1;
    };
  }

}
