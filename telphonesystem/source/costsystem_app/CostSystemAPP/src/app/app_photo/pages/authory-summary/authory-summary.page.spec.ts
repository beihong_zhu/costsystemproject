import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AuthorySummaryPage } from './authory-summary.page';

describe('AuthorySummaryPage', () => {
  let component: AuthorySummaryPage;
  let fixture: ComponentFixture<AuthorySummaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorySummaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AuthorySummaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
