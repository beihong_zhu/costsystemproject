import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrdersmanagementPage } from './ordersmanagement.page';

describe('OrdersmanagementPage', () => {
  let component: OrdersmanagementPage;
  let fixture: ComponentFixture<OrdersmanagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersmanagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrdersmanagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
