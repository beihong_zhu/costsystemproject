export class GamenLable {
    buildingProjectName1?: string;
    buildingProjectName2?: string;
    photoTaken?: string;
    arrangement1?: string;
    arrangement2?: string;
    arrangement3?: string;
    arrangement4?: string;
    arrangement5?: string;
    arrangement6?: string;
    arrangement7?: string;
    arrangement8?: string;
    photoBookOrderGuide?: string;
    buildingInformation?: string;
    building?: string;
    floor?: string;
    room?: string;
    place?: string;
    processInformation?: string;
    largeProcess?: string;
    process?: string;
    work?: string;
    situation?: string;
    dateTimeInformation?: string;
    date?: string;
    time?: string;
    equipmentInformation?: string;
    unitMarkModelType?: string;
    machineNumber?: string;
    other?: string;
    witness?: string;
    constructionCompany?: string;
    freeColumn?: string;
    return?: string;
    moveOn?: string;
    returnArrow?: string;
    moveOnArrow?: string;
    items?: Item[];
    buildingItems?: string;
    floorItems?: string;
    roomItems?: string;
    placeItems?: string;
    largeProcessItems?: string;
    processItems?: string;
    workItems?: string;
    situationItems?: string;
    dateItems?: string;
    timeItems?: string;
    unitMarkModelTypeItems?: string;
    machineNumberItems?: string;
    otherItems?: string;
    witnesslItems?: string;
    constructionCompanyItems?: string;
    freeColumnItems?: string;
    layoutItems: LayOutItem[];
}

export class LayOutItem {
    item: number;
    name: string;
    oldItem: number;
}

export class Item {
    key: number;
    value: string;
}
