import { NgModule } from '@angular/core';
import { PhotobookViewSetPage } from './photobook-view-set.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [],
  declarations: [PhotobookViewSetPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotobookViewSetPage
      }
    ])
  ]
})
export class PhotobookViewSetModule { }
