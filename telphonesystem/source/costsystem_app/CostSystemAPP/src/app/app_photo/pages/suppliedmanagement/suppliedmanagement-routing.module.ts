import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuppliedmanagementPage } from './suppliedmanagement.page';

const routes: Routes = [
  {
    path: '',
    component: SuppliedmanagementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuppliedmanagementPageRoutingModule {}
