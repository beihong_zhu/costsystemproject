import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { PhotoSortSetComponent } from './photo-sort-set.component';


@NgModule({
  // declarations: [PhotoSortSetComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoSortSetComponent
      }
    ])
  ]
})
export class PhotoSortSetModule { }
