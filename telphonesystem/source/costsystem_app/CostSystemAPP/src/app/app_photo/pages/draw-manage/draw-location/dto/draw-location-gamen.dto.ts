import { DrawingInfoDto } from './drawing-info.dto';
import { PinDto } from './pin.dto';
import { DrawingDto } from './drawing.dto';


export declare interface DrawLocationGamenDto {
  // 図面情報
  drawingInfoDto?: DrawingInfoDto;
  // ピン情報
  pinInfo?: Array<PinDto>;
  // 選択された図面
  selectedDrawing?: DrawingDto;
  // 拡大縮小
  zoomSize?: number;

  // 機種名
  machineModel?: string;
  // ピン情報
  pdfPinInfo?: Array<PinDto>;
  // 選択された図面の連番
  selectedDrawingId?: string;

}
