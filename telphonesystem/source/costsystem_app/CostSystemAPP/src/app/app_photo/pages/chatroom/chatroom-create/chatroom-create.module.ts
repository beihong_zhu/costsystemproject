import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { ChatroomCreatePage } from './chatroom-create.page';

@NgModule({
  providers: [],
  declarations: [ChatroomCreatePage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatroomCreatePage
      }
    ])
  ]
})
export class ChatroomCreatePageModule { }
