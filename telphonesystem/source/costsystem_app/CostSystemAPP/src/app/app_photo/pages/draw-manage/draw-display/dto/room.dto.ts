import { PlaceDto } from '.';

export declare interface RoomDto {
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;

  places?: PlaceDto[];
}
