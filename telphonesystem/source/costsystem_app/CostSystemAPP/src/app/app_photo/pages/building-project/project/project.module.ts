import { NgModule } from '@angular/core';
import { ProjectPage } from './project.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [],
  declarations: [ProjectPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProjectPage
      }
    ])
  ]
})
export class ProjectModule { }
