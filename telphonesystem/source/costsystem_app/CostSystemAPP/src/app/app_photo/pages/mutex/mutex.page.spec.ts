import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MutexPage } from './mutex.page';

describe('MutexPage', () => {
  let component: MutexPage;
  let fixture: ComponentFixture<MutexPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MutexPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MutexPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
