import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppStateStoreService, CameraStoreService, PhotoDetailStoreService, HeadStateService, InitInterfaceType } from '@store';
import { LookType, LocalPhoto } from '../dto/photo-manage-dto';
import { RoutingService } from '@service/app-route.service';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { CD0005Constants, CD0001Constants } from '@constant/code';
import { PhotoListConf } from '../photo-list/photo-list-conf';
import * as moment from 'moment';
import { PopoverController, Platform } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { PhotoConf } from '@conf/photo-conf';
import { DomSanitizer } from '@angular/platform-browser';



@Component({
  selector: 'app-photo-list-item',
  templateUrl: './photo-list-item.component.html',
  styleUrls: ['./photo-list-item.component.scss', './photo-list-item.mobile.scss', './photo-list-item.winapp.scss'],
})
export class PhotoListItemComponent implements OnInit {
  @Input() selectPhotos: LocalPhoto[];

  @Input() photo: LocalPhoto;
  @Input() mulitype: string;
  @Input() bigProcess: LookType;
  @Input() isShowCheckBox: boolean;
  @Output() selectp = new EventEmitter<LocalPhoto>();
  @Output() goPhotoDetail = new EventEmitter<LocalPhoto>();
  @Input() isPhotoChange: boolean;

  platformtype: string;

  isPc: boolean;
  get disable(): boolean {
    return this.checkBoxDisable();
  }
  get notiming(): string {
    return PhotoConf.NOTIMING;
  }

  // 写真表示
  get isShowPhoto(): boolean {
    return this.photoPath !== undefined && this.photoPath !== null;
  }
  // server のicon
  get isShowServerPhotoIcon(): boolean {
    return this.photo.localPhotoFlag === PhotoListConf.SERVER_POTO && this.photoPath != null;
  }
  // 写真帳選択のicon
  get isShowPhotobookIcon(): boolean {
    return this.photo.isOutputPhotoBook === PhotoListConf.OUT_PUT_PHOTO_BOOK && this.photoPath != null;
  }
  // no image icon
  get isShowNoimageIcon(): boolean {
    return this.photoPath === undefined || this.photoPath === null;
  }

  get isSelect(): boolean {
    return this.selectPhotos.indexOf(this.photo) > -1;
  }
  get photoPath(): string {
    if (this.photo.photoPathLocal) {

      const filePath = this.file.dataDirectory + this.photo.photoPathLocal;


      return this.webview.convertFileSrc(filePath);

    } else {

      return this.photo.photoImage;

    }
  }

  cd0001Constants = CD0001Constants;

  get uploadFail(): boolean {
    return this.photo.retryCount && this.photo.retryCount >= PhotoConf.RETRY_MAX_COUNT;
  }

  constructor(
    private popoverController: PopoverController,
    private appStateService: AppStateStoreService,
    private cameraStoreService: CameraStoreService,
    public routingService: RoutingService,
    private photoDetailStoreService: PhotoDetailStoreService,
    private file: File,
    private webview: WebView,
    private headStateService: HeadStateService,
    private sanitizer: DomSanitizer,
    private platform: Platform
  ) {
    this.isPc = this.appStateService.appState.isPC;
    this.platformtype = 'photolistitem-' + this.appStateService.getPlatform();
  }

  ngOnInit() { }

  // checkbox 編集可能か
  checkBoxDisable() {
    // !(写真なし || !（（写真帳選択&&端末写真）||（端末&&手動動機&&server写真））)
    return !(this.photoPath != null
      && this.isShowCheckBox
      && !((this.mulitype === CD0005Constants.PHOTO_BOOK && this.photo.localPhotoFlag === PhotoListConf.LOCAL_POTO)
        || (this.isPc === false && this.mulitype === CD0005Constants.HAND_UPLOAD
          && this.photo.localPhotoFlag === PhotoListConf.SERVER_POTO)));
  }


  selectphoto() {
    this.selectp.emit(this.photo);
  }



  gotocamera() {
    if (!this.isPc) {
      this.cameraStoreService.setFromInitInterfaceType(InitInterfaceType.FromPhotoListNoIMage);
      this.cameraStoreService.setCurrentPhoto(this.photo);
      this.routingService.goRouteLink('/camera');
    }
  }

  imageclick(event) {
    event.stopPropagation();
    console.log('imageclick');

    if (this.isShowCheckBox) {
      // this.selectp(this.photo);
    } else {
      this.gotodetail(this.photo);
    }
  }


  gotodetail(photo) {
    console.log('detail list item', photo, this.photoDetailStoreService);
    this.goPhotoDetail.emit(photo);
  }

  async onPress(ev: Event) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress', ev);
    const messageArr1 = [this.photo.processName + ' - ' + this.photo.workName, this.photo.modeltype];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


  dateform() {

    if (this.photo.photoDate === undefined || this.photo.photoDate === null || this.photo.photoDate.length === 0) {
      return '';
    } else {
      const date = moment(this.photo.photoDate, 'YYYYMMDDHHmmss');
      if (date === undefined || date === null) {
        return '';
      } else {
        return date.format('YYYY/MM/DD');
      }

    }


  }

}
