import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppStateStoreService } from '@store';
import { LookType } from '../dto/photo-manage-dto';
import { CD0001Constants, CD0007Constants } from '@constant/code';
import { S0501Constants } from '../../../constant/constant.photo';
import { from } from 'rxjs';

@Component({
  selector: 'app-photo-show-list',
  templateUrl: './photo-show-list.component.html',
  styleUrls: ['./photo-show-list.component.scss', './photo-show-list.mobile.scss', './photo-show-list.winapp.scss'],
})
export class PhotoShowListComponent implements OnInit {


  // public image = S0501Constants.IMAGE;
  public image = '';

  public date = S0501Constants.DATE;
  public machine = S0501Constants.MACHINE;
  public processwork = S0501Constants.PROCESSWORK;
  public timing = S0501Constants.TIMING;

  @Input() selectPhotos: any[];
  @Input() isPhotoChange: boolean;

  @Input() datas: any[];
  @Input() sortby: string;
  @Input() mulitype: string;
  @Input() bigProcess: LookType;
  @Input() isShowCheckBox: boolean;
  @Output() selectp = new EventEmitter<any>();
  @Output() goPhotoDetail = new EventEmitter<any>();
  isPc: boolean;
  cd0001Constants = CD0001Constants;
  cd0007Constants = CD0007Constants;
  platformtype: string;
  constructor(
    private appStateService: AppStateStoreService
  ) {
    this.isPc = this.appStateService.appState.isPC ? true : false;
    this.platformtype = 'photolist-' + this.appStateService.getPlatform();
  }

  ngOnInit() {
  }

  selectphoto(ph) {
    this.selectp.emit(ph);
  }
  goPhotoDetailpage(ph) {
    this.goPhotoDetail.emit(ph);
  }

  myitemHeight = (item, index) => {

    let headH = this.isPc ? 96 : 60;
    if (this.sortby && this.sortby === this.cd0007Constants.DATE) {
      headH = 0;
    }

    const count = (item as any[]).length;

    return count * (this.isPc ? 52 : 52 * (document.documentElement.clientWidth / 375)) + headH;
  }

  myHeaderFn = (item, index, items) => {
    if (index === 0) {
      return this.headerTitle(item[0]);
    } else {
      const title = this.headerTitle(items[index - 1][0]);

      const title1 = this.headerTitle(item[0]);
      if (title !== title1) {
        return title1;
      }
    }
    return null;
  }

  headerTitle(photo) {
    return (photo.processName === null || photo.processId === null || photo.workName === null
      || photo.workId === null) ? '工程・作業名(不明)' : photo.processName + ' - ' + photo.workName;
  }

}
