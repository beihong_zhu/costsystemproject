import { LookType, LocalPhoto, SelectTitle } from '../dto/photo-manage-dto';
import { CD0007Constants, CD0001Constants } from '@constant/code';
import * as moment from 'moment';
import * as lodash from 'lodash';

export class PhotoListComponentHelp {


  // 工程作業 sort
  groupByprocess(photos: Array<LocalPhoto>): LocalPhoto[][] {


    // machine不明
    const unMachineP = photos.filter((ph) => ph.modeltype === null || ph.unitMark === null);

    const machineP = photos.filter((ph) => ph.modeltype != null && ph.unitMark != null);

    const groupvalue = lodash.groupBy(machineP, (photo) => photo.unitMark);

    const result: LocalPhoto[][] = lodash.map(groupvalue);

    if (unMachineP.length > 0) {
      result.push(unMachineP);
    }

    return result;

  }
  // 機器sort
  groupBymachine(photos: Array<LocalPhoto>): any {


    // process不明
    const unProcesseP = photos.filter((ph) => ph.processId === null
      || ph.processName === null || ph.workId === null || ph.workName === null);

    const processeP = photos.filter((ph) => ph.processId != null && ph.processName != null && ph.workId != null && ph.workName != null);

    const groupvalue = lodash.groupBy(processeP, (photo) => photo.processOrder.toString() + photo.workOrder.toString());

    const result: any[] = lodash.map(groupvalue);

    if (unProcesseP.length > 0) {
      result.push(unProcesseP);
    }

    return result;

  }

  // 日付sort
  groupBydate(titles: SelectTitle[], photos: Array<LocalPhoto>): any {

    const groupvalue = lodash.groupBy(photos, (photo) => moment(photo.photoDate.toString(), 'YYYYMMDDHHmmss').format('YYYY/MM/DD'));

    const result = new Map();
    let id = 1;
    lodash.forEach(groupvalue, (value, key) => {
      const selectTitle: SelectTitle = {};
      selectTitle.title = key;
      selectTitle.id = id++;
      titles.push(selectTitle);
      result.set(JSON.stringify(selectTitle), value);
    });



    return result;

  }

  groupBydateCount(photos: any, count) {
    const iconsArr = [];
    photos.forEach((item, index) => {
      const page = Math.floor(index / count);
      if (!iconsArr[page]) {
        iconsArr[page] = [];
      }
      iconsArr[page].push(item);
    });
    return iconsArr;
  }


  // floor sort

  groupByfloor(photos: Array<LocalPhoto>): any {

    // process不明
    const unProcesseP = photos.filter((ph) => ph.processId === null
      || ph.processName === null || ph.workId === null || ph.workName === null);
    const processeP = photos.filter((ph) => ph.processId != null && ph.processName != null && ph.workId != null && ph.workName != null);
    const groupvalue = lodash.groupBy(processeP, (photo) => photo.processOrder.toString() + photo.workOrder.toString());

    const resultPro: any[] = lodash.map(groupvalue);

    if (unProcesseP.length > 0) {
      resultPro.push(unProcesseP);
    }

    const arr = [];
    for (const item of resultPro) {

      // machine不明
      const unMachineP = item.filter((ph) => ph.modeltype === null || ph.unitMark === null);
      const machineP = item.filter((ph) => ph.modeltype != null && ph.unitMark != null);
      const groupvalueMachine = lodash.groupBy(machineP, (photo) => photo.unitMark);

      const result: any[] = lodash.map(groupvalueMachine);

      if (unMachineP.length > 0) {
        result.push(unMachineP);
      }
      result.forEach((value, i) => {
        arr.push(value);
      });
    }


    return arr;

  }




  // .簡易写真一覧画面表示

  simplePhoto(photos: Array<LocalPhoto>): any {
    const tempmap = new Map();
    const titles: string[] = [];

    for (const photo of photos) {
      const title = photo.processId.toString() + photo.workId.toString();
      if (titles.indexOf(title) === -1) {
        titles.push(title);
        tempmap.set(title, []);
      }

      tempmap.get(title).push(photo);

    }

    const arr: any[] = [];

    tempmap.forEach((value, key) => {
      arr.push(value);
    });

    return arr;
  }




  // needPhotoList takePhotoList結合，photosAll返す
  combineNeedPhotoList(bigProcessId, needPhotoList, takePhotoList) {

    // noImageList 写真撮るべきまだ採ってない写真リスト
    const noImageList = needPhotoList.filter((neePhoto) => {

      // 写真撮るべきまだ採ってない写真 flg
      let needAndHasTakeFlg = false;
      needAndHasTakeFlg = takePhotoList.some((takePhoto) => {
        if (takePhoto.floorId === neePhoto.floorId &&
          takePhoto.roomId === neePhoto.roomId &&
          takePhoto.placeId === neePhoto.placeId &&
          takePhoto.machineId === neePhoto.machineId &&
          takePhoto.workId === neePhoto.workId &&
          takePhoto.processId === neePhoto.processId &&
          takePhoto.timingId === neePhoto.timingId) {
          return true;
        }

      });

      // 写真撮るべきまだ採ってない写真 flg === false
      if (!needAndHasTakeFlg) {
        // 写真撮るべきまだ採ってない写真
        return neePhoto;
      }
    });
    const photosAll = bigProcessId === CD0001Constants.CD0001_04 ? takePhotoList.concat(needPhotoList) : takePhotoList.concat(noImageList);
    return photosAll;

  }



  pcWidth_count() {
    const dom = document.getElementById('photo-div');
    if (dom) {
      const w = dom.offsetWidth - 130 - 160;
      const count = Math.floor((w + 10) / 302);
      console.log('pcWidth_count' + w + '----------' + count);

      return count;
    }
    return 3;
  }


}


