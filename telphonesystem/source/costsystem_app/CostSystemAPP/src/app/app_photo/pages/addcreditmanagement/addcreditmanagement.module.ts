import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddcreditmanagementPageRoutingModule } from './addcreditmanagement-routing.module';

import { AddcreditmanagementPage } from './addcreditmanagement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddcreditmanagementPageRoutingModule
  ],
  declarations: [AddcreditmanagementPage]
})
export class AddcreditmanagementPageModule {}
