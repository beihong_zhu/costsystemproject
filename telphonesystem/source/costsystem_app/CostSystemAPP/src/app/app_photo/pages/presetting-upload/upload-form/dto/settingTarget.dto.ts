export class SettingTargetDto {
  // 設定対象ID
  settingTargetId?: string;
  // 設定対象名
  settingTargetName?: string;
}
