import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'text-popover',
  templateUrl: './text-popover.component.html',
  styleUrls: ['./text-popover.component.scss'],
})
export class TextPopoverComponent implements OnInit {

  messageArr: string[];

  constructor() {
  }

  ngOnInit() {
    console.log('messageArr', this.messageArr);
  }

}
