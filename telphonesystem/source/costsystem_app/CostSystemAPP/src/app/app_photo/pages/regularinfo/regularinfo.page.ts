import { Component, OnInit } from '@angular/core';
import { CommonConstants, HeaderConstants, S1501Constants } from '@constant/constant.photo';
import { API0102Service } from '@service/api';
import { API0102InDto, API0102OutDto } from '@service/dto';

import { SysUserInfoModel } from '@service/dto/api-01-02-out-dto';
import { AppState, CommonStore } from '@store';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysUserDto } from './dto';

@Component({
  selector: 'app-regularinfo',
  templateUrl: './regularinfo.page.html',
  styleUrls: ['./regularinfo.page.scss'],
})
export class RegularinfoPage implements OnInit {

  private appState: AppState;
  private commonStore: CommonStore;
  public employeeId: string;
  public employeeName: string;
  public sysUserList: Array<SysUserDto> = new Array<SysUserDto>();

  // title
  public title: string = S1501Constants.TITLE;
  // id
  public userIdLabel: string = S1501Constants.USERIDLABEL;
  // name
  public userNameLabel: string = S1501Constants.USERNAMELABEL;
  // space
  public space: string = '           ';
  // search
  public search: string = S1501Constants.SEARCH;
  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;

  // user list header name
  userListHead: string[] = [
    S1501Constants.USERID,
    S1501Constants.USERNAME,
    S1501Constants.COMPANYNAME,
    S1501Constants.AUTHORITYID,
    S1501Constants.MAILADDRESS,
    S1501Constants.ISLOGIN
  ];

  // header title
  public headerTitle: string = HeaderConstants.TITLE;

  constructor(
    private api0102Service: API0102Service
  ) {

  }

  ngOnInit() {

  }

  public onSearch() {
    this.api0102ServicePostExec();
  }

  private get0102InDto() {
    let api0102InDto: API0102InDto = {};
    api0102InDto.userId = this.employeeId;
    api0102InDto.userName = this.employeeName;


    return api0102InDto;
  }

  private api0102ServicePostExec() {
    // // 写真帳出力情報取得
    // this.api0102Service.postExec(this.get0102InDto(), (outDto: API0102OutDto) => {
    //   this.api01f02successed(outDto);
    // },
    //   // fault
    //   () => {
    //   });

    // 写真帳出力情報取得
    this.api0102Service.postExecNew(this.get0102InDto()).then((res) => {
      // response の型は any ではなく class で型を定義した方が良いが
      // ここでは簡便さから any としておく

      // @angular/http では json() でパースする必要があったが､ @angular/common/http では不要となった
      //const response: any = res.json();
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api0102successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api0102successed(outDto: API0102OutDto) {
    this.sysUserList = new Array<SysUserDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.sysUserInfoModels));

    for (const sysUserInfoModel of loadSysusers) {
      let sysUserDto: SysUserDto = new SysUserDto();
      sysUserDto.userId = sysUserInfoModel.userId;
      sysUserDto.userName = sysUserInfoModel.userName;
      sysUserDto.companyName = sysUserInfoModel.companyName;
      sysUserDto.authorityId = sysUserInfoModel.authorityId;
      sysUserDto.mailAddress = sysUserInfoModel.mailAddress;
      sysUserDto.isLogin = sysUserInfoModel.isLogin;

      this.sysUserList.push(sysUserDto);
    }
  }
}
