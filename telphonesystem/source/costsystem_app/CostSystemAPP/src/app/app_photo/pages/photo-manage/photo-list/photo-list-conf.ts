
export class PhotoListConf {

  /** 写真一覧、選択モードの複数写真選択するパターン */
  /** 0:選択なしの状態 */
  static readonly NO_SELECT: string = '0';
  static readonly NO_SELECT_VALUE: string = '複数選択';

  /** ローカル写真 */
  static readonly LOCAL_POTO: number = 1;
  /** サーバの写真 */
  static readonly SERVER_POTO: number = 0;
  /** defaultリトライ回数 */
  static readonly RETRY_DEFAULT: number = 0;

  /** 写真帳写真 */
  static readonly OUT_PUT_PHOTO_BOOK: number = 1;

  /** 最大100枚まで選択 */
  static readonly MAX_PHOTO_COUNT: number = 100;

  static readonly PC_COUNT: number = 6;
  static readonly MOBILE_COUNT: number = 3;
}

