import * as csvCheckInfo from './csvCheckInfo.json';
import { MessageConstants } from '@constant/message-constants';
import { CD0014Constants } from '@constant/code';
import { AuthConfig } from '../../../../app_common/constant/common';
import { TextCheck } from '../../../util/text-check';


const checkInfos = csvCheckInfo.default;
const ANKEN_CSV_TYPE = CD0014Constants.ANKEN_CSV_TYPE;
const CSV_TYPE = CD0014Constants.CSV_TYPE;

export class CsvCheckRes {
  resultCode?: string;
  errorMsg?: string;
  csvData?: {};
  bukenCsvData?: {
    anken?: string;
    userKanli?: string;
    bukenInfo?: string;
    machineInfo?: string;
    projInfo?: string;
    peopleInfo?: string;
    blackProjInfo?: string;
  };
  pdfNames?: string;
}

export class CsvCheckInfo {
  title?: string;
  titcolcnt?: number;
  datacolcnt?: number;
  datatype?: [{}];
}

const bukenCsvData = {
  anken: '',
  userKanli: '',
  bukenInfo: '',
  machineInfo: '',
  projInfo: '',
  peopleInfo: '',
  blackProjInfo: '',
};

const res = new CsvCheckRes();
let csvTypeArr = [];

export class CSVUtil {

  // 数値チェックサムルール
  private static readonly regCheckNum = /^[0-9]*$/;
  // 電話番号
  private static readonly regCheckPhone = /^[0-9]{10,11}$/;
  // メールアドレス
  private static readonly regCheckEmail = /^[a-z0-9][\.a-zA-Z0-9_-]{1,62}@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]{1,63})+$/;

  // 半角チェックサムルール
  private static regCheckHalf(str: string) {
    const cArr = str.match(/[^\x00-\xff|\uff61-\uff9f]/ig);
    return (cArr == null ? true : false);
  }

  private static regCheckUseSpace(str: string) {
    return str.trim().indexOf(' ') === -1;
  }

  // 全角チェックサムルール
  private static regCheckFull(str: string) {
    const cArr = str.match(/[^\x00-\xff|\uff61-\uff9f]/ig) ? str.match(/[^\x00-\xff|\uff61-\uff9f]/ig).length : 0;
    return (cArr === str.length ? true : false);
  }

  // 数字のみの文字列
  private static regCheckHalfNumber(str: string) {
    const half = this.regCheckHalf(str);
    const num = CSVUtil.regCheckNum.test(str);
    return half && num;
  }

  // 電話番号
  private static regCheckHalfPhone(str: string) {
    const half = this.regCheckHalf(str);
    let num = true;
    if (str.length === 0) {
      num = true;
    } else {
      if (str.length > 13) {
        num = false;
      } else if (str[0] !== '0') {
        num = false;
      } else if (str[1] === '0') {
        num = false;
      } else {
        num = this.regCheckPhone.test(str.replace(/-/g, ''));
      }
    }
    return half && num;
  }

  // メールアドレス
  private static regCheckHalfEmail(str: string) {
    // if (str.length === 0) {
    //   return true;
    // } else if (str.length > 90) {
    //   return false;
    // }
    if (str.length === 0) {
      return true;
    } else if (str.length > 256) {
      return false;
    }
    return CSVUtil.regCheckEmail.test(str);
  }

  // パスワード
  private static regCheckPwd(str: string) {
    return AuthConfig.PWD_ALLOWED_CHAR_PRESETTING.test(str);
  }

  /**
   * @param csvTypeId csvのid
   * @param csvLines csvファイルのデータ
   * @param ankenCsvTypeId 案件csvのid
   */
  static checkEachCsv(csvTypeId: string, csvLines: string[], ankenCsvTypeId: number, pdfNamesArr?: string[]) {

    let checkInfo: CsvCheckInfo;

    const csvStoreData = [];
    let allRowNum = 0;
    // let csvLinesLen: number = csvLines.length > 202 ? 202 : csvLines.length;
    const csvLinesLen: number = csvLines.length;

    if (csvTypeId === '3') {
      // tslint:disable-next-line: no-string-literal
      checkInfo = checkInfos[ANKEN_CSV_TYPE[ankenCsvTypeId]['CHECK_INFO']];
      for (let i = 0; i < ankenCsvTypeId; i++) {
        allRowNum += csvTypeArr[i].length;
      }
    } else {
      checkInfo = checkInfos[CSV_TYPE[csvTypeId]];
    }

    const titlecode = checkInfo.title.split(',')[0];
    const colsTitleNotEmpty: string[] = csvLines[1].split(',');
    if (this.checkCsvBlank(csvLinesLen, csvLines, titlecode, allRowNum, colsTitleNotEmpty)) {
      return res;
    }
    for (let idx = 0; idx < csvLinesLen; idx++) {
      const cols: string[] = csvLines[idx].split(',').map((s: string) => {
        return s.trim();
      });
      const colsTitle: string[] = csvLines[1].split(',');
      // CSVファイル1行目と登録対象で選択した対象が一致しているかチェックを行う。
      // 一致していない場合エラーメッセージ(E00030)を出力し処理を中断する。
      if (idx === 0 && String(csvLines[idx]).trim() !== checkInfo.title) {
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00030;
        return res;
      }
      // CSVファイル2行目の項目数が対象の項目数と一致しているかチェックを行う。
      // 一致していない場合エラーメッセージ(E00031)を出力し処理を中断する。
      if (idx === 1 && cols.length !== checkInfo.titcolcnt) {
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00031;
        return res;
      }

      if (idx >= 2) {
        // 実行行数を計算
        if (idx === csvLinesLen - 1 && cols.length === 1 && cols[0] === '') {
          break;
        }
        const csvRowData = {};
        let rowNum = idx + 1;
        if (ankenCsvTypeId > 0) {
          for (let i = 0; i < ankenCsvTypeId; i++) {
            rowNum += csvTypeArr[i].length;
          }
        }

        // 入力項目の列数とCSVファイル2行目の列数が一致しているかチェックを行う。
        // 一致していない場合エラーメッセージ(E00032)を出力し処理を中断する。
        if (cols.length !== checkInfo.datacolcnt) {
          res.resultCode = '-1';
          res.errorMsg = MessageConstants.E00032.replace('{{value}}', rowNum.toString());
          return res;
        } else {
          // 単項目エラーチェック
          const datatype = checkInfo.datatype;
          for (let idxType = 0; idxType < cols.length; idxType++) {
            const datatypeArr = datatype[idxType][idxType + ''].split('|');
            if (this.checkSingleItemDatatype0And2(datatypeArr, cols, idxType, rowNum, colsTitle)) {
              return res;
            }
            if (this.checkSingleItemDatatype1(datatypeArr, cols, idxType, rowNum, colsTitle)) {
              return res;
            }
            /** {{value1}}行目:{{value2}}特殊な文字は使用できません。 */
            if (cols[idxType] && !TextCheck.textCheck(cols[idxType])) {
              res.resultCode = '-1';
              res.errorMsg = rowNum.toString() + '行目:' + colsTitle[idxType] + MessageConstants.E00008;
              return res;
            }
            if (csvTypeId !== '3') {
              const eachKey = CD0014Constants.CSV_ATTR[csvTypeId][idxType];
              csvRowData[eachKey] = cols[idxType];
            } else {
              const eachKey = CD0014Constants.CSV_ATTR[csvTypeId][ankenCsvTypeId][idxType];
              csvRowData[eachKey] = cols[idxType];
            }
          }
          // 物件フロア・部屋・場所CSV登録の場合、登録区分の妥当性チェック
          if (titlecode === '3') {
            if (this.checkCommonCsvCols2And5(cols, rowNum, colsTitle) || this.checkCommonCsvCols8(cols, rowNum, colsTitle)) {
              return res;
            }
          }
          if (this.checkProjectCsv(cols, rowNum, ankenCsvTypeId, colsTitle)) {
            return res;
          }
        }
        csvStoreData.push(csvRowData);
      }
    }
    res.resultCode = '1';
    // 一致していない場合エラーメッセージ(E00031)を出力し処理を中断する。
    res.errorMsg = '';
    res.csvData = csvStoreData;
    return res;
  }

  private static checkCsvBlank(csvLinesLen: number, csvLines: string[], titlecode: string, allRowNum: number, colsTitleNotEmpty: string[]) {
    let fieldCode: number;
    if (csvLinesLen < 3) {
      // 案件情報CSVの5ユーザ担当管理CSV、6物件図面情報CSV、7機器情報CSV、8工程作業情報CSV、9立会人情報CSVは行が空を許可するような変更
      if (!csvLines[2] && (titlecode === '10' || titlecode === '4' || titlecode === '1' || titlecode === '2' || titlecode === '3')) {
        if (titlecode === '10') {
          fieldCode = 0;
        } else if (titlecode === '4') {
          fieldCode = 1;
        } else if (titlecode === '1') {
          fieldCode = 0;
        } else if (titlecode === '2') {
          fieldCode = 0;
        } else if (titlecode === '3') {
          fieldCode = 0;
        }
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00066.replace('{{value1}}',
          allRowNum + 3 + '').replace('{{value2}}', colsTitleNotEmpty[fieldCode]);
        return res;
      }
    } else {
      // 案件情報CSVの5ユーザ担当管理CSV、6物件図面情報CSV、7機器情報CSV、8工程作業情報CSV、9立会人情報CSVは行が空を許可するような変更
      if (csvLinesLen === 3 && !csvLines[2] && (titlecode === '10'
        || titlecode === '4' || titlecode === '1' || titlecode === '2' || titlecode === '3')) {
        if (titlecode === '10') {
          fieldCode = 0;
        } else if (titlecode === '4') {
          fieldCode = 1;
        } else if (titlecode === '1') {
          fieldCode = 0;
        } else if (titlecode === '2') {
          fieldCode = 0;
        } else if (titlecode === '3') {
          fieldCode = 0;
        }
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00066.replace('{{value1}}',
          allRowNum + 3 + '').replace('{{value2}}', colsTitleNotEmpty[fieldCode]);
        return res;
      }
    }
    return null;
  }

  private static checkSingleItemDatatype0And2(datatypeArr: string[], cols: string[], idxType: number, rowNum: number, colsTitle: string[]) {
    // let replaceValue=rowNum+'行目:'+colsTitle[idxType]+'の値'
    // 必須項目が入力されていない場合エラーメッセージ(E00033)を出力し処理を中断する。
    if (datatypeArr[0] === 'require' && !cols[idxType]) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    if (datatypeArr[0] === 'somerequire' && (cols[idxType - 1] === '2' || cols[idxType - 1] === '3') && !cols[idxType]) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    // 既定の文字数と異なっている場合エラーメッセージ(E00007)を出力し処理を中断する。
    if (Number(datatypeArr[2]) < cols[idxType].length) {
      res.resultCode = '-1';
      // 一致していない場合エラーメッセージ(E00067)を出力し処理を中断する。
      res.errorMsg = MessageConstants.E00067.replace('{{value1}}', rowNum.toString())
        .replace('{{value2}}', colsTitle[idxType]).replace('{{value3}}', datatypeArr[2]);
      return res;
    }
    return null;
  }

  private static checkSingleItemDatatype1(datatypeArr: string[], cols: string[], idxType: number, rowNum: number, colsTitle: string[]) {
    // 型が規定と会わない場合エラーメッセージ(E00065)を出力し処理を中断する。
    if ((datatypeArr[1] === 'half' || datatypeArr[1] === 'halfspace') && cols[idxType] && !this.regCheckHalf(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    if (datatypeArr[1] === 'halfspace' && cols[idxType] && !this.regCheckUseSpace(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00130.replace('{{value1}}', rowNum.toString());
      return res;
    }
    // 型が規定と会わない場合エラーメッセージ(E00065)を出力し処理を中断する。
    if (datatypeArr[1] === 'full' && cols[idxType] && !this.regCheckFull(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    // 型が規定と会わない場合エラーメッセージ(E00065)を出力し処理を中断する。
    if (datatypeArr[1] === 'number' && cols[idxType] && !this.regCheckNum.test(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    if (datatypeArr[1] === 'half-number' && cols[idxType] && !this.regCheckHalfNumber(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    if (datatypeArr[1] === 'half-phone' && cols[idxType] && !this.regCheckHalfPhone(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    if (datatypeArr[1] === 'pwd' && cols[idxType] && !this.regCheckPwd(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}',
        rowNum.toString()).replace('{{value2}}', colsTitle[idxType]) + MessageConstants.E00115;
      return res;
    }
    if (datatypeArr[1] === 'email' && cols[idxType] && !this.regCheckHalfEmail(cols[idxType])) {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00065.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[idxType]);
      return res;
    }
    return null;
  }

  private static checkCommonCsvCols2And5(cols: string[], rowNum: number, colsTitle: string[]): CsvCheckRes {
    // フロア登録区分の登録妥当性チェック、「2」更新の場合
    if (cols[2] === '2' && cols[3] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
      return res;
    }
    // フロア登録区分の登録妥当性チェック、「3」削除の場合
    if (cols[2] === '3' && cols[3] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
      return res;
    }
    // 部屋登録区分の登録妥当性チェック、「2」更新の場合
    if (cols[5] === '2' && cols[6] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[6]);
      return res;
    }
    // 部屋登録区分の登録妥当性チェック、「3」削除の場合
    if (cols[5] === '3' && cols[6] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[6]);
      return res;
    }
    // 部屋登録区分に関するフロアIDの存在チェック、「2」更新の場合
    if (cols[5] === '2' && cols[3] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
      return res;
    }
    // 部屋登録区分に関するフロアIDの存在チェック、「3」削除の場合
    if (cols[5] === '3' && cols[3] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
      return res;
    }
    // 部屋登録区分に関する部屋名の存在チェック、「1」登録の場合
    if (cols[5] === '1' && cols[7] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[7]);
      return res;
    }
    // 部屋登録区分に関する部屋名の存在チェック、「2」更新の場合
    if (cols[5] === '2' && cols[7] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[7]);
      return res;
    }
    return null;
  }

  private static checkCommonCsvCols8(cols: string[], rowNum: number, colsTitle: string[]): CsvCheckRes {
    // 場所登録区分の登録妥当性チェック、「2」更新の場合
    if (cols[8] === '2' && cols[9] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[9]);
      return res;
    }
    // 場所登録区分の登録妥当性チェック、「3」削除の場合
    if (cols[8] === '3' && cols[9] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[9]);
      return res;
    }
    // 場所登録区分に関するフロアIDの存在チェック、「2」更新の場合
    if (cols[8] === '2' && cols[3] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
      return res;
    }
    // 場所登録区分に関するフロアIDの存在チェック、「3」削除の場合
    if (cols[8] === '3' && cols[3] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
      return res;
    }
    // 場所登録区分に関する部屋IDの存在チェック、「2」更新の場合
    if (cols[8] === '2' && cols[6] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[6]);
      return res;
    }
    // 場所登録区分に関する部屋IDの存在チェック、「3」削除の場合
    if (cols[8] === '3' && cols[6] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[6]);
      return res;
    }
    // 場所登録区分に関する場所名の存在チェック、「1」登録の場合
    if (cols[8] === '1' && cols[10] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[10]);
      return res;
    }
    // 場所登録区分に関する場所名の存在チェック、「2」更新の場合
    if (cols[8] === '2' && cols[10] === '') {
      res.resultCode = '-1';
      res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[10]);
      return res;
    }
    return null;
  }

  private static checkProjectCsv(cols: string[], rowNum: number, ankenCsvTypeId: number, colsTitle: string[]): CsvCheckRes {

    // 案件CSV登録の場合、IDの妥当性チェック、機器情報CSV
    if (ankenCsvTypeId === 3) {
      // 室外機機種名、室内機機種名のどちらか一方のみを入力してください
      if ((!cols[3] && !cols[4]) || (cols[3] && cols[4])) {
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00069;
        return res;
      }
      // 部屋IDと場所IDの妥当性チェック
      if (cols[7] !== '' && cols[6] === '') {
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[6]);
        return res;
      }
    }
    // 案件CSV登録の場合、IDの妥当性チェック、工程作業情報CSV
    if (ankenCsvTypeId === 4) {
      // 工程IDと作業IDの妥当性チェック
      if (cols[3] !== '' && cols[1] === '') {
        res.resultCode = '-1';
        res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[1]);
        return res;
      }
    }
    // 案件CSV登録の場合、登録区分の妥当性チェック、立会人情報CSV
    if (ankenCsvTypeId === 5) {
      // 立会人登録区分の登録妥当性チェック、「2」更新の場合
      if (cols[0] === '2' && (cols[1] === '' || cols[3] === '')) {
        res.resultCode = '-1';
        if(cols[1] === ''){
          res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[1]);
        } else if (cols[3] === ''){
          res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
        }
        return res;
      }
      // 立会人登録区分の登録妥当性チェック、「3」削除の場合
      if (cols[0] === '3' && (cols[1] === '' || cols[3] === '')) {
        res.resultCode = '-1';
        if(cols[1] === ''){
          res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[1]);
        } else if (cols[3] === ''){
          res.errorMsg = MessageConstants.E00066.replace('{{value1}}', rowNum.toString()).replace('{{value2}}', colsTitle[3]);
        }
        return res;
      }
    }
    return null;
  }

  static check(csvTypeId: string, csvDataTxt: string, pdfNamesCheck?: string): CsvCheckRes {
    const csvLines: string[] = csvDataTxt.split('\r\n');
    if (csvTypeId !== '3') {
      if (csvLines.length === 20003) {
        if (csvLines[csvLines.length - 1]) {
          res.resultCode = '-1';
          // 登録可能な件数は20,000件でになります。超過する場合はファイルを分けて登録してください。
          res.errorMsg = MessageConstants.W00005;
          return res;
        }
      } else {
        if (csvLines.length > 20002) {
          res.resultCode = '-1';
          // 登録可能な件数は20,000件でになります。超過する場合はファイルを分けて登録してください。
          res.errorMsg = MessageConstants.W00005;
          return res;
        }
      }
      // return
      return this.checkEachCsv(csvTypeId, csvLines, 0);
    } else {
      if (csvLines.length === 20015) {
        if (csvLines[csvLines.length - 1]) {
          res.resultCode = '-1';
          // 登録可能な件数は20,000件でになります。超過する場合はファイルを分けて登録してください。
          res.errorMsg = MessageConstants.W00005;
          return res;
        }
      } else {
        if (csvLines.length > 20014) {
          res.resultCode = '-1';
          // 登録可能な件数は20,000件でになります。超過する場合はファイルを分けて登録してください。
          res.errorMsg = MessageConstants.W00005;
          return res;
        }
      }
      const csvTypeIndex = [];
      for (const ast of ANKEN_CSV_TYPE) {
        // tslint:disable-next-line: no-string-literal
        const element = ast['VALUE'];
        if (!csvLines.includes(element)) {
          res.resultCode = '-1';
          // 一致していない場合エラーメッセージ(E00031)を出力し処理を中断する。
          res.errorMsg = MessageConstants.E00030;
          return res;
        } else {
          csvTypeIndex.push(csvLines.indexOf(element));
        }
      }

      // PDFに関する妥当性チェック BEGIN
      csvTypeArr = [];
      for (let i = 0; i < csvTypeIndex.length; i++) {
        csvTypeArr.push(csvLines.slice(csvTypeIndex[i], csvTypeIndex[i + 1]));
      }
      // console.log(csvTypeArr[2])
      const bukenCsvDataArr = Object.keys(bukenCsvData);
      let pdfNamesArrCheck = null;
      if (pdfNamesCheck !== null && pdfNamesCheck !== '') {
        pdfNamesArrCheck = pdfNamesCheck.split(',');
      }
      const contentLen = csvTypeArr[2].length;
      let num = 0;
      res.pdfNames = '';
      for (let i = 2; i < contentLen; i++) {
        const item = csvTypeArr[2][i].split(',');
        // 指定したPDFファイルの存在チェック
        if (item[0] === '1' || item[0] === '2') {
          num++;
          if (pdfNamesArrCheck !== null && pdfNamesArrCheck !== '') {
            if (!pdfNamesArrCheck.includes(item[2].toUpperCase())) {
              res.resultCode = '-1';
              // PDFファイルが存在しない
              res.errorMsg = MessageConstants.E00072;
              return res;
            }
          } else {
            res.resultCode = '-1';
            // PDFファイルが存在しない
            res.errorMsg = MessageConstants.E00072;
            return res;
          }
          res.pdfNames += item[2] + ',';
        }
      }
      res.pdfNames = res.pdfNames.substring(0, res.pdfNames.length - 1);
      // PDF件数チェック
      if (num > 0) {
        if (pdfNamesCheck !== null && pdfNamesCheck !== '' && num > pdfNamesArrCheck.length) {
          res.resultCode = '-1';
          // PDF件数が不正
          res.errorMsg = MessageConstants.E00073;
          return res;
        } else if (pdfNamesCheck === null || pdfNamesCheck === '') {
          res.resultCode = '-1';
          // PDF件数が不正
          res.errorMsg = MessageConstants.E00073;
          return res;
        }
      }
      // PDFに関する妥当性チェック END
      for (let i = 0; i < csvTypeArr.length; i++) {
        const csvLines1 = csvTypeArr[i];
        const resItem = this.checkEachCsv(csvTypeId, csvLines1, i, pdfNamesArrCheck);
        if (resItem && resItem.resultCode === '-1') {
          return res;
        }
        if (resItem && resItem.resultCode === '1') {
          bukenCsvData[bukenCsvDataArr[i]] = resItem.csvData;
        }
      }
    }
    res.resultCode = '1';
    // 一致していない場合エラーメッセージ(E00031)を出力し処理を中断する。
    res.errorMsg = '';
    res.bukenCsvData = bukenCsvData;
    return res;
  }
}
