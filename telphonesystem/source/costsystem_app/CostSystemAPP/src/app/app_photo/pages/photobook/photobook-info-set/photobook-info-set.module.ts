import { NgModule } from '@angular/core';
import { PhotobookInfoSetPage } from './photobook-info-set.page';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [],
  declarations: [PhotobookInfoSetPage],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotobookInfoSetPage
      }
    ])
  ]
})
export class PhotobookInfoSetModule { }
