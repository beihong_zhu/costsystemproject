import { Component, OnInit } from '@angular/core';
import { AppState } from '../../store/app-state/app-state.store';
import { CommonStore } from '../../store/common/common.store';
import { API0104Service } from '@service/api';
import { API0104InDto, API0104OutDto } from '@service/dto';

import { SysAgentInfoModel } from '@service/dto/api-01-04-out-dto';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';
import { SysAgentInfoDto } from './dto/sysagentinfo.dto';


@Component({
  selector: 'app-newagency',
  templateUrl: './newagency.page.html',
  styleUrls: ['./newagency.page.scss'],
})
export class NewagencyPage implements OnInit {

  private appState: AppState;
  private commonStore: CommonStore;
  public AgencyId :string;
  public AgencyName: string;
  public AgencyAbbreviation: string;
  public AgencyTelephone: string;
  public AgencyMail:string;
  public AgentAccount: string;
  public AgentType: string;
  public BussinessMode:string;

  public sysAgentInfoList: Array<SysAgentInfoDto> = new Array<SysAgentInfoDto>();


  public  agencyInfoList:any = {
  
   provinceList:['北京','天津','上海','重庆','黑龙江','吉林','辽宁','内蒙古','江西 ','河南','湖南','湖北','河北','云南','贵州','浙江','江西','宁夏','青海','西藏','贵州','新疆','山西','山西','四川','重庆'],
   province:'北京',
   statusList:['开启','关闭'],
   status:'开启',
   ModeList:['预付','淘宝'],
   mode:'预付',
   agencyList: [ '互联网','线下'],
   agency:'互联网'
   }

  constructor(private api0104Service: API0104Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api0104ServicePostExec();
  }

  private get0104InDto() {
    let api0104InDto: API0104InDto = {};
    api0104InDto.AgencyId = this.AgencyId;
    api0104InDto.AgencyName = this.AgencyName;
    api0104InDto.AgencyAbbreviation = this.AgencyAbbreviation;
    api0104InDto.AgencyTelephone = this.AgencyTelephone;
    api0104InDto.AgencyMail = this.AgencyMail;
    api0104InDto.AgentAccounts = this.AgentAccount;
    api0104InDto.province = this.agencyInfoList.province;
    api0104InDto.agency = this.agencyInfoList.agency;
    api0104InDto.status = this.agencyInfoList.status;
    api0104InDto.mode = this.agencyInfoList.mode;
    
    return api0104InDto;
    }
  
  private api0104ServicePostExec() {
  
    // 写真帳出力情報取得
    this.api0104Service.postExecNew(this.get0104InDto()).then((res) => {
      
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api0104successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }


  private api0104successed(outDto: API0104OutDto) {
    this.sysAgentInfoList = new Array<SysAgentInfoDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.sysAgentInfoModels));

    for (const sysAgentInfoModel of loadSysusers) {
      let sysAgentInfoDto: SysAgentInfoDto = new SysAgentInfoDto();
      sysAgentInfoDto.AgentId = sysAgentInfoModel.AgentId;
      sysAgentInfoDto.AgentName = sysAgentInfoModel.AgentName;
      sysAgentInfoDto.AgentReduceName = sysAgentInfoModel.AgentReduceName;
      sysAgentInfoDto.Tel = sysAgentInfoModel.Tel;
      sysAgentInfoDto.address = sysAgentInfoModel.address;
      sysAgentInfoDto.businessMode = sysAgentInfoModel.address;
      sysAgentInfoDto.mail = sysAgentInfoModel.mail;


      this.sysAgentInfoList.push(sysAgentInfoDto);
    }
  }

}
