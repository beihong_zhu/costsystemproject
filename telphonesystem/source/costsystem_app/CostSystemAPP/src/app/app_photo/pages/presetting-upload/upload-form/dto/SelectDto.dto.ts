export class SelectDto {
  // ID
  code?: string;
  // 名称
  name?: string;
  // 住所
  address?: string;
}
