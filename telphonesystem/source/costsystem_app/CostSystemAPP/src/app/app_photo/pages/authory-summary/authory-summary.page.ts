import { Component, OnInit } from '@angular/core';
import { API3013InDto, API3013OutDto } from '@service/dto';
import { API3013Service } from '@service/api';
import { ChannelSummaryDto } from './dto';
import { ResultType } from 'src/app/app_common/constant/common';
import { ApiResponse } from '../../util/api-response';


@Component({
  selector: 'app-authory-summary',
  templateUrl: './authory-summary.page.html',
  styleUrls: ['./authory-summary.page.scss'],
})
export class AuthorySummaryPage implements OnInit {

  
   public ocrManufacturingDate :string;
   public agencyabbreviation :string;
   public value : string;
   public  channelSammaryInfo:any = {

   provideSerivceList:[ '电信','移动',' 网通'],
   provideSevice:'移动'

  }

  public sysUserList: Array<ChannelSummaryDto> = new Array<ChannelSummaryDto>();

  constructor(private api3013Service:API3013Service) { }

  ngOnInit() {
  }

  public onSearch() {
    this.api3013ServicePostExec();

    
  }

  private get3013InDto() {
    let api3013InDto: API3013InDto = {};

    api3013InDto.agencyabbreviation = this.agencyabbreviation;
    api3013InDto.value = this.value;
    api3013InDto.provideSevice = this.channelSammaryInfo.provideSevice;

    api3013InDto.ocrManufacturingDate = this.ocrManufacturingDate;

    return api3013InDto;
  }


  private api3013ServicePostExec() {


    // 写真帳出力情報取得
    this.api3013Service.postExecNew(this.get3013InDto()).then((res) => {
      
      const response: any = res;
      if (response.resultCode === ResultType.NORMAL) {
        this.api3013successed(response);
      }
    })
    .catch(ApiResponse.errorHandler);
  }

  private api3013successed(outDto: API3013OutDto) {

    this.sysUserList = new Array<ChannelSummaryDto>();

    const loadSysusers = JSON.parse(JSON.stringify(outDto.sysChannelInfoModels));

    for (const sysUserInfoModel of loadSysusers) {
      let sysUserDto: ChannelSummaryDto = new ChannelSummaryDto();
      sysUserDto.agentName = sysUserInfoModel.agentName;
      sysUserDto.agentReduceName = sysUserInfoModel.agentReduceName;
      sysUserDto.orderSuccessMoney = sysUserInfoModel.orderSuccessMoney;
      sysUserDto.orderTotalSummary = sysUserInfoModel.orderTotalSummary;
      sysUserDto.orderRunningSummary = sysUserInfoModel.orderRunningSummary;
      sysUserDto.orderSuccessSummary = sysUserInfoModel.orderSuccessSummary;
      sysUserDto.orderSuccessRate = sysUserInfoModel.orderSuccessRate;
      sysUserDto.orderSuccessTime = sysUserInfoModel.orderSuccessTime;
      sysUserDto.orderFailureTime = sysUserInfoModel.orderFailureTime;
      sysUserDto.orderThreeMinsRate = sysUserInfoModel.orderThreeMinsRate;
      sysUserDto.orderTenMinsRate = sysUserInfoModel.orderTenMinsRate;

      this.sysUserList.push(sysUserDto);
    }



    
  }



  






  
  

}
