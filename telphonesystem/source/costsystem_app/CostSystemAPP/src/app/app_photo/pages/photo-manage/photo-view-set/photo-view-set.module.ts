import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { PhotoViewSetComponent } from './photo-view-set.component';


@NgModule({
  // declarations: [PhotoViewSetComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PhotoViewSetComponent
      }
    ])
  ]
})
export class PhotoViewSetModule { }
