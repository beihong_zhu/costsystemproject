export interface API0504InDto {
  // 削除写真リスト
  deletePhotoList?: Array<DeletePhotoList>;
}

export interface DeletePhotoList {
  // 撮影端末番号
  photoTerminalNumber?: string;
  // 撮影日
  photoDate?: string;
}
