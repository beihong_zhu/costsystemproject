import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1107InDto, API1107OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * アカウントロック対象者情報一覧
 */
@Injectable()
export class API1107Service extends WebServiceManager<API1107InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1107';
  }

  postExec(
    inDto: API1107InDto,
    resultFunction: (outDto: API1107OutDto) => void,
    faultFunction: () => void,
    isNeedMask: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, isNeedMask);
  }
}
