export interface API0603InDto {
  // ピンID
  pinId?: number;
  // X座標
  positionX?: number;
  // Y座標
  positionY?: number;
  // 配置フラグ
  isInstalled?: number;
}
