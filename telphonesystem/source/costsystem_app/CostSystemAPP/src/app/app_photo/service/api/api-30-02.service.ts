import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { API3002InDto, API3002OutDto } from '@service/dto';
import { CommonConstants } from '@constant/constant.photo';
import { ApiResponse } from '../../util/api-response';

/**
 * 资金记录
 */
@Injectable()
export class API3002Service extends WebServiceManager<API3002InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API3002';
  }

  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;

  postExec(
    inDto: API3002InDto,
    resultFunction: (outDto: API3002OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }

  postExecNew(inDto: API3002InDto) {
    // url created
    let url: string = '';
    url = this.wsUrl + this.api;
    // post
    return this.http.post(url, inDto, ApiResponse.httpPostOptions).toPromise();
  }
}
