import { SortBy } from '.';

export interface API0501InDto {
  // 物件ID
  buildingId?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // フロアID
  floorId?: number;
  // 工程ID
  processId?: number;
  // 作業ID
  workId?: number;
  // 機器ID
  machineId?: number;
  // 撮影日
  photoDate?: string;
  // ソートリスト
  sort?: Array<SortBy>;
}
