import { ApiBaseOutDto } from './api-base-out-dto';

export class API0601OutDto extends ApiBaseOutDto {
  // ピンリスト
  pinList?: Array<Pin>;
}

export interface Pin {
  // ピンID
  pinId?: number;
  // 図面ID
  drawingId?: string;
  // 機器ID
  unitId?: number;
  // X座標
  positionX?: number;
  // Y座標
  positionY?: number;
  // 配置フラグ
  isInstalled?: number;
}
