import { ApiBaseOutDto } from './api-base-out-dto';

export class API0302OutDto extends ApiBaseOutDto {

  // 案件表示フラグ
  isProjectDisplay?: string;
  // フロア表示フラグ
  isFloorDisplay?: string;
  // 部屋表示フラグ
  isRoomDisplay?: string;
  // 場所表示フラグ
  isPlaceDisplay?: string;
  // 系統表示フラグ
  isLineDisplay?: string;
  // 機器表示フラグ
  isUnitDisplay?: string;
  // 工程表示フラグ
  isProcessDisplay?: string;
  // 作業表示フラグ
  isWorkDisplay?: string;
  // 自由欄表示フラグ
  isFreeDisplay?: string;
  // 立会者表示フラグ
  isWitnessDisplay?: string;
  // 施工会社表示フラグ
  isConstructionCompanyDisplay?: string;
  // 日付表示フラグ
  isDateDisplay?: string;
  // 時刻表示フラグ
  isTimeDisplay?: string;
}
