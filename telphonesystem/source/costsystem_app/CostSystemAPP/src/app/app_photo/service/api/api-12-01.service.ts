import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1201InDto, API1201OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * デバイスアクセス情報更新
 */
@Injectable()
export class API1201Service extends WebServiceManager<API1201InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1201';
  }

  postExec(
    inDto: API1201InDto,
    resultFunction: (outDto: API1201OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
