import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF0101InDto, IF0101OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 物件一覧
 */
@Injectable()
export class IF0101Service extends WebServiceManager<IF0101InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/IF0101';
  }

  postExec(
    inDto: IF0101InDto,
    resultFunction: (outDto: IF0101OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
