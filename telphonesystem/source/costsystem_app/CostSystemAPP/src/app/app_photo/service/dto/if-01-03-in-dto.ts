import { SortBy } from '.';

export interface IF0103InDto {
  // ユーザID
  userId?: string;
  // 物件ID
  buildingId?: number;
  // ソートリスト
  sort?: Array<SortBy>;
}
