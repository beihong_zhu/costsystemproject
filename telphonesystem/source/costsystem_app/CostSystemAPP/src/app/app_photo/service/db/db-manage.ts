import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject, DbTransaction } from '@ionic-native/sqlite/ngx';
import { CommonStore } from '../../store/common/common.store';
import { CommonStoreService } from '../../store/common/common.store.service';

@Injectable()
export class DbManageService {
  commstore: CommonStore;

  constructor(
    public sqlite: SQLite,
    public commonStoreService: CommonStoreService) {
    // this.createDB();
    this.commstore = this.commonStoreService.commonStore;
  }

  public createDB() {
    this.creatTables();
  }

  public connectDB(): Promise<SQLiteObject> {
    return this.sqlite.create({
      name: this.commstore.userId + '.db',
      location: 'default',
    });
  }

  public async execTransaction(transaction: (tx: DbTransaction) => void) {
    const db: SQLiteObject = await this.connectDB();
    return db.transaction((tx) => {
      transaction(tx);
    });
  }

  public async selectExec<T>(query: string, params: (string | number)[]): Promise<T[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const db: SQLiteObject = await this.connectDB();
        db.transaction((tx) => {
          tx.executeSql(query, params, (innerTx, rs) => {

            const datas: T[] = [];
            for (let i = 0; i < rs.rows.length; i++) {
              const data = rs.rows.item(i);
              datas.push(data);
            }
            if (rs.rows.length > 0) {
              resolve(datas);
            } else {
              resolve(null);
            }
          }, (innerTx, error) => {
            console.error(error.message);
            reject(error);
          });
        });
      } catch (error) {
        console.error(error.message);
        reject(error);
      }
    });
  }

  public async updateExec(query: string, params: (string | number)[]): Promise<boolean> {

    return new Promise(async (resolve, reject) => {
      try {
        const db: SQLiteObject = await this.connectDB();
        db.transaction((tx) => {
          tx.executeSql(query, params, (innerTx, rs) => {
            resolve(true);
          }, (innerTx, error) => {
            console.error('sqllist error: ' + error.message);
            reject(error);
          });
        });
      } catch (error) {
        console.error('sqllist error: ' + error.message);
        reject(error);
      }
    });
  }



  public async creatTables() {

    this.execTransaction((tx) => {

      tx.executeSql(`CREATE TABLE IF NOT EXISTS PHOTO(
                buildingId INTEGER,
                projectId INTEGER,
                bigProcessId TEXT,
                photoTerminalNo TEXT,
                    photoDate TEXT,
                    floorId INTEGER,
                    floorName TEXT,
                    floorOrder INTEGER,
                    roomId INTEGER,
                    roomName TEXT,
                    roomOrder INTEGER,
                    placeId INTEGER,
                    placeName TEXT,
                    placeOrder INTEGER,
                    machineId INTEGER,
                    modeltype TEXT,
                    unitMark TEXT,
                    processId INTEGER,
                    processName TEXT,
                    processOrder INTEGER,
                    workId INTEGER,
                    workName TEXT,
                    workOrder INTEGER,
                    timingId INTEGER,
                    timingName TEXT,
                    timingAbbreviation TEXT,
                    timingOrder INTEGER,
                    constructionCompanyId INTEGER,
                    constructionCompany TEXT,
                    witnessId INTEGER,
                    witness TEXT,
                    free TEXT,
                    isBlackBoardDisplay INTEGER,
                    photoUser TEXT,
                    photoPath TEXT,
                    isOutputPhotoBook INTEGER,
                    photoImage TEXT,
                    photoPathLocal TEXT,
                    localPhotoFlag INTEGER,
                    uploadFlag INTEGER,
                    retryCount INTEGER,
                    PRIMARY KEY(photoTerminalNo,photoDate)

                )`);


      tx.executeSql(`CREATE TABLE IF NOT EXISTS DRAWING(
                projectId INTEGER,
                buildingId INTEGER,
                floorId INTEGER,
                fileName TEXT,
                fileImage TEXT,
                filePathLocal TEXT,
                PRIMARY KEY(buildingId,floorId)
                )`);


      tx.executeSql(`CREATE TABLE IF NOT EXISTS UNITS(
                unitId INTEGER,
                modeltype TEXT,
                serialNumber TEXT,
                productionDate TEXT,
                retryCount INTEGER,
                PRIMARY KEY(unitId)
                )`);


    });


  }
}

export class TbPhoto {
  buildingId?: number;
  projectId?: number;
  bigProcessId?: string;
  photoTerminalNo?: string;
  photoDate?: string;
  floorId?: number;
  floorName?: string;
  floorOrder?: number;
  roomId?: number;
  roomName?: string;
  roomOrder?: number;
  placeId?: number;
  placeName?: string;
  placeOrder?: number;
  machineId?: number;
  modeltype?: string;
  unitMark?: string;
  processId?: number;
  processName?: string;
  processOrder?: number;
  workId?: number;
  workName?: string;
  workOrder?: number;
  timingId?: number;
  timingName?: string;
  timingAbbreviation?: string;
  timingOrder?: number;
  constructionCompanyId?: number;
  constructionCompany?: string;
  witnessId?: number;
  witness?: string;
  free?: string;
  isBlackBoardDisplay?: number;
  photoUser?: string;
  photoPath?: string;
  isOutputPhotoBook?: number;
  photoImage?: string;
  photoPathLocal?: string;
  localPhotoFlag?: number;
  uploadFlag?: number;
  retryCount?: number;
}

export class TbUnits {
  unitId: number;
  modeltype: string;
  serialNumber: string;
  productionDate: string;
  retryCount?: number;
}

export class TbDraw {
  projectId?: number;
  buildingId?: number;
  floorId?: number;
  fileName?: string;
  fileImage?: string;
  filePathLocal?: string;
}
