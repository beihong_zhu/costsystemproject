import { ApiBaseOutDto } from './api-base-out-dto';

export class IF0103OutDto extends ApiBaseOutDto {
  // 案件リスト
  projects?: Array<Project0103>;
}

export interface Project0103 {
  // 案件ID
  projectId?: number;
  // 案件名
  projectName?: string;
}
