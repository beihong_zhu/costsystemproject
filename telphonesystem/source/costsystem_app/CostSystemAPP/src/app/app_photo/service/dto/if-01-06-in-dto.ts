import { SortBy } from '.';

export interface IF0106InDto {
  // 物件ID
  buildingId?: number;
  // 案件ID
  projectId?: number;
  // ソートリスト
  sorts?: Array<SortBy>;
}
