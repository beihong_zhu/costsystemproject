import { IF0102Service, API0303Service } from '@service/api';
import { RunExecutionService } from '@service/api/run-execution.service';
import { IF0102InDto, IF0102OutDto, API0303InDto, API0303OutDto } from '@service/dto';
import { API0302Service } from '@service/api';
import { API0302InDto, API0302OutDto } from '@service/dto';
import { API0304Service } from '@service/api';
import { API0304InDto, API0304OutDto } from '@service/dto';
import { API0501Service } from '@service/api';
import { API0501InDto, API0501OutDtoHeader, API0501OutDto } from '@service/dto';
import { IF0106Service } from '@service/api';
import { IF0106InDto, IF0106OutDto } from '@service/dto';
import { IF0108Service } from '@service/api';
import { IF0108InDto, IF0108OutDto } from '@service/dto';
import { API0601Service } from '@service/api';
import { API0601InDto, API0601OutDto } from '@service/dto';
import { API0605Service } from '@service/api';
import { API0605InDto, API0605OutDto } from '@service/dto';
import { API0801Service } from '@service/api';
import { API0801InDto, API0801OutDto } from '@service/dto';
import {
  CameraStoreService, PhotomanagestoreService, OfflineStoreService,
  CommonStore, CommonStoreService, DrawOfflineStoreService, AppLoadingStoreService,
  LoggerStoreService, OfflineUnitsService
} from '@store';
import { DbPhotoService } from '@service/db/db-photoservice';
import { DbDrawService } from '@service/db/db-drawservice';
import { SORTBY } from '@constant/code';
import { Store, select } from '@ngrx/store';
import { CD0001Constants } from '@constant/code';
import { AlertController, Platform } from '@ionic/angular';
import { MessageConstants } from '@constant/message-constants';
import { AlertWindowConstants } from '@constant/constant.photo';
import { PhotoListConf } from '@pages/photo-manage/photo-list/photo-list-conf';
import { DbManageService } from '@service/db/db-manage';
import { DbUnitsService } from '@service/db/db-unitsservice';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { DrawDisplayStoreService } from '../../store/draw/draw-location.store.sevice';

export class OfflineAPIService {

  commonStore: CommonStore;
  isAlerted = false;
  IF0102Download: boolean;  // 物件情報提供 IF-01-02
  API0302Download: boolean; // 黒板表示項目情報 API-03-02
  API0304Download: boolean; // 立会人情報 API-03-04
  API0501Download: boolean; // 写真一覧提供 API-05-01
  IF0106Download: boolean; // 機器情報 IF-01-06
  IF0108Download: boolean; // 図面情報 IF-01-08
  API0601Download: boolean; // ピン情報一覧 API-06-01
  API0605Download: boolean; // フロア情報一覧 API-06-05
  API0801Download: boolean; // 工程・作業情報一覧 API-08-01
  API0303Download: boolean; // 状況一覧 API-03-03

  constructor(
    private commonStoreService: CommonStoreService,
    private cameraStoreService: CameraStoreService,
    private photomanagestoreService: PhotomanagestoreService,
    private offlineStoreService: OfflineStoreService,
    private photoService: DbPhotoService,
    private drawService: DbDrawService,
    private drawOfflineStoreService: DrawOfflineStoreService,
    private drawDisplayStoreService: DrawDisplayStoreService,
    private if0102Service: IF0102Service,
    private api0302Service: API0302Service,
    private api0303Service: API0303Service,
    private api0304Service: API0304Service,
    private api0501Service: API0501Service,
    private if0106Service: IF0106Service,
    private if0108Service: IF0108Service,
    private api0601Service: API0601Service,
    private api0605Service: API0605Service,
    private api0801Service: API0801Service,
    private offlineUnitsService: OfflineUnitsService,
    private dbManageService: DbManageService,
    private storeCommon: Store<{ commonStore: CommonStore }>,
    public appLoadingStoreService: AppLoadingStoreService,
    public alertController: AlertController,
    public loggerStoreService: LoggerStoreService,
    private dbUnitsService: DbUnitsService,
    public platform: Platform,
    protected httpClient: HttpClient,
    private runExecutionService: RunExecutionService<API0501InDto>
  ) {
    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    // subscribe commostore 大工程bigProcessId
    this.storeCommon.pipe(select('commonStore')).subscribe((subscribestore => {
      this.commonStore = subscribestore;
      console.log('offline commonStore', this.commonStore);
    }));

  }

  resetAPIDownload() {
    // console.info('resetAPIDownload');

    // S-05-01_写真一覧画面,S-03-01_写真撮影画面
    this.IF0102Download = false;  // 物件情報提供 IF-01-02

    // S-05-01_写真一覧画面
    this.API0801Download = false; // 工程・作業情報一覧 API-08-01
    this.IF0106Download = false; // 機器情報 IF-01-06
    this.API0501Download = false; // 写真一覧提供 API-05-01

    // S-03-01_写真撮影画面
    this.API0302Download = false; // 黒板表示項目情報 API-03-02
    this.API0304Download = false; // 立会人情報 API-03-04
    this.API0303Download = false; // 状況一覧 API-03-03

    // S-06-01_図面表示画面
    this.IF0108Download = false; // 図面情報 IF-01-08
    this.API0601Download = false; // ピン情報一覧 API-06-01
    this.API0605Download = false; // フロア情報一覧 API-06-05

  }

  checkAPIAllDownload() {
    // console.info('checkAPIAllDownload');
    // 全てAPIダウンロード終わったか
    if (this.IF0102Download && this.API0302Download && this.API0304Download &&
      this.API0501Download && this.IF0108Download && this.API0601Download &&
      this.IF0106Download && this.API0801Download && this.API0605Download && this.API0303Download) {
      this.offlineStoreService.setAPIAllDownload(true);
      this.appLoadingStoreService.isloading(false);
    }
  }

  public async queryOfflineData() {
    // console.info('queryOfflineData');
    this.appLoadingStoreService.isloading(true);
    this.isAlerted = false;
    try {
      this.resetAPIDownload();
      this.queryIF0102(this.getIF0102InDto()); // 物件情報提供 IF-01-02
      this.queryAPI0302(this.getAPI0302InDto()); // 黒板表示項目情報 API-03-02
      this.queryAPI0304(this.getAPI0304InDto()); // 立会人情報 API-03-04
      this.queryAPI0501(this.getAPI0501InDto()); // 写真一覧提供 API-05-01
      this.queryIF0106(this.getIF0106InDto()); // 機器情報 IF-01-06
      this.queryIF0108(this.getIF0108InDto()); // 図面情報 IF-01-08
      this.queryAPI0601(this.getAPI0601InDto()); // ピン情報一覧 API-06-01
      this.queryAPI0801(this.getAPI0801InDto()); // 工程・作業情報一覧 API-08-01
      this.queryAPI0303(this.getAPI0303InDto()); // 状況一覧 API-03-03
      // 機器情報はsqlliteからstoreに更新設定
      const offlineUnits = await this.dbUnitsService.selecAllUnits();
      this.offlineUnitsService.setOfflineUnits(offlineUnits);
    } catch {
      // console.info('queryOfflineData onOfflineError');
      this.onOfflineError();
    }
  }

  // 物件情報提供
  private queryIF0102(inDto: IF0102InDto): void {
    console.log('queryIF0102');
    // APIから、物件情報を取得
    this.if0102Service.postExec(inDto, (outDto: IF0102OutDto) => {
      // console.info('if0102successed');
      // レスポンス
      this.if0102successed(outDto);
    },
      // fault
      () => {
        // console.info('if0102 onOfflineError');
        this.onOfflineError();
      },
      false, false, true
    );

  }

  private getIF0102InDto() {
    // console.info('getIF0102InDto');
    const inDto: IF0102InDto = {};
    inDto.buildingId = this.commonStore.buildingId;
    return inDto;
  }

  private if0102successed(outDto: IF0102OutDto) {
    // console.info('if0102successed');
    const floorsList = outDto.floors;
    this.IF0102Download = true;
    this.checkAPIAllDownload();
    this.setIF0102Store(floorsList);
  }

  setIF0102Store(floorsList) {
    // console.info('setIF0102Store');
    this.commonStoreService.setFloorsList(floorsList);
  }

  // 黒板表示項目情報
  private queryAPI0302(inDto: API0302InDto): void {
    // console.info('queryAPI0302');
    // APIから、物件情報を取得
    this.api0302Service.postExec(inDto, (outDto: IF0102OutDto) => {
      // console.info('api0302successed');
      // レスポンス
      this.api0302successed(outDto);
    },
      // fault
      () => {
        // console.info('api0302 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getAPI0302InDto() {
    // console.info('getAPI0302InDto');
    const inDto: API0302InDto = {};
    inDto.projectId = this.commonStore.projectId;
    inDto.bigProcessId = this.commonStore.bigProcessId;
    return inDto;
  }

  private api0302successed(outDto: API0302OutDto) {
    console.log('API0302OutDto', outDto);
    const blackboardDisplay = outDto;
    this.API0302Download = true;
    this.checkAPIAllDownload();
    this.setAPI0302Store(blackboardDisplay);
  }

  setAPI0302Store(blackboardDisplay) {
    // console.info('setAPI0302Store');
    this.cameraStoreService.setBlackboardDisplay(blackboardDisplay);

  }

  // 状況一覧
  private queryAPI0303(inDto: API0303InDto): void {
    // console.info('queryAPI0303');
    // APIから、物件情報を取得
    this.api0303Service.postExec(inDto, (outDto: API0303OutDto) => {
      // console.info('api0303successed');
      // レスポンス
      this.api0303successed(outDto);
    },
      // fault
      () => {
        // console.info('queryAPI0303 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getAPI0303InDto() {
    // console.info('getAPI0303InDto');
    const inDto: API0303InDto = {};
    inDto.bigProcessId = this.commonStore.bigProcessId;
    inDto.sort = [{ sortItem: 'displayOrder', order: SORTBY.ASC }];
    return inDto;
  }

  private api0303successed(outDto: API0303OutDto) {
    // console.info('api0303successed');
    this.API0303Download = true;
    this.cameraStoreService.setTimingsList(outDto.timings);
    this.checkAPIAllDownload();
  }

  // 立会人情報
  private queryAPI0304(inDto: API0304InDto): void {
    // console.info('queryAPI0304');
    // APIから、Witnesslist情報を取得
    this.api0304Service.postExec(inDto, (outDto: API0304OutDto) => {
      // console.info('api0304successed');
      // レスポンス
      this.api0304successed(outDto);
    },
      // fault
      () => {
        // console.info('api0304 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getAPI0304InDto() {
    // console.info('getAPI0304InDto');
    const inDto: API0304InDto = {};
    inDto.projectId = this.commonStore.projectId;
    inDto.sort = [{ sortItem: 'displayOrder', order: SORTBY.ASC }];
    return inDto;
  }

  private api0304successed(outDto: API0304OutDto) {
    // console.info('api0304successed');
    const witnessList = outDto.witnessList;
    this.API0304Download = true;
    this.checkAPIAllDownload();
    this.setAPI0304Store(witnessList);
  }

  setAPI0304Store(witnessList) {
    // console.info('setAPI0304Store');
    this.cameraStoreService.setWitnessList(witnessList);
  }

  // 写真一覧
  private async queryAPI0501(inDto: API0501InDto) {
    // APIから、作業選択メニューリスト情報を取得
    // console.log('API0501InDto', inDto);
    const executionAPI = await this.runExecutionService.runExecution('/API0501', this.getAPI0501InDto(), 1000, false, false, true);
    if (executionAPI) {
      this.downloadJSON(JSON.parse(executionAPI));
    } else {
      // console.info('api0501 onOfflineError');
      this.onOfflineError();
    }
  }

  private getAPI0501InDto() {
    // console.info('getAPI0501InDto');
    const inDto: API0501InDto = {};
    inDto.buildingId = this.commonStore.buildingId;
    inDto.projectId = this.commonStore.projectId;
    inDto.bigProcessId = this.commonStore.bigProcessId;
    return inDto;
  }

  private async api0501Successed(outDto: API0501OutDto) {
    // console.info('api0501Successed');
    const photoList = JSON.parse(JSON.stringify(outDto.photoList));
    const needPhotoList = JSON.parse(JSON.stringify(outDto.needPhotoList));
    // this.API0501Download = true;
    // this.checkAPIAllDownload();


    const dbUnits = await this.dbUnitsService.selectallunits();

    if (photoList && dbUnits) {
      for (const photo of photoList) {
        for (const unit of dbUnits) {
          if (photo.machineId === unit.unitId) {
            photo.modeltype = unit.modeltype;
          }
        }
      }
    }

    if (needPhotoList && dbUnits) {
      for (const photo of needPhotoList) {
        for (const unit of dbUnits) {
          if (photo.machineId === unit.unitId) {
            photo.modeltype = unit.modeltype;
          }
        }
      }
    }

    this.setAPI0501Store(photoList, needPhotoList);

  }

  async setAPI0501Store(photoList, needPhotoList) {
    // console.info('setAPI0501Store');
    const buildingId = this.commonStore.buildingId;
    const projectId = this.commonStore.projectId;
    const bigProcessId = this.commonStore.bigProcessId;

    for (const photo of photoList) {
      photo.localPhotoFlag = PhotoListConf.SERVER_POTO;
      photo.retryCount = PhotoListConf.RETRY_DEFAULT;
    }

    await this.photoService.savephotos(buildingId, projectId, bigProcessId, photoList);
    const takePhotoList = await this.photoService.selectphotos(buildingId, projectId, bigProcessId);
    console.log('takePhotoList', takePhotoList);
    this.loggerStoreService.addLogger('offline api', 'offline api save photo success');
    this.photomanagestoreService.setNeedPhotoList(needPhotoList);
    this.photomanagestoreService.setPhotos(takePhotoList);
    this.offlineStoreService.setPhotosDownload(photoList);
    this.API0501Download = true;
    this.checkAPIAllDownload();
  }


  // 機器情報
  private queryIF0106(inDto: IF0106InDto): void {
    // console.info('queryIF0106');
    // APIから、作業選択メニューリスト情報を取得
    this.if0106Service.postExec(inDto, (outDto: IF0106OutDto) => {
      // console.info('if0106successed');
      // レスポンス
      this.if0106successed(outDto);
    },
      // fault
      () => {
        // console.info('queryIF0106 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getIF0106InDto() {
    // console.info('getIF0106InDto');
    const inDto: IF0106InDto = {};
    inDto.buildingId = this.commonStore.buildingId;
    inDto.projectId = this.commonStore.projectId;
    inDto.sorts = [{ sortItem: 'machineId', order: SORTBY.ASC }];
    return inDto;
  }

  private async if0106successed(outDto: IF0106OutDto) {
    // console.info('if0106successed');
    const machineList = outDto.machineList;
    this.IF0106Download = true;


    const dbUnits = await this.dbUnitsService.selectallunits();

    if (machineList && dbUnits) {
      for (const machine of machineList) {
        for (const unit of dbUnits) {
          if (machine.machineId === unit.unitId) {
            machine.machineModel = unit.modeltype;
            machine.machineNumber = unit.serialNumber;
          }
        }
      }
    }

    this.checkAPIAllDownload();
    this.setIF0106Store(machineList);
  }

  setIF0106Store(machineList) {
    // console.info('setIF0106Store');
    this.commonStoreService.setMachineList(machineList);
  }


  // 図面情報
  private queryIF0108(inDto: IF0108InDto): void {
    // console.info('queryIF0108');
    // APIから、作業選択メニューリスト情報を取得
    this.if0108Service.postExec(inDto, (outDto: IF0108OutDto) => {
      // console.info('if0108successed');
      // レスポンス
      this.if0108successed(outDto);
      this.queryAPI0605(this.getAPI0605InDto()); // フロア情報一覧 API-06-05
    },
      // fault
      () => {
        // console.info('if0108 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getIF0108InDto() {
    // console.info('getIF0108InDto');
    const inDto: IF0108InDto = {};
    inDto.projectId = this.commonStore.projectId;
    inDto.sort = [{ sortItem: 'drawingId', order: SORTBY.ASC }];
    return inDto;
  }

  private if0108successed(outDto: IF0108OutDto) {
    // console.info('if0108successed');
    this.IF0108Download = true;
    this.checkAPIAllDownload();
    this.setIF0108Store(outDto);
  }

  setIF0108Store(outDto: IF0108OutDto) {
    // console.info('setIF0108Store');
    this.drawDisplayStoreService.updatedrawinfo(outDto);
    this.offlineStoreService.setPdfDownload(outDto.drawings);
  }

  // ピン情報一覧
  private queryAPI0601(inDto: API0601InDto): void {
    // console.info('queryAPI0601');
    // APIから、作業選択メニューリスト情報を取得
    this.api0601Service.postExec(inDto, (outDto: API0601OutDto) => {
      // console.info('api0601successed');
      // レスポンス
      this.api0601successed(outDto);
    },
      // fault
      () => {
        // console.info('api0601 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getAPI0601InDto() {
    // console.info('getAPI0601InDto');
    const inDto: API0601InDto = {};
    inDto.projectId = this.commonStore.projectId;
    return inDto;
  }

  private api0601successed(outDto: API0601OutDto) {
    // console.info('api0601successed');
    const pinList = outDto.pinList;
    this.API0601Download = true;
    this.checkAPIAllDownload();
    this.setAPI0601Store(pinList);
    this.drawDisplayStoreService.updatepininfo(outDto.pinList);
  }

  setAPI0601Store(pinList) {

    // todo

  }

  // フロア情報一覧
  private queryAPI0605(inDto: API0605InDto): void {
    // console.info('queryAPI0605');
    // APIから、作業選択メニューリスト情報を取得
    this.api0605Service.postExec(inDto, (outDto: API0605OutDto) => {
      // console.info('api0605successed');
      // レスポンス
      this.api0605successed(outDto);
    },
      // fault
      () => {
        // console.info('api0605 onOfflineError');
        this.onOfflineError();
      },
      false, false, false);

  }

  private getAPI0605InDto() {
    // console.info('getAPI0605InDto');
    const inDto: API0605InDto = {};
    inDto.buildingId = this.commonStoreService.commonStore.buildingId;
    inDto.floorIdList = [];
    const floorId = 'floorId';

    const if0108OutDto = this.drawDisplayStoreService.drawDisplayStore.drawingInfoDto;
    const drawingList = if0108OutDto !== null ? if0108OutDto.drawings : [];
    if (drawingList && drawingList.length > 0) {
      for (const drawing of drawingList) {
        inDto.floorIdList.push({ floorId: drawing.floorId });
      }
    }
    inDto.sort = [{ sortItem: 'displayOrder', order: SORTBY.ASC }];
    return inDto;
  }

  private api0605successed(outDto: API0605OutDto) {
    // console.info('api0605successed');
    this.API0605Download = true;
    this.checkAPIAllDownload();
    this.setAPI0605Store(outDto);
  }

  setAPI0605Store(outDto: API0605OutDto) {
    // console.info('setAPI0605Store');
    this.drawOfflineStoreService.updateAPI0605(outDto);
  }


  // 工程・作業情報一覧
  private queryAPI0801(inDto: API0801InDto): void {
    // console.info('queryAPI0801');
    // APIから、作業選択メニューリスト情報を取得
    this.api0801Service.postExec(inDto, (outDto: API0801OutDto) => {
      // console.info('api0801successed');
      // レスポンス
      this.api0801successed(outDto);
    },
      // fault
      () => {
        // console.info('api0801 onOfflineError');
        this.onOfflineError();
      },
      false, false, true);

  }

  private getAPI0801InDto() {
    // console.info('getAPI0801InDto');
    const inDto: API0801InDto = {};
    inDto.projectId = this.commonStore.projectId;
    inDto.bigProcessId = this.commonStore.bigProcessId;
    return inDto;
  }

  private api0801successed(outDto: API0801OutDto) {
    // console.info('API0801OutDto');
    const processList = outDto.processList;
    this.API0801Download = true;
    this.checkAPIAllDownload();
    this.setAPI0801Store(processList);
  }

  setAPI0801Store(processList) {
    // console.info('setAPI0801Store');
    this.commonStoreService.setProcessList(processList);
  }

  async onOfflineError() {
    //  console.info('onOfflineError');
    if (!this.isAlerted) {
      this.isAlerted = true;
      this.appLoadingStoreService.isloading(false);
      const alertWindow = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: MessageConstants.E00099,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      alertWindow.addEventListener('ionAlertDidPresent', () => {
        const alertButton = alertWindow.getElementsByClassName('alert-button');
        if (alertButton && alertButton.length > 0) {
          (alertButton[0] as HTMLElement).focus();
        }
      });
      await alertWindow.present();
    }
  }

  // -----------------------------------------
  // download s3 json
  private async apiSuccessed(res: HttpResponse<Blob>) {
    const fileReadResult = await this.runExecutionService.apiSuccessFileRead(res);
    if (fileReadResult) {
      this.api0501Successed(fileReadResult);
    } else {
      //  console.info('api0501 onOfflineError');
      this.onOfflineError();
    }
  }

  // 写真一覧URLでダウンロード
  private downloadJSON(res0501: API0501OutDtoHeader) {
    this.httpClient.get(res0501.photoAllListUrl, {
      observe: 'response',
      responseType: 'blob'
    }).subscribe(
      async (res: HttpResponse<Blob>) => {
        this.apiSuccessed(res);
      },
      (err) => {
        //  console.info('api0501 onOfflineError');
        this.onOfflineError();
      }
    );
  }
}
