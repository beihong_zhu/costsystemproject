import { ApiBaseOutDto } from './api-base-out-dto';
import { PhotoPathDto } from './common/dto/photo-path-dto';

export class API1201OutDto extends ApiBaseOutDto {
  photoPaths?: Array<PhotoPathDto>;
}
