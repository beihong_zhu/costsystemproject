import { ApiBaseOutDto } from './api-base-out-dto';

export class API1408OutDto extends ApiBaseOutDto {
  // 入力CSVのS3相対パス
  csvInputFileRelativePath?: string;
}
