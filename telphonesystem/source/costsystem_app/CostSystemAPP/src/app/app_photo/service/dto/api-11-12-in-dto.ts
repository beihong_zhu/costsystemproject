export interface API1112InDto {
  // 更新対象リスト
  updateUserList?: Array<UpdateUserList1112>;
}

export interface UpdateUserList1112 {
  // ユーザID
  userId?: string;
  // 端末コード
  deviceCode?: string;
  // アクセス許可フラグ
  isAccessApproval?: number;
}
