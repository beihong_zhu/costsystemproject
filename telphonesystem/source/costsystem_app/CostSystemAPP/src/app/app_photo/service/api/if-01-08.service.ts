import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF0108InDto, IF0108OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 図面情報
 */
@Injectable()
export class IF0108Service extends WebServiceManager<IF0108InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/IF0108';
  }

  postExec(
    inDto: IF0108InDto,
    resultFunction: (outDto: IF0108OutDto) => void,
    faultFunction: () => void,
    isDownload: boolean = false,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, isDownload, isNeedMask, isNeedErrorHander);
  }
}
