import { ApiBaseOutDto } from './api-base-out-dto';

export class API3008OutDto extends ApiBaseOutDto {
  // ユーザリスト
  zeroInfoModelResList?: Array<SysZeroInfoModel>;
}

export interface SysZeroInfoModel {
  
    //代理简称
    agencyname?: string;
    // 余额
    capital?: string;
    //类型
    capitalStatus?: string;
    //创建时间
    createdDatetime?: string;
    
    
}
