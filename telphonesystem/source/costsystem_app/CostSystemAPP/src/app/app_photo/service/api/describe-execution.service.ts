import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DescribeExecutionInDto, DescribeExecutionOutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { ResultType } from '../../../app_common/constant/common';

/**
 * ステートマシンの状態を取得する
 */
@Injectable()
export class DescribeExecutionService extends WebServiceManager<DescribeExecutionInDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.executionurl;
    this.api = '/status';
  }

  async postExec(
    inDto: DescribeExecutionInDto,
    resultFunction: (outDto: DescribeExecutionOutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(
      inDto,
      (outDto: DescribeExecutionOutDto) => {
        // タスクの実施が成功して、APIからシステムエラーを返却する場合、エラー画面に遷移
        if (outDto.status === 'SUCCEEDED') {
          const res = JSON.parse(outDto.output);
          if (res.resultCode === ResultType.SYSTEMERROR) {
            faultFunction();
            this.routingService.goRouteLink('/error/' + res.messageCode);
            return;
          }
        }
        resultFunction(outDto);
      },
      faultFunction,
      false,
      false
    );
  }

  async postParamExec(
    inDto: DescribeExecutionInDto,
    resultFunction: (outDto: DescribeExecutionOutDto) => void,
    faultFunction: () => void,
    isDownload: boolean = false,
    isNeedMask: boolean = false,
    isNeedErrorHander: boolean = false
  ) {
    this.exec(
      inDto,
      (outDto: DescribeExecutionOutDto) => {
        // タスクの実施が成功して、APIからシステムエラーを返却する場合、エラー画面に遷移
        if (outDto.status === 'SUCCEEDED') {
          const res = JSON.parse(outDto.output);
          if (res.resultCode === ResultType.SYSTEMERROR) {
            faultFunction();
            this.routingService.goRouteLink('/error/' + res.messageCode);
            return;
          }
        }
        resultFunction(outDto);
      },
      faultFunction,
      isDownload,
      isNeedMask,
      isNeedErrorHander
    );
  }
}
