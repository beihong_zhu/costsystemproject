import { SortBy } from '.';

export interface API0202InDto {

  // 渠道名字
  channelname?: string;
  // 面值
  value?: string;
  // 商品类型
  producttype?: string;
  // 商品状态
  productstatus?: string;
 

}