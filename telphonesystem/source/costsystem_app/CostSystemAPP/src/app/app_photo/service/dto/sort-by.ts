export interface SortBy {
  // ソート項目
  sortItem?: string;
  // 昇順/降順
  order?: number;
}
