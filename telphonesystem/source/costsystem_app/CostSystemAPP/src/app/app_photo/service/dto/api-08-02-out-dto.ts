import { ApiBaseOutDto } from './api-base-out-dto';

export class API0802OutDto extends ApiBaseOutDto {
  // テンプレート工程リスト
  processList?: Array<Process>;
}

export interface Process {
  // テンプレート工程ID
  processId?: number;
  // テンプレート工程名
  processName?: string;
  // 工程表示順
  displayProcessOrder?: number;
  // テンプレート作業リスト
  workList?: Array<Work>;
}

export interface Work {
  // テンプレート作業ID
  workId?: number;
  // テンプレート作業名
  workName?: string;
  // 作業表示順
  displayWorkOrder?: number;
}
