import { ApiBaseOutDto } from './api-base-out-dto';

export class API0901OutDto extends ApiBaseOutDto {
  // 写真情報リスト
  photoInfomationList?: Array<PhotoInfomation>;
}

export interface PhotoInfomation {
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 工程表示順
  processDisplayOrder?: number;
  // 撮影端末番号
  photoTerminalNo?: string;
  // 撮影日
  photoDate?: string;
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // フロア表示順
  floorDisplayOrder?: number;
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 部屋表示順
  roomDisplayOrder?: number;
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
  // 場所表示順
  placeDisplayOrder?: number;
  // 機器ID
  machineId?: number;
  // 機番
  machineNumber?: string;
  // 記号(機種名)
  unitMarkModelType?: string;
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 作業表示順
  workDisplayOrder?: number;
  // 状況ID
  timingId?: number;
  // 状況名
  timingName?: string;
  // 状況表示順
  timingDisplayOrder?: number;
  // 写真S3パス
  photoPath?: string;
  // 写真パス
  photoRelativePath?: string;
  // 立会人
  witness?: string;
  // 施工会社ID
  constructionCompanyId?: string;
  // 施工会社
  constructionCompany?: string;
  // 自由欄
  free?: string;
  // 写真帳出力フラグ
  isOutputPhotoBook?: number;
}
