import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { API0202InDto, API0202OutDto } from '@service/dto';
import { CommonConstants } from '@constant/constant.photo';
import { ApiResponse } from '../../util/api-response';

/**
 * 物件一覧
 */
@Injectable()
export class API0202Service extends WebServiceManager<API0202InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0202';
  }

  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;

  postExec(
    inDto: API0202InDto,
    resultFunction: (outDto: API0202OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }

  postExecNew(inDto: API0202InDto) {
    // url created
    let url: string = '';
    url = this.wsUrl + this.api;
    // post
    return this.http.post(url, inDto, ApiResponse.httpPostOptions).toPromise();
  }
}
