export class PhotoPathDto {
    // 撮影日
    photoDate?: string;
    // 写真パス
    photoPath?: string;
    // 写真相対パス
    photoRelativePath?: string;
}
