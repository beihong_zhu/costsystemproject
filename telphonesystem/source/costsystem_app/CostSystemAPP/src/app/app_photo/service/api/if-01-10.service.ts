import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF0110InDto, IF0110OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { ApiFaultDto } from '@service/dto/web-service-manager-dto';

/**
 * 機番情報登録
 */
@Injectable()
export class IF0110Service extends WebServiceManager<IF0110InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/IF0110';
  }

  postExec(
    inDto: IF0110InDto,
    resultFunction: (outDto: IF0110OutDto) => void,
    faultFunction: (outDto?: ApiFaultDto) => void,
    isDownload: boolean = false,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, isDownload, isNeedMask, isNeedErrorHander);
  }
}
