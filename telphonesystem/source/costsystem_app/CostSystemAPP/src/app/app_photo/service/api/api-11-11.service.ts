import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1111InDto, API1111OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * デバイスアクセス情報一覧取得
 */
@Injectable()
export class API1111Service extends WebServiceManager<API1111InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1111';
  }

  postExec(
    inDto: API1111InDto,
    resultFunction: (outDto: API1111OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
