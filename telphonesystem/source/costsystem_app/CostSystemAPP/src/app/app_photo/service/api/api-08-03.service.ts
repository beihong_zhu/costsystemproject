import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0803InDto, API0803OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 工程登録
 */
@Injectable()
export class API0803Service extends WebServiceManager<API0803InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0803';
  }

  processAdd(
    inDto: API0803InDto,
    resultFunction: (outDto: API0803OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
