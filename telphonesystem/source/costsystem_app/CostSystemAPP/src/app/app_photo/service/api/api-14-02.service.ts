import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { APIPresettingInDto, APIPresettingOutDto } from '@service/dto';

/**
 * 物件CSV登録
 */
@Injectable()
export class API1402Service extends WebServiceManager<APIPresettingInDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1402';
    this.outTime = 1000 * 60 * 60 * 24;
  }

  postExec(
    inDto: APIPresettingInDto,
    resultFunction: (outDto: APIPresettingOutDto) => void,
    faultFunction: () => void,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, isNeedMask, isNeedErrorHander);
  }
}
