import { ApiBaseOutDto } from './api-base-out-dto';

export class API3012OutDto extends ApiBaseOutDto {
  // authory info res list
  authoryInfoResList?: Array<SysSuppliedmanagementModel>;
}

export interface SysSuppliedmanagementModel {

  //账户
  authoryName?: string;

  //简称
  abbreviation?: string;
}
