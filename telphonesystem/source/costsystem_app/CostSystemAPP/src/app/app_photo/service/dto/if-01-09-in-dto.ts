export interface IF0109InDto {
  // 物件ID
  buildingId?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 写真帳ファイル名
  photoReportName?: string;
  // 写真帳帳票
  photoBookReport?: string;
}
