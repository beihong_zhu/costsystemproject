import { ApiBaseOutDto } from './api-base-out-dto';

export class API0304OutDto extends ApiBaseOutDto {
  // 立会人リスト
  witnessList?: Array<Witness>;
}

export interface Witness {
  // 立会人ID
  witnessId?: number;
  // 立会人名
  witnessName?: string;
  // 施工会社ID
  constructionCompanyId?: number;
  // 施工会社名
  constructionCompanyName?: string;
  // 表示順
  displayOrder?: number;
}
