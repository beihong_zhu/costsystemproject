import { SortBy } from '.';

export interface API3002InDto {
  // 代理账户
  corpId?: string;
  // 代理名称
  agentName?: string;
  // 代理简称
  agentRecordName?: string;
  // 代理流水
  reqId?: string;
  // 交易流水
  orderId?: string;
  // 请求流水
  billId?: string;
  // 交易方式
  orderForm?: string;
  // 交易类型
  orderType?: string;
  // 开始时间
  startTime?: string;
  // 结束时间
  endTime?: string;
}
