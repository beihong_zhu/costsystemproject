export interface API1113InDto {
  // ユーザID
  userId?: string;
  // 一段階ログイン失敗回数
  firstLoginFailCount?: number;
  // 一段階ログイン失敗時間
  firstLoginFailTime?: string;
}
