import { ApiBaseOutDto } from './api-base-out-dto';

export class API1107OutDto extends ApiBaseOutDto {
  // アカウントロックリスト
  accountLockList?: Array<AccountLockList1107>;
}

export interface AccountLockList1107 {
  // ユーザID
  userId?: string;
  // ロック日時
  lockDatetime?: string;
}
