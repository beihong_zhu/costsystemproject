import { ApiBaseOutDto } from './api-base-out-dto';

export class API0305OutDto extends ApiBaseOutDto {
  // 作業リスト
  workList?: Array<Work0305>;
}

export interface Work0305 {
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 表示順
  displayOrder?: number;
}
