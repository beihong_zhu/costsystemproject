import { ApiBaseOutDto } from './api-base-out-dto';

export class API1401OutDto extends ApiBaseOutDto {
  //   // デバイス情報リスト
  //   deviceInfomations?: Array<DeviceInfomation>;

  // // 対象物件リスト
  // targetBuildings?: Array<TargetBuilding>;
  // // 対象案件リスト
  // targetProjects?: Array<TargetProject>;
  // // 設定対象リスト
  // settingTargets?: Array<SettingTarget>;
}
export interface DeviceInfomation {
  // ユーザID
  userId?: string;
  // 端末コードID
  deviceCode?: string;
  // デバイス登録日時
  deviceCreatedDatetime?: string;
  // デバイス登録有効期限
  deviceLimitDatetime?: string;


}


export interface TargetBuilding {
  // 物件ID
  buildingId?: number;
  // 物件名
  buildingName?: string;
}

export interface TargetProject {
  // 対象案件ID
  projectId?: number;
  // 対象案件名
  projectName?: string;
}

export interface SettingTarget {
  // 設定対象ID
  settingTargetId?: string;
  // 設定対象名
  settingTargetName?: string;
}

