export interface StartExecutionInDto {
  input: string;
  stateMachineArn: string;
}
