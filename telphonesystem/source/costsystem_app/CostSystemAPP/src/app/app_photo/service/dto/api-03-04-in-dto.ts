import { SortBy } from '.';

export interface API0304InDto {
  // 案件ID
  projectId?: number;
  // ソートリスト
  sort?: Array<SortBy>;
}

