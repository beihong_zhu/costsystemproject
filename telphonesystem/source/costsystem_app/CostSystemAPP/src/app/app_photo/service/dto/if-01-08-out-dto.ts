import { ApiBaseOutDto } from './api-base-out-dto';

export class IF0108OutDto extends ApiBaseOutDto {
  // 物件ID
  buildingId?: number;
  // 図面リスト
  drawings?: Array<Drawing>;
}

export interface Drawing {
  // フロアID
  floorId?: number;
  // ファイル名称
  fileName?: string;
  // ファイルイメージ
  fileImage?: string;
  // 図面Id
  drawingId?: string;
}
