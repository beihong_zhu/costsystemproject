export interface API0801InDto {
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
}
