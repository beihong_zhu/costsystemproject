import { ApiBaseOutDto } from './api-base-out-dto';

export class API0303OutDto extends ApiBaseOutDto {
  // 状況リスト
  timings?: Array<Timing0303>;
}

export interface Timing0303 {
  // 状況ID
  timingId?: number;
  // 状況名
  timingName?: string;
  // 状況略称
  timingAbbreviation?: string;
  // 表示順
  displayOrder?: number;
}
