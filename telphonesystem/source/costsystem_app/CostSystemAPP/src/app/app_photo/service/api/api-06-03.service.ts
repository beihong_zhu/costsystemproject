import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0603InDto, API0603OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * ピン情報座標更新
 */
@Injectable()
export class API0603Service extends WebServiceManager<API0603InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0603';
  }

  postExec(
    inDto: API0603InDto,
    resultFunction: (outDto: API0603OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
