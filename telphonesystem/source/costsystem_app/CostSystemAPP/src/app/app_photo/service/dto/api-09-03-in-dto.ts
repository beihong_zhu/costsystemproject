import { ProcessIdDto } from './common/dto/processid-dto';

export interface API0903InDto {

  // 物件ID
  buildingId?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 0:”不明判定無し”、 1:"不明判定あり"
  unknownProcessOutput?: number;
  // 工程IDリスト
  processIdList?: Array<ProcessIdDto>;
}

export interface OutputInfoList {
  // 写真
  photoRelativePath?: string;
  // 物件
  building?: string;
  // フロア
  floor?: string;
  // 部屋
  room?: string;
  // 場所
  place?: string;
  // 大工程
  largeProcess?: string;
  // 工程
  process?: string;
  // 作業
  work?: string;
  // 状況
  situation?: string;
  // 日時
  dateTime?: string;
  // 時間
  time?: string;
  // 記号(機種名)
  unitMarkModelType?: string;
  // 機番
  machineNumber?: string;
  // 立会人
  witness?: string;
  // 施工会社
  constructionCompany?: string;
  // 自由欄
  freeColumn?: string;
}
