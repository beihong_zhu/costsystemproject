import { CommonStoreService, AppStateStoreService, PhotomanagestoreService, AutoUploadPhoto, AutoUploadPhotoService } from '@store';
import { DbPhotoService } from '@service/db/db-photoservice';
import { File } from '@ionic-native/file/ngx';
import { API0505Service, API0301Service } from '@service/api';
import { API0301OutDto } from '@service/dto';
import { ResultType } from '../../../app_common/constant/common';
import { TbPhoto } from '@service/db/db-manage';
import { PhotoConf } from '@conf/photo-conf';
import { AlertController } from '@ionic/angular';
import { MessageConstants } from '@constant/message-constants';
import { AlertWindowConstants } from '@constant/constant.photo';
import { ApiFaultDto } from '@service/dto/web-service-manager-dto';

export class UploadPhotoService {
  // timer
  private timer: NodeJS.Timer = null;
  // timerの間隔
  private uploadInterval = 60000;

  public isRunning = false;

  constructor(
    private file: File,
    private api0301Service: API0301Service,
    private api0505Service: API0505Service,
    private dbPhotoService: DbPhotoService,
    public commonStoreService: CommonStoreService,
    public appStateStoreService: AppStateStoreService,
    public photomanagestoreService: PhotomanagestoreService,
    public autoUploadPhotoService: AutoUploadPhotoService,
    public alertController: AlertController
  ) {
  }

  // timer start ，端末upload写真query
  startUploadTimer() {
    if (this.timer === null) {
      this.timer = setInterval(() => {
        this.batchCore();
      }, this.uploadInterval);

    }
  }

  // 警告メッセージボックスがポップアップ
  private async alertCall(errCode: string, errMsg: string) {
    if (document.getElementsByClassName(errCode).length === 0) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        cssClass: errCode,
        message: errMsg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  // システムエラー処理
  private async setErrAlert(photo: TbPhoto, errCode: string, errMsg: string) {
    await this.dbPhotoService.updatePhotoRetryCount(photo.retryCount, photo.photoTerminalNo, photo.photoDate);
    if (photo.retryCount === PhotoConf.RETRY_MAX_COUNT) {
      this.photomanagestoreService.updatePhotoRetryCount(photo);
      // アップロード失敗、写真一覧画面を更新する
      this.autoUploadPhotoService.setAutoUploadPhotoAction(this.getPhotoUploadFault(photo));
      this.alertCall(errCode, errMsg);
    }
  }

  async batchCore() {
    // offlineの場合、uploadしない
    if (this.appStateStoreService.appState.isOffline) {
      return;
    }
    // 自動同期　offの場合、uploadしない
    if (!this.commonStoreService.commonStore.autoUpload) {
      return;
    }
    if (!this.commonStoreService.commonStore.userId) {
      return;
    }

    // 通信中、実行しない
    if (this.isRunning) {
      return;
    }
    this.isRunning = true;

    // アップロードされる写真を取得すること。
    const photos: TbPhoto[] = await this.dbPhotoService.selectUploadPhoto();
    // アップロードされる写真が存在しない場合、実行しない
    if (photos === null) {
      this.isRunning = false;
      return;
    }
    for (const photo of photos) {
      // offlineの場合、uploadしない
      if (this.appStateStoreService.appState.isOffline) {
        break;
      }
      // 自動同期　offの場合、uploadしない
      if (!this.commonStoreService.commonStore.autoUpload) {
        break;
      }
      try {
        // 写真をアップロード
        const outDto0301: API0301OutDto | ApiFaultDto = await this.api0301ServicePostExec(photo);
        // アップロード失敗の場合
        let errCode: string = '';
        let errMsg: string = '';
        if (outDto0301.resultCode === ResultType.BUSINESSERROR || outDto0301.resultCode === ResultType.SYSTEMERROR) {
          if (outDto0301.messageCode === 'EIF058') {
            // errCode = 'E00105';
            errCode = 'E00135';
          } else if (outDto0301.messageCode === 'EIF059') {
            // errCode = 'E00105';
            errCode = 'E00135';
          } else if (outDto0301.messageCode === 'EIF060') {
            // errCode = 'E00105';
            errCode = 'E00135';
          } else if (outDto0301.messageCode === 'EIF069') {
            // errCode = 'E00105';
            errCode = 'E00135';
          } else if (outDto0301.messageCode === 'EIF068') {
            errCode = 'E00136';
          } else if (outDto0301.messageCode === 'EIF054') {
            errCode = 'E00137';
          } else if (outDto0301.messageCode === 'EIF076') {
            errCode = 'E00138';
          } else if (outDto0301.messageCode === 'EIF016') {
            errCode = 'E00139';
          } else if (outDto0301.messageCode === 'EIF017') {
            errCode = 'E00140';
          } else if (outDto0301.messageCode === 'EIF015') {
            errCode = 'E00141';
          } else if (outDto0301.messageCode === 'E00005') {
            errCode = 'E00142';
          }
          photo.retryCount += 1;
          errMsg = MessageConstants[errCode];
          this.setErrAlert(photo, errCode, errMsg);
          continue;

        } else if (outDto0301.resultCode === null) {
          // APIからレスポンスがない場合は、タイムアウトエラー、HTTPエラー、そのたエラー
          errCode = 'E00143';
          photo.retryCount += 1;
          errMsg = MessageConstants[errCode];
          this.setErrAlert(photo, errCode, errMsg);
          continue;
        }

        this.photomanagestoreService.updatePhotoLocalPhotoFlag(photo);
        // アップロード成功、写真一覧画面を更新する
        this.autoUploadPhotoService.setAutoUploadPhotoAction(this.getPhotoUploadSuccess(photo));

        // sqlliteでLocalPhotoFlagを更新する
        const isSuccess: boolean = await this.dbPhotoService.updatePhotoLocalPhotoFlag(
          PhotoConf.SERVER_POTO,
          photo.photoTerminalNo,
          photo.photoDate);
        // DB更新失敗の場合
        if (!isSuccess) {
          continue;
        }
      } catch (err) {
        // js異常が発生
        if (err instanceof Error) {
          throw err;
        }
      }
    }
    this.isRunning = false;
  }

  // 個別写真upload
  async api0301ServicePostExec(photoUpload: TbPhoto): Promise<API0301OutDto | ApiFaultDto> {
    const photoName = photoUpload.photoPathLocal.split('/')[1];
    const arrayBuffer = await this.file.readAsArrayBuffer(this.file.dataDirectory + 'Download/', photoName);
    const blob = new Blob([arrayBuffer], { type: 'image/jpeg' });
    return new Promise((resolve, reject) => {
      // Formdata blob
      const formData = new FormData();
      formData.append('photoFile', blob);
      formData.append('photoTerminalNo', photoUpload.photoTerminalNo);
      formData.append('photoDate', photoUpload.photoDate);
      formData.append('buildingId', this.numberToString(photoUpload.buildingId));
      formData.append('projectId', this.numberToString(photoUpload.projectId));
      formData.append('floorId', this.numberToString(photoUpload.floorId));
      formData.append('roomId', this.numberToString(photoUpload.roomId));
      formData.append('placeId', this.numberToString(photoUpload.placeId));
      formData.append('machineId', this.numberToString(photoUpload.machineId));
      formData.append('bigProcessId', photoUpload.bigProcessId);
      formData.append('processId', this.numberToString(photoUpload.processId));
      formData.append('workId', this.numberToString(photoUpload.workId));
      formData.append('timingId', this.numberToString(photoUpload.timingId));
      formData.append('constructionCompanyId', this.numberToString(photoUpload.constructionCompanyId));
      formData.append('witnessId', this.numberToString(photoUpload.witnessId));
      formData.append('free', photoUpload.free);
      formData.append('isBlackBoardDisplay', this.numberToString(photoUpload.isBlackBoardDisplay));
      formData.append('photoUser', photoUpload.photoUser);

      this.api0301Service.postFormDataExec(formData, (outDto: API0301OutDto) => {
        resolve(outDto);
      },
        // fault
        (outDto?: ApiFaultDto) => {
          resolve(outDto);
        });
    });
  }

  // アップロード成功
  getPhotoUploadSuccess(photo): AutoUploadPhoto {
    const newp: AutoUploadPhoto = {};
    newp.photoTerminalNo = photo.photoTerminalNo;
    newp.photoDate = photo.photoDate;
    newp.localPhotoFlag = PhotoConf.SERVER_POTO;
    newp.retryCount = photo.retryCount;
    return newp;
  }

  // アップロード失敗
  getPhotoUploadFault(photo): AutoUploadPhoto {
    const newp: AutoUploadPhoto = {};
    newp.photoTerminalNo = photo.photoTerminalNo;
    newp.photoDate = photo.photoDate;
    newp.localPhotoFlag = PhotoConf.LOCAL_POTO;
    newp.retryCount = PhotoConf.RETRY_MAX_COUNT;
    return newp;
  }

  numberToString(val: number) {
    return val === null || val === undefined ? '' : val.toString();
  }

}
