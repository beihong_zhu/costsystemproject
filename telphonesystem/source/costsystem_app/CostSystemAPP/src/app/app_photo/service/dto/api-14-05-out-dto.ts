import { ApiBaseOutDto } from './api-base-out-dto';

export class API1405OutDto extends ApiBaseOutDto {
  // 物件リスト
  buildings?: Array<Building1405>;
}

export interface Building1405 {
  // 物件ID
  buildingId?: number;
  // 物件名
  buildingName?: string;
  addressCity?: string;
  addressNumber?: string;
  addressPrefecture?: string;
}
