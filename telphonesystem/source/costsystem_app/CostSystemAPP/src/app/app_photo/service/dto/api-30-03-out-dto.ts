import { ApiBaseOutDto } from './api-base-out-dto';

export class API3003OutDto extends ApiBaseOutDto {
  // ユーザリスト
  capitalReducePlusModelResList?: Array<SysCapitalReducePlusInfoModel>;
}

export interface SysCapitalReducePlusInfoModel {

    AgentReduceName ?: string;

    plusCapital?:number;

    createdDatetime?:string;

    capital?:number;
}
