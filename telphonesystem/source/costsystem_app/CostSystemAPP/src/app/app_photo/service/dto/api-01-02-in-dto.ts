import { SortBy } from '.';

export interface API0102InDto {
  // ユーザID
  userId?: string;
  // ユーザネーム
  userName?: string;
}
