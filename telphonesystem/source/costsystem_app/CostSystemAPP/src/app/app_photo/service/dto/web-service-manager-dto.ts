import { ApiBaseOutDto } from './api-base-out-dto';

export interface WebServiceManagerDto<M> {
  inDto: M;
  resultFunction: (outDto: ApiBaseOutDto) => void;
  faultFunction: (outDto?: ApiFaultDto) => void;
  isDownload: boolean;
  isNeedMask: boolean;
  isNeedErrorHander: boolean;
  status: 'wait' | 'run' | 'error' | 'quit';
}

export class ApiFaultDto {
  isTimeOut?: boolean;
  status?: string;
  resultCode?: string;
  messageCode?: string;
}
