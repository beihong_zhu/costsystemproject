import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { API3007InDto, API3007OutDto } from '@service/dto';
import { CommonConstants } from '@constant/constant.photo';
import { ApiResponse } from '../../util/api-response';

/**
 * 物件一覧
 */
@Injectable()
export class API3007Service extends WebServiceManager<API3007InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API3007';
  }

  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;

  postExec(
    inDto: API3007InDto,
    resultFunction: (outDto: API3007OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }

  postExecNew(inDto: API3007InDto) {
    // url created
    let url: string = '';
    url = this.wsUrl + this.api;
    // post
    return this.http.post(url, inDto, ApiResponse.httpPostOptions).toPromise();
  }
}
