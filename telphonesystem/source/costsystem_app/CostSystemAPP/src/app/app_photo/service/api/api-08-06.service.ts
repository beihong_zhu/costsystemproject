import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0806InDto, API0806OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 作業登録
 */
@Injectable()
export class API0806Service extends WebServiceManager<API0806InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0806';
  }

  postExec(
    inDto: API0806InDto,
    resultFunction: (outDto: API0806OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
