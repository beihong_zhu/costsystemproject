import { SortBy } from '.';
export interface API3011InDto {
   // 本地流水
   locallimited?: string;
   // 渠道流水
   channellimited? :string;
   //代理流水
   agecnylimited? :string;
   //  渠道名称
   channelname?: string;
   //代理名称
   agencyname?: string; 
   // 电话号码
   telnumber? :string;
   //状态
   status?: string;
}
