import { ApiBaseOutDto } from './api-base-out-dto';

export class API0902OutDto extends ApiBaseOutDto {
  // 写真帳帳票の署名URL
  presignedUrl?: string;
  // 写真帳ファイル名
  photoBookFileName?: string;
}
