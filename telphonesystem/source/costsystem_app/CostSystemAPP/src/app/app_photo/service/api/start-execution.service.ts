import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StartExecutionInDto, StartExecutionOutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { Auth } from 'aws-amplify';
import * as pako from 'pako';

/**
 * ステートマシンの実行を開始する
 */
@Injectable()
export class StartExecutionService<T> extends WebServiceManager<StartExecutionInDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.executionurl;
    this.api = '';
  }

  async postExec(
    api: string,
    inDto: T,
    resultFunction: (outDto: StartExecutionOutDto) => void,
    faultFunction: () => void
  ) {
    const dto = await this.editStartExecutionInDto(api, inDto);
    this.exec(dto, resultFunction, faultFunction, false, false);
  }

  async postParamExec(
    api: string,
    inDto: T,
    resultFunction: (outDto: StartExecutionOutDto) => void,
    faultFunction: () => void,
    isDownload: boolean = false,
    isNeedMask: boolean = false,
    isNeedErrorHander: boolean = false
  ) {
    const dto = await this.editStartExecutionInDto(api, inDto);
    this.exec(dto, resultFunction, faultFunction, isDownload, isNeedMask, isNeedErrorHander);
  }

  private async editStartExecutionInDto(api: string, dto: T): Promise<StartExecutionInDto> {
    const result: StartExecutionInDto = {
      input: '',
      stateMachineArn: environment.aws_step_functions_arn
    };
    const session = await Auth.currentSession();
    // ステートマシンの情報（idToken、APIのURL、APIのパラメータ）
    const executionInfo = {
      authorization: session.getIdToken().getJwtToken(),
      api,
      params: dto
    };
    // gzip圧縮して、Base64エンコードする
    result.input = JSON.stringify({
      executionInfo: window.btoa(pako.gzip(JSON.stringify(executionInfo), { to: 'string' }))
    });
    return result;
  }
}
