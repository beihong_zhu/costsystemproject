import { SortBy } from '.';

export interface API3006InDto {

  channelAccount?: string;

  productType?: string;

  serviceProvede?: string;

  telephoneType?: string;

  province?: string;

  address?: string;

  productValue?: string;

  productCode?: string;

  productName?: string;
  
}
