import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1113InDto, API1113OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 一段階ログイン情報更新
 */
@Injectable({providedIn: 'root'})
export class API1113Service extends WebServiceManager<API1113InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.noauthurl;
    this.api = '/API1113';
  }

  postExec(
    inDto: API1113InDto,
    resultFunction: (outDto: API1113OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, false);
  }
}
