import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0810InDto, API0810OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 工程・作業削除
 */
@Injectable()
export class API0810Service extends WebServiceManager<API0810InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0810';
  }

  postExec(
    inDto: API0810InDto,
    resultFunction: (outDto: API0810OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
