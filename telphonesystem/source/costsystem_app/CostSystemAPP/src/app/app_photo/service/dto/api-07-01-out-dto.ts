import { ApiBaseOutDto } from './api-base-out-dto';

export class API0701OutDto extends ApiBaseOutDto {
  // 権限ID
  authorityId?: string;
  // 利用可能機能リスト
  availableFunctions?: Array<AvailableFunction>;
}

export interface AvailableFunction {
  // 利用機能ID
  useFunctionId?: string;
  // 利用機能名
  useFunctionName?: string;
}
