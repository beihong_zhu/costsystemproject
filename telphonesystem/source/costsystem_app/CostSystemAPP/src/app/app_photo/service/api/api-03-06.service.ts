import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0306InDto, API0306OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 工程情報一覧
 */
@Injectable()
export class API0306Service extends WebServiceManager<API0306InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0306';
  }

  postExec(
    inDto: API0306InDto,
    resultFunction: (outDto: API0306OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
