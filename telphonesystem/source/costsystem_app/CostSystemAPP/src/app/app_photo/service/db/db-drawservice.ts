import { DbManageService, TbDraw } from '@service/db/db-manage';
import { from } from 'rxjs';

export class DbDrawService {
  constructor(private dbManage: DbManageService) { }

  /**
   *
   * @param draw
   *   projectId BIGINT(10),
   *  buildingId BIGINT(10),
   *   floorId BIGINT(10),
   *   fileName VARCHAR(200),
   *   fileImage VARCHAR(500),
   *   filePathLocal VARCHAR(500),
   *   PRIMARY KEY(buildingId,floorId)
   */


  updateSql(): string {
    const sql = `UPDATE DRAWING SET
        fileName  = ?,
        fileImage  = ?,
        filePathLocal  = ?
         WHERE  buildingId = ? AND floorId = ? `;
    return sql;
  }

  async savedraw(projectId, buildingId, offlineDrawing: OfflineDrawing) {
    const insertSql = 'INSERT INTO DRAWING VALUES (?,?,?,?,?,?)';
    const selectByP = 'SELECT * FROM DRAWING WHERE buildingId = ? AND floorId = ?';

    const draws = await this.dbManage.selectExec<TbDraw>(selectByP, [buildingId, offlineDrawing.floorId]);
    if (draws) {
      await this.dbManage.updateExec(this.updateSql(),
        [offlineDrawing.fileName, offlineDrawing.fileImage, offlineDrawing.filePathLocal, buildingId, offlineDrawing.floorId]);
    } else {
      await this.dbManage.updateExec(insertSql,
        [projectId, buildingId, offlineDrawing.floorId, offlineDrawing.fileName, offlineDrawing.fileImage, offlineDrawing.filePathLocal]);
    }
  }

  async selectdraw(buildingId: number, floorId: number): Promise<TbDraw[]> {
    const sqlText = 'SELECT * FROM DRAWING WHERE buildingId = ? AND floorId = ? ';
    return this.dbManage.selectExec<TbDraw>(sqlText, [buildingId, floorId]);
  }
}

export interface OfflineDrawing {
  // フロアID
  floorId?: number;
  // ファイル名称
  fileName?: string;
  // ファイルイメージ
  fileImage?: string;
  // filePathLocal
  filePathLocal?: string;
}
