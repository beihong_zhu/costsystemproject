export interface API0808InDto {
  // 工程ID
  processId?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 表示順
  displayOrder?: number;
  // 工程名
  processName?: string;
}
