import { ApiBaseOutDto } from './api-base-out-dto';

export class API3004OutDto extends ApiBaseOutDto {
  // ユーザリスト
  sysAgencyInfoModels?: Array<SysAgencyInfoModel>;
}

export interface SysAgencyInfoModel {

   accountagency?: string;
   agecnyabbreviate ?:string;
   agencyname?: string;
   province ?:string
   type ?: string ; 
   mode ?: string ;
   balance ?: string ;
   use ?: string;
   status ?: string ;

}
