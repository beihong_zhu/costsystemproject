import { ApiBaseOutDto } from './api-base-out-dto';

export class API0605OutDto extends ApiBaseOutDto {
  // フロアリスト
  floorList?: Array<Floor>;
}

export interface Floor {
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 表示順
  displayOrder?: number;
  // 物件ID
  buildingId?: number;
}
