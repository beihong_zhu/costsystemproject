import { SortBy } from '.';

export interface API1111InDto {
  // ユーザID
  userId?: string;
  // ソートリスト
  sort?: Array<SortBy>;
}

