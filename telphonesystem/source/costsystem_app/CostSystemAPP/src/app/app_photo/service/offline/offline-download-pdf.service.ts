import { Store, select } from '@ngrx/store';
import { OfflineStore, OfflineStoreService, CommonStore, CommonStoreService } from '@store';
import { DbDrawService } from '@service/db/db-drawservice';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { Events } from '@ionic/angular';
import { HttpClient, HttpResponse } from '@angular/common/http';

export class OfflineDownloadPdfService {

  commonStore: CommonStore;
  offlineStore: OfflineStore;
  startDownloadPdf: boolean;

  constructor(
    private file: File,
    private httpClient: HttpClient,
    private storeOffline: Store<{ offlineStore: OfflineStore }>,
    private drawService: DbDrawService,
    private offlineStoreService: OfflineStoreService,
    private storeCommon: Store<{ commonStore: CommonStore }>,
    private commonStoreService: CommonStoreService,
    public events: Events
  ) {

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    // subscribe　commostore 大工程bigProcessId
    this.storeCommon.pipe(select('commonStore')).subscribe((subscribestore => {
      this.commonStore = subscribestore;
    }));

    // subscribe　offline data pdfDownLoad
    this.storeOffline.pipe(select('offlineStore')).subscribe((subscribestore => {
      this.offlineStore = subscribestore;

      if (this.offlineStore.pdfDownLoad && this.offlineStore.pdfDownLoad.length > 0) {
        if (!this.startDownloadPdf) {
          this.startDownloadPdf = true;
          const pdfSingle = this.offlineStore.pdfDownLoad[0];
          this.pdfDownload(pdfSingle);
        }

      }

    }));

  }

  // -----------------------------------------
  // offline -- pdf個別download
  async pdfDownload(drawing) {

    // DownloadDrawing directory 存在かcheck,無い場合、作成
    let downloadDirCreate, downloadDirExist;
    try {
      downloadDirExist = await this.file.checkDir(this.file.dataDirectory, 'DownloadDrawing');
    } catch (error) {
      downloadDirCreate = await this.file.createDir(this.file.dataDirectory, 'DownloadDrawing', false);
    }

    console.log('pdf dir exist');

    if (downloadDirCreate || downloadDirExist) {

      // local pdf flg false
      drawing.localDrawingFlag = false;

      // pdf download directory
      const url = drawing.fileImage;
      if (!url || url.length === 0) {
        return;
      }

      // url有効の場合
      const fileName = drawing.drawingId + '.pdf';
      const filePath = this.file.dataDirectory + 'DownloadDrawing' + '/';
      const drawPathLocal = 'DownloadDrawing' + '/' + fileName;

      this.pdfDownloadANDStore(url, filePath, fileName, drawing, drawPathLocal);

    }
  }

  // pdf download，更新store
  pdfDownloadANDStore(url, filePath, fileName, drawing, filePathLocal) {

    const successFunc = (response) => {
      console.log('response', response);
      this.pdfStore(drawing, filePathLocal);
    };

    const failFunc = (err) => {
      console.log('errr', err);
      this.setNewDownloadPdfs(drawing, false);
    };

    this.downloadPdf(url, filePath, fileName, successFunc, failFunc);

  }

  // 更新store 和 sqlite
  async pdfStore(downloadPdf, filePathLocal) {

    downloadPdf.filePathLocal = filePathLocal;
    downloadPdf.localDrawingFlag = true;

    const buildingId = this.commonStore.buildingId;
    const projectId = this.commonStore.projectId;

    console.log('pdf Download success', downloadPdf);

    // 更新drawing sqlite
    await this.drawService.savedraw(projectId, buildingId, downloadPdf);
    this.events.publish('pdfDownloadSuccess', buildingId, downloadPdf.floorId);
    // todo 更新drawing store
    this.setNewDownloadPdfs(downloadPdf, true);


  }

  // set newDownload photos
  setNewDownloadPdfs(downloadPdf, download) {

    // newDownload pdfs
    const downloadPdfs = this.offlineStore.pdfDownLoad;
    console.log('pdf old', downloadPdfs);

    const newDownload = downloadPdfs.filter((pdf) => {
      if (pdf.fileImage !== downloadPdf.fileImage) {
        return pdf;
      }
    });
    console.log('pdf new', newDownload);
    this.startDownloadPdf = false;
    this.offlineStoreService.setPdfDownload(newDownload);

  }

  // -----------------------------------------
  // download s3 pdf
  async downloadPdf(url, filePath, fileName, successFunc, failFunc) {

    this.httpClient.get(url, {
      observe: 'response',
      responseType: 'blob'
    }).subscribe(async (res: HttpResponse<Blob>) => {

      const blob = res.body;
      try {
        const fileEntry: FileEntry = await this.file.writeFile(filePath, fileName, blob, { replace: true });
        console.log('File created!', fileEntry);
        successFunc(res);
      } catch (error) {
        console.error('Error creating file: ' + error);
        failFunc(error);
      }

    },
      (err) => {
        console.error('Error download file: ' + err);
        failFunc(err);

      }
    );

  }

}
