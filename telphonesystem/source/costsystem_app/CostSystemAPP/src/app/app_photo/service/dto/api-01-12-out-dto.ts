import { ApiBaseOutDto } from './api-base-out-dto';

export class API0112OutDto extends ApiBaseOutDto {
  // agent summary list
  agentSummaryModelResList?: Array<AgentSummaryInfoModel>;
}

export interface AgentSummaryInfoModel {
  // 代理名
  agentName?: string;
  // 代理商账户简称
  agentReduceName?: string;
  // 成功金额
  orderSuccessMoney?: string;
  // 总笔数
  orderTotalSummary?: string;
  // 处理中笔数
  orderRunningSummary?: string;
  // 成功笔数
  orderSuccessSummary?: string;
  // 成功比率
  orderSuccessRate?: string;
  // 成功平均时长
  orderSuccessTime?: string;
  // 失败平均时长
  orderFailureTime?: string;
  // 三分钟到账率
  orderThreeMinsRate?: string;
  // 十分钟到账率
  orderTenMinsRate?: string;
}
