import { ApiBaseOutDto } from './api-base-out-dto';

export class APIPresettingOutDto extends ApiBaseOutDto {
  // 出力CSVのS3署名URL
  outputFilePresignedUrl?: string;
  // 出力CSV名
  csvOutputFileName?: string;
}
