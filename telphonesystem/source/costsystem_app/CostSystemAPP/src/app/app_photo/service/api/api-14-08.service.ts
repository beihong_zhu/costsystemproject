import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { API1408OutDto } from '@service/dto';

/**
 * CSVファイルをS3アップロード
 */
@Injectable()
export class API1408Service extends WebServiceManager<FormData> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1408';
    this.outTime = 1000 * 60 * 60 * 24;
  }

  postExec(
    inDto: FormData,
    resultFunction: (outDto: API1408OutDto) => void,
    faultFunction: () => void,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, isNeedMask, isNeedErrorHander);
  }
}
