import { Store, select } from '@ngrx/store';
import {
  PhotomanagestoreService, OfflineStore, OfflineStoreService,
  CommonStore, CommonStoreService, LoggerStoreService, PhotoDownloadSuccessService
} from '@store';
import { DbPhotoService } from '@service/db/db-photoservice';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { PhotoConf } from '@conf/photo-conf';
import { PhotoDownloadSuccess } from '@store';
import { from } from 'rxjs';

export class OfflineDownloadPhotoService {

  commonStore: CommonStore;
  offlineStore: OfflineStore;
  startDownloadPhoto: boolean;

  constructor(
    private file: File,
    private httpClient: HttpClient,
    private storeOffline: Store<{ offlineStore: OfflineStore }>,
    private photoService: DbPhotoService,
    private offlineStoreService: OfflineStoreService,
    private photomanagestoreService: PhotomanagestoreService,
    private storeCommon: Store<{ commonStore: CommonStore }>,
    private commonStoreService: CommonStoreService,
    private loggerStoreService: LoggerStoreService,
    private downloadphotoService: PhotoDownloadSuccessService
  ) {

    // APPストアに保存した共通内容を取得
    this.commonStore = this.commonStoreService.commonStore;

    // subscribe commostore 大工程bigProcessId
    this.storeCommon.pipe(select('commonStore')).subscribe((subscribestore => {
      this.commonStore = subscribestore;
    }));

    // subscribe offline data photosDownLoad
    this.storeOffline.pipe(select('offlineStore')).subscribe((subscribestore => {
      this.offlineStore = subscribestore;

      if (this.offlineStore.photosDownLoad && this.offlineStore.photosDownLoad.length > 0) {
        if (!this.startDownloadPhoto) {
          this.startDownloadPhoto = true;
          const photoSingle = this.offlineStore.photosDownLoad[0];
          this.saveOnePhotoToLocal(photoSingle);
        }
      }

    }));

  }

  // -----------------------------------------
  // 一つ写真を端末にdownload 、sqltite保存、Store更新
  async saveOnePhotoToLocal(photo) {

    // Download directory 存在かcheck,無い場合、作成
    let downloadDirCreate, downloadDirExist;
    try {
      downloadDirExist = await this.file.checkDir(this.file.dataDirectory, 'Download');
    } catch (error) {
      console.log('photoOffline checkDir', error);
      downloadDirCreate = await this.file.createDir(this.file.dataDirectory, 'Download', false);
    }

    console.log('photo dir exist');

    // Download directory ある場合、 start download photo
    if (downloadDirCreate || downloadDirExist) {
      // S3のURL
      const url = photo.photoImage;
      if (!url || url.length === 0) {
        return;
      }

      // url有効の場合
      const fileName = photo.photoTerminalNo + photo.photoDate + '.jpg'; // filename download
      const filePath = this.file.dataDirectory + 'Download' + '/'; // directory download
      const photoPathLocal = 'Download' + '/' + fileName;
      console.log('photo fileName', fileName);

      // start download 写真
      try {
        // 端末の写真はDirectoryに存在するかcheck
        await this.file.checkFile(filePath, fileName);
        // 端末の写真はDirectoryに存在する場合、新しい写真をdownload
        console.log('photo file exist', photo, url, filePath, fileName, photoPathLocal);
        this.savePhoto2SqliteAndStore(photo, photoPathLocal);

      } catch (err) {
        // 端末の写真はDirectoryに存在しない場合
        console.log('photo file download', photo, url, filePath, fileName, photoPathLocal);
        this.savePhotoToLocalSqliteStore(url, filePath, fileName, photo, photoPathLocal);
      }

    }

  }

  // 写真　download，成功の場合、sqlite保存、store更新
  savePhotoToLocalSqliteStore(url, filePath, fileName, photo, photoPathLocal) {

    // S3の写真は端末にダウンロード成功の場合,sqlite保存、store更新
    const successFunc = (response) => {
      console.log('response', response);
      this.savePhoto2SqliteAndStore(photo, photoPathLocal);
    };

    const failFunc = (err) => {
      console.log('errr', err);
      this.setNewDownloadPhotos(photo, false);
    };

    this.downloadS3PhotoToLocal(url, filePath, fileName, successFunc, failFunc);
  }

  // 写真をsqliteに保存とstoreの更新
  async savePhoto2SqliteAndStore(photoDownload, photoPathLocal) {

    // 写真端末のパスをSqliteに保存する
    photoDownload.photoPathLocal = photoPathLocal;
    // デフォルトはサーバの写真flag
    photoDownload.localPhotoFlag = PhotoConf.SERVER_POTO;
    photoDownload.retryCount = PhotoConf.RETRY_DEFAULT;
    // console.log('photoStore', photo);
    const buildingId = this.commonStore.buildingId;
    const projectId = this.commonStore.projectId;
    const bigProcessId = this.commonStore.bigProcessId;
    console.log('photo Download sucess', photoDownload);

    // download成功したら、DownloadSuccessのstore更新
    const newp: PhotoDownloadSuccess = {};
    newp.photoTerminalNo = photoDownload.photoTerminalNo;
    newp.photoPathLocal = photoDownload.photoPathLocal;

    newp.photoDate = photoDownload.photoDate;

    this.downloadphotoService.setDownloadSuccessPhotoAction(newp);

    // 写真情報をsqliteに保存
    await this.photoService.saveSinglePhoto(buildingId, projectId, bigProcessId, photoDownload);
    const photosAll = await this.photoService.selectphotos(buildingId, projectId, bigProcessId);
    // 写真情報をstoreに保存
    this.photomanagestoreService.setPhotos(photosAll);
    this.setNewDownloadPhotos(photoDownload, true);
  }

  // set newDownload photos
  setNewDownloadPhotos(photoDownload, download) {

    // newDownload photos
    const downloadPhotos = this.offlineStore.photosDownLoad;
    console.log('photo old', downloadPhotos);
    // 次の写真
    const newDownload = downloadPhotos.filter((photo) => {
      if ((photo.photoTerminalNo !== photoDownload.photoTerminalNo) || (photo.photoDate !== photoDownload.photoDate)) {
        return photo;
      }
    });
    console.log('photo new', newDownload);
    this.startDownloadPhoto = false;

    this.offlineStoreService.setPhotosDownload(newDownload);

  }

  // -----------------------------------------
  // download s3 写真
  async downloadS3PhotoToLocal(url, filePath, fileName, successFunc, failFunc) {

    this.httpClient.get(url, {
      observe: 'response',
      responseType: 'blob'
    }).subscribe(async (res: HttpResponse<Blob>) => {

      const blob = res.body;
      this.loggerStoreService.addLogger('offline download', 'download file success');
      try {
        const fileEntry: FileEntry = await this.file.writeFile(filePath, fileName, blob, { replace: true });
        this.loggerStoreService.addLogger('offline download', fileEntry);
        console.log('File created!', fileEntry);
        successFunc(res);
      } catch (err) {
        console.error('Error creating file: ' + err);
        failFunc(err);
      }
    },
      (err) => {
        console.error('Error download file: ' + err);
        failFunc(err);

      }
    );

  }

}
