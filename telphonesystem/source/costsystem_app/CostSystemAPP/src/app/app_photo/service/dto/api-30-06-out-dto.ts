import { ApiBaseOutDto } from './api-base-out-dto';

export class API3006OutDto extends ApiBaseOutDto {
  // ユーザリスト
  sysAgencyInfoModels?: Array<SysAgencyInfoModel>;
}

export interface SysAgencyInfoModel {

   accountagency?: string;
   agencyname?: string;
   agecnyabbreviate ?:string;
   province ?:string;
   mode ?: string ;
   type ?: string ; 
   balance ?: string ;
   useless ?: string;
   credit ?: string ;
   interface ?: string ;
   time ?: string ;
}
