import { ApiBaseOutDto } from './api-base-out-dto';

export class API1406OutDto extends ApiBaseOutDto {
  // 物件リスト
  buildings?: Array<Building1406>;
}

export interface Building1406 {
  // 物件ID
  buildingId?: number;
  // 物件名
  buildingName?: string;
  // 都道府県
  addressPrefecture?: string;
  // 市区町村
  addressCity?: string;
  // 町名・番地
  addressNumber?: string;
}
