import { ApiBaseOutDto } from './api-base-out-dto';

export class IF0102OutDto extends ApiBaseOutDto {
  // 物件ID
  buildingId?: number;
  // 物件名
  buildingName?: string;
  // 物件住所(番地)
  buildingAddressNumber?: string;
  // 物件住所(市区町村)
  buildingAddressCity?: string;
  // 物件住所(都道府県)
  buildingAddressPrefecture?: string;
  // 物件電話番号
  buildingTelephoneNumber?: string;
  // 郵便番号
  buildingZipCode?: string;
  // フロアリスト
  floors?: Array<Floor0102>;
}

export interface Floor0102 {
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 表示順
  displayOrder?: number;
  // 部屋リスト
  rooms?: Array<Room0102>;
}

export interface Room0102 {
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 場所リスト
  places?: Array<Place0102>;
}

export interface Place0102 {
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
}
