
import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppStateStoreService, LoggerStoreService } from '@store';
import { AlertController } from '@ionic/angular';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { OffLineConstants } from '@constant/offline';
import { select, Store } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';

@Injectable()
export class RoutingService {
  private history = [];
  private currentRoute = new Subject();
  currentRouteObservable = this.currentRoute.asObservable();
  offLine: Array<string> = OffLineConstants.OFFLINELIST;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private appStateService: AppStateStoreService,
    private loggerStoreService: LoggerStoreService,
    private storeSubscribe: Store<{ router: RouterReducerState }>,
  ) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        this.currentRoute.next(urlAfterRedirects);
        this.history = [...this.history, urlAfterRedirects];
      });
    window.addEventListener('event_router', (error: CustomEventInit) => {
      this.loggerStoreService.systemErrorInfo = error.detail.message;
      this.router.navigateByUrl('/error/SystemError');
    });
  }

  public routersubscribe(callback) {
    return this.storeSubscribe.pipe(select('router')).subscribe((state => callback(state)));
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getUrl(): string {
    return this.router.url;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 1] || '/index';
  }

  public async goRouteLink(link): Promise<void> {
    // オフラインフラグ
    let routeFlg = false;
    // オフラインすれば
    if (this.appStateService.appState.isOffline) {
      // オフライン可能画面を取得
      Array.from({ length: this.offLine.length }).map((_, i) => {
        if (link === this.offLine[i]) {
          routeFlg = true;
        }
      });

      // オフライン可能画面の場合
      if (routeFlg) {
        this.router.navigateByUrl(link);
      } else {
        // オフライン可能以外画面の場合
        if (document.getElementsByClassName('E00149').length === 0) {
          const alert = await this.alertController.create({
            header: AlertWindowConstants.ERRORTITLE,
            cssClass: 'E00149',
            message: MessageConstants.E00149.replace('{1}', AlertWindowConstants.ADMINEMAIL),
            buttons: [AlertWindowConstants.OKENGLISHBUTTON],
            backdropDismiss: false
          });
          alert.present();
        }
      }
    } else {
      // オンラインすれば
      this.router.navigateByUrl(link);
    }
  }

}
