import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0805InDto, API0805OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 工程・作業情報一括登録
 */
@Injectable()
export class API0805Service extends WebServiceManager<API0805InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0805';
  }

  postExec(
    inDto: API0805InDto,
    resultFunction: (outDto: API0805OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
