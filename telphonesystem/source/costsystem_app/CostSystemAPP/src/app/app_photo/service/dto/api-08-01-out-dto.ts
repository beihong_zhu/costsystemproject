import { ApiBaseOutDto } from './api-base-out-dto';

export class API0801OutDto extends ApiBaseOutDto {
  // 工程リスト
  processList?: Array<Process>;
}

export interface Process {
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 表示順
  displayOrder?: number;
  // 作業リスト
  workList?: Array<Work>;
}

export interface Work {
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 表示順
  displayOrder?: number;
}
