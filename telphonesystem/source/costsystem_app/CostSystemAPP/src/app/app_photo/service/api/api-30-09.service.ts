import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { API3009InDto, API3009OutDto } from '@service/dto';
import { CommonConstants } from '@constant/constant.photo';
import { ApiResponse } from '../../util/api-response';

/**
 * 物件一覧
 */
@Injectable()
export class API3009Service extends WebServiceManager<API3009InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API3009';
  }

  // success
  public success: string = CommonConstants.SUCCESS;
  // failure
  public failure: string = CommonConstants.FAILURE;

  postExec(
    inDto: API3009InDto,
    resultFunction: (outDto: API3009OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }

  postExecNew(inDto: API3009InDto) {
    // url created
    let url: string = '';
    url = this.wsUrl + this.api;
    // post
    return this.http.post(url, inDto, ApiResponse.httpPostOptions).toPromise();
  }
}
