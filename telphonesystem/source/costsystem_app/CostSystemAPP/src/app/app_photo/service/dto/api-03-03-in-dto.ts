import { SortBy } from '.';

export interface API0303InDto {

  // 大工程ID
  bigProcessId?: string;
  // ソートリスト
  sort?: Array<SortBy>;
}

