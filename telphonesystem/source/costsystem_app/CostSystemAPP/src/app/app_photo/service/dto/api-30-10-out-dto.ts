import { ApiBaseOutDto } from './api-base-out-dto';

export class API3010OutDto extends ApiBaseOutDto {
  // ユーザリスト
  sysFundRecorderInfoModels?: Array<SysFundRecorderInfoModel>;
}

export interface SysFundRecorderInfoModel {
  
    // 订单来源
    orderResource?: string;
    // 本地流水
    localLimited?: string;
    // 渠道
    channelLimited?: string;
    // 商品名
    productName?: string;
    // 充值号码
     teleNumber?: string;
    // 面值
     value ?: string;
    // 已充
     result ?: string;
     //价格
     price ?:string ;
     //实付
     realPrice?:string;
     //状态
     status?:string;
     //耗时
     speadTime?: string;
     //次数
      times?: string;
      //创建时间
      createTime?: string;
}
