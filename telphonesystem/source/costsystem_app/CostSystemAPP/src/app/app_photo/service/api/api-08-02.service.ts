import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0802InDto, API0802OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * テンプレート工程・作業情報取得
 */
@Injectable()
export class API0802Service extends WebServiceManager<API0802InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0802';
  }

  postExec(
    inDto: API0802InDto,
    resultFunction: (outDto: API0802OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
