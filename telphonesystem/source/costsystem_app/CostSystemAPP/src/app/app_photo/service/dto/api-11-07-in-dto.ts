import { SortBy } from '.';

export interface API1107InDto {
  // ユーザID
  userId?: string;
  // アカウントロックフラグ
  isAccountlocked?: string;
  // ソートリスト
  sort?: Array<SortBy>;
}
