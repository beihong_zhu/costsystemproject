export interface API0602InDto {
  // 案件ID
  projectId?: number;
  // 図面ID
  drawingId?: string;
  // 機器ID
  unitId?: number;
  // X座標
  positionX?: number;
  // Y座標
  positionY?: number;
  // 配置フラグ
  isInstalled?: number;
}
