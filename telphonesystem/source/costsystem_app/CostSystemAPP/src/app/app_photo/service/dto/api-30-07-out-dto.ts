import { ApiBaseOutDto } from './api-base-out-dto';

export class API3007OutDto extends ApiBaseOutDto {
  // onshelfprouduct infomation 
  onShelfProductModelResList?: Array<SysOnShelfProductInfoModel>;
}

export interface SysOnShelfProductInfoModel {

   AgentReduceName?: string;
   productName?: string;
   productType ?:string;
   spId ?:string;
   value ?: string ;
   price ?: string ; 
   disaccount ?: string ;
   checkPrice ?: string;
   checkCost ?: string ;
   status ?: string ;

}
