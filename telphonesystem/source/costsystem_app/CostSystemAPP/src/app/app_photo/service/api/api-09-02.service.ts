import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0902InDto, API0902OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 写真帳帳票生成
 */
@Injectable()
export class API0902Service extends WebServiceManager<API0902InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0902';
    this.outTime = 1000 * 60 * 60 * 24;
  }

  postExec(
    inDto: API0902InDto,
    resultFunction: (outDto: API0902OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
