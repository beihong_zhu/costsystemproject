import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1110InDto, API1110OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * デバイスアクセス情報取得
 */
@Injectable()
export class API1110Service extends WebServiceManager<API1110InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1110';
  }

  postExec(
    inDto: API1110InDto,
    resultFunction: (outDto: API1110OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, false);
  }
}
