export interface API0809InDto {
  // 工程ID
  processId?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
}
