import { SortBy } from '.';

export interface IF0101InDto {
  // ユーザID
  userId?: string;
  // ソートリスト
  sort?: Array<SortBy>;
}
