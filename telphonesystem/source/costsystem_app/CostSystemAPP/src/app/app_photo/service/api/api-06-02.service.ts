import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0602InDto, API0602OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * ピン情報登録
 */
@Injectable()
export class API0602Service extends WebServiceManager<API0602InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0602';
  }

  postExec(
    inDto: API0602InDto,
    resultFunction: (outDto: API0602OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
