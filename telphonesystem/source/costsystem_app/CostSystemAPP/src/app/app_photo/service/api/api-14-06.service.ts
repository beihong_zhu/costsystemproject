import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1406InDto, API1406OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 物件検索
 */
@Injectable()
export class API1406Service extends WebServiceManager<API1406InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1406';
  }

  postExec(
    inDto: API1406InDto,
    resultFunction: (outDto: API1406OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
