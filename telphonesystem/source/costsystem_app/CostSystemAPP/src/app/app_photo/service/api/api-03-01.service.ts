import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0301OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { ResultType } from '../../../app_common/constant/common';
import { ApiFaultDto } from '@service/dto/web-service-manager-dto';

/**
 * 写真情報登録
 */
@Injectable()
export class API0301Service extends WebServiceManager<FormData> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0301';
  }

  postExec(
    inDto: FormData,
    resultFunction: (outDto: API0301OutDto) => void,
    faultFunction: () => void,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true,
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, isNeedMask, isNeedErrorHander);
  }

  postFormDataExec(
    formData: FormData,
    resultFunction: (outDto: API0301OutDto) => void,
    faultFunction: (outDto?: ApiFaultDto) => void
  ) {
    this.sendFormData(formData).subscribe(
      (res: API0301OutDto) => {
        // APIシステムエラー (download request，serve から jsonのデータを返す場合ある)
        if (res.resultCode === ResultType.SYSTEMERROR) {
          const messageCode = res.messageCode;
          faultFunction({ isTimeOut: false, resultCode: ResultType.SYSTEMERROR, status: '200', messageCode });
        } else {
          // 正常
          resultFunction(res);
        }
      },
      (err: any) => {
        // タイムアウトエラー
        if (err.name === 'TimeoutError') {
          faultFunction({ isTimeOut: true, resultCode: null, status: '0' });
        } else if (err.name === 'HttpErrorResponse') {
          // HTTPエラー
          faultFunction({ isTimeOut: false, resultCode: null, status: err.status });
        } else {
          // そのたエラー
          faultFunction({ isTimeOut: false, resultCode: null, status: err.status });
        }
      }
    );
  }
}
