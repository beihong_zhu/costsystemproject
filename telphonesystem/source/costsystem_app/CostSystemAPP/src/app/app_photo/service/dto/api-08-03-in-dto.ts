export interface API0803InDto {
  // 工程名
  processName?: string;
  // 表示順
  displayOrder?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
}
