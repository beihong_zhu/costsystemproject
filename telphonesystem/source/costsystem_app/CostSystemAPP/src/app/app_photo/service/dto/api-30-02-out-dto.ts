import { ApiBaseOutDto } from './api-base-out-dto';

export class API3002OutDto extends ApiBaseOutDto {
  // ユーザリスト
  capitalRecordModelResList?: Array<CapitalRecordModelRes>;
}

export interface CapitalRecordModelRes {
  // 代理商ID
  agentId?: string
  // 代理商名
  agentName?: string
  // 代理流水
  reqId?: string;
  // 本地流水
  orderId?: string;
  // 渠道流水
  billId?: string;
  // 代理简称
  agentReduceName?: string;
  // 方式
  orderForm?: string;
  // 类型
  orderType?: string;
  // 之前余额
  capital?: string;
  // 交易金额
  price?: string;
  // 状态
  status?: string;
  // 支付时间
  ts?:string;
}
