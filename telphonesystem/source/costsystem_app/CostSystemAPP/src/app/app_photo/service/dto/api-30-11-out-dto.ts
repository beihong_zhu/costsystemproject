import { ApiBaseOutDto } from './api-base-out-dto';

export class API3011OutDto extends ApiBaseOutDto {
  // ユーザリスト
  orderManagementModelResList?: Array<SysOrderManagmentModel>;
}

export interface SysOrderManagmentModel {
  
  //代理简称
  AgentReduceName?:string;
  //渠道简称
  abbreviation?:string;
  //代理流水
  reqId?:string;
  //本地流水
  orderId?:string;
  //渠道流水
  billId?:string;
  //电话号码
  number?:string;
  //面值
  money?:string;
  //状态
  status?:string;
  // 时间
  ts?:string;
}
