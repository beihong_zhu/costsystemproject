export interface API0805InDto {
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 工程リスト
  processList?: Array<ProcessList0805>;
}

export interface ProcessList0805 {
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 表示順
  processDisplayOrder?: number;
  // 作業リスト
  workList?: Array<WorkList0805>;
}

export interface WorkList0805 {
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 表示順
  workDisplayOrder?: number;
}
