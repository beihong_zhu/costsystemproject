import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1112InDto, API1112OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * デバイスアクセス情報更新
 */
@Injectable()
export class API1112Service extends WebServiceManager<API1112InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1112';
  }

  postExec(
    inDto: API1112InDto,
    resultFunction: (outDto: API1112OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
