import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0701InDto, API0701OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 権限利用機能情報取得
 */
@Injectable()
export class API0701Service extends WebServiceManager<API0701InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0701';
  }

  postExec(
    inDto: API0701InDto,
    resultFunction: (outDto: API0701OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, false);
  }
}
