import { ApiBaseOutDto } from './api-base-out-dto';

export class API1111OutDto extends ApiBaseOutDto {
  // デバイスアクセス情報リスト
  deviceAccessInfoList?: Array<DeviceAccessInfoList1111>;
}

export interface DeviceAccessInfoList1111 {
  // ユーザID
  userId?: string;
  // 端末コード
  deviceCode?: string;
  // アクセス許可フラグ
  isAccessApproval?: number;
  // デバイス登録日時
  deviceCreatedDatetime?: string;
}

