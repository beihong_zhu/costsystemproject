import { CommonStoreService, AppState, AppStateStoreService, OfflineUnitsService } from '@store';
import { DbUnitsService } from '@service/db/db-unitsservice';
import { IF0110Service } from '@service/api';
import { IF0110InDto, IF0110OutDto } from '@service/dto';
import { TbUnits } from '../db/db-manage';
import { ResultType } from '../../../app_common/constant/common';
import { PhotoConf } from '@conf/photo-conf';
import { MessageConstants } from '@constant/message-constants';
import { AlertWindowConstants } from '@constant/constant.photo';
import { AlertController } from '@ionic/angular';
import { ApiFaultDto } from '@service/dto/web-service-manager-dto';
export class UploadMachineService {
  // timer
  private timer: NodeJS.Timer = null;
  // timerの間隔
  private uploadInterval = 60000;

  public isRunning = false;


  constructor(
    private if0110Service: IF0110Service,
    private dbUnitsService: DbUnitsService,
    public commonStoreService: CommonStoreService,
    public appStateStoreService: AppStateStoreService,
    public alertController: AlertController,
    public offlineUnitsService: OfflineUnitsService,
  ) {
  }

  // timer start
  startUploadTimer() {
    if (this.timer === null) {
      this.timer = setInterval(() => {
        this.batchCore();
      }, this.uploadInterval);

    }
  }

  // 警告メッセージボックスがポップアップ
  private async alertCall(errCode: string, errMsg: string) {
    if (document.getElementsByClassName(errCode).length === 0) {
      const alert = await this.alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        cssClass: errCode,
        message: errMsg,
        buttons: [AlertWindowConstants.OKENGLISHBUTTON],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  // システムエラー処理
  private async setErrAlert(unit: TbUnits, errCode: string, errMsg: string) {
    await this.dbUnitsService.updateUnitsRetryCount(unit.retryCount, unit.unitId);
    if (unit.retryCount === PhotoConf.RETRY_MAX_COUNT) {
      await this.offlineUnitsService.updateUnitsRetryCount(unit.unitId);
      this.alertCall(errCode, errMsg);
    }
  }

  async batchCore() {
    // offlineの場合、uploadしない
    if (this.appStateStoreService.appState.isOffline) {
      return;
    }
    if (!this.commonStoreService.commonStore.userId) {
      return;
    }
    // if0110は通信中、実行しない
    if (this.isRunning) {
      return;
    }
    this.isRunning = true;
    // アップロードされる機器情報を取得すること。
    const units: TbUnits[] = await this.dbUnitsService.selectunit();
    // アップロードされる機器情報が存在しない場合、実行しない
    if (units === null) {
      this.isRunning = false;
      return;
    }
    for (const unit of units) {
      // offlineの場合、uploadしない
      if (this.appStateStoreService.appState.isOffline) {
        break;
      }

      try {
        // 機器情報をアップロード
        const outDto0110: IF0110OutDto | ApiFaultDto = await this.if0110ServicePostExec(unit);
        // 共通エラーを取得
        let errCode: string = '';
        let errMsg: string = '';
        // if (outDto0110 instanceof IF0110OutDto) {
        // システムエラーが発生する場合
        if (outDto0110.resultCode === ResultType.BUSINESSERROR || outDto0110.resultCode === ResultType.SYSTEMERROR) {
          if (outDto0110.messageCode === 'E00083') {
            // 機器マスタに更新対象の機器IDが存在しなかった場合
            // errCode = 'E00022';
            errCode = 'E00144';
          } else if (outDto0110.messageCode === 'EIF001') {
            // リクエスト整合性チェック
            errCode = 'E00145';
          } else if (outDto0110.messageCode === 'EIF015') {
            // DataAccessException
            errCode = 'E00146';
          } else if (outDto0110.messageCode === 'EIF008') {
            // DataAccessException以外のException
            errCode = 'E00147';
          }
          unit.retryCount += 1;
          errMsg = MessageConstants[errCode];
          this.setErrAlert(unit, errCode, errMsg);
          continue;
        } else if (outDto0110.resultCode === null) {
          // APIからレスポンスがない場合は、タイムアウトエラー、HTTPエラー、そのたエラー
          errCode = 'E00148';
          unit.retryCount += 1;
          errMsg = MessageConstants[errCode];
          this.setErrAlert(unit, errCode, errMsg);
          continue;
        } else {
          // アップロード処理を行った機器情報をsqlliteから削除
          await this.dbUnitsService.deleteUnit(unit.unitId);
          await this.offlineUnitsService.deleteUnit(unit.unitId);
        }
      } catch (err) {
        // js異常が発生
        if (err instanceof Error) {
          throw err;
        }
      }
    }
    this.isRunning = false;
  }

  // if0110は通信中するか判断
  // isRunning(): boolean {
  //   if (this.if0110Service.getStatusFlg()) {
  //     return true;
  //   }
  //   return false;
  // }

  // 機器情報upload
  async if0110ServicePostExec(unitsUpload: TbUnits): Promise<IF0110OutDto | ApiFaultDto> {
    return new Promise((resolve, reject) => {
      // Formdata blob
      this.if0110Service.postExec(
        unitsUpload,
        (outDto: IF0110OutDto) => {
          resolve(outDto);
        },
        (outDto?: ApiFaultDto) => {
          resolve(outDto);
        },
        false,
        false,
        false);
    });
  }
}
