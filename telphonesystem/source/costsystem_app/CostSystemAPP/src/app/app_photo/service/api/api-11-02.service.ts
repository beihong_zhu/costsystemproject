import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1102InDto, API1102OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * パスワード変更
 */
@Injectable()
export class API1102Service extends WebServiceManager<API1102InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1102';
  }

  postExec(
    inDto: API1102InDto,
    resultFunction: (outDto: API1102OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
