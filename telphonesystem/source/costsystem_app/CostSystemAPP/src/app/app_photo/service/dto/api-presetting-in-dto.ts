export interface APIPresettingInDto {
  // 物件ID
  buildingId?: string;
  // 入力CSV名
  csvInputFileName?: string;
  // 入力CSVのS3相対パス
  csvInputFileRelativePath?: string;
}
