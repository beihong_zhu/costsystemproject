import { ApiBaseOutDto } from './api-base-out-dto';

export class API0306OutDto extends ApiBaseOutDto {
  // 工程リスト
  processList?: Array<ProcessList0306>;
}

export interface ProcessList0306 {
  // 工程id
  processId?: number;
  // 工程名
  processName?: string;
  // 表示順
  displayOrder?: number;
}
