import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';
import { API0112InDto, API0112OutDto } from '@service/dto';
import { ApiResponse } from '../../util/api-response';

/**
 * 物件一覧
 */
@Injectable()
export class API0112Service extends WebServiceManager<API0112InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0112';
  }

  postExec(
    inDto: API0112InDto,
    resultFunction: (outDto: API0112OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }

  postExecNew(inDto: API0112InDto) {
    // url created
    let url: string = '';
    url = this.wsUrl + this.api;
    // post
    return this.http.post(url, inDto, ApiResponse.httpPostOptions).toPromise();
  }
}
