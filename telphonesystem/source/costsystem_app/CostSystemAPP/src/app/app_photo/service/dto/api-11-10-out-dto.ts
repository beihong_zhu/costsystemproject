import { ApiBaseOutDto } from './api-base-out-dto';

export class API1110OutDto extends ApiBaseOutDto {
  // アクセス許可フラグ
  isAccessApproval?: number;
}
