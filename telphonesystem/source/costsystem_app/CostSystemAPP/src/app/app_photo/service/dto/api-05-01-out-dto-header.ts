import { ApiBaseOutDto } from './api-base-out-dto';

export class API0501OutDtoHeader extends ApiBaseOutDto {

  // 写真一覧URL
  photoAllListUrl?: string;

  // 写真一覧JSONファイル名
  photoAllLisJsonFileName?: string;
}
