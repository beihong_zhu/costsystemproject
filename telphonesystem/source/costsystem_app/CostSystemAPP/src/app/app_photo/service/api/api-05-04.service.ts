import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0504InDto, API0504OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 写真削除
 */
@Injectable()
export class API0504Service extends WebServiceManager<API0504InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0504';
  }

  postExec(
    inDto: API0504InDto,
    resultFunction: (outDto: API0504OutDto) => void,
    faultFunction: () => void,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, isNeedMask, isNeedErrorHander);
  }
}
