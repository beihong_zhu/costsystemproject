import { ApiBaseOutDto } from './api-base-out-dto';

export interface DescribeExecutionOutDto extends ApiBaseOutDto {
  executionArn: string;
  input: string;
  name: string;
  output: string;
  startDate: number;
  stateMachineArn: string;
  status: string;
  stopDate: number;
}
