import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1405InDto, API1405OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 物件全件取得
 */
@Injectable()
export class API1405Service extends WebServiceManager<API1405InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1405';
  }

  postExec(
    inDto: API1405InDto,
    resultFunction: (outDto: API1405OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
