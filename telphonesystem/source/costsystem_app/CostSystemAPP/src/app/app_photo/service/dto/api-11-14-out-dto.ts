import { ApiBaseOutDto } from './api-base-out-dto';

export class API1114OutDto extends ApiBaseOutDto {
  // 一段階ログイン失敗回数
  firstLoginFailCount?: number;
  // 一段階ログイン失敗時間
  firstLoginFailTime?: string;
}
