import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0604InDto, API0604OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * ピン情報配置フラグ更新
 */
@Injectable()
export class API0604Service extends WebServiceManager<API0604InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0604';
  }

  postExec(
    inDto: API0604InDto,
    resultFunction: (outDto: API0604OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
