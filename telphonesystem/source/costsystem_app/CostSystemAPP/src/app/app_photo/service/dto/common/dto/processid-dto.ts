export class ProcessIdDto {
    // 工程ID
    processId?: number;
    // 作業IDリスト
    workIdList?: {
        // 作業ID
        workId?: number;
    }[];
    // 不明作業出力フラグ
    unknownWorkOutput?: number;
}
