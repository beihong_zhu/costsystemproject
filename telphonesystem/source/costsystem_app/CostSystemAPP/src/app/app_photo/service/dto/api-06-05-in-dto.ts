import { SortBy } from '.';
import { FloorIdDto } from './common/dto/floorid-dto';

export interface API0605InDto {
  // 物件ID
  buildingId?: number;
  // フロアIDリスト
  floorIdList?: Array<FloorIdDto>;
  // ソートリスト
  sort?: Array<SortBy>;
}
