export interface API0112InDto {
  // 代理商账户简称
  agentReduceName?: string;
  // 选择面额
  selectMoney?: string;
  // 创建时间
  createTime?: string;
}
