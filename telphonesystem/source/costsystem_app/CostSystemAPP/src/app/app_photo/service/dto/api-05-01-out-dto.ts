import { ApiBaseOutDto } from './api-base-out-dto';

export class API0501OutDto extends ApiBaseOutDto {
  // 写真リスト
  photoList?: Array<PhotoList0501>;

  // No Image
  needPhotoList?: Array<PhotoList0501>;

}

export interface PhotoList0501 {
  // 撮影端末番号
  photoTerminalNo?: string;
  // 撮影日
  photoDate?: string;
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // フロア表示順
  floorOrder?: number;
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 部屋表示順
  roomOrder?: number;
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
  // 場所表示順
  placeOrder?: number;
  // 機器ID
  machineId?: number;
  // 機種名
  modeltype?: string;
  // 記号
  unitMark?: string;
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 工程表示順
  processOrder?: number;
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 作業表示順
  workOrder?: number;
  // 状況ID
  timingId?: number;
  // 状況名
  timingName?: string;
  // 状況略名
  timingAbbreviation?: string;
  // 状況表示順
  timingOrder?: number;
  // 施工会社Id
  constructionCompanyId?: number;
  // 施工会社
  constructionCompany?: string;
  // 立会者Id
  witnessId?: number;
  // 立会者
  witness?: string;
  // 自由欄
  free?: string;
  // 黒板表示フラグ
  isBlackBoardDisplay?: number;
  // 撮影者
  photoUser?: string;
  // 写真パス
  photoPath?: string;
  // 写真帳出力フラグ
  isOutputPhotoBook?: number;
  // 写真画像
  photoImage?: string;
}
