export interface API0806InDto {
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 作業名
  workName?: string;
  // 工程ID
  processId?: number;
}
