import { ApiBaseOutDto } from './api-base-out-dto';

export class IF0101OutDto extends ApiBaseOutDto {
  // 物件リスト
  buildings?: Array<Building>;
}

export interface Building {
  // 物件ID
  buildingId?: number;
  // 物件名
  buildingName?: string;
  // 物件住所(番地)
  addressNumber?: string;
  // 物件住所(市区町村)
  addressCity?: string;
  // 物件住所(都道府県)
  addressPrefecture?: string;
}
