export interface API1108InDto {
  // アカウント情報リスト
  accountInfoList?: Array<AccountInfo>;
  // アカウントロックフラグ
  isAccountlocked?: string;
}

export interface AccountInfo {
  userId?: string;
}
