import { DbManageService, TbPhoto } from '@service/db/db-manage';
import { PhotoConf } from '@conf/photo-conf';
import { LoggerStoreService } from '@store';
import { DbTransaction } from '@ionic-native/sqlite/ngx';

export class DbPhotoService {
  constructor(private dbManage: DbManageService,
              private loggerStoreService: LoggerStoreService
  ) { }


  savemodel(photo: TbPhoto): (string | number)[] {

    return [
      photo.photoTerminalNo,
      photo.photoDate,
      photo.floorId,
      photo.floorName,
      photo.floorOrder,
      photo.roomId,
      photo.roomName,
      photo.roomOrder,
      photo.placeId,
      photo.placeName,
      photo.placeOrder,
      photo.machineId,
      photo.modeltype,
      photo.unitMark,
      photo.processId,
      photo.processName,
      photo.processOrder,
      photo.workId,
      photo.workName,
      photo.workOrder,
      photo.timingId,
      photo.timingName,
      photo.timingAbbreviation,
      photo.timingOrder,
      photo.constructionCompanyId,
      photo.constructionCompany,
      photo.witnessId,
      photo.witness,
      photo.free,
      photo.isBlackBoardDisplay,
      photo.photoUser,
      photo.photoPath,
      photo.isOutputPhotoBook,
      photo.photoImage,
      photo.photoPathLocal,
      photo.localPhotoFlag,
      photo.uploadFlag,
      photo.retryCount
    ];
  }

  updatemodel(photo: TbPhoto): (string | number)[] {

    return [
      photo.floorId,
      photo.floorName,
      photo.floorOrder,
      photo.roomId,
      photo.roomName,
      photo.roomOrder,
      photo.placeId,
      photo.placeName,
      photo.placeOrder,
      photo.machineId,
      photo.modeltype,
      photo.unitMark,
      photo.processId,
      photo.processName,
      photo.processOrder,
      photo.workId,
      photo.workName,
      photo.workOrder,
      photo.timingId,
      photo.timingName,
      photo.timingAbbreviation,
      photo.timingOrder,
      photo.constructionCompanyId,
      photo.constructionCompany,
      photo.witnessId,
      photo.witness,
      photo.free,
      photo.isBlackBoardDisplay,
      photo.photoUser,
      photo.photoPath,
      photo.isOutputPhotoBook,
      photo.photoImage,
      photo.photoPathLocal,
      photo.localPhotoFlag,
      photo.uploadFlag,
      photo.retryCount,
      photo.photoTerminalNo,
      photo.photoDate
    ];
  }

  saveSql() {
    return 'INSERT INTO PHOTO VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
  }

  async saveSinglePhoto(buildingId, projectId, bigProcessId, photo: TbPhoto) {
    const insertSql = this.saveSql();
    const selectByP = 'SELECT * FROM PHOTO WHERE photoTerminalNo = ? AND photoDate = ?';

    const dbPhoto = await this.dbManage.selectExec<TbPhoto>(selectByP, [photo.photoTerminalNo, photo.photoDate]);
    if (dbPhoto) {
      await this.dbManage.updateExec(this.updateSql(), this.updatemodel(photo));
    } else {
      const value = [buildingId, projectId, bigProcessId];
      await this.dbManage.updateExec(insertSql, value.concat(this.savemodel(photo)));
    }
  }

  async savephotos(buildingId, projectId, bigProcessId, photos) {
    await this.dbManage.execTransaction((db: DbTransaction) => {
      db.executeSql('DELETE FROM PHOTO WHERE localPhotoFlag = 0');
      for (const photo of photos) {
        const sqlText = this.saveSql();
        const selectByP = 'SELECT * FROM PHOTO WHERE photoTerminalNo = ? AND photoDate = ?';
        db.executeSql(selectByP, [photo.photoTerminalNo, photo.photoDate], (tx, rs) => {
          const value = [buildingId,
            projectId,
            bigProcessId
          ];
          if (rs.rows.length > 0) {
            db.executeSql(this.updateSql(), this.updatemodel(photo));
          } else {

            db.executeSql(sqlText, value.concat(this.savemodel(photo)));
          }
        }, (tx, error) => {
          console.log('SELECT error: ' + error.message);
        });
      }
    });
  }


  async updatephotos(photos) {
    await this.dbManage.execTransaction((db) => {
      for (const photo of photos) {
        db.executeSql(this.updateSql(), this.updatemodel(photo));
      }
    });
  }

  updateSql(): string {
    const sql = `UPDATE PHOTO SET
            floorId = ?,
            floorName = ?,
            floorOrder = ?,
            roomId = ?,
            roomName = ?,
            roomOrder = ?,
            placeId = ?,
            placeName = ?,
            placeOrder = ?,
            machineId = ?,
            modeltype = ?,
            unitMark = ?,
            processId = ?,
            processName = ?,
            processOrder = ?,
            workId = ?,
            workName = ?,
            workOrder = ?,
            timingId = ?,
            timingName = ?,
            timingAbbreviation = ?,
            timingOrder = ?,
            constructionCompanyId = ?,
            constructionCompany = ?,
            witnessId = ?,
            witness = ?,
            free = ?,
            isBlackBoardDisplay = ?,
            photoUser = ?,
            photoPath = ?,
            isOutputPhotoBook = ?,
            photoImage = ?,
            photoPathLocal = ?,
            localPhotoFlag = ?,
            uploadFlag = ?,
            retryCount = ?
         WHERE  photoTerminalNo = ? AND photoDate = ?`;
    return sql;
  }

  async selectphotos(buildingId, projectId, bigProcessId): Promise<TbPhoto[]> {
    const value = [buildingId, projectId, bigProcessId];
    const sqlText = 'SELECT * FROM PHOTO WHERE buildingId = ? AND projectId = ? AND bigProcessId = ?';
    const photos = await this.dbManage.selectExec<TbPhoto>(sqlText, value);
    return photos ? photos : [];
  }


  async deletephotos(buildingId, projectId, bigProcessId, photos, success?) {
    await this.dbManage.execTransaction((db) => {
      for (const photo of photos) {
        const sqlText = 'DELETE FROM PHOTO WHERE photoTerminalNo = ? AND photoDate = ?';
        db.executeSql(sqlText, this.deleteModel(photo));
      }
    });

    console.log('save - select');
    if (success != null) {
      const allPhotos = await this.selectphotos(buildingId, projectId, bigProcessId);
      success(allPhotos);
    }
  }

  deleteModel(photo) {
    return [photo.photoTerminalNo,
    photo.photoDate];
  }

  async selectUploadPhoto(): Promise<TbPhoto[]> {
    const value = [PhotoConf.LOCAL_POTO, PhotoConf.RETRY_MAX_COUNT];
    const sqlText = 'SELECT * FROM PHOTO WHERE localPhotoFlag = ? AND retryCount < ? ';
    return this.dbManage.selectExec(sqlText, value);
  }

  async updatePhotoLocalPhotoFlag(localPhotoFlag: number,
                                  photoTerminalNo: string, photoDate: string): Promise<boolean> {
    const value = [localPhotoFlag, photoTerminalNo, photoDate];
    const sqlText = 'UPDATE PHOTO SET localPhotoFlag = ? WHERE photoTerminalNo = ? AND photoDate = ? ';
    return this.dbManage.updateExec(sqlText, value);
  }

  async updatePhotoRetryCount(retryCount, photoTerminalNo: string, photoDate: string): Promise<boolean> {
    const value = [retryCount, photoTerminalNo, photoDate];
    const sqlText = 'UPDATE PHOTO SET retryCount = ? WHERE  photoTerminalNo = ? AND photoDate = ? ';
    return this.dbManage.updateExec(sqlText, value);
  }

  async updatePhotoIsOutputPhotoBook(isOutputPhotoBook: number, photoTerminalNo: string, photoDate: string): Promise<boolean> {
    const value = [isOutputPhotoBook, photoTerminalNo, photoDate];
    const sqlText = 'UPDATE PHOTO SET isOutputPhotoBook = ? WHERE  photoTerminalNo = ? AND photoDate = ? ';
    return this.dbManage.updateExec(sqlText, value);
  }

  async updatePhotoModeltype(machineId: number, modeltype: string): Promise<boolean> {
    const value = [modeltype, machineId];
    const sqlText = 'UPDATE PHOTO SET modeltype = ? WHERE  machineId = ? ';
    return this.dbManage.updateExec(sqlText, value);
  }

  async resetPhotoRetryCount(): Promise<boolean> {
    const value = [0, PhotoConf.RETRY_MAX_COUNT];
    const sqlText = 'UPDATE PHOTO SET retryCount = ? WHERE retryCount = ?';
    return this.dbManage.updateExec(sqlText, value);
  }

}
