import { WorkIdDto } from './common/dto/workid-dto';

export interface API0810InDto {
  // 工程リスト
  processList?: Array<ProcessList0810>;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;


}

export interface ProcessList0810 {
  // 工程ID
  processId?: number;
  // 作業リスト
  workList?: Array<WorkIdDto>;
  // 工程削除フラグ "1":工程削除有 "0": 工程削除無
  isProcessDelete?: string;
}
