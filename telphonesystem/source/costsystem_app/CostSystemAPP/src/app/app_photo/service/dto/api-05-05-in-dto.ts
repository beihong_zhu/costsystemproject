export interface API0505InDto {
  // 写真リスト
  photoList?: Array<Photos0505>;
}

export interface Photos0505 {
  // 撮影端末番号
  photoTerminalNumber?: string;
  // 撮影日
  photoDate?: string;
  // 写真帳出力フラグ
  isOutputPhotoBook?: number;
}
