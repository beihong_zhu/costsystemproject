import { DbManageService, TbUnits } from '@service/db/db-manage';
import { PhotoConf } from '@conf/photo-conf';

export class DbUnitsService {
  constructor(private dbManage: DbManageService) { }


  updateSql(): string {
    const sql = `UPDATE UNITS SET
        modeltype  = ?,
        serialNumber  = ?,
        productionDate = ?,
        retryCount = ?
         WHERE  unitId = ?`;
    return sql;
  }

  async saveunits(unitId, modeltype, serialNumber, productionDate, retryCount) {
    await this.dbManage.execTransaction((db) => {
      const sqlText = 'INSERT INTO UNITS VALUES (?,?,?,?,?)';
      const selectByP = 'SELECT * FROM UNITS WHERE unitId = ?';
      db.executeSql(selectByP, [unitId], (tx, rs) => {
        if (rs.rows.length > 0) {
          db.executeSql(this.updateSql(),
            [modeltype, serialNumber, productionDate, retryCount, unitId]);
        } else {
          db.executeSql(sqlText, [unitId, modeltype, serialNumber, productionDate, retryCount]);
        }
      }, (tx, error) => {
        console.log('SELECT error: ' + error.message);
      });
    });
  }

  async deleteUnit(unitId: number) {
    const sqlText = 'DELETE FROM UNITS WHERE unitId = ?';
    return this.dbManage.updateExec(sqlText, [unitId]);
  }

  async selectunit(): Promise<TbUnits[]> {
    const value = [PhotoConf.RETRY_MAX_COUNT];
    const sqlText = 'SELECT * FROM UNITS WHERE retryCount < ?';
    return this.dbManage.selectExec<TbUnits>(sqlText, value);
  }

  async selectallunits(): Promise<TbUnits[]> {
    const sqlText = 'SELECT * FROM UNITS';
    return this.dbManage.selectExec<TbUnits>(sqlText, []);
  }

  async updateUnitsRetryCount(retryCount, unitId: number): Promise<boolean> {
    const value = [retryCount, unitId];
    const sqlText = 'UPDATE UNITS SET retryCount = ? WHERE  unitId = ?';
    return this.dbManage.updateExec(sqlText, value);
  }

  async resetUnitsRetryCount(): Promise<boolean> {
    const value = [0, PhotoConf.RETRY_MAX_COUNT];
    const sqlText = 'UPDATE UNITS SET retryCount = ? WHERE retryCount = ?';
    return this.dbManage.updateExec(sqlText, value);
  }

  async selecAllUnits(): Promise<TbUnits[]> {
    const sqlText = 'SELECT * FROM UNITS';
    return this.dbManage.selectExec<TbUnits>(sqlText, []);
  }
}
