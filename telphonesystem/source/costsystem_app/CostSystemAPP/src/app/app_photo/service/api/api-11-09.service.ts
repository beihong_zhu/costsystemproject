import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1109InDto, API1109OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * デバイスアクセス情報登録
 */
@Injectable()
export class API1109Service extends WebServiceManager<API1109InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1109';
  }

  postExec(
    inDto: API1109InDto,
    resultFunction: (outDto: API1109OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, false);
  }
}
