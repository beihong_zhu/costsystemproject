import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1108InDto, API1108OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * アカウントロック情報更新
 */
@Injectable()
export class API1108Service extends WebServiceManager<API1108InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API1108';
  }

  postExec(
    inDto: API1108InDto,
    resultFunction: (outDto: API1108OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
