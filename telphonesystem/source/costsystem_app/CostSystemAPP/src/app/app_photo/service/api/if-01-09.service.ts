import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF0109InDto, IF0109OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 写真帳情報
 */
@Injectable()
export class IF0109Service extends WebServiceManager<FormData> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/IF0109';
    this.outTime = 1000 * 60 * 60 * 24;
  }

  postExec(
    inDto: FormData,
    resultFunction: (outDto: IF0109OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
