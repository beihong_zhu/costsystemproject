import { ApiBaseOutDto } from './api-base-out-dto';

export interface StartExecutionOutDto extends ApiBaseOutDto {
  executionArn: string;
  startDate: number;
}
