import { ApiBaseOutDto } from './api-base-out-dto';

export class API0602OutDto extends ApiBaseOutDto {
    // ピンID
    pinId?: number;
    // 図面ID
    drawingId?: string;
    // 機器ID
    unitId?: number;
    // X座標
    positionX?: number;
    // Y座標
    positionY?: number;
    // 配置フラグ
    isInstalled?: number;
}
