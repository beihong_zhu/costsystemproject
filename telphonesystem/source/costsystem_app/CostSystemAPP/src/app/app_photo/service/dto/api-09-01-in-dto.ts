import { ProcessIdDto } from './common/dto/processid-dto';

export interface API0901InDto {
  // 出力パターン
  outputOrderPattern?: string;
  // 物件ID
  buildingId?: number;
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // 工程IDリスト
  processIdList?: Array<ProcessIdDto>;

  // 0:”不明判定無し”、 1:"不明判定あり"
  unknownProcessOutput?: number;
}
