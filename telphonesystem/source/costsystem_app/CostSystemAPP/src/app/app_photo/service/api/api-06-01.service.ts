import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0601InDto, API0601OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * ピン情報一覧
 */
@Injectable()
export class API0601Service extends WebServiceManager<API0601InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0601';
  }

  postExec(
    inDto: API0601InDto,
    resultFunction: (outDto: API0601OutDto) => void,
    faultFunction: () => void,
    isDownload: boolean = false,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, isDownload, isNeedMask, isNeedErrorHander);
  }
}
