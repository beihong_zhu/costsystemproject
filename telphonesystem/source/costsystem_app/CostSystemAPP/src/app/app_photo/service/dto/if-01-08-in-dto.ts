import { SortBy } from '.';

export interface IF0108InDto {
  // 案件ID
  projectId?: number;
  // ソートリスト
  sort?: Array<SortBy>;
}
