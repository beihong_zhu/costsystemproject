import { ApiBaseOutDto } from './api-base-out-dto';

export class IF0106OutDto extends ApiBaseOutDto {
  // 機器リスト
  machineList?: Array<Machine>;
}

export interface Machine {
  // 機器ID
  machineId?: number;
  // 機種名
  machineModel?: string;

  modeltype?: string;
  // 機番
  machineNumber?: string;
  // 系統名
  lineName?: string;
  // 機種区分
  modeltypeDivision?: string;
  // 記号
  unitMark?: string;
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
}
