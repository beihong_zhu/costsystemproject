export interface IF0110InDto {
  // 機器ID
  unitId?: number;
  // 機種
  modeltype?: string;
  // 機番
  serialNumber?: string;
  // 製造年月
  productionDate?: string;
}
