import { SortBy } from '.';

export interface API0306InDto {
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigProcessId?: string;
  // ソートリスト
  sort?: Array<SortBy>;
}
