import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0501InDto, API0501OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 写真一覧提供
 */
@Injectable()
export class API0501Service extends WebServiceManager<API0501InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0501';
  }

  postExec(
    inDto: API0501InDto,
    resultFunction: (outDto: API0501OutDto) => void,
    faultFunction: () => void,
    isDownload: boolean = false,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, isDownload, isNeedMask, isNeedErrorHander);
  }
}
