import { SortBy } from '.';

export interface API0305InDto {
  // 案件ID
  projectId?: number;
  // 大工程ID
  bigprocessId?: string;
  // 工程ID
  processId?: number;
  // ソートリスト
  sort?: Array<SortBy>;
}
