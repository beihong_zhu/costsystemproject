import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0809InDto, API0809OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 作業名変更。
 */
@Injectable()
export class API0809Service extends WebServiceManager<API0809InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0809';
  }

  postExec(
    inDto: API0809InDto,
    resultFunction: (outDto: API0809OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
