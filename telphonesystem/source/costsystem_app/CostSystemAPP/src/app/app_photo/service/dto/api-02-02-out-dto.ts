import { ApiBaseOutDto } from './api-base-out-dto';

export class API0202OutDto extends ApiBaseOutDto {
  // ユーザリスト
  stockInfoModelResList?: Array<SysStockInfoModel>;
}

export interface SysStockInfoModel {
  
  //  渠道简称
  abbreviation?: string;
  //  商品名称
  productName?: string;
  // 商品类型
  productMode?: string;
  // 面额
  value?: string;
  //  价格：渠道的价格
  price?: string;
  // 折扣
  disaccount?: string;
  // 优先级
  priority?: string;
  // 状态
  status?: string;

}
