import { SortBy } from '.';

export interface API1405InDto {
  // ソートリスト
  sort?: Array<SortBy>;
}
