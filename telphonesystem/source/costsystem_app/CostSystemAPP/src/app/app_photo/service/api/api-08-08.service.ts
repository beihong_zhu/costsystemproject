import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0808InDto, API0808OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 工程名変更。
 */
@Injectable()
export class API0808Service extends WebServiceManager<API0808InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0808';
  }

  postExec(
    inDto: API0808InDto,
    resultFunction: (outDto: API0808OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
