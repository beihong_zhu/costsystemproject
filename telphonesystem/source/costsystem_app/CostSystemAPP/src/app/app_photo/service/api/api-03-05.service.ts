import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0305InDto, API0305OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 作業情報一覧
 */
@Injectable()
export class API0305Service extends WebServiceManager<API0305InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0305';
  }

  postExec(
    inDto: API0305InDto,
    resultFunction: (outDto: API0305OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
