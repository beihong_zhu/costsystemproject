import { ApiBaseOutDto } from './api-base-out-dto';

export class API0903OutDto extends ApiBaseOutDto {
  // 写真数
  photoCount?: number;
}
