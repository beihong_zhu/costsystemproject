import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF0103InDto, IF0103OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 案件一覧
 */
@Injectable()
export class IF0103Service extends WebServiceManager<IF0103InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/IF0103';
  }

  postExec(
    inDto: IF0103InDto,
    resultFunction: (outDto: IF0103OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
