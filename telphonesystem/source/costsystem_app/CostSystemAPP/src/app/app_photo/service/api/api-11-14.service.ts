import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API1114InDto, API1114OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 一段階ログイン情報取得
 */
@Injectable({providedIn: 'root'})
export class API1114Service extends WebServiceManager<API1114InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.noauthurl;
    this.api = '/API1114';
  }

  postExec(
    inDto: API1114InDto,
    resultFunction: (outDto: API1114OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, false);
  }
}
