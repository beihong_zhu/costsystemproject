import { Injectable } from '@angular/core';
import { StartExecutionOutDto, DescribeExecutionInDto, DescribeExecutionOutDto, API0501OutDto } from '@service/dto';
import { StartExecutionService, DescribeExecutionService } from '@service/api';
import { HttpResponse } from '@angular/common/http';

/**
 * ステートマシンサービスの実行を開始する
 */
@Injectable()
export class RunExecutionService<T> {
  constructor(
    private startExecutionService: StartExecutionService<T>,
    private describeExecutionService: DescribeExecutionService
  ) {
  }

  // 写真一覧データを取得
  public async runExecution(
    api: string,
    inDto: T,
    time: number,
    isDownload: boolean,
    isNeedMask: boolean,
    isNeedErrorHander: boolean): Promise<string> {
    let data: string | null = null;
    data = await this.startExecutionAPI(api, inDto, time, isDownload, isNeedMask, isNeedErrorHander);
    return data;
  }

  // stepfunctionタスクを起動します
  private async startExecutionAPI(
    api: string,
    inDto: T,
    time: number,
    isDownload: boolean,
    isNeedMask: boolean,
    isNeedErrorHander: boolean): Promise<string> {
    return new Promise(async (resolve, reject) => {
      this.startExecutionService.postParamExec(
        api,
        inDto,
        async (outDto: StartExecutionOutDto) => {
          if (outDto.executionArn) {
            // タスクの起動が成功しました
            const describeExecutionInDto: DescribeExecutionInDto = {
              executionArn: outDto.executionArn
            };
            const result = await this.describeExecutionAPI(
              describeExecutionInDto,
              time,
              isDownload,
              isNeedMask,
              isNeedErrorHander);
            resolve(result);
          } else {
            resolve(null);
          }
        },
        () => resolve(null),
        isDownload,
        isNeedMask,
        isNeedErrorHander
      );
    });
  }

  // タイムスリップ
  private sleep(time: number) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }

  // タスクを監視します
  private async describeExecutionAPI(
    inDto: DescribeExecutionInDto,
    time: number,
    isDownload: boolean,
    isNeedMask: boolean,
    isNeedErrorHander: boolean): Promise<string> {
    return new Promise(async (resolve, reject) => {
      for (; ;) {
        const result = await this.describe(inDto, isDownload, isNeedMask, isNeedErrorHander);
        if (result === 'RUNNING') {
          await this.sleep(time);
        } else {
          // タスクの実施が終了しました
          resolve(result);
          break;
        }
      }
    });
  }

  // タスクを実施
  private describe(
    inDto: DescribeExecutionInDto,
    isDownload: boolean,
    isNeedMask: boolean,
    isNeedErrorHander: boolean): Promise<string> {
    return new Promise((resolve, reject) => {
      this.describeExecutionService.postParamExec(
        inDto,
        async (outDto: DescribeExecutionOutDto) => {
          if (outDto.status === 'RUNNING') {
            resolve(outDto.status);
          } else {
            if (outDto.status === 'SUCCEEDED') {
              // タスクの実施が成功しました
              resolve(outDto.output);
            } else {
              resolve(null);
            }
          }
        },
        () => {
          resolve(null);
        },
        isDownload,
        isNeedMask,
        isNeedErrorHander
      );
    });
  }

  // リスポンスデータがJSONに変換
  public async apiSuccessFileRead(res: HttpResponse<Blob>): Promise<API0501OutDto> {
    if (res.body.size > 0) {
      const fileRead = new FileReader();
      return new Promise(async (resolve, reject) => {
        try {
          fileRead.readAsText(res.body, 'utf-8');
          fileRead.onload = () => {
            const outDto = JSON.parse(fileRead.result as string);
            resolve(outDto);
          };
        } catch (err) {
          console.error(err);
          reject(err);
        }
      });
    } else {
      return null;
    }
  }
}
