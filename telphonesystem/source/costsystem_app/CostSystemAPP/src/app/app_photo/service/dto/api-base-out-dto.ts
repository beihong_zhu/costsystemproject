export class ApiBaseOutDto {
  resultCode?: string;
  resultCnt?: number;
  messageCode?: string;
  message?: string;
}
