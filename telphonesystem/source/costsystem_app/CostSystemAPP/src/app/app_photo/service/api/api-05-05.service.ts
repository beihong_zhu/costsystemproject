import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0505InDto, API0505OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 写真帳出力対象変更
 */
@Injectable()
export class API0505Service extends WebServiceManager<API0505InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0505';
  }

  postExec(
    inDto: API0505InDto,
    resultFunction: (outDto: API0505OutDto) => void,
    faultFunction: () => void,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, false, isNeedMask, isNeedErrorHander);
  }
}
