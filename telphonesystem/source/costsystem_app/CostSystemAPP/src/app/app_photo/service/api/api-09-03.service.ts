import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API0903InDto, API0903OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 写真帳出力情報取得
 */
@Injectable()
export class API0903Service extends WebServiceManager<API0903InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService,
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/API0903';
  }

  postExec(
    inDto: API0903InDto,
    resultFunction: (outDto: API0903OutDto) => void,
    faultFunction: () => void
  ) {
    this.exec(inDto, resultFunction, faultFunction);
  }
}
