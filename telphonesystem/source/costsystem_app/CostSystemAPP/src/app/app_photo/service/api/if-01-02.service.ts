import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IF0102InDto, IF0102OutDto } from '@service/dto';
import { WebServiceManager } from '../../../web-service-manager';
import { AlertController } from '@ionic/angular';
import { environment } from '@env';
import { RoutingService } from '@service/app-route.service';
import { AppLoadingStoreService } from '@store';

/**
 * 物件情報提供
 */
@Injectable()
export class IF0102Service extends WebServiceManager<IF0102InDto> {
  constructor(
    http: HttpClient,
    alertController: AlertController,
    routingService: RoutingService,
    appLoadingStoreService: AppLoadingStoreService
  ) {
    super(http, alertController, routingService, appLoadingStoreService);
    this.wsUrl = environment.wsurl;
    this.api = '/IF0102';
  }

  postExec(
    inDto: IF0102InDto,
    resultFunction: (outDto: IF0102OutDto) => void,
    faultFunction: () => void,
    isDownload: boolean = false,
    isNeedMask: boolean = true,
    isNeedErrorHander: boolean = true
  ) {
    this.exec(inDto, resultFunction, faultFunction, isDownload, isNeedMask, isNeedErrorHander);
  }
}
