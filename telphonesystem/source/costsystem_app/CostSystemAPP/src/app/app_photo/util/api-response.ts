import { HttpHeaders } from '@angular/common/http';
import { AlertWindowConstants } from '@constant/constant.photo';
import { AlertController } from '@ionic/angular';
import { ApiBaseOutDto } from '@service/dto/api-base-out-dto';
import { ResultType } from 'src/app/app_common/constant/common';

export class ApiResponse {

  /**
   * api呼出
   * @param outDto ApiBaseOutDto
   */
  static async apiFailed(alertController: AlertController, outDto: ApiBaseOutDto) {
    // api業務エラー発生の場合
    if (outDto.resultCode === ResultType.BUSINESSERROR) {
      let msg = '';
      msg = decodeURIComponent(outDto.message).replace(/%20/g, ' ');
      const alert = await alertController.create({
        header: AlertWindowConstants.ERRORTITLE,
        message: msg,
        buttons: [
          {
            text: AlertWindowConstants.OKENGLISHBUTTON
          }
        ],
        backdropDismiss: false
      });
      await alert.present();
    }
  }

  /**
   * REST-API 実行時のエラーハンドラ
   * (toPromise.then((res) =>{}) を利用する場合のコード)
   *
   * @public
   * @param {any} err エラー情報
   * @memberof HttpClientService
   */
  public static errorHandler(err) {
    console.log('Error occured.', err);
    return Promise.reject(err.message || err);
  }

  /**
   * Http クライアントを実行する際のヘッダオプション
   * @public
   * @type {*}
   * @memberof HttpClientService
   * @description
   * 認証トークンを使用するために `httpOptions` としてオブジェクトを用意した。
   */
   public static httpGetOptions: any = {
    // ヘッダ情報
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    // DELETE 実行時に `body` が必要になるケースがあるのでプロパティとして用意しておく
    // ( ここで用意しなくても追加できるけど... )
    body: null
  };


  /**
   * Http クライアントを実行する際のヘッダオプション
   * @public
   * @type {*}
   * @memberof HttpClientService
   * @description
   * 認証トークンを使用するために `httpOptions` としてオブジェクトを用意した。
   */
   public static httpPostOptions: any = {
    // ヘッダ情報
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
}
