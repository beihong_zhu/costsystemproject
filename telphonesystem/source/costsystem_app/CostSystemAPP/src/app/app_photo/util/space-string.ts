
export class SpaceString {

  static spaceString(title: string) {
    const res = decodeURI(title.replace(/ /g, '%C2%A0'));
    return res;
  }
}
