
export class TextCheck {

  static textCheck(str: string) {
    if (str === null || str === undefined) {
      return true;
    }
    str = str.replace(/[\r\n]/g, '');
    const text = /^[\u0020-\uFFEE]*$/;
    // 使用可能範囲は20～EFBFAE
    if (!text.test(str)) {
      return false;
    }
    for (const i of str) {
      // 3byteまでの文字を使用可能とする。
      if (i.length > 1) {
        return false;
      }
    }
    return true;
  }




  // 半角チェックサムルール
  static regCheckHalf(str: string) {
    const cArr = str.match(/[^\x00-\xff|\uff61-\uff9f]/ig);
    return (cArr == null ? true : false);
  }

  static allspace(str: string) {
    return str.trim() === '';
  }
}
