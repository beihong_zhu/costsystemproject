import { Action } from '@ngrx/store';
import { LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';
import { PhotoList0501 } from '@service/dto';

// Initial store
export class Photomanagestore {
  photos: LocalPhoto[];
  needPhotoList: PhotoList0501[];
}

const initialState: Photomanagestore = {
  photos: [],
  needPhotoList: []
};

const PhotomanagestoreTypes = {
  setPhotos: 'SET_PHOTOS',
  setMachineList: 'SET_MACHINELIST',
  setNeedPhotoList: 'SET_NEEDPHOTOLIST'
};

export class PhotomanagestoreAction implements Action {
  readonly type = PhotomanagestoreTypes.setPhotos;
  constructor(public photos: LocalPhoto[]) { }
}

export class PhotomanagestoreNeedPhotosListAction implements Action {
  readonly type = PhotomanagestoreTypes.setNeedPhotoList;
  constructor(public needPhotoList: PhotoList0501[]) { }
}

// reducer
export function PhotomanagestoreReducer(state = initialState, action: Action) {

  switch (action.type) {
    case PhotomanagestoreTypes.setPhotos:
      return {
        ...state,
        photos: (action as PhotomanagestoreAction).photos
      };
    case PhotomanagestoreTypes.setNeedPhotoList:
      return {
        ...state,
        needPhotoList: (action as PhotomanagestoreNeedPhotosListAction).needPhotoList
      };
    default:
      return state;
  }
}
