import { Action } from '@ngrx/store';
import { ProcessDto } from '@pages/process/process-create/dto';

export class ProcessCreateState {
  processes?: Array<ProcessDto>;
  selectProject?: number;
  bigProcessId?: string;
}

const initialState: ProcessCreateState = {
  processes: [],
  selectProject: 0,
  bigProcessId: ''
};

const PROCESSActionTypes = {
  UPDATEPROCESSLIST: 'UPDATEPROCESSLIST',
};

export class UPDATEPROCESSLISTAction implements Action {
  readonly type = PROCESSActionTypes.UPDATEPROCESSLIST;
  constructor(public processes: Array<ProcessDto>, public selectProject: number, public bigProcessId: string) { }
}


// reducer
export function ProcessReducer(state = initialState, action: Action) {
  switch (action.type) {
    case PROCESSActionTypes.UPDATEPROCESSLIST:
      return {
        ...state,
        processes: (action as UPDATEPROCESSLISTAction).processes,
        selectProject: (action as UPDATEPROCESSLISTAction).selectProject,
        bigProcessId: (action as UPDATEPROCESSLISTAction).bigProcessId
      };
      break;
    default:
      return state;
  }
}

