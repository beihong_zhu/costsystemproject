import { Action } from '@ngrx/store';

export class PreSettingStore {
  targetBuildingName: string;
  targetProjectName: string;
  settingTargetName: string;
  inputCsvPath: string;
  file: File;
  csvKbn: string;
  userCsvData: UserCsv[];
  bukenCsvData: BukenCsv[];
  bukenHeyaBasyoCsvData: BukenHeyaBasyoCsv[];
  ankenTotalCsvData: AnkenTotalCsv;
  formdata: FormData;
  targetBuildingId: string;
  message: string;
  pdfFiles: File[];
  settingTargetId: string;
  pdfNames: string;
  csvInputFileRelativePath: string;

  confirmSuccess: boolean;
}

export class UserCsv {
  uesrKbn: string;
  uesrId: string;
  bearersName: string;
  mailAdd: string;
  password: string;
  kaishaName: string;
  author: string;
  message: string;
}

export class BukenCsv {
  bukenKbn: string;
  bukenId: string;
  bukenName: string;
  bukenPost: string;
  add1: string;
  add2: string;
  add3: string;
  telephone: string;
  message: string;
}

export class BukenHeyaBasyoCsv {
  bukenId: string;
  bukenName: string;
  froaKbn: string;
  froaId: string;
  froaName: string;
  heyaKbn: string;
  heyaId: string;
  heyaName: string;
  basyoKbn: string;
  basyoId: string;
  basyoName: string;
  message: string;
}

export class AnkenTotalCsv {
  anken: AnkenCsv[];
  userKanli: UserKanliCsv[];
  bukenInfo: BukenZumenInfoCsv[];
  machineInfo: MachineInfoCsv[];
  projInfo: ProjInfoCsv[];
  peopleInfo: PeopleInfoCsv[];
  blackProjInfo: BlackboardProjInfoCsv[];
}

export class AnkenCsv {
  ankenId: string;
  ankenName: string;
  message: string;
}

export class UserKanliCsv {
  userKbn: string;
  userId: string;
  message: string;
}

export class BukenZumenInfoCsv {
  zumenKbn: string;
  froaName: string;
  zumenPath: string;
  zumenName: string;
  message: string;
}

export class MachineInfoCsv {
  machineKbn: string;
  sysName: string;
  signal: string;
  outMachineName: string;
  inMachineName: string;
  froaName: string;
  heyaName: string;
  basyoName: string;
  message: string;
}

export class ProjInfoCsv {
  bigProj: string;
  projId: string;
  projName: string;
  workId: string;
  workName: string;
  doBefore: string;
  doing: string;
  doafter: string;
  message: string;
}

export class PeopleInfoCsv {
  peopleKbn: string;
  peopleId: string;
  peopleName: string;
  doKaishaId: string;
  doKaishaName: string;
  message: string;
}

export class BlackboardProjInfoCsv {
  // 大工程
  bigProj: string;
  // 案件
  ankenFlg: string;
  // フロア表示フラグ
  froaFlg: string;
  // 部屋表示フラグ
  heyaFlg: string;
  // 場所表示フラグ
  basyoFlg: string;
  // 系統表示フラグ
  lineFlg: string;
  // 機器表示フラグ
  machineFlg: string;
  // 工程表示フラグ
  proFlg: string;
  // 作業表示フラグ
  workFlg: string;
  // 自由欄表示フラグ
  freedomearFlg: string;
  // 立会者表示フラグ
  peopleFlg: string;
  // 施工会社
  doKaishaFlg: string;
  // 日付表示フラグ
  dateFlg: string;
  // 時刻表示フラグ
  timeFlg: string;
  // 処理メッセージ
  message: string;
}


const initialStore: PreSettingStore = {
  targetBuildingName: '',
  targetProjectName: '',
  settingTargetName: '',
  inputCsvPath: '',
  file: null,
  csvKbn: '',
  userCsvData: [],
  bukenCsvData: [],
  bukenHeyaBasyoCsvData: [],
  ankenTotalCsvData: new AnkenTotalCsv(),
  formdata: new FormData(),
  targetBuildingId: '',
  message: '',
  pdfFiles: null,
  settingTargetId: '',
  pdfNames: '',
  csvInputFileRelativePath: '',
  confirmSuccess: false
};

const PreSettingActionTypes = {
  PRESETTING_DATA: 'PRESETTING_DATA',
};

export class SetPreSettingAction implements Action {
  readonly type = PreSettingActionTypes.PRESETTING_DATA;
  constructor(public preSettingStore: PreSettingStore) { }
}

// reducer
export function PreSettingReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case PreSettingActionTypes.PRESETTING_DATA:
      return {
        ...store,
        targetBuildingName: (action as SetPreSettingAction).preSettingStore.targetBuildingName,
        targetBuildingId: (action as SetPreSettingAction).preSettingStore.targetBuildingId,
        targetProjecteName: (action as SetPreSettingAction).preSettingStore.targetProjectName,
        settingTargetName: (action as SetPreSettingAction).preSettingStore.settingTargetName,
        inputCsvPath: (action as SetPreSettingAction).preSettingStore.inputCsvPath,
        file: (action as SetPreSettingAction).preSettingStore.file,
        csvKbn: (action as SetPreSettingAction).preSettingStore.csvKbn,
        userCsvData: (action as SetPreSettingAction).preSettingStore.userCsvData,
        bukenCsvData: (action as SetPreSettingAction).preSettingStore.bukenCsvData,
        bukenHeyaBasyoCsvData: (action as SetPreSettingAction).preSettingStore.bukenHeyaBasyoCsvData,
        ankenTotalCsvData: (action as SetPreSettingAction).preSettingStore.ankenTotalCsvData,
        message: (action as SetPreSettingAction).preSettingStore.message,
        pdfFiles: (action as SetPreSettingAction).preSettingStore.pdfFiles,
        settingTargetId: (action as SetPreSettingAction).preSettingStore.settingTargetId,
        confirmSuccess: (action as SetPreSettingAction).preSettingStore.confirmSuccess,
      };
    default:
      return store;
  }
}

