import { Injectable } from '@angular/core';
import { BuildingListStore, SetBuildingListAction } from './buildinglist.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class BuildingListStoreService {

  public buildinglistStore: BuildingListStore;

  constructor(
    private store: Store<BuildingListStore>,
    private storeSubscribe: Store<{ buildinglistStore: BuildingListStore }>
  ) {

    this.storeSubscribe.pipe(select('buildinglistStore')).subscribe(state => {
      if (this.buildinglistStore) {
        this.buildinglistStore.buildinglist = state.buildinglist;
      } else {
        this.buildinglistStore = state;
      }
      // console.log('this.storeSubscribe.buildinglistStore', this.buildinglistStore);
    });

  }

  public setBuildingList(array) {
    this.store.dispatch(new SetBuildingListAction(array));
  }

}
