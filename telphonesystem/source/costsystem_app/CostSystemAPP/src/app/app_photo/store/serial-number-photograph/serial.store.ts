import { Action } from '@ngrx/store';


export class SerialModel {
      // 機器ID
  machineId?: number;
  // 機種名
  machineModel?: string;
   // 物件名
   buildingName?: string;
   // 案件名
   projectName?: string;

  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
  // 制造日期
  date?: string;
}

// Initial store
export class Serialstore {
  serialModel: SerialModel;

}

const initialState: Serialstore = {
    serialModel: null

};

const SerialstoreTypes = {
  setSerial: 'SET_SERIAL',

};

export class SerialstoreAction implements Action {
  readonly type = SerialstoreTypes.setSerial;
  constructor(public serialModel: SerialModel) { }
}



// reducer
export function  SerialstoreReducer(state = initialState, action: Action) {

  switch (action.type) {
    case SerialstoreTypes.setSerial:
      return {
        ...state,
        serialModel: (action as SerialstoreAction).serialModel
      };
    default:
      return state;
  }
}
