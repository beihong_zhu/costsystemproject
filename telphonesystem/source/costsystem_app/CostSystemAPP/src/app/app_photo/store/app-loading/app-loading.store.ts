import { Action } from '@ngrx/store';

// initial state
export class AppLoading {
  isLoading: boolean;
}

const initialState: AppLoading = {
  isLoading: false,
};

const AppLoadingTypes = {
  Loading: 'LOADING',
};

export class AppLoadingAction implements Action {
  readonly type = AppLoadingTypes.Loading;
  constructor(public isLoading: boolean) { }
}

// reducer
export function AppLoadingReducer(state = initialState, action: AppLoadingAction) {
  state.isLoading = action.isLoading;

  switch (action.type) {
    case AppLoadingTypes.Loading:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
}

