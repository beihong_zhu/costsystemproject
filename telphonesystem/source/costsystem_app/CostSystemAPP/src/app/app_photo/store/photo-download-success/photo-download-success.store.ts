import { Action } from '@ngrx/store';

export class PhotoDownloadSuccessStore {
    photoDownloadSuccess: PhotoDownloadSuccess;

}

const initialStore: PhotoDownloadSuccessStore = {
    photoDownloadSuccess: {}
};

const PhotoDownloadSuccessActionTypes = {
    SETDOWNPHOTO: 'SET_DOWNLOADSUCCESSPHOTOTYPE'

};



export class PhotoDownloadSuccessAction implements Action {
    readonly type = PhotoDownloadSuccessActionTypes.SETDOWNPHOTO;
    constructor(public photoDownloadSuccess: PhotoDownloadSuccess) { }
}

// reducer
export function PhotoDwonloadSuccessStoreReducer(store = initialStore, action: Action) {
    switch (action.type) {
        case PhotoDownloadSuccessActionTypes.SETDOWNPHOTO:
            return {
                ...store,
                photoDownloadSuccess: (action as PhotoDownloadSuccessAction).photoDownloadSuccess
            };

        default:
            return store;
    }
}

export declare interface PhotoDownloadSuccess {
    // 撮影端末番号
    photoTerminalNo?: string;
    // 撮影日
    photoDate?: string;


    photoPathLocal?: string;


}
