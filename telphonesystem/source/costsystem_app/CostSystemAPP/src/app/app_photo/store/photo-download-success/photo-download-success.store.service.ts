import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
    PhotoDownloadSuccessStore,
    PhotoDownloadSuccessAction, PhotoDownloadSuccess
} from './photo-download-success.store';

@Injectable()
export class PhotoDownloadSuccessService {
    public photoDownloadSuccessStore: PhotoDownloadSuccessStore;
    constructor(
        private store: Store<PhotoDownloadSuccessStore>,
        private storeSubscribe: Store<{ photoDownloadSuccessStore: PhotoDownloadSuccessStore }>
    ) {

    }

    public setDownloadSuccessPhotoAction(photo: PhotoDownloadSuccess) {
        this.store.dispatch(new PhotoDownloadSuccessAction(photo));
    }



}
