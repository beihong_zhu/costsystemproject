import { Injectable } from '@angular/core';
import { PhotobookInfoStore, SetPhotobookInfoAction, ClearbookInfoAction } from './photobook-info-set.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class PhotobookInfoSetStoreService {

  public photobookInfoStore: PhotobookInfoStore;

  constructor(
    private createdStore: Store<PhotobookInfoStore>,
    private storeSubscribe: Store<{ photobookInfoStore: PhotobookInfoStore }>
  ) {

    this.storeSubscribe.pipe(select('photobookInfoStore')).subscribe((store => {
      if (this.photobookInfoStore) {
        this.photobookInfoStore.photobookInfos = store.photobookInfos;
        this.photobookInfoStore.chooseBuildingName = store.chooseBuildingName;
        this.photobookInfoStore.chooseConstructionName = store.chooseConstructionName;
        this.photobookInfoStore.chooseLargeProcessName = store.chooseLargeProcessName;
        this.photobookInfoStore.outputOrder = store.outputOrder;
        this.photobookInfoStore.pageCnt = store.pageCnt;
        this.photobookInfoStore.pageId = store.pageId;
        this.photobookInfoStore.chooseLargeProcessId = store.chooseLargeProcessId;
        this.photobookInfoStore.unknownProcessOutput = store.unknownProcessOutput;
      } else {
        this.photobookInfoStore = store;
      }
      // console.log('this.storeSubscribe.photobookInfoStore', this.photobookInfoStore);
    }));

  }

  public setPhotobookInfo(array) {
    this.createdStore.dispatch(new SetPhotobookInfoAction(array));
  }

  public clearBookInfoset() {
    this.createdStore.dispatch(new ClearbookInfoAction());
  }
}
