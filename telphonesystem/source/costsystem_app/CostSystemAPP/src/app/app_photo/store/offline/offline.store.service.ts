import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PhotoList0501 } from '@service/dto';
import { Drawing } from '@service/dto/if-01-08-out-dto';
import { OfflineStore, OfflineSetAPIAllDownloadAction, OfflineSetPhotosDownloadAction, OfflineSetPdfDownloadAction } from './offline.store';

@Injectable()
export class OfflineStoreService {
  constructor(private store: Store<{ offlineStore: OfflineStore }>,
  ) { }


  public offlineStoreSubscription(callback) {

    return this.store.pipe(select('offlineStore')).subscribe(state => {

      callback(state);

    });
  }

  public setAPIAllDownload(flg: boolean) {
    this.store.dispatch(new OfflineSetAPIAllDownloadAction(flg));
  }

  public setPhotosDownload(photosDownLoad: PhotoList0501[]) {
    this.store.dispatch(new OfflineSetPhotosDownloadAction(photosDownLoad));
  }


  public setPdfDownload(pdfDownLoad: Drawing[]) {
    this.store.dispatch(new OfflineSetPdfDownloadAction(pdfDownLoad));
  }

}
