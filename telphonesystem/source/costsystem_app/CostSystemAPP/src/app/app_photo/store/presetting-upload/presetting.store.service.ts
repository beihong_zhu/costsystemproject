import { Injectable } from '@angular/core';
import { PreSettingStore, SetPreSettingAction } from './presetting.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class PreSettingStoreService {

  public preSettingStore: PreSettingStore;

  constructor(
    private createdStore: Store<PreSettingStore>,
    private storeSubscribe: Store<{ preSettingStore: PreSettingStore }>
  ) {
    this.storeSubscribe.pipe(select('preSettingStore')).subscribe((store => {
      if (this.preSettingStore) {
        this.preSettingStore.targetBuildingName = store.targetBuildingName;
        this.preSettingStore.targetBuildingId = store.targetBuildingId;
        this.preSettingStore.targetProjectName = store.targetProjectName;
        this.preSettingStore.settingTargetName = store.settingTargetName;
        this.preSettingStore.inputCsvPath = store.inputCsvPath;
        this.preSettingStore.file = store.file;
        this.preSettingStore.csvKbn = store.csvKbn;
        this.preSettingStore.userCsvData = store.userCsvData;
        this.preSettingStore.bukenCsvData = store.bukenCsvData;
        this.preSettingStore.bukenHeyaBasyoCsvData = store.bukenHeyaBasyoCsvData;
        this.preSettingStore.ankenTotalCsvData = store.ankenTotalCsvData;
        this.preSettingStore.message = store.message;
        this.preSettingStore.pdfFiles = store.pdfFiles;
        this.preSettingStore.settingTargetId = store.settingTargetId;
        this.preSettingStore.confirmSuccess = store.confirmSuccess;
      } else {
        this.preSettingStore = store;
      }
      // console.log('this.storeSubscribe.preSettingStore', this.preSettingStore);
    }));

  }

  public setPreSetting(array) {
    this.createdStore.dispatch(new SetPreSettingAction(array));
  }

}
