import { Action } from '@ngrx/store';

export class Project {
  projectId: number;
  projectName: string;
}

export class ProjectListStore {
  projectlist: Project[];
}

const initialStore: ProjectListStore = {
  projectlist: [],
};

const ProjectlistActionTypes = {
  SETPROJECTLIST: 'SETPROJECTLIST',
};

export class SetProjectListAction implements Action {
  readonly type = ProjectlistActionTypes.SETPROJECTLIST;
  constructor(public projectlist: Project[]) { }
}

// reducer
export function ProjectReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case ProjectlistActionTypes.SETPROJECTLIST:
      return {
        ...store,
        projectlist: (action as SetProjectListAction).projectlist
      };
    default:
      return store;
  }
}

