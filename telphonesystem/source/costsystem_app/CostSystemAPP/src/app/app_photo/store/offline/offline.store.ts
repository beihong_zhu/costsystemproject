import { Action } from '@ngrx/store';
import { PhotoList0501 } from '@service/dto';
import { Drawing } from '@service/dto/if-01-08-out-dto';

export class OfflineStore {
  // 全てAPI download 完了か
  APIAllDownload: boolean;

  // photosDownload download 必要な写真
  photosDownLoad: PhotoList0501[];

  // pdf download
  pdfDownLoad: Drawing[];

}

const initialStore: OfflineStore = {
  APIAllDownload: false,
  photosDownLoad: [],
  pdfDownLoad: [],
};

const OfflineActionTypes = {
  SETAPIAllDownload: 'OFFLINE_APIAllDownload',
  SETPhotosDownload: 'OFFLINE_PhotosDownload',
  SETPdfDownLoad: 'OFFLINE_PdfDownLoad',

};

export class OfflineSetAPIAllDownloadAction implements Action {
  readonly type = OfflineActionTypes.SETAPIAllDownload;
  constructor(public flg: boolean) { }
}

export class OfflineSetPhotosDownloadAction implements Action {
  readonly type = OfflineActionTypes.SETPhotosDownload;
  constructor(public photosDownLoad: Array<PhotoList0501>) { }
}

export class OfflineSetPdfDownloadAction implements Action {
  readonly type = OfflineActionTypes.SETPdfDownLoad;
  constructor(public pdfDownLoad: Array<Drawing>) { }
}

// reducer
export function OfflineStoreReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case OfflineActionTypes.SETAPIAllDownload:
      return {
        ...store,
        APIAllDownload: (action as OfflineSetAPIAllDownloadAction).flg
      };
    case OfflineActionTypes.SETPhotosDownload:
      return {
        ...store,
        photosDownLoad: (action as OfflineSetPhotosDownloadAction).photosDownLoad
      };
    case OfflineActionTypes.SETPdfDownLoad:
      return {
        ...store,
        pdfDownLoad: (action as OfflineSetPdfDownloadAction).pdfDownLoad
      };
    default:
      return store;
  }
}

