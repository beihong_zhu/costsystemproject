import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
    AutoUploadPhotoStore,
    AutoUploadPhotoAction,
    AutoUploadPhoto
} from './auto-upload-photo.store';

@Injectable()
export class AutoUploadPhotoService {
    public autoUploadPhotoStore: AutoUploadPhotoStore;
    constructor(
        private store: Store<AutoUploadPhotoStore>,
        private storeSubscribe: Store<{ autoUploadPhotoStore: AutoUploadPhotoStore }>
    ) {

    }

    public setAutoUploadPhotoAction(photo: AutoUploadPhoto) {
        this.store.dispatch(new AutoUploadPhotoAction(photo));
    }



}
