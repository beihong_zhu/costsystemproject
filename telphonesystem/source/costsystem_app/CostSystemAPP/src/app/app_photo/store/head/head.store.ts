import { Action } from '@ngrx/store';

// initial state
export class HeadState {
  title: string;
  isBack: boolean;
  isMenu: boolean;
  menuFunction: () => void;
  moreFunction: () => void;
  menuDisable: boolean;
  backRoute: string;
  isMoreMenu: boolean;
  isEditMenu: boolean;
  isLoad: boolean;
  editFunction: () => void;
  backFunction: () => void;
  isAddMenu: boolean;
  isCloseMenu: boolean;
  isSettingMenu: boolean;

}

const initialState: HeadState = {
  title: '',
  isBack: false,
  isMenu: false,
  menuFunction: null,
  moreFunction: null,
  editFunction: null,
  backFunction: null,
  menuDisable: false,
  backRoute: '',
  isMoreMenu: false,
  isEditMenu: false,
  isLoad: false,
  isAddMenu: false,
  isCloseMenu: false,
  isSettingMenu: false,
};

const HeadActionTypes = {
  SETTITLE: 'HEAD_SETTITLE',
  SETISBACK: 'HEAD_SETISBACK',
  SETISMENU: 'HEAD_SETISMENU',
  SETMENUFUNCTION: 'HEAD_SETMENUFUNCTION',
  SETMOREFUNCTION: 'HEAD_SETMOREFUNCTION',
  SETEDITMENUFUNCTION: 'HEAD_SETEDITMENUFUNCTION',
  SETBACKFUNCTION: 'HEAD_SETBACKFUNCTION',
  SETMENUDISABLE: 'HEAD_SETMENUDISABLE',
  SETBACKROUTE: 'HEAD_SETBACKROUTE',
  SETISMOREMENU: 'HEAD_SETISMOREMENU',
  SETISEDITMENU: 'HEAD_SETISEDITMENU',
  SETISEDISLOAD: 'HEAD_SETISEDISLOAD',
  SETISADDMENU: 'HEAD_SETISADDMENU',
  SETISCLOSEMENU: 'HEAD_SETISCLOSEMENU',
  SETISSETTINGMENU: 'HEAD_SETISSETTINGMENU'
};

export class HeadSetTitleAction implements Action {
  readonly type = HeadActionTypes.SETTITLE;
  constructor(public title: string) { }
}

export class HeadSetBackRouteAction implements Action {
  readonly type = HeadActionTypes.SETBACKROUTE;
  constructor(public backRoute: string) { }
}

export class HeadSetIsBackAction implements Action {
  readonly type = HeadActionTypes.SETISBACK;
  constructor(public isBack: boolean) { }
}

export class HeadSetIsMenuAction implements Action {
  readonly type = HeadActionTypes.SETISMENU;
  constructor(public isMenu: boolean) { }
}

export class HeadSetIsEditAction implements Action {
  readonly type = HeadActionTypes.SETISEDITMENU;
  constructor(public isEditMenu: boolean) { }
}

export class HeadSetIsLoadAction implements Action {
  readonly type = HeadActionTypes.SETISEDISLOAD;
  constructor(public isLoad: boolean) { }
}

export class HeadSetIsMoreMenuAction implements Action {
  readonly type = HeadActionTypes.SETISMOREMENU;
  constructor(public isMoreMenu: boolean) { }
}

export class HeadSetMenuAction implements Action {
  readonly type = HeadActionTypes.SETMENUFUNCTION;
  constructor(public menuFunction: () => void) { }
}

export class HeadSetMoreAction implements Action {
  readonly type = HeadActionTypes.SETMOREFUNCTION;
  constructor(public moreFunction: () => void) { }
}

export class HeadSetEditAction implements Action {
  readonly type = HeadActionTypes.SETEDITMENUFUNCTION;
  constructor(public editFunction: () => void) { }
}

export class HeadSetBackAction implements Action {
  readonly type = HeadActionTypes.SETBACKFUNCTION;
  constructor(public backFunction: () => void) { }
}

export class HeadSetMenuDisableAction implements Action {
  readonly type = HeadActionTypes.SETMENUDISABLE;
  constructor(public menuDisable: boolean) { }
}
export class HeadSetAddMenuAction implements Action {
  readonly type = HeadActionTypes.SETISADDMENU;
  constructor(public isAddMenu: boolean) { }
}
export class HeadSetIsCloseMenuAction implements Action {
  readonly type = HeadActionTypes.SETISCLOSEMENU;
  constructor(public isCloseMenu: boolean) { }
}
export class HeadSetIsSettingMenuAction implements Action {
  readonly type = HeadActionTypes.SETISSETTINGMENU;
  constructor(public isSettingMenu: boolean) { }
}
// reducer
export function HeadReducer(state = initialState, action: Action) {
  switch (action.type) {
    case HeadActionTypes.SETTITLE:
      return {
        ...state,
        title: (action as HeadSetTitleAction).title
      };
    case HeadActionTypes.SETBACKROUTE:
      return {
        ...state,
        backRoute: (action as HeadSetBackRouteAction).backRoute
      };
    case HeadActionTypes.SETISBACK:
      return {
        ...state,
        isBack: (action as HeadSetIsBackAction).isBack
      };
    case HeadActionTypes.SETISMENU:
      return {
        ...state,
        isMenu: (action as HeadSetIsMenuAction).isMenu
      };
    case HeadActionTypes.SETISEDITMENU:
      return {
        ...state,
        isEditMenu: (action as HeadSetIsEditAction).isEditMenu
      };
    case HeadActionTypes.SETISEDISLOAD:
      return {
        ...state,
        isLoad: (action as HeadSetIsLoadAction).isLoad
      };
    case HeadActionTypes.SETISMOREMENU:
      return {
        ...state,
        isMoreMenu: (action as HeadSetIsMoreMenuAction).isMoreMenu
      };
    case HeadActionTypes.SETMENUFUNCTION:
      return {
        ...state,
        menuFunction: (action as HeadSetMenuAction).menuFunction
      };
    case HeadActionTypes.SETMOREFUNCTION:
      return {
        ...state,
        moreFunction: (action as HeadSetMoreAction).moreFunction
      };
    case HeadActionTypes.SETEDITMENUFUNCTION:
      return {
        ...state,
        editFunction: (action as HeadSetEditAction).editFunction
      };
    case HeadActionTypes.SETBACKFUNCTION:
      return {
        ...state,
        backFunction: (action as HeadSetBackAction).backFunction
      };
    case HeadActionTypes.SETMENUDISABLE:
      return {
        ...state,
        menuDisable: (action as HeadSetMenuDisableAction).menuDisable
      };
    case HeadActionTypes.SETISADDMENU:
      return {
        ...state,
        isAddMenu: (action as HeadSetAddMenuAction).isAddMenu
      };
    case HeadActionTypes.SETISCLOSEMENU:
      return {
        ...state,
        isCloseMenu: (action as HeadSetIsCloseMenuAction).isCloseMenu
      };
    case HeadActionTypes.SETISSETTINGMENU:
      return {
        ...state,
        isSettingMenu: (action as HeadSetIsSettingMenuAction).isSettingMenu
      };
    default:
      return state;
  }
}

