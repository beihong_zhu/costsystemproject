import { Action } from '@ngrx/store';
import { LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';

// Initial store
export class PhotoDetailStore {
  detailPhoto: LocalPhoto;
  detailPhotoList: Array<LocalPhoto>;

}

const initialStore: PhotoDetailStore = {
  detailPhoto: null,
  detailPhotoList: [],
};

const PhotoDetailStoreActionTypes = {
  setDetailPhoto: 'SET_DETAILPHOTO',
  setDetailPhotoList: 'SET_DETAILPHOTOLIST'
};

export class PhotoDetailStoreDetailPhotoAction implements Action {
  readonly type = PhotoDetailStoreActionTypes.setDetailPhoto;
  constructor(public detailPhoto: LocalPhoto) { }
}

export class PhotoDetailStoreDetailPhotoListAction implements Action {
  readonly type = PhotoDetailStoreActionTypes.setDetailPhotoList;
  constructor(public detailPhotoList: LocalPhoto[]) { }
}
// reducer
export function PhotoDetailStoreReducer(store = initialStore, action: Action) {

  switch (action.type) {
    case PhotoDetailStoreActionTypes.setDetailPhoto:
      return {
        ...store,
        detailPhoto: (action as PhotoDetailStoreDetailPhotoAction).detailPhoto
      };
    case PhotoDetailStoreActionTypes.setDetailPhotoList:
      return {
        ...store,
        detailPhotoList: (action as PhotoDetailStoreDetailPhotoListAction).detailPhotoList
      };
    default:
      return store;
  }
}
