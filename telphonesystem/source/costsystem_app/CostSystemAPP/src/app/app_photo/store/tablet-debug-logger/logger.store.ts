import { Action } from '@ngrx/store';

export class LoggerStore {
  messageArray: Array<string>;
}

const initialStore: LoggerStore = {
  messageArray: []
};

const LoggerActionTypes = {
  ADDLOGGER: 'LOGGER/ADDLOGGER'
};

export class LoggerAddloggerAction implements Action {
  readonly type = LoggerActionTypes.ADDLOGGER;
  constructor(public logger: string) { }
}

export function LoggerReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case LoggerActionTypes.ADDLOGGER:
      return {
        ...store,
        messageArray: store.messageArray.concat((action as LoggerAddloggerAction).logger)
      };
    default:
      return store;
  }
}

