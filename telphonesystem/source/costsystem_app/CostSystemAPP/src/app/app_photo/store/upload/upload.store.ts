import { Action } from '@ngrx/store';

export class UploadStore {

  // 自動同期アップの写真
  photosUpload: any[];

  // 手動同期アップの写真
  photosManualUpload: any[];

}

const initialStore: UploadStore = {
  photosUpload: [],
  photosManualUpload: []

};

const UploadActionTypes = {
  SetPhotosUpload: 'UPLOAD_PHOTOS',
  SetPhotosManualUpload: 'MANUAL_UPLOAD_PHOTOS'

};

export class SetPhototsUploadAction implements Action {
  readonly type = UploadActionTypes.SetPhotosUpload;
  constructor(public photosUpload: []) { }
}

export class SetPhototsManualUploadAction implements Action {
  readonly type = UploadActionTypes.SetPhotosManualUpload;
  constructor(public photosManualUpload: []) { }
}

// reducer
export function UploadStoreReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case UploadActionTypes.SetPhotosUpload:
      return {
        ...store,
        photosUpload: (action as SetPhototsUploadAction).photosUpload
      };
    case UploadActionTypes.SetPhotosManualUpload:
      return {
        ...store,
        photosManualUpload: (action as SetPhototsManualUploadAction).photosManualUpload
      };
    default:
      return store;
  }
}

