import { Action } from '@ngrx/store';

export class DeviceTokenStore {

  // push notification deviceToken
  deviceToken?: string;
  // platfrom
  devicePlatform?: string;

}

const initialStore: DeviceTokenStore = {
  deviceToken: null,
  devicePlatform: null
};

const DeviceTokenActionTypes = {
  SET_DEVICETOKEN: 'SET_DEVICETOKEN',
  SET_DEVICEPLATFORM: 'SET_DEVICEPLATFORM',
};

export class SetDeviceTokenAction implements Action {
  readonly type = DeviceTokenActionTypes.SET_DEVICETOKEN;
  constructor(public deviceToken: string) { }
}

export class SetDevicePlatformAction implements Action {
  readonly type = DeviceTokenActionTypes.SET_DEVICEPLATFORM;
  constructor(public devicePlatform: string) { }
}

// reducer
export function DeviceTokenReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case DeviceTokenActionTypes.SET_DEVICETOKEN:
      return {
        ...store,
        deviceToken: (action as SetDeviceTokenAction).deviceToken
      };
    case DeviceTokenActionTypes.SET_DEVICEPLATFORM:
      return {
        ...store,
        devicePlatform: (action as SetDevicePlatformAction).devicePlatform
      };
    default:
      return store;
  }
}

