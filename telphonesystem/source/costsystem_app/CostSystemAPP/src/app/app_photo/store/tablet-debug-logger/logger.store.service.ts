import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { LoggerAddloggerAction, LoggerStore } from './logger.store';
import * as moment from 'moment';
import { environment } from '@env';

@Injectable()
export class LoggerStoreService {

  systemErrorInfo = '';

  constructor(
    private store: Store<LoggerStore>,
    private storeSubscription: Store<{ loggerStore: LoggerStore }>) { }

  public loggerStoreSubscription(callback) {
    return this.storeSubscription.pipe(select('loggerStore')).subscribe((state => callback(state)));
  }

  public addLogger(page, logger) {
    if (!environment.debug || true) {
      return;
    }

    let loggerString;
    if (logger instanceof Object || logger instanceof Array) {
      loggerString = JSON.stringify(logger);
    } else {
      loggerString = logger;
    }
    console.log('loggerString', loggerString);

    const logDate = moment(new Date()).format('YYYY MM/DD HH:mm:ss');
    const loggerAdd = '[' + page + '  ' + logDate + ']' + '  ' + loggerString;
    this.store.dispatch(new LoggerAddloggerAction(loggerAdd));
  }

}
