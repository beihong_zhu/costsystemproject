import { Action } from '@ngrx/store';

export class RegularInfoStore {
  sysUserList: SysUserList[];
}

export class SysUserList {
  // 用户ID
  userId?: string;
  // 用户名
  userName?: string;
  // 公司名
  companyName?: string;
  // 权限ID
  authorityId?: string;
  // 邮箱地址
  mailAddress?: string;
  // 登录flg
  isLogin?: number;
}

const initialStore: RegularInfoStore = {
  sysUserList: []
};

const RegularInfoActionTypes = {
  RegularInfo_DATA: 'RegularInfo_DATA',
};

export class SetRegularInfoAction implements Action {
  readonly type = RegularInfoActionTypes.RegularInfo_DATA;
  constructor(public RegularInfoStore: RegularInfoStore) { }
}

// reducer
export function RegularInfoReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case RegularInfoActionTypes.RegularInfo_DATA:
      return {
        ...store,
        sysUserList: (action as SetRegularInfoAction).RegularInfoStore.sysUserList
      };
    default:
      return store;
  }
}
