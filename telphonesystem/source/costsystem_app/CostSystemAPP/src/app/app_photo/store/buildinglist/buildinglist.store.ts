import { Action } from '@ngrx/store';

export class Building {
  buildingId: number;
  buildingName: string;
  buildingAddress: string;
}

export class BuildingListStore {
  buildinglist: Building[];
}

const initialStore: BuildingListStore = {
  buildinglist: [],
};

const BuildinglistActionTypes = {
  SETBUILDINGLIST: 'SETBUILDINGLIST',
};

export class SetBuildingListAction implements Action {
  readonly type = BuildinglistActionTypes.SETBUILDINGLIST;
  constructor(public buildinglist: Building[]) { }
}

// reducer
export function BuildingListReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case BuildinglistActionTypes.SETBUILDINGLIST:
      return {
        ...store,
        buildinglist: (action as SetBuildingListAction).buildinglist
      };
    default:
      return store;
  }
}

