import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { SetOfflineUnitsAction, OfflineUnitsStore, Unit } from './offline-units.store';
import { PhotoConf } from '@conf/photo-conf';

@Injectable()
export class OfflineUnitsService {

  public offlineUnitsStore: OfflineUnitsStore;

  constructor(private store: Store<OfflineUnitsStore>,
              private storeSubscribe: Store<{ offlineUnitsStore: OfflineUnitsStore }>) {
    this.storeSubscribe.pipe(select('offlineUnitsStore')).subscribe((state => {
      this.offlineUnitsStore = state;
    }));
  }

  public setOfflineUnits(array) {
    this.store.dispatch(new SetOfflineUnitsAction(array));
  }

  public resetUnitsRetryCount() {
    if (this.offlineUnitsStore && this.offlineUnitsStore.units) {
      for (const unit of this.offlineUnitsStore.units) {
        if (unit.retryCount === PhotoConf.RETRY_MAX_COUNT) {
          unit.retryCount = 0;
        }
      }
      this.setOfflineUnits(this.offlineUnitsStore.units);
    }
  }

  public updateUnitsRetryCount(unitId: number) {
    if (this.offlineUnitsStore && this.offlineUnitsStore.units) {
      for (const unit of this.offlineUnitsStore.units) {
        if (unit.unitId === unitId) {
          unit.retryCount = PhotoConf.RETRY_MAX_COUNT;
          this.setOfflineUnits(this.offlineUnitsStore.units);
          break;
        }
      }
    }
  }

  public pushUnit(gamenUnit: Unit) {
    if (this.offlineUnitsStore && this.offlineUnitsStore.units) {
      for (const unit of this.offlineUnitsStore.units) {
        if (unit.unitId === gamenUnit.unitId) {
          unit.serialNumber = gamenUnit.serialNumber;
          unit.modeltype = gamenUnit.modeltype;
          unit.retryCount = 0;
          this.setOfflineUnits(this.offlineUnitsStore.units);
          return;
        }
      }
      this.offlineUnitsStore.units.push(gamenUnit);
      this.setOfflineUnits(this.offlineUnitsStore.units);
    } else {
      this.setOfflineUnits([gamenUnit]);
    }
  }

  public deleteUnit(unitId: number) {
    if (this.offlineUnitsStore && this.offlineUnitsStore.units) {
      const unitsLength = this.offlineUnitsStore.units.length;
      for (let i = 0; i < unitsLength; i++) {
        if (this.offlineUnitsStore.units[i].unitId === unitId) {
          this.offlineUnitsStore.units.splice(i, 1);
          this.setOfflineUnits(this.offlineUnitsStore.units);
          return;
        }
      }
    }
  }
}
