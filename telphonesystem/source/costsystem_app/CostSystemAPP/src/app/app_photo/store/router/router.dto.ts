import { StoreModule, ActionReducerMap } from '@ngrx/store';
import { Params, RouterStateSnapshot, Data } from '@angular/router';
import {
  StoreRouterConnectingModule,
  routerReducer,
  RouterReducerState,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { Injectable, ErrorHandler } from '@angular/core';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

export interface State {
  router: RouterReducerState<RouterStateUrl>;
}

export interface AppRouterState {
  url: string;
  params: Params;
  queryParams: Params;
  data: Data;
}

@Injectable()
export class CustomSerializer implements RouterStateSerializer<AppRouterState> {
  serialize(routerState: RouterStateSnapshot): AppRouterState {
    let route = routerState.root;
    while (route.firstChild) {
      route = route.firstChild;
    }

    const {
      url,
      root: { queryParams },
    } = routerState;

    const {
      params,
      data
    } = route;

    return { url, params, queryParams, data };
  }
}

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
};
