import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { DrawOfflineStore, UPDATEAPI0605Action } from './draw-offline.store';
import { API0605OutDto } from '@service/dto';
import { Machine } from '@service/dto/if-01-06-out-dto';

@Injectable()
export class DrawOfflineStoreService {

  public drawOfflineStore: DrawOfflineStore;

  constructor(
    private createdStore: Store<DrawOfflineStore>,
    private storeSubscribe: Store<{ drawOfflineStore: DrawOfflineStore }>
  ) {

    this.storeSubscribe.pipe(select('drawOfflineStore')).subscribe((store => {
      if (this.drawOfflineStore) {
        this.drawOfflineStore.api0605OutDto = store.api0605OutDto;
      } else {
        this.drawOfflineStore = store;
      }
    }));

  }



  public updateAPI0605(api0605OutDto: API0605OutDto) {
    this.createdStore.dispatch(new UPDATEAPI0605Action(api0605OutDto));
  }
}
