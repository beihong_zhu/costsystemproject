import { Action } from '@ngrx/store';

export class DeviceManage {
  // アクセス許可フラグ
  isAccessApproval?: number;
  // ユーザID
  userId?: string;
  // 端末コード
  deviceCode?: string;
  // デバイス登録日時
  dateMoment?: string;
}

export class DeviceManageListStore {
  deviceManageList: DeviceManage[];
}

const initialStore: DeviceManageListStore = {
  deviceManageList: [],
};

const DeviceManageListActionTypes = {
  SETDEVICEMANAGELIST: 'SETDEVICEMANAGELIST',
};

export class SetDeviceManageListAction implements Action {
  readonly type = DeviceManageListActionTypes.SETDEVICEMANAGELIST;
  constructor(public deviceManageList: DeviceManage[]) { }
}

// reducer
export function DeviceManageListReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case DeviceManageListActionTypes.SETDEVICEMANAGELIST:
      return {
        ...store,
        deviceManageList: (action as SetDeviceManageListAction).deviceManageList
      };
    default:
      return store;
  }
}

