import { Injectable } from '@angular/core';
import { DeviceTokenStore, SetDevicePlatformAction, SetDeviceTokenAction } from './device-token.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class DeviceTokenStoreService {

  public deviceTokenStore: DeviceTokenStore;

  constructor(
    private storeSubscribe: Store<{ deviceTokenStore: DeviceTokenStore }>,
  ) {


  }

  public deviceTokenStoreSubscription(callback) {
    return this.storeSubscribe.pipe(select('deviceTokenStore')).subscribe(state => callback(state));
  }

  public setDeviceToken(deviceToken) {
    this.storeSubscribe.dispatch(new SetDeviceTokenAction(deviceToken));
  }

  public setDevicePlatfrom(devicePlatform) {
    this.storeSubscribe.dispatch(new SetDevicePlatformAction(devicePlatform));
  }

}
