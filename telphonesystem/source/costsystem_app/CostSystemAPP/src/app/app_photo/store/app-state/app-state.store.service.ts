import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Store, select } from '@ngrx/store';
import {
  AppState,
  APPSetIsPCAction,
  APPSetIsWinAppAction,
  APPSetIsOfflineAction
} from './app-state.store';

@Injectable()
export class AppStateStoreService {

  public appState: AppState;

  readonly responsiveMobile = () => {
    const deviceWidth = document.documentElement.clientWidth;
    document.documentElement.style.fontSize = deviceWidth / 3.75 + 'px';
  }

  readonly responsiveWeb = () => {
    const deviceWidth = document.documentElement.clientWidth;
    document.documentElement.style.fontSize = deviceWidth / 19.2 + 'px';
  }

  constructor(
    private store: Store<AppState>,
    private storeSubscribe: Store<{ appState: AppState }>,
    private platform: Platform
  ) {
    this.storeSubscribe.pipe(select('appState')).subscribe(state => {
      if (this.appState) {
        this.appState.isPC = state.isPC;
        this.appState.isWinApp = state.isWinApp;
        this.appState.isOffline = state.isOffline;
      } else {
        this.appState = state;
      }
    });
  }

  public appStateStoreSubscription(callback) {
    return this.storeSubscribe.pipe(select('appState')).subscribe(state => callback(state));
  }

  public platformCheck() {
    // タッチスクリーンではない場合、isPCにtrueを設定する
    let isPC = this.platform.is('desktop');
    const userAgent = window.navigator.userAgent;
    // windows端末の場合
    if (/windows/i.test(userAgent)) {
      if (/msapphost/i.test(userAgent)) {
        //  windowsアプリの場合、isPCにfalseを設定する
        isPC = false;
        this.setIsWinApp(true);
      } else {
        // windowsブラウザの場合、isPCにtrueを設定する
        isPC = true;
      }
    }
    this.setIsPC(isPC);
  }

  public responsiveRem() {

    const isPC = this.platform.is('desktop');
    const userAgent = window.navigator.userAgent;

    if (/windows/i.test(userAgent)) {
      this.responsiveWeb();
      window.onresize = () => {
        this.responsiveWeb();
      };
    } else {
      if (isPC) {
        this.responsiveWeb();
        window.onresize = () => {
          this.responsiveWeb();
        };
      } else {
        this.responsiveMobile();
        window.onresize = () => {
          this.responsiveMobile();
        };
      }
    }
  }

  public getPlatform(): 'mobile' | 'web' | 'winapp' {
    if (this.appState.isPC) {
      return 'web';
    } else {
      if (this.appState.isWinApp) {
        return 'winapp';
      } else {
        return 'mobile';
      }
    }
  }

  public setIsPC(flg: boolean) {
    this.store.dispatch(new APPSetIsPCAction(flg));
  }

  public setIsWinApp(flg: boolean) {
    this.store.dispatch(new APPSetIsWinAppAction(flg));
  }

  public setIsOffline(flg: boolean) {
    this.store.dispatch(new APPSetIsOfflineAction(flg));
  }

}
