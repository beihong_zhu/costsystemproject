import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ProcessDto } from '@pages/process/process-create/dto';
import { ProcessCreateState, UPDATEPROCESSLISTAction } from './process.store';

@Injectable()
export class ProcessStoreService {

  public processCreateState: ProcessCreateState;

  constructor(public store: Store<{ processes: ProcessCreateState }>) {
    this.store.pipe(select('processes')).subscribe((state => {
      if (this.processCreateState) {
        this.processCreateState.processes = state.processes;
        this.processCreateState.selectProject = state.selectProject;
        this.processCreateState.bigProcessId = state.bigProcessId;
      } else {
        this.processCreateState = state;
      }
    }));
  }

  public updateProcessList(processes: Array<ProcessDto>, selectProject: number, bigProcessId: string) {
    this.store.dispatch(new UPDATEPROCESSLISTAction(processes, selectProject, bigProcessId));
  }
}
