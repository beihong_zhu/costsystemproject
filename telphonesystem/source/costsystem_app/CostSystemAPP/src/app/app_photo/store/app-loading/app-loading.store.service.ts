import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppLoading, AppLoadingAction } from './app-loading.store';

@Injectable()
export class AppLoadingStoreService {
  constructor(private store: Store<AppLoading>,
    private storeSubscription: Store<{ appLoading: AppLoading }>
  ) { }

  public isLoadingSubscription(callback) {
    return this.storeSubscription.pipe(select('appLoading')).subscribe((state => callback(state)));
  }

  public isloading(isLoading: boolean) {
    this.store.dispatch(new AppLoadingAction(isLoading));
  }

}
