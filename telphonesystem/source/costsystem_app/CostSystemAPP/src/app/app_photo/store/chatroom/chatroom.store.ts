import { Action } from '@ngrx/store';
import { Message, User, SelectUser, UserProjectConversations, ChatRoomUsers, InitSelectUsers } from '@pages/chatroom/dto/chatroomdto';

export interface Photopath {
  chatTalkPictures?: string;
  isChecked?: false;
  pictureID?: string;
  relativePath?: string;
}

export interface ReadMessageTableColumn {
  conversationId: string;
  createdAt: string;
  messageId: string;
  userId: string;
}


export class ChatRoomStore {
  chatMessages: Message[];
  chatUsers: User[];
  chatSelectUsers: SelectUser[];
  chatAssociated: ChatRoomUsers[];
  currentConversation: UserProjectConversations;
  conversationId: string;
  conversationName: string;
  initSelectUsers: InitSelectUsers[];
}

const initialStore: ChatRoomStore = {
  chatMessages: [],
  chatUsers: [],
  chatSelectUsers: [],
  chatAssociated: [],
  conversationId: null,
  conversationName: null,
  currentConversation: null,
  initSelectUsers: [],
};

const ChatActionTypes = {
  ADD_NEW_MESSAGE: 'CHAT/ADD_NEW_MESSAGE',
  ADD_READUSERS: 'CHAT/ADD_READUSERS',

  SET_CHAT_MESSAGES: 'CHAT/SET_CHAT_MESSAGES',
  SET_CHAT_USERS: 'CHAT/SET_CHAT_USERS',
  SET_CHAT_SELECTUSERS: 'CHAT/SET_CHAT_SELECTUSERS',
  SET_CHAT_ASSOCIATED: 'CHAT/SET_CHAT_ASSOCIATED',


  SET_CHAT_CONVERSATIONS: 'CHAT/SET_CHAT_CONVERSATIONS',
  SET_CHAT_CONVERSATION_ID: 'CHAT/SET_CHAT_CONVERSATION_ID',
  SET_CHAT_CONVERSATION_NAME: 'CHAT/SET_CHAT_CONVERSATION_NAME',
  SET_CHAT_CURRENT_CONVERSATION: 'CHAT/SET_CHAT_CURRENT_CONVERSATION',

  SET_INIT_SELECTUSERS: 'CHAT/SET_INIT_SELECTUSERS',

};

export class AddNewMessagesAction implements Action {
  readonly type = ChatActionTypes.ADD_NEW_MESSAGE;
  constructor(public newMessage: []) { }

}

export class AddReadUsersAction implements Action {
  readonly type = ChatActionTypes.ADD_READUSERS;
  constructor(public markAsReadMessages: []) { }

}

export class SetChatMessagesAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_MESSAGES;
  constructor(public chatMessages: []) { }

}

export class SetChatUsersAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_USERS;
  constructor(public chatUsers: []) { }

}

export class SetChatSelectUsersAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_SELECTUSERS;
  constructor(public chatSelectUsers: []) { }

}


export class SetChatConversationIdAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_CONVERSATION_ID;
  constructor(public conversationId: string) { }
}

export class SetChatConversationNameAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_CONVERSATION_NAME;
  constructor(public conversationName: string) { }
}

export class SetChatAssociatedAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_ASSOCIATED;
  constructor(public chatAssociated: []) { }
}

export class SetChatCurrentConversationAction implements Action {
  readonly type = ChatActionTypes.SET_CHAT_CURRENT_CONVERSATION;
  constructor(public currentConversation: UserProjectConversations) { }
}


export class SetInitSelectUsersAction implements Action {
  readonly type = ChatActionTypes.SET_INIT_SELECTUSERS;
  constructor(public initSelectUsers: []) { }

}


// reducer
export function ChatRoomReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case ChatActionTypes.ADD_READUSERS:
      const markAsReadMessages: ReadMessageTableColumn[] = (action as AddReadUsersAction).markAsReadMessages;
      const messages = JSON.parse(JSON.stringify(store.chatMessages));
      for (const markAsReadMessage of markAsReadMessages) {
        messages.some(message => {
          if (message.id === markAsReadMessage.messageId) {
            if (message.readUsers instanceof Array) {
              if (message.readUsers.indexOf(markAsReadMessage.userId) === -1) {
                message.readUsers.push(markAsReadMessage.userId);
              }
            } else {
              message.readUsers = [markAsReadMessage.userId];
            }
            return true;
          }
        });
      }
      return { ...store, chatMessages: messages };
    case ChatActionTypes.ADD_NEW_MESSAGE:
      return {
        ...store,
        chatMessages: store.chatMessages.concat((action as AddNewMessagesAction).newMessage)
      };
    case ChatActionTypes.SET_CHAT_MESSAGES:
      return {
        ...store,
        chatMessages: (action as SetChatMessagesAction).chatMessages
      };
    case ChatActionTypes.SET_CHAT_USERS:
      return {
        ...store,
        chatUsers: (action as SetChatUsersAction).chatUsers
      };
    case ChatActionTypes.SET_CHAT_SELECTUSERS:
      return {
        ...store,
        chatSelectUsers: (action as SetChatSelectUsersAction).chatSelectUsers
      };
    case ChatActionTypes.SET_CHAT_ASSOCIATED:
      return {
        ...store,
        chatAssociated: (action as SetChatAssociatedAction).chatAssociated
      };
    case ChatActionTypes.SET_CHAT_CONVERSATION_ID:
      return {
        ...store,
        conversationId: (action as SetChatConversationIdAction).conversationId
      };
    case ChatActionTypes.SET_CHAT_CONVERSATION_NAME:
      return {
        ...store,
        conversationName: (action as SetChatConversationNameAction).conversationName
      };
    case ChatActionTypes.SET_CHAT_CURRENT_CONVERSATION:
      return {
        ...store,
        currentConversation: (action as SetChatCurrentConversationAction).currentConversation
      };
    case ChatActionTypes.SET_INIT_SELECTUSERS:
      return {
        ...store,
        initSelectUsers: (action as SetInitSelectUsersAction).initSelectUsers
      };
    default:
      return store;
  }
}

