import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  CameraSetCurrentFloorAction,
  CameraSetCurrentRoomAction,
  CameraSetCurrentPlaceAction,
  CameraSetCurrentMachineAction,
  CameraSetRoomsListAction,
  CameraSetPlacesListAction,
  CameraSetCurrentProcessAction,
  CameraSetCurrentTimingAction,
  CameraSetCurrentWorkAction,
  CameraSetTimingsListAction,
  CameraSetWorksListAction,
  CameraSetIsBlackboardDisplayAction,
  CameraSetBlackboardDisplayAction,
  CameraSetFreeAction,
  CameraSetFlashModeAction,
  CameraSetFlashImageAction,
  CameraSetZoomAction,
  CameraSetCurrentWitenessAction,
  CameraSetWitenessListAction,
  CameraSetPhotoPathLocalAction,
  CameraSetPhotoDateAction,
  CameraSetTerminalNoAction,
  CameraSetCurrentInitInterfaceTypeAction,
  CameraSetFromInitInterfaceTypeAction,
  CameraSetCurrentPhotoAction,
  CameraSetPhotoSimpleGridAction,
  PhotoSimpleGrid,
  CameraStore,
  InitInterfaceType,
  CameraSetDisplayMachinesListAction,
  ClearCameraStoreAction,
  CameraSetHumbnailAction
} from './camera.store';
import { Machine } from '@service/dto/if-01-06-out-dto';
import { LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';
import { TbPhoto } from '@service/db/db-manage';

@Injectable()
export class CameraStoreService {

  public cameraStore: CameraStore;
  constructor(
    private store: Store<{ cameraStore: CameraStore }>
  ) {
    this.store.pipe(select('cameraStore')).subscribe(subscribestore => {
      this.cameraStore = subscribestore;
    });
  }

  public cameraStoreSubscription(callback) {
    return this.store.pipe(select('cameraStore')).subscribe((state => callback(state)));
  }


  public setCurrentPhoto(currentPhoto: LocalPhoto) {

    this.store.dispatch(new CameraSetCurrentPhotoAction(currentPhoto));

  }

  public setCurrentFloor(currentFloor) {
    this.store.dispatch(new CameraSetCurrentFloorAction(currentFloor));
  }

  public setCurrentRoom(currentRoom) {
    this.store.dispatch(new CameraSetCurrentRoomAction(currentRoom));
  }

  public setCurrentPlace(currentPlace) {
    this.store.dispatch(new CameraSetCurrentPlaceAction(currentPlace));
  }

  public setCurrentMachine(currentMachine) {
    this.store.dispatch(new CameraSetCurrentMachineAction(currentMachine));
  }

  public setRoomsList(roomsList) {
    this.store.dispatch(new CameraSetRoomsListAction(roomsList));
  }

  public setPlacesList(placesList) {
    this.store.dispatch(new CameraSetPlacesListAction(placesList));
  }

  public setDisplayMachinesList(displayMachinesList) {
    this.store.dispatch(new CameraSetDisplayMachinesListAction(displayMachinesList));
  }

  public setCurrentProcess(currentProcess) {
    this.store.dispatch(new CameraSetCurrentProcessAction(currentProcess));
  }

  public setCurrentTiming(currentTiming) {
    this.store.dispatch(new CameraSetCurrentTimingAction(currentTiming));
  }

  public setCurrentWork(currentWork) {
    this.store.dispatch(new CameraSetCurrentWorkAction(currentWork));
  }

  public setTimingsList(timingsList) {
    this.store.dispatch(new CameraSetTimingsListAction(timingsList));
  }

  public setWorksList(worksList) {
    this.store.dispatch(new CameraSetWorksListAction(worksList));
  }

  public setIsBlackboardDisplay(isBlackboardDisplay) {
    this.store.dispatch(new CameraSetIsBlackboardDisplayAction(isBlackboardDisplay));
  }

  public setBlackboardDisplay(blackboradDisplay) {
    this.store.dispatch(new CameraSetBlackboardDisplayAction(blackboradDisplay));
  }

  public setFree(free) {
    this.store.dispatch(new CameraSetFreeAction(free));
  }

  public setFlashMode(flashMode) {
    this.store.dispatch(new CameraSetFlashModeAction(flashMode));
  }

  public setFlashImage(flashImage) {
    this.store.dispatch(new CameraSetFlashImageAction(flashImage));
  }

  public setZoom(zoom) {
    this.store.dispatch(new CameraSetZoomAction(zoom));
  }

  public setCurrentWitness(witness) {
    this.store.dispatch(new CameraSetCurrentWitenessAction(witness));
  }

  public setPhotoPathLocal(photoPathLocal) {
    this.store.dispatch(new CameraSetPhotoPathLocalAction(photoPathLocal));
  }

  public setPhotoTime(photoDate) {
    this.store.dispatch(new CameraSetPhotoDateAction(photoDate));
  }

  public setTerminalNo(termianlNo) {
    this.store.dispatch(new CameraSetTerminalNoAction(termianlNo));
  }

  public setCurrentInitInterfaceType(initInterfaceType) {
    this.store.dispatch(new CameraSetCurrentInitInterfaceTypeAction(initInterfaceType));
  }

  public setFromInitInterfaceType(initInterfaceType) {
    this.store.dispatch(new CameraSetFromInitInterfaceTypeAction(initInterfaceType));
  }

  public setWitnessList(witnessList) {
    this.store.dispatch(new CameraSetWitenessListAction(witnessList));
  }

  public setPhotoSimpleGridAction(photoSimpleGrid: PhotoSimpleGrid) {
    this.store.dispatch(new CameraSetPhotoSimpleGridAction(photoSimpleGrid));
  }

  public clearCameraStore() {
    this.store.dispatch(new ClearCameraStoreAction());
  }

  public setThumbnail(thumbnail: TbPhoto) {
    this.store.dispatch(new CameraSetHumbnailAction(thumbnail));
  }

}
