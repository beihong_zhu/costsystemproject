import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import {
  HeadState, HeadSetTitleAction, HeadSetBackRouteAction, HeadSetIsEditAction,
  HeadSetIsBackAction, HeadSetMenuAction, HeadSetMoreAction, HeadSetIsMoreMenuAction,
  HeadSetIsMenuAction, HeadSetMenuDisableAction, HeadSetEditAction, HeadSetBackAction,
  HeadSetAddMenuAction, HeadSetIsCloseMenuAction, HeadSetIsSettingMenuAction, HeadSetIsLoadAction
} from './head.store';

@Injectable()
export class HeadStateService {
  constructor(
    private store: Store<HeadState>,
    private storeSubscribe: Store<{ headState: HeadState }>) { }

  public headStateSubscribe(callback) {
    return this.storeSubscribe.pipe(select('headState')).subscribe((state => callback(state)));
  }

  public setTitle(title) {
    this.store.dispatch(new HeadSetTitleAction(title));
  }

  public setBackRoute(route) {
    this.store.dispatch(new HeadSetBackRouteAction(route));
  }

  public setIsBack(flg) {
    this.store.dispatch(new HeadSetIsBackAction(flg));
  }

  public setIsMenu(flg) {
    this.store.dispatch(new HeadSetIsMenuAction(flg));
  }

  public setIsEditMenu(flg) {
    this.store.dispatch(new HeadSetIsEditAction(flg));
  }

  public setIsLoad(flg) {
    this.store.dispatch(new HeadSetIsLoadAction(flg));
  }

  public setIsMoreMenu(flg) {
    this.store.dispatch(new HeadSetIsMoreMenuAction(flg));
  }

  public setMoreFunction(func) {
    this.store.dispatch(new HeadSetMoreAction(func));
  }

  public setMenuFunction(func) {
    this.store.dispatch(new HeadSetMenuAction(func));
  }

  public setEditFunction(func) {
    this.store.dispatch(new HeadSetEditAction(func));
  }

  public setMenuDisable(flg) {
    this.store.dispatch(new HeadSetMenuDisableAction(flg));
  }

  public setBackFunction(func) {
    this.store.dispatch(new HeadSetBackAction(func));
  }
  public setIsAddMenu(flg) {
    this.store.dispatch(new HeadSetAddMenuAction(flg));
  }
  public setIsCloseMenu(flg) {
    this.store.dispatch(new HeadSetIsCloseMenuAction(flg));
  }
  public setIsSettingMenu(flg) {
    this.store.dispatch(new HeadSetIsSettingMenuAction(flg));
  }
}
