import { Action } from '@ngrx/store';

export class PhotobookInfo {
  processId?: number;
  processName?: string;
  displayOrder?: number;
  ischeck?: boolean;
  isOpen?: boolean;
  unknownWorkOutput?: boolean;
  // 作業リスト
  workList?: Array<{
    // 作業ID
    workId?: number;
    // 作業名
    workName?: string;
    // 表示順
    displayOrder?: number;
    ischeck?: boolean;
  }>;
}

export class PhotobookInfoStore {
  photobookInfos: PhotobookInfo[];
  chooseBuildingName: string;
  chooseConstructionName: string;
  chooseLargeProcessId: string;
  chooseLargeProcessName: string;
  outputOrder: string;
  pageCnt: string;
  pageId: string;
  unknownProcessOutput: boolean;
}

const initialStore: PhotobookInfoStore = {
  photobookInfos: [],
  chooseBuildingName: '',
  chooseConstructionName: '',
  chooseLargeProcessId: '',
  chooseLargeProcessName: '',
  outputOrder: '',
  pageCnt: '3',
  pageId: '',
  unknownProcessOutput: true,
};

const PhotobookInfoActionTypes = {
  SETPROCESSLIST: 'SETPROCESSLIST',
  CLEARBOOKINFOSET: 'CLEARBOOKINFOSET',
};

export class SetPhotobookInfoAction implements Action {
  readonly type = PhotobookInfoActionTypes.SETPROCESSLIST;
  constructor(public photobookInfoStore: PhotobookInfoStore) { }
}

export class ClearbookInfoAction implements Action {
  readonly type = PhotobookInfoActionTypes.CLEARBOOKINFOSET;
  constructor() { }
}

// reducer
export function PhotobookInfoReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case PhotobookInfoActionTypes.CLEARBOOKINFOSET:
      return {
        ...store,
        photobookInfos: [],
        chooseBuildingName: '',
        chooseConstructionName: '',
        chooseLargeProcessId: '',
        chooseLargeProcessName: '',
        outputOrder: '',
        pageCnt: '3',
        pageId: '',
        unknownProcessOutput: true,
      };
    case PhotobookInfoActionTypes.SETPROCESSLIST:
      return {
        ...store,
        photobookInfos: (action as SetPhotobookInfoAction).photobookInfoStore.photobookInfos,
        chooseBuildingName: (action as SetPhotobookInfoAction).photobookInfoStore.chooseBuildingName,
        chooseConstructionName: (action as SetPhotobookInfoAction).photobookInfoStore.chooseConstructionName,
        chooseLargeProcessName: (action as SetPhotobookInfoAction).photobookInfoStore.chooseLargeProcessName,
        outputOrder: (action as SetPhotobookInfoAction).photobookInfoStore.outputOrder,
        pageCnt: (action as SetPhotobookInfoAction).photobookInfoStore.pageCnt,
        pageId: (action as SetPhotobookInfoAction).photobookInfoStore.pageId,
        chooseLargeProcessId: (action as SetPhotobookInfoAction).photobookInfoStore.chooseLargeProcessId,
        unknownProcessOutput: (action as SetPhotobookInfoAction).photobookInfoStore.unknownProcessOutput,
      };
    default:
      return store;
  }
}

