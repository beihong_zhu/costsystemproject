import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  MenuState, HeadSetSlideMenuAction, HeadSetMoreMenuAction, HeadSetSlideMenuPhoneAction,
  SlideMenuDto, MoreMenuDto
} from './menu.store';

@Injectable()
export class MenuStateService {
  menuState: MenuState;
  constructor(private storeMenu: Store<{ menuState: MenuState }>) {

  }

  public menuStateSubscription(callback) {
    return this.storeMenu.pipe(select('menuState')).subscribe((state => {
      this.menuState = state;
      callback(state);
    }));
  }

  public setSlideMenu(array: SlideMenuDto[]) {
    this.storeMenu.dispatch(new HeadSetSlideMenuAction(array));
  }

  public setMoreMenu(array: MoreMenuDto[]) {
    this.storeMenu.dispatch(new HeadSetMoreMenuAction(array));
  }
  public setSlideMenuPhone(array) {
    this.storeMenu.dispatch(new HeadSetSlideMenuPhoneAction(array));
  }

}
