import { Action } from '@ngrx/store';

export class PhotobookViewStore {
  item1: string;
  item2: string;
  item3: string;
  item4: string;
  item5: string;
  item6: string;
  item7: string;
  item8: string;
  itemCnt1: number;
  itemCnt2: number;
  itemCnt3: number;
  itemCnt4: number;
  itemCnt5: number;
  itemCnt6: number;
  itemCnt7: number;
  itemCnt8: number;
  building: number;
  floor: number;
  room: number;
  place: number;
  largeProcess: number;
  process: number;
  work: number;
  situation: number;
  date: number;
  time: number;
  unitMarkModelType: number;
  machineNumber: number;
  witness: number;
  constructionCompany: number;
  freeColumn: number;
}

const initialStore: PhotobookViewStore = {
  item1: '',
  item2: '',
  item3: '',
  item4: '',
  item5: '',
  item6: '',
  item7: '',
  item8: '',
  itemCnt1: 0,
  itemCnt2: 0,
  itemCnt3: 0,
  itemCnt4: 0,
  itemCnt5: 0,
  itemCnt6: 0,
  itemCnt7: 0,
  itemCnt8: 0,
  building: 0,
  floor: 0,
  room: 0,
  place: 0,
  largeProcess: 0,
  process: 0,
  work: 0,
  situation: 0,
  date: 0,
  time: 0,
  unitMarkModelType: 0,
  machineNumber: 0,
  witness: 0,
  constructionCompany: 0,
  freeColumn: 0,
};

const PhotobookViewActionTypes = {
  VIEWINFO: 'VIEWINFO',
  CLEARBOOKVIEW: 'CLEARBOOKVIEW',
};

export class SetPhotobookViewAction implements Action {
  readonly type = PhotobookViewActionTypes.VIEWINFO;
  constructor(public photobookViewStore: PhotobookViewStore) { }
}

export class ClearPhotobookViewAction implements Action {
  readonly type = PhotobookViewActionTypes.CLEARBOOKVIEW;
  constructor() { }
}

// reducer
export function PhotobookViewReducer(store = initialStore, action: Action) {
  switch (action.type) {
    case PhotobookViewActionTypes.CLEARBOOKVIEW:
      return {
        ...store,
        item1: '',
        item2: '',
        item3: '',
        item4: '',
        item5: '',
        item6: '',
        item7: '',
        item8: '',
        itemCnt1: 0,
        itemCnt2: 0,
        itemCnt3: 0,
        itemCnt4: 0,
        itemCnt5: 0,
        itemCnt6: 0,
        itemCnt7: 0,
        itemCnt8: 0,
        building: 0,
        floor: 0,
        room: 0,
        place: 0,
        largeProcess: 0,
        process: 0,
        work: 0,
        situation: 0,
        date: 0,
        time: 0,
        unitMarkModelType: 0,
        machineNumber: 0,
        witness: 0,
        constructionCompany: 0,
        freeColumn: 0,
      };
    case PhotobookViewActionTypes.VIEWINFO:
      return {
        ...store,
        item1: (action as SetPhotobookViewAction).photobookViewStore.item1,
        item2: (action as SetPhotobookViewAction).photobookViewStore.item2,
        item3: (action as SetPhotobookViewAction).photobookViewStore.item3,
        item4: (action as SetPhotobookViewAction).photobookViewStore.item4,
        item5: (action as SetPhotobookViewAction).photobookViewStore.item5,
        item6: (action as SetPhotobookViewAction).photobookViewStore.item6,
        item7: (action as SetPhotobookViewAction).photobookViewStore.item7,
        item8: (action as SetPhotobookViewAction).photobookViewStore.item8,
        itemCnt1: (action as SetPhotobookViewAction).photobookViewStore.itemCnt1,
        itemCnt2: (action as SetPhotobookViewAction).photobookViewStore.itemCnt2,
        itemCnt3: (action as SetPhotobookViewAction).photobookViewStore.itemCnt3,
        itemCnt4: (action as SetPhotobookViewAction).photobookViewStore.itemCnt4,
        itemCnt5: (action as SetPhotobookViewAction).photobookViewStore.itemCnt5,
        itemCnt6: (action as SetPhotobookViewAction).photobookViewStore.itemCnt6,
        itemCnt7: (action as SetPhotobookViewAction).photobookViewStore.itemCnt7,
        itemCnt8: (action as SetPhotobookViewAction).photobookViewStore.itemCnt8,
        building: (action as SetPhotobookViewAction).photobookViewStore.building,
        floor: (action as SetPhotobookViewAction).photobookViewStore.floor,
        room: (action as SetPhotobookViewAction).photobookViewStore.room,
        place: (action as SetPhotobookViewAction).photobookViewStore.place,
        largeProcess: (action as SetPhotobookViewAction).photobookViewStore.largeProcess,
        process: (action as SetPhotobookViewAction).photobookViewStore.process,
        work: (action as SetPhotobookViewAction).photobookViewStore.work,
        situation: (action as SetPhotobookViewAction).photobookViewStore.situation,
        date: (action as SetPhotobookViewAction).photobookViewStore.date,
        time: (action as SetPhotobookViewAction).photobookViewStore.time,
        unitMarkModelType: (action as SetPhotobookViewAction).photobookViewStore.unitMarkModelType,
        machineNumber: (action as SetPhotobookViewAction).photobookViewStore.machineNumber,
        witness: (action as SetPhotobookViewAction).photobookViewStore.witness,
        constructionCompany: (action as SetPhotobookViewAction).photobookViewStore.constructionCompany,
        freeColumn: (action as SetPhotobookViewAction).photobookViewStore.freeColumn,
      };
    default:
      return store;
  }
}

