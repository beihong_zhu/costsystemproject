import { Action } from '@ngrx/store';

export class SlideMenuDto {
  pageId?: string;
  title?: string;
  path?: string;
  iconpath?: string;
}
export class MoreMenuDto {
  id?: string;
  title?: string;
  path?: string;
  switchFlg?: boolean;
  logOutFlg?: boolean;
  pageId?: string;
}

// initial state
export class MenuState {
  slideMenu: SlideMenuDto[];
  moreMenu: MoreMenuDto[];
  slideMenuPhone: string[];
}

const initialState: MenuState = {
  slideMenu: [],
  moreMenu: [],
  slideMenuPhone: []
};

const MenuActionTypes = {
  SETSLIDEMENU: 'SETSLIDEMENU',
  SETMOREMENU: 'SETMOREMENU',
  SETSLIDEMENUPHONE: 'SETSLIDEMENUPHONE'
};

export class HeadSetSlideMenuAction implements Action {
  readonly type = MenuActionTypes.SETSLIDEMENU;
  constructor(public slideMenu: SlideMenuDto[]) { }
}

export class HeadSetMoreMenuAction implements Action {
  readonly type = MenuActionTypes.SETMOREMENU;
  constructor(public moreMenu: MoreMenuDto[]) { }
}
export class HeadSetSlideMenuPhoneAction implements Action {
  readonly type = MenuActionTypes.SETSLIDEMENUPHONE;
  constructor(public slideMenuPhone: string[]) { }
}


// reducer
export function MenuReducer(state = initialState, action: Action) {
  switch (action.type) {
    case MenuActionTypes.SETSLIDEMENU:
      return {
        ...state,
        slideMenu: (action as HeadSetSlideMenuAction).slideMenu
      };
    case MenuActionTypes.SETMOREMENU:
      return {
        ...state,
        moreMenu: (action as HeadSetMoreMenuAction).moreMenu
      };
    case MenuActionTypes.SETSLIDEMENUPHONE:
      return {
        ...state,
        slideMenuPhone: (action as HeadSetSlideMenuPhoneAction).slideMenuPhone
      };
    default:
      return state;
  }
}

