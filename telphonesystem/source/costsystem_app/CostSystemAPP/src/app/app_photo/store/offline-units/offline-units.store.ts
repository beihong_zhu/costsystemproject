import { Action } from '@ngrx/store';

// initial state
export class OfflineUnitsStore {
  units?: Unit[];
}

const initialState: OfflineUnitsStore = {
  units: [],
};

const OfflineUnitsActionTypes = {
  SETOFFLINEUNITS: 'SET_OFFLINEUNITS',
};

export class SetOfflineUnitsAction implements Action {
  readonly type = OfflineUnitsActionTypes.SETOFFLINEUNITS;
  constructor(public units: Unit[]) { }
}
// reducer
export function OfflineUnitsReducer(state = initialState, action: Action) {
  switch (action.type) {
    case OfflineUnitsActionTypes.SETOFFLINEUNITS:
      return {
        ...state,
        units: (action as SetOfflineUnitsAction).units
      };
    default:
      return state;
  }
}

export class Unit {
  unitId?: number;
  modeltype?: string;
  serialNumber?: string;
  productionDate?: string;
  retryCount?: number;
}
