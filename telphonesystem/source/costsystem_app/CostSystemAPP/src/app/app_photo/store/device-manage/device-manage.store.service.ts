import { Injectable } from '@angular/core';
import { DeviceManageListStore, SetDeviceManageListAction } from './device-manage.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class DeviceManageListStoreService {

  public deviceManageListStore: DeviceManageListStore;

  constructor(
    private createdStore: Store<DeviceManageListStore>,
    private storeSubscribe: Store<{ deviceMangeListStore: DeviceManageListStore }>
  ) {

    this.storeSubscribe.pipe(select('deviceMangeListStore')).subscribe(store => {
      if (this.deviceManageListStore) {
        this.deviceManageListStore.deviceManageList = store.deviceManageList;
      } else {
        this.deviceManageListStore = store;
      }
      // console.log('this.storeSubscribe.deviceManageListStore', this.deviceManageListStore);
    });

  }

  public setDeviceManageList(array) {
    this.createdStore.dispatch(new SetDeviceManageListAction(array));
  }

}
