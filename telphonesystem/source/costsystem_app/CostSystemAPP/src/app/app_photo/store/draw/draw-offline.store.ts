import { Action } from '@ngrx/store';
import { IF0108OutDto, IF0106OutDto, API0605OutDto, API0601OutDto } from '@service/dto';


export class DrawOfflineStore {
  api0605OutDto: API0605OutDto;
}

const initialState: DrawOfflineStore = {
  api0605OutDto: null,
};

const ActionTypes = {
  UPDATEAPI0605: 'UPDATEAPI0605',
};

export class UPDATEAPI0605Action implements Action {
  readonly type = ActionTypes.UPDATEAPI0605;
  constructor(public api0605OutDto: API0605OutDto) { }
}

// reducer
export function DrawOfflineReducer(state = initialState, action: Action) {
  switch (action.type) {

    case ActionTypes.UPDATEAPI0605:
      return {
        ...state,
        api0605OutDto: (action as UPDATEAPI0605Action).api0605OutDto
      };

    default:
      return state;
  }
}

