import { Action } from '@ngrx/store';

export class AutoUploadPhotoStore {
    autoUploadPhoto: AutoUploadPhoto;

}

const initialStore: AutoUploadPhotoStore = {
    autoUploadPhoto: {}
};

const AutoUploadPhotoActionTypes = {
    SETUPPHOTO: 'SET_AUTOUPLOADPHOTOTYPE'

};



export class AutoUploadPhotoAction implements Action {
    readonly type = AutoUploadPhotoActionTypes.SETUPPHOTO;
    constructor(public autoUploadPhoto: AutoUploadPhoto) { }
}

// reducer
export function AutoUploadPhotoStoreReducer(store = initialStore, action: Action) {
    switch (action.type) {
        case AutoUploadPhotoActionTypes.SETUPPHOTO:
            return {
                ...store,
                autoUploadPhoto: (action as AutoUploadPhotoAction).autoUploadPhoto
            };

        default:
            return store;
    }
}

export declare interface AutoUploadPhoto {
    // 撮影端末番号
    photoTerminalNo?: string;
    // 撮影日
    photoDate?: string;

    retryCount?: number;

    localPhotoFlag?: number; // 1 local 0 server
}
