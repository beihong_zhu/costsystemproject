import { Action } from '@ngrx/store';
import { PinDto, DrawingInfoDto } from '../../pages/draw-manage/draw-location/dto';


export class DrawDisplayStore {
  drawingInfoDto?: DrawingInfoDto;
  pinInfo?: Array<PinDto>;
  selectedDrawingId?: string;
}

const initialState: DrawDisplayStore = {
  drawingInfoDto: {},
  pinInfo: [],
  selectedDrawingId: ''
};

const ActionTypes = {
  UPDATELIST: 'UPDATELIST',
  UPDATEDRAWINGINGODTO: 'UPDATEDRAWINGINGODTO',
  UPDATEPININFO: 'UPDATEAPI0601',
  UPDATESELECTEDDRAWINGID: 'UPDATESELECTEDDRAWINGID'
};

export class UPDATELISTAction implements Action {
  readonly type = ActionTypes.UPDATELIST;
  constructor(public drawingInfoDto: DrawingInfoDto, public pinInfo: PinDto[], public selectedDrawingId: string) { }
}

export class UPDATEDRAWINGINGODTOAction implements Action {
  readonly type = ActionTypes.UPDATEDRAWINGINGODTO;
  constructor(public drawingInfoDto: DrawingInfoDto) { }
}


export class UPDATEPININFOAction implements Action {
  readonly type = ActionTypes.UPDATEPININFO;
  constructor(public pinInfo: PinDto[]) { }
}


export class UPDATESELECTEDDRAWINGIDAction implements Action {
  readonly type = ActionTypes.UPDATESELECTEDDRAWINGID;
  constructor(public selectedDrawingId: string) { }
}

// reducer
export function DrawDisplayReducer(state = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.UPDATELIST:
      return {
        ...state,
        drawingInfoDto: (action as UPDATELISTAction).drawingInfoDto,
        pinInfo: (action as UPDATELISTAction).pinInfo,
        selectedDrawingId: (action as UPDATELISTAction).selectedDrawingId
      };
    case ActionTypes.UPDATEDRAWINGINGODTO:
      return {
        ...state,
        drawingInfoDto: (action as UPDATEDRAWINGINGODTOAction).drawingInfoDto
      };
    case ActionTypes.UPDATEPININFO:
      return {
        ...state,
        pinInfo: (action as UPDATEPININFOAction).pinInfo
      };
    case ActionTypes.UPDATESELECTEDDRAWINGID:
      return {
        ...state,
        selectedDrawingId: (action as UPDATESELECTEDDRAWINGIDAction).selectedDrawingId
      };
    default:
      return state;
  }
}

