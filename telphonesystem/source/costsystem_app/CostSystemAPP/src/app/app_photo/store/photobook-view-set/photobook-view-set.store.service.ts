import { Injectable } from '@angular/core';
import { PhotobookViewStore, SetPhotobookViewAction, ClearPhotobookViewAction } from './photobook-view-set.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class PhotobookViewSetStoreService {

  public photobookViewStore: PhotobookViewStore;

  constructor(
    private createdStore: Store<PhotobookViewStore>,
    private storeSubscribe: Store<{ photobookViewStore: PhotobookViewStore }>
  ) {

    this.storeSubscribe.pipe(select('photobookViewStore')).subscribe((store => {
      if (this.photobookViewStore) {
        this.photobookViewStore.item1 = store.item1;
        this.photobookViewStore.item2 = store.item2;
        this.photobookViewStore.item3 = store.item3;
        this.photobookViewStore.item4 = store.item4;
        this.photobookViewStore.item5 = store.item5;
        this.photobookViewStore.item6 = store.item6;
        this.photobookViewStore.item7 = store.item7;
        this.photobookViewStore.item8 = store.item8;
        this.photobookViewStore.itemCnt1 = store.itemCnt1;
        this.photobookViewStore.itemCnt2 = store.itemCnt2;
        this.photobookViewStore.itemCnt3 = store.itemCnt3;
        this.photobookViewStore.itemCnt4 = store.itemCnt4;
        this.photobookViewStore.itemCnt5 = store.itemCnt5;
        this.photobookViewStore.itemCnt6 = store.itemCnt6;
        this.photobookViewStore.itemCnt7 = store.itemCnt7;
        this.photobookViewStore.itemCnt8 = store.itemCnt8;
        this.photobookViewStore.building = store.building;
        this.photobookViewStore.floor = store.floor;
        this.photobookViewStore.room = store.room;
        this.photobookViewStore.place = store.place;
        this.photobookViewStore.largeProcess = store.largeProcess;
        this.photobookViewStore.process = store.process;
        this.photobookViewStore.work = store.work;
        this.photobookViewStore.situation = store.situation;
        this.photobookViewStore.date = store.date;
        this.photobookViewStore.time = store.time;
        this.photobookViewStore.unitMarkModelType = store.unitMarkModelType;
        this.photobookViewStore.machineNumber = store.machineNumber;
        this.photobookViewStore.witness = store.witness;
        this.photobookViewStore.constructionCompany = store.constructionCompany;
        this.photobookViewStore.freeColumn = store.freeColumn;
      } else {
        this.photobookViewStore = store;
      }
      // console.log('this.storeSubscribe.photobookViewStore', this.photobookViewStore);
    }));
  }

  public setPhotobookView(array) {
    this.createdStore.dispatch(new SetPhotobookViewAction(array));
  }

  public claerPhotobookView() {
    this.createdStore.dispatch(new ClearPhotobookViewAction());
  }

}
