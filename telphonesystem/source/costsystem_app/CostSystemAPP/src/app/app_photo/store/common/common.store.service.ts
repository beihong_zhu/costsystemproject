import { Injectable } from '@angular/core';
import {
  CommonStore, SetCommonAction, SetAutoUploadAction,
  SetFloorsListAction, SetProcessListAction, SetMachineListAction, Machine
} from './common.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class CommonStoreService {

  public commonStore: CommonStore;

  constructor(
    private store: Store<CommonStore>,
    private storeSubscribe: Store<{ commonStore: CommonStore }>,
  ) {

    this.storeSubscribe.pipe(select('commonStore')).subscribe(state => {
      if (this.commonStore) {
        this.commonStore.userId = state.userId;
        this.commonStore.userName = state.userName;
        this.commonStore.companyId = state.companyId;
        this.commonStore.authorId = state.authorId;
        this.commonStore.availableFunctionIds = state.availableFunctionIds;
        this.commonStore.buildingId = state.buildingId;
        this.commonStore.buildingName = state.buildingName;
        this.commonStore.projectId = state.projectId;
        this.commonStore.projectName = state.projectName;
        this.commonStore.bigProcessId = state.bigProcessId;
        this.commonStore.bigProcessName = state.bigProcessName;
        this.commonStore.autoUpload = state.autoUpload;
        this.commonStore.floorsList = state.floorsList;
        this.commonStore.processList = state.processList;
        this.commonStore.machineList = state.machineList;
      } else {
        this.commonStore = state;
      }
    });

  }

  public commonStoreSubscription(callback) {
    return this.storeSubscribe.pipe(select('commonStore')).subscribe((state => {
      callback(state);

    }));
  }

  public setCommon(commonStore: CommonStore) {
    this.store.dispatch(new SetCommonAction(commonStore));
  }

  public resetAutoUpload() {
    this.store.dispatch(new SetAutoUploadAction(false));
  }

  public setFloorsList(floorsList) {
    this.store.dispatch(new SetFloorsListAction(floorsList));
  }

  public setProcessList(processedList) {
    this.store.dispatch(new SetProcessListAction(processedList));
  }

  public setMachineList(machineList) {
    this.store.dispatch(new SetMachineListAction(machineList));
  }

  public updateMachineInfo(machine: Machine) {
    if (this.commonStore && this.commonStore.machineList) {
      for (const storeMachine of this.commonStore.machineList) {
        if (storeMachine.machineId === machine.machineId) {
          storeMachine.machineNumber = machine.machineNumber;
          storeMachine.machineModel = machine.machineModel;
          this.setMachineList(this.commonStore.machineList);
          break;
        }
      }
    }
  }

}
