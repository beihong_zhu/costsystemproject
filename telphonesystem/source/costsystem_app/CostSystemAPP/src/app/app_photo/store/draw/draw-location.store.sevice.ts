import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { DrawingInfoDto, PinDto } from '../../pages/draw-manage/draw-display/dto';
import { DrawDisplayStore, UPDATELISTAction, UPDATEDRAWINGINGODTOAction, UPDATEPININFOAction, UPDATESELECTEDDRAWINGIDAction } from './draw-location.store';

@Injectable()
export class DrawDisplayStoreService {

  public drawDisplayStore: DrawDisplayStore;

  constructor(
    private createdStore: Store<DrawDisplayStore>,
    private storeSubscribe: Store<{ drawDisplayStore: DrawDisplayStore }>
  ) {

    this.storeSubscribe.pipe(select('drawDisplayStore')).subscribe((store => {
      if (this.drawDisplayStore) {
        this.drawDisplayStore.drawingInfoDto = store.drawingInfoDto;
        this.drawDisplayStore.pinInfo = store.pinInfo;
        this.drawDisplayStore.selectedDrawingId = store.selectedDrawingId;
      } else {
        this.drawDisplayStore = store;
      }
    }));

  }

  public updateList(drawingInfoDto: DrawingInfoDto, pinInfo: PinDto[], selectedDrawingId: string) {
    this.createdStore.dispatch(new UPDATELISTAction(drawingInfoDto, pinInfo, selectedDrawingId));
  }

  public updatedrawinfo(drawingInfoDto: DrawingInfoDto) {
    this.createdStore.dispatch(new UPDATEDRAWINGINGODTOAction(drawingInfoDto));
  }

  public updatepininfo(pinInfo: PinDto[]) {
    this.createdStore.dispatch(new UPDATEPININFOAction(pinInfo));
  }

  public updateSelectedDrawing(selectedDrawingId: string) {
    this.createdStore.dispatch(new UPDATESELECTEDDRAWINGIDAction(selectedDrawingId));
  }

}
