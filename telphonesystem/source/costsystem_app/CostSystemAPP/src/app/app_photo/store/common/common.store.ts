import { Action } from '@ngrx/store';

export declare interface Floor {
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 表示順
  displayOrder?: number;
  // 部屋リスト
  rooms?: Array<Room>;
}

export declare interface Room {
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 場所リスト
  places?: Array<Place>;
}

export declare interface Place {
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
}

export declare interface Process {
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 表示順
  displayOrder?: number;
  // 作業リスト
  workList?: Array<Work>;
}

export declare interface Work {
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 表示順
  displayOrder?: number;
}

export declare interface Machine {
  // 機器ID
  machineId?: number;
  // 機種名
  machineModel?: string;
  // 機番
  machineNumber?: string;
  // 系統名
  lineName?: string;
  // 機種区分
  modeltypeDivision?: string;
  // マーク
  unitMark?: string;
  // フロアID
  floorId?: number;
  floorName?: string;
  // 部屋ID
  roomId?: number;
  roomName?: string;
  // 場所ID
  placeId?: number;
  placeName?: string;
}

export class CommonStore {
  userId: string;
  userName: string;
  companyId: number;
  authorId: string;
  availableFunctionIds: Array<string>;
  buildingId: number;
  buildingName: string;
  projectId: number;
  projectName: string;
  bigProcessId: string;
  bigProcessName: string;
  autoUpload: boolean;
  floorsList: Array<Floor>;
  processList: Array<Process>;
  machineList: Array<Machine>;
}

const initialStore: CommonStore = {
  userId: '',
  userName: '',
  companyId: 0,
  authorId: '',
  availableFunctionIds: [],
  buildingId: 0,
  buildingName: '',
  projectId: 0,
  projectName: '',
  bigProcessId: '',
  bigProcessName: '',
  autoUpload: false,
  floorsList: [],
  processList: [],
  machineList: [],
};

const CommonActionTypes = {
  SETCOMMON: 'SETCOMMON',
  SETAUTOUPLOAD: 'SETAUTOUPLOAD',
  SETFLOORSLIST: 'SETCOMMONFLOORSLIST',
  SETPROCESSLIST: 'SETCOMMONPROCESSLIST',
  SETMACHINELIST: 'SETCOMMONMACHINELIST',
};

export class SetCommonAction implements Action {
  readonly type = CommonActionTypes.SETCOMMON;
  constructor(public commonStore: CommonStore) { }
}

export class SetAutoUploadAction implements Action {
  readonly type = CommonActionTypes.SETAUTOUPLOAD;
  constructor(public autoUpload: boolean) { }
}

export class SetFloorsListAction implements Action {
  readonly type = CommonActionTypes.SETFLOORSLIST;
  constructor(public floorsList: Array<Floor>) { }
}

export class SetProcessListAction implements Action {
  readonly type = CommonActionTypes.SETPROCESSLIST;
  constructor(public processList: Array<Process>) { }
}

export class SetMachineListAction implements Action {
  readonly type = CommonActionTypes.SETMACHINELIST;
  constructor(public machineList: Array<Machine>) { }
}

// reducer
export function CommonReducer(store = initialStore, action: Action) {
  let nextStore: CommonStore;
  switch (action.type) {
    case CommonActionTypes.SETCOMMON:
      nextStore = {
        ...store,
        userId: (action as SetCommonAction).commonStore.userId,
        userName: (action as SetCommonAction).commonStore.userName,
        companyId: (action as SetCommonAction).commonStore.companyId,
        authorId: (action as SetCommonAction).commonStore.authorId,
        availableFunctionIds: (action as SetCommonAction).commonStore.availableFunctionIds,
        buildingId: (action as SetCommonAction).commonStore.buildingId,
        buildingName: (action as SetCommonAction).commonStore.buildingName,
        projectId: (action as SetCommonAction).commonStore.projectId,
        projectName: (action as SetCommonAction).commonStore.projectName,
        bigProcessId: (action as SetCommonAction).commonStore.bigProcessId,
        bigProcessName: (action as SetCommonAction).commonStore.bigProcessName,
        autoUpload: (action as SetCommonAction).commonStore.autoUpload
      };
      return nextStore;
    case CommonActionTypes.SETAUTOUPLOAD:
      nextStore = {
        ...store,
        autoUpload: (action as SetAutoUploadAction).autoUpload
      };
      return nextStore;
    case CommonActionTypes.SETFLOORSLIST:
      return {
        ...store,
        floorsList: (action as SetFloorsListAction).floorsList
      };
    case CommonActionTypes.SETPROCESSLIST:
      return {
        ...store,
        processList: (action as SetProcessListAction).processList
      };
    case CommonActionTypes.SETMACHINELIST:
      return {
        ...store,
        machineList: (action as SetMachineListAction).machineList
      };
    default:
      return store;
  }
}
