import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  Photomanagestore,
  PhotomanagestoreAction,
  PhotomanagestoreNeedPhotosListAction
} from './photo-manage.store';
import { Machine } from '@service/dto/if-01-06-out-dto';
import { PhotoListConf } from '@pages/photo-manage/photo-list/photo-list-conf';
import { PhotoConf } from '@conf/photo-conf';
import { PhotoList0501 } from '@service/dto';
import { LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';

@Injectable()
export class PhotomanagestoreService {
  public photomanageState: Photomanagestore;
  constructor(
    private store: Store<Photomanagestore>,
    private storeSubscribe: Store<{ photomanageState: Photomanagestore }>
  ) {
    this.storeSubscribe.pipe(select('photomanageState')).subscribe((state => {
      // console.log('photomanageState Service', state);
      this.photomanageState = state;
    }));
  }

  public setPhotos(photos: LocalPhoto[]) {
    this.store.dispatch(new PhotomanagestoreAction(photos));
  }

  public updatePhotoLocalPhotoFlag(photo) {
    if (this.photomanageState && this.photomanageState.photos) {
      for (const storePhoto of this.photomanageState.photos) {
        if (photo.photoTerminalNo === storePhoto.photoTerminalNo &&  photo.photoDate === storePhoto.photoDate) {
          storePhoto.localPhotoFlag = PhotoListConf.SERVER_POTO;
          storePhoto.uploadFlag = PhotoConf.AUTO_UPLOAD_OFF;
          this.setPhotos(this.photomanageState.photos);
          break;
        }
      }
    }
  }

  public updatePhotoRetryCount(photo) {
    if (this.photomanageState && this.photomanageState.photos) {
      for (const storePhoto of this.photomanageState.photos) {
        if (photo.photoTerminalNo === storePhoto.photoTerminalNo &&  photo.photoDate === storePhoto.photoDate) {
          storePhoto.retryCount = photo.retryCount;
          this.setPhotos(this.photomanageState.photos);
          break;
        }
      }
    }
  }

  public resetPhotoRetryCount() {
    if (this.photomanageState && this.photomanageState.photos) {
      for (const storePhoto of this.photomanageState.photos) {
        if (storePhoto.retryCount === PhotoConf.RETRY_MAX_COUNT) {
          storePhoto.retryCount = 0;
        }
      }
      this.setPhotos(this.photomanageState.photos);
    }
  }

  public updateMachineInfo(machine: Machine) {
    if (this.photomanageState && this.photomanageState.needPhotoList) {
      for (const storeMachine of this.photomanageState.needPhotoList) {
        if (storeMachine.machineId === machine.machineId) {
          // storeMachine.machineNumber = machine.machineNumber;
          storeMachine.modeltype = machine.machineModel;
        }
      }
      this.setNeedPhotoList(this.photomanageState.needPhotoList);
    }
    if (this.photomanageState && this.photomanageState.photos) {
      for (const photo of this.photomanageState.photos) {
        if (photo.machineId === machine.machineId) {
          photo.modeltype = machine.machineModel;
        }
      }
      this.setPhotos(this.photomanageState.photos);
    }
  }

  public setNeedPhotoList(needPhotoList: PhotoList0501[]) {
    this.store.dispatch(new PhotomanagestoreNeedPhotosListAction(needPhotoList));
  }

}
