import { Action } from '@ngrx/store';
import { S0301Assets } from '@constant/assets';
import { LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';
import { TbPhoto } from '@service/db/db-manage';

export declare interface Floor {
  // フロアID
  floorId?: number;
  // フロア名
  floorName?: string;
  // 表示順
  displayOrder?: number;
  // 部屋リスト
  rooms?: Array<Room>;
}

export declare interface Room {
  // 部屋ID
  roomId?: number;
  // 部屋名
  roomName?: string;
  // 場所リスト
  places?: Array<Place>;
}

export declare interface Place {
  // 場所ID
  placeId?: number;
  // 場所名
  placeName?: string;
}

export declare interface Witness {
  // 立会人ID
  witnessId?: number;
  // 立会人名
  witnessName?: string;
  // 施工会社ID
  constructionCompanyId?: number;
  // 施工会社名
  constructionCompanyName?: string;
  // 表示順
  displayOrder?: number;
}

export declare interface Machine {
  // 機器ID
  machineId?: number;
  // 機種名
  machineModel?: string;
  // 機番
  machineNumber?: string;

  // 系統名
  lineName?: string;

  // 機種区分
  modeltypeDivision?: string;

  // マーク
  unitMark?: string;

  // フロアID
  floorId?: number;

  floorName?: string;

  // 部屋ID
  roomId?: number;

  roomName?: string;

  // 場所ID
  placeId?: number;

  placeName?: string;

}

export declare interface Process {
  // 工程ID
  processId?: number;
  // 工程名
  processName?: string;
  // 表示順
  displayOrder?: number;
  // 作業リスト
  workList?: Array<Work>;
}

export declare interface Timing {
  // 状況ID
  timingId?: number;
  // 状況名
  timingName?: string;
  // 表示順
  displayOrder?: number;
  // 状況名略写
  timingAbbreviation: string;

}

export declare interface Work {
  // 作業ID
  workId?: number;
  // 作業名
  workName?: string;
  // 表示順
  displayOrder?: number;
}

export declare interface BlackBoardDisplay {

  // 案件表示フラグ
  isProjectDisplay?: string;
  // フロア表示フラグ
  isFloorDisplay?: string;
  // 部屋表示フラグ
  isRoomDisplay?: string;
  // 場所表示フラグ
  isPlaceDisplay?: string;
  // 系統表示フラグ
  isLineDisplay?: string;
  // 機器表示フラグ
  isUnitDisplay?: string;
  // 工程表示フラグ
  isProcessDisplay?: string;
  // 作業表示フラグ
  isWorkDisplay?: string;
  // 自由欄表示フラグ
  isFreeDisplay?: string;
  // 立会者表示フラグ
  isWitnessDisplay?: string;
  // 施工会社表示フラグ
  isConstructionCompanyDisplay?: string;
  // 日付表示フラグ
  isDateDisplay?: string;
  // 時刻表示フラグ
  isTimeDisplay?: string;
}

export declare interface PhotoSimpleGrid {
  machineId?: number;
  floorId?: number;
  roomId?: number;
  placeId?: number;
}

export enum InitInterfaceType {
  FromDrawing = 1,
  FromPhotoList = 2,
  FromPhotoListNoIMage = 3,
  FromDrawingNoIMage = 4,
  FromMenu = 3,
}

export class CameraStore {
  currentInitInterfaceType: InitInterfaceType;
  fromInitInterfaceType: InitInterfaceType;
  terminalNo: string;
  currentFloor: Floor; // フロア名リスト
  currentRoom: Room;  // 部屋名リスト
  currentPlace: Place;  // 場所名アイコン
  currentMachine: Machine; // 機種名リスト　「機種名＋（＋系統名＋）」と表示
  roomsList: Array<Room>;
  placesList: Array<Place>;
  displayMachinesList: Array<Machine>;
  currentProcess: Process; // 工程選択メニューリスト
  currentTiming: Timing; // 状況選択メニューリスト
  currentWork: Work; // 作業選択メニューリスト
  timimgsList: Array<Timing>;
  worksList: Array<Work>;
  witnessList: Array<Witness>;
  isBlackboardDisplay: boolean;
  blackboardDisplay: BlackBoardDisplay;
  free: string; // 備考欄(自由記述)
  photoDate: string; // 撮影日時
  currentWitness: Witness;
  flashMode: string;
  flashImage: string;
  zoom: number;
  photoPathLocal: string;
  currentPhoto: LocalPhoto; // current noimageのphoto情報
  photoSimpleGrid: PhotoSimpleGrid; // 簡易一覧画面情報
  thumbnail: TbPhoto; // サムネイル
}

const initialState: CameraStore = {
  currentFloor: null,
  currentRoom: null,
  currentPlace: null,
  currentMachine: null,
  roomsList: [],
  placesList: [],
  displayMachinesList: [],
  currentProcess: null,
  currentTiming: null,
  currentWork: null,
  timimgsList: [],
  worksList: [],
  witnessList: [],
  isBlackboardDisplay: true,
  blackboardDisplay: null,
  free: '',
  photoDate: '',
  currentWitness: null,
  flashMode: 'AUTO',
  flashImage: S0301Assets.CAMERAFLASHAUTO,
  zoom: 1,
  photoPathLocal: null,
  terminalNo: '',
  currentInitInterfaceType: null,
  fromInitInterfaceType: null,
  currentPhoto: null,
  photoSimpleGrid: null,
  thumbnail: null
};

export const CameraActionTypes = {
  SETCURRENTFLOOR: 'CAMERA_SETCURRENTFLOOR',
  SETCURRENTROOM: 'CAMERA_SETCURRENTROOM',
  SETCURRENTPLACE: 'CAMERA_SETCURRENTPLACE',
  SETCURRENTMACHINE: 'CAMERA_SETCURRENTMACHINE',
  SETROOMSLIST: 'CAMERA_SETROOMSLIST',
  SETPLACESLIST: 'CAMERA_SETPLACESLIST',
  SETDISPLAYMACHINESLIST: 'CAMERA_SETDISPLAYMACHINESLIST',
  SETCURRENTPROCESS: 'CAMERA_SETCURRENTPROCESS',
  SETCURRENTTIMING: 'CAMERA_SETCURRENTTIMING',
  SETCURRENTWORK: 'CAMERA_SETCURRENTWORK',
  SETTIMINGSLIST: 'CAMERA_SETTIMINGSLIST',
  SETWORKSLIST: 'CAMERA_SETWORKSLIST',
  SETISBLACKBOARDDISPLAY: 'CAMERA_SETISBLACKBOARDDISPLAY',
  SETBLACKBOARDDISPLAY: 'CAMERA_SETBLACKBOARDDISPLAY',
  SETFREE: 'CAMERA_SETFREE',
  SETTAKEPHOTOTIME: 'CAMERA_SETTAKEPHOTOTIME',
  SETFLASHMODE: 'CAMERA_SETFLASHMODE',
  SETFLASHIMAGE: 'CAMERA_SETFLASHIMAGE',
  SETZOOM: 'CAMERA_SETFLASHMODE',
  SETCURRENTWITNESS: 'CAMERA_SETCURRENTWITNESS',
  SETWITNESSLIST: 'CAMERA_SETWITNESSLIST',
  SETPHOTOPATHLOCAL: 'CAMERA_SETPHOTOPATHLOCAL',
  SETPHOTODATE: 'CAMERA_SETPHOTODATE',
  SETTERMINALNO: 'CAMERA_SETTERMINALNO',
  SETCURRENTINITINTERFACETYPE: 'CAMERA_SETCURRENTINITINTERFACETYPE',
  SETFROMINITINTERFACETYPE: 'CAMERA_SETFROMINITINTERFACETYPE',
  SETCURRENTPHOTO: 'CAMERA_SETCURRENTPHOTO',
  SETPHOTOSIMPLEGRID: 'CAMERA_SETPHOTOSIMPLEGRID',
  CLEARCAMERASTORE: 'CLEAR_CAMERASTORE',
  SETTHUMBNAIL: 'SET_THUMBNAIL'
};

export class CameraSetHumbnailAction implements Action {
  readonly type = CameraActionTypes.SETTHUMBNAIL;
  constructor(public thumbnail: TbPhoto) { }
}

export class CameraSetCurrentFloorAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTFLOOR;
  constructor(public currentFloor: Floor) { }
}

export class CameraSetCurrentRoomAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTROOM;
  constructor(public currentRoom: Room) { }
}

export class CameraSetCurrentPlaceAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTPLACE;
  constructor(public currentPlace: Place) { }
}

export class CameraSetCurrentMachineAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTMACHINE;
  constructor(public currentMachine: Machine) { }
}

export class CameraSetRoomsListAction implements Action {
  readonly type = CameraActionTypes.SETROOMSLIST;
  constructor(public roomsList: Array<Room>) { }
}

export class CameraSetPlacesListAction implements Action {
  readonly type = CameraActionTypes.SETPLACESLIST;
  constructor(public placesList: Array<Place>) { }
}

export class CameraSetDisplayMachinesListAction implements Action {
  readonly type = CameraActionTypes.SETDISPLAYMACHINESLIST;
  constructor(public displayMachinesList: Array<Machine>) { }
}

export class CameraSetCurrentProcessAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTPROCESS;
  constructor(public currentProcess: Process) { }
}

export class CameraSetCurrentTimingAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTTIMING;
  constructor(public currentTiming: Timing) { }
}

export class CameraSetCurrentWorkAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTWORK;
  constructor(public currentWork: Work) { }
}

export class CameraSetTimingsListAction implements Action {
  readonly type = CameraActionTypes.SETTIMINGSLIST;
  constructor(public timimgsList: Array<Timing>) { }
}

export class CameraSetWorksListAction implements Action {
  readonly type = CameraActionTypes.SETWORKSLIST;
  constructor(public worksList: Array<Work>) { }
}

export class CameraSetIsBlackboardDisplayAction implements Action {
  readonly type = CameraActionTypes.SETISBLACKBOARDDISPLAY;
  constructor(public isBlackboardDisplay: boolean) { }
}

export class CameraSetBlackboardDisplayAction implements Action {
  readonly type = CameraActionTypes.SETBLACKBOARDDISPLAY;
  constructor(public blackboardDisplay: BlackBoardDisplay) { }
}

export class CameraSetFreeAction implements Action {
  readonly type = CameraActionTypes.SETFREE;
  constructor(public free: string) { }
}

export class CameraSetFlashModeAction implements Action {
  readonly type = CameraActionTypes.SETFLASHMODE;
  constructor(public flashMode: string) { }
}

export class CameraSetFlashImageAction implements Action {
  readonly type = CameraActionTypes.SETFLASHIMAGE;
  constructor(public flashImage: string) { }
}

export class CameraSetZoomAction implements Action {
  readonly type = CameraActionTypes.SETZOOM;
  constructor(public zoom: number) { }
}

export class CameraSetCurrentWitenessAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTWITNESS;
  constructor(public currentWitness: Witness) { }
}

export class CameraSetWitenessListAction implements Action {
  readonly type = CameraActionTypes.SETWITNESSLIST;
  constructor(public witnessList: Witness) { }
}

export class CameraSetPhotoPathLocalAction implements Action {
  readonly type = CameraActionTypes.SETPHOTOPATHLOCAL;
  constructor(public photoPathLocal: string) { }
}

export class CameraSetPhotoDateAction implements Action {
  readonly type = CameraActionTypes.SETPHOTODATE;
  constructor(public photoDate: string) { }
}

export class CameraSetTerminalNoAction implements Action {
  readonly type = CameraActionTypes.SETTERMINALNO;
  constructor(public terminalNo: string) { }
}

export class CameraSetCurrentInitInterfaceTypeAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTINITINTERFACETYPE;
  constructor(public currentInitInterfaceType: InitInterfaceType) { }
}

export class CameraSetFromInitInterfaceTypeAction implements Action {
  readonly type = CameraActionTypes.SETFROMINITINTERFACETYPE;
  constructor(public fromInitInterfaceType: InitInterfaceType) { }
}

export class CameraSetCurrentPhotoAction implements Action {
  readonly type = CameraActionTypes.SETCURRENTPHOTO;
  constructor(public currentPhoto: LocalPhoto) { }
}

export class CameraSetPhotoSimpleGridAction implements Action {
  readonly type = CameraActionTypes.SETPHOTOSIMPLEGRID;
  constructor(public photoSimpleGrid: PhotoSimpleGrid) { }
}

export class ClearCameraStoreAction implements Action {
  readonly type = CameraActionTypes.CLEARCAMERASTORE;
  constructor() { }
}


// reducer
export function CameraReducer(state = initialState, action: Action) {
  switch (action.type) {
    case CameraActionTypes.SETCURRENTFLOOR:
      return {
        ...state,
        currentFloor: (action as CameraSetCurrentFloorAction).currentFloor
      };
    case CameraActionTypes.SETCURRENTROOM:
      return {
        ...state,
        currentRoom: (action as CameraSetCurrentRoomAction).currentRoom
      };
    case CameraActionTypes.SETCURRENTPLACE:
      return {
        ...state,
        currentPlace: (action as CameraSetCurrentPlaceAction).currentPlace
      };
    case CameraActionTypes.SETCURRENTMACHINE:
      return {
        ...state,
        currentMachine: (action as CameraSetCurrentMachineAction).currentMachine
      };
    case CameraActionTypes.SETROOMSLIST:
      return {
        ...state,
        roomsList: (action as CameraSetRoomsListAction).roomsList
      };
    case CameraActionTypes.SETPLACESLIST:
      return {
        ...state,
        placesList: (action as CameraSetPlacesListAction).placesList
      };
    case CameraActionTypes.SETDISPLAYMACHINESLIST:
      return {
        ...state,
        displayMachinesList: (action as CameraSetDisplayMachinesListAction).displayMachinesList
      };
    case CameraActionTypes.SETCURRENTPROCESS:
      return {
        ...state,
        currentProcess: (action as CameraSetCurrentProcessAction).currentProcess
      };
    case CameraActionTypes.SETCURRENTTIMING:
      return {
        ...state,
        currentTiming: (action as CameraSetCurrentTimingAction).currentTiming
      };
    case CameraActionTypes.SETCURRENTWORK:
      return {
        ...state,
        currentWork: (action as CameraSetCurrentWorkAction).currentWork
      };
    case CameraActionTypes.SETTIMINGSLIST:
      return {
        ...state,
        timimgsList: (action as CameraSetTimingsListAction).timimgsList
      };
    case CameraActionTypes.SETWORKSLIST:
      return {
        ...state,
        worksList: (action as CameraSetWorksListAction).worksList
      };
    case CameraActionTypes.SETISBLACKBOARDDISPLAY:
      return {
        ...state,
        isBlackboardDisplay: (action as CameraSetIsBlackboardDisplayAction).isBlackboardDisplay
      };
    case CameraActionTypes.SETBLACKBOARDDISPLAY:
      return {
        ...state,
        blackboardDisplay: (action as CameraSetBlackboardDisplayAction).blackboardDisplay
      };
    case CameraActionTypes.SETFREE:
      return {
        ...state,
        free: (action as CameraSetFreeAction).free
      };
    case CameraActionTypes.SETFLASHMODE:
      return {
        ...state,
        flashMode: (action as CameraSetFlashModeAction).flashMode
      };
    case CameraActionTypes.SETFLASHIMAGE:
      return {
        ...state,
        flashImage: (action as CameraSetFlashImageAction).flashImage
      };
    case CameraActionTypes.SETZOOM:
      return {
        ...state,
        zoom: (action as CameraSetZoomAction).zoom
      };
    case CameraActionTypes.SETCURRENTWITNESS:
      return {
        ...state,
        currentWitness: (action as CameraSetCurrentWitenessAction).currentWitness
      };
    case CameraActionTypes.SETWITNESSLIST:
      return {
        ...state,
        witnessList: (action as CameraSetWitenessListAction).witnessList
      };
    case CameraActionTypes.SETPHOTOPATHLOCAL:
      return {
        ...state,
        photoPathLocal: (action as CameraSetPhotoPathLocalAction).photoPathLocal
      };
    case CameraActionTypes.SETPHOTODATE:
      return {
        ...state,
        photoDate: (action as CameraSetPhotoDateAction).photoDate
      };
    case CameraActionTypes.SETTERMINALNO:
      return {
        ...state,
        terminalNo: (action as CameraSetTerminalNoAction).terminalNo
      };
    case CameraActionTypes.SETCURRENTINITINTERFACETYPE:
      return {
        ...state,
        currentInitInterfaceType: (action as CameraSetCurrentInitInterfaceTypeAction).currentInitInterfaceType
      };
    case CameraActionTypes.SETFROMINITINTERFACETYPE:
      return {
        ...state,
        fromInitInterfaceType: (action as CameraSetFromInitInterfaceTypeAction).fromInitInterfaceType
      };
    case CameraActionTypes.SETCURRENTPHOTO:
      return {
        ...state,
        currentPhoto: (action as CameraSetCurrentPhotoAction).currentPhoto
      };
    case CameraActionTypes.SETPHOTOSIMPLEGRID:
      return {
        ...state,
        photoSimpleGrid: (action as CameraSetPhotoSimpleGridAction).photoSimpleGrid
      };
    case CameraActionTypes.SETTHUMBNAIL:
      return {
        ...state,
        thumbnail: (action as CameraSetHumbnailAction).thumbnail
      };
    case CameraActionTypes.CLEARCAMERASTORE:
      return {
        ...state,
        currentFloor: null,
        currentRoom: null,
        currentPlace: null,
        currentMachine: null,
        floorsList: [],
        roomsList: [],
        placesList: [],
        machinesList: [],
        displayMachinesList: [],
        currentProcess: null,
        currentTiming: null,
        currentWork: null,
        timimgsList: [],
        worksList: [],
        witnessList: [],
        isBlackboardDisplay: true,
        blackboardDisplay: null,
        free: '',
        photoDate: '',
        currentWitness: null,
        flashMode: 'AUTO',
        flashImage: S0301Assets.CAMERAFLASHAUTO,
        zoom: 1,
        photoPathLocal: null,
        terminalNo: '',
        currentInitInterfaceType: null,
        fromInitInterfaceType: null,
        currentPhoto: null,
        photoSimpleGrid: null,
        thumbnail: null
      };
    default:
      return state;
  }
}

