import { Injectable } from '@angular/core';
import {
  ChatRoomStore,
  AddReadUsersAction,
  AddNewMessagesAction,
  SetChatMessagesAction,
  SetChatUsersAction,
  SetChatSelectUsersAction,
  SetChatAssociatedAction,
  SetChatConversationIdAction,
  SetChatConversationNameAction,
  SetChatCurrentConversationAction,
  SetInitSelectUsersAction
} from './chatroom.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class ChatRoomStoreService {

  public chatroomStore: ChatRoomStore;

  constructor(
    private store: Store<ChatRoomStore>,
    private storeSubscribe: Store<{ chatRoomStore: ChatRoomStore }>,
  ) {

    this.storeSubscribe.pipe(select('chatRoomStore')).subscribe((storeSub => {
      this.chatroomStore = storeSub;
    }));
  }

  public chatRoomStoreSubscription(callback) {
    return this.storeSubscribe.pipe(select('chatRoomStore')).subscribe((state => {
      callback(state);
    }));
  }


  public setChatMessages(messages) {
    this.store.dispatch(new SetChatMessagesAction(messages));
  }

  public addNewMessage(newMessage) {
    this.store.dispatch(new AddNewMessagesAction(newMessage));
  }

  public setChatUsers(chatUsers) {
    this.store.dispatch(new SetChatUsersAction(chatUsers));
  }

  public setChatSelectUsers(chatSelectUsers) {
    this.store.dispatch(new SetChatSelectUsersAction(chatSelectUsers));
  }



  public setChatAssociated(chatAssociated) {
    this.store.dispatch(new SetChatAssociatedAction(chatAssociated));
  }

  public setChatConversationId(conversationId) {
    this.store.dispatch(new SetChatConversationIdAction(conversationId));
  }

  public setChatConversationName(conversationName) {
    this.store.dispatch(new SetChatConversationNameAction(conversationName));
  }

  public setChatCurrentConversation(currentConversation) {
    this.store.dispatch(new SetChatCurrentConversationAction(currentConversation));
  }

  public addReadUsers(markAsReadMessages) {
    this.store.dispatch(new AddReadUsersAction(markAsReadMessages));
  }

  public setInitSelectUsers(initSelectUsers) {
    this.store.dispatch(new SetInitSelectUsersAction(initSelectUsers));
  }

}
