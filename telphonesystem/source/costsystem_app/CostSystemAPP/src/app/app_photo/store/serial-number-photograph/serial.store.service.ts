import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
    SerialstoreAction, Serialstore
} from './serial.store';

@Injectable()
export class SerialStoreService {

  public serialStore: Serialstore;
  constructor(
    private store: Store<Serialstore>,
    private serialStoreSubscribe: Store<{ serialStore: Serialstore }>
  ) {

    this.serialStoreSubscribe.pipe(select('serialStore')).subscribe((subscribestore => {
      this.serialStore = subscribestore;

    }));

  }

  public setSerial(currentSerial) {
    // console.log('setSerial', currentSerial);
    this.store.dispatch(new SerialstoreAction(currentSerial));
  }

}
