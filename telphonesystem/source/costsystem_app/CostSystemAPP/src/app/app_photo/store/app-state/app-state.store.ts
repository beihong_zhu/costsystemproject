import { Action } from '@ngrx/store';

export class AppState {
  isPC: boolean;
  isWinApp: boolean;
  isOffline: boolean;
}

const initialState: AppState = {
  isPC: true,
  isWinApp: false,
  isOffline: false
};

const AppActionTypes = {
  SETISPC: 'APP_SETISPC',
  SETISWINAPP: 'APP_SETISWINAPP',
  SETISOFFLINE: 'APP_SETISOFFLINE'
};

export class APPSetIsPCAction implements Action {
  readonly type = AppActionTypes.SETISPC;
  constructor(public isPC: boolean) { }
}

export class APPSetIsWinAppAction implements Action {
  readonly type = AppActionTypes.SETISWINAPP;
  constructor(public isWinApp: boolean) { }
}

export class APPSetIsOfflineAction implements Action {
  readonly type = AppActionTypes.SETISOFFLINE;
  constructor(public isOffline: boolean) { }
}

// reducer
export function AppReducer(state = initialState, action: Action) {
  switch (action.type) {
    case AppActionTypes.SETISPC:
      return {
        ...state,
        isPC: (action as APPSetIsPCAction).isPC
      };
    case AppActionTypes.SETISWINAPP:
      return {
        ...state,
        isWinApp: (action as APPSetIsWinAppAction).isWinApp
      };
    case AppActionTypes.SETISOFFLINE:
      return {
        ...state,
        isOffline: (action as APPSetIsOfflineAction).isOffline
      };

    default:
      return state;
  }
}
