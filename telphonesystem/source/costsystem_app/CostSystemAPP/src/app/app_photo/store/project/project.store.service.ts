import { Injectable } from '@angular/core';
import { ProjectListStore, SetProjectListAction } from './project.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class ProjectListStoreService {

  public projectlistStore: ProjectListStore;

  constructor(
    private createdStore: Store<ProjectListStore>,
    private storeSubscribe: Store<{ projectlistStore: ProjectListStore }>
  ) {

    this.storeSubscribe.pipe(select('projectlistStore')).subscribe((store => {
      if (this.projectlistStore) {
        this.projectlistStore.projectlist = store.projectlist;
      } else {
        this.projectlistStore = store;
      }
      // console.log('this.storeSubscribe.projectlistStore', this.projectlistStore);
    }));

  }

  public setProjectList(array) {
    this.createdStore.dispatch(new SetProjectListAction(array));
  }

}
