import { Injectable } from '@angular/core';
import { RegularInfoStore, SetRegularInfoAction } from './regularinfo.store';
import { Store, select } from '@ngrx/store';

@Injectable()
export class RegularInfoStoreService {

  public RegularInfoStore: RegularInfoStore;

  constructor(
    private createdStore: Store<RegularInfoStore>,
    private storeSubscribe: Store<{ RegularInfoStore: RegularInfoStore }>
  ) {
    this.storeSubscribe.pipe(select('RegularInfoStore')).subscribe((store => {
      if (this.RegularInfoStore) {
        this.RegularInfoStore.sysUserList = store.sysUserList;
      } else {
        this.RegularInfoStore = store;
      }
      // console.log('this.storeSubscribe.RegularInfoStore', this.RegularInfoStore);
    }));
  }

  public setRegularInfo(array) {
    this.createdStore.dispatch(new SetRegularInfoAction(array));
  }
}
