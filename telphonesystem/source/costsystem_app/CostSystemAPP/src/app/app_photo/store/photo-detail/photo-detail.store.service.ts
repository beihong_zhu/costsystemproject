import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { LocalPhoto } from '@pages/photo-manage/dto/photo-manage-dto';
import {
  PhotoDetailStore,
  PhotoDetailStoreDetailPhotoAction,
  PhotoDetailStoreDetailPhotoListAction
} from './photo-detail.store';

@Injectable()
export class PhotoDetailStoreService {
  public photoDetailStore: PhotoDetailStore;
  constructor(
    private store: Store<PhotoDetailStore>,
    private storeSubscribe: Store<{ photoDetailStore: PhotoDetailStore }>
  ) {

    this.storeSubscribe.pipe(select('photoDetailStore')).subscribe((storePhotoDetail => {
      // console.log('storePhotoDetail Service', storePhotoDetail);
      this.photoDetailStore = storePhotoDetail;
    }));
  }

  public photoDetailStoreSubscription(callback) {
    return this.storeSubscribe.pipe(select('photoDetailStore')).subscribe((state => {
      callback(state);
    }));
  }

  public setDetailPhoto(detailPhoto: LocalPhoto) {
    this.store.dispatch(new PhotoDetailStoreDetailPhotoAction(detailPhoto));
  }

  public setDetailPhotoList(detailPhotoList: LocalPhoto[]) {
    this.store.dispatch(new PhotoDetailStoreDetailPhotoListAction(detailPhotoList));
  }
}
