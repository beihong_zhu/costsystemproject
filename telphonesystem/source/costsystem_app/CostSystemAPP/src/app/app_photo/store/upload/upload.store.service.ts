import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UploadStore, SetPhototsUploadAction, SetPhototsManualUploadAction } from './upload.store';

@Injectable()
export class UploadStoreService {

  constructor(private store: Store<UploadStore>) { }

  public setPhotoUpload(photosUpload) {
    this.store.dispatch(new SetPhototsUploadAction(photosUpload));
  }

  public setPhotoManualUpload(photosManualUpload) {
    this.store.dispatch(new SetPhototsManualUploadAction(photosManualUpload));
  }

}
