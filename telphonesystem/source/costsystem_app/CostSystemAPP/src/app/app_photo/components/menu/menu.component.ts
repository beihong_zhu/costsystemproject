import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  MenuState,
  AppStateStoreService,
  CommonStoreService,
  PhotobookInfoSetStoreService,
  PhotobookViewSetStoreService,
  DrawDisplayStoreService,
  CommonStore, SlideMenuDto,
  MenuStateService
} from '@store';
import { RoutingService } from '@service/app-route.service';
import { Platform, Events } from '@ionic/angular';
import { CD0001Constants } from '@constant/code';
import { S0801Constants } from '@constant/constant.photo';
import { PopoverController } from '@ionic/angular';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss', './menu.component.mobile.scss', './menu.component.winapp.scss'],
})
export class MenuComponent implements OnInit {

  @Input() contentId: string;
  menuState: MenuState;
  isPc: boolean;
  bigProcessId: string;
  commonStore: CommonStore;
  disblayProcess = true;
  S0801TITLE = S0801Constants.CREATETITLE;
  platformtype: string;
  menuStateSubscription: Subscription;

  constructor(
    private menuStateService: MenuStateService,
    private routingService: RoutingService,
    public appStateService: AppStateStoreService,
    public commonStoreService: CommonStoreService,
    public events: Events,
    public popoverController: PopoverController,
    private photobookInfoSetStoreService: PhotobookInfoSetStoreService,
    private photobookViewSetStoreService: PhotobookViewSetStoreService,
    private platform: Platform,
    private drawDisplayStoreService: DrawDisplayStoreService,
  ) {
    this.commonStore = this.commonStoreService.commonStore;
    this.menuStateSubscription = this.menuStateService.menuStateSubscription(state => {
      this.menuState = state;
    });
    events.subscribe('refreshMenu', (isAll) => {
      this.commonStore = this.commonStoreService.commonStore;
      // スマホの場合は、大工程前画面で選択した大工程が、「施工試運転」若しくは「運用中」の場合のみ、工程画面に遷移可能とする
      this.isPc = this.appStateService.appState.isPC ? true : false;
      if (!this.isPc) {
        this.disblayProcess = (this.commonStore.bigProcessId === CD0001Constants.CD0001_03
          || this.commonStore.bigProcessId === CD0001Constants.CD0001_04);
      } else {
        this.disblayProcess = true;
      }
      this.platformtype = 'slideMenu-' + this.appStateService.getPlatform();
    });
  }

  ngOnInit() {
  }

  clickItem(menu: SlideMenuDto) {
    if (menu.path === '/photobook-info-set') {
      this.photobookInfoSetStoreService.clearBookInfoset();
      this.photobookViewSetStoreService.claerPhotobookView();
    }
    if (menu.path === '/project' || menu.path === '/bigProcess' || menu.path === '/draw-display') {
      if (this.drawDisplayStoreService.drawDisplayStore.selectedDrawingId !== '') {
        this.drawDisplayStoreService.updateSelectedDrawing('');
      }
    }
    this.routingService.goRouteLink(menu.path);
  }

  hasRole(menu: SlideMenuDto): boolean {
    if (this.commonStore.availableFunctionIds.includes(menu.pageId)) {
      return true;
    }
    return false;
  }

  async onPressBuilding(ev: Event) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPressBuilding', ev);
    const messageArr1 = [this.commonStoreService.commonStore.buildingName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }

  async onPressProject(ev: Event) {
    // PCとwinタブの以外の場合は、実行
    if (!this.platform.is('android') && !this.platform.is('ios')) {
      return;
    }

    console.log('onPress2', ev);
    const messageArr1 = [this.commonStoreService.commonStore.projectName];
    const popover = await this.popoverController.create({
      component: TextPopoverComponent,
      componentProps: { messageArr: messageArr1 },
      event: ev,
      translucent: true,
      showBackdrop: true,
    });
    return await popover.present();

  }


}
