import { Component, OnInit } from '@angular/core';
import { RoutingService } from '@service/app-route.service';
import { CameraStoreService, InitInterfaceType, AppState, AppStateStoreService } from '@store';

@Component({
  selector: 'app-photo-draw-footer',
  templateUrl: './photo-draw-footer.component.html',
  styleUrls: ['./photo-draw-footer.component.mobile.scss', './photo-draw-footer.component.winapp.scss'],
})
export class PhotoDrawFooterComponent implements OnInit {

  appState: AppState;
  platformtype: string;

  constructor(
    public routingService: RoutingService,
    private cameraStoreService: CameraStoreService,
    private appStateService: AppStateStoreService
  ) {
    this.platformtype = 'photoDrawFooter-' + this.appStateService.getPlatform();
  }

  ngOnInit() { }

  phototake() {

    this.cameraStoreService.setFromInitInterfaceType(InitInterfaceType.FromPhotoList);
    this.routingService.goRouteLink('/camera');

  }

  photomanage() {
    this.routingService.goRouteLink('/photo-list');
  }

  drawmanage() {
    this.routingService.goRouteLink('/draw-display');
  }

}
