import { Component, OnInit } from '@angular/core';
import { PopoverController, AlertController } from '@ionic/angular';
import { CommonStore, MenuState, MenuStateService, CommonStoreService, PhotomanagestoreService, OfflineUnitsService,
  MoreMenuDto } from '@store';
import { RoutingService } from '@service/app-route.service';
import { AuthService } from '../../../app_common/service/auth.service';
import { AlertWindowConstants } from '@constant/constant.photo';
import { MessageConstants } from '@constant/message-constants';
import { DbPhotoService } from '@service/db/db-photoservice';
import { DbUnitsService } from '@service/db/db-unitsservice';

@Component({
  selector: 'app-moremenu',
  templateUrl: './moremenu.component.html',
  styleUrls: ['./moremenu.component.scss'],
})
export class MoremenuComponent implements OnInit {

  menuState: MenuState;
  commonStore: CommonStore;
  autoUpload: boolean;

  constructor(
    private menuStateService: MenuStateService,
    public popoverController: PopoverController,
    private routingService: RoutingService,
    private commonStoreService: CommonStoreService,
    private authService: AuthService,
    public alertController: AlertController,
    private dbPhotoService: DbPhotoService,
    public photomanagestoreService: PhotomanagestoreService,
    private dbUnitsService: DbUnitsService,
    private offlineUnitsService: OfflineUnitsService

  ) {

    this.menuState = this.menuStateService.menuState;


    this.commonStore = this.commonStoreService.commonStore;
    this.autoUpload = this.commonStore.autoUpload;

  }

  ngOnInit() {
  }

  async clickMenu(menu: MoreMenuDto) {
    // 「自動同期」ではない
    if (!menu.switchFlg) {
      this.popoverController.dismiss();
      // 「ログアウト」の場合
      if (menu.logOutFlg) {
        // ログアウトのダイアログを作ります
        const alert = await this.alertController.create({
          header: 'ログアウト',
          message: MessageConstants.C00006,
          buttons: [{
            text: AlertWindowConstants.OKBUTTON,
            handler: async () => {
              await this.authService.signOut();
              this.routingService.goRouteLink(menu.path);
            }
          },
          {
            text: AlertWindowConstants.CANCELBUTTON,
            role: 'cancel',
            cssClass: 'primary',
            handler: (blah) => {
            }
          }
          ],
          backdropDismiss: false
        });
        await alert.present();
      }
      if (!menu.logOutFlg) {
        this.routingService.goRouteLink(menu.path);
      }
    }
  }

  autoUploadChange() {
    this.commonStore.autoUpload = this.autoUpload;
    this.commonStoreService.setCommon(this.commonStore);
    if (!this.autoUpload) {
      this.dbPhotoService.resetPhotoRetryCount();
      this.photomanagestoreService.resetPhotoRetryCount();
      this.dbUnitsService.resetUnitsRetryCount();
      this.offlineUnitsService.resetUnitsRetryCount();
    }
  }

  hasRole(menu: MoreMenuDto): boolean {
    if (this.commonStore.availableFunctionIds.includes(menu.pageId)) {
      return true;
    }
    return false;
  }

}
