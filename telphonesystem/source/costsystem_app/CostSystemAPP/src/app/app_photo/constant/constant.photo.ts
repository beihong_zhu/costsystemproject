import * as data from '../../../assets/json/title.json';
import * as adSort from '../../../assets/json/address.json';
import * as menudata from '../../../assets/json/slideMenu.json';
import * as moreMenudata from '../../../assets/json/moreMenu.json';
import * as privacyPolicydata from '../../../assets/json/privacy-policy.json';

const tData = data.default;
const adsort = adSort.default;
const menuData = menudata.default;
const moreMenuData = moreMenudata.default;
const privacyPolicyData = privacyPolicydata.default;

/**
 * 三点リーダーメニュー情報
 */
export abstract class MoreMenuPcConstants {
  static readonly S0101: [] = moreMenuData.S0101;
  static readonly S0102: [] = moreMenuData.S0102;
  static readonly S0501: [] = moreMenuData.S0501;
  static readonly S0502: [] = moreMenuData.S0502;
  static readonly S0601: [] = moreMenuData.S0601;
  static readonly S0602: [] = moreMenuData.S0602;
  static readonly S0901: [] = moreMenuData.S0901;
  static readonly S0902: [] = moreMenuData.S0902;
  static readonly S0903: [] = moreMenuData.S0903;
  static readonly S0801: [] = moreMenuData.S0801;
  static readonly S0802: [] = moreMenuData.S0802;
  static readonly S1201: [] = moreMenuData.S1201;
  static readonly S1202: [] = moreMenuData.S1202;
  static readonly S1203: [] = moreMenuData.S1203;
  static readonly S1204: [] = moreMenuData.S1204;
  static readonly S1103: [] = moreMenuData.S1103;
  static readonly S1104: [] = moreMenuData.S1104;
  static readonly S1401: [] = moreMenuData.S1401;
  static readonly S1302: [] = moreMenuData.S1302;
  static readonly S1303: [] = moreMenuData.S1303;
  static readonly S1501: [] = moreMenuData.S1501;
}

export abstract class MoreMenuPhoneConstants {
  static readonly S0101_PHONE: [] = moreMenuData.S0101_Phone;
  static readonly S0102_PHONE: [] = moreMenuData.S0102_Phone;
  static readonly S0201_PHONE: [] = moreMenuData.S0201_Phone;
  static readonly S0301_PHONE: [] = moreMenuData.S0301_Phone;
  static readonly S0401_PHONE: [] = moreMenuData.S0401_Phone;
  static readonly S0501_PHONE: [] = moreMenuData.S0501_Phone;
  static readonly S0502_PHONE: [] = moreMenuData.S0502_Phone;
  static readonly S0601_PHONE: [] = moreMenuData.S0601_Phone;
  static readonly S0901_PHONE: [] = moreMenuData.S0901_Phone;
  static readonly S0902_PHONE: [] = moreMenuData.S0902_Phone;
  static readonly S0903_PHONE: [] = moreMenuData.S0903_Phone;
  static readonly S0801_PHONE: [] = moreMenuData.S0801_Phone;
  static readonly S0802_PHONE: [] = moreMenuData.S0802_Phone;
  static readonly S1201_PHONE: [] = moreMenuData.S1201_Phone;
  static readonly S1202_PHONE: [] = moreMenuData.S1202_Phone;
  static readonly S1203_PHONE: [] = moreMenuData.S1203_Phone;
  static readonly S1204_PHONE: [] = moreMenuData.S1204_Phone;
  static readonly S1303_PHONE: [] = moreMenuData.S1303_Phone;
}

export abstract class SlideMenuConstants {
  static readonly S0301: [] = menuData.S0301;
  static readonly S0301_PHONE: [] = menuData.S0301_Phone;
  static readonly S0501: [] = menuData.S0501;
  static readonly S0501_PHONE: [] = menuData.S0501_Phone;
  static readonly S0601: [] = menuData.S0601;
  static readonly S0601_PHONE: [] = menuData.S0601_Phone;
  static readonly S0901: [] = menuData.S0901;
  static readonly S0901_PHONE: [] = menuData.S0901_Phone;
  static readonly S0902: [] = menuData.S0902;
  static readonly S0902_PHONE: [] = menuData.S0902_Phone;
  static readonly S0903: [] = menuData.S0903;
  static readonly S0903_PHONE: [] = menuData.S0903_Phone;
  static readonly S0801: [] = menuData.S0801;
  static readonly S0801_PHONE: [] = menuData.S0801_Phone;
  static readonly S0802: [] = menuData.S0802;
  static readonly S0802_PHONE: [] = menuData.S0802_Phone;
  static readonly S1103: [] = menuData.S1103;
  static readonly S1104: [] = menuData.S1104;
  static readonly S1201: [] = menuData.S1201;
  static readonly S1201_PHONE: [] = menuData.S1201_Phone;
}

/**
 * AlertWindow ダイアログ情報
 */
export abstract class AlertWindowConstants {
  static readonly ERRORTITLE: string = tData.AlertWindow.errorTitle;
  static readonly TIPSTITLE: string = tData.AlertWindow.tipsTitle;
  static readonly CONFIRMTITLE: string = tData.AlertWindow.confirmTitle;
  static readonly WARNINGTITLE: string = tData.AlertWindow.warningTitle;
  static readonly OKBUTTON: string = tData.AlertWindow.okButton;
  static readonly OKENGLISHBUTTON: string = tData.AlertWindow.okEnglishButton;
  static readonly CANCELBUTTON: string = tData.AlertWindow.cancelButton;
  static readonly ADMINEMAIL: string = tData.AlertWindow.adminEmail;
}

/**
 * Title ダイアログ情報
 */
 export abstract class HeaderConstants {
  static readonly TITLE: string = tData.Header.title;
}

/**
 * S-01-01 物件一覧画面情報
 */
export abstract class S0101Constants {
  /** 物件一覧画面タイトル */
  static readonly TITLE: string = tData.S0101.title;
  static readonly NUMBEROFPROPERTIES: string = tData.S0101.numberOfproperties;
  static readonly COUNTS: string = tData.S0101.counts;
  static readonly BUILDINGID: string = tData.S0101.buildingId;
  static readonly BUILDINGNAME: string = tData.S0101.buildingName;
  static readonly BUILDINGADDRESS: string = tData.S0101.buildingAddress;
}
/**
 * S-01-02 案件一覧画面情報
 */
export abstract class S0102Constants {
  static readonly TITLE: string = tData.S0102.title;
  static readonly PROJECTID: string = tData.S0102.projectId;
  static readonly PROJECTNAME: string = tData.S0102.projectName;
  static readonly NUMBEROFCONSTRUCTIONS: string = tData.S0102.numberOfConstructions;
  static readonly COUNTS: string = tData.S0102.counts;
}

/**
 * S-02-01 大工程選択
 */
export abstract class S0201Constants {
  static readonly TITLE: string = tData.S0201.title;
}

/**
 * S-03-01 写真撮影
 */
export abstract class S0301Constants {
  static readonly TITLE: string = tData.S0301.title;
  static readonly DEFAULTFLOOR: string = tData.S0301.defaultFloor;
  static readonly DEFAULTROOM: string = tData.S0301.defaultRoom;
  static readonly DEFAULTPLACE: string = tData.S0301.defaultPlace;
  static readonly DEFAULTMACHINE: string = tData.S0301.defaultMachine;
  static readonly DEFAULTLINE: string = tData.S0301.defaultLine;
  static readonly DEFAULTPROCESS: string = tData.S0301.defaultProcess;
  static readonly DEFAULTWORK: string = tData.S0301.defaultWork;
  static readonly DEFAULTTIMING: string = tData.S0301.defaultTiming;
  static readonly DEFAULTPROJECT: string = tData.S0301.defaultProject;
  static readonly DEFAULTFREE: string = tData.S0301.defaultFree;
  static readonly DEFAULTCOMPANYNAME: string = tData.S0301.defaultCompanyName;
  static readonly DEFAULTWITNESSNAME: string = tData.S0301.defaultWitnessName;
  static readonly DEFAULTPHOTOTIME: string = tData.S0301.defaultPhotoTime;
  static readonly DEFAULTIMAGE: string = tData.S0301.defaultImage;
  static readonly CAMERAIMAGE: string = tData.S0301.cameraImage;
  static readonly CAMERACHECKNG: string = tData.S0301.cameraCheckNG;
  static readonly CAMERACHECKOK: string = tData.S0301.cameraCheckOK;
  static readonly CAMERACHECKNONE: string = tData.S0301.cameraCheckNone;
  static readonly CAMERAFLASHAUTO: string = tData.S0301.cameraFlashAuto;
  static readonly CAMERAFLASHOFF: string = tData.S0301.cameraFlashOff;
  static readonly CAMERAFLASHON: string = tData.S0301.cameraFlashOn;
  static readonly BDPROJECTTITLE: string = tData.S0301.BdProjectTitle;
  static readonly BDPLACETITLE: string = tData.S0301.BdPlaceTitle;
  static readonly BDLINETITLE: string = tData.S0301.BdLineTitle;
  static readonly BDMACHINETITLE: string = tData.S0301.BdMachineTitle;
  static readonly BDPROCESSTITLE: string = tData.S0301.BdProcessTitle;
  static readonly BDWORKTITLE: string = tData.S0301.BdWorkTitle;
}

/**
 * S-04-01 機番撮影
 */
export abstract class S0401Constants {
  static readonly TITLE: string = tData.S0401.title;
  static readonly REGIST: string = tData.S0401.regist;
  static readonly RETAKEPHOTO: string = tData.S0401.retakePhoto;
  static readonly MANUALIMPORT: string = tData.S0401.manualImport;
  static readonly PRODUCTIONNUMBER: string = tData.S0401.productionNumber;
  static readonly PRODUCTIONYEARMONTH: string = tData.S0401.productionYearMonth;
  static readonly MACINENAME: string = tData.S0401.machineName;
  static readonly MACINENAMETITLE: string = tData.S0401.machineNameTitle;
  static readonly UNITMARK: string = tData.S0401.unitMark;
  static readonly BUILDINGNAME: string = tData.S0401.buildingName;
  static readonly CONSTRUCTIONNAME: string = tData.S0401.constructionName;
  static readonly FLOORTITLE: string = tData.S0401.floorTitle;
  static readonly ROOMNAME: string = tData.S0401.roomName;
  static readonly PLACETITLE: string = tData.S0401.placeTitle;
}

/**
 * S-05-01 写真一覧
 */
export abstract class S0501Constants {
  static readonly TITLE: string = tData.S0501.title;
  static readonly UPDATE: string = tData.S0501.update;
  static readonly PHOTOBOOKSELECT: string = tData.S0501.photoBookSelect;
  static readonly MULTISELECT: string = tData.S0501.multiSelect;
  static readonly DISPLAYMETHOD: string = tData.S0501.displayMethod;
  static readonly PHOTODELETE: string = tData.S0501.photodelete;
  static readonly DECISION: string = tData.S0501.decision;
  static readonly CANCLE: string = tData.S0501.cancel;
  static readonly IMAGE: string = tData.S0501.image;
  static readonly PROCESSWORK: string = tData.S0501.processwork;
  static readonly MACHINE: string = tData.S0501.machine;
  static readonly DATE: string = tData.S0501.date;
  static readonly TIMING: string = tData.S0501.timing;
  static readonly FILMINGSTATUS: string = tData.S0501.filmingStatus;
  static readonly PROCESSTIMING: string = tData.S0501.processTiming;
  static readonly TARGET: string = tData.S0501.target;
  static readonly ASCENDINGDES: string = tData.S0501.ascendingDescending;
  static readonly LIST: string = tData.S0501.list;
  static readonly GRID: string = tData.S0501.grid;
  static readonly FILTERSETTING: string = tData.S0501.filterSetting;
  static readonly SORTSETTING: string = tData.S0501.sortSetting;
  static readonly DISPLAYFORMATSETTING: string = tData.S0501.displayFormatSetting;
  static readonly SERIALNUMBERPHOTOGRAPH: string = tData.S0501.serialNumberPhotograph;
}

/**
 * S-05-02 写真詳細
 */
export abstract class S0502Constants {
  static readonly TITLE: string = tData.S0502.title;
  static readonly BDTITLE: string = tData.S0502.BdTitle;
  static readonly BDBUILDINGTITLE: string = tData.S0502.BdBuildingTitle;
  static readonly BDPROJECTTITLE: string = tData.S0502.BdProjectTitle;
  static readonly BDFLOORTITLE: string = tData.S0502.BdFloorTitle;
  static readonly BDROOMTITLE: string = tData.S0502.BdRoomTitle;
  static readonly BDPLACETITLE: string = tData.S0502.BdPlaceTitle;
  static readonly BDBIGPROCESSTITLE: string = tData.S0502.BdBigProcessTitle;
  static readonly BDMACHINETITLE: string = tData.S0502.BdMachineTitle;
  static readonly BDPROCESSTITLE: string = tData.S0502.BdProcessTitle;
  static readonly BDWORKTITLE: string = tData.S0502.BdWorkTitle;
  static readonly BDTIMINGTITLE: string = tData.S0502.BdTimingTitle;

}

/**
 * S-06-01 図面表示
 */
export abstract class S0601Constants {
  static readonly TITLE: string = tData.S0601.title;
  static readonly SETBUTTON: string = tData.S0601.setButton;
  static readonly DECISION: string = tData.S0601.decision;
  static readonly CANCEL: string = tData.S0601.cancel;
  static readonly PHOTOLIST: string = tData.S0601.photolist;
  static readonly PHOTOGRAPH: string = tData.S0601.photograph;
  static readonly POPUPTITLE: string = tData.S0601.popupTitle;
}

/**
 * S-06-02 図面位置
 */
export abstract class S0602Constants {
  static readonly TITLE: string = tData.S0602.title;
  static readonly DRAWINGLIST: string = tData.S0602.drawingList;
  static readonly PHOTOGRAPHPLACELIST: string = tData.S0602.photographPlacelist;
  static readonly FLOORSELECTION: string = tData.S0602.floorSelection;
  static readonly ROOMSELECTION: string = tData.S0602.roomSelection;
  static readonly PLACESELECTION: string = tData.S0602.placeSelection;
  static readonly PACEMENT1: string = tData.S0602.pacement1;
  static readonly PACEMENT0: string = tData.S0602.pacement0;
  static readonly PINMOVE: string = tData.S0602.pinMove;
  static readonly PINDELETE: string = tData.S0602.pinDelete;
  static readonly PINDELETELINE: string = tData.S0602.pinDeleteline;
  static readonly MODELTYPE: string = tData.S0602.modelType;
}

/**
 * S-08-01 工程編集画面情報
 */
export abstract class S0801Constants {
  static readonly TITLE: string = tData.S0801.title;
  static readonly CREATETITLE: string = tData.S0801.createTitle;
  static readonly HEADER: string = tData.S0801.header;
  static readonly SELECTCNTLABEL: string = tData.S0801.selectCntLabel;
  static readonly PROCESSCHANGETITLE: string = tData.S0801.processChangeTitle;
  static readonly WORKCHANGETITLE: string = tData.S0801.workChangeTitle;
  static readonly DISPLAYORDER: string = tData.S0801.displayOrder;
  static readonly CHANGENAME: string = tData.S0801.changeName;
  static readonly MESSAGE: string = tData.S0801.message;
  static readonly CHANGE: string = tData.S0801.change;
  static readonly CANCEL: string = tData.S0801.cancel;
  static readonly REGISTER: string = tData.S0801.register;
  static readonly ADDPROCESSTITLE: string = tData.S0801.addProcessTitle;
  static readonly ADDWORKTITLE: string = tData.S0801.addWorkTitle;
  static readonly ADDNAME: string = tData.S0801.addName;
  static readonly EDIT: string = tData.S0801.edit;
  static readonly ADDPROCESSLABEL: string = tData.S0801.addProcessLabel;
  static readonly ADDWORKLABEL: string = tData.S0801.addWorkLabel;
  static readonly PROCESSMSG: string = tData.S0801.processMsg;
  static readonly WORKMSG: string = tData.S0801.workMsg;
}

/**
 * S-09-01 写真帳出力情報設定画面
 */
export abstract class S0901Constants {
  static readonly TITLE: string = tData.S0901.title;
  static readonly SCREENGUIDE: string = tData.S0901.screenGuide;
  static readonly BASICINFORMATION: string = tData.S0901.basicInformation;
  static readonly BUILDINGNAME: string = tData.S0901.buildingName;
  static readonly CONSTRUCTIONNAME: string = tData.S0901.constructionName;
  static readonly LARGEPROCESSNAME: string = tData.S0901.largeProcessName;
  static readonly OUTPUTPROCESS: string = tData.S0901.outputProcess;
  static readonly OUTPUTPROCESSGUIDE1: string = tData.S0901.outputProcessGuide1;
  static readonly OUTPUTPROCESSGUIDE2: string = tData.S0901.outputProcessGuide2;
  static readonly OVERALLOUTPUTORDER: string = tData.S0901.overallOutputOrder;
  static readonly OVERALLOUTPUTORDERGUIDE1: string = tData.S0901.overallOutputOrderGuide1;
  static readonly OVERALLOUTPUTORDERGUIDE2: string = tData.S0901.overallOutputOrderGuide2;
  static readonly PROCESSPRIORITY: string = tData.S0901.processPriority;
  static readonly FLOORPRIORITY: string = tData.S0901.floorPriority;
  static readonly OUTPUTPERPAGE: string = tData.S0901.outputPerPage;
  static readonly ISMOBILE: string = tData.S0901.isMobile;
  static readonly MOVEON: string = tData.S0901.moveOn;
  static readonly MOVEONARROW: string = tData.S0901.moveOnArrow;
}

/**
 * S-09-02 写真帳レイアウト
 */
export abstract class S0902Constants {
  static readonly TITLE: string = tData.S0902.title;
  static readonly BUILDINGPROJECTNAME1: string = tData.S0902.buildingProjectName1;
  static readonly BUILDINGPROJECTNAME2: string = tData.S0902.buildingProjectName2;
  static readonly PHOTOTAKEN: string = tData.S0902.photoTaken;
  static readonly ARRANGEMENT1: string = tData.S0902.arrangement1;
  static readonly ARRANGEMENT2: string = tData.S0902.arrangement2;
  static readonly ARRANGEMENT3: string = tData.S0902.arrangement3;
  static readonly ARRANGEMENT4: string = tData.S0902.arrangement4;
  static readonly ARRANGEMENT5: string = tData.S0902.arrangement5;
  static readonly ARRANGEMENT6: string = tData.S0902.arrangement6;
  static readonly ARRANGEMENT7: string = tData.S0902.arrangement7;
  static readonly ARRANGEMENT8: string = tData.S0902.arrangement8;
  static readonly PHOTOBOOKORDERGUIDE: string = tData.S0902.photoBookOrderGuide;
  static readonly BUILDINGINFORMATION: string = tData.S0902.buildingInformation;
  static readonly BUILDING: string = tData.S0902.building;
  static readonly FLOOR: string = tData.S0902.floor;
  static readonly ROOM: string = tData.S0902.room;
  static readonly PLACE: string = tData.S0902.place;
  static readonly PROCESSINFORMATION: string = tData.S0902.processInformation;
  static readonly LARGEPROCESS: string = tData.S0902.largeProcess;
  static readonly PROCESS: string = tData.S0902.process;
  static readonly WORK: string = tData.S0902.work;
  static readonly SITUATION: string = tData.S0902.situation;
  static readonly DATETIMEINFORMATION: string = tData.S0902.dateTimeInformation;
  static readonly DATE: string = tData.S0902.date;
  static readonly TIME: string = tData.S0902.time;
  static readonly EQUIPMENTINFORMATION: string = tData.S0902.equipmentInformation;
  static readonly unitMarkModelType: string = tData.S0902.unitMarkModelType;
  static readonly MACHINENUMBER: string = tData.S0902.machineNumber;
  static readonly OTHER: string = tData.S0902.other;
  static readonly WITNESS: string = tData.S0902.witness;
  static readonly CONSTRUCTIONCOMPANY: string = tData.S0902.constructionCompany;
  static readonly FREECOLUMN: string = tData.S0902.freeColumn;
  static readonly RETURN: string = tData.S0902.return;
  static readonly MOVEON: string = tData.S0902.moveOn;
  static readonly RETURNARROW: string = tData.S0902.returnArrow;
  static readonly MOVEONARROW: string = tData.S0902.moveOnArrow;
  static readonly ITEMS: string = tData.S0902.items;
  static readonly BUILDINGITEM: string = tData.S0902.buildingItem;
  static readonly FLOORITEM: string = tData.S0902.floorItem;
  static readonly ROOMITEM: string = tData.S0902.roomItem;
  static readonly PLACEITEM: string = tData.S0902.placeItem;
  static readonly LARGEPROCESSITEM: string = tData.S0902.largeProcessItem;
  static readonly PROCESSITEM: string = tData.S0902.processItem;
  static readonly WORKITEM: string = tData.S0902.workItem;
  static readonly SITUATIONITEM: string = tData.S0902.situationItem;
  static readonly DATEITEM: string = tData.S0902.dateItem;
  static readonly TIMEITEM: string = tData.S0902.timeItem;
  static readonly unitMarkModelTypeITEM: string = tData.S0902.unitMarkModelTypeItem;
  static readonly MACHINENUMBERITEM: string = tData.S0902.machineNumberItem;
  static readonly OTHERITEM: string = tData.S0902.otherItem;
  static readonly WITNESSITEM: string = tData.S0902.witnessItem;
  static readonly CONSTRUCTIONCOMPANYITEM: string = tData.S0902.constructionCompanyItem;
  static readonly FREECOLUMNITEM: string = tData.S0902.freeColumnItem;
  static readonly LAYOUTITEMS: string = tData.S0902.layoutItems;
}

/**
 * S-09-03 写真帳プレビュー
 */
export abstract class S0903Constants {
  static readonly TITLE: string = tData.S0903.title;
  static readonly DOWNLOADGUIDE: string = tData.S0903.downloadGuide;
  static readonly DOWNLOAD: string = tData.S0903.download;
  static readonly RETURN: string = tData.S0903.return;
  static readonly RETURNARROW: string = tData.S0903.returnArrow;
}

/**
 * S-11-01 ログイン
 */
export abstract class S1101Constants {
  static readonly LOGINMESSAGE: string = tData.S1101.loginMessage;
  static readonly PASSWORDCHANGEMESSAGE: string = tData.S1101.passwordChangeMessage;
  static readonly PASSWORDCHANGE: string = tData.S1101.passwordChange;
  static readonly USEVERSIONSELECTMESSAGE: string = tData.S1101.useVersionSelectMessage;
  static readonly PCEDITION: string = tData.S1101.pcEdition;
  static readonly SMARTPHONEEDITION: string = tData.S1101.smartphoneEdition;
  static readonly SIGNIN: string = tData.S1101.signIn;
  static readonly PASSCODEINPUT: string = tData.S1101.passcodeInput;
  static readonly CONFIRMSIGNIN: string = tData.S1101.confirmSignIn;
  static readonly ADMINEMAIL: string = tData.S1101.adminEmail;
}

/**
 * S-11-02 パスワード変更
 */
export abstract class S1102Constants {
  static readonly PASSWORDCHANGEMESSEGE: string = tData.S1102.passwordChangeMessege;
  static readonly PASSWORDCHANGEDESCRIPTION: string = tData.S1102.passwordChangeDescription;
  static readonly USERIDMESSEGE: string = tData.S1102.userIdMessege;
  static readonly CURRENTPASSWORD: string = tData.S1102.currentPassword;
  static readonly NEWPASSWORD: string = tData.S1102.newPassword;
  static readonly CONFIRMATIONPASSWORD: string = tData.S1102.confirmationPassword;
  static readonly PASSWORDDESCRIPTION: string = tData.S1102.passwordDescription;
  static readonly PASSWORDCHANGE: string = tData.S1102.passwordChange;
  static readonly PASSCODEINPUT: string = tData.S1102.passcodeInput;
  static readonly CONFIRMSIGNIN: string = tData.S1102.confirmSignIn;
  static readonly ADMINEMAIL: string = tData.S1102.adminEmail;
  static readonly CANCEL: string = tData.S1102.cancel;
}

/**
 * S-11-03 二段階認証デバイス管理
 */
export abstract class S1103Constants {
  static readonly TITLE: string = tData.S1103.title;
  static readonly SEARCH: string = tData.S1103.search;
  static readonly UPDATE: string = tData.S1103.update;
  static readonly UPDATEMESSAGE: string = tData.S1103.updateMessage;
  static readonly EFFECTIVENESS: string = tData.S1103.effectiveness;
  static readonly INVALID: string = tData.S1103.invalid;
  static readonly USERID: string = tData.S1103.userId;
  static readonly DEVICECODE: string = tData.S1103.deviceCode;
  static readonly DATEMOMENT: string = tData.S1103.dateMoment;
  static readonly SEARCHTITLE: string = tData.S1103.searchTitle;
  static readonly DIALOGLABEL: string = tData.S1103.dialogLabel;
}

/**
 * S-11-04 アカウントロック管理
 */
export abstract class S1104Constants {
  static readonly TITLE: string = tData.S1104.title;
  static readonly SEARCH: string = tData.S1104.search;
  static readonly UNLOCK: string = tData.S1104.unLock;
  static readonly USERID: string = tData.S1104.userId;
  static readonly LOCKTIME: string = tData.S1104.lockTime;
  static readonly DIALOGLABEL: string = tData.S1104.dialogLabel;
  static readonly SEARCHTITLE: string = tData.S1104.searchTitle;
}

/**
 * S-12-01 チャットルーム一覧
 */
export abstract class S1201Constants {
  static readonly TITLE: string = tData.S1201.title;
}

/**
 * S-12-02 チャットルーム作成
 */
export abstract class S1202Constants {
  static readonly TITLE: string = tData.S1202.title;
}

/**
 * S-12-02 ルーム作成メンバー選択画面
 */
export abstract class S1205Constants {
  static readonly TITLE: string = tData.S1205.title;
}

/**
 * S-12-02 ルーム名入力・作成完了画面
 */
export abstract class S1206Constants {
  static readonly TITLE: string = tData.S1206.title;
}

/**
 * S-12-03 チャット画面
 */
export abstract class S1203Constants {
  static readonly TITLE: string = tData.S1203.title;
}

/**
 * S-12-04 チャットルーム編集
 */
export abstract class S1204Constants {
  static readonly TITLE: string = tData.S1204.title;
}

/**
 * S-12-04 ルーム編集メンバー選択画面
 */
export abstract class S1207Constants {
  static readonly TITLE: string = tData.S1207.title;
}

/**
 * S-12-04 ルーム編集完了画面
 */
export abstract class S1208Constants {
  static readonly TITLE: string = tData.S1208.title;
}

/**
 * S-14-01 事前設定情報登録画面
 */
export abstract class S1401Constants {
  static readonly TITLE: string = tData.S1401.title;
  static readonly TARGET_BUILDING: string = tData.S1401.targetBuilding;
  static readonly TARGET_PROJECT: string = tData.S1401.targetProject;
  static readonly SETTING_TARGET: string = tData.S1401.settingTarget;
  static readonly INPUT_CSV: string = tData.S1401.inputCsv;
  static readonly READING_CSV: string = tData.S1401.readingCsv;
  static readonly UPLOAD_CONTENTS: string = tData.S1401.uploadedContents;
  static readonly UPLOAD_LABEL: string = tData.S1401.uploadLabel;
  static readonly INPUT_PDF: string = tData.S1401.inputPdf;

  static readonly USERCSV_USERKBN: string = tData.S1401.userCsvuesrKbn;
  static readonly USERCSV_USERID: string = tData.S1401.userCsvuesrId;
  static readonly USERCSV_BEARERSNAME: string = tData.S1401.userCsvbearersName;
  static readonly USERCSV_MAILADD: string = tData.S1401.userCsvmailAdd;
  static readonly USERCSV_PASSWORD: string = tData.S1401.userCsvpassword;
  static readonly USERCSV_KAISHANAME: string = tData.S1401.userCsvkaishaName;
  static readonly USERCSV_AUTHOR: string = tData.S1401.userCsvauthor;
  static readonly USERCSV_MESSAGE: string = tData.S1401.userCsvmessage;

  static readonly BUKENCSV_BUKENKBN: string = tData.S1401.bukenCsvbukenKbn;
  static readonly BUKENCSV_BUKENID: string = tData.S1401.bukenCsvbukenId;
  static readonly BUKENCSV_BUKENNAME: string = tData.S1401.bukenCsvbukenName;
  static readonly BUKENCSV_BUKENPOST: string = tData.S1401.bukenCsvbukenPost;
  static readonly BUKENCSV_ADD1: string = tData.S1401.bukenCsvadd1;
  static readonly BUKENCSV_ADD2: string = tData.S1401.bukenCsvadd2;
  static readonly BUKENCSV_ADD3: string = tData.S1401.bukenCsvadd3;
  static readonly BUKENCSV_TELEPHONE: string = tData.S1401.bukenCsvtelephone;
  static readonly BUKENCSV_MESSAGE: string = tData.S1401.bukenCsvmessage;

  static readonly BUKENHEYABASYOCSV_BUKENID: string = tData.S1401.bukenHeyaBasyoCsvbukenId;
  static readonly BUKENHEYABASYOCSV_BUKENNAME: string = tData.S1401.bukenHeyaBasyoCsvbukenName;
  static readonly BUKENHEYABASYOCSV_FROAKBN: string = tData.S1401.bukenHeyaBasyoCsvfroaKbn;
  static readonly BUKENHEYABASYOCSV_FROAID: string = tData.S1401.bukenHeyaBasyoCsvfroaId;
  static readonly BUKENHEYABASYOCSV_FROANAME: string = tData.S1401.bukenHeyaBasyoCsvfroaName;
  static readonly BUKENHEYABASYOCSV_HEYAKBN: string = tData.S1401.bukenHeyaBasyoCsvheyaKbn;
  static readonly BUKENHEYABASYOCSV_HEYAID: string = tData.S1401.bukenHeyaBasyoCsvheyaId;
  static readonly BUKENHEYABASYOCSV_HEYANAME: string = tData.S1401.bukenHeyaBasyoCsvheyaName;
  static readonly BUKENHEYABASYOCSV_BASYOKBN: string = tData.S1401.bukenHeyaBasyoCsvbasyoKbn;
  static readonly BUKENHEYABASYOCSV_BASYOID: string = tData.S1401.bukenHeyaBasyoCsvbasyoId;
  static readonly BUKENHEYABASYOCSV_BASYONAME: string = tData.S1401.bukenHeyaBasyoCsvbasyoName;
  static readonly BUKENHEYABASYOCSV_MESSAGE: string = tData.S1401.bukenHeyaBasyoCsvmessage;

  static readonly ANKENCSV_ANKENID: string = tData.S1401.ankenCsvankenId;
  static readonly ANKENCSV_ANKENNAME: string = tData.S1401.ankenCsvankenName;
  static readonly ANKENCSV_MESSAGE: string = tData.S1401.ankenCsvmessage;

  static readonly USERKANLICSV_USERKBN: string = tData.S1401.userKanliCsvuserKbn;
  static readonly USERKANLICSV_USERID: string = tData.S1401.userKanliCsvuserId;
  static readonly USERKANLICSV_MESSAGE: string = tData.S1401.userKanliCsvmessage;

  static readonly BUKENZUMENINFOCSV_ZUMENKBN: string = tData.S1401.bukenZumenInfoCsvzumenKbn;
  static readonly BUKENZUMENINFOCSV_FROANAME: string = tData.S1401.bukenZumenInfoCsvfroaName;
  static readonly BUKENZUMENINFOCSV_ZUMENPATH: string = tData.S1401.bukenZumenInfoCsvzumenPath;
  static readonly BUKENZUMENINFOCSV_ZUMENNAME: string = tData.S1401.bukenZumenInfoCsvzumenName;
  static readonly BUKENZUMENINFOCSV_MESSAGE: string = tData.S1401.bukenZumenInfoCsvmessage;

  static readonly MACHINEINFOCSV_MACHINEKBN: string = tData.S1401.machineInfoCsvmachineKbn;
  static readonly MACHINEINFOCSV_SYSNAME: string = tData.S1401.machineInfoCsvsysName;
  static readonly MACHINEINFOCSV_SIGNAL: string = tData.S1401.machineInfoCsvsignal;
  static readonly MACHINEINFOCSV_OUTMACHINENAME: string = tData.S1401.machineInfoCsvoutMachineName;
  static readonly MACHINEINFOCSV_INMACHINENAME: string = tData.S1401.machineInfoCsvinMachineName;
  static readonly MACHINEINFOCSV_FROANAME: string = tData.S1401.machineInfoCsvfroaName;
  static readonly MACHINEINFOCSV_HEYANAME: string = tData.S1401.machineInfoCsvheyaName;
  static readonly MACHINEINFOCSV_BASYONAME: string = tData.S1401.machineInfoCsvbasyoName;
  static readonly MACHINEINFOCSV_MESSAGE: string = tData.S1401.machineInfoCsvmessage;

  static readonly PROJINFOCSV_BIGPROJ: string = tData.S1401.projInfoCsvbigProj;
  static readonly PROJINFOCSV_PROJID: string = tData.S1401.projInfoCsvprojId;
  static readonly PROJINFOCSV_PROJNAME: string = tData.S1401.projInfoCsvprojName;
  static readonly PROJINFOCSV_WORKID: string = tData.S1401.projInfoCsvworkId;
  static readonly PROJINFOCSV_WORKNAME: string = tData.S1401.projInfoCsvworkName;
  static readonly PROJINFOCSV_DOBEFORE: string = tData.S1401.projInfoCsvdoBefore;
  static readonly PROJINFOCSV_DOING: string = tData.S1401.projInfoCsvdoing;
  static readonly PROJINFOCSV_DOAFTER: string = tData.S1401.projInfoCsvdoafter;
  static readonly PROJINFOCSV_MESSAGE: string = tData.S1401.projInfoCsvmessage;

  static readonly PEOPLEINFOCSV_PEOPLEKBN: string = tData.S1401.peopleInfoCsvpeopleKbn;
  static readonly PEOPLEINFOCSV_PEOPLEID: string = tData.S1401.peopleInfoCsvpeopleId;
  static readonly PEOPLEINFOCSV_PEOPLENAME: string = tData.S1401.peopleInfoCsvpeopleName;
  static readonly PEOPLEINFOCSV_DOKAISHAID: string = tData.S1401.peopleInfoCsvdoKaishaId;
  static readonly PEOPLEINFOCSV_DOKAISHANAME: string = tData.S1401.peopleInfoCsvdoKaishaName;
  static readonly PEOPLEINFOCSV_WORKNAME: string = tData.S1401.peopleInfoCsvworkName;

  static readonly BLACKBOARDPROJINFOCSV_BIGPROJ: string = tData.S1401.blackboardProjInfoCsvbigProj;
  static readonly BLACKBOARDPROJINFOCSV_ANKENFLG: string = tData.S1401.blackboardProjInfoCsvankenFlg;
  static readonly BLACKBOARDPROJINFOCSV_FROAFLG: string = tData.S1401.blackboardProjInfoCsvfroaFlg;
  static readonly BLACKBOARDPROJINFOCSV_HEYAFLG: string = tData.S1401.blackboardProjInfoCsvheyaFlg;
  static readonly BLACKBOARDPROJINFOCSV_BASYOFLG: string = tData.S1401.blackboardProjInfoCsvbasyoFlg;
  static readonly BLACKBOARDPROJINFOCSV_LINEFLG: string = tData.S1401.blackboardProjInfoCsvlineFlg;
  static readonly BLACKBOARDPROJINFOCSV_MACHINEFLG: string = tData.S1401.blackboardProjInfoCsvmachineFlg;
  static readonly BLACKBOARDPROJINFOCSV_PROFLG: string = tData.S1401.blackboardProjInfoCsvproFlg;
  static readonly BLACKBOARDPROJINFOCSV_WORKFLG: string = tData.S1401.blackboardProjInfoCsvworkFlg;
  static readonly BLACKBOARDPROJINFOCSV_FREEDOMEARFLG: string = tData.S1401.blackboardProjInfoCsvfreedomearFlg;
  static readonly BLACKBOARDPROJINFOCSV_PEOPLEFLG: string = tData.S1401.blackboardProjInfoCsvpeopleFlg;
  static readonly BLACKBOARDPROJINFOCSV_DOKAISHAFLG: string = tData.S1401.blackboardProjInfoCsvdoKaishaFlg;
  static readonly BLACKBOARDPROJINFOCSV_DATEFLG: string = tData.S1401.blackboardProjInfoCsvdateFlg;
  static readonly BLACKBOARDPROJINFOCSV_TIMEFLG: string = tData.S1401.blackboardProjInfoCsvtimeFlg;
  static readonly BLACKBOARDPROJINFOCSV_MESSAGE: string = tData.S1401.blackboardProjInfoCsvmessage;
}

/**
 * S-13-02 システムエラー画面
 */
export abstract class S1302Constants {
  static readonly TITLE: string = tData.S1302.title;
}

/**
 * S-13-03 プライバシーポリシー画面
 */
export abstract class S1303Constants {
  static readonly TITLE: string = tData.S1303.title;
}

/**
 * S-15-01 公司员工信息系统
 */
 export abstract class S1501Constants {
  static readonly TITLE: string = tData.S1501.title;
  static readonly USERIDLABEL: string = tData.S1501.userIdLabel;
  static readonly USERNAMELABEL: string = tData.S1501.userNameLabel;

  static readonly USERID: string = tData.S1501.userId;
  static readonly USERNAME: string = tData.S1501.userName;
  static readonly COMPANYNAME: string = tData.S1501.companyName;
  static readonly AUTHORITYID: string = tData.S1501.authorityId;
  static readonly MAILADDRESS: string = tData.S1501.mailAddress;
  static readonly ISLOGIN: string = tData.S1501.isLogin;

  static readonly SEARCH: string = tData.S1501.search;
}


/**
 * S-16-01    在库管理
 */
 export abstract class S1601Constants {
  static readonly TITLE: string = tData.S1601.title;
  static readonly channelName: string = tData.S1601.channelName;
  static readonly channelID: string = tData.S1601.channelID;
  static readonly productType: string = tData.S1601.productType;
  static readonly productStatus: string = tData.S1601.productStatus;
  static readonly price: string = tData.S1601.price;
  static readonly productName: string = tData.S1601.productName;
  static readonly value: string = tData.S1601.value;
  static readonly status: string = tData.S1601.status;
  static readonly level: string = tData.S1601.level;
  static readonly split: string = tData.S1601.split;
  static readonly block: string = tData.S1601.block;
 }


/**
 * S-16-01    在库管理
 */
 export abstract class S1701Constants {
  static readonly TITLE: string = tData.S1701.title;
  static readonly channelName: string = tData.S1701.channelName;
  static readonly channelID: string = tData.S1701.channelID;
  static readonly productType: string = tData.S1701.productType;
  static readonly productStatus: string = tData.S1701.productStatus;
  static readonly price: string = tData.S1701.price;
  static readonly productName: string = tData.S1701.productName;
  static readonly value: string = tData.S1701.value;
  static readonly status: string = tData.S1701.status;
  static readonly level: string = tData.S1701.level;
  static readonly split: string = tData.S1701.split;
  static readonly block: string = tData.S1701.block;

}

/**
 * S-18-01 代理商汇总信息
 */
 export abstract class S1801Constants {
  static readonly TITLE: string = tData.S1801.title;
  static readonly MANAGER: string = tData.S1801.manager;
  static readonly SELECTMONEY: string = tData.S1801.selectMoney;
  static readonly CREATETIME: string = tData.S1801.createTime;
  static readonly AGENTNAME: string = tData.S1801.agentName;  
  static readonly AGENTREDUCENAME: string = tData.S1801.agentReduceName;
  static readonly ORDERMONEY: string = tData.S1801.orderMoney;
  static readonly ORDERSUCCESSMONEY: string = tData.S1801.orderSuccessMoney;
  static readonly ORDERTOTALSUMMARY: string = tData.S1801.orderTotalSummary;
  static readonly ORDERRUNNINGSUMMARY: string = tData.S1801.orderRunningSummary;
  static readonly ORDERSUCCESSSUMMARY: string = tData.S1801.orderSuccessSummary;
  static readonly ORDERSUCCESSRATE: string = tData.S1801.orderSuccessRate;
  static readonly ORDERSUCCESSTIME: string = tData.S1801.orderSuccessTime;
  static readonly ORDERFAILURETIME: string = tData.S1801.orderFailureTime;
  static readonly ORDERTHREEMINSRATE: string = tData.S1801.orderThreeMinsRate;
  static readonly ORDERTENMINSRATE: string = tData.S1801.orderTenMinsRate;
}


/**
 * S-19-01 代理商查询系统
 */
 export abstract class S1901Constants {
  static readonly TITLE: string = tData.S1501.title;
  static readonly USERIDLABEL: string = tData.S1501.userIdLabel;
  static readonly USERNAMELABEL: string = tData.S1501.userNameLabel;

  static readonly USERID: string = tData.S1501.userId;
  static readonly USERNAME: string = tData.S1501.userName;
  static readonly COMPANYNAME: string = tData.S1501.companyName;
  static readonly AUTHORITYID: string = tData.S1501.authorityId;
  static readonly MAILADDRESS: string = tData.S1501.mailAddress;
  static readonly ISLOGIN: string = tData.S1501.isLogin;

  static readonly SEARCH: string = tData.S1501.search;
}

/**
 * S-20-10 资金记录编辑画面
 */
export abstract class S2010Constants {
  static readonly TITLE: string = tData.S2010.title;
  static readonly AGENTNAME: string = tData.S2010.agentName;
  static readonly REQID: string = tData.S2010.reqId;
  static readonly ORDERID: string = tData.S2010.orderId;
  static readonly BILLID: string = tData.S2010.billId;
  static readonly AGENTREDUCENAME: string = tData.S2010.agentReduceName;
  static readonly ORDERFORM: string = tData.S2010.orderForm;
  static readonly ORDERTYPE: string = tData.S2010.orderType;
  static readonly CAPITAL: string = tData.S2010.capital;
  static readonly PRICE: string = tData.S2010.price;
  static readonly STATUS: string = tData.S2010.status;
  static readonly CREATEDDATETIME: string = tData.S2010.createdDatetime;
  static readonly CHANGE: string = tData.S2010.change;
  static readonly CANCEL: string = tData.S2010.cancel;
}

/**
 * S-20-11 订单管理
 */
 export abstract class S2011Constants {
  static readonly TITLE: string = tData.S2011.title;
  static readonly AGENTNAME: string = tData.S2011.agentName;
  static readonly CHANNELNAME: string = tData.S2011.channelName;
  static readonly ORDERID: string = tData.S2011.orderId;
  static readonly BILLID: string = tData.S2011.billId;
  static readonly REQUESTID: string = tData.S2011.requestId;
  static readonly NUMBER: string = tData.S2011.number;
  static readonly MONEY: string = tData.S2011.money;
  static readonly STATUS: string = tData.S2011.status;
  static readonly TIME: string = tData.S2011.time;
  static readonly CHANGE: string = tData.S2011.change;
  static readonly CANCEL: string = tData.S2011.cancel;
}


/**
 * S-20-12 货架管理
 */
 export abstract class S2012Constants {
  static readonly TITLE: string = tData.S2012.title;
  static readonly AGENTNAME: string = tData.S2012.agentName;
  static readonly PRODUCTNAME: string = tData.S2012.productName;
  static readonly PRODUCTTYPE: string = tData.S2012.productType;
  static readonly SERVICEPROVIDE: string = tData.S2012.serviceProvide;
  static readonly MONEY: string = tData.S2012.money;
  static readonly PRICE: string = tData.S2012.price;
  static readonly DISACCOUNT: string = tData.S2012.disaccount;
  static readonly CHECKPRICE: string = tData.S2012.checkPrice;
  static readonly CHECKCOST: string = tData.S2012.checkCost;
  static readonly STATUS: string = tData.S2012.status;
  static readonly CHANGE: string = tData.S2012.change;
  static readonly CANCEL: string = tData.S2012.cancel;
}



/**
 * 所有运营商信息
 */
 export abstract class ManagerConstants {
  static readonly MOBILE: string = tData.ManagerList.mobile;
  static readonly TELECOM: string = tData.ManagerList.telecom;
  static readonly UNICOM: string = tData.ManagerList.unicom;
}

/**
 * 所有交易面额信息
 */
 export abstract class MoneyConstants {
  static readonly TENYUAN: string = tData.MoneyList.tenYuan;
  static readonly TWENTYYUAN: string = tData.MoneyList.twentyYuan;
  static readonly THIRTYYUAN: string = tData.MoneyList.thirtyYuan;
  static readonly FIFTYYUAN: string = tData.MoneyList.fiftyYuan;
  static readonly ONEHUNDREDYUAN: string = tData.MoneyList.oneHundredYuan;
  static readonly TWOHUNDREDYUAN: string = tData.MoneyList.twoHundredYuan;
  static readonly THREEHUNDREDYUAN: string = tData.MoneyList.threeHundredYuan;
  static readonly FIVEHUNDREDYUAN: string = tData.MoneyList.fiveHundredYuan;
}

/**
 * Common
 */
 export abstract class CommonConstants {
  static readonly SUCCESS: string = tData.Common.success;
  static readonly FAILURE: string = tData.Common.failure;
}

/**
 * 機能ID一覧
 */
export abstract class PageId {
  static readonly S0101 = 'S-01-01';
  static readonly S0102 = 'S-01-02';
  static readonly S0201 = 'S-02-01';
  static readonly S0901 = 'S-09-01';
  static readonly S0902 = 'S-09-02';
  static readonly S0903 = 'S-09-03';
}

/**
 * 言語別住所編集順JSON仕様
 */
export abstract class AddressSort {
  static readonly ADDRESSSORTJA: string[] = adsort.JA;
  static readonly ADDRESSSORTUS: string[] = adsort.US;
}

/**
 * プライバシーポリシー文言
 */
export abstract class PrivacyPolicyMessage {
  static readonly PRIVACYPOLICYMSG: string = privacyPolicyData.privacyPolicy;
}
