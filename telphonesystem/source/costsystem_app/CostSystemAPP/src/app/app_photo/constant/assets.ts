import * as assetsdata from '../../../assets/json/assets.json';

const assetsData = assetsdata.default;

export abstract class S0301Assets {

  // 写真default image
  static readonly DEFAULTIMAGE: string = assetsData.S0301Assets.defaultImage;
  // 撮影button image
  static readonly CAMERAIMAGE: string = assetsData.S0301Assets.cameraImage;
  // NG image
  static readonly CAMERACHECKNG: string = assetsData.S0301Assets.cameraCheckNG;
  // OK image
  static readonly CAMERACHECKOK: string = assetsData.S0301Assets.cameraCheckOK;
  // check no image
  static readonly CAMERACHECKNONE: string = assetsData.S0301Assets.cameraCheckNone;
  // FLASHAUTO image
  static readonly CAMERAFLASHAUTO: string = assetsData.S0301Assets.cameraFlashAuto;
  // FLASH off image
  static readonly CAMERAFLASHOFF: string = assetsData.S0301Assets.cameraFlashOff;
  // FLASH show image
  static readonly CAMERAFLASHON: string = assetsData.S0301Assets.cameraFlashOn;

}

export abstract class ImageAssets {
  static readonly PHOTOBOOK_W: string = assetsData.photobookw;
  static readonly PHOTOBOOK_M: string = assetsData.photobookm;
  static readonly PHOTOCLOUD_W: string = assetsData.photocloudw;
  static readonly PHOTOCLOUD_M: string = assetsData.photocloudm;
}
