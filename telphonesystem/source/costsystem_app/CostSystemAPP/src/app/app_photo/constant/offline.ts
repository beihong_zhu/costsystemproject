import * as offlinedata from '../../../assets/json/offline.json';

const offlineData = offlinedata.default;

export abstract class OffLineConstants {
  /** オフライン可能画面 */
  static readonly OFFLINELIST: Array<string> = offlineData.OFFLINE;
  /** オフライン可能以外画面 */
  static readonly ONLINELIST: Array<string> = offlineData.ONLINE;
}
