import * as mdata from '../../../assets/json/message.json';

const mData = mdata.default;

export abstract class MessageConstants {
  /** 処理が正常に終了しました。 */
  static readonly I00001: string = mData.I00001;
  /** 登録が完了しました。 */
  static readonly I00002: string = mData.I00002;
  /** 更新が完了しました。 */
  static readonly I00003: string = mData.I00003;
  /** 削除が完了しました。 */
  static readonly I00004: string = mData.I00004;
  /** アップロードが完了しました。 */
  static readonly I00005: string = mData.I00005;
  /** ダウンロードが完了しました。 */
  static readonly I00006: string = mData.I00006;
  /** 登録されたメールアドレスへ、ログイン確認メールを送信しています。承認を行った後に、ログインボタンを押してください。 */
  static readonly I00007: string = mData.I00007;
  /** QRコードが枠内に収まるようにカメラを向けてください。 */
  static readonly I00008: string = mData.I00008;
  /** Copyright＠2020 DAIKIN INDUSTRIES, LTD. All Rights Reserved. */
  static readonly I00009: string = mData.I00009;
  /** QRコードから情報を読み取りました。 */
  static readonly I00010: string = mData.I00010;
  /** QRコードの情報が読み取れませんでした。撮影をやり直してください。 */
  static readonly I00012: string = mData.I00012;
  /** 手動登録 */
  static readonly I00013: string = mData.I00013;
  /** アップロードが完了しました。元の画面に戻ります。 */
  static readonly I00014: string = mData.I00014;
  /** パスワードを変更しました。 */
  static readonly I00015: string = mData.I00015;
  /** 変更が完了しました。 */
  static readonly I00016: string = mData.I00016;
  /** 登録内容の上位200行のみ表示されます。 */
  static readonly I00017: string = mData.I00017;
  /** 写真帳出力対象の変更が完了しました。 */
  static readonly I00018: string = mData.I00018;
  /** 登録内容の各CSV上位50行のみ表示されます。 */
  static readonly I00019: string = mData.I00019;
  /** 選択されたピンは既に図面に登録されています。 */
  static readonly I00020: string = mData.I00020;
  /** 登録が完了しました。工程登録画面、工程編集画面以外の画面で登録内容を表示するには再度、大工程選択画面から選択し直してください。 */
  static readonly I00022: string = mData.I00022;
  /** 変更が完了しました。工程登録画面、工程編集画面以外の画面で変更内容を表示するには再度、大工程選択画面から選択し直してください。 */
  static readonly I00023: string = mData.I00023;
  /** 削除が完了しました。工程登録画面、工程編集画面以外の画面で削除内容を表示するには再度、大工程選択画面から選択し直してください。 */
  static readonly I00024: string = mData.I00024;
  /** 写真帳「{{value1}}」を【{{value2}}】に保存しました。 */
  static readonly I00025: string = mData.I00025;
  /** 検索結果が{value}件を超えたため表示されていないデータがあります。条件検索してください。 */
  static readonly I00026: string = mData.I00026;
  /** 機番の情報を保存しました。\r\nオフラインのため、オンラインへ復帰後システムに登録されます。 */
  static readonly I00027: string = mData.I00027;

  /** 対象データが0件のため、実行できませんでした。 */
  static readonly W00001: string = mData.W00001;
  /** 対象を1つ以上選択してください。 */
  static readonly W00002: string = mData.W00002;
  /** アカウントがロックされています。システム管理者にお問い合わせください。 */
  static readonly W00003: string = mData.W00003;
  /** 検索結果が0 件です。 */
  static readonly W00004: string = mData.W00004;
  /** 登録される件数は20,000件までになります。超過する場合はファイルを分けて登録してください。 */
  static readonly W00005: string = mData.W00005;
  /** テンプレートから工程・作業が取得できません。工程登録画面から追加してください。 */
  static readonly W00006: string = mData.W00006;

  /** 必須項目が入力されていません。 */
  static readonly E00001: string = mData.E00001 + '（E00001）';
  /** ログインに失敗しました。再度ログインを行ってください。 */
  static readonly E00002: string = mData.E00002 + '（E00002）';
  /** 登録されたメールアドレスへログイン確認メールを送信しています。承認を行った後に再度ログインボタンを押して下さい。メールが届かない場合は、システム管理者にお問い合わせ下さい。 */
  static readonly E00003: string = mData.E00003 + '（E00003）';
  /** 対象の削除に失敗しました。 */
  static readonly E00004: string = mData.E00004 + '（E00004）';
  /** 対象のアップロードに失敗しました。 */
  static readonly E00005: string = mData.E00005 + '（E00005）';
  /** 対象のダウンロードに失敗しました。 */
  static readonly E00006: string = mData.E00006 + '（E00006）';
  /** 入力できる最大文字数は*文字です。 */
  static readonly E00007: string = mData.E00007 + '（E00007）';
  /** 特殊な文字は使用できません。 */
  static readonly E00008: string = mData.E00008 + '（E00008）';
  /** ***の値が不正です。 */
  static readonly E00009: string = mData.E00009 + '（E00009）';
  /** ユーザ登録に失敗しました。再度登録を行っても失敗する場合は、システム管理者にお問い合わせください。 */
  static readonly E00010: string = mData.E00010 + '（E00010）';
  /** ユーザ更新に失敗しました。再度更新を行っても失敗する場合は、システム管理者にお問い合わせください。 */
  static readonly E00011: string = mData.E00011 + '（E00011）';
  /** ユーザID、もしくはパスワードが未入力です。 */
  static readonly E00012: string = mData.E00012 + '（E00012）';
  /** ***が未選択です。 */
  static readonly E00013: string = mData.E00013 + '（E00013）';
  /** ログインに失敗した回数が上限を超えた為、一時的にログインを制限します。 */
  static readonly E00014: string = mData.E00014 + '（E00014）';
  /** エラーが発生しました。写真をアップロードできません。 */
  static readonly E00016: string = mData.E00016 + '（E00016）';
  /** 担当物件が０件のため、ログアウトします。案件責任者にお問い合わせください。 */
  static readonly E00017: string = mData.E00017 + '（E00017）';
  /** 未入力の項目があります。すべての項目を入力してください。 */
  static readonly E00018: string = mData.E00018 + '（E00018）';
  /** ユーザID、または現在パスワードが不正です。再度入力してください。 */
  static readonly E00019: string = mData.E00019 + '（E00019）';
  /** 新しいパスワードと確認用パスワードが不一致です。再度入力してください。 */
  static readonly E00020: string = mData.E00020 + '（E00020）';
  /** データをアップロードできませんでした。 */
  static readonly E00021: string = mData.E00021 + '（E00021）';
  /** 機番情報の更新に失敗しました。再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00022: string = mData.E00022 + '（E00022）';
  /** 読み取り中にエラーが発生しました。 */
  static readonly E00023: string = mData.E00023 + '（E00023）';
  /** システムエラーが発生しました。 */
  static readonly E00024: string = mData.E00024;
  /** ご迷惑をおかけして申し訳ありません。ログイン画面からやり直してください。(***) */
  static readonly E00025: string = mData.E00025;
  /** 入力されたパスコードが不正です。再度入力してください。 */
  static readonly E00026: string = mData.E00026 + '（E00026）';
  /** トークを送信できません。\r\n通信環境の良いところで再送信しても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00027: string = mData.E00027 + '（E00027）';
  /** チャットルーム作成画面を表示できません。 */
  static readonly E00028: string = mData.E00028 + '（E00028）';
  /** チャット画面を表示できません。 */
  static readonly E00029: string = mData.E00029 + '（E00029）';
  /** CSVファイルの登録対象テーブルに誤りがあります。 */
  static readonly E00030: string = mData.E00030 + '（E00030）';
  /** CSVファイルの列数に誤りがあります。 */
  static readonly E00031: string = mData.E00031 + '（E00031）';
  /** ***行目:入力項目の列数に誤りがあります。 */
  static readonly E00032: string = mData.E00032 + '（E00032）';
  /** ***行目:必須項目が入力されていません。 */
  static readonly E00033: string = mData.E00033 + '（E00033）';
  /** 削除するユーザ情報が選択されていません */
  static readonly E00034: string = mData.E00034 + '（E00034）';
  /** チャットルーム編集画面を表示できません。 */
  static readonly E00035: string = mData.E00035 + '（E00035）';
  /** 出力する工程を選択してください。 */
  static readonly E00036: string = mData.E00036 + '（E00036）';
  /** チャットルームを作成できませんでした。 */
  static readonly E00037: string = mData.E00037 + '（E00037）';
  /** ルーム名を入力してください。 */
  static readonly E00038: string = mData.E00038 + '（E00038）';
  /** 更新するアカウント情報が選択されていません。 */
  static readonly E00039: string = mData.E00039 + '（E00039）';
  /** チャットルームを編集できませんでした。 */
  static readonly E00040: string = mData.E00040 + '（E00040）';
  /** ユーザIDと現在パスワードの認証に失敗した回数が上限を超えた為、一時的にパスワード変更を制限します。 */
  static readonly E00041: string = mData.E00041 + '（E00041）';
  /** 選択できません。選択できる写真枚数を超えています。 */
  static readonly E00042: string = mData.E00042 + '（E00042）';
  /** 選択したファイルがCSVファイルではありません。CSVファイルを選択してください。 */
  static readonly E00043: string = mData.E00043 + '（E00043）';
  /** 更新対象がありません。 */
  static readonly E00044: string = mData.E00044 + '（E00044）';
  /** 追加登録できませんでした。登録できる{0}は99件までです。 */
  static readonly E00045: string = mData.E00045 + '（E00045）';
  /** この端末でのアクセスは現在制限されています。システム管理者({value})にお問い合わせください。 */
  static readonly E00046: string = mData.E00046 + '（E00046）';
  /** 選択できません。1つの配置欄に設定できる最大数は4です。 */
  static readonly E00047: string = mData.E00047 + '（E00047）';
  /** 他の人が編集作業中です。時間をおいてから再度処理をおこなってください。 */
  static readonly E00048: string = mData.E00048 + '（E00048）';
  /** リクエストのヘッダートークンが不正です。(リクエストヘッダー = *** */
  static readonly E00049: string = mData.E00049 + '（E00049）';
  /** 必須項目に値が設定されていません。(CSVパス = ***、 エラー項目 = ***) */
  static readonly E00050: string = mData.E00050 + '（E00050）';
  /** リクエストヘッダーからユーザIDの取得に失敗しました。 */
  static readonly E00051: string = mData.E00051 + '（E00051）';
  /** 登録用CSVファイルが存在しません。(ファイルパス = ***) */
  static readonly E00052: string = mData.E00052 + '（E00052）';
  /** CSVファイルにレコードが存在しません。(ファイルパス = ***) */
  static readonly E00053: string = mData.E00053 + '（E00053）';
  /** 値の設定は必須です。(対象項目 = ***) */
  static readonly E00054: string = mData.E00054 + '（E00054）';
  /** 値の形式が不正です。(対象項目 = ***) */
  static readonly E00055: string = mData.E00055 + '（E00055）';
  /** 桁数数が不正です。(対象項目 = ***) */
  static readonly E00056: string = mData.E00056 + '（E00056）';
  /** 登録時に異常終了発生。(エラー発生テーブル名 = ***) */
  static readonly E00057: string = mData.E00057 + '（E00057）';
  /** 機器名複数設定エラー。室外機、室内機両方に名称が設定されています。 */
  static readonly E00058: string = mData.E00058 + '（E00058）';
  /** 登録区分が不正です。(フロア登録区分 = ***、部屋登録区分 = ***、場所登録区分 = ***) */
  static readonly E00059: string = mData.E00059 + '（E00059）';
  /** 登録区分が全て変更無しと設定されています。 */
  static readonly E00060: string = mData.E00060 + '（E00060）';
  /** 削除時に異常終了発生。(エラー発生テーブル名 = ***) */
  static readonly E00061: string = mData.E00061 + '（E00061）';
  /** 更新時に異常終了発生。(エラー発生テーブル名 = ***) */
  static readonly E00062: string = mData.E00062 + '（E00062）';
  /** 取得時に異常終了発生。(エラー発生テーブル名 = ***) */
  static readonly E00063: string = mData.E00063 + '（E00063）';
  /** 写真帳出力対象の変更に失敗しました。 */
  static readonly E00064: string = mData.E00064 + '（E00064）';
  // "{{value1}}行目:{{value2}}の値が不正です"
  static readonly E00065: string = mData.E00065 + '（E00065）';
  // "{{value1}}行目:{{value2}}に値が入力されていません。"
  static readonly E00066: string = mData.E00066 + '（E00066）';
  // "{{value1}}行目:{{value2}}の値に入力できる最大文字数は{{value3}}文字です"
  static readonly E00067: string = mData.E00067 + '（E00067）';
  /** 通信エラー、又はシステムでエラーが発生しました。\r\n通信環境の良いところで操作しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00069: string = mData.E00069 + '（E00069）';
  // "パスコードの有効期限が切れています。ログインからやり直してください。"
  static readonly E00070: string = mData.E00070 + '（E00070）';
  // 選択したファイルがPDFファイルではありません。PDFファイルを選択してください。"
  static readonly E00071: string = mData.E00071 + '（E00071）';
  // 定したPDFファイルが存在しません。
  static readonly E00072: string = mData.E00072 + '（E00072）';
  // CSVで指定したPDFファイルをすべて参照してください。
  static readonly E00073: string = mData.E00073 + '（E00073）';
  /** 選択された工程の写真が存在しません。出力対象を再度ご確認ください。 */
  static readonly E00074: string = mData.E00074 + '（E00074）';
  // 新しいパスワードは現在のパスワードと異なるパスワードとしてください。
  static readonly E00075: string = mData.E00075 + '（E00075）';
  // オフライン状態のため処理できません。オンライン状態の時に操作してください。
  static readonly E00076: string = mData.E00076 + '（E00076）';
  /** 対象物件を選択してください。 */
  static readonly E00078: string = mData.E00078 + '（E00078）';
  /** CSVファイルを選択してください。 */
  static readonly E00079: string = mData.E00079 + '（E00079）';
  // 選択した図面ファイルサイズが上限を超えています。8MB以下の図面ファイルを指定してください。
  static readonly E00080: string = mData.E00080 + '（E00080）';
  // 図面ファイルのアップロード処理に失敗しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。。
  static readonly E00081: string = mData.E00081 + '（E00081）';
  /** 対象データが0件のため、実行できませんでした。 */
  static readonly E00083: string = mData.E00083 + '（E00083）';
  /** 同期予定の写真が含まれるため、削除できません。 */
  static readonly E00092: string = mData.E00092 + '（E00092）';
  /** オフライン状態のため、実行できません。オンライン状態になってから、もう一度実行してください。 */
  static readonly E00094: string = mData.E00094 + '（E00094）';
  /** 写真が選択されていませんので、写真を選択後、もう一度実行してください。 */
  static readonly E00095: string = mData.E00095 + '（E00095）';
  /** 写真帳の対象に変更がありませんでした。写真を１つ以上変更後、もう一度実行してください。 */
  static readonly E00096: string = mData.E00096 + '（E00096）';
  /** データの取得ができませんでした。操作をやり直してください。 */
  static readonly E00099: string = mData.E00099 + '（E00099）';
  /** 読み取り中にエラーが発生しました。 */
  static readonly E00101: string = mData.E00101 + '（E00101）';
  /** 読み取りに失敗しました。QRコードの読み込みが許可されているか確認してください。 */
  static readonly E00102: string = mData.E00102 + '（E00102）';
  /** ログインに失敗した回数が上限を超えた為、一時的にログインを制限しています。 */
  static readonly E00104: string = mData.E00104 + '（E00104）';
  /** 写真をアップロード時にリクエストの解析に失敗しました。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00105: string = mData.E00105 + '（E00105）';
  /** アプリでは利用できません。ブラウザをご利用ください。 */
  static readonly E00106: string = mData.E00106 + '（E00106）';
  /** ブラウザでは利用できません。アプリから起動してください。 */
  static readonly E00107: string = mData.E00107 + '（E00107）';
  /** 別のタブで工事写真アプリを開いているため表示できません。 */
  static readonly E00108: string = mData.E00108;
  /** ユーザID、またはパスワードが不正です。再度入力してください。 */
  static readonly E00110: string = mData.E00110 + '（E00110）';
  /** システムで異常が発生しました。時間をおいて再度、実施してください。再度、実施しても同じ異常が発生する場合は、システム管理者にお問い合わせください。 */
  static readonly E00111: string = mData.E00111 + '（E00111）';
  /** 不正なアクセスが行われました。\r\n再度、実施しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00112: string = mData.E00112 + '（E00112）';
  /** データ送受信時において、容量制限エラーが発生しました。システム管理者({1})にお問い合わせください。 */
  static readonly E00113: string = mData.E00113 + '（E00113）';
  /** ログインに失敗しました。\r\n通信環境の良いところで再度ログインを行っても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00114: string = mData.E00114 + '（E00114）';
  /** \r\n6～20文字の半角英字、半角数字を含めたパスワードを入力してください。 */
  static readonly E00115: string = mData.E00115 + '（E00115）';
  /** CSVファイルのアップロード処理に失敗しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00116: string = mData.E00116 + '（E00116）';
  /** トークを送信できません。\r\n通信環境の良いところで再送信しても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00117: string = mData.E00117 + '（E00117）';
  /** トークを送信できません。\r\n通信環境の良いところで再送信しても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00118: string = mData.E00118 + '（E00118）';
  /** トークを送信できません。\r\n通信環境の良いところで再送信しても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00119: string = mData.E00119 + '（E00119）';
  /** トークの情報更新に失敗したため、トークを送信できません。\r\n通信環境の良いところで再送信しても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00120: string = mData.E00120 + '（E00120）';
  /** トークの通知に失敗しました。\r\n通信環境の良いところで再送信しても同じ異常が発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00121: string = mData.E00121 + '（E00121）';
  /** チャットルームを編集できませんでした。 */
  static readonly E00122: string = mData.E00122 + '（E00122）';
  /** チャットルームを編集できませんでした。 */
  static readonly E00123: string = mData.E00123 + '（E00123）';
  /** チャットルームの作成中の送信が失敗したため、チャットルームを作成できませんでした。 */
  static readonly E00124: string = mData.E00124 + '（E00124）';
  /** 案件、ユーザーの紐づけに失敗したため、チャットルームを編集できませんでした。 */
  static readonly E00125: string = mData.E00125 + '（E00125）';
  /** 図面ファイルのアップロード処理に失敗しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00126: string = mData.E00126 + '（E00126）';
  /** 図面ファイルのアップロード処理に失敗しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00127: string = mData.E00127 + '（E00127）';
  /** 写真撮影に失敗しました。再撮影をお願いします。再度撮影しても同じエラーが発生する場合は、システム管理者({1})にお問い合わせください。 */
  static readonly E00128: string = mData.E00128 + '（E00128）';
  /** {0}名が重複しています。別の名称を使用してください。 */
  static readonly E00129: string = mData.E00129 + '（E00129）';
  /** {1}行目:ユーザIDに半角スペースは使用できません。 */
  static readonly E00130: string = mData.E00130 + '（E00130）';
  /** 写真帳出力対象の変更に失敗しました。 */
  static readonly E00132: string = mData.E00132 + '（E00132）';
  /** 写真帳出力対象の変更に失敗しました。 */
  static readonly E00133: string = mData.E00133 + '（E00133）';
  /** 図面ファイルのアップロード処理に予期せぬエラーが発生しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00134: string = mData.E00134 + '（E00134）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00135: string = mData.E00135 + '（E00135）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00136: string = mData.E00136 + '（E00136）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00137: string = mData.E00137 + '（E00137）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00138: string = mData.E00138 + '（E00138）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00139: string = mData.E00139 + '（E00139）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00140: string = mData.E00140 + '（E00140）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00141: string = mData.E00141 + '（E00141）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00142: string = mData.E00142 + '（E00142）';
  /** 写真をアップロードできませんでした。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00143: string = mData.E00143 + '（E00143）';
  /** 機番情報の更新に失敗しました。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00144: string = mData.E00144 + '（E00144）';
  /** 機番情報の更新に失敗しました。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00145: string = mData.E00145 + '（E00145）';
  /** 機番情報の更新に失敗しました。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00146: string = mData.E00146 + '（E00146）';
  /** 機番情報の更新に失敗しました。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00147: string = mData.E00147 + '（E00147）';
  /** 機番情報の更新に失敗しました。\r\n再送信する場合は自動同期を一度OFFにした後、ONに変更してください。 */
  static readonly E00148: string = mData.E00148 + '（E00148）';
  /** 通信がオフラインのため、通信エラーが発生しました。\r\n通信環境の良いところで操作しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00149: string = mData.E00149 + '（E00149）';
  /** 通信エラー、又はシステムでエラーが発生しました。\r\n通信環境の良いところで操作しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。({2}) */
  static readonly E00150: string = mData.E00150 + '（E00150）';
  /** CSVファイルのアップロード処理に失敗しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00151: string = mData.E00151 + '（E00151）';
  /** CSVファイルのアップロード処理に失敗しました。\r\n再度実行しても同じエラーが発生する場合は、\r\nシステム管理者({1})にお問い合わせください。 */
  static readonly E00152: string = mData.E00152 + '（E00152）';
  /** 削除時に予期せぬエラーが発生。(エラー発生テーブル名 = {1}) */
  static readonly E00153: string = mData.E00153 + '（E00153）';
  /** 更新時に予期せぬエラーが発生。(エラー発生テーブル名 = {1}) */
  static readonly E00154: string = mData.E00154 + '（E00154）';
  /** チャットルームを作成できませんでした。 */
  static readonly E00155: string = mData.E00155 + '（E00155）';
  /** チャットルームを作成できませんでした。 */
  static readonly E00156: string = mData.E00156 + '（E00156）';
  /** 登録時に異常終了しました。(エラー発生テーブル名 = {1}) */
  static readonly E00159: string = mData.E00159 + '（E00159）';
  /** 登録時に異常終了しました。(エラー発生テーブル名 = {1}) */
  static readonly E00160: string = mData.E00160 + '（E00160）';
  /** 削除時に異常終了しました。(エラー発生テーブル名 = {1}) */
  static readonly E00161: string = mData.E00161 + '（E00161）';
  /** 削除時に異常終了しました。(エラー発生テーブル名 = {1}) */
  static readonly E00162: string = mData.E00162 + '（E00162）';
  /** 更新時に異常終了しました。(エラー発生テーブル名 = {1}) */
  static readonly E00163: string = mData.E00163 + '（E00163）';
  /** 更新時に異常終了しました。(エラー発生テーブル名 = {1}) */
  static readonly E00164: string = mData.E00164 + '（E00164）';

  /** 単項目チェックエラー。 */
  static readonly EIF001: string = mData.EIF001;
  /** 型チェックエラー */
  static readonly EIF002: string = mData.EIF002;
  /** 桁数チェックエラー */
  static readonly EIF003: string = mData.EIF003;
  /** 検索時に異常終了発生。 */
  static readonly EIF004: string = mData.EIF004;
  /** 検索結果設定時に異常終了発生。 */
  static readonly EIF005: string = mData.EIF005;
  /** 登録時に異常終了発生。 */
  static readonly EIF006: string = mData.EIF006;
  /** 排他エラー。 */
  static readonly EIF007: string = mData.EIF007;
  /** 更新時に異常終了発生。 */
  static readonly EIF008: string = mData.EIF008;
  /** 削除時に異常終了発生。 */
  static readonly EIF009: string = mData.EIF009;
  /** 画像、PDFファイル変換時に異常終了発生。 */
  static readonly EIF010: string = mData.EIF010;
  /** APIの呼出が失敗しました。 */
  static readonly ECT001: string = mData.ECT001;
  /** アクセス許可フラグ妥当性が不正。 */
  static readonly ECT004: string = mData.ECT004;
  /** アカウントロックフラグ妥当性が不正。 */
  static readonly ECT005: string = mData.ECT005;
  /** ピン情報配置フラグ妥当性が不正。 */
  static readonly ECT006: string = mData.ECT006;
  /** 半角英数のみ。 */
  static readonly ECT007: string = mData.ECT007;
  /** 必須項目です。 */
  static readonly ECT008: string = mData.ECT008;
  /** 半角数字のみ。 */
  static readonly ECT009: string = mData.ECT009;
  /** 桁数が範囲外。 */
  static readonly ECT010: string = mData.ECT010;
  /** ソート妥当性が不正。 */
  static readonly ECT011: string = mData.ECT011;
  /** ソート項目、昇順/降順のいずれが設定してない。 */
  static readonly ECT012: string = mData.ECT012;
  /** 写真帳出力フラグ妥当性が不正。 */
  static readonly ECT013: string = mData.ECT013;
  /** 工程ID、作業IDのいずれが設定してない。 */
  static readonly ECT014: string = mData.ECT014;
  /** CSVファイルの項目数不正。 */
  static readonly ECT015: string = mData.ECT015;
  /** 黒板表示フラグ妥当性が不正。 */
  static readonly ECT016: string = mData.ECT016;
  /** S3ファイル取得失敗 */
  static readonly ECT017: string = mData.ECT017;
  /** 大工程ID妥当性が不正。 */
  static readonly ECT018: string = mData.ECT018;
  /** 日付のみ。 */
  static readonly ECT019: string = mData.ECT019;
  /** 大工程IDのコード値が「03」と「04」のみ */
  static readonly ECT020: string = mData.ECT020;
  /** 出力パターン妥当性が不正。 */
  static readonly ECT021: string = mData.ECT021;
  /** 削除フラグ妥当性が不正。 */
  static readonly ECT022: string = mData.ECT022;
  /** {{value0}}データ1件以上で、{{value1}}が不正。 */
  static readonly ECT023: string = mData.ECT023;
  /** {{value0}}登録時に、{{value1}}でデータ抽出が失敗で、異常終了発生。 */
  static readonly ECT024: string = mData.ECT024;
  /** 帳票出力異常終了発生。 */
  static readonly ECT025: string = mData.ECT025;
  /** テーブルのキーが衝突します。 */
  static readonly ECT026: string = mData.ECT026;
  /** CSV読み込みが失敗しました。 */
  static readonly ECT027: string = mData.ECT027;
  /** {{value}}が失敗しました。 */
  static readonly ECT028: string = mData.ECT028;
  /** エクセル生成時のエラー。 */
  static readonly ECT029: string = mData.ECT029;
  /** エクセル生成のテンプレートファイル取得失敗。 */
  static readonly ECT030: string = mData.ECT030;
  /** アマゾンS3からファイルダウンロード失敗。 */
  static readonly ECT031: string = mData.ECT031;
  /** CSV同期ファイル生成失敗。 */
  static readonly ECT032: string = mData.ECT032;
  /** 登録データが存在で、対象テーブル名：{0}、対象項目名：{1}。 */
  static readonly ECT033: string = mData.ECT033;
  /** 削除データが存在しないで、対象テーブル名：{0}、対象項目名：{1}。 */
  static readonly ECT034: string = mData.ECT034;
  /** 対象アップロードPDFが存在しない。 */
  static readonly ECT035: string = mData.ECT035;
  /** 対象テーブル{0}の操作エラー発生。 */
  static readonly ECT036: string = mData.ECT036;
  /** 図面ファイル削除時、エラー発生し、異常終了。 */
  static readonly ECT037: string = mData.ECT037;
  /** 項目ユニーコードが不正、対象項目名：{0}。 */
  static readonly ECT038: string = mData.ECT038;

  /** 実行します。よろしいですか？ */
  static readonly C00001: string = mData.C00001;
  /** 削除します。よろしいですか？ */
  static readonly C00002: string = mData.C00002;
  /** アップロードします。よろしいですか？ */
  static readonly C00003: string = mData.C00003;
  /** ダウンロードします。よろしいですか？ */
  static readonly C00004: string = mData.C00004;
  /** チェック済の項目を削除します。よろしいですか？ */
  static readonly C00005: string = mData.C00005;
  /** ログアウトしますか？ */
  static readonly C00006: string = mData.C00006;
  /** 事前登録した「機種名」と、読み取った「機種名」が違います。読み取った「機種名」を登録します。よろしいですか？ */
  static readonly C00007: string = mData.C00007;
  /** ピンを配置します、よろしいですか？ */
  static readonly C00008: string = mData.C00008;
  /** ピンを移動します。よろしいですか？ */
  static readonly C00009: string = mData.C00009;
  /** 図面上からピンを削除します。よろしいですか？ */
  static readonly C00010: string = mData.C00010;
  /** ルーム作成を終了して一覧画面に戻りますか？ */
  static readonly C00011: string = mData.C00011;
  /** 選択したアカウント情報のアカウントロックを解除します。よろしいですか？ */
  static readonly C00012: string = mData.C00012;
  /** ルーム編集を終了して元の画面に戻りますか？ */
  static readonly C00013: string = mData.C00013;
  /** 変更されていませんが、完了してよろしいですか？ */
  static readonly C00014: string = mData.C00014;
  /** 未送信のメッセージがあります。チャットを終了してよろしいですか？ */
  static readonly C00015: string = mData.C00015;
  /** 更新します。よろしいですか？ */
  static readonly C00016: string = mData.C00016;
}


