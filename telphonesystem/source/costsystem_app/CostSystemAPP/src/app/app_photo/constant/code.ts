import * as codedata from '../../../assets/json/code.json';

const codeData = codedata.default;


export abstract class ListConstants {
  /** CD-0001_大工程 */
  static readonly CD0001_LIST: [] = codeData.CD0001_LIST;
  /** CD-0005_複数選択 */
  static readonly CD0005_LIST: [] = codeData.CD0005_LIST;
  /** CD-0006_表示方法 */
  static readonly CD0006_LIST: [] = codeData.CD0006_LIST;
  /** CD-0007_ソート設定 */
  static readonly CD0007_LIST: [] = codeData.CD0007_LIST;
  /** CD-0008_昇順降順 */
  static readonly CD0008_LIST: [] = codeData.CD0008_LIST;
  /** CD-0009_表示設定 */
  static readonly CD0009_LIST: [] = codeData.CD0009_LIST;
  /** CD-0024_状況 */
  static readonly CD0024_LIST: [] = codeData.CD0024_LIST;
  /** CD-filmed_状況 */
  static readonly CDFILMED_LIST: [] = codeData.CDFILMED_LIST;

}

/**
 * 大工程
 */
export abstract class CD0001Constants {

  /** 01:提案前現地調査 */
  static readonly CD0001_01: string = codeData.CD0001.CD0001_01;
  static readonly CD0001_01_VALUE: string = codeData.CD0001.CD0001_01_VALUE;
  static readonly CD0001_01_TIMNGLIST: [] = codeData.CD0001.CD0001_01_TIMNGLIST;

  /** 02:工事前現地調査 */
  static readonly CD0001_02: string = codeData.CD0001.CD0001_02;
  static readonly CD0001_02_VALUE: string = codeData.CD0001.CD0001_02_VALUE;
  static readonly CD0001_02_TIMNGLIST: [] = codeData.CD0001.CD0001_02_TIMNGLIST;

  /** 03:施工試運転 */
  static readonly CD0001_03: string = codeData.CD0001.CD0001_03;
  static readonly CD0001_03_VALUE: string = codeData.CD0001.CD0001_03_VALUE;
  static readonly CD0001_03_TIMNGLIST: [] = codeData.CD0001.CD0001_03_TIMNGLIST;

  /** 04:運用中 */
  static readonly CD0001_04: string = codeData.CD0001.CD0001_04;
  static readonly CD0001_04_VALUE: string = codeData.CD0001.CD0001_04_VALUE;
  static readonly CD0001_04_TIMNGLIST: [] = codeData.CD0001.CD0001_04_TIMNGLIST;

}

/**
 * 黒板出力有無
 */
export abstract class CD0002Constants {

  /** 01:ON */
  static readonly SWICH_ON: string = codeData.CD0002.SWICH_ON;
  static readonly SWICH_ON_VALUE: string = codeData.CD0002.SWICH_ON_VALUE;
  /** 02:OFF */
  static readonly SWICH_OFF: string = codeData.CD0002.SWICH_OFF;
  static readonly SWICH_OFF_VALUE: string = codeData.CD0002.SWICH_OFF_VALUE;
}

/**
 * 作業判定
 */
export abstract class CD0003Constants {

  /** 01:ブランク */
  static readonly WORK_JUDGE_BLANK: string = codeData.CD0003.WORK_JUDGE_BLANK;
  static readonly WORK_JUDGE_BLANK_VALUE: string = codeData.CD0003.WORK_JUDGE_BLANK_VALUE;
  /** 02:OK */
  static readonly WORK_JUDGE_OK: string = codeData.CD0003.WORK_JUDGE_OK;
  static readonly WORK_JUDGE_OK_VALUE: string = codeData.CD0003.WORK_JUDGE_OK_VALUE;
  /** 03:NG */
  static readonly WORK_JUDGE_NG: string = codeData.CD0003.WORK_JUDGE_NG;
  static readonly WORK_JUDGE_NG_VALUE: string = codeData.CD0003.WORK_JUDGE_NG_VALUE;
}

/**
 * フラッシュ機能制御
 */
export abstract class CD0004Constants {

  /** 01:ON */
  static readonly FLASH_STATUS_ON: string = codeData.CD0004.FLASH_STATUS_ON;
  static readonly FLASH_STATUS_ON_VALUE: string = codeData.CD0004.FLASH_STATUS_ON_VALUE;
  /** 02:OFF */
  static readonly FLASH_STATUS_OFF: string = codeData.CD0004.FLASH_STATUS_OFF;
  static readonly FLASH_STATUS_OFF_VALUE: string = codeData.CD0004.FLASH_STATUS_OFF_VALUE;
  /** 03:AUTO */
  static readonly FLASH_STATUS_AUTO: string = codeData.CD0004.FLASH_STATUS_AUTO;
  static readonly FLASH_STATUS_AUTO_VALUE: string = codeData.CD0004.FLASH_STATUS_AUTO_VALUE;
}

/**
 * 写真一覧処理選択
 */
export abstract class CD0005Constants {

  /** 1:手動同期 */
  static readonly HAND_UPLOAD: string = codeData.CD0005.HAND_UPLOAD;
  static readonly HAND_UPLOAD_VALUE: string = codeData.CD0005.HAND_UPLOAD_VALUE;
  /** 2:削除 */
  static readonly DELETE: string = codeData.CD0005.DELETE;
  static readonly DELETE_VALUE: string = codeData.CD0005.DELETE_VALUE;

  /** 3:写真帳選択 */
  static readonly PHOTO_BOOK: string = codeData.CD0005.PHOTO_BOOK;
  static readonly PHOTO_BOOK_VALUE: string = codeData.CD0005.PHOTO_BOOK_VALUE;
}

/**
 * 写真一覧表示方法選択
 */
export abstract class CD0006Constants {

  /** 1:フィルタ */
  static readonly FLITER: string = codeData.CD0006.FLITER;
  static readonly FLITER_VALUE: string = codeData.CD0006.FLITER_VALUE;
  /** 2:ソート */
  static readonly SORT: string = codeData.CD0006.SORT;
  static readonly SORT_VALUE: string = codeData.CD0006.SORT_VALUE;
  /** 2:表示形式 */
  static readonly DISPLAY: string = codeData.CD0006.DISPLAY;
  static readonly DISPLAY_VALUE: string = codeData.CD0006.DISPLAY_VALUE;
}

/**
 * 写真一覧ソートパターン
 */
export abstract class CD0007Constants {

  /** 1:工程 */
  static readonly PROCESS: string = codeData.CD0007.PROCESS;
  static readonly PROCESS_VALUE: string = codeData.CD0007.PROCESS_VALUE;
  /** 2:機器 */
  static readonly MACHINE: string = codeData.CD0007.MACHINE;
  static readonly MACHINE_VALUE: string = codeData.CD0007.MACHINE_VALUE;
  /** 3:日付 */
  static readonly DATE: string = codeData.CD0007.DATE;
  static readonly DATE_VALUE: string = codeData.CD0007.DATE_VALUE;
  /** 4:フロア */
  static readonly FLOOR: string = codeData.CD0007.FLOOR;
  static readonly FLOOR_VALUE: string = codeData.CD0007.FLOOR_VALUE;
}

/**
 * ソート設定
 */
export abstract class CD0008Constants {

  /** 1:昇順 */
  static readonly ASC: string = codeData.CD0008.ASC;
  static readonly ASC_VALUE: string = codeData.CD0008.ASC_VALUE;
  /** 2:降順 */
  static readonly ESC: string = codeData.CD0008.ESC;
  static readonly ESC_VALUE: string = codeData.CD0008.ESC_VALUE;
}

/**
 * 写真一覧表示形式
 */
export abstract class CD0009Constants {

  /** 1:グリッド */
  static readonly GRID_SHOW: string = codeData.CD0009.GRID_SHOW;
  static readonly GRID_SHOW_VALUE: string = codeData.CD0009.GRID_SHOW_VALUE;
  /** 2:リスト */
  static readonly LIST_SHOW: string = codeData.CD0009.LIST_SHOW;
  static readonly LIST_SHOW_VALUE: string = codeData.CD0009.LIST_SHOW_VALUE;
}

/**
 * 写真帳レイアウト配置選択
 */
export abstract class CD0010Constants {

  /** 1:項目名 */
  static readonly PHOTO_LAYOUT_NAME: string = codeData.CD0010.PHOTO_LAYOUT_NAME;
  static readonly PHOTO_LAYOUT_NAME_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_NAME_VALUE;
  /** 2:配置1 */
  static readonly PHOTO_LAYOUT_SELECT1: string = codeData.CD0010.PHOTO_LAYOUT_SELECT1;
  static readonly PHOTO_LAYOUT_SELECT1_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT1_VALUE;
  /** 3:配置2 */
  static readonly PHOTO_LAYOUT_SELECT2: string = codeData.CD0010.PHOTO_LAYOUT_SELECT2;
  static readonly PHOTO_LAYOUT_SELECT2_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT2_VALUE;
  /** 4:配置3 */
  static readonly PHOTO_LAYOUT_SELECT3: string = codeData.CD0010.PHOTO_LAYOUT_SELECT3;
  static readonly PHOTO_LAYOUT_SELECT3_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT3_VALUE;
  /** 5:配置4 */
  static readonly PHOTO_LAYOUT_SELECT4: string = codeData.CD0010.PHOTO_LAYOUT_SELECT4;
  static readonly PHOTO_LAYOUT_SELECT4_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT4_VALUE;
  /** 6:配置5 */
  static readonly PHOTO_LAYOUT_SELECT5: string = codeData.CD0010.PHOTO_LAYOUT_SELECT5;
  static readonly PHOTO_LAYOUT_SELECT5_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT5_VALUE;
  /** 7:配置6 */
  static readonly PHOTO_LAYOUT_SELECT6: string = codeData.CD0010.PHOTO_LAYOUT_SELECT6;
  static readonly PHOTO_LAYOUT_SELECT6_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT6_VALUE;
  /** 8:配置7 */
  static readonly PHOTO_LAYOUT_SELECT7: string = codeData.CD0010.PHOTO_LAYOUT_SELECT7;
  static readonly PHOTO_LAYOUT_SELECT7_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT7_VALUE;
  /** 9:配置8 */
  static readonly PHOTO_LAYOUT_SELECT8: string = codeData.CD0010.PHOTO_LAYOUT_SELECT8;
  static readonly PHOTO_LAYOUT_SELECT8_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_SELECT8_VALUE;
  /** 10:空欄 */
  static readonly PHOTO_LAYOUT_BLANK: string = codeData.CD0010.PHOTO_LAYOUT_BLANK;
  static readonly PHOTO_LAYOUT_BLANK_VALUE: string = codeData.CD0010.PHOTO_LAYOUT_BLANK_VALUE;
}

/**
 * 写真帳全体出力順
 */
export abstract class CD0011Constants {

  /** A:工程優先パターン */
  static readonly LAYOUT_ORDER1: string = codeData.CD0011.LAYOUT_ORDER1;
  static readonly LAYOUT_ORDER1_VALUE: string = codeData.CD0011.LAYOUT_ORDER1_VALUE;
  /** B:フロア優先パターン */
  static readonly LAYOUT_ORDER2: string = codeData.CD0011.LAYOUT_ORDER2;
  static readonly LAYOUT_ORDER2_VALUE: string = codeData.CD0011.LAYOUT_ORDER2_VALUE;
}

/**
 * 写真帳1頁出力数
 */
export abstract class CD0012Constants {

  /** 2:1ページの表示数2 */
  static readonly LAYOUT_NUMBER1: string = codeData.CD0012.LAYOUT_NUMBER1;
  static readonly LAYOUT_NUMBER1_VALUE: string = codeData.CD0012.LAYOUT_NUMBER1_VALUE;
  /** 3:1ページの表示数3 */
  static readonly LAYOUT_NUMBER2: string = codeData.CD0012.LAYOUT_NUMBER2;
  static readonly LAYOUT_NUMBER2_VALUE: string = codeData.CD0012.LAYOUT_NUMBER2_VALUE;
}

/**
 * 工事写真メニューコード.
 */
export abstract class CD0013Constants {

  /** 01:案件表示 */
  static readonly MENU_PROJECT: string = codeData.CD0013.MENU_PROJECT;
  static readonly MENU_PROJECT_VALUE: string = codeData.CD0013.MENU_PROJECT_VALUE;
  /** 02:写真撮影 */
  static readonly MENU_MACHINE_PHOTO: string = codeData.CD0013.MENU_MACHINE_PHOTO;
  static readonly MENU_MACHINE_PHOTO_VALUE: string = codeData.CD0013.MENU_MACHINE_PHOTO_VALUE;
  /** 03:写真一覧 */
  static readonly MENU_PHOTO_LIST: string = codeData.CD0013.MENU_PHOTO_LIST;
  static readonly MENU_PHOTO_LIST_VALUE: string = codeData.CD0013.MENU_PHOTO_LIST_VALUE;
  /** 04:図面表示 */
  static readonly MENU_DRAW_DISPLAY: string = codeData.CD0013.MENU_DRAW_DISPLAY;
  static readonly MENU_DRAW_DISPLAY_VALUE: string = codeData.CD0013.MENU_DRAW_DISPLAY_VALUE;
  /** 05:写真帳出力 */
  static readonly MENU_PHOTOBOOK_INFO_SET: string = codeData.CD0013.MENU_PHOTOBOOK_INFO_SET;
  static readonly MENU_PHOTOBOOK_INFO_SET_VALUE: string = codeData.CD0013.MENU_PHOTOBOOK_INFO_SET_VALUE;
  /** 06:工程登録 */
  static readonly MENU_PROCESS_CREATE: string = codeData.CD0013.MENU_PROCESS_CREATE;
  static readonly MENU_PROCESS_CREATE_VALUE: string = codeData.CD0013.MENU_PROCESS_CREATE_VALUE;
  /** 07:チャットルーム一覧 */
  static readonly MENU_CHATROOMLIST: string = codeData.CD0013.MENU_CHATROOMLIST;
  static readonly MENU_CHATROOMLIST_VALUE: string = codeData.CD0013.MENU_CHATROOMLIST_VALUE;
}

/**
 * CSV登録対象設定
 */
export abstract class CD0014Constants {

  /** 01:ユーザCSV */
  static readonly CSVKIND_USER: string = codeData.CD0014.CSVKIND_USER;
  static readonly CSVKIND_USER_VALUE: string = codeData.CD0014.CSVKIND_USER_VALUE;
  static readonly CSVKIND_1401: string = codeData.CD0014.CSVKIND_1401;
  /** 02:物件CSV */
  static readonly CSVKIND_OBJECT: string = codeData.CD0014.CSVKIND_OBJECT;
  static readonly CSVKIND_OBJECT_VALUE: string = codeData.CD0014.CSVKIND_OBJECT_VALUE;
  static readonly CSVKIND_1402: string = codeData.CD0014.CSVKIND_1402;
  /** 03:物件フロア・部屋・場所CSV */
  static readonly CSVKIND_OBJ_ROOM_PLACE: string = codeData.CD0014.CSVKIND_OBJ_ROOM_PLACE;
  static readonly CSVKIND_OBJ_ROOM_PLACE_VALUE: string = codeData.CD0014.CSVKIND_OBJ_ROOM_PLACE_VALUE;
  static readonly CSVKIND_1403: string = codeData.CD0014.CSVKIND_1403;
  /** 04:案件CSV */
  static readonly CSVKIND_ANKEN: string = codeData.CD0014.CSVKIND_ANKEN;
  static readonly CSVKIND_ANKEN_VALUE: string = codeData.CD0014.CSVKIND_ANKEN_VALUE;
  static readonly CSVKIND_1404: string = codeData.CD0014.CSVKIND_1404;
  /** 05:ユーザ担当管理CSV */
  static readonly CSVKIND_USER_ADMIN: string = codeData.CD0014.CSVKIND_USER_ADMIN;
  static readonly CSVKIND_USER_ADMIN_VALUE: string = codeData.CD0014.CSVKIND_USER_ADMIN_VALUE;
  /** 06:物件情報CSV */
  static readonly CSVKIND_OBJECT_INFO: string = codeData.CD0014.CSVKIND_OBJECT_INFO;
  static readonly CSVKIND_OBJECT_INFO_VALUE: string = codeData.CD0014.CSVKIND_OBJECT_INFO_VALUE;
  /** 07:機器情報CSV */
  static readonly CSVKIND_MACHINE_INFO: string = codeData.CD0014.CSVKIND_MACHINE_INFO;
  static readonly CSVKIND_MACHINE_INFO_VALUE: string = codeData.CD0014.CSVKIND_MACHINE_INFO_VALUE;
  /** 08:工程作業CSV */
  static readonly CSVKIND_PROCESS_WORK: string = codeData.CD0014.CSVKIND_PROCESS_WORK;
  static readonly CSVKIND_PROCESS_WORK_VALUE: string = codeData.CD0014.CSVKIND_PROCESS_WORK_VALUE;
  /** 09:立会人情報CSV */
  static readonly CSVKIND_WITNESS_INFO: string = codeData.CD0014.CSVKIND_WITNESS_INFO;
  static readonly CSVKIND_WITNESS_INFO_VALUE: string = codeData.CD0014.CSVKIND_WITNESS_INFO_VALUE;
  /** 10:黒板表示項目情報CSV */
  static readonly CSVKIND_BLACKBOARD_INFO: string = codeData.CD0014.CSVKIND_BLACKBOARD_INFO;
  static readonly CSVKIND_BLACKBOARD_INFO_VALUE: string = codeData.CD0014.CSVKIND_BLACKBOARD_INFO_VALUE;
  /** 11:案件情報CSV */
  static readonly CSVKIND_ANKEN_INFO: string = codeData.CD0014.CSVKIND_ANKEN_INFO;
  static readonly CSVKIND_ANKEN_INFO_VALUE: string = codeData.CD0014.CSVKIND_ANKEN_INFO_VALUE;
  static readonly CSV_ATTR: string = codeData.CD0014_CSV_ATTR;
  static readonly ANKEN_CSV_TYPE: string = codeData.CD0014_ANKEN_CSV_TYPE;
  static readonly CSV_TYPE: string = codeData.CD0014_CSV_TYPE;
}

/**
 * 権限
 */
export abstract class CD0015Constants {

  /** 1:レベル01 システム管理者 */
  static readonly AUTHORITY01: string = codeData.CD0015.AUTHORITY01;
  static readonly AUTHORITY01_VALUE: string = codeData.CD0015.AUTHORITY01_VALUE;
  /** 2:レベル02 DAT/サービス工事担当課長 */
  static readonly AUTHORITY02: string = codeData.CD0015.AUTHORITY02;
  static readonly AUTHORITY02_VALUE: string = codeData.CD0015.AUTHORITY02_VALUE;
  /** 3:レベル03 DAT/サービス工事担当 */
  static readonly AUTHORITY03: string = codeData.CD0015.AUTHORITY03;
  static readonly AUTHORITY03_VALUE: string = codeData.CD0015.AUTHORITY03_VALUE;
  /** 4:レベル04 協力店 職長(主)・作業者(副) */
  static readonly AUTHORITY04: string = codeData.CD0015.AUTHORITY04;
  static readonly AUTHORITY04_VALUE: string = codeData.CD0015.AUTHORITY04_VALUE;
}

/**
 * 表示フラグ
 */
export abstract class CD0016Constants {

  /** 0:非表示 */
  static readonly NO_DISPLAY_FLAG: string = codeData.CD0016.NO_DISPLAY_FLAG;
  static readonly NO_DISPLAY_FLAG_VALUE: string = codeData.CD0016.NO_DISPLAY_FLAG_VALUE;
  /** 1:表示 */
  static readonly DISPLAY_FLAG: string = codeData.CD0016.DISPLAY_FLAG;
  static readonly DISPLAY_FLAG_VALUE: string = codeData.CD0016.DISPLAY_FLAG_VALUE;
}

/**
 * 削除フラグ
 */
export abstract class CD0017Constants {

  /** 0:未削除 */
  static readonly NO_DELETE_FLAG: string = codeData.CD0017.NO_DELETE_FLAG;
  static readonly NO_DELETE_FLAG_VALUE: string = codeData.CD0017.NO_DELETE_FLAG_VALUE;
  /** 1:削除済み */
  static readonly DELETE_FLAG: string = codeData.CD0017.DELETE_FLAG;
  static readonly DELETE_FLAG_VALUE: string = codeData.CD0017.DELETE_FLAG_VALUE;
}

/**
 * アカウントロックフラグ
 */
export abstract class CD0018Constants {

  /** 0:ロックなし */
  static readonly UNLOCK_FLAG: string = codeData.CD0018.UNLOCK_FLAG;
  static readonly UNLOCK_FLAG_VALUE: string = codeData.CD0018.UNLOCK_FLAG_VALUE;
  /** 1:ロック中 */
  static readonly LOCK_FLAG: string = codeData.CD0018.LOCK_FLAG;
  static readonly LOCK_FLAG_VALUE: string = codeData.CD0018.LOCK_FLAG_VALUE;
}

/**
 * 配置フラグ
 */
export abstract class CD0019Constants {

  /** 0:未設定 */
  static readonly UNSET_FLAG: number = codeData.CD0019.UNSET_FLAG;
  static readonly UNSET_FLAG_VALUE: string = codeData.CD0019.UNSET_FLAG_VALUE;
  /** 1:設定済み */
  static readonly SET_FLAG: number = codeData.CD0019.SET_FLAG;
  static readonly SET_FLAG_VALUE: string = codeData.CD0019.SET_FLAG_VALUE;
}

/**
 * 写真帳出力フラグ
 */
export abstract class CD0020Constants {

  /** 0:出力対象外 */
  static readonly OUT_OF_OUTPUT_OBJECT_FLAG: number = codeData.CD0020.OUT_OF_OUTPUT_OBJECT_FLAG;
  static readonly OUT_OF_OUTPUT_OBJECT_FLAG_VALUE: string = codeData.CD0020.OUT_OF_OUTPUT_OBJECT_FLAG_VALUE;
  /** 1:出力対象 */
  static readonly OUTPUT_OBJECT_FLAG: number = codeData.CD0020.OUTPUT_OBJECT_FLAG;
  static readonly OUTPUT_OBJECT_FLAG_VALUE: string = codeData.CD0020.OUTPUT_OBJECT_FLAG_VALUE;
}

/**
 * 機種区分
 */
export abstract class CD0021Constants {

  /** 1:室外機 */
  static readonly OUTDOOR_MACHINE_FLAG: string = codeData.CD0021.OUTDOOR_MACHINE_FLAG;
  static readonly OUTDOOR_MACHINE_FLAG_VALUE: string = codeData.CD0021.OUTDOOR_MACHINE_FLAG_VALUE;
  /** 2:室内機 */
  static readonly INDOOR_MACHINE_FLAG: string = codeData.CD0021.INDOOR_MACHINE_FLAG;
  static readonly INDOOR_MACHINE_FLAG_VALUE: string = codeData.CD0021.INDOOR_MACHINE_FLAG_VALUE;
}

/**
 * 事前設定撮影必須項目選択
 */
export abstract class CD0022Constants {

  /** 1:室外機のみ必要 */
  static readonly ONLY_OUTDOOR_MACHINE: string = codeData.CD0022.ONLY_OUTDOOR_MACHINE;
  static readonly ONLY_OUTDOOR_MACHINE_VALUE: string = codeData.CD0022.ONLY_OUTDOOR_MACHINE_VALUE;
  /** 2:室内機のみ必要 */
  static readonly ONLY_INDOOR_MACHINE: string = codeData.CD0022.ONLY_INDOOR_MACHINE;
  static readonly ONLY_INDOOR_MACHINE_VALUE: string = codeData.CD0022.ONLY_INDOOR_MACHINE_VALUE;
  /** 3:室外機、室内機ともに必要 */
  static readonly ALL_NEED: string = codeData.CD0022.ALL_NEED;
  static readonly ALL_NEED_VALUE: string = codeData.CD0022.ALL_NEED_VALUE;
  /** 4:必要なし */
  static readonly NO_NEED: string = codeData.CD0022.NO_NEED;
  static readonly NO_NEED_VALUE: string = codeData.CD0022.NO_NEED_VALUE;
}

/**
 * アクセス許可フラグ
 */
export abstract class CD0023Constants {

  /** 0:無効 */
  static readonly INVALID_FLAG: string = codeData.CD0023.INVALID_FLAG;
  static readonly INVALID_FLAG_VALUE: string = codeData.CD0023.INVALID_FLAG_VALUE;
  /** 1:有効 */
  static readonly VALID_FLAG: string = codeData.CD0023.VALID_FLAG;
  static readonly VALID_FLAG_VALUE: string = codeData.CD0023.VALID_FLAG_VALUE;
}

/**
 * 状況設定
 */
export abstract class CD0024Constants {

  /** 1:施工前 */
  static readonly BEFORE_CONSTRUCTION: string = codeData.CD0024.BEFORE_CONSTRUCTION;
  static readonly BEFORE_CONSTRUCTION_VALUE: string = codeData.CD0024.BEFORE_CONSTRUCTION_VALUE;
  /** 2:施工中 */
  static readonly IN_CONSTRUCTION: string = codeData.CD0024.IN_CONSTRUCTION;
  static readonly IN_CONSTRUCTION_VALUE: string = codeData.CD0024.IN_CONSTRUCTION_VALUE;
  /** 3:施工後 */
  static readonly AFTER_CONSTRUCTION: string = codeData.CD0024.AFTER_CONSTRUCTION;
  static readonly AFTER_CONSTRUCTION_VALUE: string = codeData.CD0024.AFTER_CONSTRUCTION_VALUE;
  /** 4:運用中 */
  static readonly IN_WORKING: string = codeData.CD0024.IN_WORKING;
  static readonly IN_WORKING_VALUE: string = codeData.CD0024.IN_WORKING_VALUE;
  /** 6:占有部 */
  static readonly POSSESSION_PART: string = codeData.CD0024.POSSESSION_PART;
  static readonly POSSESSION_PART_VALUE: string = codeData.CD0024.POSSESSION_PART_VALUE;
  /** 7:共有部 */
  static readonly SHARE_PART: string = codeData.CD0024.SHARE_PART;
  static readonly SHARE_PART_VALUE: string = codeData.CD0024.SHARE_PART_VALUE;

}

/**
 * 登録区分
 */
export abstract class CD0025Constants {

  /** 1:登録 */
  static readonly LOGIN: string = codeData.CD0025.LOGIN;
  static readonly LOGIN_VALUE: string = codeData.CD0025.LOGIN_VALUE;
  /** 2:更新 */
  static readonly UPDATE: string = codeData.CD0025.UPDATE;
  static readonly UPDATE_VALUE: string = codeData.CD0025.UPDATE_VALUE;
  /** 3:削除 */
  static readonly DELETE: string = codeData.CD0025.DELETE;
  static readonly DELETE_VALUE: string = codeData.CD0025.DELETE_VALUE;
  /** 4:変更なし */
  static readonly UNCHANGE: string = codeData.CD0025.UNCHANGE;
  static readonly UNCHANGE_VALUE: string = codeData.CD0025.UNCHANGE_VALUE;
}

/**
 * 処理結果コード
 */
export abstract class CD0026Constants {

  /** 1:正常終了 */
  static readonly NORMAL_END: string = codeData.CD0026.NORMAL_END;
  static readonly NORMAL_END_VALUE: string = codeData.CD0026.NORMAL_END_VALUE;
  /** 2:エラー終了 */
  static readonly ERROR_END: string = codeData.CD0026.ERROR_END;
  static readonly ERROR_END_VALUE: string = codeData.CD0026.ERROR_END_VALUE;
  /** 3:異常終了 */
  static readonly ABNORMAL_END: string = codeData.CD0026.ABNORMAL_END;
  static readonly ABNORMAL_END_VALUE: string = codeData.CD0026.ABNORMAL_END_VALUE;
}


export abstract class CDFILMEDConstants {

  /** 1:撮影済み */
  static readonly FILMED: string = codeData.CDFILMED.FILMED;
  static readonly FILMED_VALUE: string = codeData.CDFILMED.FILMED_VALUE;
  /** 2:未撮影 */
  static readonly UNFILMED: string = codeData.CDFILMED.UNFILMED;
  static readonly UNFILMED_VALUE: string = codeData.CDFILMED.UNFILMED_VALUE;
}
/** 写真一覧、選択モードの複数写真選択するパターン */
export abstract class MULITYPE {

  /** 1:手動同期 */
  static readonly HAND_UPLOAD: string = '1';
  static readonly HAND_UPLOAD_VALUE: string = '手動同期';
  /** 2:削除 */
  static readonly DELETE: string = '2';
  static readonly DELETE_VALUE: string = '削除';

  /** 3:写真帳選択 */
  static readonly PHOTO_BOOK: string = '3';
  static readonly PHOTO_BOOK_VALUE: string = '写真帳選択';
}

// ソート順
export abstract class SORTBY {
  // 1:'昇順'
  static readonly ASC: number = 1;
  // 2:'降順'
  static readonly ESC: number = 2;
}

// FLAG
export abstract class FLAG {
  // 1:true
  static readonly FLAG_TRUE: number = 1;
  // 0:false
  static readonly FLAG_FALSE: number = 0;
}

export abstract class TEXT {
  static readonly NO: string = 'いいえ';
  static readonly YES: string = 'はい';
  static readonly OK: string = 'はい';
}
