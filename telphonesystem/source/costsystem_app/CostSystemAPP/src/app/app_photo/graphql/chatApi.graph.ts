// チャットロームリストを取得する。
export const LSIT_CONVERSTAIONS = `
query getUserProjectConversations($projectId: String!, $userId: String!, $after: String, $first: Int) {
  getUserProjectConversations(projectId: $projectId, userId: $userId, after: $after, first: $first) {
    userProjectConversations{
      conversationId
      userId
      isInvalid
      associated {
        __typename
        userId
        isInvalid
        usersInfo{
          companyName
          userId
          userName
          createdAt
          updatedAt
          isInvalid
        }
      }
      conversation{
        conversationId
        conversationName
        projectId
        createdAt
        updatedAt
        isInvalid
        creatorId
      }
    }
    nextToken
  }

}
`;

// 参加可能のusers
export const LIST_USERS = `
query getInvitableUsers($projectId: String!) {
  invitableUsers(projectId: $projectId) {
    userId
    projectId
    usersInfo{
      companyName
	    userId
	    userName
    	createdAt
	    updatedAt
      isInvalid
      authorId
    }
    isInvalid
    createdAt
    updatedAt
  }
}`;

// 会話中のusers
export const LIST_ASSOCIATED_USERS = `
query conversationAssoicated($conversationId: ID!) {
  conversationAssoicated(conversationId: $conversationId) {
    __typename
    userId
    isInvalid
    usersInfo{
      companyName
      userId
      userName
      createdAt
      updatedAt
      isInvalid
    }
  }
}`;


// チャットを作成
export const CREATE_CONVERSATION =
  `mutation createConversationDev($id: ID!, $name: String!, $createdAt: String!, $projectId: String!, $creatorId: String) {
  createConversationDev(id: $id, name: $name, createdAt: $createdAt, projectId: $projectId, creatorId: $creatorId) {
    __typename
    conversationId
    conversationName
    createdAt
    updatedAt
    creatorId
  }
}`;

// チャット会話時間を更新
export const UPDATE_CONVERSATION =
  `mutation updateConversationUpdatedAt($conversationId: ID!) {
  updateConversationUpdatedAt(conversationId: $conversationId) {
    __typename
    conversationId
    conversationName
    createdAt
    updatedAt
    creatorId
  }
}`;

// 会話、案件、ユーザの関連を作る
export const CREATE_USER_PROJECT_CONVERSATION = `
mutation createUserProjectConversations($userProjectId: ID!, $conversationId: ID!, $userId: String, $projectId: String) {
  createUserProjectConversations(userProjectId: $userProjectId, conversationId: $conversationId, userId: $userId, projectId: $projectId) {
    userProjectId
    conversationId
    userId
    isInvalid
  }
}`;


// 会話、案件、ユーザの関連を削除
export const DELETE_USER_PROJECT_CONVERSATION = `
mutation deleteUserProjectConversations($userProjectId: ID!, $conversationId: ID!, $userId: String) {
  deleteUserProjectConversations(userProjectId: $userProjectId, conversationId: $conversationId, userId: $userId){
    userProjectId
    conversationId
    userId
    isInvalid
  }
}`;


// 会話メッセージリストを取得
export const LIST_MESSAGES = `
query getConversationMessages($conversationId: ID!, $after: String, $first: Int) {
  allMessageConnection(conversationId: $conversationId, after: $after, first: $first) {
    __typename
    messages {
      __typename
      id
      conversationId
      content
      identityId
      s3FilePath
      createdAt
      sender
      isSent
      isInvalid
      readUsers
      s3FullPath
    }
  }
}`;

// メッセージ送信
export const CREATE_MESSAGE = `
mutation createMessage($id: ID!, $content: String, $conversationId: ID!, $sender: String!,
                       $createdAt: String!, $identityId: String, $s3FilePath: String) {
  createMessage(id: $id, content: $content, conversationId: $conversationId, createdAt: $createdAt,
                sender: $sender,identityId: $identityId, s3FilePath: $s3FilePath){
    __typename
    conversationId
    createdAt
    id
    sender
    content
    identityId
    s3FilePath
    isSent
    isInvalid
    readUsers
    s3FullPath
  }
}`;

// メッセージ送信を監視
export const SUBSCRIBE_NEW_MESSAGE = `
subscription subscribeToNewMessage($conversationId: ID!) {
  subscribeToNewMessage(conversationId: $conversationId) {
    __typename
    conversationId
    createdAt
    id
    sender
    content
    identityId
    s3FilePath
    isSent
    isInvalid
    readUsers
    s3FullPath
  }
}`;

// 既読監視
export const SUBSCRIBE_MARK_AS_READ = `
subscription subscribeToMarkAsRead($conversationId: ID!) {
  subscribeToMarkAsRead(conversationId: $conversationId) {
    __typename
    conversationId
    readMessageInfo{
      __typename
      messageId
      userId
      conversationId
      createdAt
    }
  }
}`;

// メッセージを既読にする
export const MUTATION_MAKE_AS_READ = `
mutation createReadMessage($messageIds: [String]!, $conversationId: ID!, $userId: String!) {
  createReadMessage(messageIds: $messageIds, conversationId: $conversationId, userId: $userId){
    __typename
    conversationId
    readMessageInfo{
      __typename
      messageId
      userId
      conversationId
      createdAt
    }
  }
}`;

// デバイスとユーザの関連を作成する
export const MUTATION_CREATE_USER_DEVICE = `
mutation createUserDevice($deviceId: String!,$type: String!,$userId: String!){
  createUserDevice(deviceId:$deviceId,type: $type,userId: $userId){
    userId
    deviceId
    type
  }
}
`;

// デバイスとユーザの関連を削除する
export const MUTATION_DELETE_USER_DEVICE = `
mutation deleteUserDevice($deviceId: String!,$type: String!,$userId: String!){
  deleteUserDevice(deviceId:$deviceId,type: $type,userId: $userId)
}
`;

// プッシュ通知をlambdaに委託する
export const MUTATION_PUSH_NOTIFICATION = `
mutation pushNotification($sender: String!, $conversationId: ID!, $messageId: ID!) {
    pushNotification(sender: $sender, conversationId: $conversationId, messageId: $messageId)
}
`;
