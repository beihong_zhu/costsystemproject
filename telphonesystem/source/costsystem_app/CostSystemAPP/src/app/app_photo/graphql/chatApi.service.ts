import API, { graphqlOperation, GraphQLResult } from '@aws-amplify/api';
import {
  LSIT_CONVERSTAIONS,
  LIST_USERS,
  LIST_MESSAGES,
  LIST_ASSOCIATED_USERS,
  CREATE_MESSAGE,
  CREATE_CONVERSATION,
  CREATE_USER_PROJECT_CONVERSATION,
  DELETE_USER_PROJECT_CONVERSATION,
  SUBSCRIBE_NEW_MESSAGE,
  MUTATION_MAKE_AS_READ,
  SUBSCRIBE_MARK_AS_READ,
  MUTATION_CREATE_USER_DEVICE,
  MUTATION_DELETE_USER_DEVICE,
  MUTATION_PUSH_NOTIFICATION,
  UPDATE_CONVERSATION
} from '@graphql/chatApi.graph';
import {
  ProjectConversations,
  GetInvitableUsers,
  Assoicated,
  CreateConversationDev,
  GetConversationMessages,
  CreateMessage,
  Message
} from '@pages/chatroom/dto/chatroomdto';
import { v4 as uuid } from 'uuid';

export const Config = {
  CONVERSATION_FIRST: 100,  // チャットルーム初期表示数
  MESSAGE_FIRST: 100,      // チャットメッセージ初期表示数
  CRLF: '&lt;br&gt;',                 // 改行コード置換
  INIT_SET_SCROLL_POSITION_TIME: 2000,     // スクロール位置を設定する待ち時間(初期表示時)
  SENDMESSAGE_SET_SCROLL_POSITION_TIME: 500,     // スクロール位置を設定する待ち時間（メッセージ投稿時）
  INVITABLE_EMP_USERS_FIRST: null, // ルーム作成・招待時の社員ユーザ取得部署ページングサイズ
  DOUBLE_SUBMIT_TIME: 500,
  TOAST_DISPLAY_TIME: 2000,
  OFFLE_CHANGE_WATETIME: 3000,
  OVER_HUNDRED: '99+',
  DELIVERY_ROUTE_INFO_FIRST: null,
};

export const escapeString = (text) => {
  if (!text) {
    return text;
  }
  return text.replace(/(\n)/g, Config.CRLF)
    .replace(/(\")/g, '”')
    .replace(/(\t)/g, '\\t')
    .replace(/(\f)/g, '\\f')
    .replace(/(\r)/g, '\\r')
    .replace(/(\\)/g, '\\\\');
};

export class ChatAPI {

  public static convSubscription = null;
  public static convMarkAsReadSubscription = null;

  public static apiService = {

    // 会話を検索
    queryListConversation: async (successFunc, failFunc, userId1, projectId1) => {

      try {
        const after1 = null, first1 = 100;
        const { data } = await API.graphql(graphqlOperation(LSIT_CONVERSTAIONS,
          { projectId: projectId1, userId: userId1, after: after1, first: first1 })) as GraphQLResult<ProjectConversations>;
        const conversations = data.getUserProjectConversations.userProjectConversations;
        successFunc(conversations);

      } catch (err) {
        failFunc(err);
        throw err;

      }

    },

    // 参加可能のusers
    queryConversationUser: async (successFunc, failFunc, projectIdPara) => {

      try {
        const { data } = await API.graphql(graphqlOperation(LIST_USERS,
          { projectId: projectIdPara })) as GraphQLResult<GetInvitableUsers>;
        console.log('res', data);
        const userList = data.invitableUsers;
        successFunc(userList);

      } catch (err) {
        failFunc(err);
        throw err;
      }

    },

    // 会話中のusers
    queryAssocaitedUser: async (successFunc, failFunc, convId) => {

      try {
        const { data } = await API.graphql(graphqlOperation(LIST_ASSOCIATED_USERS,
          { conversationId: convId })) as GraphQLResult<Assoicated>;
        console.log('res', data);
        const userList = data.conversationAssoicated;
        successFunc(userList);

      } catch (err) {
        failFunc(err);
        throw err;
      }

    },

    // チャットを作成
    createConversation: async (successFunc, failFunc, userList, registeredRoomName, projectId1, userId1) => {

      try {
        // チャットルーム作成
        let response: GraphQLResult<CreateConversationDev> = null;
        const converstaionId1 = uuid();
        let errorType: string = '';
        let errorTypeInfo: string = '';
        try {
          response = await API.graphql(graphqlOperation(CREATE_CONVERSATION, {
            id: converstaionId1,
            name: escapeString(registeredRoomName),
            createdAt: new Date().toISOString(),
            projectId: projectId1,
            creatorId: userId1
          })) as GraphQLResult<CreateConversationDev>;
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00155');
              }
            } else {
              failFunc('E00155');
            }
          }
          return;
        }

        // 話、案件、ユーザの関連を作る
        try {
          for (const user of userList) {
            await API.graphql(graphqlOperation(CREATE_USER_PROJECT_CONVERSATION, {
              userProjectId: user.userId + projectId1,
              conversationId: converstaionId1,
              userId: user.userId,
              projectId: projectId1
            }));
          }
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00156');
              }
            } else {
              failFunc('E00156');
            }
          }
          return;
        }
        successFunc(response);

      } catch (err) {
        failFunc(err);
      }
    },

    // チャットルーム編集画面は、メンバーを削除と追加処理
    createAndDeleteUserProjectConversation: async (successFunc, failFunc, converstaionId1, adduserList, deleteuserList, projectId1) => {

      try {
        // 会話、案件、ユーザの関連を作る
        let errorType: string = '';
        let errorTypeInfo: string = '';
        try {
          // 追加メンバー
          for (const user of adduserList) {
            await API.graphql(graphqlOperation(CREATE_USER_PROJECT_CONVERSATION, {
              userProjectId: user.userId + projectId1,
              conversationId: converstaionId1,
              userId: user.userId,
              projectId: projectId1
            }));
          }
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00122');
              }
            } else {
              failFunc('E00122');
            }
          }
          return;
        }

        try {
          // 会話、案件、ユーザの関連を削除
          // 削除メンバー
          for (const user of deleteuserList) {
            await API.graphql(graphqlOperation(DELETE_USER_PROJECT_CONVERSATION, {
              userProjectId: user.userId + projectId1,
              conversationId: converstaionId1,
              userId: user.userId
            }));
          }
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00123');
              }
            } else {
              failFunc('E00123');
            }
          }
          return;
        }
        successFunc();
      } catch (err) {
        failFunc(err);
      }
    },

    // 会話、案件、ユーザの関連を作る
    createUserProjectConversation: async (successFunc, failFunc, converstaionId1, userList, projectId1) => {

      try {

        for (const user of userList) {
          await API.graphql(graphqlOperation(CREATE_USER_PROJECT_CONVERSATION, {
            userProjectId: user.userId + projectId1,
            conversationId: converstaionId1,
            userId: user.userId,
            projectId: projectId1
          }));

        }
        successFunc();

      } catch (err) {
        failFunc(err);

      }

    },

    // 会話、案件、ユーザの関連を削除
    deleteUserProjectConversation: async (successFunc, failFunc, converstaionId1, userList, projectId1) => {

      try {

        for (const user of userList) {
          await API.graphql(graphqlOperation(DELETE_USER_PROJECT_CONVERSATION, {
            userProjectId: user.userId + projectId1,
            conversationId: converstaionId1,
            userId: user.userId
          }));

        }
        successFunc();

      } catch (err) {
        failFunc(err);

      }

    },

    // 会話メッセージリストを取得
    queryConversationChatMessage: async (successFunc, failFunc, convId) => {

      try {
        const { data } = await API.graphql(graphqlOperation(LIST_MESSAGES, {
          conversationId: convId,
          first: Config.MESSAGE_FIRST
        })) as GraphQLResult<GetConversationMessages>;
        successFunc(data.allMessageConnection.messages);

      } catch (err) {
        failFunc(err);
        throw err;
      }

    },

    // 写真メッセージ送信
    createPhotosMessage: async (successFunc, failFunc, currentConvId, userId, s3FilePath1) => {

      try {

        const date = new Date().toISOString();
        const id1 = `${date}_${uuid()}`;
        const message = {
          conversationId: currentConvId,
          content: 'NONE',
          createdAt: date,
          sender: userId,
          identityId: 'NONE',
          s3FilePath: s3FilePath1,
          isSent: false,
          id: id1
        };
        let errorType: string = '';
        let errorTypeInfo: string = '';

        // メッセージ作成
        let response: GraphQLResult<CreateMessage> = null;
        try {
          response = await API.graphql(graphqlOperation(CREATE_MESSAGE, message)) as GraphQLResult<CreateMessage>;
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00117');
              }
            } else {
              failFunc('E00117');
            }
          }
          return;
        }

        // チャット会話時間を更新
        let resulta = null;
        try {
          resulta = await API.graphql(graphqlOperation(UPDATE_CONVERSATION, { conversationId: currentConvId }));
          console.log('update converstaion result', resulta);
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00118');
              }
            } else {
              failFunc('E00118');
            }
          }
          return;
        }

        // プッシュ通知をlambdaに委託する
        let result = null;
        try {
          result = await API.graphql(graphqlOperation(MUTATION_PUSH_NOTIFICATION,
            { sender: userId, conversationId: currentConvId, messageId: id1 }));
          console.log('image send pushNotification result', result);
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00119');
              }
            } else {
              failFunc('E00119');
            }
          }
          return;
        }

        successFunc(response.data.createMessage);

      } catch (err) {
        failFunc(err);

      }

    },

    // メッセージ送信
    createMessage: async (successFunc, failFunc, currentConvId, userId, inputText) => {

      try {

        const date = new Date().toISOString();
        const id1 = `${date}_${uuid()}`;
        const messageContent = escapeString(inputText);
        const message = {
          conversationId: currentConvId,
          content: messageContent,
          createdAt: date,
          sender: userId,
          identityId: 'NONE',
          s3FilePath: 'NONE',
          isSent: false,
          id: id1
        };
        let errorType: string = '';
        let errorTypeInfo: string = '';

        // メッセージ作成
        let response: GraphQLResult<CreateMessage> = null;
        try {
          response = await API.graphql(graphqlOperation(CREATE_MESSAGE, message)) as GraphQLResult<CreateMessage>;
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00117');
              }
            } else {
              failFunc('E00117');
            }
          }
          return;
        }

        // チャット会話時間を更新
        let resulta = null;
        try {
          resulta = await API.graphql(graphqlOperation(UPDATE_CONVERSATION, { conversationId: currentConvId }));
          console.log('update converstaion result', resulta);
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00118');
              }
            } else {
              failFunc('E00118');
            }
          }
          return;
        }

        // プッシュ通知をlambdaに委託する
        let result = null;
        try {
          result = await API.graphql(graphqlOperation(
            MUTATION_PUSH_NOTIFICATION,
            { sender: userId, conversationId: currentConvId, messageId: id1 }));
          console.log('send text pushNotification result', result);
        } catch (err) {
          console.error(err);
          if (err.errors) {
            if (err.errors['0'].errorType) {
              errorTypeInfo = err.errors['0'].errorType;
            }
          }
          if (errorTypeInfo) {
            errorType = errorTypeInfo.split(':')[0];
          }
          if (errorType === 'DynamoDB') {
            failFunc('EIF018');
          } else {
            if (errorTypeInfo) {
              if (errorTypeInfo === 'MappingTemplate') {
                failFunc('EIF019');
              } else {
                failFunc('E00119');
              }
            } else {
              failFunc('E00119');
            }
          }
          return;
        }

        successFunc(response.data.createMessage);

      } catch (err) {
        failFunc(err);

      }

    },

    // メッセージ送信を監視
    newMessageSubscibe: async (convId, callBackFunc) => {

      // メッセージ送信監視を解除
      if (ChatAPI.convSubscription) {
        ChatAPI.convSubscription.unsubscribe();
      }

      // メッセージ送信監視を追加
      const subscribeNewMessage: any = API.graphql(
        graphqlOperation(SUBSCRIBE_NEW_MESSAGE, { conversationId: convId })
      );

      ChatAPI.convSubscription = subscribeNewMessage.subscribe({
        next: async (eventData) => {
          const newMessage = eventData.value.data.subscribeToNewMessage;
          callBackFunc(newMessage);
        },
        error: () => {
          // sleepからerrorがある
        }
      });

    },

    // 既読監視
    markAsRead: async (messages: Message[], userId1, convId) => {

      try {

        const chatMessageIds = [];
        if (messages && messages.length > 0) {
          const getAllMessages: Array<Message> = Object.assign([], messages);
          getAllMessages.map((message) => {
            if (message.readUsers.indexOf(userId1) === -1 && message.sender !== userId1 && !message.isInvalid) {
              chatMessageIds.push(message.id);
            }
          });
          console.log('getAllMessages', getAllMessages, chatMessageIds);
          if (chatMessageIds.length > 0) {
            for (let i = 0; i < chatMessageIds.length; i = i + 10) {
              const recordsInfo = {
                messageIds: chatMessageIds.slice(0 + i, 10 + i > chatMessageIds.length ? chatMessageIds.length : 10 + i),
                conversationId: convId,
                userId: userId1
              };
              console.log('recordsInfo', recordsInfo);
              API.graphql(graphqlOperation(MUTATION_MAKE_AS_READ, recordsInfo));
            }
          }
        }

      } catch (err) {
        throw err;
      }

    },

    // 既読監視
    markAsReadSubscribe: async (convId, callBackFunc) => {

      // 既読監視を解除
      if (ChatAPI.convMarkAsReadSubscription) {
        ChatAPI.convMarkAsReadSubscription.unsubscribe();
      }

      // 既読監視を追加
      const subscribeReadMessage: any = API.graphql(
        graphqlOperation(SUBSCRIBE_MARK_AS_READ, { conversationId: convId })
      );

      ChatAPI.convMarkAsReadSubscription = subscribeReadMessage.subscribe({
        next: async (eventData) => {
          console.log('mask read message', eventData);
          const markAsReadMessages = eventData.value.data.subscribeToMarkAsRead.readMessageInfo;
          callBackFunc(markAsReadMessages);

        },
        error: () => {
          // sleepからerrorがある
        }
      });
    },

    // 既読監視を解除
    unsubscribeReadMessage: async () => {

      // 既読監視を解除
      if (ChatAPI.convMarkAsReadSubscription) {
        ChatAPI.convMarkAsReadSubscription.unsubscribe();
      }
    },

    // 新メッセージ読監視を解除
    unsubscribeNewMessage: async () => {

      // 新メッセージ読監視を解除
      if (ChatAPI.convSubscription) {
        ChatAPI.convSubscription.unsubscribe();
      }
    },

    // デバイスとユーザの関連を作成する
    createUserDevice: async (deviceToken, platform, userId1) => {

      try {
        const result = await API.graphql(graphqlOperation(MUTATION_CREATE_USER_DEVICE,
          { deviceId: deviceToken, type: platform, userId: userId1 }));
        console.log('createUserDevice result', result);

      } catch (err) {
        console.log('createUserDevice err', err);
        throw err;
      }

    },

    // デバイスとユーザの関連を削除する
    deleteUserDevice: async (deviceToken, platform, userId1) => {
      try {
        const result = await API.graphql(graphqlOperation(MUTATION_DELETE_USER_DEVICE,
          { deviceId: deviceToken, type: platform, userId: userId1 }));
        console.log('deleteUserDevice result', result);

      } catch (err) {
        console.log('deleteUserDevice err', err);
        throw err;
      }

    },

    pushNotification: async (sender1, messageId1, conversationId1) => {

      try {
        const result = await API.graphql(graphqlOperation(MUTATION_PUSH_NOTIFICATION,
          { sender: sender1, conversationId: conversationId1, messageId: messageId1 }));
        console.log('pushNotification result', result);

      } catch (err) {
        console.log('pushNotification err', err);
        throw err;
      }

    }
  };
}
