import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Auth } from 'aws-amplify';
import { CommonStoreService, CommonStore } from '@store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private commonStore: CommonStore;

  constructor(
    private router: Router,
    private commonStoreService: CommonStoreService,
  ) {
    this.commonStore = this.commonStoreService.commonStore;
  }

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try {
      // 証済みユーザを取得できる
      const user = await Auth.currentAuthenticatedUser();
      // 権限利用機能チェック
      if (this.commonStore.availableFunctionIds.includes(next.data.pageId)) {
        return true;
      } else {
        this.toLogin();
        return false;
      }
    } catch (err) {
      // 認証済みユーザを取得できない
      this.toLogin();
      return false;
    }
  }

  private toLogin() {
    const isPresetting = localStorage.getItem('com.dkj.photo.presetting');
    if (isPresetting && isPresetting === '1') {
      this.router.navigate(['/presetting-login']);
    } else {
      this.router.navigate(['/login']);
    }
  }
}
