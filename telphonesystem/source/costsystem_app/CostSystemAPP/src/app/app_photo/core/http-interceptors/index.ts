import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { AuthInterceptor } from './auth-interceptor';
// import { CachingInterceptor } from './caching-interceptor';
import { LoggingInterceptor } from './logging-interceptor';
// import { UploadInterceptor } from './upload-interceptor';

export const httpInterceptors = [
  // { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: LoggingInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: UploadInterceptor, multi: true },
  // { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true },
];
