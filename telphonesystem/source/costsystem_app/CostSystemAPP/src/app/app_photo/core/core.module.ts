import { NgModule, ErrorHandler, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
// import { ServiceWorkerModule } from '@angular/service-worker';
import { AppErrorHandler } from './error-handler/app-error-handler';
import { httpInterceptors } from './http-interceptors/index';
// import { environment } from '@env';

@NgModule({
  declarations: [],
  imports: [
    // angular
    CommonModule,
    HttpClientModule,

    // service worker
    // ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    { provide: ErrorHandler, useClass: AppErrorHandler },
    httpInterceptors
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule.');
    }
  }
}
