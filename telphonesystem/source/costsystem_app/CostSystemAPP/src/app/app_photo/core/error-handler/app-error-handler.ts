import { Injectable, ErrorHandler } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '@env';

@Injectable()
export class AppErrorHandler extends ErrorHandler {

  constructor() {
    super();
  }

  handleError(error: Error | HttpErrorResponse) {
    let displayMessage = 'An error occurred.';
    if (!environment.production) {
      displayMessage += ' See console for details.';
    }
    console.log(error);
    console.log(displayMessage);
    super.handleError(error);
    if (!(error instanceof HttpErrorResponse) &&
      this.getErrorMessage(error).indexOf('Timeout disconnect') === -1 &&
      this.getErrorMessage(error).indexOf('Network Error') === -1) {
      const myEvent = new CustomEvent('event_router', {
        detail: { message: error.message},
      });
      if (window.dispatchEvent) {
        window.dispatchEvent(myEvent);
      }
    }
  }

  getErrorMessage(error): string {
    if (error.message) {
      return error.message;
    }
    return '';
  }
}
