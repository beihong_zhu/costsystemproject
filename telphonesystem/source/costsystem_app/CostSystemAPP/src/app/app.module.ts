import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { File } from '@ionic-native/file/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Network } from '@ionic-native/network/ngx';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, routerReducer, RouterStateSerializer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { Auth, PubSub, API } from 'aws-amplify';
import { awsConfig } from './awsconfig/aws_config';
import { AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular';
Auth.configure(awsConfig);
PubSub.configure(awsConfig);
API.configure(awsConfig);

import { environment } from '@env';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './app_common/service/auth.service';
import { CoreModule } from './app_photo/core/core.module';
import { SharedModule } from './app_photo/shared/shared.module';
import { RoutingService } from './app_photo/service/app-route.service';
import { PhotoListPopoverComponent } from '@pages/photo-manage/photo-list-popover/photo-list-popover.component';
import { TextPopoverComponent } from '@pages/building-project/popover/Text-popover.component';

import { TextSinglePopoverComponent } from '@pages/camera/text-popover/text-popover.component';

import { OfflineDownloadPhotoService } from '@service/offline/offline-download-photo.service';
import { OfflineDownloadPdfService } from '@service/offline/offline-download-pdf.service';
import {
  IF0101Service,
  IF0102Service,
  IF0103Service,
  IF0106Service,
  IF0108Service,
  IF0109Service,
  IF0110Service,
  API0202Service,
  API0301Service,
  API0302Service,
  API0303Service,
  API0304Service,
  API0306Service,
  API0501Service,
  API0504Service,
  API0505Service,
  API0601Service,
  API0602Service,
  API0603Service,
  API0604Service,
  API0605Service,
  API0701Service,
  API0801Service,
  API0802Service,
  API0803Service,
  API0805Service,
  API0806Service,
  API0808Service,
  API0809Service,
  API0810Service,
  API0901Service,
  API0902Service,
  API0903Service,
  API1107Service,
  API1108Service,
  API1109Service,
  API1110Service,
  API1111Service,
  API1112Service,
  API1201Service,
  API1401Service,
  API1402Service,
  API1403Service,
  API1404Service,
  API1405Service,
  API1406Service,
  API1407Service,
  API1408Service,
  API0102Service,
  API0112Service,
  API3002Service,
  API3004Service,
  API3006Service,
  API3007Service,
  API3008Service,
  API3009Service,
  API0104Service,
  API3010Service,
  API3011Service,
  API3012Service,
  API3013Service,
  API3014Service,
  API3003Service,
  

  StartExecutionService,
  DescribeExecutionService
} from '@service/api';
import { OfflineAPIService } from '@service/offline/offline-api.service';
import { RunExecutionService } from '@service/api/run-execution.service';
import { DbManageService } from '@service/db/db-manage';
import { DbPhotoService } from '@service/db/db-photoservice';
import { DbDrawService } from '@service/db/db-drawservice';
import { DbUnitsService } from '@service/db/db-unitsservice';
import {
  AppLoadingReducer,
  AppLoadingStoreService,
  AppReducer,
  AppStateStoreService,
  CameraReducer,
  ChatRoomReducer,
  CommonReducer,
  CommonStoreService,
  DrawDisplayReducer,
  DrawDisplayStoreService,
  HeadReducer,
  HeadStateService,
  MenuReducer,
  MenuStateService,
  OfflineStoreReducer,
  OfflineStoreService,
  PhotobookInfoReducer,
  PhotobookInfoSetStoreService,
  PhotobookViewReducer,
  PhotobookViewSetStoreService,
  PhotoDetailStoreReducer,
  PhotomanagestoreReducer,
  PhotomanagestoreService,
  PreSettingReducer,
  PreSettingStoreService,
  DeviceTokenReducer,
  DeviceTokenStoreService,
  CommonStore,
  LoggerReducer,
  LoggerStoreService,
  PhotoDwonloadSuccessStoreReducer,
  AutoUploadPhotoStoreReducer,
  PhotoDownloadSuccessService,
  AutoUploadPhotoService,
  OfflineUnitsReducer,
  OfflineUnitsService,
  CameraStoreService,
  DrawOfflineStoreService,
  PhotoDetailStoreService,
  ChatRoomStoreService,
  ProcessStoreService,
  RegularInfoStoreService
} from '@store';
import { CustomSerializer } from './app_photo/store/router/router.dto';
import { Push } from '@ionic-native/push/ngx';
import { IonicGestureConfig } from './ionic-gesture-config';
import { CookieService } from 'ngx-cookie-service';
import { PhotoListComponentHelp } from '@pages/photo-manage/photo-list/photo-list.component-help';
import { Photosort } from '@pages/photo-manage/photo-list/photo-sort';

@NgModule({
  declarations: [
    AppComponent,
    PhotoListPopoverComponent,
    TextSinglePopoverComponent,
    TextPopoverComponent
  ],
  entryComponents: [PhotoListPopoverComponent, TextSinglePopoverComponent, TextPopoverComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot({ swipeBackEnabled: false }),
    IonicStorageModule.forRoot(),
    StoreModule.forRoot({
      router: routerReducer,
      headState: HeadReducer,
      menuState: MenuReducer,
      appState: AppReducer,
      photobookInfoStore: PhotobookInfoReducer,
      photobookViewStore: PhotobookViewReducer,
      commonStore: CommonReducer,
      drawDisplayStore: DrawDisplayReducer,
      appLoading: AppLoadingReducer,
      cameraStore: CameraReducer,
      photomanageState: PhotomanagestoreReducer,
      offlineStore: OfflineStoreReducer,
      preSettingStore: PreSettingReducer,
      photoDetailStore: PhotoDetailStoreReducer,
      chatRoomStore: ChatRoomReducer,
      deviceTokenStore: DeviceTokenReducer,
      loggerStore: LoggerReducer,
      photoDownloadSuccessStore: PhotoDwonloadSuccessStoreReducer,
      autoUploadPhotoStore: AutoUploadPhotoStoreReducer,
      offlineUnitsStore: OfflineUnitsReducer

    }),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router' }),
    !environment.production ? StoreDevtoolsModule.instrument({ name: 'dkj', maxAge: 5 }) : [],
    CoreModule,
    SharedModule,
    AmplifyAngularModule,
    AppRoutingModule
  ],
  providers: [
    LoggerStoreService,
    Push,
    SplashScreen,
    StatusBar,
    File,
    HTTP,
    WebView,
    Network,
    AmplifyService,
    AuthService,
    CommonStore,
    DbDrawService,
    DbManageService,
    DbPhotoService,
    DbUnitsService,
    RoutingService,
    OfflineDownloadPdfService,
    OfflineDownloadPhotoService,
    PhotoDownloadSuccessService,
    AutoUploadPhotoService,
    AppLoadingStoreService,
    AppStateStoreService,
    CommonStoreService,
    DrawDisplayStoreService,
    HeadStateService,
    MenuStateService,
    OfflineStoreService,
    PhotobookInfoSetStoreService,
    PhotobookViewSetStoreService,
    PhotomanagestoreService,
    PreSettingStoreService,
    DeviceTokenStoreService,
    OfflineUnitsService,
    IF0101Service,
    IF0102Service,
    IF0103Service,
    IF0106Service,
    IF0108Service,
    IF0109Service,
    IF0110Service,
    API0202Service,
    API0301Service,
    API0302Service,
    API0303Service,
    API0304Service,
    API0306Service,
    API0501Service,
    API0504Service,
    API0505Service,
    API0601Service,
    API0602Service,
    API0603Service,
    API0604Service,
    API0605Service,
    API0701Service,
    API0801Service,
    API0802Service,
    API0803Service,
    API0805Service,
    API0806Service,
    API0808Service,
    API0809Service,
    API0810Service,
    API0901Service,
    API0902Service,
    API0903Service,
    API1107Service,
    API1108Service,
    API1109Service,
    API1110Service,
    API1111Service,
    API1112Service,
    API1201Service,
    API1401Service,
    API1402Service,
    API1403Service,
    API1404Service,
    API1405Service,
    API1406Service,
    API1407Service,
    API1408Service,
    API0102Service,
    API0112Service,
    API3002Service,
    API3004Service,
    API3006Service,
    API3007Service,
    API3008Service,
    API3009Service,
    API0104Service,
    API3010Service,
    API3011Service,
    API3012Service,
    API3013Service,
    API3014Service,
    API3003Service,

    StartExecutionService,
    DescribeExecutionService,
    RunExecutionService,
    CameraStoreService,
    DrawOfflineStoreService,
    PhotoDetailStoreService,
    ChatRoomStoreService,
    OfflineAPIService,
    ProcessStoreService,
    PhotoListComponentHelp,
    Photosort,
    CookieService,
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
