import { Component, OnDestroy } from '@angular/core';
import { Platform, Events } from '@ionic/angular';
import { Router, NavigationEnd } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AppStateStoreService } from './app_photo/store/app-state/app-state.store.service';
import { AuthService } from './app_common/service/auth.service';
import { EventManager } from '@angular/platform-browser';
import { Push, PushObject, PushOptions, RegistrationEventResponse } from '@ionic-native/push/ngx';
import { DeviceTokenStore, DeviceTokenStoreService } from '@store';
import { Storage } from '@ionic/storage';
import { environment } from '@env';
import { OfflineDownloadPdfService } from '@service/offline/offline-download-pdf.service';
import { OfflineDownloadPhotoService } from '@service/offline/offline-download-photo.service';
import { CookieService } from 'ngx-cookie-service';
import { MessageConstants } from '@constant/message-constants';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy {

  public gamenLable = {
    runningMsg: MessageConstants.E00108
  };

  runningFlg = false;
  isLoading: boolean;
  private deviceTokenStoreSubscription: Subscription;
  private deviceToken: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private appStateService: AppStateStoreService,
    private authService: AuthService,
    private network: Network,
    private router: Router,
    private eventManager: EventManager,
    private push: Push,
    private deviceTokenStoreService: DeviceTokenStoreService,
    public storage: Storage,
    public offlineDownloadPdfService: OfflineDownloadPdfService,
    public offlineDownloadPhotoService: OfflineDownloadPhotoService,
    public events: Events,
    private cookieService: CookieService
  ) {
    this.initializeApp();
  }

  ngOnDestroy() {
    this.deviceTokenStoreSubscription.unsubscribe();
  }

  async initializeApp() {
    const appId = new Date().getTime();

    await this.platform.ready();

    // pc/mobileかチェック
    this.appStateService.platformCheck();
    this.appStateService.responsiveRem();

    // ブラウザの戻る・進むを禁止
    const navEnd = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ) as Observable<NavigationEnd>;
    navEnd.subscribe(event => {
      history.pushState(null, null, event.url);
    });
    window.addEventListener('popstate', (e: PopStateEvent) => {
      history.pushState(null, null, window.location.pathname);
    });

    // キーボードは、F5リフレッシュを禁止する
    this.eventManager.addGlobalEventListener('window', 'keydown', (event: KeyboardEvent) => {
      if (event.key === 'F5') {
        if (event.preventDefault) {
          event.preventDefault();
        }
      }
      if (this.isLoading) {
        event.preventDefault();
      }
    });

    // 別のタブに稼働の制御
    if (this.appStateService.appState.isPC) {
      this.eventManager.addGlobalEventListener('window', 'beforeunload', (event: BeforeUnloadEvent) => {
        const runningBul = this.cookieService.get('com.dkj.photo.running');
        const runningObjBul = runningBul ? JSON.parse(runningBul) : { appIds: [] };
        runningObjBul.appIds = runningObjBul.appIds.filter((id: number) => id !== appId);
        this.cookieService.set('com.dkj.photo.running', JSON.stringify(runningObjBul), 0);
      });
      const running = this.cookieService.get('com.dkj.photo.running');
      const runningObj = running ? JSON.parse(running) : { appIds: [] };
      runningObj.appIds.push(appId);
      this.cookieService.set('com.dkj.photo.running', JSON.stringify(runningObj), 0);
      if (runningObj.appIds.length > 1) {
        this.runningFlg = true;
        return;
      }
    }

    // ブラウザ閉めるの場合、自動ログアウトを実施します。
    this.eventManager.addGlobalEventListener('window', 'beforeunload', async (event: BeforeUnloadEvent) => {
      await this.authService.signOut();
    });

    // mobileまたはwindowsタブレットの場合
    if (this.platform.is('android') || this.platform.is('ios') || this.appStateService.appState.isWinApp) {
      // get push
      this.pushSetup();
    }

    // ネットワーク状態を監視
    this.network.onDisconnect().subscribe(() => {
      this.appStateService.setIsOffline(true);
    });
    this.network.onConnect().subscribe(() => {
      this.appStateService.setIsOffline(false);
    });

    // 起動画面隠す
    this.splashScreen.hide();

    // 設定status bar
    if (this.platform.is('android')) {
      this.statusBar.styleLightContent();
    } else if (this.platform.is('ios')) {
      this.statusBar.styleDefault();
    }

    // APP backgroukaか監視
    if (this.platform.is('cordova')) {
      this.setPlatformListener();
    }

    this.initLoadingComponent();
    this.deviceTokenStoreSubscription = this.deviceTokenStoreService.deviceTokenStoreSubscription((store) => {
      this.deviceToken = store.deviceToken;
    });
  }

  setPlatformListener() {
    this.platform.pause.subscribe(() => {// background
      // console.log('In Background');
    });

    this.platform.resume.subscribe(() => {// foreground
      // console.log('In Foreground');
      this.events.publish('foreground');
    });
  }

  initLoadingComponent() {
  }

  async pushSetup() {
    const firstLaunch = await this.storage.get('firstLaunch');
    if (firstLaunch && firstLaunch === 1) {
      return;
    }

    // get token
    await this.storage.set('firstLaunch', 1);
    const options: PushOptions = {
      android: {
        senderID: environment.senderID,
        forceShow: true
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);
    // pushObject.on('notification').subscribe((notification) => console.log('Receivedotification', notification));
    pushObject.on('registration').subscribe((registration: RegistrationEventResponse) => {
      // console.log('Device registered', registration);
      // console.log(registration.registrationId);
      // console.log(registration.registrationType);
      const deviceToken = registration.registrationId;
      this.deviceTokenStoreService.setDeviceToken(deviceToken);

    });
    // pushObject.on('error').subscribe(error => console.error('Device registered Error', error));
  }

}
