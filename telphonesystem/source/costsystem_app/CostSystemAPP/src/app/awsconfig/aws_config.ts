import { environment } from '@env';

export const awsConfig = {
  // AWSリージョン
  aws_project_region: environment.aws_project_region,
  // Cognitoの配置
  aws_cognito_region: environment.aws_cognito_region,
  aws_user_pools_id: environment.aws_user_pools_id,
  aws_user_pools_web_client_id: environment.aws_user_pools_web_client_id,
  authenticationFlowType: 'CUSTOM_AUTH',
  // AppSyncの配置
  aws_appsync_region: environment.aws_appsync_region,
  aws_appsync_graphqlEndpoint: environment.aws_appsync_graphqlEndpoint,
  aws_appsync_authenticationType: 'AMAZON_COGNITO_USER_POOLS'
};
