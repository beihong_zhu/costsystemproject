export const environment = {
  debug: true,
  // FCM SenderID
  senderID: '644354443988',
  // サーバー環境フラグ
  production: true,
  // ダミーJSONを利用フラグ
  locadDummy: false,
  // APIのRootURL
  // wsurl: 'https://apicdn.dev.dksrv.jp/api',
  wsurl: 'https://restapi.dev.dksrv.jp/api',
  noauthurl: 'https://restapi.dev.dksrv.jp/noauth',
  executionurl: 'https://restapi.dev.dksrv.jp/execution',
  // AWSリージョン
  aws_project_region: 'ap-northeast-1',
  // Cognitoの配置
  aws_cognito_region: 'ap-northeast-1',
  aws_user_pools_id: 'ap-northeast-1_LtlhyagA1',
  aws_user_pools_web_client_id: '2ac3m796ulr1mc9t33kdul409v',
  // AppSyncの配置
  aws_appsync_region: 'ap-northeast-1',
  aws_appsync_graphqlEndpoint: 'https://htbj7eepn5cpnmkbkl35okjxoq.appsync-api.ap-northeast-1.amazonaws.com/graphql',
  // Step Functionsの配置
  aws_step_functions_arn: 'arn:aws:states:ap-northeast-1:497877802779:stateMachine:test-state'
};
