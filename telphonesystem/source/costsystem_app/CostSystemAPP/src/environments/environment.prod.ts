export const environment = {
  debug: false,
  // FCM SenderID
  senderID: '184658646683',
  // サーバー環境フラグ
  production: true,
  // ダミーJSONを利用フラグ
  locadDummy: false,
  // APIのRootURL
  wsurl: 'https://api.connect.daikin.co.jp/api',
  noauthurl: 'https://api.connect.daikin.co.jp/noauth',
  executionurl: 'https://api.connect.daikin.co.jp/execution',
  // AWSリージョン
  aws_project_region: 'ap-northeast-1',
  // Cognitoの配置
  aws_cognito_region: 'ap-northeast-1',
  aws_user_pools_id: 'ap-northeast-1_BaiXgHZvF',
  aws_user_pools_web_client_id: 'mt9qas2ei7fjrhkknnd4bd3k1',
  // AppSyncの配置
  aws_appsync_region: 'ap-northeast-1',
  aws_appsync_graphqlEndpoint: 'https://x6jridr7qndhdeu7gthxaqfnwa.appsync-api.ap-northeast-1.amazonaws.com/graphql',
  // Step Functionsの配置
  aws_step_functions_arn: 'arn:aws:states:ap-northeast-1:571145061886:stateMachine:dk-prod-state'
};
