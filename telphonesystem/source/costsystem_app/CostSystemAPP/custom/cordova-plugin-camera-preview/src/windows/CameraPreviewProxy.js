var mediaCapture;
var capturePreview;
var messageBox;
var sensor;
var simpleOrientation = Windows.Devices.Sensors.SimpleOrientation;
var orientationChangeFlg = 0;

function onOrientationChange(e) {
  // 撮影フラグ設定
  if (e.orientation === simpleOrientation.notRotated) {
    orientationChangeFlg = 0;
  } else if (e.orientation === simpleOrientation.rotated90DegreesCounterclockwise) {
    orientationChangeFlg = 1;
  } else if (e.orientation === simpleOrientation.rotated180DegreesCounterclockwise) {
    orientationChangeFlg = 0;
  } else if (e.orientation === simpleOrientation.rotated270DegreesCounterclockwise) {
    orientationChangeFlg = 1;
  } else if (e.orientation !== simpleOrientation.notRotated
    && e.orientation !== simpleOrientation.rotated90DegreesCounterclockwise
    && e.orientation !== simpleOrientation.rotated270DegreesCounterclockwise) {
    orientationChangeFlg = orientationChangeFlg;
  } else {
    orientationChangeFlg = 1;
  }
  // 撮影フラグで撮影制御
  if (orientationChangeFlg === 0) {
    capturePreview.play();
    messageBox.style.display = 'none';
  } else if (orientationChangeFlg === 1) {
    messageBox.style.display = 'block';
    capturePreview.pause();
  }
}

function orientationToRotation(orientation) {
  switch (orientation) {
    // portrait
    case simpleOrientation.notRotated:
      return Windows.Media.Capture.VideoRotation.none;
    // landscape
    case simpleOrientation.rotated90DegreesCounterclockwise:
      return Windows.Media.Capture.VideoRotation.clockwise270Degrees;
    // portrait-flipped
    case simpleOrientation.rotated180DegreesCounterclockwise:
      return Windows.Media.Capture.VideoRotation.clockwise180Degrees;
    // landscape-flipped
    case simpleOrientation.rotated270DegreesCounterclockwise:
      return Windows.Media.Capture.VideoRotation.clockwise90Degrees;
    // faceup & facedown
    default:
      // Falling back to portrait default
      return Windows.Media.Capture.VideoRotation.none;
  }
}

function hasView(errorCallback) {
  if (!capturePreview) {
    errorCallback('No preview');
    return false;
  }
  return true;
}

function hasCamera(errorCallback) {
  if (hasView(errorCallback) == false) {
    return false;
  }
  if (!mediaCapture) {
    errorCallback('No Camera');
    return false;
  }
  return true;
}

function showCapturePreview(successCallback, errorCallback, args) {
  var x = args[0];
  var y = args[1];
  var width = args[2];
  var height = args[3];
  var camera = args[4];

  var deviceEnum = Windows.Devices.Enumeration;
  var expectedPanel = camera === 'rear' ? deviceEnum.Panel.back : deviceEnum.Panel.front;

  deviceEnum.DeviceInformation.findAllAsync(deviceEnum.DeviceClass.videoCapture).then(function (devices) {
    if (devices.length <= 0) {
      errorCallback('Camera not found');
      return;
    }

    var winCapture = Windows.Media.Capture;
    var captureSettings = new winCapture.MediaCaptureInitializationSettings();
    captureSettings.alwaysPlaySystemShutterSound = true;
    captureSettings.streamingCaptureMode = winCapture.StreamingCaptureMode.video;
    devices.forEach(function (currDev) {
      if (currDev.enclosureLocation.panel && currDev.enclosureLocation.panel === expectedPanel) {
        captureSettings.videoDeviceId = currDev.id;
      }
    });
    if (!captureSettings.videoDeviceId) {
      captureSettings.videoDeviceId = devices[0].id;
    }

    mediaCapture = new winCapture.MediaCapture();
    return mediaCapture.initializeAsync(captureSettings);
  }).then(function () {
    capturePreview = document.createElement('video');
    capturePreview.style.cssText = `position: fixed; left: ${x}px; top: ${y}px; width: ${width}px; height: ${height}px;`;
    capturePreview.msZoom = true;
    capturePreview.src = URL.createObjectURL(mediaCapture);
    capturePreview.play();
    var content = document.getElementById('camera-content');
    content = content ? content : document.body;
    content.appendChild(capturePreview);

    messageBox = document.createElement('div');
    messageBox.style.cssText = `position: fixed; left: 0; top: 0; bottom:0;right:0; width: 100%; height: 100%;padding-top:25%; text-align: center;white-space:normal; word-break:break-all; word-wrap:break-word; font-weight: bold; font-size: 20px; background: #ffffff; display: none; z-index: 20`;
    messageBox.innerHTML = `警告：端末が縦向きになっている為、カメラ機能を停止します。横向きに持ち直してください。`;
    content.appendChild(messageBox);

    sensor = Windows.Devices.Sensors.SimpleOrientationSensor.getDefault();
    if (sensor !== null) {
      sensor.addEventListener('orientationchanged', onOrientationChange);
    }

    successCallback('Camera started');
  }).done(null, function (err) {
    errorCallback('Camera intitialization error ' + err);
  });
}

function takePictureBase64(successCallback, errorCallback, args) {
  var width = args[0];
  var height = args[1];
  var quality = args[2];

  var streams = Windows.Storage.Streams;
  var photoStream = new streams.InMemoryRandomAccessStream();
  var finalStream = new streams.InMemoryRandomAccessStream();
  var iBuffer;

  mediaCapture.capturePhotoToStreamAsync(Windows.Media.MediaProperties.ImageEncodingProperties.createJpeg(), photoStream)
    .then(function () {
      return Windows.Graphics.Imaging.BitmapDecoder.createAsync(photoStream);
    })
    .then(function (dec) {
      finalStream.size = 0;
      return Windows.Graphics.Imaging.BitmapEncoder.createForTranscodingAsync(finalStream, dec);
    })
    .then(function (enc) {
      // rotate the photo wrt sensor orientation
      if (sensor) {
        enc.bitmapTransform.rotation = orientationToRotation(sensor.getCurrentOrientation());
      }
      return enc.flushAsync();
    })
    .then(function () {
      iBuffer = new streams.Buffer(finalStream.size);
      return finalStream.readAsync(iBuffer, finalStream.size, streams.InputStreamOptions.none);
    })
    .done(function (buffer) {
      photoStream.close();
      finalStream.close();
      successCallback(Windows.Security.Cryptography.CryptographicBuffer.encodeToBase64String(iBuffer));
    }, function () {
      photoStream.close();
      finalStream.close();
      errorCallback('An error has occured while capturing the photo.');
    });
}

module.exports = {
  startCamera: function (successCallback, errorCallback, args) {
    showCapturePreview(successCallback, errorCallback, args);
  },
  stopCamera: function (successCallback, errorCallback, args) {
    if (hasCamera(errorCallback)) {
      if (sensor !== null) {
        sensor.removeEventListener('orientationchanged', onOrientationChange);
      }
      mediaCapture.close();
      mediaCapture = null;
      capturePreview.pause();
      capturePreview.src = null;
      var content = document.getElementById('camera-content');
      content = content ? content : document.body;
      content.removeChild(capturePreview);
      capturePreview = null;
      content.removeChild(messageBox);
      messageBox = null;
      successCallback('Camera stoped');
    }
  },
  takePicture: function (successCallback, errorCallback, args) {
    if (!hasCamera(errorCallback)) {
      return;
    }
    if (messageBox.style.display === 'block') {
      errorCallback('Camera orientation not supported');
      return;
    }
    takePictureBase64(successCallback, errorCallback, args);
  },
  getMaxZoom: function (successCallback, errorCallback) {
    if (hasCamera(errorCallback)) {
      var zoomControl = mediaCapture.videoDeviceController.zoom;
      if (zoomControl.capabilities.supported) {
        successCallback(zoomControl.capabilities.max);
      } else {
        errorCallback('Zoom not supported');
      }
    }
  },
  setZoom: function (successCallback, errorCallback, args) {
    if (hasCamera(errorCallback)) {
      var zoom = args[0];
      var zoomControl = mediaCapture.videoDeviceController.zoom;
      if (zoomControl.capabilities.supported) {
        zoomControl.trySetValue(zoom);
        successCallback(zoom);
      } else {
        errorCallback('Zoom not supported');
      }
    }
  },
  setFlashMode: function (successCallback, errorCallback, args) {
    if (hasCamera(errorCallback)) {
      var flashMode = args[0];
      var flashControl = mediaCapture.videoDeviceController.flashControl;
      if (flashControl.supported) {
        if (flashMode === 'off') {
          flashControl.enabled = false;
        } else if (flashMode === 'on') {
          flashControl.enabled = true;
          flashControl.auto = false;
        } else if (flashMode === 'auto') {
          flashControl.enabled = true;
          flashControl.auto = true;
        } else {
          errorCallback('Flash mode not recognised: ' + flashMode);
          return;
        }
        successCallback(flashMode);
      } else {
        errorCallback('Flash not supported');
      }
    }
  }
};

require('cordova/exec/proxy').add('CameraPreview', module.exports);
