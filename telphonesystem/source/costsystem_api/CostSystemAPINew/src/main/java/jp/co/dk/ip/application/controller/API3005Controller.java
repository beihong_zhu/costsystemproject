package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3005Req;
import jp.co.dk.ip.application.response.API3005Res;
import jp.co.dk.ip.application.response.data.AgentFundRecorderModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3005Service;

/**
 * all 资金记录 API Controller
 */
@RestController
@RequestMapping("/api")
public class API3005Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3005Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3005Service api3005Service;

	@PostMapping("/API3005")
	private API3005Res agentSummaryProcess(@RequestBody @Valid API3005Req api3005Req, BindingResult bindingResult) {

		logger.debug("API0112Controller agentSummaryProcess");

		API3005Res api3005Res = new API3005Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3005Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<AgentFundRecorderModelRes> agentFundRecorderModelResList = new ArrayList<AgentFundRecorderModelRes>();
			agentFundRecorderModelResList = api3005Service.getAgentNamesResList(api3005Req);
			
			// response setting
			if (!Objects.equals(agentFundRecorderModelResList, null)) {
				api3005Res.setResultCnt(agentFundRecorderModelResList.size());
				api3005Res.setAgentFundRecorderModelResList(agentFundRecorderModelResList);
				api3005Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api3005Res = api3005Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api3005Res = api3005Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3005);
		}

		return api3005Res;
	}
}
