package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import jp.co.dk.ip.application.request.APIAgentReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIAgentRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIAgentService;

/**
 * agent data reception  API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAgentController {

	private static final Logger logger = LoggerFactory.getLogger(APIAgentController.class);

	@Autowired
	private APIAgentService apiAgentService;
	
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIAGENT")
	private APIAgentRes agentDataProcess(@RequestBody @Valid APIAgentReq apiAgentReq, BindingResult bindingResult) {

		logger.debug("APIAgentController agentDataProcess");

		APIAgentRes apiAgentRes = new APIAgentRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiAgentService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiAgentRes = apiAgentService.agentDataProcess(apiAgentReq);
			
			// response setting
			if (!Utils.isEmpty(apiAgentRes)) {
				if (!Objects.equals(apiAgentRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiAgentRes = success(apiAgentRes);
				} else {
					apiAgentRes = failure(apiAgentRes);
				}
			} else {
				apiAgentRes = failure(apiAgentRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIAGENT + e.getMessage(), e);
			apiAgentRes = apiAgentService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIAGENT + e.getMessage(), e);
			apiAgentRes = apiAgentService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000005,
					Const.ERRRES_API0101);
		}

		return apiAgentRes;
	}
	
	
	/**
	 * success
	 */
	private APIAgentRes success(APIAgentRes apiAgentRes) {
		
		logger.debug("APIAgentController success");
		
		if (Utils.isEmpty(apiAgentRes.getResultCnt())) {
			apiAgentRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiAgentRes.getResultCode())) {
			apiAgentRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiAgentRes;
	}
	
	/**
	 * failure
	 */
	private APIAgentRes failure(APIAgentRes apiAgentRes) {
		
		logger.debug("APIAgentController failure");
		
		if (Utils.isEmpty(apiAgentRes)) {
			apiAgentRes = new APIAgentRes();
		}
		
		if (Utils.isEmpty(apiAgentRes.getResultCnt())) {
			apiAgentRes.setResultCnt(Const.RESULT_CNT_0);	
		}
		if (Utils.isEmpty(apiAgentRes.getResultCode())) {
			apiAgentRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiAgentRes.getMessageCode())) {
			apiAgentRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiAgentRes.getMessage())) {
			apiAgentRes.setMessage(message);
		}
		logger.error(message);
		
		return apiAgentRes;
	}
}
