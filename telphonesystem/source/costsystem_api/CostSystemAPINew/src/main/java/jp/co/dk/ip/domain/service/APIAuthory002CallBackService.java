package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import jp.co.dk.ip.application.request.APIAuthory002CallBackReq;
import jp.co.dk.ip.application.response.APIAuthory002CallBackRes;
import jp.co.dk.ip.application.response.APIChargeReturnRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.quartz.service.authory.AuthoryOrderPostService;
import jp.co.dk.ip.domain.repository.OrdersRepository;

/**
 * authory data reception 002 call back API Service
 */
@Service
@Transactional
public class APIAuthory002CallBackService {

    private static final Logger logger = LoggerFactory.getLogger(APIAuthory002CallBackService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private AuthoryOrderPostService authoryOrderPostService;
    
    @Autowired
    private OrdersRepository ordersRepository;
    
    /**
     * agent data reception 002 process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIAuthory002CallBackRes authoryDataProcess(APIAuthory002CallBackReq apiAuthor002CallBackReq) throws Exception {
		
        logger.debug("APIAuthory002CallBackService authoryDataProcess");
        
        APIAuthory002CallBackRes apiAuthory002CallBackRes = new APIAuthory002CallBackRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.AUTHORY_002_CALL_BACK_API.BILLID + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getBillId()
        			+ Const.AUTHORY_002_CALL_BACK_API.AND + Const.AUTHORY_002_CALL_BACK_API.CARRIERORDERID + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getCarrierOrderId()
        			+ Const.AUTHORY_002_CALL_BACK_API.AND + Const.AUTHORY_002_CALL_BACK_API.COMPANYID + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getCompanyId()
        			+ Const.AUTHORY_002_CALL_BACK_API.AND + Const.AUTHORY_002_CALL_BACK_API.COMPLETETIME + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getCompleteTime()
        			+ Const.AUTHORY_002_CALL_BACK_API.AND + Const.AUTHORY_002_CALL_BACK_API.ORDERID + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getOrderId()
        			+ Const.AUTHORY_002_CALL_BACK_API.AND + Const.AUTHORY_002_CALL_BACK_API.PHONE + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getPhone()
        			+ Const.AUTHORY_002_CALL_BACK_API.AND + Const.AUTHORY_002_CALL_BACK_API.STATUS + Const.AUTHORY_002_CALL_BACK_API.EQUAL + apiAuthor002CallBackReq.getStatus();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiAuthor002CallBackReq.getSign())) {
				// response error
				apiAuthory002CallBackRes.setResult(Const.AUTHORY_002_CALL_BACK_API.FAILURE);
				apiAuthory002CallBackRes.setResultCode(Const.RESULT_CODE_2);
				apiAuthory002CallBackRes.setResultCnt(Const.COUNT_0);
				apiAuthory002CallBackRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiAuthory002CallBackRes.setMessage(message);
				return apiAuthory002CallBackRes;
			}
			
			// response result
			String result = Const.EMPTY_STRING;
//			result = authoryOrderPostService.orderPostAgentData(
//					apiAuthor002CallBackReq.getOrderId(), 
//					apiAuthor002CallBackReq.getBillId(),
//					ordersRepository);
			apiAuthory002CallBackRes.setResult(result);
			
			return apiAuthory002CallBackRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAuthory002CallBackRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIAuthory002CallBackRes apiAuthory002CallBackRes = new APIAuthory002CallBackRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiAuthory002CallBackRes.setResultCode(resultCode);
        apiAuthory002CallBackRes.setMessageCode(errorCode);
        apiAuthory002CallBackRes.setMessage(errMsg);
        return apiAuthory002CallBackRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return apiAgent002Res response class
     */
    public APIAuthory002CallBackRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAuthory002CallBackService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAuthory002CallBackRes setErrRes(CustomHandleException e) {

        logger.debug("APIAuthory002CallBackService setErrRes");

        APIAuthory002CallBackRes apiAuthory002CallBackRes = new APIAuthory002CallBackRes();
        apiAuthory002CallBackRes.setResult(Const.AUTHORY_002_CALL_BACK_API.FAILURE);
        apiAuthory002CallBackRes.setResultCode(e.getErrorMessage().getResultCode());
        apiAuthory002CallBackRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiAuthory002CallBackRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiAuthory002CallBackRes.setMessage(e.getErrorMessage().getMessage());
        return apiAuthory002CallBackRes;
    }
}
