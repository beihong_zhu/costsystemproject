package jp.co.dk.ip.domain.model;

import com.google.gson.annotations.SerializedName;

import jp.co.dk.ip.domain.model.data.ThresHolds;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * 人脸识别 compare
 */
@Getter
@Setter
@AllArgsConstructor
public class FaceCompareModel {

    // confidence
	@SerializedName("confidence")
    private Double confidence;

    // request id
	@SerializedName("request_id")
    private String requestId;

    // time used
	@SerializedName("time_used")
    private Integer timeUsed;

    // thresholds
	@SerializedName("thresholds")
    private ThresHolds thresHolds;
}
