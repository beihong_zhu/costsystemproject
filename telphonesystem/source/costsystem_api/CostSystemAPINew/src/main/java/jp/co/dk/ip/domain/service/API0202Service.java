package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0202Req;
import jp.co.dk.ip.application.response.API0202Res;
import jp.co.dk.ip.application.response.data.StockInfoModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.StockInfoModel;
import jp.co.dk.ip.domain.repository.StocksRepository;

/**
 * all stock management API Service
 */
@Service
@Transactional
public class API0202Service {

	private static final Logger logger = LoggerFactory.getLogger(API0202Service.class);

	@Autowired
	private StocksRepository stocksRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * stock management
	 *
	 * @param api0202Req stock management
	 * @return result
	 */
	public List<StockInfoModelRes> getAgentNamesResList(API0202Req api0202Req) throws Exception {

		logger.debug("API0202Service getAgentNamesResList");

		List<StockInfoModelRes> stockInfoModelResList = new ArrayList<StockInfoModelRes>();
		
		try {
			List<StockInfoModel> stockInfoModels = new ArrayList<StockInfoModel>();

			// agents data setting
			stockInfoModels = stocksRepository.findStockByStockInfo(
					api0202Req.getChannelname(),
					api0202Req.getValue(),
					api0202Req.getProducttype(),
					api0202Req.getProductstatus()				
					);
			logger.info(Const.FIND_DATA_COUNT + stockInfoModels.size());

			// get all agents
			for (StockInfoModel stockInfoModel : stockInfoModels) {
				StockInfoModelRes stockInfoModelRes = new StockInfoModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(stockInfoModel, stockInfoModelRes);
				stockInfoModelResList.add(stockInfoModelRes);
			}

			return stockInfoModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API0202Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API0112Service getErrRes");

		API0202Res api0202Res = new API0202Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api0202Res.setResultCode(resultCode);
		api0202Res.setMessageCode(errorCode);
		api0202Res.setMessage(errMsg);
		return api0202Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0Res response class
	 */
	public API0202Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API0112Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API0202Res setErrRes(CustomHandleException e) {

		logger.debug("API0202Service setErrRes");

		API0202Res api0202Res = new API0202Res();
		api0202Res.setResultCode(e.getErrorMessage().getResultCode());
		api0202Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api0202Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api0202Res.setMessage(e.getErrorMessage().getMessage());
		return api0202Res;
	}
}
