package jp.co.dk.ip.domain.quartz.service.agent;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.apache.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.co.dk.ip.domain.model.data.OrderResultPostReq;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class APIAgentOrderResultCallBackService {
	
    private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderResultCallBackService.class);
	
	/**
	 * send order result request to agent
	 * 
	 * @return none
	 */
	@SuppressWarnings("unused")
	public void sendOrderResultRequestToAgent(
			String url,
			OrderResultPostReq orderPostReq,
			OrdersRepository ordersRepository) {

		try {
//			APIAgentOrderResultCallBackService.postAsyncClient(
//					url,
//					orderPostReq,
//					this.ordersRepository);
			RunAgentOrderResultCallBackService.postAgentAsync(
					url,
					orderPostReq,
					ordersRepository);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
//	/**
//	 * send request
//	 * 
//	 * @return client response
//	 */
//	public static void postAsyncClient(
//			String url,
//			OrderResultPostReq orderPostReq,
//			OrdersRepository ordersRepository)
//			throws InterruptedException, ExecutionException, ParseException, IOException {
//		
//		// 发送请求设置
//		RequestConfig requestConfig = RequestConfig
//				.custom()
//				.setConnectTimeout(Const.CONN_TIME_OUT_30000)
//				.setSocketTimeout(Const.SOCKET_TIME_OUT_50000)
//				.setConnectionRequestTimeout(Const.CONN_REQUEST_TIME_OUT_1000)
//				.build();
//
//		// 配置io线程
//		IOReactorConfig ioReactorConfig = IOReactorConfig
//				.custom()
//				.setIoThreadCount(Runtime.getRuntime().availableProcessors())
//				.setSoKeepAlive(true)
//				.build();
//
//		// 设置连接池大小
//		ConnectingIOReactor ioReactor = null;
//		try {
//			ioReactor = new DefaultConnectingIOReactor(ioReactorConfig);
//		} catch (IOReactorException e) {
//			e.printStackTrace();
//		}
//		PoolingNHttpClientConnectionManager connManager = new PoolingNHttpClientConnectionManager(ioReactor);
//		connManager.setMaxTotal(Const.MAX_TOTAL_ROUTE_100);
//		connManager.setDefaultMaxPerRoute(Const.MAX_TOTAL_ROUTE_100);
//
//		final CloseableHttpAsyncClient client = HttpAsyncClients
//				.custom()
//				.setConnectionManager(connManager)
//				.setDefaultRequestConfig(requestConfig)
//				.build();
//
//		// 构造请求
//		HttpPost httpPost = new HttpPost(url);
//		StringEntity entity = null;
//
//		try {
////            String a = "{ \"index\": { \"_index\": \"test\", \"_type\": \"test\"} }\n" +
////                    "{\"name\": \"上海\",\"age\":33}\n";
//
//			String requestData = Const.EMPTY_STRING;
//			Gson gson = new Gson();
//			requestData = gson.toJson(orderPostReq);
//			entity = new StringEntity(requestData);
//			entity.setContentType("application/json;charset=UTF-8");
//			httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
//			httpPost.setEntity(entity);
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
//
//		// start
//		client.start();
//
//		// 异步请求
//		RunCallBack runCallBack = new RunCallBack(url, ordersRepository, orderPostReq.getOrderid());
//		client.execute(httpPost, runCallBack);
//	}
//
//	/**
//	 * run http request call back
//	 */
//    private static class RunCallBack implements FutureCallback<HttpResponse> {
//
//        private long start = System.currentTimeMillis();
//		final CountDownLatch latch = new CountDownLatch(Const.COUNT_1);
//		public String output = Const.EMPTY_STRING;
//		
//		private String url;
//		private OrdersRepository ordersRepository;
//		private String orderId;
//		
//		RunCallBack(String url, OrdersRepository ordersRepository, String orderId) {
//			this.url = url;
//			this.ordersRepository = ordersRepository;
//			this.orderId = orderId;
//		}
//
//		@Override
//		public void completed(HttpResponse httpResponse) {
//
//			try {
//				output = EntityUtils.toString(httpResponse.getEntity(), Const.UNITCODE_TYPE.UTF_8);
//
//				// response process
//				AgentHttpResponseService.postAgentResponse(this.url, ordersRepository, output, this.orderId);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		@Override
//		public void failed(Exception e) {
//
//			latch.countDown();
//			System.err.println("cost is:" + (System.currentTimeMillis() - start) + ":" + e);
//			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
//		}
//
//		@Override
//        public void cancelled() {
//
//			latch.countDown();
//			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
//        }
//    }
}
