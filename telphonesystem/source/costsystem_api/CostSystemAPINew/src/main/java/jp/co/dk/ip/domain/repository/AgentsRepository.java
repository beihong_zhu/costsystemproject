package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.entity.Agents;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**.
 * agents repository
 */
@Repository
public interface AgentsRepository extends JpaRepository<Agents, String>, AgentsCustomRepository {

    // @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    Agents findByAgentIdAndIsDeleted(String agentId, int isDeleted);
    
    List<Agents> findByIsDeleted(int isDeleted);
}
