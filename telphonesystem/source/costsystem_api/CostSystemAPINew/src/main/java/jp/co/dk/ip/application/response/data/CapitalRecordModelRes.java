package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**
 * . capital record response
 */
@Getter
@Setter
public class CapitalRecordModelRes {

	/**
	 * . 代理商
	 */
	private String agentName;

	/**
	 * . 代理流水
	 */
	private String reqId;

	/**
	 * . 本地流水
	 */
	private String orderId;

	/**
	 * . 渠道流水
	 */
	private String billId;

	/**
	 * . 代理简称
	 */
	private String agentReduceName;

	/**
	 * . 方式
	 */
	private String orderForm;

	/**
	 * . 类型
	 */
	private String orderType;

	/**
	 * . 之前余额
	 */
	private Double capital;

	/**
	 * . 交易金额
	 */
	private Double price;

	/**
	 * . 状态
	 */
	private Integer status;

	/**
	 * . 支付时间
	 */
	private Double createdDatetime;
}
