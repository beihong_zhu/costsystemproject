package jp.co.dk.ip.domain.model.data;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * client data post 001 API Request
 */
@Getter
@Setter
public class OrderPostAuthory001Req extends BaseReq {

	// 商户编号 联系运营获取(不是用户名 是用户编号 例如：6)
	private String merid;

	// 产品类型 话费填phone,中石油填zsy，中石化填 zsh，Q币qb
	private String type;

	// 充值号码
	private String rechargeno;

	// 面值 例如：10 20 30
	private String amount;

	// 时间戳 例如：1573543703000 到毫秒
	private String ts;

	// 回调地址 需要回调，请填回调地址，最长为100字符，成功接收请返回success
	private String callbackurl;

	// 运营商编码 运营商编码，见附1运营商编码，充Q币时为空
	private String carrier;

	// 充值号码所属省份编码 充值号码所属省份编码见附2省份编码，充Q币时为空
	private String province;

	// 商户订单编号 商家提交的订单编号（必须唯一）8-32位之间
	private String orderid;

	// 签名串 MD5(amount + callbackurl + merid + orderid + rechargeno + ts+type + Key)
	private String sign;
}