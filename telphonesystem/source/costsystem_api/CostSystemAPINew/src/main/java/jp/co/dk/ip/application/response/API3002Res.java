package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.BaseRes;
import jp.co.dk.ip.application.response.data.CapitalRecordModelRes;
import lombok.Getter;
import lombok.Setter;

/**
 * capital record API Response
 */
@Getter
@Setter
public class API3002Res extends BaseRes {

    // data process result
    public List<CapitalRecordModelRes> capitalRecordModelResList;
}
