package jp.co.dk.ip.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * . capital status pk
 */
@SuppressWarnings("serial")
@Getter
@Setter
@EqualsAndHashCode
public class CapitalStatusPK implements Serializable {

	/**
	 * . 订单编号
	 */
	@Id
	@Column(name = "order_id")
	private String orderId;

	/**
	 * . 代理商ID
	 */
	@Id
	@Column(name = "agent_id")
	private String agentId;
}
