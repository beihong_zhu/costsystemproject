package jp.co.dk.ip.domain.entity;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * . agent product price pk
 */
@SuppressWarnings("serial")
@Getter
@Setter
@EqualsAndHashCode
public class AgentProductPricePK implements Serializable {

	/**
	 * . 代理商ID
	 */
	private String agentId;

	/**
	 * . 商品ID
	 */
	private String productId;
}
