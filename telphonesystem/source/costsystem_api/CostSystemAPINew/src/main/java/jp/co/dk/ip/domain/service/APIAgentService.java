package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import jp.co.dk.ip.application.request.APIAgentReq;
import jp.co.dk.ip.application.response.APIAgentRes;
import jp.co.dk.ip.application.response.APIChargeReturnRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.APIAgentOrderReqInfo;

/**
 * agent data reception API Service
 */
@Service
@Transactional
public class APIAgentService {

    private static final Logger logger = LoggerFactory.getLogger(APIAgentService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIAgentRes agentDataProcess(APIAgentReq apiAgentReq) throws Exception {
		
        logger.debug("APIAgentService agentDataProcess");
        
        APIAgentRes apiAgentRes = new APIAgentRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.AGENT_CHARGE_API.CORPID + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getCorpId()
        			+ Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.REQID + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getReqId()
        			+ Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.TS + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getTs()
        			+ Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.MONEY + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getMoney()
        			+ Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.SPID + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getSpId()
        			// + Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.PROVID + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getProvId()
        			+ Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.NUMBER + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getNumber()
        			+ Const.AGENT_CHARGE_API.AND + Const.AGENT_CHARGE_API.RETURL + Const.AGENT_CHARGE_API.EQUAL + apiAgentReq.getRetUrl();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiAgentReq.getSign())) {
				// response error
				apiAgentRes.setData(null);
				apiAgentRes.setResultCode(Const.RESULT_CODE_2);
				apiAgentRes.setResultCnt(Const.COUNT_0);
				apiAgentRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiAgentRes.setMessage(message);
				return apiAgentRes;
			}
        	
			// transfer request object
			APIAgentOrderReqInfo apiAgentOrderReqInfo = new APIAgentOrderReqInfo();
			Utils.copyPropertiesIgnoreNull(apiAgentReq, apiAgentOrderReqInfo);
			apiAgentOrderReqInfo.setIsRun(Const.NORUN);

			// enter into queue
			Utils.jinZhanQueue(apiAgentOrderReqInfo);

        	// response setting
			apiAgentRes.setData(Const.DATA_OK);
			apiAgentRes.setResultCode(Const.RESULT_CODE_0);
			return apiAgentRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAgentRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIAgentRes apiAgentRes = new APIAgentRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiAgentRes.setResultCode(resultCode);
        apiAgentRes.setMessageCode(errorCode);
        apiAgentRes.setMessage(errMsg);
        return apiAgentRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return apiAgentRes response class
     */
    public APIAgentRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAgentService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAgentRes setErrRes(CustomHandleException e) {

        logger.debug("APIAgentService setErrRes");

        APIAgentRes apiAgentRes = new APIAgentRes();
        apiAgentRes.setResultCode(e.getErrorMessage().getResultCode());
        apiAgentRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiAgentRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiAgentRes.setMessage(e.getErrorMessage().getMessage());
        return apiAgentRes;
    }
}
