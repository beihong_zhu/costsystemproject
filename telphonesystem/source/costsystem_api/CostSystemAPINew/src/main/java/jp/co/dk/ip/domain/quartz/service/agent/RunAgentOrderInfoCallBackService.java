package jp.co.dk.ip.domain.quartz.service.agent;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.util.EntityUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.HttpModel;
import jp.co.dk.ip.domain.model.data.OrderInfoPostReq;
import jp.co.dk.ip.domain.quartz.service.common.APICommonCallBackService;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class RunAgentOrderInfoCallBackService {
	
	/**
	 * send order info request for calling back
	 * @param <T>
	 * 
	 * @return none
	 */
	public void postAgentAsync(
			String url,
			OrderInfoPostReq orderInfoPostReq,
			OrdersRepository ordersRepository)
			throws InterruptedException, ExecutionException, ParseException, IOException {

		APICommonCallBackService apiCommonCallBackService = new APICommonCallBackService();
		HttpModel httpModel = new HttpModel();
		httpModel = apiCommonCallBackService.postAsync(url, orderInfoPostReq);
		final CloseableHttpAsyncClient client = httpModel.getClient();
		HttpPost httpPost = httpModel.getHttpPost();

		// start
		client.start();

		// 异步请求
		RunAgentCallBack runCallBack = new RunAgentCallBack(url, ordersRepository, orderInfoPostReq.getOrderId());
		client.execute(httpPost, runCallBack);
	}
	
	/**
	 * run agent http request call back
	 */
	public class RunAgentCallBack implements FutureCallback<HttpResponse> {

        private long start = System.currentTimeMillis();
		final CountDownLatch latch = new CountDownLatch(Const.COUNT_1);
		public String output = Const.EMPTY_STRING;
		
		private String url;
		private OrdersRepository ordersRepository;
		private String orderId;
		
		RunAgentCallBack(String url, OrdersRepository ordersRepository, String orderId) {
			this.url = url;
			this.ordersRepository = ordersRepository;
			this.orderId = orderId;
		}

		@Override
		public void completed(HttpResponse httpResponse) {

			try {
				output = EntityUtils.toString(httpResponse.getEntity(), Const.UNITCODE_TYPE.UTF_8);

				// response process
				AgentHttpResponseService.postAgentResponse(this.url, ordersRepository, output, this.orderId);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void failed(Exception e) {

			latch.countDown();
			System.err.println("cost is:" + (System.currentTimeMillis() - start) + ":" + e);
			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
		}

		@Override
        public void cancelled() {

			latch.countDown();
			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
        }
    }
}
