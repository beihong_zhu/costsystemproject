package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class AgentFundRecorderModel {

    // 供应商
    private String channelName;
    
    // 代理商流水号
    private String agencyLimited;

    // 本地流水
	private String localLimited;
    
    // 渠道流水
    private String channelLimited;
    
    // 账户简称
    private String accountAbref;
    
    // 方式
    private String mode;
    
    // 类型
    private String type;
    
    // 之前余额
    private String balanceLess;
    
    // 交易金额
    private String tractionLess;
    
    // 状态
    private String status;
    
    // 时间
    private String time;
    
   

    /**.
     * CSV Field
     *
     * @return String CSV Field
     */
    public String toRow() {
        return String.format(
                Utils.combinationString(Const.MANAGEMENTFILE.S_MARK, Const.MANAGEMENTFILE.USER_NUM_13),
                this.accountAbref,
                this.agencyLimited,
                this.localLimited,
                this.channelLimited,
                this.accountAbref,
                this.mode,
                this.type,
                this.balanceLess,
                this.tractionLess,
                this.status,
                this.time

        		);
    }
}
