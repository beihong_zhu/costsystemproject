package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * onshelf info API Request
 */
@Getter
@Setter
public class API3007Req extends BaseReq {	

	//代理名称
	@Length(max = 20, message = "{ECT010}")
	private String agencyName;
	//面额
    @Length(max = 20, message = "{ECT010}")
    private String value; 
    //商品类型
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productType;
	//商品状态
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productStatus;
 

  
    
   
    
    
}
