package jp.co.dk.ip.domain.quartz.service.common;

import java.util.Date;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Orders;
import jp.co.dk.ip.domain.model.OrderRedisModel;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class APIDBService {

    private static final Logger logger = LoggerFactory.getLogger(APIDBService.class);
   	
    /**
     * order insert
     * @param orderRedisModel
     * @param user
     * @return Boolean
     */
    public Boolean orderProcess(
    		OrderRedisModel orderRedisModel,
    		String user,
    		OrdersRepository ordersRepository) {
    	logger.info(new StringBuffer("order insert OrderServiceImpl.orderProcess indto: " +
        		(JSONObject) JSONObject.toJSON(orderRedisModel)).toString() + "," + user);

        Boolean result = true;

    	Orders order = new Orders();
    	Orders getOrder = new Orders();
    	order = ordersRepository.findByOrderIdAndIsDeleted(
    			orderRedisModel.getOrderId(),
    			Const.INT_DELETE_FLG_OFF);
        Integer updateFlg = 0;
        if (Utils.isEmpty(order)) {
            updateFlg = 0;
            order = new Orders();
        } else {
            updateFlg = 1;
        }
        
        if (!StringUtils.isEmpty(orderRedisModel.getCorpId())) {
        	order.setCorpId(orderRedisModel.getCorpId());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getReqId())) {
        	order.setReqId(orderRedisModel.getReqId());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getTs())) {
        	order.setTs(orderRedisModel.getTs());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getMoney())) {
        	order.setMoney(Integer.valueOf(orderRedisModel.getMoney()));
        }
        if (!StringUtils.isEmpty(orderRedisModel.getSpId())) {
        	order.setSpId(orderRedisModel.getSpId());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getProvId())) {
        	order.setProvId(orderRedisModel.getProvId());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getNumber())) {
        	order.setNumber(orderRedisModel.getNumber());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getUrl())) {
        	order.setRetUrl(orderRedisModel.getUrl());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getStatus())) {
        	order.setStatus(orderRedisModel.getStatus());
        }
        if (!StringUtils.isEmpty(orderRedisModel.getOrderId())) {
        	order.setOrderId(orderRedisModel.getOrderId());
        }
    	order.setOrderForm(Const.ORDER_FORM_ACCOUNT);
    	order.setOrderType(Const.ORDER_TYPE_PAY);

        // 共通字段
        if (Objects.equals(updateFlg, 0)) {
            if (StringUtils.isEmpty(order.getCreatedBy())) {
            	order.setCreatedBy(user);
            }
            if (Utils.isEmpty(order.getCreatedDatetime())) {
            	order.setCreatedDatetime(new Date());
            }
        }
        order.setUpdatedBy(user);
        order.setUpdatedDatetime(new Date());
    	logger.info(new StringBuffer("订单主表登录 OrderServiceImpl.orderProcess 开始：" +
        		(JSONObject) JSONObject.toJSON(orderRedisModel)).toString() + "," + user);
    	getOrder = ordersRepository.saveAndFlush(order);
        if (!Utils.isEmpty(getOrder)) {
        	result = true;
        	logger.info(new StringBuffer("订单主表登录 OrderServiceImpl.orderProcess 成功：" +
            		(JSONObject) JSONObject.toJSON(orderRedisModel)).toString() + "," + user);
        } else {
        	result = false;
        	logger.info(new StringBuffer("订单主表登录 OrderServiceImpl.orderProcess 失败：" +
            		(JSONObject) JSONObject.toJSON(orderRedisModel)).toString() + "," + user);
        }
        return result;
    }
}
