package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0111Req;
import jp.co.dk.ip.application.response.API0111Res;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Agents;
import jp.co.dk.ip.domain.repository.AgentsRepository;

/**
 * all agents search API Service
 */
@Service
@Transactional
public class API0111Service {

	private static final Logger logger = LoggerFactory.getLogger(API0111Service.class);

	@Autowired
	private AgentsRepository agentsRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents getting process
	 *
	 * @param api0111Req all agents getting request
	 * @return result
	 */
	public List<String> getAgentNamesResList(API0111Req api0111Req) throws Exception {

		logger.debug("API0111Service getAgentsResList");

		try {
			List<Agents> agents = new ArrayList<Agents>();

			// agents data setting
			agents = agentsRepository.findByIsDeleted(Const.INT_DELETE_FLG_OFF);
			logger.info(Const.FIND_DATA_COUNT + agents.size());

			// get all agents
			List<String> agentNames = new ArrayList<String>();
			agentNames.add(Const.ALL_AGENT_NAMES);
			for (Agents agent : agents) {
				String agentName = Const.EMPTY_STRING;
				agentName = agent.getAgentName();

				agentNames.add(agentName);
			}

			return agentNames;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0111 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0111 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0111 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API0111Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API0111Service getErrRes");

		API0111Res api0111Res = new API0111Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api0111Res.setResultCode(resultCode);
		api0111Res.setMessageCode(errorCode);
		api0111Res.setMessage(errMsg);
		return api0111Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0111Res response class
	 */
	public API0111Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API0111Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API0111Res setErrRes(CustomHandleException e) {

		logger.debug("API0111Service setErrRes");

		API0111Res api0111Res = new API0111Res();
		api0111Res.setResultCode(e.getErrorMessage().getResultCode());
		api0111Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api0111Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api0111Res.setMessage(e.getErrorMessage().getMessage());
		return api0111Res;
	}
}
