package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.StockInfoModelRes;
import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Response
 */
@Getter
@Setter
public class API0202Res extends BaseRes {

    // data process result
    public List<StockInfoModelRes> stockInfoModelResList;
}
