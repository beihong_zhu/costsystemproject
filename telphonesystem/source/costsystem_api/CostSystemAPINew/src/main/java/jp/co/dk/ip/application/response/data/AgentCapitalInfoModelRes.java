package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**
 * . agent info
 */
@Getter
@Setter
public class AgentCapitalInfoModelRes {

	/**
	 * . 账户名
	 */
	private String agentId;

	/**
	 * . 简称
	 */
	private String agentReduceName;

	/**
	 * . 省
	 */
	private String province;

	/**
	 * . 市
	 */
	private String city;

	/**
	 * . 模式
	 */
	private String businessMode;

	/**
	 * . 可用余额
	 */
	private Double capital;

	/**
	 * . 可用与否
	 */
	private String interfaceStatus;

	/**
	 * . 授信额度
	 */
	private Double creditLimit;

	/**
	 * . 创建时间
	 */
	private String createdDatetime;
}
