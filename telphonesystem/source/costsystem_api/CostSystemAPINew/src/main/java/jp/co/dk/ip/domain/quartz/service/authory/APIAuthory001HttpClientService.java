package jp.co.dk.ip.domain.quartz.service.authory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import org.apache.http.ParseException;
import jp.co.dk.ip.domain.model.data.OrderPostAuthory001Req;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class APIAuthory001HttpClientService {

	/**
	 * send request to authory
	 * 
	 * @return none
	 */
	public void sendRequestToAuthory001(
			String url,
			OrderPostAuthory001Req orderPostAuthory001Req,
			OrdersRepository ordersRepository) {

		APIAuthoryHttpClientService apiAuthoryHttpClientService = new APIAuthoryHttpClientService();
		try {
			apiAuthoryHttpClientService.postAsyncClient(
					url,
					orderPostAuthory001Req, 
					ordersRepository);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
