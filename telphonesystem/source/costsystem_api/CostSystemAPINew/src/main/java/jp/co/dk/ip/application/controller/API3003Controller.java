package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3003Req;
import jp.co.dk.ip.application.response.API3003Res;
import jp.co.dk.ip.application.response.data.CapitalReducePlusInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3003Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API3003Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3003Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3003Service api3003Service;

	@PostMapping("/API3003")
	private API3003Res agentSummaryProcess(@RequestBody @Valid API3003Req api3003Req, BindingResult bindingResult) {

		logger.debug("API3003Controller agentSummaryProcess");

		API3003Res api3003Res = new API3003Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3003Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<CapitalReducePlusInfoModelRes> capitalReducePlusInfoModelResList = new ArrayList<CapitalReducePlusInfoModelRes>();
			capitalReducePlusInfoModelResList = api3003Service.getAgentNamesResList(api3003Req);
			
			// response setting
			if (!Objects.equals(capitalReducePlusInfoModelResList, null)) {
				api3003Res.setResultCnt(capitalReducePlusInfoModelResList.size());
				api3003Res.setCapitalReducePlusInfoModelResList(capitalReducePlusInfoModelResList);
				api3003Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API3003 + e.getMessage(), e);
			api3003Res = api3003Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API3003 + e.getMessage(), e);
			api3003Res = api3003Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3003);
		}

		return api3003Res;
	}
}
