package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.BaseRes;
import jp.co.dk.ip.domain.model.SysUserInfoModel;
import lombok.Getter;
import lombok.Setter;

/**
 * user info getting API Response
 */
@Getter
@Setter
public class API0102Res extends BaseRes {

    // data process result
    public List<SysUserInfoModel> sysUserInfoModels;
}
