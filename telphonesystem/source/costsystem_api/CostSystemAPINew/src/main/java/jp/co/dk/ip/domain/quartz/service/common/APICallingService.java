package jp.co.dk.ip.domain.quartz.service.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.alibaba.fastjson.JSONObject;

import jp.co.dk.ip.application.response.APIAgentOrderInfoCallBackRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.domain.model.APIAgentOrderReqInfo;
import jp.co.dk.ip.domain.model.OrderRedisModel;
import jp.co.dk.ip.domain.model.data.OrderInfoPostReq;
import jp.co.dk.ip.domain.quartz.service.agent.APIAgentOrderInfoCallBackService;
import jp.co.dk.ip.domain.repository.AgentProductPriceRepository;
import jp.co.dk.ip.domain.repository.AgentsRepository;

@Service
@Transactional
public class APICallingService {

    private static final Logger logger = LoggerFactory.getLogger(APICallingService.class);
	
    /**
     * redis存储
     * @param <T>
     * @param t
     * @return Boolean
     */
	@SuppressWarnings("unused")
	public OrderRedisModel getOrderInfoPostAgentModel(
			APIAgentOrderReqInfo apiAgentOrderReqInfo,
			AgentsRepository agentsRepository,
			AgentProductPriceRepository agentProductPriceRepository,
			MessageSource messageSource,
			RedisTemplate redisTemplate
			) {
		
		OrderInfoPostReq orderInfoPostReq = new OrderInfoPostReq();
		OrderRedisModel orderRedisModel = new OrderRedisModel();
		APIAgentOrderInfoCallBackService apiAgentOrderInfoCallBackService = new APIAgentOrderInfoCallBackService();
        APIAgentOrderInfoCallBackRes apiAgentOrderInfoCallBackRes = new APIAgentOrderInfoCallBackRes();
		try {
			// create return object
			orderInfoPostReq = apiAgentOrderInfoCallBackService.orderInfoPostReqProcess(
					agentsRepository,
					agentProductPriceRepository,
					apiAgentOrderReqInfo);
			orderRedisModel.setCorpId(apiAgentOrderReqInfo.getCorpId());
			orderRedisModel.setOrderId(orderInfoPostReq.getOrderId());
			orderRedisModel.setOrderInfoPostReq(orderInfoPostReq);
			orderRedisModel.setUrl(Const.AGENT_ORDER_INFO_CALL_BACK_API.URL);
			
        	// redis insert
        	Boolean resultRedis = true;
        	APIRedisService apiRedisService = new APIRedisService();
        	String key = Const.EMPTY_STRING;
        	key = apiRedisService.getAgentRedisKey(orderRedisModel);
        	resultRedis = apiRedisService.redisProcess(
        			orderRedisModel,
        			messageSource,
        			redisTemplate,
        			key);
            if (resultRedis) {
            	logger.info(new StringBuffer("redis insert redisProcess success").append(
            			(JSONObject) JSONObject.toJSON(orderRedisModel)).toString());
            } else {
            	logger.info(new StringBuffer("redis insert redisProcess failure").append(
            			(JSONObject) JSONObject.toJSON(orderRedisModel)).toString());	            	
            }
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIAGENTORDERINFO + e.getMessage(), e);
			apiAgentOrderInfoCallBackRes = apiAgentOrderInfoCallBackService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIAGENTORDERINFO + e.getMessage(), e);
			apiAgentOrderInfoCallBackRes = apiAgentOrderInfoCallBackService.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000016,
					Const.ERRRES_API0101);
		}
		return orderRedisModel;
	}
}
