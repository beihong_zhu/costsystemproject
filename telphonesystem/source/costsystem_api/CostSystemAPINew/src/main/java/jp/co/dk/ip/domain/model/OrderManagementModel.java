package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class OrderManagementModel {

    // 代理名字
    private String agencyname;
    
    // 代理流水
    private String agecnylimited;

    // 本地流水
	private String locallimited;
    
    // 渠道流水
    private String channellimited;
    
    // 商品名
    private String productname;
    
    // 电话号码
    private String telnumber;
    
    // 面值
    private String value;
    
    // 状态
    private String status;
    
    // 时间
    private String time;

    /**.
     * CSV Field
     *
     * @return String CSV Field
     */
    public String toRow() {
        return String.format(
                Utils.combinationString(Const.MANAGEMENTFILE.S_MARK, Const.MANAGEMENTFILE.USER_NUM_13),
                this.agencyname,
                this.agecnylimited,
                this.locallimited,
                this.channellimited,
                this.productname,
                this.telnumber,
                this.value,
                this.status,
                this.time
 
        		);
    }
}
