package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * agent adding API Request
 */
@Getter
@Setter
public class API0104Req extends BaseReq {

	// 代理全称
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String agentFullName;

	// 代理简称
    @NotEmpty(message = "{ECT008}")
	@Length(max = 10, message = "{ECT010}")
	private String agentSimpleName;

	// 联系电话
    @NotEmpty(message = "{ECT008}")
	@Length(max = 20, message = "{ECT010}")
	private String tel;

	// 电子邮件
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String mail;
    
    // 代理账户
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String agentAccount;

	// 详细地址
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String detailedAddress;

	// 代理类型
    @NotEmpty(message = "{ECT008}")
	@Length(max = 5, message = "{ECT010}")
	private String agentType;
    
    // 账户状态
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String agentStatus;

	// 业务模式
    @NotEmpty(message = "{ECT008}")
	@Length(max = 5, message = "{ECT010}")
	private String businessMode;
    
  
    
  
}