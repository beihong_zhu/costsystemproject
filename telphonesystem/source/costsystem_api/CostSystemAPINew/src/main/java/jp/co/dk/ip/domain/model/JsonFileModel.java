package jp.co.dk.ip.domain.model;

import java.io.File;

import lombok.Getter;
import lombok.Setter;

/**.
 * JSONファイル対象
 */
@Getter
@Setter
public class JsonFileModel {

    // 写真データファイル
    private File jsonFile;

    // 写真データファイル名
    private String jsonFileName;
}
