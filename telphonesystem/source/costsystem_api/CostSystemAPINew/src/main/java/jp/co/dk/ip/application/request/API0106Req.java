package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * authory adding API Request
 */
@Getter
@Setter
public class API0106Req extends BaseReq {

	// 渠道全称
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String authoryFullName;

	// 渠道简称
    @NotEmpty(message = "{ECT008}")
	@Length(max = 10, message = "{ECT010}")
	private String authorySimpleName;

	// 联系电话
    @NotEmpty(message = "{ECT008}")
	@Length(max = 20, message = "{ECT010}")
	private String tel;

	// 电子邮件
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String mail;

	// 详细地址
    @NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String detailedAddress;

	// 渠道类型
    @NotEmpty(message = "{ECT008}")
	@Length(max = 5, message = "{ECT010}")
	private String authoryType;

	// 业务模式
    @NotEmpty(message = "{ECT008}")
	@Length(max = 5, message = "{ECT010}")
	private String businessMode;
}
