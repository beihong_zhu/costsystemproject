package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentFundRecorderModel;;

/**
 * . agents custom repository
 */
public class AgentsFundRecorderCustomRepositoryImpl implements AgentsFundRecorderCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentFundRecorderModel> findAgentFundRecorderByAgentInfo(String agencyLimited, String agentName, String localLimited,
			String agencyacount,String bussinessMode,String stranctionMode,String channelLimited,String agencyabbreviate,String ocrManufacturingDate) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " a.agent_id, "
                + " a.agent_reduce_name, "
                + " a.province, "
                + " a.city, "
                + " a.business_mode, "
                + " ct.capital, "
                + " a.interface_status, "
                + " a.credit_limit, "
                + " a.created_datetime "
                + "FROM "
                + " agents a "
                + "WHERE "
                + " a.is_deleted = :agentReduceName ";
		if (!Objects.equals(agencyLimited, Const.EMPTY_STRING) && !Objects.equals(agencyLimited, null)) {
			sql += "         AND a.agent_id = :agencyLimited ";
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(localLimited, Const.EMPTY_STRING) && !Objects.equals(localLimited, null)) {
			sql += "         AND a.agent_reduce_name = :localLimited ";
		}
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			sql += "         AND a.agent_reduce_name = :agencyacount ";
		}
		
		if (!Objects.equals(bussinessMode, Const.EMPTY_STRING) && !Objects.equals(bussinessMode, null)) {
			sql += "         AND a.agent_reduce_name = :bussinessMode ";
		}
		
		if (!Objects.equals(stranctionMode, Const.EMPTY_STRING) && !Objects.equals(stranctionMode, null)) {
			sql += "         AND a.agent_reduce_name = :stranctionMode ";
		}
		
		if (!Objects.equals(channelLimited, Const.EMPTY_STRING) && !Objects.equals(channelLimited, null)) {
			sql += "         AND a.agent_reduce_name = :channelLimited ";
		}
		
		if (!Objects.equals(agencyabbreviate, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviate, null)) {
			sql += "         AND a.agent_reduce_name = :agencyabbreviate ";
		}
		
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			sql += "         AND a.agent_reduce_name = :ocrManufacturingDate ";
		}
		
				
		// sql injection preventing
		agencyLimited = Utils.escapeSql(agencyLimited);
		agentName = Utils.escapeSql(agentName);
		agencyacount = Utils.escapeSql(agencyacount);
		bussinessMode = Utils.escapeSql(bussinessMode);
		stranctionMode = Utils.escapeSql(stranctionMode);
		channelLimited = Utils.escapeSql(channelLimited);
		agencyabbreviate = Utils.escapeSql(agencyabbreviate);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);
		
		
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.AgentFundRecorderModel");
		if (!Objects.equals(agencyLimited, Const.EMPTY_STRING) && !Objects.equals(agencyLimited, null)) {
			query.setParameter("agencyLimited", agencyLimited);
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			query.setParameter("agentName", agentName);
		}
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			query.setParameter("agencyacount", agencyacount);
		}
		
		if (!Objects.equals(bussinessMode, Const.EMPTY_STRING) && !Objects.equals(bussinessMode, null)) {
			query.setParameter("bussinessMode", bussinessMode);
		}
		
		if (!Objects.equals(stranctionMode, Const.EMPTY_STRING) && !Objects.equals(stranctionMode, null)) {
			query.setParameter("stranctionMode", stranctionMode);
		}
		
		if (!Objects.equals(channelLimited, Const.EMPTY_STRING) && !Objects.equals(channelLimited, null)) {
			query.setParameter("channelLimited", channelLimited);
		}
		
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			query.setParameter("agencyacount", agencyacount);
		}
		
		if (!Objects.equals(agencyabbreviate, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviate, null)) {
			query.setParameter("agencyabbreviate", agencyabbreviate);
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			query.setParameter("ocrManufacturingDate", ocrManufacturingDate);
		}
		

		List<AgentFundRecorderModel> list = query.getResultList();
		return list;
	}
}
