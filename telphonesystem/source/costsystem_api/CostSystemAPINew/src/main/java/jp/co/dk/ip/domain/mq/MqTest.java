package jp.co.dk.ip.domain.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.remoting.exception.RemotingException;

public class MqTest {

	private static final Logger logger = LoggerFactory.getLogger(MqTest.class);

	public static void main(String[] args) {

		sendMsg();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		receiveMsg();
	}

	public static void sendMsg() {

		// 获取消息生产者
		DefaultMQProducer producer = Producer.getDefaultMQProducer();
		SendResult sendResult = new SendResult();
		Message msg = null;

		try {
			for (int i = 0; i < 2000; i++) {
				msg = new Message("TopicTest1", // topic
						"TagA", // tag
						"OrderID00" + i, // key
						("Hello MetaQ" + i).getBytes()); // body
				sendResult = producer.send(msg);
				logger.info("sendResult:{}", sendResult);
			}
		} catch (MQClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemotingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MQBrokerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		producer.shutdown();
	}

	public static void receiveMsg() {

		// 获取消息生产者
		DefaultMQPushConsumer consumer = Consumer.getDefaultMQPushConsumer();

		// 订阅主体
		try {
			consumer.subscribe("TopicTest1", "*");

			consumer.registerMessageListener(new MessageListenerConcurrently() {

				/**
				 * * 默认msgs里只有一条消息，可以通过设置consumeMessageBatchMaxSize参数来批量接收消息
				 */
				public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
						ConsumeConcurrentlyContext context) {

					logger.info("currentThreadName:{} and Receive New Messages:{}", Thread.currentThread().getName(),
							msgs);

					MessageExt msg = msgs.get(0);

					if (msg.getTopic().equals("TopicTest1")) {
						// 执行TopicTest1的消费逻辑
						if (msg.getTags() != null && msg.getTags().equals("TagA")) {
							// 执行TagA的消费
							logger.info("MsgBody:{}", new String(msg.getBody()));
						} else if (msg.getTags() != null && msg.getTags().equals("TagC")) {
							// 执行TagC的消费
						} else if (msg.getTags() != null && msg.getTags().equals("TagD")) {
							// 执行TagD的消费
						}
					} else if (msg.getTopic().equals("TopicTest2")) {
						// 执行TopicTest2的消费逻辑
					}
					return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
				}
			});

			/**
			 * Consumer对象在使用之前必须要调用start初始化，初始化一次即可<br>
			 */
			consumer.start();

			logger.info("Consumer Started.");
		} catch (MQClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
