package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0109Req;
import jp.co.dk.ip.application.response.API0109Res;
import jp.co.dk.ip.application.response.data.CapitalInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.service.API0109Service;

/**
 * stock getting API Controller
 */
@RestController
@RequestMapping("/api")
public class API0109Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0109Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0109Service api0109Service;

	@PostMapping("/API0109")
	private API0109Res capitalDataProcess(@RequestBody @Valid API0109Req api0109Req, BindingResult bindingResult) {

		logger.debug("API0109Controller capitalDataProcess");

		API0109Res api0109Res = new API0109Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0109Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {
			// start time checking
			if (!api0109Req.getStartTime().isEmpty()) {
				boolean checkResult = Utils.isValidTimeWithMs(api0109Req.getStartTime());
				if (!checkResult) {
					return api0109Service.getErrRes(
							new String[] {},
							Const.RESULT_CODE_9,
							Const.ECT019,
							Const.FIELD_ERROR);
				}
			}

			// end time checking
			if (!api0109Req.getEndTime().isEmpty()) {
				boolean checkResult = Utils.isValidTimeWithMs(api0109Req.getEndTime());
				if (!checkResult) {
					return api0109Service.getErrRes(
							new String[] {},
							Const.RESULT_CODE_9,
							Const.ECT019,
							Const.FIELD_ERROR);
				}
			}
			
			// data getting
			List<CapitalInfoModelRes> capitalInfoModelResList = new ArrayList<CapitalInfoModelRes>();
			capitalInfoModelResList = api0109Service.getCapitalInfoModelResList(api0109Req);
			
			// response setting
			if (!Objects.equals(capitalInfoModelResList, null)) {
				api0109Res.setResultCnt(capitalInfoModelResList.size());
				api0109Res.setCapitalInfoModels(capitalInfoModelResList);
				api0109Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001, 
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0109 + e.getMessage(), e);
			api0109Res = api0109Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0109 + e.getMessage(), e);
			api0109Res = api0109Service.getErrRes(
					new String[] {}, 
					Const.RESULT_CODE_9, 
					Const.E000005,
					Const.ERRRES_API0109);
		}

		return api0109Res;
	}
}
