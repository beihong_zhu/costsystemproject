package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3004Req extends BaseReq {
	
	// 代理账户
	@Length(max = 20, message = "{ECT010}")
	private String accountagency;

	// 代理商公司名称
	@Length(max = 20, message = "{ECT010}")
	private String agencyname;

	// 代理商简称
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String agecnyabbreviate;
    
	// 商品类型
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String agency;
    
}
