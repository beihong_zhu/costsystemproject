package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIChargeReturnReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIChargeReturnRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIChargeReturnService;

/**
 * charge return API Controller
 */
@RestController
@RequestMapping("/api")
public class APIChargeReturnController {

	private static final Logger logger = LoggerFactory.getLogger(APIChargeReturnController.class);

	@Autowired
	private APIChargeReturnService apiChargeReturnService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APICHARGERETURN")
	private APIChargeReturnRes agentDataProcess(@RequestBody @Valid APIChargeReturnReq apiChargeReturnReq, BindingResult bindingResult) {

		logger.debug("APIChargeReturnController agentDataProcess");

        APIChargeReturnRes apiChargeReturnRes = new APIChargeReturnRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiChargeReturnService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiChargeReturnRes = apiChargeReturnService.agentDataProcess(apiChargeReturnReq);
			
			// response setting
			if (!Utils.isEmpty(apiChargeReturnRes)) {
				if (!Objects.equals(apiChargeReturnRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiChargeReturnRes = success(apiChargeReturnRes);
				} else {
					apiChargeReturnRes = failure(apiChargeReturnRes);
				}
			} else {
				apiChargeReturnRes = failure(apiChargeReturnRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APICHARGERETURN + e.getMessage(), e);
			apiChargeReturnRes = apiChargeReturnService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APICHARGERETURN + e.getMessage(), e);
			apiChargeReturnRes = apiChargeReturnService.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000016,
					Const.ERRRES_APICHARGERETURN);
		}

		return apiChargeReturnRes;
	}
	
	/**
	 * success
	 */
	private APIChargeReturnRes success(APIChargeReturnRes apiChargeReturnRes) {
		
		logger.debug("APIChargeReturnController success");
		
		if (Utils.isEmpty(apiChargeReturnRes.getResultCnt())) {
			apiChargeReturnRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiChargeReturnRes.getResultCode())) {
			apiChargeReturnRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiChargeReturnRes;
	}
	
	/**
	 * failure
	 */
	private APIChargeReturnRes failure(APIChargeReturnRes apiChargeReturnRes) {
		
		logger.debug("APIChargeReturnController failure");
		
		if (Utils.isEmpty(apiChargeReturnRes)) {
			apiChargeReturnRes = new APIChargeReturnRes();
		}
		
		if (Utils.isEmpty(apiChargeReturnRes.getResultCnt())) {
			apiChargeReturnRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiChargeReturnRes.getResultCode())) {
			apiChargeReturnRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiChargeReturnRes.getMessageCode())) {
			apiChargeReturnRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiChargeReturnRes.getMessage())) {
			apiChargeReturnRes.setMessage(message);
		}
		logger.error(message);
		
		return apiChargeReturnRes;
	}
}
