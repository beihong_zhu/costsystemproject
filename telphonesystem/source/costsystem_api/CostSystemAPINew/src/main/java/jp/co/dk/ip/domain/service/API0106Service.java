package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0106Req;
import jp.co.dk.ip.application.response.API0106Res;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Authories;
import jp.co.dk.ip.domain.repository.AuthoriesRepository;

/**
 * authory adding API Service
 */
@Service
@Transactional
public class API0106Service {

    private static final Logger logger = LoggerFactory.getLogger(API0106Service.class);

    @Autowired
    private AuthoriesRepository authoriesRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * authory adding
     *
     * @param api0106Req authory data
     * @return result
     */
	public void addAuthory(API0106Req api0106Req) throws Exception {

        logger.debug("API0106Service addAuthory");

        try {
        	// authory data getting
        	List<Authories> authories = new ArrayList<Authories>();
        	authories = authoriesRepository.findByIsDeleted(Const.INT_DELETE_FLG_OFF);
        	
        	// set authory id
        	List<Integer> authoryIds = new ArrayList<Integer>();
        	for (Authories authory: authories) {
        		Integer authoryId = Const.COUNT_0;
        		authoryId = Integer.valueOf(authory.getAuthoryId());
        		authoryIds.add(authoryId);
        	}
        	// get max authory id
			Integer max = Const.COUNT_0;
			max = authoryIds.stream().reduce(Integer::max).get();
        	
        	// authory data setting
			Authories authory = new Authories();
			authory.setAuthoryId(String.valueOf(max + Const.COUNT_1));
        	authory.setAuthoryName(api0106Req.getAuthoryFullName());
        	authory.setAbbreviation(api0106Req.getAuthorySimpleName());
        	authory.setTel(api0106Req.getTel());
        	authory.setMail(api0106Req.getMail());
        	authory.setAddress(api0106Req.getDetailedAddress());
        	authory.setAuthoryType(api0106Req.getAuthoryType());
        	authory.setBusinessMode(api0106Req.getBusinessMode());
        	
        	authoriesRepository.saveAndFlush(authory);
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0106 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0106 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0106 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public API0106Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("API0106Service getErrRes");

        API0106Res api0106Res = new API0106Res();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        api0106Res.setResultCode(resultCode);
        api0106Res.setMessageCode(errorCode);
        api0106Res.setMessage(errMsg);
        return api0106Res;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return API0106Res response class
     */
    public API0106Res getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("API0106Service getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public API0106Res setErrRes(CustomHandleException e) {

        logger.debug("API0106Service setErrRes");

        API0106Res api0106Res = new API0106Res();
        api0106Res.setResultCode(e.getErrorMessage().getResultCode());
        api0106Res.setResultCnt(e.getErrorMessage().getResultCnt());
        api0106Res.setMessageCode(e.getErrorMessage().getMessageCode());
        api0106Res.setMessage(e.getErrorMessage().getMessage());
        return api0106Res;
    }
}
