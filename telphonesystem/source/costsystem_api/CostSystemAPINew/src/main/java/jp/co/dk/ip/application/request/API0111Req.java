package jp.co.dk.ip.application.request;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents search API Request
 */
@Getter
@Setter
public class API0111Req extends BaseReq {

}