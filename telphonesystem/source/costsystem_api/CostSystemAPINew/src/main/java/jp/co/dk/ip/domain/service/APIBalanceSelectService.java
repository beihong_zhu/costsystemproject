package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.APIBalanceSelectReq;
import jp.co.dk.ip.application.response.APIBalanceSelectRes;
import jp.co.dk.ip.application.response.APIChargeReturnRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.RedisUtil;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.quartz.service.common.APIRedisService;

/**
 * balance charge API Service
 */
@Service
@Transactional
public class APIBalanceSelectService {

    private static final Logger logger = LoggerFactory.getLogger(APIBalanceSelectService.class);

    @Autowired
    private RedisUtil redisUtil;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIBalanceSelectRes agentDataProcess(APIBalanceSelectReq apiBalanceSelectReq) throws Exception {
		
        logger.debug("APIBalanceSelectService agentDataProcess");
        
        APIBalanceSelectRes apiBalanceSelectRes = new APIBalanceSelectRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.BALANCE_SELECT_API.CORPID + Const.BALANCE_SELECT_API.EQUAL + apiBalanceSelectReq.getCorpId()
        			+ Const.BALANCE_SELECT_API.AND + Const.BALANCE_SELECT_API.REQID + Const.BALANCE_SELECT_API.EQUAL + apiBalanceSelectReq.getReqId()
        			+ Const.BALANCE_SELECT_API.AND + Const.BALANCE_SELECT_API.ACCOUNTID + Const.BALANCE_SELECT_API.EQUAL + apiBalanceSelectReq.getAccountId()
        			+ Const.BALANCE_SELECT_API.AND + Const.BALANCE_SELECT_API.TS + Const.BALANCE_SELECT_API.EQUAL + apiBalanceSelectReq.getTs()
        			+ Const.BALANCE_SELECT_API.AND + Const.BALANCE_SELECT_API.NUMBER + Const.BALANCE_SELECT_API.EQUAL + apiBalanceSelectReq.getNumber();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiBalanceSelectReq.getSign())) {
				// response error
				apiBalanceSelectRes.setResultCode(Const.RESULT_CODE_2);
				apiBalanceSelectRes.setResultCnt(Const.COUNT_0);
				apiBalanceSelectRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiBalanceSelectRes.setMessage(message);
				return apiBalanceSelectRes;
			}
        	
			// redis data find
			APIRedisService apiRedisService = new APIRedisService();
			OrderRedisMicroUserModel orderRedisMicroUserModel = new OrderRedisMicroUserModel();
			String key = Const.EMPTY_STRING;
			key = apiRedisService.getMicroRedisKey(orderRedisMicroUserModel, apiBalanceSelectReq);
			orderRedisMicroUserModel = apiRedisService.getOrderRedisMicroUserModel(key, redisTemplate);

			// set response
			if (!Utils.isEmpty(orderRedisMicroUserModel)) {
				if (!Utils.checkObjAllFieldsIsNull(orderRedisMicroUserModel)) {
					apiBalanceSelectRes.setBalance(orderRedisMicroUserModel.getBalance());
				} else {
					apiBalanceSelectRes.setResultCnt(Const.COUNT_0);
				}
			} else {
				apiBalanceSelectRes = null;
			}
			return apiBalanceSelectRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIBalanceSelectRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIBalanceSelectRes apiBalanceSelectRes = new APIBalanceSelectRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiBalanceSelectRes.setResultCode(resultCode);
        apiBalanceSelectRes.setMessageCode(errorCode);
        apiBalanceSelectRes.setMessage(errMsg);
        return apiBalanceSelectRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIBalanceSelectRes response class
     */
    public APIBalanceSelectRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIBalanceSelectService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIBalanceSelectRes setErrRes(CustomHandleException e) {

        logger.debug("APIBalanceSelectService setErrRes");

        APIBalanceSelectRes apiBalanceSelectRes = new APIBalanceSelectRes();
        apiBalanceSelectRes.setResultCode(e.getErrorMessage().getResultCode());
        apiBalanceSelectRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiBalanceSelectRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiBalanceSelectRes.setMessage(e.getErrorMessage().getMessage());
        return apiBalanceSelectRes;
    }
}
