package jp.co.dk.ip.domain.repository;

import java.util.List;

import javax.persistence.LockModeType;

import jp.co.dk.ip.domain.entity.SysUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

/**.
 * 系统用户 repository
 */
@Repository
public interface SysUsersRepository extends JpaRepository<SysUsers, String> {

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<SysUsers> findByIsDeleted(int isDeleted);

    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<SysUsers> findByUserIdAndIsDeleted(String userId, int isDeleted);
    
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<SysUsers> findByUserNameAndIsDeleted(String userName, int isDeleted);
      
    @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    List<SysUsers> findByUserIdAndUserNameAndIsDeleted(String userId, String userName, int isDeleted);
}
