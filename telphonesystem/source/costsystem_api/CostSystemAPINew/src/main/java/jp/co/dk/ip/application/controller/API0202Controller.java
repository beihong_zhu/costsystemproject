package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0202Req;
import jp.co.dk.ip.application.response.API0202Res;
import jp.co.dk.ip.application.response.data.StockInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0202Service;

/**
 * all stock management API Controller
 */
@RestController
@RequestMapping("/api")
public class API0202Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0202Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0202Service api0202Service;

	@PostMapping("/API0202")
	private API0202Res agentSummaryProcess(@RequestBody @Valid API0202Req api0202Req, BindingResult bindingResult) {

		logger.debug("API0202Controller agentSummaryProcess");

		API0202Res api0202Res = new API0202Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0202Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<StockInfoModelRes> stockInfoModelResList = new ArrayList<StockInfoModelRes>();
			stockInfoModelResList = api0202Service.getAgentNamesResList(api0202Req);
			
			// response setting
			if (!Objects.equals(stockInfoModelResList, null)) {
				api0202Res.setResultCnt(stockInfoModelResList.size());
				api0202Res.setStockInfoModelResList(stockInfoModelResList);
				api0202Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0202 + e.getMessage(), e);
			api0202Res = api0202Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0202 + e.getMessage(), e);
			api0202Res = api0202Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0202);
		}

		return api0202Res;
	}
}
