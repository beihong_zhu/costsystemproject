package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.AgentCapitalInfoModelRes;
import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * agent info API Response
 */
@Getter
@Setter
public class API0110Res extends BaseRes {

    // data process result
    public List<AgentCapitalInfoModelRes> agentCapitalInfoModels;
}
