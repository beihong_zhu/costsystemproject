package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3013Req;
import jp.co.dk.ip.application.response.API3013Res;
import jp.co.dk.ip.application.response.data.CapitalTotalInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3013Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API3013Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3013Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3013Service api3013Service;

	@PostMapping("/API3013")
	private API3013Res agentSummaryProcess(@RequestBody @Valid API3013Req api3013Req, BindingResult bindingResult) {

		logger.debug("API3013Controller agentSummaryProcess");

		API3013Res api3013Res = new API3013Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3013Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<CapitalTotalInfoModelRes> capitalTotalInfoModelResList = new ArrayList<CapitalTotalInfoModelRes>();
			capitalTotalInfoModelResList = api3013Service.getAgentNamesResList(api3013Req);
			
			// response setting
			if (!Objects.equals(capitalTotalInfoModelResList, null)) {
				api3013Res.setResultCnt(capitalTotalInfoModelResList.size());
				api3013Res.setFundInfoModelResList(capitalTotalInfoModelResList);
				api3013Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0113 + e.getMessage(), e);
			api3013Res = api3013Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0113 + e.getMessage(), e);
			api3013Res = api3013Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0113);
		}

		return api3013Res;
	}
}
