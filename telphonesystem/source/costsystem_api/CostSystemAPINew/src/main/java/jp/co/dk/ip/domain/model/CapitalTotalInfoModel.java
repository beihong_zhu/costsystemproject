package jp.co.dk.ip.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * . order
 */
@Getter
@Setter
@AllArgsConstructor
public class CapitalTotalInfoModel {

	/**
	 * . 代理流水
	 */
	private String corpid;

	/**
	 * . 订单号
	 */
	private String orderId;

	/**
	 * . 账户简称
	 */
	private String agentReduceName;

	/**
	 * . 方式
	 */
	private String orderMoney;

	/**
	 * . 交易名称
	 */
	private String orderSuccessMoney;

	/**
	 * . 之前余额
	 */
	private String orderTotalSummary;

	/**
	 * . 交易金额
	 */
	private String orderRunningSummary;

	/**
	 * . 状态
	 */
	private String orderSuccessSummary;

	/**
	 * . 支付时间
	 */
	private String orderSuccessRate;
	
	/**
	 * . 支付时间
	 */
	private String orderSuccessTime;
	
	/**
	 * . 支付时间
	 */
	private String orderFailureTime;
	
	/**
	 * . 支付时间
	 */
	private String orderThreeMinsRate;
	
	
	/**
	 * . 支付时间
	 */
	private String orderTenMinsRate;
	
	
	
	
	
}
