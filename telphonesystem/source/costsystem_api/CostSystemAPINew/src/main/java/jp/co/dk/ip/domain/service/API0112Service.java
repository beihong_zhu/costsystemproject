package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0112Req;
import jp.co.dk.ip.application.response.API0112Res;
import jp.co.dk.ip.application.response.data.AgentSummaryModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentSummaryModel;
import jp.co.dk.ip.domain.repository.OrdersRepository;

/**
 * all agents summary API Service
 */
@Service
@Transactional
public class API0112Service {

	private static final Logger logger = LoggerFactory.getLogger(API0112Service.class);

	@Autowired
	private OrdersRepository ordersRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents summary
	 *
	 * @param api0112Req all agents summary
	 * @return result
	 */
	public List<AgentSummaryModelRes> getAgentNamesResList(API0112Req api0112Req) throws Exception {

		logger.debug("API0112Service getAgentNamesResList");

		List<AgentSummaryModelRes> agentSummaryModelResList = new ArrayList<AgentSummaryModelRes>();
		
		try {
			List<AgentSummaryModel> agentSummaryModels = new ArrayList<AgentSummaryModel>();

			// agents data setting
			agentSummaryModels = ordersRepository.findOrderSummaryByOrderInfo(
					api0112Req.getAgentName(),
					api0112Req.getAgentReduceName(),
					api0112Req.getMoney(),
					api0112Req.getStartTime(),
					api0112Req.getEndTime()					
					);
			logger.info(Const.FIND_DATA_COUNT + agentSummaryModels.size());

			// get all agents
			for (AgentSummaryModel agentSummaryModel : agentSummaryModels) {
				AgentSummaryModelRes agentSummaryModelRes = new AgentSummaryModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(agentSummaryModel, agentSummaryModelRes);
				agentSummaryModelResList.add(agentSummaryModelRes);
			}

			return agentSummaryModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API0112Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API0112Service getErrRes");

		API0112Res api0112Res = new API0112Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api0112Res.setResultCode(resultCode);
		api0112Res.setMessageCode(errorCode);
		api0112Res.setMessage(errMsg);
		return api0112Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0112Res response class
	 */
	public API0112Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API0112Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API0112Res setErrRes(CustomHandleException e) {

		logger.debug("API0112Service setErrRes");

		API0112Res api0112Res = new API0112Res();
		api0112Res.setResultCode(e.getErrorMessage().getResultCode());
		api0112Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api0112Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api0112Res.setMessage(e.getErrorMessage().getMessage());
		return api0112Res;
	}
}
