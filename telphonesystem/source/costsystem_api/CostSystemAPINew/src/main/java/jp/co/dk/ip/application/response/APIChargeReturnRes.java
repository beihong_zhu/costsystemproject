package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * charge return API Response
 */
@Getter
@Setter
public class APIChargeReturnRes extends BaseRes {

    // 充值返金
    private String returnMoney;
}
