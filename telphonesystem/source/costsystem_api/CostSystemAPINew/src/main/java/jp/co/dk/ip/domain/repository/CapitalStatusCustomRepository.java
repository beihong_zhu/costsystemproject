package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.CapitalInfoModel;
import jp.co.dk.ip.domain.model.CapitalRecordModel;

/**
 * . capital repository
 */
public interface CapitalStatusCustomRepository {

	List<CapitalInfoModel> findCapitalByCapitalInfo(
			String corpId,
			String agentName,
			String agentReduceName,
			String spId,
			String orderId,
			String billId,
			String startTime,
			String endTime);
	
	List<CapitalRecordModel> findCapitalRecordByCapitalInfo(
			String corpId,
			String agentName,
			String agentReduceName,
			String reqId,
			String orderId,
			String billId,
			String orderForm,
			String orderType,
			String startTime,
			String endTime);
}
