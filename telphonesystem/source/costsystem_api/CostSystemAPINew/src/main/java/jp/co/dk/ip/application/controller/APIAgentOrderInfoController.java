package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIAgentOrderInfoReq;
import jp.co.dk.ip.application.response.APIAgentOrderInfoRes;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIAgentOrderInfoService;

/**
 * agent data reception  API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAgentOrderInfoController {

	private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderInfoController.class);

	@Autowired
	private APIAgentOrderInfoService apiAgentOrderInfoService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIAGENTORDERINFO")
	private APIAgentOrderInfoRes agentDataProcess(@RequestBody @Valid APIAgentOrderInfoReq apiAgentOrderInfoReq, BindingResult bindingResult) {

		logger.debug("APIAgentOrderInfoController agentDataProcess");

		APIAgentOrderInfoRes apiAgentOrderInfoRes = new APIAgentOrderInfoRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiAgentOrderInfoService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiAgentOrderInfoRes = apiAgentOrderInfoService.agentDataProcess(apiAgentOrderInfoReq);
			
			// response setting
			if (!Utils.isEmpty(apiAgentOrderInfoRes)) {
				if (!Objects.equals(apiAgentOrderInfoRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiAgentOrderInfoRes = success(apiAgentOrderInfoRes);
				} else {
					apiAgentOrderInfoRes = failure(apiAgentOrderInfoRes);
				}
			} else {
				apiAgentOrderInfoRes = failure(apiAgentOrderInfoRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIAGENTORDERINFO + e.getMessage(), e);
			apiAgentOrderInfoRes = apiAgentOrderInfoService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIAGENTORDERINFO + e.getMessage(), e);
			apiAgentOrderInfoRes = apiAgentOrderInfoService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000016,
					Const.ERRRES_APIAGENTORDERINFO);
		}

		return apiAgentOrderInfoRes;
	}
	
	/**
	 * success
	 */
	private APIAgentOrderInfoRes success(APIAgentOrderInfoRes apiAgentOrderInfoRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiAgentOrderInfoRes.getResultCnt())) {
			apiAgentOrderInfoRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiAgentOrderInfoRes.getResultCode())) {
			apiAgentOrderInfoRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiAgentOrderInfoRes;
	}
	
	/**
	 * failure
	 */
	private APIAgentOrderInfoRes failure(APIAgentOrderInfoRes apiAgentOrderInfoRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiAgentOrderInfoRes)) {
			apiAgentOrderInfoRes = new APIAgentOrderInfoRes();
		}
		
		if (Utils.isEmpty(apiAgentOrderInfoRes.getResultCnt())) {
			apiAgentOrderInfoRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiAgentOrderInfoRes.getResultCode())) {
			apiAgentOrderInfoRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiAgentOrderInfoRes.getMessageCode())) {
			apiAgentOrderInfoRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiAgentOrderInfoRes.getMessage())) {
			apiAgentOrderInfoRes.setMessage(message);
		}
		logger.error(message);
		
		return apiAgentOrderInfoRes;
	}
}
