package jp.co.dk.ip.application.controller;

import java.util.Locale;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0106Req;
import jp.co.dk.ip.application.response.API0106Res;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0106Service;

/**
 * authory adding API Controller
 */
@RestController
@RequestMapping("/api")
public class API0106Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0106Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0106Service api0106Service;

	@PostMapping("/API0106")
	private API0106Res authoryDataProcess(@RequestBody @Valid API0106Req api0106Req, BindingResult bindingResult) {

		logger.debug("API0106Controller authoryDataProcess");

		API0106Res api0106Res = new API0106Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0106Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {
			// data getting
			api0106Service.addAuthory(api0106Req);
			
			// response setting
			api0106Res.setResultCnt(Const.COUNT_1);
			api0106Res.setResultCode(Const.RESULT_CODE_0);
			logger.info(
					messageSource.getMessage(
							Const.I000001, 
							new String[] {},
							Locale.forLanguageTag(settingContext.getLangZh())));
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0106 + e.getMessage(), e);
			api0106Res = api0106Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0106 + e.getMessage(), e);
			api0106Res = api0106Service.getErrRes(
					new String[] {}, 
					Const.RESULT_CODE_9, 
					Const.E000005,
					Const.ERRRES_API0106);
		}

		return api0106Res;
	}
}
