	package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception call back response
 */
@Getter
@Setter
public class APIAgentOrderResultCallBackRes extends BaseRes {

    // 结果代码(最大长度:4 必填:Y)
    private String result;
}
