package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.ProductStockModelRes;
import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * all fund recorder  API Response
 * 
 */
@Getter
@Setter
public class API3006Res extends BaseRes {

    // data process result
    public List<ProductStockModelRes> productStockModelResList;
}
