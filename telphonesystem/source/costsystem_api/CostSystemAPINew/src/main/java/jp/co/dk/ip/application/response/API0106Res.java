package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * authory adding API Response
 */
@Getter
@Setter
public class API0106Res extends BaseRes {

}
