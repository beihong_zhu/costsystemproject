package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class OnShelfProductModelRes {

    // 代理简称
    private String AgentReduceName;
    
    // 商品名称    
	private String productName;
    
    // 商品类型 
    private String productType;
    
    // 运营商
    private String spId;
     
    // 面额   
    private String value;
   
    // 价格
    private String price;
    
    // 折扣
    private String disaccount;
    
    // 检查价格
    private String checkPrice;
    
    // 检查成本
    private String checkCost;
    
    // 状态
    private String status;
    
}
