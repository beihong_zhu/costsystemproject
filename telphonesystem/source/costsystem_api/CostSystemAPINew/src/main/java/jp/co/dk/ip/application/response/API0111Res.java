package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents info API Response
 */
@Getter
@Setter
public class API0111Res extends BaseRes {

    // data process result
    public List<String> agentNames;
}
