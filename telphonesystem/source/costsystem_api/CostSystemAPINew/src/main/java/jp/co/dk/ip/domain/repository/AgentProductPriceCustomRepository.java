package jp.co.dk.ip.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import jp.co.dk.ip.domain.model.OnShelfProductModel;

/**
 * . orders repository
 */

public interface AgentProductPriceCustomRepository {

	List<OnShelfProductModel> findStockByOnshelfInfo(	
			String agencyName,
			String value,
			String productType,
			String productStatus
					
		);
}
