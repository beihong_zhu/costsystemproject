package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIMicroLoginReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIMicroLoginService;

/**
 * MicroLogin return API Controller
 */
@RestController
@RequestMapping("/api")
public class APIMicroLoginController {

	private static final Logger logger = LoggerFactory.getLogger(APIMicroLoginController.class);

	@Autowired
	private APIMicroLoginService apiMicroLoginService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIMICROLOGIN")
	private APIMicroLoginRes agentDataProcess(@RequestBody @Valid APIMicroLoginReq apiMicroLoginReq, BindingResult bindingResult) {

		logger.debug("APIMicroLoginController agentDataProcess");

        APIMicroLoginRes apiMicroLoginRes = new APIMicroLoginRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiMicroLoginService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiMicroLoginRes = apiMicroLoginService.agentDataProcess(apiMicroLoginReq);
			
			// response setting
			if (!Utils.isEmpty(apiMicroLoginRes)) {
				if (!Objects.equals(apiMicroLoginRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiMicroLoginRes = success(apiMicroLoginRes);
				} else {
					apiMicroLoginRes = failure(apiMicroLoginRes);
				}
			} else {
				apiMicroLoginRes = failure(apiMicroLoginRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIMICROLOGIN + e.getMessage(), e);
			apiMicroLoginRes = apiMicroLoginService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIMICROLOGIN + e.getMessage(), e);
			apiMicroLoginRes = apiMicroLoginService.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000016,
					Const.ERRRES_APIMICROLOGIN);
		}

		return apiMicroLoginRes;
	}
	
	/**
	 * success
	 */
	private APIMicroLoginRes success(APIMicroLoginRes apiMicroLoginRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiMicroLoginRes.getResultCnt())) {
			apiMicroLoginRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiMicroLoginRes.getResultCode())) {
			apiMicroLoginRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiMicroLoginRes;
	}
	
	/**
	 * failure
	 */
	private APIMicroLoginRes failure(APIMicroLoginRes apiMicroLoginRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiMicroLoginRes)) {
			apiMicroLoginRes = new APIMicroLoginRes();
		}
		
		if (Utils.isEmpty(apiMicroLoginRes.getResultCnt())) {
			apiMicroLoginRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiMicroLoginRes.getResultCode())) {
			apiMicroLoginRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiMicroLoginRes.getMessageCode())) {
			apiMicroLoginRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiMicroLoginRes.getMessage())) {
			apiMicroLoginRes.setMessage(message);
		}
		logger.error(message);
		
		return apiMicroLoginRes;
	}
}
