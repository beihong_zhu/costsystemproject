package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3009Req;
import jp.co.dk.ip.application.response.API3009Res;
import jp.co.dk.ip.application.response.data.ProductInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3009Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API3009Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3009Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3009Service api3009Service;

	@PostMapping("/API3009")
	private API3009Res agentSummaryProcess(@RequestBody @Valid API3009Req api3009Req, BindingResult bindingResult) {

		logger.debug("API0112Controller agentSummaryProcess");

		API3009Res api3009Res = new API3009Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3009Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<ProductInfoModelRes> productInfoModelResList = new ArrayList<ProductInfoModelRes>();
			productInfoModelResList = api3009Service.getAgentNamesResList(api3009Req);		
			// response setting
			if (!Objects.equals(productInfoModelResList, null)) {
				api3009Res.setResultCnt(productInfoModelResList.size());
				api3009Res.setProductInfoModelResList(productInfoModelResList);
				api3009Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api3009Res = api3009Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api3009Res = api3009Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3009);
		}

		return api3009Res;
	}
}
