package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.ZeroInfoModel;
import lombok.Getter;
import lombok.Setter;

/**
 * . capital_total
 */
@Getter
@Setter
@Entity
@Table(name = "capital_total")
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "CapitalTotal.ZeroInfoModel", classes = {
		@ConstructorResult(targetClass = ZeroInfoModel.class, columns = {
				@ColumnResult(name = "agent_reduce_name", type = String.class),
				@ColumnResult(name = "capital_status", type = String.class),
				@ColumnResult(name = "capital", type = String.class),
				@ColumnResult(name = "created_datetime", type = String.class)
				})
		})
public class CapitalTotal {

	/**
	 * . 代理商ID
	 */
	@Id
	@Column(name = "agent_id")
	private String agentId;

	/**
	 * . 资金状态
	 */
	@Column(name = "capital_status")
	private String capitalStatus;

	/**
	 * . 资金量
	 */
	@Column(name = "capital")
	private Double capital;
	
	/**
	 * . 资金加量
	 */
	@Column(name = "plus_capital")
	private Double pluscapital;

	/**
	 * . 资金预警值
	 */
	@Column(name = "capital_warn")
	private String capitalWarn;
	
	/**
	 * . 预付款
	 */
	@Column(name = "facility")
	private String facility;
	
	/**
	 * . 利润
	 */
	@Column(name = "profit")
	private String profit;
	

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
