package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIBalanceSelectReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIBalanceSelectRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIBalanceSelectService;

/**
 * balance charge API Controller
 */
@RestController
@RequestMapping("/api")
public class APIBalanceSelectController {

	private static final Logger logger = LoggerFactory.getLogger(APIBalanceSelectController.class);

	@Autowired
	private APIBalanceSelectService apiBalanceSelectService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIBALANCESELECT")
	private APIBalanceSelectRes agentDataProcess(@RequestBody @Valid APIBalanceSelectReq apiBalanceSelectReq, BindingResult bindingResult) {

		logger.debug("APIBalanceSelectController agentDataProcess");

        APIBalanceSelectRes apiBalanceSelectRes = new APIBalanceSelectRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiBalanceSelectService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiBalanceSelectRes = apiBalanceSelectService.agentDataProcess(apiBalanceSelectReq);
			
			// response setting
			if (!Utils.isEmpty(apiBalanceSelectRes)) {
				if (!Objects.equals(apiBalanceSelectRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiBalanceSelectRes = success(apiBalanceSelectRes);
				} else {
					apiBalanceSelectRes = failure(apiBalanceSelectRes);
				}
			} else {
				apiBalanceSelectRes = failure(apiBalanceSelectRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIBALANCESELECT + e.getMessage(), e);
			apiBalanceSelectRes = apiBalanceSelectService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIBALANCESELECT + e.getMessage(), e);
			apiBalanceSelectRes = apiBalanceSelectService.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000016,
					Const.ERRRES_APIBALANCESELECT);
		}

		return apiBalanceSelectRes;
	}
	
	/**
	 * success
	 */
	private APIBalanceSelectRes success(APIBalanceSelectRes apiBalanceSelectRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiBalanceSelectRes.getResultCnt())) {
			apiBalanceSelectRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiBalanceSelectRes.getResultCode())) {
			apiBalanceSelectRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiBalanceSelectRes;
	}
	
	/**
	 * failure
	 */
	private APIBalanceSelectRes failure(APIBalanceSelectRes apiBalanceSelectRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiBalanceSelectRes)) {
			apiBalanceSelectRes = new APIBalanceSelectRes();
		}
		
		if (Utils.isEmpty(apiBalanceSelectRes.getResultCnt())) {
			apiBalanceSelectRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiBalanceSelectRes.getResultCode())) {
			apiBalanceSelectRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiBalanceSelectRes.getMessageCode())) {
			apiBalanceSelectRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiBalanceSelectRes.getMessage())) {
			apiBalanceSelectRes.setMessage(message);
		}
		logger.error(message);
		
		return apiBalanceSelectRes;
	}
}
