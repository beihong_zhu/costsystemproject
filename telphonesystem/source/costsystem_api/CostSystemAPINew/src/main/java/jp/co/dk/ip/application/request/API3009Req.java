package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3009Req extends BaseReq {
	
	// 代理商公司全称
	@Length(max = 20, message = "{ECT010}")
	private String aggencyLimited;

	// 代理商公司
	@Length(max = 8, message = "{ECT010}")
	private String localLimited;

	// 代理商充值面额
    @NotEmpty(message = "{ECT008}")
    @Length(max = 5, message = "{ECT010}")
    private String channelLimited;
    
	// 代理商起始时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String ocrManufacturingDate;
    

	
}
