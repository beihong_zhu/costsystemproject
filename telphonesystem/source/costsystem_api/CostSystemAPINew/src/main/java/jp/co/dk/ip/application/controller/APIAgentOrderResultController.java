package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIAgentOrderResultReq;
import jp.co.dk.ip.application.request.APIAgentReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIAgentRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIAgentOrderResultService;
import jp.co.dk.ip.domain.service.APIAgentService;

/**
 * agent data reception  API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAgentOrderResultController {

	private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderResultController.class);

	@Autowired
	private APIAgentOrderResultService apiAgentOrderResultService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIAGENTORDERRESULT")
	private APIAgentOrderResultRes agentDataProcess(@RequestBody @Valid APIAgentOrderResultReq apiAgentOrderResultReq, BindingResult bindingResult) {

		logger.debug("APIAgentOrderResultController agentDataProcess");

		APIAgentOrderResultRes apiAgentOrderResultRes = new APIAgentOrderResultRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiAgentOrderResultService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiAgentOrderResultRes = apiAgentOrderResultService.agentDataProcess(apiAgentOrderResultReq);
			
			// response setting
			if (!Utils.isEmpty(apiAgentOrderResultRes)) {
				if (!Objects.equals(apiAgentOrderResultRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiAgentOrderResultRes = success(apiAgentOrderResultRes);
				} else {
					apiAgentOrderResultRes = failure(apiAgentOrderResultRes);
				}
			} else {
				apiAgentOrderResultRes = failure(apiAgentOrderResultRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIAGENTORDERRESULT + e.getMessage(), e);
			apiAgentOrderResultRes = apiAgentOrderResultService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIAGENTORDERRESULT + e.getMessage(), e);
			apiAgentOrderResultRes = apiAgentOrderResultService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000016,
					Const.ERRRES_APIAGENTORDERRESULT);
		}

		return apiAgentOrderResultRes;
	}
	
	/**
	 * success
	 */
	private APIAgentOrderResultRes success(APIAgentOrderResultRes apiAgentOrderResultRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiAgentOrderResultRes.getResultCnt())) {
			apiAgentOrderResultRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiAgentOrderResultRes.getResultCode())) {
			apiAgentOrderResultRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiAgentOrderResultRes;
	}
	
	/**
	 * failure
	 */
	private APIAgentOrderResultRes failure(APIAgentOrderResultRes apiAgentOrderResultRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiAgentOrderResultRes)) {
			apiAgentOrderResultRes = new APIAgentOrderResultRes();
		}
		
		if (Utils.isEmpty(apiAgentOrderResultRes.getResultCnt())) {
			apiAgentOrderResultRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiAgentOrderResultRes.getResultCode())) {
			apiAgentOrderResultRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiAgentOrderResultRes.getMessageCode())) {
			apiAgentOrderResultRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiAgentOrderResultRes.getMessage())) {
			apiAgentOrderResultRes.setMessage(message);
		}
		logger.error(message);
		
		return apiAgentOrderResultRes;
	}
}
