package jp.co.dk.ip.domain.quartz.service.authory;

import jp.co.dk.ip.domain.model.data.OrderPostAuthory002Req;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class APIAuthory002HttpClientService {

	/**
	 * send request to authory 002
	 * 
	 * @return none
	 */
	public void sendRequestToAuthory002(
			String url,
			OrderPostAuthory002Req orderPostAuthory002Req,
			OrdersRepository ordersRepository) {

		APIAuthoryHttpClientService apiAuthoryHttpClientService = new APIAuthoryHttpClientService();
		try {
			apiAuthoryHttpClientService.postClient(
					url,
					orderPostAuthory002Req,
					orderPostAuthory002Req.getOrderId(),
					ordersRepository);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
	}
}
