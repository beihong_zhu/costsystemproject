package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import lombok.Getter;
import lombok.Setter;

/**
 * . orders_status
 */
@Getter
@Setter
@Entity
@Table(name = "orders_status")
@EntityListeners(AuditingEntityListener.class)
public class OrdersStatus {

	/**
	 * . 订单状态编号
	 */
	@Id
	@Column(name = "orders_status_code")
	private String ordersStatusCode;

	/**
	 * . 订单状态名
	 */
	@Column(name = "orders_status_name")
	private String ordersStatusName;
	
	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
