package jp.co.dk.ip.domain.model;

import lombok.Getter;
import lombok.Setter;

/**.
 * 写真帳用の作業リスト
 */
@Getter
@Setter
public class WorkIdListModel {

    // 工程ID
    private String processId;

    // 作業ID文字列
    private String workIdStr;
    
    // 不明作業出力フラグ
    private String unknownWorkOutput;
}
