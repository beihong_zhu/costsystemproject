package jp.co.dk.ip.domain.quartz.service.agent;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import org.apache.http.ParseException;
import com.google.gson.Gson;

import jp.co.dk.ip.application.response.APIAgentOrderResultCallBackRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.entity.Orders;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class AgentHttpResponseService {

	/**
	 * send request to agent
	 * 
	 * @return none
	 */
	public static void postAgentResponse(
			String url,
			OrdersRepository ordersRepository,
			String output,
			String orderId)
			throws InterruptedException, ExecutionException, ParseException, IOException {

		// enter into response queue
		if (!Objects.equals(output, Const.EMPTY_STRING)) {
			Gson gson = new Gson();
			APIAgentOrderResultCallBackRes apiAgentCallBackRes = gson.fromJson(output, APIAgentOrderResultCallBackRes.class);
			String result = Const.EMPTY_STRING;
			result = apiAgentCallBackRes.getResult();
			if (Objects.equals(result, Const.AGENT_CHARGE_API.SUCCESS)) {
				Orders order = new Orders();
				order = ordersRepository.findByOrderIdAndIsDeleted(
						orderId,
						Const.INT_DELETE_FLG_OFF);
				order.setStatus(Const.ORDERS_STATUS.FIVE);
				ordersRepository.saveAndFlush(order);
				System.out.println(url);
			}
		}
	}
}
