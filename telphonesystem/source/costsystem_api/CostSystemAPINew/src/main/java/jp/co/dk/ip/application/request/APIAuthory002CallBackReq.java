package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * authory data reception 002 call back API Request
 */
@Getter
@Setter
public class APIAuthory002CallBackReq extends BaseReq {

	// 用户id（平台分配）
	@NotEmpty(message = "{ECT008}")
	@Length(max = 30, message = "{ECT010}")
	private String companyId;

	// 需要充值的手机号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 30, message = "{ECT010}")
	private String billId;

	// 订单流水号(长度不超过32)
	@NotEmpty(message = "{ECT008}")
	@Length(max = 32, message = "{ECT010}")
	private String orderId;
	
	// 订单状态 '1'成功 '0'失败 '2'充值中（只有在'0'或者'1'的情况下才能确认是失败或者成功）
	@NotEmpty(message = "{ECT008}")
	@Length(max = 1, message = "{ECT010}")
	private String status;
	
	// 需要充值的手机号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 11, message = "{ECT010}")
	private String phone;

	// 产品编号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 8, message = "{ECT010}")
	private String carrierOrderId;

	// 类型（1：全国  2:分省） 注：不传时不参与签名
	@NotEmpty(message = "{ECT008}")
	@Length(max = 1, message = "{ECT010}")
	private String completeTime;

	// 签名md5(callbackUrl=www.testtest.cn/notice&companyId=90004&orderId=delaytestorderId10001&phone=15158158060&productId=50004&type=1I8K11nWVfv39tAlWB8MtvOJS82pTD5Vo)
	// 其中appKey由平台提供，详细签名规则见首页
	@NotEmpty(message = "{ECT008}")
	@Length(max = 32, message = "{ECT010}")
	private String sign;
}
