package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import jp.co.dk.ip.application.request.API3008Req;
import jp.co.dk.ip.application.response.API3008Res;
import jp.co.dk.ip.application.response.data.ZeroInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3008Service;

/**
 * all  zero account API Controller
 */
@RestController
@RequestMapping("/api")
public class API3008Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3008Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3008Service api3008Service;

	@PostMapping("/API3008")
	private API3008Res agentSummaryProcess(@RequestBody @Valid API3008Req api3008Req, BindingResult bindingResult) {

		logger.debug("API3008Controller agentSummaryProcess");

		API3008Res api3008Res = new API3008Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3008Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<ZeroInfoModelRes> zeroInfoModelResList= new ArrayList<ZeroInfoModelRes>();
			zeroInfoModelResList = api3008Service.getAgentNamesResList(api3008Req);
			
			// response setting
			if (!Objects.equals(zeroInfoModelResList, null)) {
				api3008Res.setResultCnt(zeroInfoModelResList.size());
				api3008Res.setZeroInfoModelResList(zeroInfoModelResList);
				api3008Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API3008 + e.getMessage(), e);
			api3008Res = api3008Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API3008 + e.getMessage(), e);
			api3008Res = api3008Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3008);
		}

		return api3008Res;
	}
}
