package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * agent adding API Response
 */
@Getter
@Setter
public class API0104Res extends BaseRes {

}
