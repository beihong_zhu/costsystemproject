package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3002Req;
import jp.co.dk.ip.application.response.API3002Res;
import jp.co.dk.ip.application.response.data.CapitalRecordModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3002Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API3002Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3002Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3002Service api3002Service;

	@PostMapping("/API3002")
	private API3002Res capitalRecordProcess(@RequestBody @Valid API3002Req api3002Req, BindingResult bindingResult) {

		logger.debug("API3002Controller capitalRecordProcess");

		API3002Res api3002Res = new API3002Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3002Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<CapitalRecordModelRes> capitalRecordModelResList = new ArrayList<CapitalRecordModelRes>();
			capitalRecordModelResList = api3002Service.getCapitalRecordModelResList(api3002Req);
			
			// response setting
			if (!Objects.equals(capitalRecordModelResList, null)) {
				api3002Res.setResultCnt(capitalRecordModelResList.size());
				api3002Res.setCapitalRecordModelResList(capitalRecordModelResList);
				api3002Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API3003 + e.getMessage(), e);
			api3002Res = api3002Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API3002 + e.getMessage(), e);
			api3002Res = api3002Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3002);
		}

		return api3002Res;
	}
}
