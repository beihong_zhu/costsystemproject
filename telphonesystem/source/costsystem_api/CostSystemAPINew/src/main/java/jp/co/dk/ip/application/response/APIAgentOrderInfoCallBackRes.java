package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception API Response
 */
@Getter
@Setter
public class APIAgentOrderInfoCallBackRes extends BaseRes {

    // 结果代码(最大长度:4 必填:Y)
    private String retcode;
    
    // 平台交易单号(最大长度:20 必填:N)
    private String orderid;
    
    // 价格（以元为单位，精确到小数点后4位）(最大长度:15 必填:N)
    private String price;
}
