package jp.co.dk.ip.domain.model;

import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception API Request
 */
@Getter
@Setter
public class APIAgentOrderReqInfo {

    // 账号
    private String corpId;
    
    // 代理商流水号
    private String reqId;
    
    // 时间戳，格式为：yyyyMMddHHmmss
    private String ts;
    
    // 面值（元）
    private String money;
    
    // 运营商：1-移动，2-联通，3-电信，4-中石化
    private String spId;
    
//    // 省份编码（详见附录）
//    private String provId;
    
    // 充值号码
    private String number;
    
    // 回调地址
    private String retUrl;
    
    // 签名
    private String sign;
    
    // run (1-run, 0-no run)
    private String isRun;
}
