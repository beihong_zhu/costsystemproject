package jp.co.dk.ip.domain.mq;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;

/**
 * @author zbh
 * @description 消息生产者
 * @date 2021-11-28
 */
public class Producer {

	/*
	 * Constructs a client instance with your account for accessing
	 * DefaultMQProducer
	 */
	private static DefaultMQProducer producer = new DefaultMQProducer("ProducerGroupName");
	private static int initialState = 0;

	private Producer() {
	}

	public static DefaultMQProducer getDefaultMQProducer() {
		if (producer == null) {
			producer = new DefaultMQProducer("ProducerGroupName");
		}
		if (initialState == 0) {
			producer.setNamesrvAddr("localhost:9876");
			try {
				producer.start();
			} catch (MQClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			initialState = 1;
		}
		return producer;
	}
}
