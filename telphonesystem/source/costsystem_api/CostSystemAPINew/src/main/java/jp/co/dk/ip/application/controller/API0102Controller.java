package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0102Req;
import jp.co.dk.ip.application.response.API0102Res;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.model.SysUserInfoModel;
import jp.co.dk.ip.domain.service.API0102Service;

/**
 * user info getting API Controller
 */
@RestController
@RequestMapping("/api")
public class API0102Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0102Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0102Service api0102Service;

	@PostMapping("/API0102")
	private API0102Res agentDataProcess(@RequestBody @Valid API0102Req api0102Req, BindingResult bindingResult) {

		logger.debug("API0102Controller agentDataProcess");

		API0102Res api0102Res = new API0102Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0102Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {
			// data getting
			List<SysUserInfoModel> sysUserInfoModels = new ArrayList<SysUserInfoModel>();
			sysUserInfoModels = api0102Service.getSysUserInfoModel(
					api0102Req.getUserId(),
					api0102Req.getUserName());
			// response setting
			if (!Objects.equals(sysUserInfoModels, null)) {
				api0102Res.setResultCnt(sysUserInfoModels.size());
				api0102Res.setSysUserInfoModels(sysUserInfoModels);
				api0102Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001, 
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0102 + e.getMessage(), e);
			api0102Res = api0102Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0102 + e.getMessage(), e);
			api0102Res = api0102Service.getErrRes(
					new String[] {}, 
					Const.RESULT_CODE_9, 
					Const.E000005,
					Const.ERRRES_API0102);
		}

		return api0102Res;
	}
}
