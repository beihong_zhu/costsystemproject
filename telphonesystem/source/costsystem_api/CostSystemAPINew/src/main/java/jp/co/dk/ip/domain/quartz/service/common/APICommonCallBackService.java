package jp.co.dk.ip.domain.quartz.service.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import com.google.gson.Gson;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.HttpModel;

public class APICommonCallBackService {
	
	/**
	 * send request for calling back
	 * @param <T>
	 * 
	 * @return none
	 */
	public <T> HttpModel postAsync(
			String url,
			T t)
			throws InterruptedException, ExecutionException, ParseException, IOException {
		
		// 发送请求设置
		RequestConfig requestConfig = RequestConfig
				.custom()
				.setConnectTimeout(Const.CONN_TIME_OUT_30000)
				.setSocketTimeout(Const.SOCKET_TIME_OUT_50000)
				.setConnectionRequestTimeout(Const.CONN_REQUEST_TIME_OUT_1000)
				.build();

		// 配置io线程
		IOReactorConfig ioReactorConfig = IOReactorConfig
				.custom()
				.setIoThreadCount(Runtime.getRuntime().availableProcessors())
				.setSoKeepAlive(true)
				.build();

		// 设置连接池大小
		ConnectingIOReactor ioReactor = null;
		try {
			ioReactor = new DefaultConnectingIOReactor(ioReactorConfig);
		} catch (IOReactorException e) {
			e.printStackTrace();
		}
		PoolingNHttpClientConnectionManager connManager = new PoolingNHttpClientConnectionManager(ioReactor);
		connManager.setMaxTotal(Const.MAX_TOTAL_ROUTE_100);
		connManager.setDefaultMaxPerRoute(Const.MAX_TOTAL_ROUTE_100);

		final CloseableHttpAsyncClient client = HttpAsyncClients
				.custom()
				.setConnectionManager(connManager)
				.setDefaultRequestConfig(requestConfig)
				.build();

		// 构造请求
		HttpPost httpPost = new HttpPost(url);
		StringEntity entity = null;

		try {
//            String a = "{ \"index\": { \"_index\": \"test\", \"_type\": \"test\"} }\n" +
//                    "{\"name\": \"上海\",\"age\":33}\n";

			String requestData = Const.EMPTY_STRING;
			Gson gson = new Gson();
			requestData = gson.toJson(t);
			entity = new StringEntity(requestData);
			entity.setContentType("application/json;charset=UTF-8");
			httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
			httpPost.setEntity(entity);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		// make response
		HttpModel httpModel = new HttpModel();
		httpModel.setHttpPost(httpPost);
		httpModel.setClient(client);

		return httpModel;
	}
}
