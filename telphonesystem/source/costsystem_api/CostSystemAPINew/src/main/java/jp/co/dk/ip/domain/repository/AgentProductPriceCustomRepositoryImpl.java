package jp.co.dk.ip.domain.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OnShelfProductModel;


/**
 * . 渠道商信息取得
 */
@SuppressWarnings("unchecked")
public class AgentProductPriceCustomRepositoryImpl implements AgentProductPriceCustomRepository
{
	@PersistenceContext
	private EntityManager entityManager;
    
	@Override
	public List<OnShelfProductModel> findStockByOnshelfInfo(
			String agencyName,
			String value,
			String productType,
			String productStatus) 
	{
		
		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
        		+ " o.agent_reduce_name, "
                + " cs.product_name, "
                + " cs.product_type, "
                + " cs.sp_id, "
                + " cs.value, "
                + " cs.price, "
                + " cs.disaccount, "
                + " cs.check_price, "
                + " cs.check_cost, "
                + " cs.status  "
                + "FROM "
                + " agent_product_price cs "
                + "LEFT JOIN agents o "
                + "ON cs.agent_id = o.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";

		if (null != agencyName) {
			sql += "         AND o.agent_reduce_name = :agencyName ";
		}

		if (null != value) {
			sql += "         AND cs.value = :value ";
		}
		if (null != productType) {
			sql += "         AND cs.product_type = :productType ";
		}

		if (null != value) {
			sql += "         AND cs.status = :productStatus ";
		}
		// sql injection preventing

		agencyName = Utils.escapeSql(agencyName);
		value = Utils.escapeSql(value);
		productType = Utils.escapeSql(productType);
		productStatus = Utils.escapeSql(productStatus);

		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "AgentProductPrice.OnShelfProductModel");

		if (null != agencyName) {
			query.setParameter("agencyname", agencyName);
		}

		if (null != value) {
			query.setParameter("value", value);
		}
		
		if (null != productType) {
			query.setParameter("productType", productType);
		}

		if (null != productStatus) {
			query.setParameter("productStatus", productStatus);
		}
		
		List<OnShelfProductModel> list = query.getResultList();
		return list;
	}
}
