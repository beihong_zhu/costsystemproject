package jp.co.dk.ip.domain.quartz.service.authory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class APIAuthoryHttpClientService {
	
	/**
	 * send request to authory client
	 * @param <T>
	 * 
	 * @return authory client response
	 */
	public <T> void postAsyncClient(String url, T t, OrdersRepository ordersRepository)
			throws InterruptedException, ExecutionException, ParseException, IOException {

		// 发送请求设置
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(Const.CONN_TIME_OUT_30000)
				.setSocketTimeout(Const.SOCKET_TIME_OUT_50000)
				.setConnectionRequestTimeout(Const.CONN_REQUEST_TIME_OUT_1000).build();

		// 配置io线程
		IOReactorConfig ioReactorConfig = IOReactorConfig.custom()
				.setIoThreadCount(Runtime.getRuntime().availableProcessors()).setSoKeepAlive(true).build();

		// 设置连接池大小
		ConnectingIOReactor ioReactor = null;
		try {
			ioReactor = new DefaultConnectingIOReactor(ioReactorConfig);
		} catch (IOReactorException e) {
			e.printStackTrace();
		}
		PoolingNHttpClientConnectionManager connManager = new PoolingNHttpClientConnectionManager(ioReactor);
		connManager.setMaxTotal(Const.MAX_TOTAL_ROUTE_100);
		connManager.setDefaultMaxPerRoute(Const.MAX_TOTAL_ROUTE_100);

		final CloseableHttpAsyncClient client = HttpAsyncClients
				.custom()
				.setConnectionManager(connManager)
				.setDefaultRequestConfig(requestConfig)
				.build();

		// 构造请求
		HttpPost httpPost = new HttpPost(url);
		StringEntity entity = null;

		try {
//            String a = "{ \"index\": { \"_index\": \"test\", \"_type\": \"test\"} }\n" +
//                    "{\"name\": \"上海\",\"age\":33}\n";

			String requestData = Const.EMPTY_STRING;
			Gson gson = new Gson();
			requestData = gson.toJson(t);
			entity = new StringEntity(requestData);
			entity.setContentType("application/json;charset=UTF-8");
			httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
			httpPost.setEntity(entity);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// start
		client.start();

		// 异步请求
		RunCallBack runCallBack = new RunCallBack(url, ordersRepository);
		client.execute(httpPost, runCallBack);

//		// 响应数据获取
//        String responseData = Const.EMPTY_STRING;
//		while (true) {
//			try {
//				// TimeUnit.SECONDS.sleep(Const.COUNT_1);
//				Thread.sleep(Const.MAX_TOTAL_ROUTE_100);
//				responseData = runCallBack.output;
//				if (!Objects.equals(responseData, Const.EMPTY_STRING)) {
//					System.out.println(responseData);
//					// DB TODO
//					break;
//				}
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
	}

	/**
	 * run http request call back
	 * 
	 * @return none
	 */
	private class RunCallBack implements FutureCallback<HttpResponse> {

		private long start = System.currentTimeMillis();
		final CountDownLatch latch = new CountDownLatch(Const.COUNT_1);
		public String output = Const.EMPTY_STRING;

		private String url;
		private OrdersRepository ordersRepository;

		RunCallBack(String url, OrdersRepository ordersRepository) {
			this.url = url;
			this.ordersRepository = ordersRepository;
		}

		@Override
		public void completed(HttpResponse httpResponse) {

			try {
				output = EntityUtils.toString(httpResponse.getEntity(), Const.UNITCODE_TYPE.UTF_8);
//				System.out.println(
//						"cost is:" 
//				+ (System.currentTimeMillis() - start)
//				+ ":"
//				+ EntityUtils.toString(httpResponse.getEntity(), Const.UNITCODE_TYPE.UTF_8));

				// response process
//				APIAuthory001FakeRes apiAuthory001FakeRes = new APIAuthory001FakeRes();
//				if (!Objects.equals(output, Const.EMPTY_STRING)) {
//					Gson gson = new Gson();
//					apiAuthory001FakeRes = gson.fromJson(output, APIAuthory001FakeRes.class);
//				}
//				if (Objects.equals(apiAuthory001FakeRes.getCode(), Const.AUTHORY_RES_CODE.CODE_000)) {
//					AuthoryHttpResponseService.postAuthoryResponseSuccess(
//							this.url,
//							ordersRepository,
//							apiAuthory001FakeRes.getOrderid());	
//				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void failed(Exception e) {

			latch.countDown();
			System.err.println("cost is:" + (System.currentTimeMillis() - start) + ":" + e);
			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
		}

		@Override
		public void cancelled() {

			latch.countDown();
			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
		}
	}
	
	public <T> void postClient(
			String url,
			T t,
			String orderId,
			OrdersRepository ordersRepository) throws Exception {

		// 构造请求
		HttpPost httpPost = new HttpPost(url);
		StringEntity entity = null;

		// request setting
		String requestData = Const.EMPTY_STRING;
		Gson gson = new Gson();
		requestData = gson.toJson(t);
		entity = new StringEntity(requestData);
		entity.setContentType("application/json;charset=UTF-8");
		httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
		httpPost.setEntity(entity);

		// create request
		CloseableHttpClient httpClient = HttpClients.createDefault();
		// execute request
		CloseableHttpResponse response = httpClient.execute(httpPost);
		// get response
		String output = EntityUtils.toString(response.getEntity(), Const.UNITCODE_TYPE.UTF_8);
		System.out.println(output);
		// close httpclient
		response.close();
		httpClient.close();
		
//		// response process
//		APIAuthory002FakeRes apiAuthory002FakeRes = new APIAuthory002FakeRes();
//		if (!Objects.equals(output, Const.EMPTY_STRING)) {
//			apiAuthory002FakeRes = gson.fromJson(output, APIAuthory002FakeRes.class);
//		}
//		if (Objects.equals(apiAuthory002FakeRes.getCode(), Const.AUTHORY_RES_CODE.CODE_200)) {
//			AuthoryHttpResponseService.postAuthoryResponseSuccess(
//					url,
//					ordersRepository,
//					orderId);	
//		}
	}
}
