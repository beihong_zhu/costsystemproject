package jp.co.dk.ip.application.controller;

import java.util.Locale;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3010Req;
import jp.co.dk.ip.application.response.API3010Res;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3010Service;

/**
 * productInfo adding API Controller
 */
@RestController
@RequestMapping("/api")
public class API3010Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3010Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3010Service api3010Service;

	@PostMapping("/API3010")
	private API3010Res authoryDataProcess(@RequestBody @Valid API3010Req api3010Req, BindingResult bindingResult) {

		logger.debug("API3010Controller authoryDataProcess");

		API3010Res api3010Res = new API3010Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3010Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {
			// data getting
			api3010Service.addAuthory(api3010Req);
			
			// response setting
			api3010Res.setResultCnt(Const.COUNT_1);
			api3010Res.setResultCode(Const.RESULT_CODE_0);
			logger.info(
					messageSource.getMessage(
							Const.I000001, 
							new String[] {},
							Locale.forLanguageTag(settingContext.getLangZh())));
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0106 + e.getMessage(), e);
			api3010Res = api3010Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0106 + e.getMessage(), e);
			api3010Res = api3010Service.getErrRes(
					new String[] {}, 
					Const.RESULT_CODE_9, 
					Const.E000005,
					Const.ERRRES_API0106);
		}

		return api3010Res;
	}
}
