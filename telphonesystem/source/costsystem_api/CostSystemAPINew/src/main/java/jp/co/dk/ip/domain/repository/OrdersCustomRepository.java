package jp.co.dk.ip.domain.repository;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.domain.model.AgentSummaryModel;
import jp.co.dk.ip.domain.model.OrderManagementModel;

/**
 * . orders repository
 */
public interface OrdersCustomRepository {

	List<AgentSummaryModel> findOrderSummaryByOrderInfo(
			String agentName,
			String agentReduceName,
			String money,
			String startTime,
			String endTime);
	

	List<OrderManagementModel> findOrderByProductInfo(String orderorigin, String channelname, String agencyname,
			String agecnylimited, String locallimited, String channellimited, String telnumber, String productname,
			String providesevice, String teletype, String status, String ocrManufacturingDate);
		 
}
