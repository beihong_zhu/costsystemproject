package jp.co.dk.ip.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * . stock
 */
@Getter
@Setter
@AllArgsConstructor
public class AuthoryInfoModel {

	/**
	 * . 渠道商名
	 */
	private String authoryName;

	/**
	 * . 简称
	 */
	private String abbreviation;
}
