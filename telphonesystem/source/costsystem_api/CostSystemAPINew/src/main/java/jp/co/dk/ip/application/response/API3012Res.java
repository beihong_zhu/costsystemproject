package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.AuthoryInfoRes;
import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Response
 */
@Getter
@Setter
public class API3012Res extends BaseRes {

    // data process result
    public List<AuthoryInfoRes> authoryInfoResList;
}
