package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * APIレスポンスの共通項目
 */
@Getter
@Setter
public class DataRes {

    // bill id
    private String billId;
}
