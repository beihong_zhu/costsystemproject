package jp.co.dk.ip.application.request;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * agent search API Request
 */
@Getter
@Setter
public class API0110Req extends BaseReq {

	// 账户名
	@Length(max = 20, message = "{ECT010}")
	private String agentId;

	// 公司全称
	@Length(max = 20, message = "{ECT010}")
	private String agentName;

	// 公司简称
	@Length(max = 8, message = "{ECT010}")
	private String agentReduceName;
}