package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * balance charge API Response
 */
@Getter
@Setter
public class APIBalanceSelectRes extends BaseRes {

    // 充值余额
    private String balance;
}
