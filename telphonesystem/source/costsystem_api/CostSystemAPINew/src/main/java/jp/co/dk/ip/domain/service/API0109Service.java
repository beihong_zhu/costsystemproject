package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0109Req;
import jp.co.dk.ip.application.response.API0109Res;
import jp.co.dk.ip.application.response.data.CapitalInfoModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.CapitalInfoModel;
import jp.co.dk.ip.domain.repository.CapitalStatusRepository;

/**
 * capital search API Service
 */
@Service
@Transactional
public class API0109Service {

    private static final Logger logger = LoggerFactory.getLogger(API0109Service.class);

    @Autowired
    private CapitalStatusRepository capitalStatusRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * capital getting process
     *
     * @param api0109Req stock getting request
     * @return result
     */
	public List<CapitalInfoModelRes> getCapitalInfoModelResList(API0109Req api0109Req) throws Exception {

        logger.debug("API0109Service getCapitalInfoModelResList");

        try {
			List<CapitalInfoModel> capitalInfoModels = new ArrayList<CapitalInfoModel>();
			
			// capital info setting
			capitalInfoModels = capitalStatusRepository.findCapitalByCapitalInfo(
					api0109Req.getCorpId(),
					api0109Req.getAgentName(),
					api0109Req.getAgentReduceName(),
					api0109Req.getReqId(),
					api0109Req.getOrderId(),
					api0109Req.getBillId(),
					api0109Req.getStartTime(),
					api0109Req.getEndTime());
			logger.info(Const.FIND_DATA_COUNT + capitalInfoModels.size());
			
			// transfer capital data
			Date payTime = new Date();
			List<CapitalInfoModelRes> capitalInfoModelResList = new ArrayList<CapitalInfoModelRes>();
			for (CapitalInfoModel capitalInfoModel : capitalInfoModels) {
				CapitalInfoModelRes capitalInfoModelRes = new CapitalInfoModelRes();
				capitalInfoModelRes.setReqId(capitalInfoModel.getReqId());
				capitalInfoModelRes.setOrderId(capitalInfoModel.getOrderId());
				capitalInfoModelRes.setAgentReduceName(capitalInfoModel.getAgentReduceName());
				capitalInfoModelRes.setOrderForm(capitalInfoModel.getOrderForm());
				capitalInfoModelRes.setProductName(capitalInfoModel.getProductName());
				capitalInfoModelRes.setCapital(Double.valueOf(capitalInfoModel.getCapital()));
				capitalInfoModelRes.setPrice(Double.valueOf(capitalInfoModel.getPrice()));
				capitalInfoModelRes.setStatus(capitalInfoModel.getStatus());
				
				// pay time setting
				if (!Objects.equals(capitalInfoModel.getPayTime(), null)) {
					payTime = Utils.dateStringToDateMillSecondByUpper(capitalInfoModel.getPayTime());
	                String strPayTime = payTime != null ? Utils.dataToStringMillSecond(payTime) : null;
					capitalInfoModelRes.setPayTime(strPayTime);
				}
				
				capitalInfoModelResList.add(capitalInfoModelRes);
			}
			
            return capitalInfoModelResList;
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0109 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0109 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0109 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public API0109Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("API0109Service getErrRes");

        API0109Res api0109Res = new API0109Res();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        api0109Res.setResultCode(resultCode);
        api0109Res.setMessageCode(errorCode);
        api0109Res.setMessage(errMsg);
        return api0109Res;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return API0109Res response class
     */
    public API0109Res getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("API0109Service getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public API0109Res setErrRes(CustomHandleException e) {

        logger.debug("API0109Service setErrRes");

        API0109Res api0109Res = new API0109Res();
        api0109Res.setResultCode(e.getErrorMessage().getResultCode());
        api0109Res.setResultCnt(e.getErrorMessage().getResultCnt());
        api0109Res.setMessageCode(e.getErrorMessage().getMessageCode());
        api0109Res.setMessage(e.getErrorMessage().getMessage());
        return api0109Res;
    }
}
