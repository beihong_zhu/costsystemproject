package jp.co.dk.ip.application.request;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * capital record API Request
 */
@Getter
@Setter
public class API3002Req extends BaseReq {
	
	// 代理账户
	private String corpId;
	
	// 代理名称
	private String agentName;
	
	// 代理简称
	private String agentRecordName;
	
	// 代理流水
	private String reqId;

    // 交易流水 
    private String orderId;

    // 请求流水
    private String billId;

    // 交易方式 
    private String orderForm;

    // 交易类型 
    private String orderType;

    // 开始时间 
    private String startTime;

    // 结束时间 
    private String endTime;
}
