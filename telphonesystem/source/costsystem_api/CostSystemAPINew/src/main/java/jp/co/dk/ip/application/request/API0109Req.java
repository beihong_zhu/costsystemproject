package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * capital search API Request
 */
@Getter
@Setter
public class API0109Req extends BaseReq {

	// 代理账户
	@Length(max = 20, message = "{ECT010}")
	private String corpId;

	// 代理名称
	@Length(max = 20, message = "{ECT010}")
	private String agentName;

	// 代理简称
	@Length(max = 8, message = "{ECT010}")
	private String agentReduceName;

	// 代理流水
	@Length(max = 20, message = "{ECT010}")
	private String reqId;

	// 交易流水
	@Length(max = 20, message = "{ECT010}")
	private String orderId;

	// 渠道流水
	@Length(max = 20, message = "{ECT010}")
	private String billId;

	// 起始时间
	@NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String startTime;

	// 结束时间
	@NotEmpty(message = "{ECT008}")
	@Length(max = 100, message = "{ECT010}")
	private String endTime;
}