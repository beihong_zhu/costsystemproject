package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3011Req extends BaseReq {
	
	// 订单来源
	@Length(max = 20, message = "{ECT010}")
	private String orderorigin;

	// 渠道名称
	@Length(max = 20, message = "{ECT010}")
	private String channelname;

	// 代理名称
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String agencyname;
    
	// 代理流水
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String agecnylimited;
    
	// 本地流水
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String locallimited;
    
	// 渠道流水
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String channellimited;
    
    // 电话号码
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String telnumber;
    
    // 商品名称
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productname;
    
    // 提供商
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String providesevice;

    
    // 号码类型
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String teletype;    
    
    // 状态
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String status;    
    
    // 时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String ocrManufacturingDate;       
    
    
    
    
    
    
    
    
    
    
}
