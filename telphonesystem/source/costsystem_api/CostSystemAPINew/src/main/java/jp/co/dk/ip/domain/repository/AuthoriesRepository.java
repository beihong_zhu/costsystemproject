package jp.co.dk.ip.domain.repository;

import jp.co.dk.ip.domain.entity.Authories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * . authory repository
 */
@Repository
public interface AuthoriesRepository extends JpaRepository<Authories, String>, AuthoriesCustomRepository {

	// @Lock(value = LockModeType.PESSIMISTIC_WRITE)
	List<Authories> findByIsDeleted(int isDeleted);
}
