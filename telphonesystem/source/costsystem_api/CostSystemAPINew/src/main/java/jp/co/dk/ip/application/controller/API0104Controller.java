package jp.co.dk.ip.application.controller;

import java.util.Locale;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0104Req;
import jp.co.dk.ip.application.response.API0104Res;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0104Service;

/**
 * agent adding API Controller
 */
@RestController
@RequestMapping("/api")
public class API0104Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0104Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0104Service api0104Service;

	@PostMapping("/API0104")
	private API0104Res agentDataProcess(@RequestBody @Valid API0104Req api0104Req, BindingResult bindingResult) {

		logger.debug("API0104Controller agentDataProcess");

		API0104Res api0104Res = new API0104Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0104Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {
			// data getting
			api0104Service.addAgent(api0104Req);
			
			// response setting
			api0104Res.setResultCnt(Const.COUNT_1);
			api0104Res.setResultCode(Const.RESULT_CODE_0);
			logger.info(
					messageSource.getMessage(
							Const.I000001, 
							new String[] {},
							Locale.forLanguageTag(settingContext.getLangZh())));
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0104 + e.getMessage(), e);
			api0104Res = api0104Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0104 + e.getMessage(), e);
			api0104Res = api0104Service.getErrRes(
					new String[] {}, 
					Const.RESULT_CODE_9, 
					Const.E000005,
					Const.ERRRES_API0104);
		}

		return api0104Res;
	}
}
