package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.domain.model.data.OrderResultPostReq;
import lombok.Getter;
import lombok.Setter;

/**.
 * order post request Info
 */
@Getter
@Setter
public class OrderResultPostAgentModel {

    // order post request
    private OrderResultPostReq orderResultPostReq;
    
    // order post url
    private String url;
    
    // run (1-run, 0-no run)
    private String isRun;
}