package jp.co.dk.ip.domain.repository;

import jp.co.dk.ip.domain.entity.CapitalStatus;
import jp.co.dk.ip.domain.entity.CapitalStatusPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**.
 * agent capital repository
 */
@Repository
public interface CapitalStatusRepository extends JpaRepository<CapitalStatus, CapitalStatusPK>, CapitalStatusCustomRepository {

}
