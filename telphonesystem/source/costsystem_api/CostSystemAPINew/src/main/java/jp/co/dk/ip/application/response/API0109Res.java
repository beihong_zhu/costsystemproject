package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.BaseRes;
import jp.co.dk.ip.application.response.data.CapitalInfoModelRes;
import lombok.Getter;
import lombok.Setter;

/**
 * capital info API Response
 */
@Getter
@Setter
public class API0109Res extends BaseRes {

    // data process result
    public List<CapitalInfoModelRes> capitalInfoModels;
}
