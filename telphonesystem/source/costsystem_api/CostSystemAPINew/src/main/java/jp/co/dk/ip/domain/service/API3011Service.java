package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API3011Req;
import jp.co.dk.ip.application.response.API3011Res;
import jp.co.dk.ip.application.response.data.OrderManagementModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderManagementModel;
import jp.co.dk.ip.domain.repository.OrdersRepository;

/**
 * all agents summary API Service
 */
@Service
@Transactional
public class API3011Service {

	private static final Logger logger = LoggerFactory.getLogger(API3011Service.class);

	@Autowired
	private OrdersRepository ordersRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents summary
	 *
	 * @param api0112Req all agents summary
	 * @return result
	 */
	public List<OrderManagementModelRes> getAgentNamesResList(API3011Req api3011Req) throws Exception {

		logger.debug("API3011Service getAgentNamesResList");

		List<OrderManagementModelRes> orderManagementModelResList = new ArrayList<OrderManagementModelRes>();
		
		try {
			List<OrderManagementModel> orderManagementModels = new ArrayList<OrderManagementModel>();

			// agents data setting
			orderManagementModels = ordersRepository.findOrderByProductInfo(
					api3011Req.getOrderorigin(),
					api3011Req.getChannelname(),
					api3011Req.getAgencyname(),
					api3011Req.getAgecnylimited(),
					api3011Req.getLocallimited(),
					api3011Req.getChannellimited(),
					api3011Req.getTelnumber(),
					api3011Req.getProductname(),
					api3011Req.getProvidesevice(),
					api3011Req.getTeletype(),
					api3011Req.getStatus(),
					api3011Req.getOcrManufacturingDate()				
					);
			logger.info(Const.FIND_DATA_COUNT + orderManagementModels.size());

			// get all agents
			for (OrderManagementModel orderManagementModel : orderManagementModels) {
				OrderManagementModelRes orderManagementModelRes = new OrderManagementModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(orderManagementModel, orderManagementModelRes);
				orderManagementModelResList.add(orderManagementModelRes);
			}

			return orderManagementModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API3011Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API0112Service getErrRes");

		API3011Res api3011Res = new API3011Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api3011Res.setResultCode(resultCode);
		api3011Res.setMessageCode(errorCode);
		api3011Res.setMessage(errMsg);
		return api3011Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0112Res response class
	 */
	public API3011Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API0112Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API3011Res setErrRes(CustomHandleException e) {

		logger.debug("API0112Service setErrRes");

		API3011Res api3011Res = new API3011Res();
		api3011Res.setResultCode(e.getErrorMessage().getResultCode());
		api3011Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api3011Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api3011Res.setMessage(e.getErrorMessage().getMessage());
		return api3011Res;
	}
}
