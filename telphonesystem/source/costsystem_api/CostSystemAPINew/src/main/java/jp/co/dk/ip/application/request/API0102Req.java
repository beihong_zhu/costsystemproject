package jp.co.dk.ip.application.request;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * user info getting API Request
 */
@Getter
@Setter
public class API0102Req extends BaseReq {

	// user ID
	@Length(max = 10, message = "{ECT010}")
	@Pattern(regexp = "\\d*", message = "{ECT009}")
	private String userId;

	// user name
	@Length(max = 10, message = "{ECT010}")
	private String userName;
}