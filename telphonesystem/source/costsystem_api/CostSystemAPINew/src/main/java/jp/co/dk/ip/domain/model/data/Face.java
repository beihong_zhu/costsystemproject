package jp.co.dk.ip.domain.model.data;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * 人脸识别 detect data
 */
@Getter
@Setter
@AllArgsConstructor
public class Face {

    // face token
	@SerializedName("face_token")
    private String faceToken;

    // face rectangle
	@SerializedName("face_rectangle")
    private FaceRectangle faceRectangle;
}
