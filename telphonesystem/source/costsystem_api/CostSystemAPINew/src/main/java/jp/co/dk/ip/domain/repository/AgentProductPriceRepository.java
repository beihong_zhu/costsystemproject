package jp.co.dk.ip.domain.repository;

import jp.co.dk.ip.domain.entity.AgentProductPrice;
import jp.co.dk.ip.domain.entity.AgentProductPricePK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**.
 * agent product price repository
 */
@Repository
public interface AgentProductPriceRepository extends JpaRepository<AgentProductPrice, AgentProductPricePK>, AgentProductPriceCustomRepository {

    // @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    AgentProductPrice findByAgentIdAndProductIdAndStatusAndIsDeleted(String agentId, String productId, int status, int isDeleted);
}
