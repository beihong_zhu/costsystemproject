package jp.co.dk.ip.domain.model.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * order result post
 */
@Getter
@Setter
public class OrderResultPostReq {

    // 账号
    private String corpId;
    
    // 代理商流水号
    private String reqId;
    
    // 平台交易单号
    private String orderId;
    
    // 充值结果
    private String result;
    
    // 签名
    private String sign;
}