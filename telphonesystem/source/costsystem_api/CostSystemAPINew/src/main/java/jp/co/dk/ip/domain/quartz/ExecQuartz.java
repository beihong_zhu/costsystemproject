package jp.co.dk.ip.domain.quartz;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.CollectionUtils;
import com.alibaba.fastjson.JSONObject;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Authories;
import jp.co.dk.ip.domain.entity.CapitalStatus;
import jp.co.dk.ip.domain.entity.CapitalTotal;
import jp.co.dk.ip.domain.entity.Stocks;
import jp.co.dk.ip.domain.model.APIAgentOrderReqInfo;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.OrderResultPostAgentModel;
import jp.co.dk.ip.domain.model.OrderPostAuthory001Model;
import jp.co.dk.ip.domain.model.OrderPostAuthory002Model;
import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.model.OrderRedisModel;
import jp.co.dk.ip.domain.model.data.OrderPostAuthory001Req;
import jp.co.dk.ip.domain.model.data.OrderPostAuthory002Req;
import jp.co.dk.ip.domain.quartz.config.SpringContextJobUtil;
import jp.co.dk.ip.domain.quartz.service.agent.APIAgentOrderInfoCallBackService;
import jp.co.dk.ip.domain.quartz.service.agent.APIAgentOrderResultCallBackService;
import jp.co.dk.ip.domain.quartz.service.authory.APIAuthory001HttpClientService;
import jp.co.dk.ip.domain.quartz.service.authory.APIAuthory002HttpClientService;
import jp.co.dk.ip.domain.quartz.service.authory.AuthoryOrderPostService;
import jp.co.dk.ip.domain.quartz.service.common.APICallingService;
import jp.co.dk.ip.domain.quartz.service.common.APIDBService;
import jp.co.dk.ip.domain.quartz.service.common.APIRedisService;
import jp.co.dk.ip.domain.repository.AgentProductPriceRepository;
import jp.co.dk.ip.domain.repository.AgentsRepository;
import jp.co.dk.ip.domain.repository.AuthoriesRepository;
import jp.co.dk.ip.domain.repository.CapitalStatusRepository;
import jp.co.dk.ip.domain.repository.CapitalTotalRepository;
import jp.co.dk.ip.domain.repository.OrdersRepository;
import jp.co.dk.ip.domain.repository.StocksRepository;

/**
 * Job类
 */
public class ExecQuartz implements Job {
	
	@Autowired
	private OrdersRepository ordersRepository;
	
	@Autowired
	private StocksRepository stocksRepository;
	
	@Autowired
	private CapitalTotalRepository capitalTotalRepository;
	
	@Autowired
	private CapitalStatusRepository capitalStatusRepository;

	@Autowired
	private AuthoriesRepository authoriesRepository;
	
	@Autowired
	private AgentsRepository agentsRepository;
	
    @Autowired
    private AgentProductPriceRepository agentProductPriceRepository;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private RedisTemplate redisTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(ExecQuartz.class);
	
	public void abc() {
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {

		// interface initial
		this.ordersRepository = (OrdersRepository) SpringContextJobUtil.getBean("ordersRepository");
		this.stocksRepository = (StocksRepository) SpringContextJobUtil.getBean("stocksRepository");
		this.capitalTotalRepository = (CapitalTotalRepository) SpringContextJobUtil.getBean("capitalTotalRepository");
		this.capitalStatusRepository = (CapitalStatusRepository) SpringContextJobUtil.getBean("capitalStatusRepository");
		this.authoriesRepository = (AuthoriesRepository) SpringContextJobUtil.getBean("authoriesRepository");
		this.agentsRepository = (AgentsRepository) SpringContextJobUtil.getBean("agentsRepository");
		this.agentProductPriceRepository = (AgentProductPriceRepository) SpringContextJobUtil.getBean("agentProductPriceRepository");
		this.messageSource = (MessageSource) SpringContextJobUtil.getBean("messageSource");
		this.redisTemplate = (RedisTemplate) SpringContextJobUtil.getBean("redisTemplate");
		
		// micro charge process
		this.microChargeProcess();
		
		// return order info to agent
		this.agentOrderInfoSending();
		
		// insert agent data into DB
		this.insertData();

		// agent data process
		this.agentDataProcess();
		
		// return order result to agent
		this.agentOrderResultSending();
	}
	
    /**
     * micro charge process
     * @return none
     */
	private void microChargeProcess() {
		
		// insert micro charge data into DB
		if (Utils.getMicroChargeListSize() > Const.REQUEST_NUMBER_0) {
			List<OrderRedisMicroUserModel> orderRedisMicroUserModels = new ArrayList<OrderRedisMicroUserModel>();
			orderRedisMicroUserModels = Utils.chuZhanMicroChargeQueue();
			// redis data process
			APIRedisService apiRedisService = new APIRedisService();
			String key = Const.EMPTY_STRING;
			Integer size = 0;
			size = orderRedisMicroUserModels.size();
			Integer balance = 0;
			Integer redisBalance = 0;
			Integer listBalance = 0;
			for (OrderRedisMicroUserModel orderRedisMicroUserModel : orderRedisMicroUserModels) {
				if (Objects.equals(orderRedisMicroUserModel.getIsRun(), Const.NORUN)) {
					orderRedisMicroUserModel.setIsRun(Const.RUN);
					Boolean resultRedis = false;
					
					// redis主键获取
					key = apiRedisService.getMicroRedisKey(orderRedisMicroUserModel);
					
					// 余额累加
					JSONObject value = new JSONObject();
					value = apiRedisService.getValue(key, redisTemplate);
					if (!Utils.isEmpty(value)) {
						redisBalance = Utils.getIntegerByNull(value.getString("balance"));
					} else {
						redisBalance = 0;
					}
					if (!Utils.isEmpty(orderRedisMicroUserModel.getAmount())) {
						listBalance = Utils.getIntegerByNull(orderRedisMicroUserModel.getAmount());
					} else {
						listBalance = 0;
					}
					balance = redisBalance + listBalance;
					orderRedisMicroUserModel.setBalance(String.valueOf(balance));

					// redis update
		        	resultRedis = apiRedisService.redisProcess(
		        			orderRedisMicroUserModel,
		        			this.messageSource,
		        			this.redisTemplate,
		        			key);
		            if (resultRedis) {
		            	logger.info(new StringBuffer("micro charge process success").append(
		                		(JSONObject) JSONObject.toJSON(orderRedisMicroUserModels.get(Const.COUNT_0))).toString() + "," + key);
		            } else {
		            	logger.info(new StringBuffer("micro charge process failure").append(
		                		(JSONObject) JSONObject.toJSON(orderRedisMicroUserModels.get(Const.COUNT_0))).toString() + "," + key);
		            }	
				}
			}
		}
	}
	
	/**
	 * authory data process
	 * 
	 * @return none
	 */
	private void authoryDataProcess(AgentInfoModel agentInfoModel, List<String> stockKeys) {

		List<Authories> authories = new ArrayList<Authories>();
		authories = this.authoriesRepository.findByIsDeleted(Const.INT_DELETE_FLG_OFF);

		// authory data prority
		if (authories.size() > Const.COUNT_1) {
			authories = setProrityListOrder(authories);

			switch (authories.get(Const.COUNT_0).getAuthoryId()) {
			case Const.AUTHORY_ID.NO_1:
				// send http request to authory
				String requestKey001 = Const.EMPTY_STRING;
				requestKey001 = agentInfoModel.getProductId() + Const.AUTHORY_ID.NO_1;
				if (stockKeys.contains(requestKey001)) {
					// authory 001 post request setting
					OrderPostAuthory001Model orderPostAuthory001Model = new OrderPostAuthory001Model();
					orderPostAuthory001Model.setUrl(Const.AUTHORY_001_URL);
					OrderPostAuthory001Req orderPostAuthory001Req = new OrderPostAuthory001Req();
					orderPostAuthory001Req.setMerid(Const.AUTHORY_001_MERID);
					orderPostAuthory001Req.setType(Const.AUTHORY_001_TYPE_PHONE);
					orderPostAuthory001Req.setRechargeno(agentInfoModel.getNumber());
					orderPostAuthory001Req.setAmount(agentInfoModel.getMoney());
					orderPostAuthory001Req.setTs(agentInfoModel.getTs());
					orderPostAuthory001Req.setCallbackurl(Const.OUR_CALL_BACK_URL);
					orderPostAuthory001Req.setCarrier(agentInfoModel.getSpId());
//					orderPostAuthory001Req.setProvince(agentInfoModel.getProvId());
					orderPostAuthory001Req.setOrderid(agentInfoModel.getOrderId());
					
					// MD5 validation
		        	String md5Sign = Const.EMPTY_STRING;
		        	md5Sign = AuthoryOrderPostService.getAuthory001PostSign(
		        			orderPostAuthory001Req.getAmount(),
		        			orderPostAuthory001Req.getCallbackurl(),
		        			orderPostAuthory001Req.getMerid(),
		        			orderPostAuthory001Req.getOrderid(),
		        			orderPostAuthory001Req.getRechargeno(),
		        			orderPostAuthory001Req.getTs(),
		        			orderPostAuthory001Req.getType());
		        	orderPostAuthory001Req.setSign(md5Sign);
					
		        	// create authory 001 post data
					orderPostAuthory001Model.setOrderPostAuthory001Req(orderPostAuthory001Req);
					
					// authory 001 post
					APIAuthory001HttpClientService apiAuthory001HttpClientService =
							new APIAuthory001HttpClientService();
					apiAuthory001HttpClientService.sendRequestToAuthory001(
							orderPostAuthory001Model.getUrl(),
							orderPostAuthory001Model.getOrderPostAuthory001Req(),
							this.ordersRepository);

					// delete
					apiAuthory001HttpClientService.sendRequestToAuthory001(
							Const.AUTHORY_001_URL_OTHER,
							orderPostAuthory001Model.getOrderPostAuthory001Req(),
							this.ordersRepository);
				}
				break;
			case Const.AUTHORY_ID.NO_2:
				// send http request to authory 002
				String requestKey002 = Const.EMPTY_STRING;
				requestKey002 = agentInfoModel.getProductId() + Const.AUTHORY_ID.NO_2;
				if (stockKeys.contains(requestKey002)) {
					// authory 002 post request setting
					OrderPostAuthory002Model orderPostAuthory002Model = new OrderPostAuthory002Model();
					orderPostAuthory002Model.setUrl(Const.AUTHORY_002_URL);
					OrderPostAuthory002Req orderPostAuthory002Req = new OrderPostAuthory002Req();
					orderPostAuthory002Req.setCompanyId(Const.AUTHORY_002_COMPANYID);
					orderPostAuthory002Req.setPhone(agentInfoModel.getNumber());
					orderPostAuthory002Req.setOrderId(agentInfoModel.getOrderId());
					orderPostAuthory002Req.setProductId(agentInfoModel.getProductId());
					orderPostAuthory002Req.setCallbackUrl(Const.OUR_CALL_BACK_URL);
					orderPostAuthory002Req.setType(Const.AUTHORY_002_TYPE_1);

					// MD5 validation
		        	String md5Sign = Const.EMPTY_STRING;	
					md5Sign = AuthoryOrderPostService.getAuthory002PostSign(
							orderPostAuthory002Req.getCallbackUrl(),
							orderPostAuthory002Req.getCompanyId(),
							orderPostAuthory002Req.getOrderId(),
							orderPostAuthory002Req.getPhone(),
							orderPostAuthory002Req.getProductId(),
							String.valueOf(orderPostAuthory002Req.getType()));
					orderPostAuthory002Req.setSign(md5Sign);
					APIAuthory002HttpClientService apiAuthory002HttpClientService =
							new APIAuthory002HttpClientService();
					apiAuthory002HttpClientService.sendRequestToAuthory002(
							orderPostAuthory002Model.getUrl(),
							orderPostAuthory002Model.getOrderPostAuthory002Req(),
							this.ordersRepository);
				}
				break;
			default:
				break;
			}
		}
	}
	
	/**
	 * . set prority list order
	 * 
	 * @param authories original list
	 * @return authories
	 */
    public List<Authories> setProrityListOrder(List<Authories> authories) {

        logger.debug("ExecQuartz setProrityListOrder");

        // sort setting
        authories.stream().sorted(
        		Comparator.comparing(Authories::getPrority)
        		.thenComparing(Authories::getPrice)).collect(Collectors.toList());
        return authories;
    }
	
	/**
	 * agent data process
	 * 
	 * @return none
	 */
	private void agentDataProcess() {

		if (Utils.getOrderResultCallBackListSize() >= Const.REQUEST_NUMBER_3) {
			List<AgentInfoModel> agentInfoModels = new ArrayList<AgentInfoModel>();
			agentInfoModels = Utils.chuZhanOrderResultCallBackQueue();
			Integer size = Const.COUNT_0;
			size = agentInfoModels.size();
			for (AgentInfoModel agentInfoModel : agentInfoModels) {
				if (Objects.equals(agentInfoModel.getIsRun(), Const.PART_RUN)) {
					agentInfoModel.setIsRun(Const.RUN);
					
					// adjust money to agent
					updateMoneyToAgent(agentInfoModel);

					// products confirm
					List<String> stockKeys = new ArrayList<String>();
					stockKeys = confirmProduct(agentInfoModel);
					if (!CollectionUtils.isEmpty(stockKeys)) {
						// authory data process
						authoryDataProcess(agentInfoModel, stockKeys);
					}	
				}
			}
		}
	}
	
	/**
	 * send order info request to agent
	 * 
	 * @return none
	 */
	private void agentOrderInfoSending() {

		// insert agent data into DB
		if (Utils.getResListSize() >= Const.REQUEST_NUMBER_3) {
			List<APIAgentOrderReqInfo> apiAgentOrderReqInfos = new ArrayList<APIAgentOrderReqInfo>();
			apiAgentOrderReqInfos = Utils.chuZhanQueue();
			
			Integer size = Const.COUNT_0;
			size = apiAgentOrderReqInfos.size();
			OrderRedisModel orderRedisModel = null;
			for (APIAgentOrderReqInfo apiAgentOrderReqInfo : apiAgentOrderReqInfos) {
				if (Objects.equals(apiAgentOrderReqInfo.getIsRun(), Const.NORUN)) {
					apiAgentOrderReqInfo.setIsRun(Const.RUN);
					
					// 获取回调函数信息
					APICallingService apiCallingService = new APICallingService();
					orderRedisModel = new OrderRedisModel();
					orderRedisModel = apiCallingService.getOrderInfoPostAgentModel(
							apiAgentOrderReqInfo,
							this.agentsRepository,
							this.agentProductPriceRepository,
							this.messageSource,
							this.redisTemplate);

					// send order info request to agent
					APIAgentOrderInfoCallBackService apiAgentOrderInfoCallBackService =
							new APIAgentOrderInfoCallBackService();
					apiAgentOrderInfoCallBackService.sendOrderInfoRequestToAgent(
							orderRedisModel.getUrl(),
							orderRedisModel.getOrderInfoPostReq(),
							this.ordersRepository);
				}
			}
		}
	}
	
	/**
	 * send order result request to agent
	 * 
	 * @return none
	 */
	private void agentOrderResultSending() {

		// insert agent order result data into DB
		if (Utils.getOrderResultCallBackListSize() >= Const.REQUEST_NUMBER_3) {
			List<OrderResultPostAgentModel> orderResultPostAgentModels = new ArrayList<OrderResultPostAgentModel>();
			orderResultPostAgentModels = Utils.chuZhanOrderResultCallBackQueue();
			
			Integer size = Const.COUNT_0;
			size = orderResultPostAgentModels.size();
			for (OrderResultPostAgentModel orderResultPostAgentModel : orderResultPostAgentModels) {
				if (Objects.equals(orderResultPostAgentModel.getIsRun(), Const.PART_RUN)) {
					orderResultPostAgentModel.setIsRun(Const.RUN);

					// send order result request to agent
					APIAgentOrderResultCallBackService apiAgentOrderResultCallBackService
					 = new APIAgentOrderResultCallBackService();
					apiAgentOrderResultCallBackService.sendOrderResultRequestToAgent(
							orderResultPostAgentModel.getUrl(),
							orderResultPostAgentModel.getOrderResultPostReq(),
							this.ordersRepository);
				}
			}
		}
	}
	
	/**
	 * confirm product
	 * 
	 * @return none
	 */
	private List<String> confirmProduct(AgentInfoModel agentInfoModel) {

		List<String> stockKeys = new ArrayList<String>(); 		
		
		// product validation
		List<Stocks> stocks = new ArrayList<Stocks>(); 
		stocks = this.stocksRepository.findByProductIdAndStatusAndIsDeleted(
				agentInfoModel.getProductId(),
				Const.ON_SALE_STATUS.ON,
				Const.INT_DELETE_FLG_OFF);
		String stockKey = Const.EMPTY_STRING;
		if (!CollectionUtils.isEmpty(stocks)) {
			for (Stocks stock : stocks) {
				stockKey = stock.getProductId() + stock.getAuthoryId();
				stockKeys.add(stockKey);
			}
		}
		return stockKeys;
	}
	
    /**
     * insert data
     * @return none
     */
	private void insertData() {
		
		// insert agent data into DB
		if (Utils.getOrderInfoCallBackListSize() >= Const.REQUEST_NUMBER_3) {
			List<AgentInfoModel> agentInfoModels = new ArrayList<AgentInfoModel>();
			Integer size = Const.COUNT_0;
			agentInfoModels = Utils.chuZhanOrderInfoCallBackQueue();
			size = agentInfoModels.size();
			OrderRedisModel orderRedisModel = null;
			for (AgentInfoModel agentInfoModel : agentInfoModels) {
				if (Objects.equals(agentInfoModel.getIsRun(), Const.NORUN)) {
					agentInfoModel.setIsRun(Const.PART_RUN);
					// create insert object
					orderRedisModel = new OrderRedisModel();
					Utils.copyPropertiesIgnoreNull(agentInfoModel, orderRedisModel);
					orderRedisModel.setStatus(Const.AGENT_DATA_POST_STATUS_1);

					// redis insert
					Boolean resultRedis = true;
					APIRedisService apiRedisService = new APIRedisService();
					String key = Const.EMPTY_STRING;
					key = apiRedisService.getAgentRedisKey(orderRedisModel);
					resultRedis = apiRedisService.redisProcess(orderRedisModel, this.messageSource, this.redisTemplate,
							key);
					if (resultRedis) {
						logger.info(new StringBuffer("redis insert redisProcess success").append(orderRedisModel)
								.toString());
					} else {
						logger.info(new StringBuffer("redis insert redisProcess failure").append(orderRedisModel)
								.toString());
					}

					// order table insert
					Boolean resultOrder = true;
					String user = Const.EMPTY_STRING;
					user = orderRedisModel.getCorpId();
					APIDBService apiDBService = new APIDBService();
					resultOrder = apiDBService.orderProcess(orderRedisModel, user, this.ordersRepository);
					if (resultOrder) {
						logger.info(new StringBuffer("order table insert redisProcess success")
								.append((JSONObject) JSONObject.toJSON(orderRedisModel)).toString() + "," + user);
					} else {
						logger.info(new StringBuffer("order table insert redisProcess failure")
								.append((JSONObject) JSONObject.toJSON(orderRedisModel)).toString() + "," + user);
					}

					// enter into business queue
					Utils.jinZhanOrderResultCallBackQueue(agentInfoModel);
				}
			}
		}
	}
	
	/**
	 * update money to agent
	 * 
	 * @return none
	 */
	private void updateMoneyToAgent(AgentInfoModel agentInfoModel) {

		// get money info from agent
		CapitalTotal capitalTotal = new CapitalTotal();
		capitalTotal = this.capitalTotalRepository.findByAgentId(agentInfoModel.getCorpId());
		
		// update info to agent
		CapitalStatus capitalStatus = new CapitalStatus();
		capitalStatus.setAgentId(agentInfoModel.getCorpId());
		capitalStatus.setOrderId(agentInfoModel.getOrderId());
		capitalStatus.setMoney(Integer.valueOf(agentInfoModel.getMoney()));
		capitalStatus.setCapitalStatus(capitalTotal.getCapitalStatus());
		capitalStatus.setCapitalWarn(capitalTotal.getCapitalWarn());
		// capital caculating
		Double capital = Const.COUNT_0_000;
		if (Objects.equals(capitalStatus.getCapitalStatus(), Const.CAPITAL_STATUS.ONE)) {
			capital = capitalTotal.getCapital() - Double.valueOf(agentInfoModel.getPrice());
			capitalStatus.setCapital(capital);
			capitalTotal.setCapital(capital);
		} else if (Objects.equals(capitalStatus.getCapitalStatus(), Const.CAPITAL_STATUS.TWO)) {
			capital = capitalTotal.getCapital() + Double.valueOf(agentInfoModel.getPrice());
			capitalStatus.setCapital(capital);
			capitalTotal.setCapital(capital);
		}
		
    	// redis insert
    	Boolean resultRedis = true;
    	APIRedisService apiRedisService = new APIRedisService();
    	String key = Const.EMPTY_STRING;
    	OrderRedisModel orderRedisModel = new OrderRedisModel();
    	key = apiRedisService.getAgentRedisKey(orderRedisModel, agentInfoModel);
    	resultRedis = apiRedisService.redisProcess(
    			capitalStatus,
    			this.messageSource,
    			this.redisTemplate,
    			key);
    	resultRedis = apiRedisService.redisProcess(
    			capitalTotal,
    			this.messageSource,
    			this.redisTemplate,
    			key);
        if (resultRedis) {
        	logger.info(new StringBuffer("redis insert redisProcess success").append(capitalStatus).toString());
        } else {
        	logger.info(new StringBuffer("redis insert redisProcess failure").append(capitalStatus).toString());	            	
        }

		CapitalStatus getCapitalStatus = new CapitalStatus();
		CapitalTotal getCapitalTotal = new CapitalTotal();
		getCapitalStatus = capitalStatusRepository.saveAndFlush(capitalStatus);
		getCapitalTotal = capitalTotalRepository.saveAndFlush(capitalTotal);
        if (!Utils.isEmpty(getCapitalStatus)) {
        	logger.info(new StringBuffer("capital_status table insert redisProcess success").append(capitalStatus).toString());
        } else {
        	logger.info(new StringBuffer("capital_status table insert redisProcess failure").append(capitalStatus).toString());	            	
        }
        if (!Utils.isEmpty(getCapitalTotal)) {
        	logger.info(new StringBuffer("capital_total table insert redisProcess success").append(capitalTotal).toString());
        } else {
        	logger.info(new StringBuffer("capital_total table insert redisProcess failure").append(capitalTotal).toString());	            	
        }
	}
}
