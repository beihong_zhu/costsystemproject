package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception API Request
 */
@Getter
@Setter
public class APIAgentOrderResultReq extends BaseReq {

	// 客户端账号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 32, message = "{ECT010}")
	private String corpId;

	// 代理商流水号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 50, message = "{ECT010}")
	private String reqId;
	
    // 订单编号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String orderId;
    
    // 签名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String sign;
}
