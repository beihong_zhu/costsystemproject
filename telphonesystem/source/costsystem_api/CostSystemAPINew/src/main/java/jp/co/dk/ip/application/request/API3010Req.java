package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3010Req extends BaseReq {
	
	// 代理商公司全称
	@Length(max = 20, message = "{ECT010}")
	private String channelAccount;

	// 代理商公司
	@Length(max = 8, message = "{ECT010}")
	private String costType;

	// 代理商充值面额
    @NotEmpty(message = "{ECT008}")
    @Length(max = 5, message = "{ECT010}")
    private String provideSevice;
    
	// 代理商起始时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String teletype;
    
 // 代理商起始时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String province;
    
	// 代理商结束时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String address;
    
	// 代理商结束时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productValue;
    
	// 代理商结束时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productCode;
    
	// 代理商结束时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productName;
    
	
}
