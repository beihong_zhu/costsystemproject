package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3004Req;

import jp.co.dk.ip.application.response.API3004Res;
import jp.co.dk.ip.application.response.data.AgentInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3004Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API3004Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3004Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3004Service api3004Service;

	@PostMapping("/API3004")
	private API3004Res agentSummaryProcess(@RequestBody @Valid API3004Req api3004Req, BindingResult bindingResult) {

		logger.debug("API3004Controller agentSummaryProcess");

		API3004Res api3004Res = new API3004Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3004Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<AgentInfoModelRes> agentInofModelResList = new ArrayList<AgentInfoModelRes>();
			agentInofModelResList = api3004Service.getAgentNamesResList(api3004Req);
			
			// response setting
			if (!Objects.equals(agentInofModelResList, null)) {
				api3004Res.setResultCnt(agentInofModelResList.size());
				api3004Res.setAgentInfoModelResList(agentInofModelResList);
				api3004Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API3004 + e.getMessage(), e);
			api3004Res = api3004Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API3004 + e.getMessage(), e);
			api3004Res = api3004Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0112);
		}

		return api3004Res;
	}
}
