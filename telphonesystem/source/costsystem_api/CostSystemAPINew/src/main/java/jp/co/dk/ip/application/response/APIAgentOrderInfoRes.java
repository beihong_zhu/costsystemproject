package jp.co.dk.ip.application.response;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception API Response
 */
@Getter
@Setter
public class APIAgentOrderInfoRes extends BaseRes {

    // 代理商账号
    private String corpId;
    
    // 代理商流水号
    private String reqId;
    
    // 平台交易单号(最大长度:20 必填:N)
    private String orderId;
    
    // 时间戳，格式为：yyyyMMddHHmmss
    private String ts;
    
    // 价格（以元为单位，精确到小数点后4位）(最大长度:15 必填:N)
    private String price;
}
