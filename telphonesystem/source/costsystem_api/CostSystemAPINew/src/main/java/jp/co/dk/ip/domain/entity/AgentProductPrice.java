package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.OnShelfProductModel;
import lombok.Getter;
import lombok.Setter;

/**
 * . agent_product_price
 */
@Getter
@Setter
@Entity
@Table(name = "agent_product_price")
@IdClass(AgentProductPricePK.class)
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "AgentProductPrice.OnShelfProductModel", classes = {
		@ConstructorResult(targetClass = OnShelfProductModel.class, columns = {
				@ColumnResult(name = "agent_reduce_name", type = String.class),
				@ColumnResult(name = "product_name", type = String.class),
				@ColumnResult(name = "product_type", type = String.class),
				@ColumnResult(name = "sp_id", type = String.class),
				@ColumnResult(name = "value", type = String.class),
				@ColumnResult(name = "price", type = String.class),
				@ColumnResult(name = "disaccount", type = String.class),
				@ColumnResult(name = "check_price", type = String.class),
				@ColumnResult(name = "check_cost", type = String.class),
				@ColumnResult(name = "status", type = String.class)
				})
		})
public class AgentProductPrice {

	/**
	 * . 代理商ID
	 */
	@Id
	@Column(name = "agent_id")
	private String agentId;

	/**
	 * . 商品ID
	 */
	@Id
	@Column(name = "product_id")
	private String productId;
	
	/**
	 * . 商品名字
	 */
	@Id
	@Column(name = "product_name")
	private String productName;

	/**
	 * . 运营商
	 */
	@Column(name = "sp_id")
	private String spId;


	/**
	 * . 面值
	 */
	@Column(name = "value")
	private Integer value;

	/**
	 * . 价格
	 */
	@Column(name = "price")
	private Double price;
	

	/**
	 * . 折扣
	 */
	@Column(name = "disaccount")
	private String disaccount;
	
	
	/**
	 * . 商品类型
	 */
	@Column(name = "product_type")
	private String productType;


	/**
	 * . 核价格
	 */
	@Column(name = "check_price")
	private String checkPrice;
	
	/**
	 * . 核成本
	 */
	@Column(name = "check_cost")
	private String checkCost;

	/**
	 * . 状态
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
