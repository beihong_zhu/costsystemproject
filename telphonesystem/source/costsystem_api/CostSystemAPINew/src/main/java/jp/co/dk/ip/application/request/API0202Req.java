package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all stock management API Request
 */
@Getter
@Setter
public class API0202Req extends BaseReq {
	
	// 渠道名字
	@Length(max = 20, message = "{ECT010}")
	private String channelname;

	// 面值
	@Length(max = 8, message = "{ECT010}")
	private String value;

	// 商品类型
    @NotEmpty(message = "{ECT008}")
    @Length(max = 5, message = "{ECT010}")
    private String producttype;
    
	// 商品状态
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String productstatus;
    
	
}
