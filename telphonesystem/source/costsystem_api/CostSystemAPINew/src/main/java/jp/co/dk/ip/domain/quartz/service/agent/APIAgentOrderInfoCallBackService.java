package jp.co.dk.ip.domain.quartz.service.agent;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import javax.transaction.Transactional;

import org.apache.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.dk.ip.application.response.APIAgentOrderInfoCallBackRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.AgentProductPrice;
import jp.co.dk.ip.domain.entity.Agents;
import jp.co.dk.ip.domain.model.APIAgentOrderReqInfo;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.data.OrderInfoPostReq;
import jp.co.dk.ip.domain.repository.AgentProductPriceRepository;
import jp.co.dk.ip.domain.repository.AgentsRepository;
import jp.co.dk.ip.domain.repository.OrdersRepository;

/**
 * agent order info data reception API Service
 */
@Service
@Transactional
public class APIAgentOrderInfoCallBackService {

    private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderInfoCallBackService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
	
	/**
	 * send order info request to agent
	 * 
	 * @return none
	 */
	@SuppressWarnings("unused")
	public void sendOrderInfoRequestToAgent(
			String url,
			OrderInfoPostReq orderInfoPostReq,
			OrdersRepository ordersRepository) {

		RunAgentOrderInfoCallBackService runAgentOrderInfoCallBackService = new RunAgentOrderInfoCallBackService();
		try {
			runAgentOrderInfoCallBackService.postAgentAsync(
					url,
					orderInfoPostReq,
				    ordersRepository);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public OrderInfoPostReq orderInfoPostReqProcess(
			AgentsRepository agentsRepository,
			AgentProductPriceRepository agentProductPriceRepository,
			APIAgentOrderReqInfo apiAgentOrderReqInfo) throws Exception {
		
        logger.debug("APIAgentOrderInfoCallBackService agentDataProcess");
        
        OrderInfoPostReq orderInfoPostReq = new OrderInfoPostReq();
        
        try {
			// agent activity validation
			String interfaceStatus = Const.EMPTY_STRING;
			Agents agent = new Agents();
			agent = agentsRepository.findByAgentIdAndIsDeleted(apiAgentOrderReqInfo.getCorpId(), Const.INT_DELETE_FLG_OFF);
			if (StringUtils.isEmpty(agent)) {
				// response error
				orderInfoPostReq = null;
				return orderInfoPostReq;
			} else {
				interfaceStatus = agent.getInterfaceStatus();
				if (Objects.equals(interfaceStatus, Const.ON_INTERFACE_STATUS.OFF)) {
					// response error
					orderInfoPostReq = null;
					return orderInfoPostReq;
				}
			}
        	
			// transfer request object
			AgentInfoModel agentInfoModel = new AgentInfoModel();
			Utils.copyPropertiesIgnoreNull(apiAgentOrderReqInfo, agentInfoModel);
			
			// get order id
			String orderId = Const.EMPTY_STRING;
			String nowDate = Const.EMPTY_STRING;
			Date date = new Date();
			nowDate = Utils.dataToStringMillSecond(date);
			Random r = new Random();
			int end = Const.COUNT_0;
			end = r.nextInt(Const.RANDOM_89);
			end = end + Const.RANDOM_10;
			orderId = Const.ORDER_ID_D + nowDate + String.valueOf(end);
			agentInfoModel.setOrderId(orderId);

			// get product id
			String productId = Const.EMPTY_STRING;
			productId = agentInfoModel.getSpId() + Const.UNDERBAR_DOUBLE_QUOTATION + agentInfoModel.getMoney();
			agentInfoModel.setProductId(productId);
			
			// get order price
			Double price = Const.COUNT_0_000;
			AgentProductPrice agentProductPrice = new AgentProductPrice();
			agentProductPrice = agentProductPriceRepository.findByAgentIdAndProductIdAndStatusAndIsDeleted(
					agentInfoModel.getCorpId(),
					productId,
					Const.ON_SALE_STATUS.ON,
					Const.INT_DELETE_FLG_OFF);
			if (StringUtils.isEmpty(agentProductPrice)) {
				// response error
				orderInfoPostReq = null;
				return orderInfoPostReq;
			} else {
				price = agentProductPrice.getPrice();
				agentInfoModel.setPrice(String.valueOf(price));
			}

			// enter into queue
			Utils.jinZhanOrderInfoCallBackQueue(agentInfoModel);

			// response setting
			orderInfoPostReq.setCorpId(agentInfoModel.getCorpId());
			orderInfoPostReq.setReqId(agentInfoModel.getReqId());
			orderInfoPostReq.setOrderId(orderId);
			orderInfoPostReq.setPrice(String.valueOf(price));
			
			return orderInfoPostReq;
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENTORDERINFO + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENTORDERINFO + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
        } catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
        	message = messageSource.getMessage(
        			Const.E000016, 
        			new String[] {},
        			Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENTORDERINFO + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000016, message);
            throw new CustomHandleException(errorRes);	
        }
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAgentOrderInfoCallBackRes getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("APIAgentOrderInfoCallBackService getErrRes");

        APIAgentOrderInfoCallBackRes apiAgentOrderInfoCallBackRes = new APIAgentOrderInfoCallBackRes();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiAgentOrderInfoCallBackRes.setResultCode(resultCode);
        apiAgentOrderInfoCallBackRes.setMessageCode(errorCode);
        apiAgentOrderInfoCallBackRes.setMessage(errMsg);
        return apiAgentOrderInfoCallBackRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return apiAgentRes response class
     */
    public APIAgentOrderInfoCallBackRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAgentOrderInfoCallBackService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAgentOrderInfoCallBackRes setErrRes(CustomHandleException e) {

        logger.debug("APIAgentOrderInfoCallBackService setErrRes");

        APIAgentOrderInfoCallBackRes apiAgentOrderInfoCallBackRes = new APIAgentOrderInfoCallBackRes();
        apiAgentOrderInfoCallBackRes.setResultCode(e.getErrorMessage().getResultCode());
        apiAgentOrderInfoCallBackRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiAgentOrderInfoCallBackRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiAgentOrderInfoCallBackRes.setMessage(e.getErrorMessage().getMessage());
        return apiAgentOrderInfoCallBackRes;
    }
}
