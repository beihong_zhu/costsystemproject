package jp.co.dk.ip.domain.quartz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import jp.co.dk.ip.domain.quartz.ExecQuartz;

/**
 * Quartz配置类
 *
 *
 */
@Configuration
public class QuartzConfig {
	
//	@Autowired
//	private SetJobFactory setJobFactory;
	
	/**
	 * 1.创建Job对象
	 */
	@Bean
	public JobDetailFactoryBean jobDetailFactoryBean(){
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		//关联我们自己的Job类
		factory.setJobClass(ExecQuartz.class);
		return factory;
	}
	
//	/**
//	 * 2.创建Trigger对象
//	 * 简单的Trigger
//	 */
//	@Bean
//	public SimpleTriggerFactoryBean simpleTriggerFactoryBean(JobDetailFactoryBean jobDetailFactoryBean){
//		SimpleTriggerFactoryBean factory = new SimpleTriggerFactoryBean();
//		//关联JobDetail对象
//		factory.setJobDetail(jobDetailFactoryBean.getObject());
//		//该参数表示一个执行的毫秒数
//		factory.setRepeatInterval(2000);
//		//重复次数
//		factory.setRepeatCount(5);
//		return factory;
//	}
	
	/**
	 * Cron Trigger
	 */
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBean(JobDetailFactoryBean jobDetailFactoryBean){
		CronTriggerFactoryBean factory = new CronTriggerFactoryBean();
		factory.setJobDetail(jobDetailFactoryBean.getObject());
		//设置触发时间
		factory.setCronExpression("0/20 * * * * ?");
		return factory;
	}
	
	/**
	 * 3.创建Scheduler对象
	 */
	@Bean(name = "schedulerFactory")
	public SchedulerFactoryBean schedulerFactoryBean(CronTriggerFactoryBean cronTriggerFactoryBean){
		SchedulerFactoryBean factory = new SchedulerFactoryBean();
		//关联trigger
		factory.setTriggers(cronTriggerFactoryBean.getObject());
		
		// factory.setJobFactory(setJobFactory);
		
		return factory;
	}
	
//	@Bean(name = "schedulerFactory")
//	public SchedulerFactoryBean schedulerFactoryBean() {
//		SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
//		factoryBean.setJobFactory(setJobFactory);
//		return factoryBean;
//	}
	
//	@Bean(name = "scheduler")
//	public Scheduler scheduler() {
//		return schedulerFactoryBean().getScheduler();
//	}
}
