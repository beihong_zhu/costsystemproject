package jp.co.dk.ip;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.HttpUtils;


public class TestMain {

	public static void main(String[] args) {
		
		String url = Const.EMPTY_STRING;
		
		HttpUtils.invoke(url); //异步请求
		String result = "error";
		
//		//用户认证
//		url = "http://localhost:8088/auth/authoriz.do?param=eyJ1aWQiOiJ3ZWJtYXN0ZXIiLCJwd2QiOiJjaGluYXNlcnZlXzIwMTQifQ==";
//		result = httpClientGet(url);
//		System.out.println(result);
//	
//		url = "http://localhost:8088/auth/authoriz.do";
//		NameValuePair[] param = { new NameValuePair("param", "eyJ1aWQiOiJ3ZWJtYXN0ZXIiLCJwd2QiOiJjaGluYXNlcnZlXzIwMTQifQ==")};  
//		result = httpClientPost(url,param);
//		System.out.println(result);
//
//		//展品注册
//		url = "http://localhost:8088/exhibit/item.do?param=eyJjb2RlIjoiMTExMTEwMDAwMSIsImNvZGUxIjoiWjAwMTAiLCJjb2RlMiI6IjAwMSIsImNvZGUzIjoiMDAxIiwiaW50cm8iOiLlsZXlk4Hku4vnu43lhoXlrrkt566A5LuLIiwibmFtZSI6IuWxleWTgeWQjeensHNkIiwidHlwZSI6MSwidXBfdHlwZSI6MSwicGFnZV91cmwiOiJodHRwOi8vd3d3LmJhaWR1LmNvbSIsImltZ191cmwiOiJodHRwOi8vd3d3LmJhaWR1LmNvbSJ9";
//		result = httpClientGet(url);
//		System.out.println(result);*/
//		
//		url = "http://localhost:8088/exhibit/item.do";
//		add:eyJjb2RlIjoiMTExMTEwMDAwMSIsImNvZGUxIjoiWjAwMTAiLCJjb2RlMiI6IjAwMSIsImNvZGUzIjoiMDAxIiwiaW50cm8iOiLlsZXlk4Hku4vnu43lhoXlrrkt566A5LuLIiwibmFtZSI6Iuavm+S4u+W4rS3muZbljZciLCJ0eXBlIjoxLCJ1cF90eXBlIjowLCJwYWdlX3VybCI6Imh0dHA6Ly93d3cuYmFpZHUuY29tIiwiaW1nX3VybCI6Imh0dHA6Ly93d3cuYmFpZHUuY29tIn0=
//		
//		NameValuePair[] param1 = { new NameValuePair("param", "eyJjb2RlIjoiWjAwMTAwMDAxOSIsImNvZGUxIjoiWjAwMTAiLCJjb2RlMiI6IjAwMSIsImNvZGUzIjoiMDAxIiwiaW50cm8iOiLlsZXlk4Hku4vnu43lhoXlrrkt566A5LuLIiwibmFtZSI6Iuavm+S4u+W4rS3muZbljZciLCJ0eXBlIjoxLCJ1cF90eXBlIjowLCJwYWdlX3VybCI6Imh0dHA6Ly93d3cuYmFpZHUuY29tIiwiaW1nX3VybCI6Imh0dHA6Ly93d3cuYmFpZHUuY29tIn0=")};  
//		result = httpClientPost(url,param1);
//		System.out.println(result);
//
//		//展品-附件
//		String URLName = "http://pica.nipic.com/2007-11-12/20071112133257795_2.jpg";
//		String imageData = ImageUtil.dispose(URLName,1024);
//		System.out.println(imageData);
//		byte[] imgData = Base64.decode(imageData);
//		ImageUtil.saveToFile(imgData, "D://aa.jpg");
//		
//		String files = "{\"id\":15010269,\"code\":\"1111100001\",\"type\":\"jpg\",\"file\":\""+imageData+"\"}";
//		
//		url = "http://localhost:8088/exhibit/itemdata.do?param="+ImageUtil.getBASE64Encoder(files);
//		result = httpClientGet(url);
//		System.out.println(result);
//		
//		url = "http://localhost:8088/exhibit/itemdata.do";
//		NameValuePair[] param2 = { new NameValuePair("param",ImageUtil.getBASE64Encoder(files))};  
//		result = httpClientPost(url,param2);
//		System.out.println(result);
//		
//		url = Const.EMPTY_STRING;
	}
	
	/**
	 * HttpClinet发送请求并结束返回值
	 * @param url
	 * @return json_str
	 * */
	public static String httpClientGet(String url){
		String json_str="";
		try {
			HttpClient client=new HttpClient();
			HttpMethod method=new GetMethod(url);
			client.executeMethod(method);
			json_str=method.getResponseBodyAsString();
			method.releaseConnection(); //释放连接
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json_str;
	}
	
	/**
	 * HttpClinet发送请求并结束返回值
	 * @param url
	 * @return json_str
	 * */
	public static String httpClientPost(String url,NameValuePair[] param){
		String json_str = Const.EMPTY_STRING;
		
		try {
			HttpClient client = new HttpClient(); 
			PostMethod method = new PostMethod(url);  
			method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8"); 
			//NameValuePair[] param = { new NameValuePair("param", "eyJ1aWQiOiJ3ZWJtYXN0ZXIiLCJwd2QiOiJjaGluYXNlcnZlXzIwMTQifQ==")};  
			method.setRequestBody(param);  
			int statusCode = client.executeMethod(method);  
			json_str = method.getResponseBodyAsString();
			System.out.println(statusCode);  
			method.releaseConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json_str;
	}
	
	
	
	
	
	/**
	 * HttpClinet发送请求并结束返回值
	 * @param url
	 * @return json_str
	 * */
	public static String httpClientPost1(String url){
		String json_str="";
		try {
			HttpClient httpClient=new HttpClient();
			PostMethod postMethod = new PostMethod(url);
			// 填入各个表单域的值
			NameValuePair[] data = { new NameValuePair("id", "youUserName"),new NameValuePair("passwd", "yourPwd") };
			// 将表单的值放入postMethod中
			postMethod.setRequestBody(data);
			// 执行postMethod
			int statusCode = httpClient.executeMethod(postMethod);
			// HttpClient对于要求接受后继服务的请求，象POST和PUT等不能自动处理转发
			// 301或者302
			if (statusCode == HttpStatus.SC_MOVED_PERMANENTLY || 
			statusCode == HttpStatus.SC_MOVED_TEMPORARILY) {
			    // 从头中取出转向的地址
			    Header locationHeader = postMethod.getResponseHeader("location");
			    if (locationHeader != null) {
			    	json_str = locationHeader.getValue();
			     System.out.println("The page was redirected to:" + json_str);
			    } else {
			     System.err.println("Location field value is null.");
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return json_str;
	}
	
	public static String httpClientPost2(String url){
		String json_str = Const.EMPTY_STRING;
		try {
			HttpClient client = new HttpClient();
			//client.getHostConfiguration().setHost("localhost", 8088,"http");
			PostMethod post = new PostMethod(url);
			client.executeMethod(post);
			System.out.println(post.getStatusLine().toString());
			post.releaseConnection();
			// 检查是否重定向
			int statuscode = post.getStatusCode();
			if ((statuscode == HttpStatus.SC_MOVED_TEMPORARILY) || (statuscode == HttpStatus.SC_MOVED_PERMANENTLY) 
				|| (statuscode == HttpStatus.SC_SEE_OTHER) || (statuscode == HttpStatus.SC_TEMPORARY_REDIRECT)) {// 读取新的URL地址
				Header header = post.getResponseHeader("location");
				if (header != null) {
					String newuri = header.getValue();
					if ((newuri == null) || (newuri.equals(""))) {
						newuri ="/";
					}
					GetMethod redirect = new GetMethod(newuri);
					client.executeMethod(redirect);
					System.out.println("Redirect:"+ redirect.getStatusLine().toString());
					// 打印结果页面
					String response = new String(redirect.getResponseBodyAsString().getBytes("UTF-8"));
					// 打印返回的信息
					System.out.println(response);
					redirect.releaseConnection();
				} else {
					System.out.println("Invalid redirect");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

		return json_str;
	}
}
