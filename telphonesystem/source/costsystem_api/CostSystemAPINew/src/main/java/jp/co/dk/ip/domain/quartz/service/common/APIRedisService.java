package jp.co.dk.ip.domain.quartz.service.common;

import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.RedisUtil;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.model.OrderRedisModel;

@Service
@Transactional
public class APIRedisService {

    private static final Logger logger = LoggerFactory.getLogger(APIRedisService.class);
	
    /**
     * redis存储
     * @param <T>
     * @param t
     * @return Boolean
     */
    @SuppressWarnings({ "unused", "unchecked", "static-access" })
    public <T> Boolean redisProcess(
    		T t,
    		MessageSource messageSource,
    		RedisTemplate redisTemplate,
    		String key) {
        
        logger.debug("APIRedisService redisProcess");
        
        Boolean resultRedis = false;
        
        // key check
        if (StringUtils.isEmpty(key)) {
        	resultRedis = false;
        	return resultRedis;
        }
        
        // class parameter
        RedisUtil redisUtil = new RedisUtil(redisTemplate);
        SettingContext settingContext = new SettingContext();
        
        // get key and value
        try {
            Boolean redisTryLock = false;
            Boolean unlockResult = false;
            
    		Integer sumLockTimeRedis = 0;
    		Integer sumLockTimeLock = 0;
            
            JSONObject value = null;
            JSONObject jsonObject = null;
            String lock = Const.EMPTY_STRING;
			while (true) {
				// 等待50毫秒
				Utils.sleep(50L);
				value = new JSONObject();
	            value = (JSONObject) redisUtil.get(key);
	            jsonObject = new JSONObject();
	            jsonObject = (JSONObject) JSONObject.toJSON(t);
	            
				// redis加锁
				if (!redisTryLock) {
					if (!Utils.isEmpty(value)) {
						lock = value.getString("lock");	
					} else {
						lock = Const.UNLOCK;
					}
					if (Objects.equals(lock, Const.UNLOCK)) {
						jsonObject.put("lock", Const.LOCK);
						redisTryLock = saveNewOrderInfoBean(
	                    		redisUtil,
	                    		key,
	                    		jsonObject);
						logger.info(new StringBuffer("redis加锁 OrderRedisServiceImpl.redisProcess 获取锁状态：")
								.append(redisTryLock)
								.append("，取锁key值：")
								.append(key)
								.toString());
					}
				} else {
					// 等待100毫秒
					Utils.sleep(100L);
				}
				// redis和数据库插入更新处理
				if (redisTryLock) {
					// redis登录
                    resultRedis = saveNewOrderInfoBean(
                    		redisUtil,
                    		key,
                    		jsonObject);
				} else {
					// 解锁处理
					sumLockTimeLock++;
					if (sumLockTimeLock >= 100) {
						unlockResult = false;
						sumLockTimeLock = 0;
						logger.info(new StringBuffer("获取锁失败解锁处理 OrderRedisServiceImpl.redisProcess 强制释放锁")
								.append("，加锁key值：")
								.append(key)
								.append("，计时flg：")
								.append(sumLockTimeLock)
								.toString());
						if (!unlockResult) {
							if (!Utils.isEmpty(value)) {
								lock = value.getString("lock");	
							} else {
								lock = Const.LOCK;
							}
							if (Objects.equals(lock, Const.LOCK)) {
								jsonObject.put("lock", Const.UNLOCK);
								unlockResult = saveNewOrderInfoBean(
			                    		redisUtil,
			                    		key,
			                    		jsonObject);
							}
						}
		                if (!unlockResult) {
		                	logger.info(new StringBuffer("redis unlock redisProcess failure").append(key).toString());
		                	resultRedis = unlockResult;
		                } else {
							redisTryLock = false;
		                }
					}
				}
				// 跳出循环
				if (resultRedis) {
					// 解锁处理
					if (redisTryLock) {
						if (!Utils.isEmpty(value)) {
							lock = value.getString("lock");	
						} else {
							lock = Const.LOCK;
						}
						if (Objects.equals(lock, Const.LOCK)) {
							jsonObject.put("lock", Const.UNLOCK);
							unlockResult = saveNewOrderInfoBean(
		                    		redisUtil,
		                    		key,
		                    		jsonObject);
						}
					}
	                if (!unlockResult) {
	                	logger.info(new StringBuffer("redis unlock redisProcess failure").append(key).toString());
	                	resultRedis = unlockResult;
	                } else {
						break;
	                }
				}
			}
        } catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000017, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
        	logger.error(runEx.getMessage());
        	logger.info(new StringBuffer("redis unlock redisProcess failure").append(key).toString());
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000017, message);
            throw new CustomHandleException(errorRes);
        }
        
        return resultRedis;
    }
   	
	/**
	 * redis data saving
	 *
	 * @param key
	 * @param jsonObject
	 * @return OrderRedisModel
	 */
	@SuppressWarnings({ "unused" })
	private Boolean saveNewOrderInfoBean(
			RedisUtil redisUtil,
			String key,
			JSONObject jsonObject) {
		logger.info(new StringBuffer("redis insert APIRedisService.saveNewOrderInfoBean indto").append(
				key + "," + jsonObject).toString());

		Boolean result = true;
        
		try {
			Boolean hasType = false;
			hasType = redisUtil.hasKey(key);
			// redis列表操作对象
			JSONObject redisData = null;
			if (hasType) {
				redisData = new JSONObject();
				redisData = (JSONObject) redisUtil.get(key);
			} else {
				redisData = new JSONObject();
			}
			// redis对象list重新设置
			JSONObject inputJsonObject = new JSONObject();
			String getStatus = Const.EMPTY_STRING;
			if (!Utils.isEmpty(redisData)) {
				String judgeStr = Const.EMPTY_STRING;
				judgeStr = key.substring(Const.COUNT_0, Const.COUNT_1);
				// redis基本对象作成
				if (Objects.equals(judgeStr, Const.A)) {
					OrderRedisModel orderRedisModelOld = null;
					OrderRedisModel orderRedisModelNew = null;
					orderRedisModelOld = new OrderRedisModel();
					orderRedisModelNew = new OrderRedisModel();
					orderRedisModelOld = JSONObject.toJavaObject(jsonObject, OrderRedisModel.class);
					orderRedisModelNew = JSONObject.toJavaObject(redisData, OrderRedisModel.class);
					Utils.copyPropertiesIgnoreNull(orderRedisModelOld, orderRedisModelNew);
					// redis插入对象作成
					inputJsonObject = (JSONObject) JSONObject.toJSON(orderRedisModelNew);
				} else if (Objects.equals(judgeStr, Const.M)) {
					OrderRedisMicroUserModel orderRedisMicroUserModelOld = null;
					OrderRedisMicroUserModel orderRedisMicroUserModelNew = null;
					orderRedisMicroUserModelOld = new OrderRedisMicroUserModel();
					orderRedisMicroUserModelNew = new OrderRedisMicroUserModel();
					orderRedisMicroUserModelOld = JSONObject.toJavaObject(
							jsonObject,
							OrderRedisMicroUserModel.class);
					orderRedisMicroUserModelNew = JSONObject.toJavaObject(
							redisData,
							OrderRedisMicroUserModel.class);
					Utils.copyPropertiesIgnoreNull(orderRedisMicroUserModelOld, orderRedisMicroUserModelNew);
					// redis插入对象作成
					inputJsonObject = (JSONObject) JSONObject.toJSON(orderRedisMicroUserModelNew);
				}
			} else {
				// redis插入对象作成
				inputJsonObject = jsonObject;
			}
			redisUtil.setObjectValue(key, inputJsonObject);
			result = true;
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			logger.error(new StringBuffer("redis登录 OrderRedisServiceImpl.saveNewOrderInfoBean 失败").append(jsonObject).toString());
			result = false;
		}
		if (result) {
			logger.info(new StringBuffer("redis登录 OrderRedisServiceImpl.saveNewOrderInfoBean 成功").append(jsonObject).toString());
		} else {
			logger.info(new StringBuffer("redis登录 OrderRedisServiceImpl.saveNewOrderInfoBean 失败").append(jsonObject).toString());
		}
		return result;
	}
	
	/**
	 * get redis key
	 *
	 * @param key
	 * @param jsonObject
	 * @return OrderRedisModel
	 */
	public String getMicroRedisKey(OrderRedisMicroUserModel orderRedisMicroUserModel, Object... objects) {
		
		String key = Const.EMPTY_STRING;
		if (!Utils.isEmpty(objects)) {
			Utils.copyPropertiesIgnoreNull(objects[Const.COUNT_0], orderRedisMicroUserModel);	
		}
		if (!Utils.isAllEmpty(orderRedisMicroUserModel)) {
			if (!StringUtils.isEmpty(orderRedisMicroUserModel.getAccountId())) {
				key = orderRedisMicroUserModel.getAccountId();
			} else {
				key = Const.M + Const.PATH_UNDERBAR + orderRedisMicroUserModel.getCorpId() + Const.PATH_UNDERBAR + orderRedisMicroUserModel.getReqId();
			}
		}
        return key;
	}
	
	/**
	 * get redis key
	 * @param <T>
	 *
	 * @param key
	 * @param jsonObject
	 * @return OrderRedisModel
	 */
	public <T> String getAgentRedisKey(OrderRedisModel orderRedisModel, T... ts) {
		
		String key = Const.EMPTY_STRING;
		if (!Utils.isEmpty(ts)) {
			Utils.copyPropertiesIgnoreNull(ts[Const.COUNT_0], orderRedisModel);	
		}
		if (!Utils.isAllEmpty(orderRedisModel)) {
			key = Const.A + Const.PATH_UNDERBAR + orderRedisModel.getCorpId() + Const.PATH_UNDERBAR + orderRedisModel.getReqId();
		}
        return key;
	}
	
    /**
     * @author zbh
     * description: get orderRedisModel object
     * @DateTime: 01:02 2022/01/12
     * @Params keyContent
     * @return JSONObject
     */
     private JSONObject getJSONObject(
    		 String keyContent,
    		 RedisTemplate redisTemplate) {
		// redis data find
        RedisUtil redisUtil = new RedisUtil(redisTemplate);
		Set<String> keys = new HashSet<>();
		JSONObject value = new JSONObject();
		keys = redisUtil.getAllKeys(keyContent);
		for (String key: keys) {
            value = (JSONObject) redisUtil.get(key);
		}
		return value;
    }
    
    /**
     * @author zbh
     * description: get orderRedisModel object
     * @DateTime: 01:02 2022/01/12
     * @Params keyContent
     * @return OrderRedisModel
     */
    public OrderRedisModel getOrderRedisModel(
    		String keyContent,
    		RedisTemplate redisTemplate) {
    	// get redis json object
        RedisUtil redisUtil = new RedisUtil(redisTemplate);
    	JSONObject value = new JSONObject();
    	value = getJSONObject(keyContent, redisTemplate);
		// redis data find
		OrderRedisModel orderRedisModel = new OrderRedisModel();
		orderRedisModel = JSONObject.toJavaObject(value, OrderRedisModel.class);
		return orderRedisModel;
    }
    
    /**
     * @author zbh
     * description: get orderRedisMicroUserModel object
     * @DateTime: 01:02 2022/01/12
     * @Params keyContent
     * @return OrderRedisMicroUserModel
     */
    public OrderRedisMicroUserModel getOrderRedisMicroUserModel(
    		String keyContent, 
    		RedisTemplate redisTemplate) {
    	// get redis json object
    	JSONObject value = new JSONObject();
    	value = getJSONObject(keyContent, redisTemplate);
    	// redis data find
    	OrderRedisMicroUserModel orderRedisMicroUserModel = new OrderRedisMicroUserModel();
    	orderRedisMicroUserModel = JSONObject.toJavaObject(value, OrderRedisMicroUserModel.class);
		return orderRedisMicroUserModel;
    }
    
    /**
     * @author zbh
     * description: get value
     * @DateTime: 22:55 2022/01/23
     * @Params key
     * @return OrderRedisMicroUserModel
     */
    public JSONObject getValue(String key, RedisTemplate redisTemplate) {
    	
		JSONObject value = new JSONObject();
		RedisUtil redisUtil = new RedisUtil(redisTemplate);
		value = (JSONObject) redisUtil.get(key);
        
		return value;
        
    }
}
