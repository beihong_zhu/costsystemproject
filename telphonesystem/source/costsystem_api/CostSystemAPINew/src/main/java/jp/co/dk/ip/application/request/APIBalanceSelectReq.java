package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * balance charge API Request
 */
@Getter
@Setter
public class APIBalanceSelectReq extends BaseReq {

	// 客户端账号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 32, message = "{ECT010}")
	private String corpId;

	// 代理商流水号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 50, message = "{ECT010}")
	private String reqId;
	
    // 顾客账号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 200, message = "{ECT010}")
    private String accountId;
    
    // 时间戳，格式为：yyyyMMddHHmmss
    @NotEmpty(message = "{ECT008}")
    @Length(max =14, message = "{ECT010}")
    private String ts;

    // 充值号码
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String number;
    
    // 签名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String sign;
}
