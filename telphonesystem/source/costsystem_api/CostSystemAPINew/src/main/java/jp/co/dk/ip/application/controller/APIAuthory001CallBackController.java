package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import jp.co.dk.ip.application.request.APIAuthory001CallBackReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIAuthory001CallBackRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIAuthory001CallBackService;

/**
 * authory data reception 001 call back API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAuthory001CallBackController {

	private static final Logger logger = LoggerFactory.getLogger(APIAuthory001CallBackController.class);

	@Autowired
	private APIAuthory001CallBackService apiAuthory001CallBackService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIAUTHORY001")
	private APIAuthory001CallBackRes authoryDataProcess(
			@RequestBody @Valid APIAuthory001CallBackReq apiAuthory001CallBackReq,
			BindingResult bindingResult) {

		logger.debug("APIAuthory001CallBackController authoryDataProcess");

		APIAuthory001CallBackRes apiAuthory001CallBackRes = new APIAuthory001CallBackRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiAuthory001CallBackService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiAuthory001CallBackRes = apiAuthory001CallBackService.authoryDataProcess(apiAuthory001CallBackReq);
			
			// response setting
			if (!Utils.isEmpty(apiAuthory001CallBackRes)) {
				if (!Objects.equals(apiAuthory001CallBackRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiAuthory001CallBackRes = success(apiAuthory001CallBackRes);
				} else {
					apiAuthory001CallBackRes = failure(apiAuthory001CallBackRes);
				}
			} else {
				failure(apiAuthory001CallBackRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
			apiAuthory001CallBackRes = apiAuthory001CallBackService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
			apiAuthory001CallBackRes = apiAuthory001CallBackService.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0101);
		}

		return apiAuthory001CallBackRes;
	}
	
	/**
	 * success
	 */
	private APIAuthory001CallBackRes success(APIAuthory001CallBackRes apiAuthory001CallBackRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiAuthory001CallBackRes.getResultCnt())) {
			apiAuthory001CallBackRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiAuthory001CallBackRes.getResultCode())) {
			apiAuthory001CallBackRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiAuthory001CallBackRes;
	}
	
	/**
	 * failure
	 */
	private APIAuthory001CallBackRes failure(APIAuthory001CallBackRes apiAuthory001CallBackRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiAuthory001CallBackRes)) {
			apiAuthory001CallBackRes = new APIAuthory001CallBackRes();
		}
		
		if (Utils.isEmpty(apiAuthory001CallBackRes.getResultCnt())) {
			apiAuthory001CallBackRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiAuthory001CallBackRes.getResultCode())) {
			apiAuthory001CallBackRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiAuthory001CallBackRes.getMessageCode())) {
			apiAuthory001CallBackRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiAuthory001CallBackRes.getMessage())) {
			apiAuthory001CallBackRes.setMessage(message);
		}
		logger.error(message);
		
		return apiAuthory001CallBackRes;
	}
}
