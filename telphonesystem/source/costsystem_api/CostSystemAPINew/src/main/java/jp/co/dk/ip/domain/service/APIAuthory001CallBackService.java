package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import jp.co.dk.ip.application.request.APIAuthory001CallBackReq;
import jp.co.dk.ip.application.response.APIAuthory001CallBackRes;
import jp.co.dk.ip.application.response.APIChargeReturnRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.quartz.service.authory.AuthoryOrderPostService;
import jp.co.dk.ip.domain.repository.OrdersRepository;

/**
 * authory data reception 001 call back API Service
 */
@Service
@Transactional
public class APIAuthory001CallBackService {

    private static final Logger logger = LoggerFactory.getLogger(APIAuthory001CallBackService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private AuthoryOrderPostService authoryOrderPostService;
    
    @Autowired
    private OrdersRepository ordersRepository;
    
    /**
     * agent data reception 001 process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIAuthory001CallBackRes authoryDataProcess(APIAuthory001CallBackReq apiAuthor001CallBackReq) throws Exception {
		
        logger.debug("APIAuthory001CallBackService authoryDataProcess");
        
        APIAuthory001CallBackRes apiAuthory001CallBackRes = new APIAuthory001CallBackRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.AUTHORY_001_CALL_BACK_API.MERCHANTORDERID + Const.AUTHORY_001_CALL_BACK_API.EQUAL + apiAuthor001CallBackReq.getMerchantorderId()
        			+ Const.AUTHORY_001_CALL_BACK_API.AND + Const.AUTHORY_001_CALL_BACK_API.PLATFORMORDERID + Const.AUTHORY_001_CALL_BACK_API.EQUAL + apiAuthor001CallBackReq.getPlatformorderid()
        			+ Const.AUTHORY_001_CALL_BACK_API.AND + Const.AUTHORY_001_CALL_BACK_API.ORDERID + Const.AUTHORY_001_CALL_BACK_API.EQUAL + apiAuthor001CallBackReq.getOrderid()
        			+ Const.AUTHORY_001_CALL_BACK_API.AND + Const.AUTHORY_001_CALL_BACK_API.RECHARGENO + Const.AUTHORY_001_CALL_BACK_API.EQUAL + apiAuthor001CallBackReq.getRechargeno()
        			+ Const.AUTHORY_001_CALL_BACK_API.AND + Const.AUTHORY_001_CALL_BACK_API.AMOUNT + Const.AUTHORY_001_CALL_BACK_API.EQUAL + apiAuthor001CallBackReq.getAmount()
        			+ Const.AUTHORY_001_CALL_BACK_API.AND + Const.AUTHORY_001_CALL_BACK_API.RECHARGESTATE + Const.AUTHORY_001_CALL_BACK_API.EQUAL + apiAuthor001CallBackReq.getRechargestate();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiAuthor001CallBackReq.getSign())) {
				// response error
				apiAuthory001CallBackRes.setResultCode(Const.RESULT_CODE_2);
				apiAuthory001CallBackRes.setResultCnt(Const.COUNT_0);
				apiAuthory001CallBackRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiAuthory001CallBackRes.setMessage(message);
				return apiAuthory001CallBackRes;
			}

			// response result
			String result = Const.EMPTY_STRING;
//			result = authoryOrderPostService.getAuthory001PostSign(
//					apiAuthor001CallBackReq.getOrderid(), 
//					apiAuthor001CallBackReq.getPlatformorderid(),
//					ordersRepository);
			apiAuthory001CallBackRes.setResult(result);
			
			return apiAuthory001CallBackRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAuthory001CallBackRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIAuthory001CallBackRes apiAuthory001CallBackRes = new APIAuthory001CallBackRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiAuthory001CallBackRes.setResultCode(resultCode);
        apiAuthory001CallBackRes.setMessageCode(errorCode);
        apiAuthory001CallBackRes.setMessage(errMsg);
        return apiAuthory001CallBackRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return apiAgent001Res response class
     */
    public APIAuthory001CallBackRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAuthory001CallBackService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAuthory001CallBackRes setErrRes(CustomHandleException e) {

        logger.debug("APIAuthory001CallBackService setErrRes");

        APIAuthory001CallBackRes apiAuthory001CallBackRes = new APIAuthory001CallBackRes();
        apiAuthory001CallBackRes.setResult(Const.AUTHORY_001_CALL_BACK_API.FAILURE);
        apiAuthory001CallBackRes.setResultCode(e.getErrorMessage().getResultCode());
        apiAuthory001CallBackRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiAuthory001CallBackRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiAuthory001CallBackRes.setMessage(e.getErrorMessage().getMessage());
        return apiAuthory001CallBackRes;
    }
}
