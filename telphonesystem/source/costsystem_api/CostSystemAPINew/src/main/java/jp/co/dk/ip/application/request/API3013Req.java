package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3013Req extends BaseReq {
	
	// 代理账户
	private String agencyabbreviation;
	
	//代理名字
	private String value;
	//申请类型
	
	private String provideSevice;

    // 状态 
    private String ocrManufacturingDate;
    


}
