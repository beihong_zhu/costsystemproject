package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * micro login API Response
 */
@Getter
@Setter
public class APIMicroLoginRes extends BaseRes {

    // 返回结果
    private String accountId;
}
