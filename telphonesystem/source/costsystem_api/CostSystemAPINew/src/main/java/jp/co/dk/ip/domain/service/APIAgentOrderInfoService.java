package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.APIAgentOrderInfoReq;
import jp.co.dk.ip.application.response.APIAgentOrderInfoRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.RedisUtil;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderRedisModel;
import jp.co.dk.ip.domain.quartz.service.common.APIRedisService;

/**
 * agent data reception API Service
 */
@Service
@Transactional
public class APIAgentOrderInfoService {

    private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderInfoService.class);

    @Autowired
    private RedisUtil redisUtil;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIAgentOrderInfoRes agentDataProcess(APIAgentOrderInfoReq apiAgentOrderInfoReq) throws Exception {
		
        logger.debug("APIAgentService agentDataProcess");
        
        APIAgentOrderInfoRes apiAgentOrderInfoRes = new APIAgentOrderInfoRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.AGENT_CHARGE_API.CORPID + Const.AGENT_CHARGE_API.EQUAL + apiAgentOrderInfoReq.getCorpId()
        			+ Const.AGENT_CHARGE_API.AND 
        			+ Const.AGENT_CHARGE_API.REQID + Const.AGENT_CHARGE_API.EQUAL + apiAgentOrderInfoReq.getReqId();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiAgentOrderInfoReq.getSign())) {
				// response error
				apiAgentOrderInfoRes.setOrderId(null);
				apiAgentOrderInfoRes.setPrice(null);
				apiAgentOrderInfoRes.setResultCode(Const.RESULT_CODE_2);
				apiAgentOrderInfoRes.setResultCnt(Const.COUNT_0);
				apiAgentOrderInfoRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiAgentOrderInfoRes.setMessage(message);
				return apiAgentOrderInfoRes;
			}
        	
			// redis data find
			APIRedisService apiRedisService = new APIRedisService();
			OrderRedisModel orderRedisModel = new OrderRedisModel();
			String key = Const.EMPTY_STRING;
			key = apiRedisService.getAgentRedisKey(orderRedisModel, apiAgentOrderInfoReq);
			orderRedisModel = apiRedisService.getOrderRedisModel(key, redisTemplate);
			
			// response setting
			if (!Utils.isEmpty(orderRedisModel)) {
				if (!Utils.checkObjAllFieldsIsNull(orderRedisModel)) {
					apiAgentOrderInfoRes.setOrderId(orderRedisModel.getOrderId());
					apiAgentOrderInfoRes.setPrice(orderRedisModel.getCorpId());	
				} else {
					apiAgentOrderInfoRes.setResultCnt(Const.COUNT_0);
				}
			} else {
				apiAgentOrderInfoRes = null;
			}
			return apiAgentOrderInfoRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENTORDERINFO + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENTORDERINFO + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENTORDERINFO + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAgentOrderInfoRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIAgentOrderInfoRes apiAgentOrderInfoRes = new APIAgentOrderInfoRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiAgentOrderInfoRes.setResultCode(resultCode);
        apiAgentOrderInfoRes.setMessageCode(errorCode);
        apiAgentOrderInfoRes.setMessage(errMsg);
        return apiAgentOrderInfoRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIAgentOrderInfoRes response class
     */
    public APIAgentOrderInfoRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAgentService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAgentOrderInfoRes setErrRes(CustomHandleException e) {

        logger.debug("APIAgentOrderInfoService setErrRes");

        APIAgentOrderInfoRes apiAgentOrderInfoRes = new APIAgentOrderInfoRes();
        apiAgentOrderInfoRes.setResultCode(e.getErrorMessage().getResultCode());
        apiAgentOrderInfoRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiAgentOrderInfoRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiAgentOrderInfoRes.setMessage(e.getErrorMessage().getMessage());
        return apiAgentOrderInfoRes;
    }
}
