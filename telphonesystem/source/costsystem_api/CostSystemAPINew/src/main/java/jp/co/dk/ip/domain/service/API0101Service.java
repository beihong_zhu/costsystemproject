package jp.co.dk.ip.domain.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import jp.co.dk.ip.application.response.API0101Res;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.SysUsers;
import jp.co.dk.ip.domain.repository.SysUsersRepository;
import jp.co.dk.ip.domain.service.common.FaceDetectService;
import jp.co.dk.ip.domain.service.common.FaceCompareService;

/**
 * 登录页面人脸识别API Service
 */
@Service
@Transactional
public class API0101Service {

    private static final Logger logger = LoggerFactory.getLogger(API0101Service.class);

    @Autowired
    private SysUsersRepository sysUsersRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * 登录页面人脸识别信息取得
     *
     * @param pictureMultipartFile face picture
     * @return face recognization result
     */
    @SuppressWarnings("rawtypes")
	public boolean findSysUserByFace(MultipartFile pictureMultipartFile) throws Exception {

        logger.debug("API0101Service findSysUserByFace");

        try {
        File file = null;
        file =  Utils.convertToFile(pictureMultipartFile);
        if (Objects.equals(file, null)) {
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000009, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000009, message);
            throw new CustomHandleException(errorRes);	
        }
        
    	List<SysUsers> sysUsers = new ArrayList<SysUsers>();
        sysUsers = sysUsersRepository.findByIsDeleted(Const.INT_DELETE_FLG_OFF);
        logger.info(Const.FIND_DATA_COUNT + sysUsers.size());
        
			// 获取人脸识别face token
			String faceToken = Const.EMPTY_STRING;
			faceToken = FaceDetectService.RunFaceDetect(
					file, 
					messageSource,
					settingContext);

			// 进行比对
			Double confidence = Const.COUNT_0_00;
			for (SysUsers sysUser : sysUsers) {
				confidence = FaceCompareService.RunFaceCompare(
						faceToken, 
						sysUser.getFaceSetToken(),
						messageSource,
						settingContext);

				// 人脸识别验证通过
				if (confidence > Const.CONFIDENCE_NUM_90) {
					sysUser.setIsLogin(Const.COUNT_1);
					sysUsersRepository.saveAndFlush(sysUser);
					return true;
				}
			}
			return false;
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0101 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0101 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0101 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public API0101Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("API0101Service getErrRes");

        API0101Res api0101Res = new API0101Res();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        api0101Res.setResultCode(resultCode);
        api0101Res.setMessageCode(errorCode);
        api0101Res.setMessage(errMsg);
        return api0101Res;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return API0101Res response class
     */
    public API0101Res getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("API0101Service getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public API0101Res setErrRes(CustomHandleException e) {

        logger.debug("API0101Service setErrRes");

        API0101Res api0101Res = new API0101Res();
        api0101Res.setResultCode(e.getErrorMessage().getResultCode());
        api0101Res.setResultCnt(e.getErrorMessage().getResultCnt());
        api0101Res.setMessageCode(e.getErrorMessage().getMessageCode());
        api0101Res.setMessage(e.getErrorMessage().getMessage());
        return api0101Res;
    }
}
