package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API3013Req;
import jp.co.dk.ip.application.response.API3013Res;
import jp.co.dk.ip.application.response.data.CapitalTotalInfoModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.CapitalTotalInfoModel;
import jp.co.dk.ip.domain.repository.CapitalTotalRepository;

/**
 * all agents summary API Service
 */
@Service
@Transactional
public class API3013Service {

	private static final Logger logger = LoggerFactory.getLogger(API3013Service.class);

	@Autowired
	private CapitalTotalRepository capitalTotalRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents summary
	 *
	 * @param api0112Req all agents summary
	 * @return result
	 */
	public List<CapitalTotalInfoModelRes> getAgentNamesResList(API3013Req api3003Req) throws Exception {

		logger.debug("API3003Service getAgentNamesResList");

		List<CapitalTotalInfoModelRes> capitalTotalInfoModelResList = new ArrayList<CapitalTotalInfoModelRes>();
		
		try {
			List<CapitalTotalInfoModel> capitalTotalInfoModels = new ArrayList<CapitalTotalInfoModel>();

			// agents data setting
			capitalTotalInfoModels = capitalTotalRepository.findCapitalByCapitalInfo(
					api3003Req.getAgencyabbreviation(),
					api3003Req.getValue(),
					api3003Req.getProvideSevice(),
					api3003Req.getOcrManufacturingDate()	
					);
			logger.info(Const.FIND_DATA_COUNT + capitalTotalInfoModels.size());

			// get all agents
			for (CapitalTotalInfoModel capitalTotalInfoModel : capitalTotalInfoModels) {
				CapitalTotalInfoModelRes capitalTotalInfoModelRes = new CapitalTotalInfoModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(capitalTotalInfoModel, capitalTotalInfoModelRes);
				capitalTotalInfoModelResList.add(capitalTotalInfoModelRes);
			}

			return capitalTotalInfoModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API3013Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API3003Service getErrRes");

		API3013Res api3003Res = new API3013Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api3003Res.setResultCode(resultCode);
		api3003Res.setMessageCode(errorCode);
		api3003Res.setMessage(errMsg);
		return api3003Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0112Res response class
	 */
	public API3013Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API3003Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API3013Res setErrRes(CustomHandleException e) {

		logger.debug("API0112Service setErrRes");

		API3013Res api3003Res = new API3013Res();
		api3003Res.setResultCode(e.getErrorMessage().getResultCode());
		api3003Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api3003Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api3003Res.setMessage(e.getErrorMessage().getMessage());
		return api3003Res;
	}
}
