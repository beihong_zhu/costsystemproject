package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * authory data reception 002 call back API Response
 */
@Getter
@Setter
public class APIAuthory002CallBackRes extends BaseRes {

    // 返回结果
    private String result;
}
