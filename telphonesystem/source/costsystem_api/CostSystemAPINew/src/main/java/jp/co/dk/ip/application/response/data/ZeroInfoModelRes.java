package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class ZeroInfoModelRes {

	private String agencyname;
	
	private String capital;
	
	private String capitalStatus;
	
	private String  createdDatetime;
   

}
