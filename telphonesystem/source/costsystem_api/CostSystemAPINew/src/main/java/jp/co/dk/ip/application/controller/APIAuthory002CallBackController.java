package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import jp.co.dk.ip.application.request.APIAuthory002CallBackReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultRes;
import jp.co.dk.ip.application.response.APIAuthory002CallBackRes;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIAuthory002CallBackService;

/**
 * authory data reception 002 call back API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAuthory002CallBackController {

	private static final Logger logger = LoggerFactory.getLogger(APIAuthory002CallBackController.class);

	@Autowired
	private APIAuthory002CallBackService apiAuthory002CallBackService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIAUTHORY002")
	private APIAuthory002CallBackRes authoryDataProcess(
			@RequestBody @Valid APIAuthory002CallBackReq apiAuthory002CallBackReq,
			BindingResult bindingResult) {

		logger.debug("APIAuthory002CallBackController authoryDataProcess");

		APIAuthory002CallBackRes apiAuthory002CallBackRes = new APIAuthory002CallBackRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiAuthory002CallBackService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiAuthory002CallBackRes = apiAuthory002CallBackService.authoryDataProcess(apiAuthory002CallBackReq);
			
			// response setting
			if (!Utils.isEmpty(apiAuthory002CallBackRes)) {
				if (!Objects.equals(apiAuthory002CallBackRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiAuthory002CallBackRes = success(apiAuthory002CallBackRes);
				} else {
					apiAuthory002CallBackRes = failure(apiAuthory002CallBackRes);
				}
			} else {
				apiAuthory002CallBackRes = failure(apiAuthory002CallBackRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
			apiAuthory002CallBackRes = apiAuthory002CallBackService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
			apiAuthory002CallBackRes = apiAuthory002CallBackService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000005,
					Const.ERRRES_API0101);
		}

		return apiAuthory002CallBackRes;
	}
	
	/**
	 * success
	 */
	private APIAuthory002CallBackRes success(APIAuthory002CallBackRes apiAuthory002CallBackRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiAuthory002CallBackRes.getResultCnt())) {
			apiAuthory002CallBackRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiAuthory002CallBackRes.getResultCode())) {
			apiAuthory002CallBackRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiAuthory002CallBackRes;
	}
	
	/**
	 * failure
	 */
	private APIAuthory002CallBackRes failure(APIAuthory002CallBackRes apiAuthory002CallBackRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiAuthory002CallBackRes)) {
			apiAuthory002CallBackRes = new APIAuthory002CallBackRes();
		}
		
		if (Utils.isEmpty(apiAuthory002CallBackRes.getResultCnt())) {
			apiAuthory002CallBackRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiAuthory002CallBackRes.getResultCode())) {
			apiAuthory002CallBackRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiAuthory002CallBackRes.getMessageCode())) {
			apiAuthory002CallBackRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiAuthory002CallBackRes.getMessage())) {
			apiAuthory002CallBackRes.setMessage(message);
		}
		logger.error(message);
		
		return apiAuthory002CallBackRes;
	}
}
