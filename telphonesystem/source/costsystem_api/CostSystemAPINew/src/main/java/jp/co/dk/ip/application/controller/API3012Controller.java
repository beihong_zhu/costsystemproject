package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3012Req;
import jp.co.dk.ip.application.response.API3012Res;
import jp.co.dk.ip.application.response.data.AuthoryInfoRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3012Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API3012Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3012Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3012Service api3012Service;

	@PostMapping("/API3012")
	private API3012Res agentProcess(@RequestBody @Valid API3012Req api3012Req, BindingResult bindingResult) {

		logger.debug("API3012Controller agentProcess");

		API3012Res api3012Res = new API3012Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3012Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<AuthoryInfoRes> authoryInfoResList = new ArrayList<AuthoryInfoRes>();
			authoryInfoResList = api3012Service.getAgentNamesResList(api3012Req);
			
			// response setting
			if (!Objects.equals(authoryInfoResList, null)) {
				api3012Res.setResultCnt(authoryInfoResList.size());
				api3012Res.setAuthoryInfoResList(authoryInfoResList);
				api3012Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API3012 + e.getMessage(), e);
			api3012Res = api3012Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API3012 + e.getMessage(), e);
			api3012Res = api3012Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3012);
		}

		return api3012Res;
	}
}
