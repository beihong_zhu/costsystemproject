package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0111Req;
import jp.co.dk.ip.application.response.API0111Res;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0111Service;

/**
 * all agents getting API Controller
 */
@RestController
@RequestMapping("/api")
public class API0111Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0111Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0111Service api0111Service;

	@PostMapping("/API0111")
	private API0111Res agentDataProcess(@RequestBody @Valid API0111Req api0111Req, BindingResult bindingResult) {

		logger.debug("API0111Controller agentDataProcess");

		API0111Res api0111Res = new API0111Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0111Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<String> agentNames = new ArrayList<String>();
			agentNames = api0111Service.getAgentNamesResList(api0111Req);
			
			// response setting
			if (!Objects.equals(agentNames, null)) {
				api0111Res.setResultCnt(agentNames.size());
				api0111Res.setAgentNames(agentNames);
				api0111Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001, 
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0111 + e.getMessage(), e);
			api0111Res = api0111Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0111 + e.getMessage(), e);
			api0111Res = api0111Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0111);
		}

		return api0111Res;
	}
}
