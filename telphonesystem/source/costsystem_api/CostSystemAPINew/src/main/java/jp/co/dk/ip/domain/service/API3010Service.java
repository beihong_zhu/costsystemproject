package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API3010Req;
import jp.co.dk.ip.application.response.API3010Res;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Stocks;
import jp.co.dk.ip.domain.repository.StocksRepository;

/**
 * product adding API Service
 */
@Service
@Transactional
public class API3010Service {

    private static final Logger logger = LoggerFactory.getLogger(API3010Service.class);

    @Autowired
    private StocksRepository stocksRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * product adding
     *
     * @param api3010Req product data
     * @return result
     */
	public void addAuthory(API3010Req api3010Req) throws Exception {

        logger.debug("API3010Service addAuthory");

        try {
        	// product data getting
        	List<Stocks> stocks = new ArrayList<Stocks>();
        	stocks = stocksRepository.findByIsDeleted(Const.INT_DELETE_FLG_OFF);
        	
        	// set product id
        	List<Integer> stockIds = new ArrayList<Integer>();
        	for (Stocks stock: stocks) {
        		Integer productId = Const.COUNT_0;
        		productId = Integer.valueOf(stock.getProductId());
        		stockIds.add(productId);
        	}
        	// get max product id
			Integer max = Const.COUNT_0;
			max = stockIds.stream().reduce(Integer::max).get();
        	
        	// product data setting
			Stocks stock = new Stocks();
			stock.setProductId(String.valueOf(max + Const.COUNT_1));
			stock.setAccountControl(api3010Req.getChannelAccount());
			stock.setProductMode(api3010Req.getCostType());
			stock.setName(api3010Req.getProvideSevice());
			stock.setNumType(api3010Req.getTeletype());
			stock.setProvince(api3010Req.getProvince());
			stock.setCity(api3010Req.getAddress());
			stock.setValue(api3010Req.getProductValue());
			stock.setProductName(api3010Req.getProductName());
			
        	
			stocksRepository.saveAndFlush(stock);
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0106 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0106 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0106 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public API3010Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("API0106Service getErrRes");

        API3010Res api3010Res = new API3010Res();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        api3010Res.setResultCode(resultCode);
        api3010Res.setMessageCode(errorCode);
        api3010Res.setMessage(errMsg);
        return api3010Res;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return API0106Res response class
     */
    public API3010Res getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("API0106Service getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public API3010Res setErrRes(CustomHandleException e) {

        logger.debug("API0106Service setErrRes");

        API3010Res api3010Res = new API3010Res();
        api3010Res.setResultCode(e.getErrorMessage().getResultCode());
        api3010Res.setResultCnt(e.getErrorMessage().getResultCnt());
        api3010Res.setMessageCode(e.getErrorMessage().getMessageCode());
        api3010Res.setMessage(e.getErrorMessage().getMessage());
        return api3010Res;
    }
}
