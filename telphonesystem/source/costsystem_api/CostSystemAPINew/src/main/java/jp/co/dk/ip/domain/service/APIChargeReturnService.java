package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.APIChargeReturnReq;
import jp.co.dk.ip.application.response.APIChargeReturnRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.RedisUtil;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.quartz.service.common.APIRedisService;

/**
 * balance charge API Service
 */
@Service
@Transactional
public class APIChargeReturnService {

    private static final Logger logger = LoggerFactory.getLogger(APIChargeReturnService.class);

    @Autowired
    private RedisUtil redisUtil;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIChargeReturnRes agentDataProcess(APIChargeReturnReq apiChargeReturnReq) throws Exception {
		
        logger.debug("APIChargeReturnService agentDataProcess");
        
        APIChargeReturnRes apiChargeReturnRes = new APIChargeReturnRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.CHARGE_RETURN_API.CORPID + Const.CHARGE_RETURN_API.EQUAL + apiChargeReturnReq.getCorpId()
        			+ Const.CHARGE_RETURN_API.AND + Const.CHARGE_RETURN_API.REQID + Const.CHARGE_RETURN_API.EQUAL + apiChargeReturnReq.getReqId()
        			+ Const.CHARGE_RETURN_API.AND + Const.CHARGE_RETURN_API.ACCOUNTID + Const.CHARGE_RETURN_API.EQUAL + apiChargeReturnReq.getAccountId()
        			+ Const.CHARGE_RETURN_API.AND + Const.CHARGE_RETURN_API.TS + Const.CHARGE_RETURN_API.EQUAL + apiChargeReturnReq.getTs()
        			+ Const.CHARGE_RETURN_API.AND + Const.CHARGE_RETURN_API.MONEY + Const.CHARGE_RETURN_API.EQUAL + apiChargeReturnReq.getMomey()
        			+ Const.CHARGE_RETURN_API.AND + Const.CHARGE_RETURN_API.NUMBER + Const.CHARGE_RETURN_API.EQUAL + apiChargeReturnReq.getNumber();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiChargeReturnReq.getSign())) {
				// response error
				apiChargeReturnRes.setResultCode(Const.RESULT_CODE_2);
				apiChargeReturnRes.setResultCnt(Const.COUNT_0);
				apiChargeReturnRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiChargeReturnRes.setMessage(message);
				return apiChargeReturnRes;
			}
        	
			// redis data find
			APIRedisService apiRedisService = new APIRedisService();
			OrderRedisMicroUserModel orderRedisMicroUserModel = new OrderRedisMicroUserModel();
			String key = Const.EMPTY_STRING;
			key = apiRedisService.getMicroRedisKey(orderRedisMicroUserModel, apiChargeReturnReq);
			orderRedisMicroUserModel = apiRedisService.getOrderRedisMicroUserModel(key, redisTemplate);

			// set response
			if (!Utils.isEmpty(orderRedisMicroUserModel)) {
				if (!Utils.checkObjAllFieldsIsNull(orderRedisMicroUserModel)) {
					Utils.copyPropertiesIgnoreNull(apiChargeReturnReq, orderRedisMicroUserModel);
					orderRedisMicroUserModel.setIsRun(Const.NORUN);
					
					// get return money
					Double returnMoney = null;
					returnMoney = Utils.getReturnMoney(apiChargeReturnReq.getMomey());
					orderRedisMicroUserModel.setReturnMoney(String.valueOf(returnMoney));
					
					// enter into queue
					Utils.jinZhanMicroChargeQueue(orderRedisMicroUserModel);
					
					// response setting
					apiChargeReturnRes.setReturnMoney(String.valueOf(returnMoney));
				} else {
					// response setting
					apiChargeReturnRes.setResultCnt(Const.COUNT_0);
				}
			} else {
				// response error
				apiChargeReturnRes.setResultCode(Const.RESULT_CODE_3);
				apiChargeReturnRes.setResultCnt(Const.COUNT_0);
				apiChargeReturnRes.setMessageCode(Const.E000020);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000020,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiChargeReturnRes.setMessage(message);
			}
			return apiChargeReturnRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIChargeReturnRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIChargeReturnRes apiChargeReturnRes = new APIChargeReturnRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiChargeReturnRes.setResultCode(resultCode);
        apiChargeReturnRes.setMessageCode(errorCode);
        apiChargeReturnRes.setMessage(errMsg);
        return apiChargeReturnRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIChargeReturnRes response class
     */
    public APIChargeReturnRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIChargeReturnService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIChargeReturnRes setErrRes(CustomHandleException e) {

        logger.debug("APIChargeReturnService setErrRes");

        APIChargeReturnRes apiChargeReturnRes = new APIChargeReturnRes();
        apiChargeReturnRes.setResultCode(e.getErrorMessage().getResultCode());
        apiChargeReturnRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiChargeReturnRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiChargeReturnRes.setMessage(e.getErrorMessage().getMessage());
        return apiChargeReturnRes;
    }
}
