package jp.co.dk.ip.domain.repository;

import jp.co.dk.ip.domain.entity.CapitalTotal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * . capital total repository
 */
@Repository
public interface CapitalTotalRepository extends JpaRepository<CapitalTotal, String>,CapitalTotalCustomRepository
{

	CapitalTotal findByAgentId(String agentId);
}
