package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0112Req;
import jp.co.dk.ip.application.response.API0112Res;
import jp.co.dk.ip.application.response.data.AgentSummaryModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0112Service;

/**
 * all agents summary API Controller
 */
@RestController
@RequestMapping("/api")
public class API0112Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0112Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0112Service api0112Service;

	@PostMapping("/API0112")
	private API0112Res agentSummaryProcess(@RequestBody @Valid API0112Req api0112Req, BindingResult bindingResult) {

		logger.debug("API0112Controller agentSummaryProcess");

		API0112Res api0112Res = new API0112Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0112Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<AgentSummaryModelRes> agentSummaryModelResList = new ArrayList<AgentSummaryModelRes>();
			agentSummaryModelResList = api0112Service.getAgentNamesResList(api0112Req);
			
			// response setting
			if (!Objects.equals(agentSummaryModelResList, null)) {
				api0112Res.setResultCnt(agentSummaryModelResList.size());
				api0112Res.setAgentSummaryModelResList(agentSummaryModelResList);
				api0112Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api0112Res = api0112Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api0112Res = api0112Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0112);
		}

		return api0112Res;
	}
}
