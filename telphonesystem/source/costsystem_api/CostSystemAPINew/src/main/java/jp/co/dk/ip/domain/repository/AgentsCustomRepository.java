package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.AgentCapitalInfoModel;
import jp.co.dk.ip.domain.model.AgentFundRecorderModel;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.OnShelfProductModel;

/**
 * . agents custom repository
 */
public interface AgentsCustomRepository {

	List<AgentCapitalInfoModel> findAgentCapitalByAgentInfo(String agentId, String agentName, String agentReduceName);

	List<AgentInfoModel> findInfoAgentInfoByAgentInfo(String accountagency, String agencyname, String  agencyabbreviation , String agency);
	
	List<AgentInfoModel> findInfoAgentCapitalByAgentInfo(
			String agentAccount,
			String agentName,
			String agentNumber,
			String transactionalNumber,
		    String requestNumber,
		    String transactionalMethod,
		    String transactionalType,
		    String timeScope);
	
	List<AgentFundRecorderModel> findAgentFundRecorderByAgentInfo(
			String agencyLimited,
			String agentName,
			String localLimited,
			String agencyacount,
			String bussinessMode,
			String stranctionMode,
			String channelLimited,
			String agencyabbreviate,
			String ocrManufacturingDate);
	
	
}
