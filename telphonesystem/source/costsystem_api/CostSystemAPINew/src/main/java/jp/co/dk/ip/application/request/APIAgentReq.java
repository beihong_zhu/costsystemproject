package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception API Request
 */
@Getter
@Setter
public class APIAgentReq extends BaseReq {

    // 代理商账号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String corpId;
    
    // 代理商流水号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 50, message = "{ECT010}")
    private String reqId;
    
    // 时间戳，格式为：yyyyMMddHHmmss
    @NotEmpty(message = "{ECT008}")
    @Length(max = 14, message = "{ECT010}")
    private String ts;
    
    // 面值（元）
    @NotEmpty(message = "{ECT008}")
    @Length(max = 5, message = "{ECT010}")
    private String money;
    
    // 运营商：1-移动，2-联通，3-电信，4-中石化
    @NotEmpty(message = "{ECT008}")
    @Length(max = 2, message = "{ECT010}")
    private String spId;
    
//    // 省份编码（详见附录）
//    @NotEmpty(message = "{ECT008}")
//    @Length(max = 2, message = "{ECT010}")
//    private String provId;
    
    // 充值号码
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String number;
    
    // 回调地址
    @NotEmpty(message = "{ECT008}")
    @Length(max = 128, message = "{ECT010}")
    private String retUrl;
    
    // 签名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String sign;
}
