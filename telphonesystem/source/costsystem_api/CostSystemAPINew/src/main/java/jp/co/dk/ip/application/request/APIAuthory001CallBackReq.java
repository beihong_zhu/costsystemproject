package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * authory data reception 001 call back API Request
 */
@Getter
@Setter
public class APIAuthory001CallBackReq extends BaseReq {

	// 000为请求成功，否则为失败
	@NotEmpty(message = "{ECT008}")
	@Length(max = 8, message = "{ECT010}")
	private String code;

	// 商户订单编号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 30, message = "{ECT010}")
	private String merchantorderId;

	// 平台订单号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 30, message = "{ECT010}")
	private String platformorderid;

	// 充值平台凭证号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 20, message = "{ECT010}")
	private String orderid;

	// 充值号码
	@NotEmpty(message = "{ECT008}")
	@Length(max = 11, message = "{ECT010}")
	private String rechargeno;

	// 充值面值
	@NotEmpty(message = "{ECT008}")
	@Length(max = 8, message = "{ECT010}")
	private String amount;

	// 付款金额（不参与签名）
	@NotEmpty(message = "{ECT008}")
	@Length(max = 8, message = "{ECT010}")
	private String payamount;

	// 充值状态，见下表：Rechargestate为充值状态
	@NotEmpty(message = "{ECT008}")
	@Length(max = 5, message = "{ECT010}")
	private String rechargestate;

	// 订单创建时间（不参与签名）
	@NotEmpty(message = "{ECT008}")
	@Length(max = 20, message = "{ECT010}")
	private String createtime;

	// 签名
	@NotEmpty(message = "{ECT008}")
	@Length(max = 32, message = "{ECT010}")
	private String sign;
}
