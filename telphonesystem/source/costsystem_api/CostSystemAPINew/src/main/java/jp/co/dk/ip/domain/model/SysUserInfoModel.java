package jp.co.dk.ip.domain.model;

import lombok.Getter;
import lombok.Setter;

/**
 * . system users
 */
@Getter
@Setter
public class SysUserInfoModel {

	/**
	 * . 用户ID
	 */
	private String userId;

	/**
	 * . 用户名
	 */
	private String userName;

	/**
	 * . 公司名
	 */
	private String companyName;

	/**
	 * . 脸部face set token
	 */
	private String faceSetToken;

	/**
	 * . 权限ID
	 */
	private String authorityId;

	/**
	 * . 邮箱地址
	 */
	private String mailAddress;

	/**
	 * . 登录flg
	 */
	private int isLogin;
}
