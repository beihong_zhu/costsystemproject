package jp.co.dk.ip.domain.mq;
 
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
 
import java.util.List;
 
/**
 * 消费者
 */
public class ConsumerTest {
	
	public static void main(String[] args) throws InterruptedException, MQClientException {
	    DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("unique_group_name_quickstart");

	    consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);

	    consumer.setNamesrvAddr("localhost:9876");
	    consumer.setInstanceName("QuickStartConsumer");

	    consumer.subscribe("TopicTest", "TagA3");

	    consumer.registerMessageListener(new MessageListenerConcurrently() {
	        @Override
	        public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
	                                                        ConsumeConcurrentlyContext context) {
	            System.out.println(Thread.currentThread().getName() + " Receive New Messages: " + msgs);
	           for (Message msg :msgs){
	               System.out.println(new String(msg.getBody()));
	           }
	            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
	        }
	    });

	    consumer.start();

	    System.out.println("Consumer Started.");
	}
}