package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API3002Req;
import jp.co.dk.ip.application.response.API3002Res;
import jp.co.dk.ip.application.response.data.AgentInfoModelRes;
import jp.co.dk.ip.application.response.data.CapitalRecordModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.CapitalRecordModel;
import jp.co.dk.ip.domain.repository.AgentsInfoCustomRepository;
import jp.co.dk.ip.domain.repository.AgentsRepository;
import jp.co.dk.ip.domain.repository.CapitalStatusRepository;

/**
 * all agents summary API Service
 */
@Service
@Transactional
public class API3002Service {

	private static final Logger logger = LoggerFactory.getLogger(API3002Service.class);

	@Autowired
	private CapitalStatusRepository capitalStatusRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents summary
	 *
	 * @param api0112Req all agents summary
	 * @return result
	 */
	public List<CapitalRecordModelRes> getCapitalRecordModelResList(API3002Req api3002Req) throws Exception {

		logger.debug("API3002Service getCapitalRecordModelList");

		List<CapitalRecordModelRes> capitalRecordModelResList = new ArrayList<CapitalRecordModelRes>();
		
		try {
			List<CapitalRecordModel> capitalRecordModels = new ArrayList<CapitalRecordModel>();

			// capital records data setting
			capitalRecordModels = capitalStatusRepository.findCapitalRecordByCapitalInfo(
					api3002Req.getCorpId(),
					api3002Req.getAgentName(),
					api3002Req.getAgentRecordName(),
					api3002Req.getReqId(),
					api3002Req.getOrderId(),
					api3002Req.getBillId(),
					api3002Req.getOrderForm(),
					api3002Req.getOrderType(),
					api3002Req.getStartTime(),
					api3002Req.getEndTime());
			logger.info(Const.FIND_DATA_COUNT + capitalRecordModels.size());

			// get all agents
			for (CapitalRecordModel capitalRecordModel : capitalRecordModels) {
				CapitalRecordModelRes capitalRecordModelRes = new CapitalRecordModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(capitalRecordModel, capitalRecordModelRes);
				capitalRecordModelResList.add(capitalRecordModelRes);
			}

			return capitalRecordModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API3002Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API3002Service getErrRes");

		API3002Res api3002Res = new API3002Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api3002Res.setResultCode(resultCode);
		api3002Res.setMessageCode(errorCode);
		api3002Res.setMessage(errMsg);
		return api3002Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0112Res response class
	 */
	public API3002Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API3002Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API3002Res setErrRes(CustomHandleException e) {

		logger.debug("API3002Service setErrRes");

		API3002Res api3002Res = new API3002Res();
		api3002Res.setResultCode(e.getErrorMessage().getResultCode());
		api3002Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api3002Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api3002Res.setMessage(e.getErrorMessage().getMessage());
		return api3002Res;
	}
}
