package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * 登录页面人脸识别API Response
 */
@Getter
@Setter
public class API0101Res extends BaseRes {

    // 人脸识别结果
    public boolean faceJudge;
}
