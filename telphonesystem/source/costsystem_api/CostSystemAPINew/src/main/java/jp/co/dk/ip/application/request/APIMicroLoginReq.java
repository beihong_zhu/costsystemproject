package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * micro login API Request
 */
@Getter
@Setter
public class APIMicroLoginReq extends BaseReq {

	// 客户端账号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 32, message = "{ECT010}")
	private String corpId;

	// 代理商流水号
	@NotEmpty(message = "{ECT008}")
	@Length(max = 50, message = "{ECT010}")
	private String reqId;
    
    // 用户名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String username;
    
    // 密码
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String password;
    
    // 用户手机号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 14, message = "{ECT010}")
    private String tel;

    // 时间戳，格式为：yyyyMMddHHmmss
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String ts;

    // 用户性别
    @NotEmpty(message = "{ECT008}")
    @Length(max = 2, message = "{ECT010}")
    private String gender;
    
    // 签名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String sign;
}
