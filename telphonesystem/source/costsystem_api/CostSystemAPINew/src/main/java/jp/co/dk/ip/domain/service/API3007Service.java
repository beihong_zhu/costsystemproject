package jp.co.dk.ip.domain.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import jp.co.dk.ip.application.request.API3007Req;
import jp.co.dk.ip.application.response.API3007Res;
import jp.co.dk.ip.application.response.data.OnShelfProductModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OnShelfProductModel;
import jp.co.dk.ip.domain.repository.AgentProductPriceRepository;
/**
 * all onshelf info API Service
 */
@Service
@Transactional
public class API3007Service {

	private static final Logger logger = LoggerFactory.getLogger(API3007Service.class);

	@Autowired
	private AgentProductPriceRepository AgentProductPriceRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all onshelf info summary
	 *
	 * @param api3007Req all agents summary
	 * @return result
	 */
	public List<OnShelfProductModelRes> getAgentNamesResList(API3007Req api3007Req) throws Exception {

		logger.debug("API3007Service getAgentNamesResList");

		List<OnShelfProductModelRes> onShelfProductModelResList = new ArrayList<OnShelfProductModelRes>();

		try {
			List<OnShelfProductModel> onShelfProductModels = new ArrayList<OnShelfProductModel>();

			// agents data setting
			onShelfProductModels = AgentProductPriceRepository.findStockByOnshelfInfo(
					api3007Req.getAgencyName(),
					api3007Req.getProductType(),
					api3007Req.getProductStatus(),
					api3007Req.getValue());
			
			logger.info(Const.FIND_DATA_COUNT + onShelfProductModels.size());

			// get all agents
			for (OnShelfProductModel onShelfProductModel : onShelfProductModels) {
				OnShelfProductModelRes onShelfProductModelRes = new OnShelfProductModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(onShelfProductModel, onShelfProductModelRes);
				onShelfProductModelResList.add(onShelfProductModelRes);
			}

			return onShelfProductModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API3007 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API3007 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API3007 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API3007Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API3007Service getErrRes");

		API3007Res api3007Res = new API3007Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api3007Res.setResultCode(resultCode);
		api3007Res.setMessageCode(errorCode);
		api3007Res.setMessage(errMsg);
		return api3007Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API3007Res response class
	 */
	public API3007Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API3007Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API3007Res setErrRes(CustomHandleException e) {

		logger.debug("API3007Service setErrRes");

		API3007Res api3007Res = new API3007Res();
		api3007Res.setResultCode(e.getErrorMessage().getResultCode());
		api3007Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api3007Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api3007Res.setMessage(e.getErrorMessage().getMessage());
		return api3007Res;
	}
}
