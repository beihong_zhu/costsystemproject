package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.CapitalInfoModel;
import lombok.Getter;
import lombok.Setter;

/**
 * . capital_status
 */
@Getter
@Setter
@Entity
@Table(name = "capital_status")
@IdClass(CapitalStatusPK.class)
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "CapitalStatus.CapitalInfoModel", classes = {
		@ConstructorResult(targetClass = CapitalInfoModel.class, columns = {
				@ColumnResult(name = "req_id", type = String.class),
				@ColumnResult(name = "order_id", type = String.class),
				@ColumnResult(name = "agent_reduce_name", type = String.class),
				@ColumnResult(name = "order_form", type = String.class),
				@ColumnResult(name = "product_name", type = String.class),
				@ColumnResult(name = "capital", type = String.class),
				@ColumnResult(name = "price", type = String.class),
				@ColumnResult(name = "status", type = String.class),
				@ColumnResult(name = "created_datetime", type = String.class)
				})
		})
@SqlResultSetMapping(name = "CapitalStatus.CapitalRecordModel", classes = {
		@ConstructorResult(targetClass = CapitalInfoModel.class, columns = {
				@ColumnResult(name = "agent_name", type = String.class),
				@ColumnResult(name = "req_id", type = String.class),
				@ColumnResult(name = "order_id", type = String.class),
				@ColumnResult(name = "bill_id", type = String.class),
				@ColumnResult(name = "agent_reduce_name", type = String.class),
				@ColumnResult(name = "order_form", type = String.class),
				@ColumnResult(name = "order_type", type = String.class),
				@ColumnResult(name = "capital", type = String.class),
				@ColumnResult(name = "price", type = String.class),
				@ColumnResult(name = "status", type = String.class),
				@ColumnResult(name = "created_datetime", type = String.class)
				})
		})
public class CapitalStatus {

	/**
	 * . 订单编号
	 */
	@Id
	@Column(name = "order_id")
	private String orderId;

	/**
	 * . 代理商ID
	 */
	@Id
	@Column(name = "agent_id")
	private String agentId;

	/**
	 * . 面值
	 */
	@Column(name = "money")
	private Integer money;

	/**
	 * . 价格
	 */
	@Column(name = "price")
	private Double price;

	/**
	 * . 资金状态
	 */
	@Column(name = "capital_status")
	private String capitalStatus;

	/**
	 * . 资金量
	 */
	@Column(name = "capital")
	private Double capital;

	/**
	 * . 资金预警值
	 */
	@Column(name = "capital_warn")
	private String capitalWarn;

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
