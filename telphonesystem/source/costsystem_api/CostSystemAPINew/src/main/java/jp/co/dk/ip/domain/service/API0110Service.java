package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0110Req;
import jp.co.dk.ip.application.response.API0110Res;
import jp.co.dk.ip.application.response.data.AgentCapitalInfoModelRes;
import jp.co.dk.ip.application.response.data.CapitalInfoModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentCapitalInfoModel;
import jp.co.dk.ip.domain.model.CapitalInfoModel;
import jp.co.dk.ip.domain.repository.AgentsRepository;
import jp.co.dk.ip.domain.repository.CapitalStatusRepository;

/**
 * agent search API Service
 */
@Service
@Transactional
public class API0110Service {

	private static final Logger logger = LoggerFactory.getLogger(API0110Service.class);

	@Autowired
	private AgentsRepository agentsRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * agent capital getting process
	 *
	 * @param api0110Req agent capital getting request
	 * @return result
	 */
	public List<AgentCapitalInfoModelRes> getAgentCapitalInfoModelResList(API0110Req api0110Req) throws Exception {

		logger.debug("API0110Service getAgentCapitalInfoModelResList");

		try {
			List<AgentCapitalInfoModel> agentCapitalInfoModels = new ArrayList<AgentCapitalInfoModel>();

			// agent capital info setting
			agentCapitalInfoModels = agentsRepository.findAgentCapitalByAgentInfo(
					api0110Req.getAgentId(),
					api0110Req.getAgentName(),
					api0110Req.getAgentReduceName());
			logger.info(Const.FIND_DATA_COUNT + agentCapitalInfoModels.size());

			// transfer capital data
			Date createdDatetime = new Date();
			List<AgentCapitalInfoModelRes> agentCapitalInfoModelResList = new ArrayList<AgentCapitalInfoModelRes>();
			for (AgentCapitalInfoModel agentCapitalInfoModel : agentCapitalInfoModels) {
				AgentCapitalInfoModelRes agentCapitalInfoModelRes = new AgentCapitalInfoModelRes();
				agentCapitalInfoModelRes.setAgentId(agentCapitalInfoModel.getAgentId());
				agentCapitalInfoModelRes.setAgentReduceName(agentCapitalInfoModel.getAgentReduceName());
				agentCapitalInfoModelRes.setProvince(agentCapitalInfoModel.getProvince());
				agentCapitalInfoModelRes.setCity(agentCapitalInfoModel.getCity());
				agentCapitalInfoModelRes.setBusinessMode(agentCapitalInfoModel.getBusinessMode());
				agentCapitalInfoModelRes.setCapital(Double.valueOf(agentCapitalInfoModel.getCapital()));
				agentCapitalInfoModelRes.setInterfaceStatus(agentCapitalInfoModel.getInterfaceStatus());
				agentCapitalInfoModelRes.setCreditLimit(Double.valueOf(agentCapitalInfoModel.getCreditLimit()));

				// pay time setting
				if (!Objects.equals(agentCapitalInfoModel.getCreatedDatetime(), null)) {
					createdDatetime = Utils.dateStringToDateMillSecondByUpper(agentCapitalInfoModel.getCreatedDatetime());
					String strCreatedDatetime = createdDatetime != null ? Utils.dataToStringMillSecond(createdDatetime) : null;
					agentCapitalInfoModelRes.setCreatedDatetime(strCreatedDatetime);
				}

				agentCapitalInfoModelResList.add(agentCapitalInfoModelRes);
			}

			return agentCapitalInfoModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0109 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0110 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0110 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API0110Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API0110Service getErrRes");

		API0110Res api0110Res = new API0110Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api0110Res.setResultCode(resultCode);
		api0110Res.setMessageCode(errorCode);
		api0110Res.setMessage(errMsg);
		return api0110Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0110Res response class
	 */
	public API0110Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API0110Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API0110Res setErrRes(CustomHandleException e) {

		logger.debug("API0110Service setErrRes");

		API0110Res api0110Res = new API0110Res();
		api0110Res.setResultCode(e.getErrorMessage().getResultCode());
		api0110Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api0110Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api0110Res.setMessage(e.getErrorMessage().getMessage());
		return api0110Res;
	}
}
