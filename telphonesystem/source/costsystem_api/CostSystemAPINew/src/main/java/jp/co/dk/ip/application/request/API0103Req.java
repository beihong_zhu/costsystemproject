package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * stock search API Request
 */
@Getter
@Setter
public class API0103Req extends BaseReq {

	// 渠道编号
	@Length(max = 100, message = "{ECT010}")
	private String channelNumber;

	// 渠道简称
	@Length(max = 10, message = "{ECT010}")
	private String channelAbbre;

	// 商品类型
	@NotEmpty(message = "{ECT008}")
	@Length(max = 20, message = "{ECT010}")
	private String productMode;

	// 商品状态
	@NotEmpty(message = "{ECT008}")
	@Length(max = 5, message = "{ECT010}")
	private String productStatus;

	// 省份
	private String[] provinceList;
}