package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3005Req extends BaseReq {
	
	// 代理流水
	@Length(max = 20, message = "{ECT010}")
	private String agencyLimited;

	// 代理名称
	@Length(max = 8, message = "{ECT010}")
	private String agentName;

	// 本地流水
    @NotEmpty(message = "{ECT008}")
    @Length(max = 8, message = "{ECT010}")
    private String localLimited;
    
	// 代理账号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String agencyacount;
 
    
	// 交易方式
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String bussinessMode;
    
	// 交易方式
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String stranctionMode;
    
    // 渠道流水
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String channelLimited;
    
       
	// 代理简称
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String agencyabbreviate;
    
    
	// 交易时间
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String ocrManufacturingDate;
    
    
    

}
