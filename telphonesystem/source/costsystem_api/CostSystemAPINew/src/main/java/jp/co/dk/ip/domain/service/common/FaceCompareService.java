package jp.co.dk.ip.domain.service.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.FaceCompareModel;
import jp.co.dk.ip.domain.service.API0101Service;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

@Service
public class FaceCompareService {
	
    private static final Logger logger = LoggerFactory.getLogger(API0101Service.class);
    
	@SuppressWarnings("rawtypes")
	public static Double RunFaceCompare(
			String token1, 
			String token2, 
			MessageSource messageSource,
			SettingContext settingContext) throws Exception{
		
		Double confidence = Const.COUNT_0_00;
        
		List<BasicNameValuePair> formparams = new ArrayList<>();
		formparams.add(new BasicNameValuePair(Const.API_KEY, settingContext.getFacePlusPlusApiKey()));
		formparams.add(new BasicNameValuePair(Const.API_SECRET, settingContext.getFacePlusPlusApiSecret()));
		formparams.add(new BasicNameValuePair(Const.FACE_TOKEN_1, token1));
		formparams.add(new BasicNameValuePair(Const.FACE_TOKEN_2, token2));

        try{
        	confidence = post(formparams, settingContext.getFacePlusPlusUrlCompare(), messageSource, settingContext);
        	return confidence;
        } catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000011, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000011, message);
            throw new CustomHandleException(errorRes);	
        }
    }

	/**
	 * 发送 post请求访问本地应用并根据传递参数不同返回不同结果
	 */
	@SuppressWarnings({ "finally", "rawtypes" })
	public static Double post(
			List<BasicNameValuePair> formparams, 
			String url,
			MessageSource messageSource,
			SettingContext settingContext){
		// 创建默认的httpClient实例.
		CloseableHttpClient httpclient = HttpClients.createDefault();
		// 创建httppost
		HttpPost httppost = new HttpPost(url);
		UrlEncodedFormEntity uefEntity;
		Double confidence = Const.COUNT_0_00;
		try {
			uefEntity = new UrlEncodedFormEntity(formparams, Const.UNITCODE_TYPE.UTF_8);
			httppost.setEntity(uefEntity);
			System.out.println("executing request " + httppost.getURI());
			CloseableHttpResponse response = httpclient.execute(httppost);
			try {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String output = Const.EMPTY_STRING;
					output = EntityUtils.toString(entity, Const.UNITCODE_TYPE.UTF_8);
					Gson gson = new Gson();
					FaceCompareModel faceCompareModel = gson.fromJson(output, FaceCompareModel.class);
					confidence = Double.valueOf(faceCompareModel.getConfidence());
				}
				return confidence;
			} catch (RuntimeException runEx) {
	            logger.error(runEx.getMessage(), runEx);
				ErrorRes errorRes = new ErrorRes();
	            String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000011, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
	            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000011, message);
	            throw new CustomHandleException(errorRes);
			} finally {
				response.close();
			}
		} catch (ClientProtocolException cpEx) {
            logger.error(cpEx.getMessage(), cpEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000011, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000011, message);
            throw new CustomHandleException(errorRes);
		} catch (UnsupportedEncodingException ueEx) {
            logger.error(ueEx.getMessage(), ueEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000011, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000011, message);
            throw new CustomHandleException(errorRes);
		} catch (IOException ioEx) {
            logger.error(ioEx.getMessage(), ioEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000011, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000011, message);
            throw new CustomHandleException(errorRes);
		} finally {
			// 关闭连接,释放资源
			try {
				httpclient.close();
				return confidence;
			} catch (IOException ioEx) {
	            logger.error(ioEx.getMessage(), ioEx);
				ErrorRes errorRes = new ErrorRes();
	            String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000011, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
	            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000011, message);
	            throw new CustomHandleException(errorRes);
			}
		}
	}
}
