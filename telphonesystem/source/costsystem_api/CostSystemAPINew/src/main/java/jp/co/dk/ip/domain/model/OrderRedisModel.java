package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.domain.model.data.OrderInfoPostReq;
import lombok.Getter;
import lombok.Setter;

/**
 * . orders
 */
@Getter
@Setter
public class OrderRedisModel {

	private String orderId;

	private String corpId;

	private String reqId;

	private String billId;

	private String ts;

	private Integer money;

	private String spId;

	private String provId;

	private String number;

	private String retUrl;

	private Integer status;

	private Double price;

	private String orderForm;

	private String orderType;

	private String refId;

	private String capitalStatus;

	private Double capital;

	private String capitalWarn;
	
    // order post request
    private OrderInfoPostReq orderInfoPostReq;
    
    // order post url
    private String url;
    
    private String authoryResult;
    
    private String sign;
    
    // lock (1-lock, 0-unlock)
    private String lock;
    
    // run (1-run, 0-no run)
    private String isRun;
}
