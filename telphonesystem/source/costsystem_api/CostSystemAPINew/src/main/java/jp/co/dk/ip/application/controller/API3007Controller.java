package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3007Req;
import jp.co.dk.ip.application.response.API3007Res;
import jp.co.dk.ip.application.response.data.OnShelfProductModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3007Service;

/**
 *  onshelf API Controller
 */
@RestController
@RequestMapping("/api")
public class API3007Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3007Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3007Service api3007Service;

	@PostMapping("/API3007")
	private API3007Res agentSummaryProcess(@RequestBody @Valid API3007Req api3007Req, BindingResult bindingResult) {

		logger.debug("API3007Controller agentSummaryProcess");

		API3007Res api3007Res = new API3007Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3007Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<OnShelfProductModelRes> onShelfProductModelResList = new ArrayList<OnShelfProductModelRes>();
			onShelfProductModelResList = api3007Service.getAgentNamesResList(api3007Req);
			
			// response setting
			if (!Objects.equals(onShelfProductModelResList, null)) {
				api3007Res.setResultCnt(onShelfProductModelResList.size());
				api3007Res.setOnShelfProductModelResList(onShelfProductModelResList);
				api3007Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API3007 + e.getMessage(), e);
			api3007Res = api3007Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API3007 + e.getMessage(), e);
			api3007Res = api3007Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API3007);
		}

		return api3007Res;
	}
}
