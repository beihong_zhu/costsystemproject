package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;
import jp.co.dk.ip.application.response.API0102Res;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.SysUsers;
import jp.co.dk.ip.domain.model.SysUserInfoModel;
import jp.co.dk.ip.domain.repository.SysUsersRepository;

/**
 * user info getting API Service
 */
@Service
@Transactional
public class API0102Service {

    private static final Logger logger = LoggerFactory.getLogger(API0102Service.class);

    @Autowired
    private SysUsersRepository sysUsersRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * user info getting process
     *
     * @param pictureMultipartFile face picture
     * @return face recognization result
     */
	public List<SysUserInfoModel> getSysUserInfoModel(String userId, String userName) throws Exception {

        logger.debug("API0102Service getSysUserInfoModel");

        try {
        	List<SysUsers> sysUsers = new ArrayList<SysUsers>();
			if ((!Objects.equals(userId, null) && !Objects.equals(userId, String.valueOf(Const.COUNT_0)))
					&& (!Objects.equals(userName, null) && !Objects.equals(userName, Const.EMPTY_STRING))) {
				sysUsers = sysUsersRepository.findByUserIdAndUserNameAndIsDeleted(
						userId, 
						userName,
						Const.INT_DELETE_FLG_OFF);
				logger.info(Const.FIND_DATA_COUNT + sysUsers.size());
			} else if (!Objects.equals(userId, null) && !Objects.equals(userId, String.valueOf(Const.COUNT_0))) {
				sysUsers = sysUsersRepository.findByUserIdAndIsDeleted(userId, Const.INT_DELETE_FLG_OFF);
				logger.info(Const.FIND_DATA_COUNT + sysUsers.size());
			} else if (!Objects.equals(userName, null) && !Objects.equals(userName, Const.EMPTY_STRING)) {
				sysUsers = sysUsersRepository.findByUserNameAndIsDeleted(userName, Const.INT_DELETE_FLG_OFF);
				logger.info(Const.FIND_DATA_COUNT + sysUsers.size());
			}
        	List<SysUserInfoModel> sysUserInfoModels = new ArrayList<SysUserInfoModel>();
			for (SysUsers sysUser : sysUsers) {
				SysUserInfoModel sysUserInfoModel = new SysUserInfoModel();
				sysUserInfoModel.setUserId(sysUser.getUserId());
				sysUserInfoModel.setUserName(sysUser.getUserName());
				sysUserInfoModel.setCompanyName(sysUser.getCompanyName());
				sysUserInfoModel.setAuthorityId(sysUser.getAuthorityId());
				sysUserInfoModel.setFaceSetToken(sysUser.getFaceSetToken());
				sysUserInfoModel.setIsLogin(sysUser.getIsLogin());
				sysUserInfoModel.setMailAddress(sysUser.getMailAddress());
				
				sysUserInfoModels.add(sysUserInfoModel);
			}
            return sysUserInfoModels;
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0102 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0102 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0102 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public API0102Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("API0102Service getErrRes");

        API0102Res api0102Res = new API0102Res();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        api0102Res.setResultCode(resultCode);
        api0102Res.setMessageCode(errorCode);
        api0102Res.setMessage(errMsg);
        return api0102Res;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return API0102Res response class
     */
    public API0102Res getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("API0102Service getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public API0102Res setErrRes(CustomHandleException e) {

        logger.debug("API0102Service setErrRes");

        API0102Res api0102Res = new API0102Res();
        api0102Res.setResultCode(e.getErrorMessage().getResultCode());
        api0102Res.setResultCnt(e.getErrorMessage().getResultCnt());
        api0102Res.setMessageCode(e.getErrorMessage().getMessageCode());
        api0102Res.setMessage(e.getErrorMessage().getMessage());
        return api0102Res;
    }
}
