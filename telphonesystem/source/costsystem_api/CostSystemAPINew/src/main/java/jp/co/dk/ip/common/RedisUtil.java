package jp.co.dk.ip.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.model.OrderRedisModel;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * this.redisTemplate 工具类
 */
@Slf4j
@Component
public class RedisUtil {

	private RedisTemplate redisTemplate;
    public RedisUtil(RedisTemplate redisTemplate) {
    	this.redisTemplate = redisTemplate;
    }
    
    // 获取锁的超时时间
    private static final Long SUCCESS = 1L;
    
    // timeout
    private long timeout = 9999;

    //- - - - - - - - - - - - - - - - - - - - -  公共方法 - - - - - - - - - - - - - - - - - - - -

    @SuppressWarnings("unchecked")
	public boolean deleteByKey(String key) {
       return this.redisTemplate.delete(key);
    }

    /**
     * 给一个指定的 key 值附加过期时间
     *
     * @param key
     * @param time
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean expire(String key, long time) {
        return this.redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public long getTime(String key) {
        return this.redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean hasKey(String key) {
        return this.redisTemplate.hasKey(key);
    }

    /**
     * 移除指定key 的过期时间
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean persist(String key) {
        return this.redisTemplate.boundValueOps(key).persist();
    }

    //- - - - - - - - - - - - - - - - - - - - -  String类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 根据key获取值
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : this.redisTemplate.opsForValue().get(key);
    }

    /**
     * 将值放入缓存
     *
     * @param key   键
     * @param value 值
     * @return true成功 false 失败
     */
    @SuppressWarnings("unchecked")
	public void set(String key, String value) {
        this.redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 将值放入缓存并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) -1为无期限
     * @return true成功 false 失败
     */
    @SuppressWarnings("unchecked")
	public void set(String key, String value, long time) {
        if (time > 0) {
            this.redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
        } else {
            this.redisTemplate.opsForValue().set(key, value);
        }
    }

    /**
     * 批量添加 key (重复的键会覆盖)
     *
     * @param keyAndValue
     */
    @SuppressWarnings("unchecked")
	public void batchSet(Map<String, String> keyAndValue) {
        this.redisTemplate.opsForValue().multiSet(keyAndValue);
    }

    /**
     * 批量添加 key-value 只有在键不存在时,才添加
     * map 中只要有一个key存在,则全部不添加
     *
     * @param keyAndValue
     */
    @SuppressWarnings("unchecked")
	public void batchSetIfAbsent(Map<String, String> keyAndValue) {
        this.redisTemplate.opsForValue().multiSetIfAbsent(keyAndValue);
    }

    /**
     * 对一个 key-value 的值进行加减操作,
     * 如果该 key 不存在 将创建一个key 并赋值该 number
     * 如果 key 存在,但 value 不是长整型 ,将报错
     *
     * @param key
     * @param number
     */
    @SuppressWarnings("unchecked")
	public Long increment(String key, long number) {
        return this.redisTemplate.opsForValue().increment(key, number);
    }

    /**
     * 对一个 key-value 的值进行加减操作,
     * 如果该 key 不存在 将创建一个key 并赋值该 number
     * 如果 key 存在,但 value 不是 纯数字 ,将报错
     *
     * @param key
     * @param number
     */
    @SuppressWarnings("unchecked")
	public Double increment(String key, double number) {
        return this.redisTemplate.opsForValue().increment(key, number);
    }

    //- - - - - - - - - - - - - - - - - - - - -  set类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 将数据放入set缓存
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public void sSet(String key, String value) {
        this.redisTemplate.opsForSet().add(key, value);
    }

    /**
     * 获取变量中的值
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public Set<Object> members(String key) {
        return this.redisTemplate.opsForSet().members(key);
    }

    /**
     * 随机获取变量中指定个数的元素
     *
     * @param key   键
     * @param count 值
     * @return
     */
    @SuppressWarnings("unchecked")
	public void randomMembers(String key, long count) {
        this.redisTemplate.opsForSet().randomMembers(key, count);
    }

    /**
     * 随机获取变量中的元素
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object randomMember(String key) {
        return this.redisTemplate.opsForSet().randomMember(key);
    }

    /**
     * 弹出变量中的元素
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object pop(String key) {
        return this.redisTemplate.opsForSet().pop("setValue");
    }

    /**
     * 获取变量中值的长度
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public long size(String key) {
        return this.redisTemplate.opsForSet().size(key);
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    @SuppressWarnings("unchecked")
	public boolean sHasKey(String key, Object value) {
        return this.redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 检查给定的元素是否在变量中。
     *
     * @param key 键
     * @param obj 元素对象
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean isMember(String key, Object obj) {
        return this.redisTemplate.opsForSet().isMember(key, obj);
    }

    /**
     * 转移变量的元素值到目的变量。
     *
     * @param key     键
     * @param value   元素对象
     * @param destKey 元素对象
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean move(String key, String value, String destKey) {
        return this.redisTemplate.opsForSet().move(key, value, destKey);
    }

    /**
     * 批量移除set缓存中元素
     *
     * @param key    键
     * @param values 值
     * @return
     */
    @SuppressWarnings("unchecked")
	public void remove(String key, Object... values) {
        this.redisTemplate.opsForSet().remove(key, values);
    }

    /**
     * 通过给定的key求2个set变量的差值
     *
     * @param key     键
     * @param destKey 键
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Set<Set> difference(String key, String destKey) {
        return this.redisTemplate.opsForSet().difference(key, destKey);
    }


    //- - - - - - - - - - - - - - - - - - - - -  hash类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 加入缓存
     *
     * @param key 键
     * @param map 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public void add(String key, Map<String, String> map) {
        this.redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 获取 key 下的 所有  hashkey 和 value
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public Map<Object, Object> getHashEntries(String key) {
        return this.redisTemplate.opsForHash().entries(key);
    }

    /**
     * 验证指定 key 下 有没有指定的 hashkey
     *
     * @param key
     * @param hashKey
     * @return
     */
    @SuppressWarnings("unchecked")
	public boolean hashKey(String key, String hashKey) {
        return this.redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * 获取指定key的值string
     *
     * @param key  键
     * @param key2 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public String getMapString(String key, String key2) {
        return this.redisTemplate.opsForHash().get("map1", "key1").toString();
    }

    /**
     * 获取指定的值Int
     *
     * @param key  键
     * @param key2 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public Integer getMapInt(String key, String key2) {
        return (Integer) this.redisTemplate.opsForHash().get("map1", "key1");
    }

    /**
     * 弹出元素并删除
     *
     * @param key 键
     * @return
     */
    @SuppressWarnings("unchecked")
	public String popValue(String key) {
        return this.redisTemplate.opsForSet().pop(key).toString();
    }

//    /**
//     * 删除指定 hash 的 HashKey
//     *
//     * @param key
//     * @param hashKeys
//     * @return 删除成功的 数量
//     */
//    @SuppressWarnings("unchecked")
//	public Long delete(String key, String... hashKeys) {
//        return this.redisTemplate.opsForHash().delete(key, hashKeys);
//    }

    /**
     * 给指定 hash 的 hashkey 做增减操作
     *
     * @param key
     * @param hashKey
     * @param number
     * @return
     */
    @SuppressWarnings("unchecked")
	public Long increment(String key, String hashKey, long number) {
        return this.redisTemplate.opsForHash().increment(key, hashKey, number);
    }

    /**
     * 给指定 hash 的 hashkey 做增减操作
     *
     * @param key
     * @param hashKey
     * @param number
     * @return
     */
    @SuppressWarnings("unchecked")
	public Double increment(String key, String hashKey, Double number) {
        return this.redisTemplate.opsForHash().increment(key, hashKey, number);
    }

    /**
     * 获取 key 下的 所有 key 字段
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public Set<Object> getKeys(String key) {
        return this.redisTemplate.opsForHash().keys(key);
    }

    /**
     * 获取指定 hash 下面的 键值对 数量
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public Long hashSize(String key) {
        return this.redisTemplate.opsForHash().size(key);
    }

    //- - - - - - - - - - - - - - - - - - - - -  list类型 - - - - - - - - - - - - - - - - - - - -

    /**
     * 在变量左边添加元素值
     *
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
	public void leftPush(String key, Object value) {
        this.redisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 获取集合指定位置的值。
     *
     * @param key
     * @param index
     * @return
     */
    @SuppressWarnings("unchecked")
	public Object index(String key, long index) {
        return this.redisTemplate.opsForList().index("list", 1);
    }

    /**
     * 获取指定区间的值。
     *
     * @param key
     * @param start
     * @param end
     * @return
     */
    @SuppressWarnings("unchecked")
	public List<Object> range(String key, long start, long end) {
        return this.redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 把最后一个参数值放到指定集合的第一个出现中间参数的前面，
     * 如果中间参数值存在的话。
     *
     * @param key
     * @param pivot
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
	public void leftPush(String key, String pivot, String value) {
        this.redisTemplate.opsForList().leftPush(key, pivot, value);
    }

//    /**
//     * 向左边批量添加参数元素。
//     *
//     * @param key
//     * @param values
//     * @return
//     */
//    @SuppressWarnings("unchecked")
//	public void leftPushAll(String key, String... values) {
////        this.redisTemplate.opsForList().leftPushAll(key,"w","x","y");
//        this.redisTemplate.opsForList().leftPushAll(key, values);
//    }

    /**
     * 向集合最右边添加元素。
     *
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
	public void leftPushAll(String key, String value) {
        this.redisTemplate.opsForList().rightPush(key, value);
    }

//    /**
//     * 向左边批量添加参数元素。
//     *
//     * @param key
//     * @param values
//     * @return
//     */
//
//	@SuppressWarnings("unchecked")
//	public void rightPushAll(String key, String... values) {
//        //this.redisTemplate.opsForList().leftPushAll(key,"w","x","y");
//        this.redisTemplate.opsForList().rightPushAll(key, values);
//    }

    /**
     * 向已存在的集合中添加元素。
     *
     * @param key
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
	public void rightPushIfPresent(String key, Object value) {
        this.redisTemplate.opsForList().rightPushIfPresent(key, value);
    }

    /**
     * 向已存在的集合中添加元素。
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public long listLength(String key) {
        return this.redisTemplate.opsForList().size(key);
    }

    /**
     * 移除集合中的左边第一个元素。
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public void leftPop(String key) {
        this.redisTemplate.opsForList().leftPop(key);
    }

    /**
     * 移除集合中左边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public void leftPop(String key, long timeout, TimeUnit unit) {
        this.redisTemplate.opsForList().leftPop(key, timeout, unit);
    }

    /**
     * 移除集合中右边的元素。
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public void rightPop(String key) {
        this.redisTemplate.opsForList().rightPop(key);
    }

    /**
     * 移除集合中右边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public void rightPop(String key, long timeout, TimeUnit unit) {
        this.redisTemplate.opsForList().rightPop(key, timeout, unit);
    }
    
    @SuppressWarnings("unchecked")
    public void flushAll() {
        this.redisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.flushAll();
                return null;
            }
        });
    }

    /**
     * 	取所有缓存中key
     *
     * @param key
     * @return
     */
    @SuppressWarnings("unchecked")
	public Set<String> getAllKeys(String key) {
    	return this.redisTemplate.keys(key);
    }
    
   	/**
   	 * 读取缓存
   	 * 
   	 * @param key
   	 * @return
   	 */
   	public String getValue(final String key) {
   		return (String) this.redisTemplate.opsForValue().get(key);
   	}
    
   	/**
   	 * 写入缓存
   	 */
   	public boolean setValue(final String key, String value) {
   		boolean result = false;
   		try {
   			this.redisTemplate.opsForValue().set(key, value);
   			result = true;
   		} catch (Exception e) {
   			e.printStackTrace();
   		}
   		return result;
   	}
   	
   	/**
   	 * 写入缓存
   	 */
   	@SuppressWarnings("unchecked")
	public boolean setObjectValue(final String key, Object value) {
   		boolean result = false;
   		try {
   			this.redisTemplate.boundValueOps(key).set(value);
   			result = true;
   		} catch (Exception e) {
   			e.printStackTrace();
   		}
   		return result;
   	}
    
   	/**
   	 * 更新缓存
   	 */
   	@SuppressWarnings("unchecked")
	public boolean getAndSetKey(final String key, String value) {
   		boolean result = false;
   		try {
   			this.redisTemplate.opsForValue().getAndSet(key, value);
   			result = true;
   		} catch (Exception e) {
   			e.printStackTrace();
   		}
   		return result;
   	}
    
   	/**
   	 * 删除缓存
   	 */
   	@SuppressWarnings("unchecked")
   	public boolean delete(final String key) {
   		boolean result = false;
   		try {
   			this.redisTemplate.delete(key);
   			result = true;
   		} catch (Exception e) {
   			e.printStackTrace();
   		}
   		return result;
   	}
   	
    /**
     * 加锁，无阻塞
     *
     * @param this.redisTemplate
     * @param key
     * @param value
     * @param expireTime
     * @return Boolean
     */
    public Boolean tryLock(RedisTemplate<String, JSONObject> redisTemplate, String key, JSONObject value, long expireTime) {
        Boolean result = true;
    	try {
            // SET命令返回OK ，则证明获取锁成功
    		result = redisTemplate.opsForValue().setIfAbsent(key, value, expireTime, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
    	return result;
    }
 
    /**
     * 解锁
     *
     * @param this.redisTemplate
     * @param key
     * @param value
     * @return Boolean
     */
    public static Boolean unlock(RedisTemplate<String, JSONObject> redisTemplate, String key, JSONObject value) {
        Boolean result = true;
    	try {
    		
//    		JSONObject currentValue = redisTemplate.opsForValue().get(key);
//            if (Objects.equals(currentValue, value)) {
//            	result = redisTemplate.opsForValue().getOperations().delete(key);
//            }
            
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            RedisScript<String> redisScript = new DefaultRedisScript<>(script, String.class);
            // redis脚本执行
            Long ret = 0L;
            ret = redisTemplate.delete(Collections.singletonList(key));
            // getRet = redisTemplate.execute(redisScript, Collections.singletonList(key), value);
            if (Objects.equals(ret, SUCCESS)) {
            	result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }
    
    /**
     * @author zbh
     * description: rename
     * @DateTime: 9:58 2021/11/29
     * @Params oldKey, newkey
     * @return boolean
     */
    public boolean rname(String oldKey, String newkey) {
        return this.redisTemplate.renameIfAbsent(oldKey, newkey);
    }
}
