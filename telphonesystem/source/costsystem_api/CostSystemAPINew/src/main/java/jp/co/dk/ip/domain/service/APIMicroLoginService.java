package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.APIMicroLoginReq;
import jp.co.dk.ip.application.response.APIMicroLoginRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.RedisUtil;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.quartz.service.common.APIRedisService;

/**
 * micro login API Service
 */
@Service
@Transactional
public class APIMicroLoginService {

    private static final Logger logger = LoggerFactory.getLogger(APIMicroLoginService.class);

    @Autowired
    private RedisUtil redisUtil;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    @Autowired
    private APIRedisService apiRedisService;
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIMicroLoginRes agentDataProcess(APIMicroLoginReq apiMicroLoginReq) throws Exception {
		
        logger.debug("APIMicroLoginService agentDataProcess");
        
        APIMicroLoginRes apiMicroLoginRes = new APIMicroLoginRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.MICRO_LOGIN_API.CORPID + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getCorpId()
        			+ Const.MICRO_LOGIN_API.AND + Const.MICRO_LOGIN_API.REQID + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getReqId()
        			+ Const.MICRO_LOGIN_API.AND + Const.MICRO_LOGIN_API.USERNAME + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getUsername()
        			+ Const.MICRO_LOGIN_API.AND + Const.MICRO_LOGIN_API.PASSWORD + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getPassword()
        			+ Const.MICRO_LOGIN_API.AND + Const.MICRO_LOGIN_API.TEL + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getTel()
        			+ Const.MICRO_LOGIN_API.AND + Const.MICRO_LOGIN_API.TS + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getTs()
        			+ Const.MICRO_LOGIN_API.AND + Const.MICRO_LOGIN_API.GENDER + Const.MICRO_LOGIN_API.EQUAL + apiMicroLoginReq.getGender();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiMicroLoginReq.getSign())) {
				// response error
				apiMicroLoginRes.setResultCode(Const.RESULT_CODE_2);
				apiMicroLoginRes.setResultCnt(Const.COUNT_0);
				apiMicroLoginRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiMicroLoginRes.setMessage(message);
				return apiMicroLoginRes;
			}
			
			// transfer request object
			OrderRedisMicroUserModel orderRedisMicroUserModel = new OrderRedisMicroUserModel();
			String accountId = Const.EMPTY_STRING;
			accountId = Const.M + Const.PATH_UNDERBAR + apiMicroLoginReq.getUsername();
			orderRedisMicroUserModel = apiRedisService.getOrderRedisMicroUserModel(
					accountId, 
		    		this.redisTemplate);
			if (!Utils.isAllEmpty(orderRedisMicroUserModel)) {
				// response error
				apiMicroLoginRes.setResultCode(Const.RESULT_CODE_3);
				apiMicroLoginRes.setResultCnt(Const.COUNT_0);
				apiMicroLoginRes.setMessageCode(Const.E000019);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000019,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiMicroLoginRes.setMessage(message);
			} else {
				Utils.copyPropertiesIgnoreNull(apiMicroLoginReq, orderRedisMicroUserModel);
	        	
				// enter into queue
				orderRedisMicroUserModel.setIsRun(Const.NORUN);
				orderRedisMicroUserModel.setAccountId(accountId);
				Utils.jinZhanMicroChargeQueue(orderRedisMicroUserModel);
				
	        	// response setting
				apiMicroLoginRes.setAccountId(accountId);
			}
			
			return apiMicroLoginRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIMicroLoginRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIMicroLoginRes apiMicroLoginRes = new APIMicroLoginRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiMicroLoginRes.setResultCode(resultCode);
        apiMicroLoginRes.setMessageCode(errorCode);
        apiMicroLoginRes.setMessage(errMsg);
        return apiMicroLoginRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIMicroLoginRes response class
     */
    public APIMicroLoginRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIMicroLoginService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIMicroLoginRes setErrRes(CustomHandleException e) {

        logger.debug("APIMicroLoginService setErrRes");

        APIMicroLoginRes apiMicroLoginRes = new APIMicroLoginRes();
        apiMicroLoginRes.setResultCode(e.getErrorMessage().getResultCode());
        apiMicroLoginRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiMicroLoginRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiMicroLoginRes.setMessage(e.getErrorMessage().getMessage());
        return apiMicroLoginRes;
    }
}
