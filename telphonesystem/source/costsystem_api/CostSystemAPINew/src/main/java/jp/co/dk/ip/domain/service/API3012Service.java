package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API3012Req;
import jp.co.dk.ip.application.response.API3012Res;
import jp.co.dk.ip.application.response.data.AuthoryInfoRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AuthoryInfoModel;
import jp.co.dk.ip.domain.repository.AuthoriesRepository;

/**
 * all agents summary API Service
 */
@Service
@Transactional
public class API3012Service {

	private static final Logger logger = LoggerFactory.getLogger(API3012Service.class);

	@Autowired
	private AuthoriesRepository authoriesRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents summary
	 *
	 * @param api0112Req all agents summary
	 * @return result
	 */
	public List<AuthoryInfoRes> getAgentNamesResList(API3012Req api3012Req) throws Exception {

		logger.debug("API3012Service getAgentNamesResList");

		List<AuthoryInfoRes> authoryInfoResList = new ArrayList<AuthoryInfoRes>();
		
		try {
			List<AuthoryInfoModel> authoryInfoModels = new ArrayList<AuthoryInfoModel>();

			// agents data setting
			authoryInfoModels = authoriesRepository.findAuthoryInfo(
					api3012Req.getAbbreviation()
				
							
					);
			logger.info(Const.FIND_DATA_COUNT + authoryInfoModels.size());

			// get all agents
			for (AuthoryInfoModel authoryInfoModel : authoryInfoModels) {
				AuthoryInfoRes authoryInfoRes = new AuthoryInfoRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(authoryInfoModel, authoryInfoRes);
				authoryInfoResList.add(authoryInfoRes);
			}

			return authoryInfoResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API3012Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API3012Service getErrRes");

		API3012Res api3012Res = new API3012Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api3012Res.setResultCode(resultCode);
		api3012Res.setMessageCode(errorCode);
		api3012Res.setMessage(errMsg);
		return api3012Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0112Res response class
	 */
	public API3012Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API3012Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API3012Res setErrRes(CustomHandleException e) {

		logger.debug("API0112Service setErrRes");

		API3012Res api3012Res = new API3012Res();
		api3012Res.setResultCode(e.getErrorMessage().getResultCode());
		api3012Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api3012Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api3012Res.setMessage(e.getErrorMessage().getMessage());
		return api3012Res;
	}
}
