package jp.co.dk.ip.domain.model;

import lombok.Getter;
import lombok.Setter;

/**.
 * agent info
 */
@Getter
@Setter
public class AgentInfoModel {

	/**
	 * . 账户名
	 */
	private String agentId;

	/**
	 * . 简称
	 */
	private String agentReduceName;

	/**
	 * . 省
	 */
	private String province;

	/**
	 * . 市
	 */
	private String city;

	/**
	 * . 模式
	 */
	private String businessMode;

	/**
	 * . 可用余额
	 */
	private String capital;

	/**
	 * . 可用与否
	 */
	private String interfaceStatus;

	/**
	 * . 授信额度
	 */
	private String creditLimit;

	/**
	 * . 创建时间
	 */
	private String createdDatetime;
	
	private String retUrl;
	
	private String orderId;
	
	private String spId;
	
	private String productId;
	
	private String corpId;
	
	private String price;
	
	private String money;	
	
	private String number;
	
	private String ts;
	
//	private String provId;
	
	private String reqId;
	
    // run (1-run, 0-no run)
    private String isRun;
}
