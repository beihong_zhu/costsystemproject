package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * authory data reception 001 call back API Response
 */
@Getter
@Setter
public class APIAuthory001CallBackRes extends BaseRes {

    // 返回结果
    private String result;
}
