package jp.co.dk.ip.domain.quartz.service.authory;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import org.apache.http.ParseException;
import com.google.gson.Gson;

import jp.co.dk.ip.application.response.APIAgentOrderResultCallBackRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.entity.Orders;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class AuthoryHttpResponseService {
	
	/**
	 * send request to authory client
	 * 
	 * @return authory client response
	 */
	public static void postAuthoryResponseSuccess(
			String url,
			OrdersRepository ordersRepository,
			String orderId)
			throws InterruptedException, ExecutionException, ParseException, IOException {
		
		// enter into response queue
		Orders order = new Orders();
		order = ordersRepository.findByOrderIdAndIsDeleted(
				orderId,
				Const.INT_DELETE_FLG_OFF);
		order.setStatus(Const.ORDERS_STATUS.THREE);
		ordersRepository.saveAndFlush(order);
		System.out.println(url);
	}
}
