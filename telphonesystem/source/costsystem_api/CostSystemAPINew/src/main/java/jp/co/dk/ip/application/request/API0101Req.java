package jp.co.dk.ip.application.request;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * 登录页面人脸识别API Request
 */
@Getter
@Setter
public class API0101Req extends BaseReq {

}