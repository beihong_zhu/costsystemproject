package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class AgentFundRecorderModelRes {

    // 代理商ID
    private String corpid;
    
    // 代理商流水号
    private String reqid;

    // 代理商账户简称
	private String agentReduceName;
    
    // 交易金额
    private String orderMoney;
    
    // 成功金额
    private String orderSuccessMoney;
    
    // 总笔数
    private String orderTotalSummary;
    
    // 处理中笔数
    private String orderRunningSummary;
    
    // 成功笔数
    private String orderSuccessSummary;
    
    // 成功比率
    private String orderSuccessRate;
    
    // 成功平均时长
    private String orderSuccessTime;
    
    // 失败平均时长
    private String orderFailureTime;
    
    // 三分钟到账率
    private String orderThreeMinsRate;
    
    // 十分钟到账率
    private String orderTenMinsRate;
}
