package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import jp.co.dk.ip.application.request.APIBalanceChargeReq;
import jp.co.dk.ip.application.response.APIBalanceChargeRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OrderRedisMicroUserModel;
import jp.co.dk.ip.domain.quartz.service.common.APIRedisService;

/**
 * balance charge API Service
 */
@Service
@Transactional
public class APIBalanceChargeService {

    private static final Logger logger = LoggerFactory.getLogger(APIBalanceChargeService.class);
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private RedisTemplate redisTemplate;
    
    @Autowired
    private APIRedisService apiRedisService;
    
    /**
     * agent data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIBalanceChargeRes agentDataProcess(APIBalanceChargeReq apiBalanceChargeReq) throws Exception {
		
        logger.debug("APIBalanceChargeService agentDataProcess");
        
        APIBalanceChargeRes apiBalanceChargeRes = new APIBalanceChargeRes();

        try {
			// MD5 validation
        	String originalSign = Const.EMPTY_STRING;
        	String md5Sign = Const.EMPTY_STRING;
        	originalSign = 
        			Const.BALANCE_CHARGE_API.CORPID + Const.BALANCE_CHARGE_API.EQUAL + apiBalanceChargeReq.getCorpId()
        			+ Const.BALANCE_CHARGE_API.AND + Const.BALANCE_CHARGE_API.REQID + Const.BALANCE_SELECT_API.EQUAL + apiBalanceChargeReq.getReqId()
        			+ Const.BALANCE_CHARGE_API.AND + Const.BALANCE_CHARGE_API.ACCOUNTID + Const.BALANCE_SELECT_API.EQUAL + apiBalanceChargeReq.getAccountId()
        			+ Const.BALANCE_CHARGE_API.AND + Const.BALANCE_CHARGE_API.TS + Const.BALANCE_CHARGE_API.EQUAL + apiBalanceChargeReq.getTs()
        			+ Const.BALANCE_CHARGE_API.AND + Const.BALANCE_CHARGE_API.AMOUNT + Const.BALANCE_CHARGE_API.EQUAL + apiBalanceChargeReq.getAmount()
        			+ Const.BALANCE_CHARGE_API.AND + Const.BALANCE_CHARGE_API.NUMBER + Const.BALANCE_CHARGE_API.EQUAL + apiBalanceChargeReq.getNumber();
			md5Sign = Utils.stringToMD5(originalSign);
			if (!Objects.equals(md5Sign, apiBalanceChargeReq.getSign())) {
				// response error
				apiBalanceChargeRes.setResultCode(Const.RESULT_CODE_2);
				apiBalanceChargeRes.setResultCnt(Const.COUNT_0);
				apiBalanceChargeRes.setMessageCode(Const.E000018);
				String message = Const.EMPTY_STRING;
				message = messageSource.getMessage(
						Const.E000018,
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh()));
				apiBalanceChargeRes.setMessage(message);
				return apiBalanceChargeRes;
			}
			
			// transfer request object
			OrderRedisMicroUserModel orderRedisMicroUserModel = new OrderRedisMicroUserModel();
			String accountId = Const.EMPTY_STRING;
			accountId = apiBalanceChargeReq.getAccountId();
			if (!StringUtils.isEmpty(accountId)) {
				orderRedisMicroUserModel = apiRedisService.getOrderRedisMicroUserModel(
						accountId, 
			    		this.redisTemplate);
				if (!Utils.isAllEmpty(orderRedisMicroUserModel)) {
					Utils.copyPropertiesIgnoreNull(apiBalanceChargeReq, orderRedisMicroUserModel);
					orderRedisMicroUserModel.setIsRun(Const.NORUN);
					
					// enter into queue
					Utils.jinZhanMicroChargeQueue(orderRedisMicroUserModel);

		        	// response setting
		    		apiBalanceChargeRes.setChargeStatus(Const.CHARGE_SUCCESS);
		    		apiBalanceChargeRes.setResultCode(Const.RESULT_CODE_0);
				} else {
					// response error
					apiBalanceChargeRes.setResultCode(Const.RESULT_CODE_3);
					apiBalanceChargeRes.setResultCnt(Const.COUNT_0);
					apiBalanceChargeRes.setMessageCode(Const.E000020);
					String message = Const.EMPTY_STRING;
					message = messageSource.getMessage(
							Const.E000020,
							new String[] {},
							Locale.forLanguageTag(settingContext.getLangZh()));
					apiBalanceChargeRes.setMessage(message);
				}
			}

			return apiBalanceChargeRes;
		} catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes<?> errorRes = new ErrorRes<Object>();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAGENT + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
	
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIBalanceChargeRes getErrRes(String[] param, String resultCode, String errorCode, String logError, String... message) {

        logger.debug("APIMicroLoginService getErrRes");

        APIBalanceChargeRes apiBalanceChargeRes = new APIBalanceChargeRes();
        String errMsg = Const.EMPTY_STRING;
        if (Utils.isEmpty(message)) {
            errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));	
        } else {
        	errMsg = message[Const.COUNT_0];
        }
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiBalanceChargeRes.setResultCode(resultCode);
        apiBalanceChargeRes.setMessageCode(errorCode);
        apiBalanceChargeRes.setMessage(errMsg);
        return apiBalanceChargeRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIBalanceChargeRes response class
     */
    public APIBalanceChargeRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIBalanceChargeService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIBalanceChargeRes setErrRes(CustomHandleException e) {

        logger.debug("APIBalanceChargeService setErrRes");

        APIBalanceChargeRes apiBalanceChargeRes = new APIBalanceChargeRes();
        apiBalanceChargeRes.setResultCode(e.getErrorMessage().getResultCode());
        apiBalanceChargeRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiBalanceChargeRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiBalanceChargeRes.setMessage(e.getErrorMessage().getMessage());
        return apiBalanceChargeRes;
    }
}
