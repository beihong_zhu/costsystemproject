package jp.co.dk.ip.domain.quartz.service.authory;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.util.EntityUtils;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.HttpModel;
import jp.co.dk.ip.domain.quartz.service.common.APICommonCallBackService;
import jp.co.dk.ip.domain.repository.OrdersRepository;

public class RunAuthoryCallBackService {

	
	/**
	 * send request for calling back
	 * @param <T>
	 * 
	 * @return none
	 */
	public static <T> void postAuthoryAsync(
			String url,
			T t,
			OrdersRepository ordersRepository,
			String orderId)
			throws InterruptedException, ExecutionException, ParseException, IOException {

		APICommonCallBackService apiCommonCallBackService = new APICommonCallBackService();
		HttpModel httpModel = new HttpModel();
		httpModel = apiCommonCallBackService.postAsync(url, t);
		final CloseableHttpAsyncClient client = httpModel.getClient();
		HttpPost httpPost = httpModel.getHttpPost();

		// start
		client.start();

		// 异步请求
		RunAuthoryCallBack runCallBack = new RunAuthoryCallBack(url, ordersRepository, orderId);
		client.execute(httpPost, runCallBack);
	}
	
	/**
	 * run http request call back
	 */
    private static class RunAuthoryCallBack implements FutureCallback<HttpResponse> {

        private long start = System.currentTimeMillis();
		final CountDownLatch latch = new CountDownLatch(Const.COUNT_1);
		public String output = Const.EMPTY_STRING;
		
		private String url;
		private OrdersRepository ordersRepository;
		private String orderId;
		
		RunAuthoryCallBack(String url, OrdersRepository ordersRepository, String orderId) {
			this.url = url;
			this.ordersRepository = ordersRepository;
			this.orderId = orderId;
		}

		@Override
		public void completed(HttpResponse httpResponse) {

			try {
				output = EntityUtils.toString(httpResponse.getEntity(), Const.UNITCODE_TYPE.UTF_8);

				// response process
				// AuthoryHttpResponseService.postAgentResponse(this.url, ordersRepository, output, this.orderId);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void failed(Exception e) {

			latch.countDown();
			System.err.println("cost is:" + (System.currentTimeMillis() - start) + ":" + e);
			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
		}

		@Override
        public void cancelled() {

			latch.countDown();
			System.out.println(" callback thread id is : " + Thread.currentThread().getId());
        }
    }
}
