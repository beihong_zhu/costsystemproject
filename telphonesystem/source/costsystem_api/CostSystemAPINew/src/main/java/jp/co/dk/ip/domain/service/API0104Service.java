package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API0104Req;
import jp.co.dk.ip.application.response.API0104Res;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Agents;
import jp.co.dk.ip.domain.repository.AgentsRepository;

/**
 * agent adding API Service
 */
@Service
@Transactional
public class API0104Service {

    private static final Logger logger = LoggerFactory.getLogger(API0104Service.class);

    @Autowired
    private AgentsRepository agentsRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * agent adding
     *
     * @param api0104Req agent data
     * @return result
     */
	public void addAgent(API0104Req api0104Req) throws Exception {

        logger.debug("API0104Service addAgent");

        try {
        	// agent data getting
        	List<Agents> agents = new ArrayList<Agents>();
        	agents = agentsRepository.findByIsDeleted(Const.INT_DELETE_FLG_OFF);
        	
        	// set agent id
        	List<Integer> agentIds = new ArrayList<Integer>();
        	for (Agents agent: agents) {
        		Integer agentId = Const.COUNT_0;
        		agentId = Integer.valueOf(agent.getAgentId());
        		agentIds.add(agentId);
        	}
        	// get max agent id
			Integer max = Const.COUNT_0;
			max = agentIds.stream().reduce(Integer::max).get();
        	
        	// agent data setting
        	Agents agent = new Agents();
			agent.setAgentId(String.valueOf(max + Const.COUNT_1));
        	agent.setAgentName(api0104Req.getAgentFullName());
        	agent.setAgentReduceName(api0104Req.getAgentSimpleName());
        	agent.setTel(api0104Req.getTel());
        	agent.setMail(api0104Req.getMail());
        	agent.setAccountName(api0104Req.getAgentAccount());
        	agent.setAddress(api0104Req.getDetailedAddress());
        	agent.setAccountName(api0104Req.getAgentAccount());
        	agent.setAcountStatus(api0104Req.getAgentStatus());
        	agent.setAgentType(api0104Req.getAgentType());
        	agent.setBusinessMode(api0104Req.getBusinessMode());
        	
        	agentsRepository.saveAndFlush(agent);
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0104 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0104 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_API0104 + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public API0104Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("API0104Service getErrRes");

        API0104Res api0104Res = new API0104Res();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        api0104Res.setResultCode(resultCode);
        api0104Res.setMessageCode(errorCode);
        api0104Res.setMessage(errMsg);
        return api0104Res;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return API0104Res response class
     */
    public API0104Res getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("API0104Service getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public API0104Res setErrRes(CustomHandleException e) {

        logger.debug("API0104Service setErrRes");

        API0104Res api0104Res = new API0104Res();
        api0104Res.setResultCode(e.getErrorMessage().getResultCode());
        api0104Res.setResultCnt(e.getErrorMessage().getResultCnt());
        api0104Res.setMessageCode(e.getErrorMessage().getMessageCode());
        api0104Res.setMessage(e.getErrorMessage().getMessage());
        return api0104Res;
    }
}
