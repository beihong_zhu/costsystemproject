package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.CapitalTotalInfoModel;
import jp.co.dk.ip.domain.model.CapitalReducePlusModel;
import jp.co.dk.ip.domain.model.ZeroInfoModel;



/**
 * . capital repository
 */
public interface CapitalTotalCustomRepository {

	List<CapitalTotalInfoModel> findCapitalByCapitalInfo(
			String agencyabbreviation,
			String value,
			String provideSevice,
			String ocrManufacturingDate
		
			);
	
	List<CapitalReducePlusModel> findCapitalPlusReduceByCapitalInfo(
			String agencyacount,
			String agencyname,
			String applymode,
			String status,
			String ocrManufacturingDate
			);
	
	
	List<ZeroInfoModel> findCapitalZeroByCapitalInfo(
			String agencyname
			);
	
	
	
	
	
}
