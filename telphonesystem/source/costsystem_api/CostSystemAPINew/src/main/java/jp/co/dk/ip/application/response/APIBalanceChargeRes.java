package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * balance charge API Response
 */
@Getter
@Setter
public class APIBalanceChargeRes extends BaseRes {
    
    // 充值状态（1-成功，0-失败）
    private String chargeStatus;
}
