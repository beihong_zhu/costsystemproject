package jp.co.dk.ip.domain.quartz.service.agent;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Orders;
import jp.co.dk.ip.domain.model.OrderResultPostAgentModel;
import jp.co.dk.ip.domain.model.data.OrderResultPostReq;
import jp.co.dk.ip.domain.repository.OrdersRepository;

@Service
public class AgentOrderResultPostService {
	
	/**
	 * send request to agent
	 * 
	 * @return agent response
	 */
	public String orderResultPostAgentData(
			String orderId,
			String billId,
			OrdersRepository ordersRepository)
			throws RuntimeException {
		
		// update order status
		Orders order = new Orders();
		order = ordersRepository.findByOrderIdAndIsDeleted(
				orderId,
				Const.INT_DELETE_FLG_OFF);
		if (!StringUtils.isEmpty(order)) {
			// order setting
			order.setStatus(Const.ORDERS_STATUS.THREE);
			order.setBillId(billId);
			ordersRepository.saveAndFlush(order);
			// set queue data
			OrderResultPostReq orderResultPostReq = new OrderResultPostReq();
			orderResultPostReq.setCorpId(order.getCorpId());
			orderResultPostReq.setReqId(order.getReqId());
			orderResultPostReq.setOrderId(order.getOrderId());
			orderResultPostReq.setResult(String.valueOf(order.getStatus()));

			// MD5 sign setting
			String postMd5Sign = Const.EMPTY_STRING;
			postMd5Sign = getAgentOrderResultPostSign(
					orderResultPostReq.getCorpId(),
					orderResultPostReq.getReqId(),
					orderResultPostReq.getOrderId(),
					orderResultPostReq.getResult());
			orderResultPostReq.setSign(postMd5Sign);
			
			// create order result queue data
			OrderResultPostAgentModel orderResultPostAgentModel = new OrderResultPostAgentModel();
			orderResultPostAgentModel.setOrderResultPostReq(orderResultPostReq);
			orderResultPostAgentModel.setUrl(order.getRetUrl());
			
			// enter into order result queue
			Utils.jinZhanOrderResultCallBackQueue(orderResultPostAgentModel);
			
			return Const.AGENT_ORDER_RESULT_CALL_BACK_API.SUCCESS;
		} else {
			// response error
			return Const.AGENT_ORDER_RESULT_CALL_BACK_API.FAILURE;
		}
	}
	
	/**
	 * get agent order result post sign
	 * 
	 * @return agent order result response
	 */
	public String getAgentOrderResultPostSign(
			String corpId,
			String reqId,
			String orderId,
			String result
			) {
		
		// MD5 sign setting
		String postSign = Const.EMPTY_STRING;
		String postMd5Sign = Const.EMPTY_STRING;
		postSign = 
    			Const.AGENT_ORDER_RESULT_CALL_BACK_API.CORPID + Const.AGENT_ORDER_RESULT_CALL_BACK_API.EQUAL + corpId
    			+ Const.AGENT_ORDER_RESULT_CALL_BACK_API.AND + Const.AGENT_ORDER_RESULT_CALL_BACK_API.REQID + Const.AGENT_ORDER_RESULT_CALL_BACK_API.EQUAL + reqId
    			+ Const.AGENT_ORDER_RESULT_CALL_BACK_API.AND + Const.AGENT_ORDER_RESULT_CALL_BACK_API.ORDERID + Const.AGENT_ORDER_RESULT_CALL_BACK_API.EQUAL + orderId
    			+ Const.AGENT_ORDER_RESULT_CALL_BACK_API.AND + Const.AGENT_ORDER_RESULT_CALL_BACK_API.RESULT + Const.AGENT_ORDER_RESULT_CALL_BACK_API.EQUAL + result;
		postMd5Sign = Utils.stringToMD5(postSign);
		return postMd5Sign;
	}
}
