package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import jp.co.dk.ip.application.response.API0101Res;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0101Service;

/**
  * face recognition API Controller
 */
@RestController
@RequestMapping("/api")
public class API0101Controller {

    private static final Logger logger = LoggerFactory.getLogger(API0101Controller.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

    @Autowired
    private API0101Service api0101Service;

    @PostMapping("/API0101")
	private API0101Res findFaceRecognization(
			@RequestParam(value = "pictureFile", required = false) MultipartFile pictureMultipartFile) {
    	
        logger.debug("API0101Controller findFaceRecognization");

        API0101Res api0101Res = new API0101Res();

        // picture validation֤
        if (null == pictureMultipartFile) {
            return api0101Service.getReqErr(new String[] {}, Const.E000008, Const.DBFieldName.PICTURE_CHINESE);
        }

        try {
            // face recognition result
        	boolean faceJudge = api0101Service.findSysUserByFace(pictureMultipartFile);
            
        	// get response
        	if (Objects.equals(faceJudge, true)) {
                api0101Res.setResultCnt(Const.COUNT_1);
                api0101Res.setFaceJudge(faceJudge);
                api0101Res.setResultCode(Const.RESULT_CODE_0);
                logger.info(messageSource.getMessage(
                		Const.I000001, 
                		new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
			} else {
	            api0101Res = api0101Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000005, Const.ERRRES_API0101);
			}
        } catch (CustomHandleException e) {
            logger.error(Const.ERRRES_API0101 + e.getMessage(), e);
            api0101Res = api0101Service.setErrRes(e);
        } catch (Exception e) {
            logger.error(Const.ERRRES_API0101 + e.getMessage(), e);
            api0101Res = api0101Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000005, Const.ERRRES_API0101);
        }

        return api0101Res;
    }
}
