package jp.co.dk.ip.domain.mq;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;

/**
 * @author zbh
 * @description 消息消费者
 * @date 2021-11-28
 */
public class Consumer {

	/*
	 * Constructs a client instance with your account for accessing
	 * DefaultMQConsumer
	 */
	private static DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("ConsumerGroupName");
	private static int initialState = 0;

	private Consumer() {

	}

	public static DefaultMQPushConsumer getDefaultMQPushConsumer() {
		if (consumer == null) {
			consumer = new DefaultMQPushConsumer("ConsumerGroupName");
		}
		if (initialState == 0) {
			consumer.setNamesrvAddr("localhost:9876");
			consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
			initialState = 1;
		}
		return consumer;
	}
}
