package jp.co.dk.ip.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.API3009Req;
import jp.co.dk.ip.application.response.API3009Res;
import jp.co.dk.ip.application.response.data.ProductInfoModelRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.ProductInfoModel;
import jp.co.dk.ip.domain.repository.StocksRepository;

/**
 * all agents summary API Service
 */
@Service
@Transactional
public class API3009Service {

	private static final Logger logger = LoggerFactory.getLogger(API3009Service.class);

	@Autowired
	private StocksRepository stocksRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	/**
	 * all agents summary
	 *
	 * @param api0112Req all agents summary
	 * @return result
	 */
	public List<ProductInfoModelRes> getAgentNamesResList(API3009Req api3009Req) throws Exception {

		logger.debug("API3009Service getAgentNamesResList");

		List<ProductInfoModelRes> agentSummaryModelResList = new ArrayList<ProductInfoModelRes>();
		
		try {
			List<ProductInfoModel> productInfoModels = new ArrayList<ProductInfoModel>();

			// agents data setting
			productInfoModels =stocksRepository.findStockByProductInfo(
					api3009Req.getAggencyLimited(),
					api3009Req.getLocalLimited(),
					api3009Req.getChannelLimited(),
					api3009Req.getOcrManufacturingDate()			
					);
			logger.info(Const.FIND_DATA_COUNT + productInfoModels.size());

			// get all agents
			for (ProductInfoModel productInfoModel : productInfoModels) {
				ProductInfoModelRes productInfoModelRes = new ProductInfoModelRes();
				// transfer request object
				Utils.copyPropertiesIgnoreNull(productInfoModel, productInfoModelRes);
				agentSummaryModelResList.add(productInfoModelRes);
			}

			return agentSummaryModelResList;
		} catch (PessimisticLockingFailureException plfEx) {
			// DB排他
			logger.error(plfEx.getMessage(), plfEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000048, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
			throw new CustomHandleException(errorRes);
		} catch (DataAccessException daEx) {
			logger.error(daEx.getMessage(), daEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000015, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
			throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
			logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
			String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(Const.E000005, new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
			logger.error(Const.ERRRES_API0112 + Const.ERROR_MSG + message);
			errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
			throw new CustomHandleException(errorRes);
		}
	}

	/**
	 * . 错误信息取得
	 *
	 * @param param      错误参数
	 * @param resultCode 返回code
	 * @param errorCode  错误code
	 * @param logError   错误log说明
	 * @return response class
	 */
	public API3009Res getErrRes(String[] param, String resultCode, String errorCode, String logError) {

		logger.debug("API0112Service getErrRes");

		API3009Res api3009Res = new API3009Res();
		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		logger.error(logError + Const.ERROR_CODE + errorCode);
		logger.error(logError + Const.ERROR_MSG + errMsg);
		api3009Res.setResultCode(resultCode);
		api3009Res.setMessageCode(errorCode);
		api3009Res.setMessage(errMsg);
		return api3009Res;
	}

	/**
	 * . 单项目错误信息取得
	 *
	 * @param param     错误参数
	 * @param errorCode 错误code
	 * @param fieldName 字段中文名
	 * @return API0112Res response class
	 */
	public API3009Res getReqErr(String[] param, String errorCode, String fieldName) {

		logger.debug("API0112Service getReqErr");

		String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
		String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
		logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
		logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
		return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
	}

	/**
	 * . 错误信息设定
	 *
	 * @param e 自定义错误获取类
	 * @return response class
	 */
	public API3009Res setErrRes(CustomHandleException e) {

		logger.debug("API0112Service setErrRes");

		API3009Res api3009Res = new API3009Res();
		api3009Res.setResultCode(e.getErrorMessage().getResultCode());
		api3009Res.setResultCnt(e.getErrorMessage().getResultCnt());
		api3009Res.setMessageCode(e.getErrorMessage().getMessageCode());
		api3009Res.setMessage(e.getErrorMessage().getMessage());
		return api3009Res;
	}
}
