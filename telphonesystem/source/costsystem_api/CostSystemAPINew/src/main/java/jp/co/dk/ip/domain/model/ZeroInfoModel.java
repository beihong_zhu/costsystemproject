package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class ZeroInfoModel {

	/**
	 * . 渠道简称
	 */
	private String agencyname;
	
	private String capital;
	
	private String capitalStatus;
	
	private String  createdDatetime;
    
   

    /**.
     * CSV Field
     *
     * @return String CSV Field
     */
    public String toRow() {
        return String.format(
                Utils.combinationString(Const.MANAGEMENTFILE.S_MARK, Const.MANAGEMENTFILE.USER_NUM_13),
                this.agencyname,
                this.capital,
                this.capitalStatus,
                this.createdDatetime             
        		);
    }
}
