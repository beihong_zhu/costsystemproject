package jp.co.dk.ip.common.enums;

/**
 * 布尔状态枚举类
 */
public enum BoolCodeEnum {
    // bool
    TRUE(1, "true"),
    FALSE(0, "false")
    ;

    private String msg;
    private int code;

    public String msg() {
        return msg;
    }

    public int getCode() {
        return code;
    }

    BoolCodeEnum(int code, String msg) {
        this.msg = msg;
        this.code = code;
    }
}
