package jp.co.dk.ip.application.controller;

import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIBalanceChargeReq;
import jp.co.dk.ip.application.response.APIBalanceChargeRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.common.WrapperUtils;
import jp.co.dk.ip.domain.service.APIBalanceChargeService;

/**
 * balance charge API Controller
 */
@RestController
@RequestMapping("/api")
public class APIBalanceChargeController {

	private static final Logger logger = LoggerFactory.getLogger(APIBalanceChargeController.class);

	@Autowired
	private APIBalanceChargeService apiBalanceChargeService;
    
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

	@PostMapping("/APIBALANCECHARGE")
	private APIBalanceChargeRes agentDataProcess(@RequestBody @Valid APIBalanceChargeReq apiBalanceChargeReq, BindingResult bindingResult) {

		logger.debug("APIBalanceChargeController agentDataProcess");

        APIBalanceChargeRes apiBalanceChargeRes = new APIBalanceChargeRes();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			WrapperUtils wrapperUtils = new WrapperUtils();
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = String.valueOf(fieldError.getField());
				String message = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				wrapperUtils.setErrMsg(message);
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + wrapperUtils.getErrMsg());
			});
			return apiBalanceChargeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR, wrapperUtils.getErrMsg());
		}

		try {
			// response获取
			apiBalanceChargeRes = apiBalanceChargeService.agentDataProcess(apiBalanceChargeReq);
			
			// response setting
			if (!Utils.isEmpty(apiBalanceChargeRes)) {
				if (!Objects.equals(apiBalanceChargeRes.getResultCode(), Const.RESULT_CODE_2)) {
					apiBalanceChargeRes = success(apiBalanceChargeRes);
				} else {
					apiBalanceChargeRes = failure(apiBalanceChargeRes);
				}
			} else {
				apiBalanceChargeRes = failure(apiBalanceChargeRes);
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_APIBALANCECHARGE + e.getMessage(), e);
			apiBalanceChargeRes = apiBalanceChargeService.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_APIBALANCECHARGE + e.getMessage(), e);
			apiBalanceChargeRes = apiBalanceChargeService.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000016,
					Const.ERRRES_APIBALANCECHARGE);
		}

		return apiBalanceChargeRes;
	}
	
	/**
	 * success
	 */
	private APIBalanceChargeRes success(APIBalanceChargeRes apiBalanceChargeRes) {
		
		logger.debug("APIMicroLoginController success");
		
		if (Utils.isEmpty(apiBalanceChargeRes.getResultCnt())) {
			apiBalanceChargeRes.setResultCnt(Const.RESULT_CNT_1);	
		}
		if (Utils.isEmpty(apiBalanceChargeRes.getResultCode())) {
			apiBalanceChargeRes.setResultCode(Const.RESULT_CODE_0);
		}
		logger.info(
				messageSource.getMessage(
						Const.I000001, 
						new String[] {},
						Locale.forLanguageTag(settingContext.getLangZh())));
		
		return apiBalanceChargeRes;
	}
	
	/**
	 * failure
	 */
	private APIBalanceChargeRes failure(APIBalanceChargeRes apiBalanceChargeRes) {
		
		logger.debug("APIMicroLoginController failure");
		
		if (Utils.isEmpty(apiBalanceChargeRes)) {
			apiBalanceChargeRes = new APIBalanceChargeRes();
		}
		
		if (Utils.isEmpty(apiBalanceChargeRes.getResultCnt())) {
			apiBalanceChargeRes.setResultCnt(Const.RESULT_CNT_0);
		}
		if (Utils.isEmpty(apiBalanceChargeRes.getResultCode())) {
			apiBalanceChargeRes.setResultCode(Const.RESULT_CODE_9);
		}
		String message = Const.EMPTY_STRING;
		message = messageSource.getMessage(
				Const.E000016,
				new String[] {},
				Locale.forLanguageTag(settingContext.getLangZh()));
		if (Utils.isEmpty(apiBalanceChargeRes.getMessageCode())) {
			apiBalanceChargeRes.setMessageCode(Const.E000016);
		}
		if (Utils.isEmpty(apiBalanceChargeRes.getMessage())) {
			apiBalanceChargeRes.setMessage(message);
		}
		logger.error(message);
		
		return apiBalanceChargeRes;
	}
}
