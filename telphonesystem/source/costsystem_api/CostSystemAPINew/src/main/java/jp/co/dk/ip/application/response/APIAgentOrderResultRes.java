package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception API Response
 */
@Getter
@Setter
public class APIAgentOrderResultRes extends BaseRes {

    // 返回结果
    private String data;
}
