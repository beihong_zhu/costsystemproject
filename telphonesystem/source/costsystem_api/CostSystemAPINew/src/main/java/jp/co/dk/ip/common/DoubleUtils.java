package jp.co.dk.ip.common;

import java.math.BigDecimal;

/**
 * Double工具类
 */
public class DoubleUtils {

	private static final int DEF_DIV_SCALE = 10;
	
	// 两个Double数相加
	public static Double add(Double v1, Double v2){
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		Double getDouble = null;
		getDouble = b1.add(b2).doubleValue();
		return getDouble;
	}
	
	// 两个Double数相减
	public static Double sub(Double v1, Double v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		Double getDouble = null;
		getDouble = b1.subtract(b2).doubleValue();
		return getDouble;
	}

	// 两个Double数相乘
	public static Double mul(Double v1, Double v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		Double getDouble = null;
		getDouble = b1.multiply(b2).doubleValue();
		return getDouble;
	}

	// 两个Double数相除
	public static Double div(Double v1, Double v2) {
		BigDecimal b1 = new BigDecimal(v1.toString());
		BigDecimal b2 = new BigDecimal(v2.toString());
		Double getDouble = null;
		getDouble = b1.divide(b2, DEF_DIV_SCALE, BigDecimal.ROUND_HALF_UP).doubleValue();
		return getDouble;
	}
}
