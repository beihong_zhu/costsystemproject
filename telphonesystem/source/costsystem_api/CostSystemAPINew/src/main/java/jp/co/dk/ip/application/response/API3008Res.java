package jp.co.dk.ip.application.response;

import java.util.List;

import jp.co.dk.ip.application.response.data.ZeroInfoModelRes;
import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Response
 */
@Getter
@Setter
public class API3008Res extends BaseRes {

    // data process result
    public List<ZeroInfoModelRes> zeroInfoModelResList;
}
