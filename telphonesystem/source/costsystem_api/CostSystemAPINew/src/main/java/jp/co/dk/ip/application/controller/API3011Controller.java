package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API3011Req;
import jp.co.dk.ip.application.response.API3011Res;
import jp.co.dk.ip.application.response.data.OrderManagementModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API3011Service;

/**
 * all order management API Controller
 */
@RestController
@RequestMapping("/api")
public class API3011Controller {

	private static final Logger logger = LoggerFactory.getLogger(API3011Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API3011Service api3011Service;

	@PostMapping("/API3011")
	private API3011Res agentSummaryProcess(@RequestBody @Valid API3011Req api3011Req, BindingResult bindingResult) {

		logger.debug("API3011Controller agentSummaryProcess");

		API3011Res api3011Res = new API3011Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api3011Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<OrderManagementModelRes> orderManagementModelResList = new ArrayList<OrderManagementModelRes>();
			orderManagementModelResList = api3011Service.getAgentNamesResList(api3011Req);
			
			// response setting
			if (!Objects.equals(orderManagementModelResList, null)) {
				api3011Res.setResultCnt(orderManagementModelResList.size());
				api3011Res.setAgentSummaryModelResList(orderManagementModelResList);
				api3011Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001,
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api3011Res = api3011Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0112 + e.getMessage(), e);
			api3011Res = api3011Service.getErrRes(
					new String[] {},
					Const.RESULT_CODE_9,
					Const.E000005,
					Const.ERRRES_API0112);
		}

		return api3011Res;
	}
}
