package jp.co.dk.ip.application.request;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * all agents summary API Request
 */
@Getter
@Setter
public class API3012Req extends BaseReq {
	
	// 代理商公司全称
	@Length(max = 20, message = "{ECT010}")
	private String abbreviation;



}
