package jp.co.dk.ip.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.API0110Req;
import jp.co.dk.ip.application.response.API0110Res;
import jp.co.dk.ip.application.response.data.AgentCapitalInfoModelRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.API0110Service;

/**
 * agent getting API Controller
 */
@RestController
@RequestMapping("/api")
public class API0110Controller {

	private static final Logger logger = LoggerFactory.getLogger(API0110Controller.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private SettingContext settingContext;

	@Autowired
	private API0110Service api0110Service;

	@PostMapping("/API0110")
	private API0110Res agentDataProcess(@RequestBody @Valid API0110Req api0110Req, BindingResult bindingResult) {

		logger.debug("API0110Controller agentDataProcess");

		API0110Res api0110Res = new API0110Res();

		// 单项目验证
		if (bindingResult.hasErrors()) {
			bindingResult.getFieldErrors().stream().forEach(fieldError -> {
				String fieldName = MapSort.getMapValue(fieldError.getField());
				String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
				logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
				logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
			});
			return api0110Service.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
		}

		try {			
			// data getting
			List<AgentCapitalInfoModelRes> agentCapitalInfoModelResList = new ArrayList<AgentCapitalInfoModelRes>();
			agentCapitalInfoModelResList = api0110Service.getAgentCapitalInfoModelResList(api0110Req);
			
			// response setting
			if (!Objects.equals(agentCapitalInfoModelResList, null)) {
				api0110Res.setResultCnt(agentCapitalInfoModelResList.size());
				api0110Res.setAgentCapitalInfoModels(agentCapitalInfoModelResList);
				api0110Res.setResultCode(Const.RESULT_CODE_0);
				logger.info(
						messageSource.getMessage(
								Const.I000001, 
								new String[] {},
								Locale.forLanguageTag(settingContext.getLangZh())));
			}
		} catch (CustomHandleException e) {
			logger.error(Const.ERRRES_API0110 + e.getMessage(), e);
			api0110Res = api0110Service.setErrRes(e);
		} catch (Exception e) {
			logger.error(Const.ERRRES_API0110 + e.getMessage(), e);
			api0110Res = api0110Service.getErrRes(
					new String[] {}, 
					Const.RESULT_CODE_9, 
					Const.E000005,
					Const.ERRRES_API0110);
		}

		return api0110Res;
	}
}
