package jp.co.dk.ip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * . system application main file
 */
@EnableAutoConfiguration
@SpringBootApplication
@EnableScheduling
@ComponentScan
public class InstallationphotoAPIApplication {

	public static void main(String[] args) {

		System.setProperty("spring.devtools.restart.enabled", "false");
		SpringApplication.run(InstallationphotoAPIApplication.class, args);
	}
}
