package jp.co.dk.ip.domain.model.data;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * client data post 002 API Request
 */
@Getter
@Setter
public class OrderPostAuthory002Req extends BaseReq {

	// 用户id（平台分配）
	private String companyId;

	// 需要充值的手机号
	private String phone;

	// 订单流水号(长度不超过32)
	private String orderId;

	// 产品编号
	private String productId;

	// 回调地址
	private String callbackUrl;

	// 类型（1：全国  2:分省） 注：不传时不参与签名
	private Integer type;

	// 签名md5(callbackUrl=www.testtest.cn/notice&companyId=90004&orderId=delaytestorderId10001&phone=15158158060&productId=50004&type=1I8K11nWVfv39tAlWB8MtvOJS82pTD5Vo)
	// 其中appKey由平台提供，详细签名规则见首页
	private String sign;
}
