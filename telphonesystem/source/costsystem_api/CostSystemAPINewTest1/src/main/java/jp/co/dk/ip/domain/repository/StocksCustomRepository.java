package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.StockInfoModel;
import jp.co.dk.ip.domain.model.ProductInfoModel;

/**.
 * stock repository
 */
public interface StocksCustomRepository {

	List<StockInfoModel> findStockByStockInfo(
    		String channelname, 
    		String value,
    		String producttype,
    		String productstatus);
	
	List<ProductInfoModel> findStockByProductInfo(
    		String aggencyLimited, 
    		String localLimited,
    		String channelLimited,
    		String ocrManufacturingDate);	
}
