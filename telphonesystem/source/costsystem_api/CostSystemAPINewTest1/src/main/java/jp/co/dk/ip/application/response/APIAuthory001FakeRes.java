package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import lombok.Getter;
import lombok.Setter;

/**
 * client data post 001 API Response
 */
@Getter
@Setter
public class APIAuthory001FakeRes extends BaseRes {

    // code
    private String code;
    
    // return message
    private String msg;
    
    // order id
    private String orderid;
    
    // order number
    private String orderno;
}
