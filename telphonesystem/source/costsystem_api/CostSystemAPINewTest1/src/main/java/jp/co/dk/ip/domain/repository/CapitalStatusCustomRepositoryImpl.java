package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.util.StringUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.CapitalInfoModel;
import jp.co.dk.ip.domain.model.CapitalRecordModel;

/**
 * . capital repository
 */
public class CapitalStatusCustomRepositoryImpl implements CapitalStatusCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<CapitalInfoModel> findCapitalByCapitalInfo(
			String corpId,
			String agentName,
			String agentReduceName,
			String reqId,
			String orderId,
			String billId,
			String startTime,
			String endTime) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(corpId, Const.EMPTY_STRING) && !Objects.equals(corpId, null)) {
			sql += "         AND o.corp_id = :corpId ";
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}
		if (!Objects.equals(reqId, Const.EMPTY_STRING) && !Objects.equals(reqId, null)) {
			sql += "         AND o.req_id = :reqId ";
		}
		if (!Objects.equals(orderId, Const.EMPTY_STRING) && !Objects.equals(orderId, null)) {
			sql += "         AND o.order_id = :orderId ";
		}
		if (!Objects.equals(billId, Const.EMPTY_STRING) && !Objects.equals(billId, null)) {
			sql += "         AND o.bill_id = :billId ";
		}
		if ((!Objects.equals(startTime, Const.EMPTY_STRING) && !Objects.equals(startTime, null)) &&
				(!Objects.equals(endTime, Const.EMPTY_STRING) && !Objects.equals(endTime, null))) {
			sql += "         AND o.created_datetime between :startTime and :endTime";
		}

		// sql injection preventing
		corpId = Utils.escapeSql(corpId);
		agentName = Utils.escapeSql(agentName);
		agentReduceName = Utils.escapeSql(agentReduceName);
		reqId = Utils.escapeSql(reqId);
		orderId = Utils.escapeSql(orderId);
		billId = Utils.escapeSql(billId);
		startTime = Utils.escapeSql(startTime);
		endTime = Utils.escapeSql(endTime);

		// time setting
		String strStartTime = Const.EMPTY_STRING;
		String strEndTime = Const.EMPTY_STRING;
        if (!StringUtils.isEmpty(startTime)) {
        	strStartTime = Utils.setDateString(startTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
        	strEndTime = Utils.setDateString(endTime);
        }
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "CapitalStatus.CapitalInfoModel");
		if (!Objects.equals(corpId, Const.EMPTY_STRING) && !Objects.equals(corpId, null)) {
			query.setParameter("corpId", corpId);
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			query.setParameter("agentName", agentName);
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			query.setParameter("agentReduceName", agentReduceName);
		}
		if (!Objects.equals(reqId, Const.EMPTY_STRING) && !Objects.equals(reqId, null)) {
			query.setParameter("reqId", reqId);
		}
		if (!Objects.equals(orderId, Const.EMPTY_STRING) && !Objects.equals(orderId, null)) {
			query.setParameter("orderId", orderId);
		}
		if (!Objects.equals(billId, Const.EMPTY_STRING) && !Objects.equals(billId, null)) {
			query.setParameter("billId", billId);
		}
		if ((!Objects.equals(startTime, Const.EMPTY_STRING) && !Objects.equals(startTime, null)) &&
				(!Objects.equals(endTime, Const.EMPTY_STRING) && !Objects.equals(endTime, null))) {
			query.setParameter("startTime", strStartTime);
			query.setParameter("endTime", strEndTime);
		}
		List<CapitalInfoModel> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CapitalRecordModel> findCapitalRecordByCapitalInfo(
			String corpId,
			String agentName,
			String agentReduceName,
			String reqId,
			String orderId,
			String billId,
			String orderForm,
			String orderType,
			String startTime,
			String endTime) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " a.agent_name, "
                + " o.req_id, "
                + " o.order_id, "
                + " o.bill_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " o.order_type, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(corpId, Const.EMPTY_STRING) && !Objects.equals(corpId, null)) {
			sql += "         AND o.corp_id = :corpId ";
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}
		if (!Objects.equals(reqId, Const.EMPTY_STRING) && !Objects.equals(reqId, null)) {
			sql += "         AND o.req_id = :reqId ";
		}
		if (!Objects.equals(orderId, Const.EMPTY_STRING) && !Objects.equals(orderId, null)) {
			sql += "         AND o.order_id = :orderId ";
		}
		if (!Objects.equals(billId, Const.EMPTY_STRING) && !Objects.equals(billId, null)) {
			sql += "         AND o.bill_id = :billId ";
		}
		if (!Objects.equals(orderForm, Const.EMPTY_STRING) && !Objects.equals(orderForm, null)) {
			sql += "         AND o.order_form = :orderForm ";
		}
		if (!Objects.equals(orderType, Const.EMPTY_STRING) && !Objects.equals(orderType, null)) {
			sql += "         AND o.order_type = :orderType ";
		}
		if ((!Objects.equals(startTime, Const.EMPTY_STRING) && !Objects.equals(startTime, null)) &&
				(!Objects.equals(endTime, Const.EMPTY_STRING) && !Objects.equals(endTime, null))) {
			sql += "         AND o.created_datetime between :startTime and :endTime";
		}

		// sql injection preventing
		corpId = Utils.escapeSql(corpId);
		agentName = Utils.escapeSql(agentName);
		agentReduceName = Utils.escapeSql(agentReduceName);
		reqId = Utils.escapeSql(reqId);
		orderId = Utils.escapeSql(orderId);
		billId = Utils.escapeSql(billId);
		orderForm = Utils.escapeSql(orderForm);
		orderType = Utils.escapeSql(orderType);
		startTime = Utils.escapeSql(startTime);
		endTime = Utils.escapeSql(endTime);

		// time setting
		String strStartTime = Const.EMPTY_STRING;
		String strEndTime = Const.EMPTY_STRING;
        if (!StringUtils.isEmpty(startTime)) {
        	strStartTime = Utils.setDateString(startTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
        	strEndTime = Utils.setDateString(endTime);
        }
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "CapitalStatus.CapitalInfoModel");
		if (!Objects.equals(corpId, Const.EMPTY_STRING) && !Objects.equals(corpId, null)) {
			query.setParameter("corpId", corpId);
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			query.setParameter("agentName", agentName);
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			query.setParameter("agentReduceName", agentReduceName);
		}
		if (!Objects.equals(reqId, Const.EMPTY_STRING) && !Objects.equals(reqId, null)) {
			query.setParameter("reqId", reqId);
		}
		if (!Objects.equals(orderId, Const.EMPTY_STRING) && !Objects.equals(orderId, null)) {
			query.setParameter("orderId", orderId);
		}
		if (!Objects.equals(billId, Const.EMPTY_STRING) && !Objects.equals(billId, null)) {
			query.setParameter("billId", billId);
		}
		if ((!Objects.equals(startTime, Const.EMPTY_STRING) && !Objects.equals(startTime, null)) &&
				(!Objects.equals(endTime, Const.EMPTY_STRING) && !Objects.equals(endTime, null))) {
			query.setParameter("startTime", strStartTime);
			query.setParameter("endTime", strEndTime);
		}
		List<CapitalRecordModel> list = query.getResultList();
		return list;
	}
}
