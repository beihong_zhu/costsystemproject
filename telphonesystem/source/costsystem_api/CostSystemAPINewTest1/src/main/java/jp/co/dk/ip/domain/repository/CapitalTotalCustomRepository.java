package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.CapitalTotalInfoModel;
import jp.co.dk.ip.domain.model.CapitalReducePlusModel;


/**
 * . capital repository
 */
public interface CapitalTotalCustomRepository {

	List<CapitalTotalInfoModel> findCapitalByCapitalInfo(
			String agencyabbreviation,
			String value,
			String provideSevice,
			String ocrManufacturingDate
		
			);
	
	List<CapitalReducePlusModel> findCapitalPlusReduceByCapitalInfo(
			String agencyacount,
			String agencyname,
			String applymode,
			String status,
			String ocrManufacturingDate
			);
	
	
	List<CapitalReducePlusModel> findCapitalZeroByCapitalInfo(
			String zero,
			String ocrManufacturingDate
			);
	
	
	
	
	
}
