package jp.co.dk.ip;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.impl.nio.conn.PoolingNHttpClientConnectionManager;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.nio.reactor.ConnectingIOReactor;
import org.apache.http.nio.reactor.IOReactorException;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

import jp.co.dk.ip.application.request.APIAuthory001FakeReq;
import jp.co.dk.ip.application.response.APIAuthory001FakeRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.FaceCompareModel;

public class TestHttpClient {
	
    public static void main(String[] args){

    	APIAuthory001FakeReq apiClientReq = new APIAuthory001FakeReq();
    	apiClientReq.setMerid("abc");
    	
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(50000)
                .setSocketTimeout(50000)
                .setConnectionRequestTimeout(1000)
                .build();

        // 配置io线程
        IOReactorConfig ioReactorConfig = IOReactorConfig.custom().
                setIoThreadCount(Runtime.getRuntime().availableProcessors())
                .setSoKeepAlive(true)
                .build();
        
        // 设置连接池大小
		ConnectingIOReactor ioReactor = null;
		try {
			ioReactor = new DefaultConnectingIOReactor(ioReactorConfig);
		} catch (IOReactorException e) {
			e.printStackTrace();
		}
        PoolingNHttpClientConnectionManager connManager = new PoolingNHttpClientConnectionManager(ioReactor);
        connManager.setMaxTotal(100);
        connManager.setDefaultMaxPerRoute(100);


        final CloseableHttpAsyncClient client = HttpAsyncClients.custom().
                setConnectionManager(connManager)
                .setDefaultRequestConfig(requestConfig)
                .build();

        // 构造请求
        String url = "http://192.168.1.120:8080/api/APICLIENT";
        HttpPost httpPost = new HttpPost(url);
        StringEntity entity = null;
		
		try {
//            String a = "{ \"index\": { \"_index\": \"test\", \"_type\": \"test\"} }\n" +
//                    "{\"name\": \"上海\",\"age\":33}\n";
//            String a = "{ \"corpid\": \"abc\" }\n";
            
			String output = Const.EMPTY_STRING;
			Gson gson = new Gson();
			output = gson.toJson(apiClientReq);
            
            entity = new StringEntity(output);
            
    		entity.setContentType("application/json;charset=UTF-8");
    		httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
            httpPost.setEntity(entity);
            
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //start
        client.start();
        
        //异步请求
        client.execute(httpPost, new callBack());

        while(true){
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static class callBack implements FutureCallback<HttpResponse>{

        private long start = System.currentTimeMillis();
        
		callBack() {

		}

		public void completed(HttpResponse httpResponse) {

			try {
				System.out.println("cost is:" + (System.currentTimeMillis() - start) + ":" + EntityUtils.toString(httpResponse.getEntity()));
		        APIAuthory001FakeRes apiClientRes = new APIAuthory001FakeRes();
				HttpEntity entity = httpResponse.getEntity();
				if (entity != null) {
					String output = Const.EMPTY_STRING;
					output = EntityUtils.toString(entity, "UTF-8");
					Gson gson = new Gson();
					apiClientRes = gson.fromJson(output, APIAuthory001FakeRes.class);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void failed(Exception e) {
			System.err.println("cost is:" + (System.currentTimeMillis() - start) + ":" + e);
		}

        public void cancelled() {

        }
    }
}
