package jp.co.dk.ip.domain.model.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class OrderPostReq {

    // 账号
    private String corpId;
    
    // 代理商流水号
    private String reqId;
    
    // 平台交易单号
    private String orderId;
    
    // 实付金额
    private String price;
    
    // 运营商：1-移动，2-联通，3-电信，4-中石化
    private String status;
    
    // 签名
    private String sign;
}