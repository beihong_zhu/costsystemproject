package jp.co.dk.ip.common;

public class CodeSettings {

    /**.
     * CSV登録区分妥当性チェック配列
     */
    public static String[] buildingsCsvDifCheck = new String[] {
        Const.UP_DIF_BUILDINGS_1,
        Const.UP_DIF_BUILDINGS_2,
        Const.UP_DIF_BUILDINGS_3,
        Const.UP_DIF_BUILDINGS_4 };

    /**.
     * 大工程妥当性チェック配列
     */
    public static String[] bigProcessIdCheck = new String[] {
        Const.BigProcessIdCheck.CHECK_1,
        Const.BigProcessIdCheck.CHECK_2,
        Const.BigProcessIdCheck.CHECK_3,
        Const.BigProcessIdCheck.CHECK_4 };

    /**.
     * 大工程妥当性チェック配列 「03」と「04」のみ
     */
    public static String[] bigProcessIdFor0304Check = new String[] { Const.BigProcessIdCheck.CHECK_3, Const.BigProcessIdCheck.CHECK_4 };

    /**.
     * 工程削除フラグ妥当性チェック配列
     */
    public static String[] isProcessDeleteCheck = new String[] { Const.IsProcessDeleteCheck.CHECK_1, Const.IsProcessDeleteCheck.CHECK_0 };

    /**.
     * 黒板出力有無妥当性チェック配列
     */
    public static String[] blackboardDisplayCheck = new String[] { Const.BlackboardDisplayCheck.CHECK_ON, Const.BlackboardDisplayCheck.CHECK_OFF };

    /**.
     * 黒板表示フラグ妥当性チェック配列
     */
    public static String[] isBlackBoardDisplayCheck = new String[] { Const.IS_BLACK_BOARD_DISPLAY_ON, Const.IS_BLACK_BOARD_DISPLAY_OFF };

    /**.
     * 作業判定コード設定の妥当性チェック
     */
    public static String[] workCheck = new String[] { Const.WorksCheck.CHECK_FLAG, Const.WorksCheck.CHECK_OK, Const.WorksCheck.CHECK_NG };

    /**.
     * フラッシュ機能制御コード設定の妥当性チェック
     */
    public static String[] flashCheck = new String[] { Const.FlashCheck.CHECK_ON, Const.FlashCheck.CHECK_OFF, Const.FlashCheck.CHECK_AUTO };

    /**.
     * 写真一覧処理選択の妥当性チェック
     */
    public static String[] photostTreatCheck = new String[] { Const.PhotostTreatCheck.CHECK_UPDATE, Const.PhotostTreatCheck.CHECK_DEL };

    /**.
     * 写真一覧表示方法選択の妥当性チェック
     */
    public static String[] photosChooseCheck = new String[] {
        Const.PhotosChooseCheck.CHECK_PARAM,
        Const.PhotosChooseCheck.CHECK_SORT,
        Const.PhotosChooseCheck.CHECK_DISPLAY };

    /**.
     * 写真一覧ソートパターンの妥当性チェック
     */
    public static String[] photosSortCheck = new String[] {
        Const.PhotosSortCheck.CHECK_PROCESSES,
        Const.PhotosSortCheck.CHECK_UNITS,
        Const.PhotosSortCheck.CHECK_DATE,
        Const.PhotosSortCheck.CHECK_FLOOR };

    /**.
     * ソート設定の妥当性チェック
     */
    public static String[] sortCheck = new String[] { Const.SortCheck.CHECK_ASC, Const.SortCheck.CHECK_DESC };

    /**.
     * 写真一覧表示形式の妥当性チェック
     */
    public static String[] photosDisplayCheck = new String[] { Const.PhotosDisplayCheck.CHECK_1, Const.PhotosDisplayCheck.CHECK_2 };

    /**.
     * 写真帳レイアウト配置選択の妥当性チェック
     */
    public static String[] photoReportLayoutCheck = new String[] {
        Const.PhotoReportLayoutCheck.CHECK_PROJECT_NAME,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_1,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_2,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_3,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_4,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_5,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_6,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_7,
        Const.PhotoReportLayoutCheck.CHECK_CONFIG_8,
        Const.PhotoReportLayoutCheck.CHECK_BLANK };

    /**.
     * 写真帳全体出力順の妥当性チェック
     */
    public static String[] photosOutOrderCheck = new String[] { Const.PhotosOutOrderCheck.CHECK_PROCESS, Const.PhotosOutOrderCheck.CHECK_FLOOR };

    /**.
     * 写真帳1頁出力数の妥当性チェック
     */
    public static String[] photosPageNumCheck = new String[] { Const.PhotosPageNumCheck.CHECK_PAGE_2, Const.PhotosPageNumCheck.CHECK_PAGE_3 };

    /**.
     * PDF、EXCEL両方S3にアップロードするフラグ、1：スマホ　0：PC
     */
    public static String[] photosIsMobileCheck = new String[] { Const.PhotosIsMobileCheck.CHECK_MOBILE_1, Const.PhotosIsMobileCheck.CHECK_MOBILE_0 };

    /**.
     * 工事写真メニューコードの妥当性チェック
     */
    public static String[] photosMenuCodeCheck = new String[] {
        Const.PhotosMenuCodeCheck.CHECK_PROJECT,
        Const.PhotosMenuCodeCheck.CHECK_PHOTO,
        Const.PhotosMenuCodeCheck.CHECK_PHOTO_LIST,
        Const.PhotosMenuCodeCheck.CHECK_DRAWING,
        Const.PhotosMenuCodeCheck.CHECK_PHOTO_OUT,
        Const.PhotosMenuCodeCheck.CHECK_PROCESS_LOGIN_IN,
        Const.PhotosMenuCodeCheck.CHECK_CHAT_ROOM };

    /**.
     * CSV登録対象設定の妥当性チェック
     */
    public static String[] cvsLoginInObjectOutCheck = new String[] {
        Const.CVSLoginInObjectOutCheck.CHECK_USER_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_BUILDING_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_BUILDING_PLACE_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_PROJECT_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_USER_MANAGER_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_BUILDING_INFO_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_UNITS_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_PROCESS_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_WITNESS_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_BLACKBOARD_CSV,
        Const.CVSLoginInObjectOutCheck.CHECK_PROJECT_INFO_CSV };

    /**.
     * 権限の妥当性チェック
     */
    public static String[] authorCheck = new String[] {
        Const.AuthorCheck.CHECK_AUTHOR_01,
        Const.AuthorCheck.CHECK_AUTHOR_02,
        Const.AuthorCheck.CHECK_AUTHOR_03,
        Const.AuthorCheck.CHECK_AUTHOR_04 };

    /**.
     * 表示フラグの妥当性チェック
     */
    public static String[] displayFlagCheck = new String[] {
        Const.DisplayFlagCheck.CHECK_DISPLAY_FLAG_OFF,
        Const.DisplayFlagCheck.CHECK_DISPLAY_FLAG_ON };

    /**.
     * 削除フラグの妥当性チェック
     */
    public static String[] deleteFlagCheck = new String[] { Const.DeleteFlagCheck.CHECK_DEL_FLAG_0, Const.DeleteFlagCheck.CHECK_DEL_FLAG_1 };

    /**.
     * アカウントロックフラグの妥当性チェック
     */
    public static String[] accountLockFlagCheck = new String[] { Const.AccountLockFlagCheck.CHECK_NO_LOCK, Const.AccountLockFlagCheck.CHECK_LOCKING };

    /**.
     * api1108アカウントロックフラグの妥当性チェック
     */
    public static String[] api1108AccountLockFlagCheck = new String[] { Const.AccountLockFlagCheck.CHECK_NO_LOCK };

    /**.
     * api1107アカウントロックフラグの妥当性チェック
     */
    public static String[] api1107AccountLockFlagCheck = new String[] { Const.AccountLockFlagCheck.CHECK_LOCKING };

    /**.
     * 配置フラグの妥当性チェック
     */
    public static String[] installFlagCheck = new String[] { Const.InstallFlagCheck.CHECK_NO_SETTING, Const.InstallFlagCheck.CHECK_SETTING };

    /**.
     * 配置フラグの妥当性チェック API0602のみ
     */
    public static String[] api0602InstallFlagCheck = new String[] { Const.InstallFlagCheck.CHECK_SETTING };

    /**.
     * 配置フラグの妥当性チェック API0603のみ
     */
    public static String[] api0603InstallFlagCheck = new String[] { Const.InstallFlagCheck.CHECK_SETTING };

    /**.
     * 写真帳出力フラグの妥当性チェック
     */
    public static String[] isOutputPhotoBookCheck = new String[] {
        Const.AppropriateCheck.CHECK_OUTPUT_PHOTO_0,
        Const.AppropriateCheck.CHECK_OUTPUT_PHOTO_1 };

    /**.
     * 機種区分の妥当性チェック
     */
    public static String[] modeltypeDivisionCheck = new String[] {
        Const.ModeltypeDivisionCheck.CHECK_ROOM_OUT,
        Const.ModeltypeDivisionCheck.CHECK_ROOM_IN };

    /**.
     * 事前設定撮影必須項目選択の妥当性チェック
     */
    public static String[] mustInputItemCheck = new String[] {
        Const.MustInputItemCheck.CHECK_ONLY_ROOM_OUT,
        Const.MustInputItemCheck.CHECK_ONLY_ROOM_IN,
        Const.MustInputItemCheck.CHECK_ROOM_OUT_IN,
        Const.MustInputItemCheck.CHECK_NONE };

    /**.
     * アクセス許可フラグの妥当性チェック
     */
    public static String[] accessFlagCheck = new String[] { Const.AccessFlagCheck.CHECK_INVALID, Const.AccessFlagCheck.CHECK_VALID };

    /**.
     * 状況設定の妥当性チェック
     */
    public static String[] timingSetCheck = new String[] {
        Const.TimingSetCheck.CHECK_TIMING_SET_1,
        Const.TimingSetCheck.CHECK_TIMING_SET_2,
        Const.TimingSetCheck.CHECK_TIMING_SET_3,
        Const.TimingSetCheck.CHECK_TIMING_SET_4,
        Const.TimingSetCheck.CHECK_TIMING_SET_5,
        Const.TimingSetCheck.CHECK_TIMING_SET_6 };

    /**.
     * 登録区分の妥当性チェック
     */
    public static String[] loginInDivisionCheck = new String[] {
        Const.LoginInDivisionCheck.CHECK_LOGIN_IN,
        Const.LoginInDivisionCheck.CHECK_UPDATE,
        Const.LoginInDivisionCheck.CHECK_DELETE,
        Const.LoginInDivisionCheck.CHECK_NONE };

    /**.
     * 処理結果コードの妥当性チェック
     */
    public static String[] resultCheck = new String[] {
        Const.ResultCheck.CHECK_DONE,
        Const.ResultCheck.CHECK_ERROE,
        Const.ResultCheck.CHECK_EXCEPTION };

    /**.
     * 出力パターンの妥当性チェック
     */
    public static String[] outputOrderPatternCheck = new String[] { Const.OUTPUT_ORDER_PATTERN_A, Const.OUTPUT_ORDER_PATTERN_B };

    /**.
     * 不明出力フラグの妥当性チェック
     */
    public static String[] unknownOutputCheck = new String[] { Const.UNKNOWN_OUTPUT_1, Const.UNKNOWN_OUTPUT_0 };

    /**.
     * 操作端末区分妥当性チェック
     */
    public static String[] deviceCodeCheck = new String[] { Const.DeviceCodeCheck.PC_DEVICE, Const.DeviceCodeCheck.SP_DEVICE };

}
