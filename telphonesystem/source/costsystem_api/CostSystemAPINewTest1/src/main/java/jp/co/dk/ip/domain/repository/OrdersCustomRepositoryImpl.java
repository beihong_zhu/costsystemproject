package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.util.StringUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentSummaryModel;
import jp.co.dk.ip.domain.model.OrderManagementModel;

/**
 * . orders repository
 */
public class OrdersCustomRepositoryImpl implements OrdersCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentSummaryModel> findOrderSummaryByOrderInfo(
			String agentName,
			String agentReduceName,
			String money,
			String startTime,
			String endTime
			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}
		if (!Objects.equals(money, Const.EMPTY_STRING) && !Objects.equals(money, null)) {
			sql += "         AND o.money = :money ";
		}
		if ((!Objects.equals(startTime, Const.EMPTY_STRING) && !Objects.equals(startTime, null)) &&
				(!Objects.equals(endTime, Const.EMPTY_STRING) && !Objects.equals(endTime, null))) {
			sql += "         AND o.created_datetime between :startTime and :endTime";
		}

		// sql injection preventing
		agentName = Utils.escapeSql(agentName);
		agentReduceName = Utils.escapeSql(agentReduceName);
		money = Utils.escapeSql(money);
		startTime = Utils.escapeSql(startTime);
		endTime = Utils.escapeSql(endTime);

		// time setting
		String strStartTime = Const.EMPTY_STRING;
		String strEndTime = Const.EMPTY_STRING;
        if (!StringUtils.isEmpty(startTime)) {
        	strStartTime = Utils.setDateString(startTime);
        }
        if (!StringUtils.isEmpty(endTime)) {
        	strEndTime = Utils.setDateString(endTime);
        }
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Orders.AgentSummaryModel");
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			query.setParameter("agentName", agentName);
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			query.setParameter("agentReduceName", agentReduceName);
		}
		if (!Objects.equals(money, Const.EMPTY_STRING) && !Objects.equals(money, null)) {
			query.setParameter("money", money);
		}
		if ((!Objects.equals(startTime, Const.EMPTY_STRING) && !Objects.equals(startTime, null)) &&
				(!Objects.equals(endTime, Const.EMPTY_STRING) && !Objects.equals(endTime, null))) {
			query.setParameter("startTime", strStartTime);
			query.setParameter("endTime", strEndTime);
		}
		List<AgentSummaryModel> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderManagementModel> findOrderByProductInfo(
			String orderorigin,
			String channelname,
			String agencyname,
			String agecnylimited,
			String locallimited,
			String channellimited,
			String telnumber,
			String productname,
			String providesevice,
			String teletype,
			String status,
			String ocrManufacturingDate

			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(orderorigin, Const.EMPTY_STRING) && !Objects.equals(orderorigin, null)) {
			sql += "         AND a.orderorigin = :orderorigin ";
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			sql += "         AND a.agent_reduce_name = :agencyname ";
		}
		if (!Objects.equals(status, Const.EMPTY_STRING) && !Objects.equals(status, null)) {
			sql += "         AND o.money = :status ";
		}
		if ((!Objects.equals(teletype, Const.EMPTY_STRING) && !Objects.equals(status, null)) &&
				(!Objects.equals(telnumber, Const.EMPTY_STRING) && !Objects.equals(telnumber, null))) {
			sql += "         AND o.created_datetime between :startTime and :endTime";
		}

		// sql injection preventing
		channellimited = Utils.escapeSql(channellimited);
		teletype = Utils.escapeSql(teletype);
		status = Utils.escapeSql(status);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);

		// time setting
		String strStartTime = Const.EMPTY_STRING;
		String strEndTime = Const.EMPTY_STRING;
        if (!StringUtils.isEmpty(productname)) {
        	strStartTime = Utils.setDateString(productname);
        }
        if (!StringUtils.isEmpty(telnumber)) {
        	strEndTime = Utils.setDateString(telnumber);
        }
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Orders.AgentSummaryModel");
		if (!Objects.equals(agecnylimited, Const.EMPTY_STRING) && !Objects.equals(agecnylimited, null)) {
			query.setParameter("agentName", agecnylimited);
		}
		if (!Objects.equals(agecnylimited, Const.EMPTY_STRING) && !Objects.equals(agecnylimited, null)) {
			query.setParameter("agentReduceName", agecnylimited);
		}
		if (!Objects.equals(agecnylimited, Const.EMPTY_STRING) && !Objects.equals(agecnylimited, null)) {
			query.setParameter("money", agecnylimited);
		}
		if ((!Objects.equals(agecnylimited, Const.EMPTY_STRING) && !Objects.equals(agecnylimited, null)) &&
				(!Objects.equals(agecnylimited, Const.EMPTY_STRING) && !Objects.equals(agecnylimited, null))) {
			query.setParameter("startTime", strStartTime);
			query.setParameter("endTime", strEndTime);
		}
		List<OrderManagementModel> list = query.getResultList();
		return list;
	}
}
