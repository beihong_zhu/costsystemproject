package jp.co.dk.ip.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * . order
 */
@Getter
@Setter
@AllArgsConstructor
public class CapitalInfoModel {

	/**
	 * . 代理流水
	 */
	private String reqId;

	/**
	 * . 订单号
	 */
	private String orderId;

	/**
	 * . 账户简称
	 */
	private String agentReduceName;

	/**
	 * . 方式
	 */
	private String orderForm;

	/**
	 * . 交易名称
	 */
	private String productName;

	/**
	 * . 之前余额
	 */
	private String capital;

	/**
	 * . 交易金额
	 */
	private String price;

	/**
	 * . 状态
	 */
	private String status;

	/**
	 * . 支付时间
	 */
	private String payTime;
}
