package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.domain.model.data.OrderPostAuthory002Req;
import lombok.Getter;
import lombok.Setter;

/**.
 * order post request Info
 */
@Getter
@Setter
public class OrderPostAuthory002Model {

    // order post request
    private OrderPostAuthory002Req orderPostAuthory002Req;
    
    // order post url
    private String url;
}