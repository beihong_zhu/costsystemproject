package jp.co.dk.ip.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * . stock
 */
@Getter
@Setter
@AllArgsConstructor
public class StockInfoModel {

	/**
	 * . 渠道简称
	 */
	private String abbreviation;

	/**
	 * . 商品名称
	 */
	private String product_name;

	/**
	 * . 商品类型
	 */
	private String product_mode;

	/**
	 * . 面额
	 */
	private String value;

	/**
	 * . 价格：渠道的价格
	 */
	private String price;

	/**
	 * . 折扣
	 */
	private String disaccount;

	/**
	 * . 优先级： 渠道的优先级
	 */
	private String priority;

	/**
	 * . 状态
	 */
	private String status;

}
