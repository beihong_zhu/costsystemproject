package jp.co.dk.ip.domain.service.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.ResInfoModel;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.google.gson.Gson;

/**.
 * CSV共通処理
 */
@Service
@Transactional
public class CsvUtilsService {

    private static final Logger logger = LoggerFactory.getLogger(CsvUtilsService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;

    @Autowired
    private S3AccessService s3AccessService;

    /**.
     * データレコードのバリデーションチェック 数字型チェック
     *
     * @param value ファイル内容
     * @return boolean
     */
    public boolean isNum(String value) {
        Pattern pattern = Pattern.compile(Const.CHECK_NUM);
        return !pattern.matcher(value).matches();
    }

    /**.
     * データレコードのバリデーションチェック  桁チェック
     *
     * @param value ファイル内容
     * @param num 桁
     * @return boolean
     */
    public boolean checkCount(String value, int num) {
        if (!StringUtils.isEmpty(value) && value.length() > num) {
            return true;
        }
        return false;
    }

    /**.
     * 指定インデックスでリスト配列作成
     *
     * @param list リスト配列
     * @param begin 開始桁
     * @param end 終了桁
     * @return CSVデータコレクション
     */
    public List<List<String>> getCollection(List<List<String>> list, int begin, int end) {
        List<List<String>> result = new ArrayList<List<String>>();
        List<String> line = new ArrayList<>();
        if (end > begin) {
            for (int i = begin; i < end; i++) {
                line = list.get(i);
                result.add(line);
            }
        }
        return result;
    }

    /**.
     * 同期CSVファイルを生成
     *
     * @param writeRowList CSV行データリスト
     * @param inputHeadArr CSVヘッダフィールド
     * @param name CSVファイル名
     * @return CSVファイル
     */
    public File resultListToCsvFile(List<String> writeRowList, String[] inputHeadArr, String name) throws Exception {

        File csvFile = File.createTempFile(name, Const.CSV_FILE_SUFFIX);

        if (csvFile != null) {
            try (BufferedWriter csvWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), Const.CSV_CREATE_UTF8),
                    Const.CSV_CREATE_BINARY);) {
                // ファイルタイトル
                csvWriter.write(String.join(Const.COMMA, inputHeadArr));
                csvWriter.newLine();
                // ファイル内容
                if (!CollectionUtils.isEmpty(writeRowList)) {
                    for (String row : writeRowList) {
                        csvWriter.write(row);
                        csvWriter.newLine();
                    }
                }
            }
        }

        return csvFile;
    }

    /**.
     * 結果CSVファイルを生成
     *
     * @param writeRowList CSV行データリスト
     * @param name CSVファイル名
     * @return CSVファイル
     */
    public File resultListToUploadCsvFile(List<String> writeRowList, String name) throws Exception {

        File csvFile = File.createTempFile(name, Const.CSV_FILE_SUFFIX);

        if (csvFile != null) {
            try (BufferedWriter csvWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(csvFile), Const.CSV_CREATE_UTF8),
                    Const.CSV_CREATE_BINARY);) {
                // ファイル内容
                if (!CollectionUtils.isEmpty(writeRowList)) {
                    for (String row : writeRowList) {
                        csvWriter.write(row + Const.NEW_LINE_SEPARATOR_RN);
                    }
                }
            }
        }

        return csvFile;
    }

    /**.
    * リスポンスファイルデータ作成
    *
    * @param list データリスト
    * @return ファイルデータ
    */
    public List<String[]> createResponseFileData(List<List<String>> list) {

        // 物件処理結果CSVデータ作成
        List<String[]> values = new ArrayList<String[]>();
        for (int i = 0; i < list.size(); i++) {
            List<String> line = list.get(i);
            String[] value = line.toArray(new String[line.size()]);
            values.add(value);
        }
        return values;
    }

    /**.
     * エラー情報を取得
     *
     * @param param エラー情報
     * @param resultCode 復帰コード
     * @param errorCode エラーコード
     * @param logError ログ接頭
     * @return
     */
    public ResponseEntity<StreamingResponseBody> getErrRes(String[] param, String resultCode, String errorCode,
            String logError) {
        String errMsg = messageSource.getMessage(errorCode,
                param, Locale.forLanguageTag(settingContext.getLang()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .header(Const.CSVHEADER.CSV_ACCESS_HEADERS,
                        Const.CSVHEADER.CSV_HEADER_RESULT_CODE + Const.COMMA + Const.CSVHEADER.CSV_HEADER_MESSAGE_CODE + Const.COMMA
                                + Const.CSVHEADER.CSV_HEADER_MESSAGE)
                .header(Const.CSVHEADER.CSV_HEADER_RESULT_CODE, resultCode)
                .header(Const.CSVHEADER.CSV_HEADER_MESSAGE_CODE, errorCode)
                .header(Const.CSVHEADER.CSV_HEADER_MESSAGE, Utils.urlEncoder(errMsg))
                .body(null);
    }

    /**.
     * CSV返却
     *
     * @param lines CSV返却データ
     * @return CSVファイル
     */
    public StreamingResponseBody getBody(List<List<String>> lines) {
        // CSV返却サンプル
        StreamingResponseBody body = outputStream -> {
            try (OutputStreamWriter writer = new OutputStreamWriter(outputStream, Const.CSV_CREATE_UTF8);
                    CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withRecordSeparator(Const.NEW_LINE_SEPARATOR_RN));) {
                for (List<String> line : lines) {
                    printer.printRecord(line);
                }
            }
        };
        return body;
    }

    /**.
     * 復帰のエラー情報を取得
     *
     * @param errCode エラーコード
     * @param errContent エラーコードパラメータ
     * @param exportListInfo エクスポートデータリスト
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> ResInfoModel getResInfo(String errCode, String[] errContent, List<List<String>>... exportListInfo) {
        ResInfoModel resInfo = new ResInfoModel();
        resInfo.setErrCode(errCode);
        if (errContent != null) {
            resInfo.setErrContent(errContent);
        }
        if (exportListInfo != null) {
            resInfo.setExportListInfo(exportListInfo);
        }
        return resInfo;
    }

    /**.
     * リスポンス例外エラー情報を設定
     *
     * @param request リクエスト,
     * @param sessionKeyErr セッションキー, リスポンス例外のエラー情報
     * @param sessionKeyRes セッションキー, リスポンスエラー情報
     * @param errCode エラーコード
     * @param errContent エラーコードパラメータ
     */
    @SuppressWarnings("unchecked")
    public void getExceptionInfo(
            HttpServletRequest request,
            String sessionKeyErr,
            String sessionKeyRes,
            String errCode,
            String[] errContent) {
        HttpSession session = request.getSession();
        ResInfoModel resInfo = new ResInfoModel();
        session.setAttribute(sessionKeyErr, null);
        resInfo = getResInfo(errCode, errContent);
        session.setAttribute(sessionKeyRes, resInfo);
    }

    /**.
     * セッションのプログラムエラー情報を取得
     *
     * @param request リクエスト,
     * @param sessionKey セッションキー, リスポンスエラー情報
     * @param errCode エラーコード
     * @param errContent エラーコードパラメータ
     * @return ResInfoModelモデル
     */
    private ResInfoModel getSessionErrInfo(
            HttpServletRequest request,
            String sessionKey,
            String errCode,
            String[] errContent) {
        HttpSession session = request.getSession();
        ResInfoModel resInfo = new ResInfoModel();
        resInfo.setErrCode(errCode);
        if (errContent != null) {
            resInfo.setErrContent(errContent);
        }
        session.setAttribute(sessionKey, resInfo);
        return resInfo;
    }

    /**.
     * 初期セッション情報を設定
     *
     * @param request リクエスト,
     * @param sessionNum セッションキー, iNum 自動増加変数
     * @param sessionNo セッションキー, no CSVナンバー
     * @param sessionTitle セッションキー, title CSVタイトル
     * @param sessionTable セッションキー, テーブル
     * @param sessionProgErr セッションキー, errCode エラーコード
     * @return
     */
    public void getShokkiSessionInfo(HttpServletRequest request,
            String sessionNum, int num,
            String sessionNo, String no,
            String sessionTitle, String title,
            String sessionTable, String table,
            String sessionProgErr, String errCode) {
        HttpSession session = request.getSession();
        session.setAttribute(sessionNum, num);
        session.setAttribute(sessionNo, no);
        session.setAttribute(sessionTitle, title);
        session.setAttribute(sessionTable, table);
        getSessionErrInfo(request, sessionProgErr, errCode, new String[] { table });
    }

    /**.
    * リスポンスデータの容量を確認
    * @param <T>
    *
    * @param resList リスポンスデータリスト
    * @param resListOther リスポンスデータリストの以外
    * @return リスポンスデータの容量を超える判断
    */
    @SuppressWarnings("unchecked")
    public static <T> boolean judgeDataVolumn(List<T> resList, List<T>... resListOther) {

        // リスポンスデータの容量を確認
        Gson gson = new Gson();
        String json = gson.toJson(resList);
        String jsonOther = Const.EMPTY_STRING;
        byte[] byteArr = json.getBytes();
        byte[] byteArrOther = null;
        if (resListOther.length > Const.COUNT_0) {
            jsonOther = gson.toJson(resListOther);
            byteArrOther = jsonOther.getBytes();
        }
        int dataVolumn = byteArr.length;
        if (byteArrOther != null) {
            if (byteArrOther.length > Const.COUNT_0) {
                dataVolumn = dataVolumn + byteArrOther.length;
            }
        }
        if (dataVolumn > Const.DATA_VOLUMN_10MB) {
            return false;
        }
        return true;
    }

    /**.
     * 解析CSVファイル
     *
     * @param csvStream インプットCSVファイルStream
     * @return inputList CSVファイル情報
     * @throws Exception Exception
     */
    public List<List<String>> readCsv(InputStream csvStream) throws Exception {

        List<List<String>> inputList = new ArrayList<List<String>>();
        try (CSVParser parser = CSVFormat.newFormat(',').parse(new InputStreamReader(csvStream));) {
            for (CSVRecord record : parser.getRecords()) {
                List<String> items = new ArrayList<String>();
                record.forEach(item -> {
                    items.add(StringUtils.trimWhitespace(item));
                });
                inputList.add(items);
            }
        } catch (Exception e) {
            String errMsg = messageSource.getMessage(Const.ECT027, new String[] {}, Locale.forLanguageTag(settingContext.getLang()));
            logger.error(errMsg, e);
            throw e;
        }
        return inputList;
    }
    
    /**.
     * 解析CSVファイル
     *
     * @param csvFile インプットCSVファイル
     * @return inputList CSVファイル情報
     * @throws Exception Exception
     */
    public List<List<String>> readCsvFromFile(File csvFile) throws Exception {

    	FileInputStream csvStream = new FileInputStream(csvFile.getPath());
        List<List<String>> inputList = new ArrayList<List<String>>();
        try (CSVParser parser = CSVFormat.newFormat(',').parse(new InputStreamReader(csvStream));) {
            for (CSVRecord record : parser.getRecords()) {
                List<String> items = new ArrayList<String>();
                record.forEach(item -> {
                    items.add(StringUtils.trimWhitespace(item));
                });
                inputList.add(items);
            }
        } catch (Exception e) {
            String errMsg = messageSource.getMessage(Const.ECT027, new String[] {}, Locale.forLanguageTag(settingContext.getLang()));
            logger.error(errMsg, e);
            throw e;
        }
        return inputList;
    }

    /**.
    * CSV登録処理ファイル生成とS3へアップロード
    *
    * @param inputList 入力CSV
    * @param outputCsvPath 出力CSVのS3相対パス
    * @param csvFileName 出力CSVのファイル名
    */
    public void uploadCsvFile(List<List<String>> inputList, String outputCsvPath, String csvFileName) throws Exception {

        List<String> inputCsvList = new ArrayList<String>();
        String line = Const.EMPTY_STRING;
        for (List<String> csvList : inputList) {
            line = Utils.combinationListString(csvList);
            inputCsvList.add(line);
        }
        File csvFile = null;
        try {
            csvFile = resultListToUploadCsvFile(inputCsvList, csvFileName);
            if (null != csvFile) {
                s3AccessService.uploadSyncCsv(outputCsvPath, csvFile);
            }
        } finally {
            if (null != csvFile) {
                csvFile.deleteOnExit();
            }
        }
    }

    /**.
    * 事前登録ファイル名を取得
    *
    * @param originalFileName オリジナルファイル名
    * @return 文字列 ファイル名
    */
    public String getCsvFileName(String originalFileName, String csvFilePrefix) {

        // 事前登録ファイル名を取得
        String judgeCsv = getOriginalFileName(originalFileName, Const.DOT_USING);
        String fileName = Const.EMPTY_STRING;
        if (Const.CSV_FILE_SUFFIX_VALUE.equals(judgeCsv)) {
            fileName = csvFilePrefix
                    + originalFileName.substring(Const.SUBSTRING_BEGIN_INDEX_0, originalFileName.lastIndexOf(Const.DOT))
                    + Const.PATH_UNDERBAR
                    + Utils.dataToString(new Date())
                    + Const.CSV_FILE_SUFFIX;
        } else {
            fileName = csvFilePrefix
                    + originalFileName
                    + Const.PATH_UNDERBAR
                    + Utils.dataToString(new Date())
                    + Const.CSV_FILE_SUFFIX;
        }
        return fileName;
    }

    /**.
    * ファイル相対パスを作成
     *
     * @param request リクエスト
     * @param apiName API名
     * @param originalFileName オリジナルファイル名
     * @return ファイル相対パス
     */
    public String relativeS3FilePath(HttpServletRequest request, String apiName, String originalFileName) {

        // ユーザIDを取得
        String userId = request.getAttribute(Const.REQ_ATTR_USER_ID).toString();
        if (Const.EMPTY_STRING.equals(userId) || userId == null) {
            return null;
        }
        // ファイル相対パスを作成
        String path = Const.PATH_CSV_FILE
                + Const.PATH_SLASH
                + apiName
                + Const.PATH_SLASH
                + userId
                + Const.PATH_SLASH
                + originalFileName;
        return path;
    }

    /**.
    * ファイル名からオリジナルファイル名を取得
    * @param <T>
    *
    * @param fileName ファイル名
    * @return 文字列 オリジナルファイル名
    */
    public <T> String getOriginalFileName(String fileName, String splitKey, Integer... key) {

        // ファイル相対パスからオリジナルファイル名を取得
        Integer[] getKeyArr = new Integer[] {};
        getKeyArr = key;
        String[] filePathArr = new String[] {};
        filePathArr = fileName.split(splitKey);
        String nameInfo = Const.EMPTY_STRING;
        nameInfo = setOriginalFileName(getKeyArr, filePathArr, fileName);
        return nameInfo;
    }

    /**.
     * オリジナルファイル名を設定
     *
     * @param getKeyArr 桁数配列
     * @param filePathArr ファイル名配列
     * @param fileName ファイル名
     * @return オリジナルファイル名
     */
    private String setOriginalFileName(Integer[] getKeyArr, String[] filePathArr, String fileName) {

        // オリジナルファイル名を設定
        if (filePathArr != null) {
            if (filePathArr.length > Const.COUNT_0) {
                if (getKeyArr != null) {
                    if (getKeyArr.length > Const.COUNT_0) {
                        if (filePathArr.length < (getKeyArr[Const.COUNT_0] + Const.COUNT_1)) {
                            return filePathArr[filePathArr.length - Const.COUNT_1];
                        } else {
                            return filePathArr[getKeyArr[Const.COUNT_0]];
                        }
                    } else {
                        return filePathArr[getKeyArr.length + Const.COUNT_1];
                    }
                } else {
                    return filePathArr[Const.COUNT_1];
                }
            } else {
                return fileName;
            }
        } else {
            return fileName;
        }
    }
}