package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.OnShelfProductModel;

/**
 * . orders repository
 */
public interface OnShelfProductRepository {

	List<OnShelfProductModel> findOrderSummaryByOrderInfo(
			String bussiness,
			String bussitype,
			String privince,
			String agency,
			String producttype,
			String productstatus,
			String numbertype,
			String provideservice,
			String numbermode,
			String agencyname,
			String agencyID,
			String productname,
			String value
		
			
		);
}
