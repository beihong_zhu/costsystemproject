package jp.co.dk.ip.domain.service.common;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.transaction.Transactional;

import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**.
 * AmazonS3サービス
 */
@Service
@Transactional
public class S3AccessService {

    private static final Logger logger = LoggerFactory.getLogger(S3AccessService.class);

    @Autowired
    private SettingContext settingContext;

    @Autowired
    private MessageSource messageSource;

    private AmazonS3 s3Client = null;

    /**.
     * AmazonS3クライアントを取得
     *
     * @return S3クライアント
     */
    private AmazonS3 getS3Client() {

        if (this.s3Client == null) {
            if (Const.BUILD_FLG_LOCAL == settingContext.getS3BbuildFlg()) {
                // ローカル環境用AmazonS3クライアント
                this.s3Client = AmazonS3ClientBuilder
                        .standard()
                        .withCredentials(new AWSStaticCredentialsProvider(
                                new BasicAWSCredentials(settingContext.getS3AccessKey(), settingContext.getS3SecretKey())))
                        .withRegion(settingContext.getS3Region())
                        .build();
            } else {
                this.s3Client = AmazonS3ClientBuilder
                        .standard()
                        .withCredentials(new InstanceProfileCredentialsProvider(false))
                        .withRegion(settingContext.getS3Region())
                        .build();
            }
        }
        return this.s3Client;
    }

    /**.
     * 署名付きURLの生成
     *
     * @param path 対象ファイルまでのフルパス
     * @return 署名付きURL
     */
    @SuppressWarnings("rawtypes")
    public ErrorRes generatePresignedUrl(String path) {

        // リスポンス情報設定
        ErrorRes errorRes = new ErrorRes();
        String errorCode = Const.EMPTY_STRING;
        String[] messageContent = new String[] {};
        String errMsg = Const.EMPTY_STRING;

        // 署名付きURLの生成
        String presignedUrl = Const.EMPTY_STRING;
        try {
            presignedUrl = getS3Client()
                    .generatePresignedUrl(new GeneratePresignedUrlRequest(settingContext.getS3PhotoBucketName(), path)
                            .withMethod(HttpMethod.GET)
                            .withExpiration(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)))
                    .toString();
            errorRes.setResultCode(Const.RESULT_CODE_0);
            errorRes.setResultCnt(Const.COUNT_1);
            errorRes.setPresignedUrl(presignedUrl);
        } catch (AmazonS3Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF016;
            messageContent = new String[] { path };
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_9,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        } catch (Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF017;
            messageContent = new String[] {};
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_9,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        }

        return errorRes;
    }

    /**.
     * S3サーバへ同期CSVファイルをアップロード
     *
     * @param fullpath 同期CSVファイルのフルパス
     * @param file   同期CSVファイル
     * @return 終了コード
     */
    @SuppressWarnings("rawtypes")
    public ErrorRes uploadSyncCsv(String fullpath, File file) throws RuntimeException {

        logger.debug("S3アップロード開始：" + fullpath);

        // リスポンス情報設定
        ErrorRes errorRes = new ErrorRes();
        String errorCode = Const.EMPTY_STRING;
        String[] messageContent = new String[] {};
        String errMsg = Const.EMPTY_STRING;

        // アップロード
        try {
            getS3Client().putObject(new PutObjectRequest(settingContext.getS3PhotoBucketName(), fullpath, file));
            errorRes.setResultCode(Const.RESULT_CODE_0);
            errorRes.setResultCnt(Const.COUNT_1);
            errorRes.setMessageCode(Const.I00005);
        } catch (AmazonS3Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF016;
            messageContent = new String[] { file.getName() };
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_9,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        } catch (Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF017;
            messageContent = new String[] {};
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_9,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        }

        logger.debug("S3アップロード終了：" + fullpath);

        return errorRes;
    }

    /**.
     * S3サーバへPDFファイルをアップロード
     *
     * @param path  PDFディレクトリのフルパス
     * @param files PDFファイル
     * @return 終了コード
     */
    public String uploadPdf(String path, MultipartFile[] files) throws Exception {

        for (MultipartFile file : files) {
            String fileFullPath = path + file.getOriginalFilename();

            logger.debug("S3アップロード開始：" + fileFullPath);

            // アップロード情報設定
            ObjectMetadata om = new ObjectMetadata();
            om.setContentType(file.getContentType());
            om.setContentLength(file.getSize());

            // アップロード
            getS3Client().putObject(new PutObjectRequest(settingContext.getS3PhotoBucketName(), fileFullPath, file.getInputStream(), om));

            logger.debug("S3アップロード終了：" + fileFullPath);
        }

        return Const.I00005;
    }

    /**.
     * S3サーバからPDFファイルを削除
     *
     * @param path PDFディレクトリのフルパス
     * @return 終了コード
     */
    public String deletePdf(String path) throws Exception {

        // アップロード
        List<S3ObjectSummary> pdfList = getS3Client().listObjects(settingContext.getS3PhotoBucketName(), path).getObjectSummaries();
        for (S3ObjectSummary pdfFile : pdfList) {
            logger.debug("S3削除開始：" + pdfFile.getKey());
            getS3Client().deleteObject(new DeleteObjectRequest(settingContext.getS3PhotoBucketName(), pdfFile.getKey()));
            logger.debug("S3削除終了：" + pdfFile.getKey());
        }

        return Const.I00004;
    }

    /**.
     * S3サーバからのダウンロード
     *
     * @param path 対象ファイルまでのフルパス
     * @return ストリーム
     */
    public InputStream downloadS3(String path) {

        logger.debug("S3ダウンロード開始：" + path);

        // ダウンロード
        S3Object s3Object = getS3Client().getObject(new GetObjectRequest(settingContext.getS3PhotoBucketName(), path));

        logger.debug("S3ダウンロード終了：" + path);

        return s3Object.getObjectContent();
    }

    /**.
     * S3サーバにファイル存在チェック
     *
     * @param path 対象ファイルまでのフルパス
     * @return boolean
     */
    public boolean existenceS3(String path) {

        logger.debug("S3存在チェック開始：" + path);

        // 存在チェック
        if (!getS3Client().doesObjectExist(settingContext.getS3PhotoBucketName(), path)) {
            logger.error("S3存在しないで終了：" + path);
            return false;
        }

        logger.debug("S3存在チェック終了：" + path);

        return true;
    }

    /**.
     * S3サーバにファイル存在チェック
     *
     * @param path 対象ファイルまでのフルパス
     * @return boolean
     */
    @SuppressWarnings("rawtypes")
    public ErrorRes judgeExistenceS3(String path) {

        logger.debug("S3存在チェック開始：" + path);

        // リスポンス情報設定
        ErrorRes errorRes = new ErrorRes();
        String errorCode = Const.EMPTY_STRING;
        String[] messageContent = new String[] {};
        String errMsg = Const.EMPTY_STRING;

        // アップロード
        try {
            if (!getS3Client().doesObjectExist(settingContext.getS3PhotoBucketName(), path)) {
                logger.error("S3存在しないで終了：" + path);
                errorRes.setResultCode(Const.RESULT_CODE_0);
                errorRes.setResultCnt(Const.COUNT_1);
                errorRes.setResFlg(Const.FLAG_FALSE);
            }
            errorRes.setResultCode(Const.RESULT_CODE_0);
            errorRes.setResultCnt(Const.COUNT_1);
            errorRes.setResFlg(Const.FLAG_TRUE);
        } catch (AmazonS3Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF016;
            messageContent = new String[] { path };
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_9,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            errorRes.setResFlg(Const.FLAG_FALSE);
            logger.error(Const.ERRORS_S3 + errMsg);
        } catch (Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF017;
            messageContent = new String[] {};
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_9,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            errorRes.setResFlg(Const.FLAG_FALSE);
            logger.error(Const.ERRORS_S3 + errMsg);
        }

        logger.debug("S3存在チェック終了：" + path);

        return errorRes;
    }

    /**.
     * S3サーバにファイルアップロードする署名を作成
     *
     * @param path 対象ファイルまでのフルパス
     * @return String S3サーバにファイルアップロードする署名
     */
    public String uploadS3PresignedURL(String path) {

        logger.debug("S3アップロードURL作成開始：" + path);

        String uploadPresignedURL = Const.EMPTY_STRING;

        // 存在チェック
        if (getS3Client().doesObjectExist(settingContext.getS3PhotoBucketName(), path)) {
            logger.error("S3にアップロードファイルが存在するので終了：" + path);
            uploadPresignedURL = Const.FILE_EXISTENCE;
        }

        if (!Const.FILE_EXISTENCE.equals(uploadPresignedURL)) {
            uploadPresignedURL = getS3Client()
                    .generatePresignedUrl(new GeneratePresignedUrlRequest(settingContext.getS3PhotoBucketName(), path)
                            .withMethod(HttpMethod.PUT)
                            .withExpiration(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)))
                    .toString();
            logger.info("S3アップロードURL作成終了：" + path);
        }

        return uploadPresignedURL;
    }

    /**.
     * S3サーバにファイル移動
     *
     * @param pathFrom コピー元の対象ファイルまでのパス, pathTo コピー先の対象ファイルまでのパス, pdfName S3に存在してるPDFファイル名
     * @return boolean コピー結果
     */
    public boolean copyS3PDF(String pathFrom, String pathTo, String pdfName) {

        logger.debug("S3ファイル移動開始：" + pathFrom);

        // ファイルの相対パスを取得
        String pdfPathFrom = Const.EMPTY_STRING;
        pdfPathFrom = pathFrom + pdfName;
        String pdfPathTo = Const.EMPTY_STRING;
        pdfPathTo = pathTo + pdfName;

        // 存在チェック
        if (!getS3Client().doesObjectExist(settingContext.getS3PhotoBucketName(), pdfPathFrom)) {
            logger.error("S3存在しないで終了：" + pathFrom);
            return false;
        }

        // ファイル移動処理
        getS3Client().copyObject(settingContext.getS3PhotoBucketName(), pdfPathFrom, settingContext.getS3PhotoBucketName(), pdfPathTo);

        logger.debug("S3ファイル移動終了：" + pathFrom);

        return true;
    }

    /**.
     * S3サーバへPDFをアップロード
     *
     * @param path PDFアップロードパス
     * @param file 写真ファイル
     * @return 終了コード
     */
    @SuppressWarnings("rawtypes")
    public ErrorRes uploadPDF(String path, MultipartFile file) throws Exception {

        logger.debug("PDFをS3アップロード開始：" + path);

        // リスポンス情報設定
        ErrorRes errorRes = new ErrorRes();
        String errorCode = Const.EMPTY_STRING;
        String[] messageContent = new String[] {};
        String errMsg = Const.EMPTY_STRING;

        try {
            // アップロード情報設定
            ObjectMetadata om = new ObjectMetadata();
            om.setContentType(file.getContentType());
            om.setContentLength(file.getSize());

            // アップロード
            getS3Client().putObject(new PutObjectRequest(settingContext.getS3PhotoBucketName(), path, file.getInputStream(), om));
            errorRes.setResultCode(Const.RESULT_CODE_0);
            errorRes.setResultCnt(Const.COUNT_1);
            errorRes.setMessageCode(Const.I00005);
        } catch (AmazonS3Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF016;
            messageContent = new String[] { path };
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_3,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        } catch (Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF017;
            messageContent = new String[] {};
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_3,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        }

        logger.debug("PDFをS3アップロード終了：" + path);
        return errorRes;

    }

    /**.
     * S3サーバへCSVをアップロード
     *
     * @param path CSVアップロードパス
     * @param file CSVファイル
     * @return 終了コード
     */
    @SuppressWarnings("rawtypes")
    public ErrorRes uploadCSV(String path, MultipartFile file) throws Exception {

        logger.debug("CSVをS3アップロード開始：" + path);

        // リスポンス情報設定
        ErrorRes errorRes = new ErrorRes();
        String errorCode = Const.EMPTY_STRING;
        String[] messageContent = new String[] {};
        String errMsg = Const.EMPTY_STRING;

        try {
            // アップロード情報設定
            ObjectMetadata om = new ObjectMetadata();
            om.setContentType(file.getContentType());
            om.setContentLength(file.getSize());

            // アップロード
            getS3Client().putObject(new PutObjectRequest(settingContext.getS3PhotoBucketName(), path, file.getInputStream(), om));
            errorRes.setResultCode(Const.RESULT_CODE_0);
            errorRes.setResultCnt(Const.COUNT_1);
            errorRes.setMessageCode(Const.I00005);
        } catch (AmazonS3Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF016;
            messageContent = new String[] { path };
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_3,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        } catch (Exception e) {
            logger.error(Const.ERRORS_S3 + e.getMessage(), e);
            errorCode = Const.EIF017;
            messageContent = new String[] {};
            errMsg = messageSource.getMessage(errorCode, messageContent, Locale.forLanguageTag(settingContext.getLang()));
            errorRes = Utils.customHandleException(
                    Const.RESULT_CODE_3,
                    Const.COUNT_0,
                    errorCode,
                    errMsg,
                    messageContent);
            logger.error(Const.ERRORS_S3 + errMsg);
        }

        logger.debug("CSVをS3アップロード終了：" + path);

        return errorRes;
    }
}
