package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.StockInfoModel;
import lombok.Getter;
import lombok.Setter;

/**
 * . stocks
 */
@Getter
@Setter
@Entity
@Table(name = "stocks")
@IdClass(StocksPK.class)
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "Stocks.StockInfoModel", classes = {
		@ConstructorResult(targetClass = StockInfoModel.class, columns = {
				@ColumnResult(name = "abbreviation", type = String.class),
				@ColumnResult(name = "product_name", type = String.class),
				@ColumnResult(name = "product_mode", type = String.class),
				@ColumnResult(name = "value", type = String.class),
				@ColumnResult(name = "price", type = String.class),
				@ColumnResult(name = "disaccount", type = String.class),
				@ColumnResult(name = "priority", type = String.class),
				@ColumnResult(name = "status", type = String.class)
				})
		})
public class Stocks {

	/**
	 * . 商品ID
	 */
	@Id
	@Column(name = "product_id")
	private String productId;

	/**
	 * . 渠道商ID
	 */
	@Id
	@Column(name = "authory_id")
	private String authoryId;

	/**
	 * . 商品名
	 */
	@Column(name = "product_name")
	private String productName;

	/**
	 * . 商品类型
	 */
	@Column(name = "product_mode")
	private String productMode;

	/**
	 * . 运营商
	 */
	@Column(name = "operator")
	private String operator;

	/**
	 * . 账户控制
	 */
	@Column(name = "account_control")
	private String accountControl;

	/**
	 * . 号码类型
	 */
	@Column(name = "num_type")
	private String numType;

	/**
	 * . 省
	 */
	@Column(name = "province")
	private String province;

	/**
	 * . 市
	 */
	@Column(name = "city")
	private String city;

	/**
	 * . 面值
	 */
	@Column(name = "value")
	private String value;

	/**
	 * .价格
	 */
	@Column(name = "price")
	private String price;


	/**
	 * . 折扣
	 */
	@Column(name = "disaccount")
	private String disaccount;
	
	/**
	 * . 名称
	 */
	@Column(name = "name")
	private String name;

	/**
	 * . 优先级
	 */
	@Column(name = "priority")
	private String priority;
	
	/**
	 * . 状态
	 */
	@Column(name = "status")
	private Integer status;
	
	/**
	 * . 核价格
	 */
	@Column(name = "check_price")
	private Integer check_price;
	
	
	/**
	 * . 核成本
	 */
	@Column(name = "check_cost")
	private Integer check_cost;

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
