package jp.co.dk.ip.domain.model.data;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * 人脸识别 compare data
 */
@Getter
@Setter
@AllArgsConstructor
public class ThresHolds {

    // 1e-3
	@SerializedName("1e-3")
    private Double oneEThree;

    // 1e-5
	@SerializedName("1e-5")
    private Double oneEFive;

    // 1e-4
	@SerializedName("1e-4")
    private Double oneEFour;
}
