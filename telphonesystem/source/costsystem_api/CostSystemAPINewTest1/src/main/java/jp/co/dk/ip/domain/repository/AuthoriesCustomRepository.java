package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.AgentCapitalInfoModel;
import jp.co.dk.ip.domain.model.AgentFundRecorderModel;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.AuthoryInfoModel;


/**
 * . agents custom repository
 */
public interface AuthoriesCustomRepository {

	List<AgentCapitalInfoModel> findAgentCapitalByAgentInfo(String agentId, String agentName, String agentReduceName);

	List<AgentInfoModel> findInfoAgentCapitalByAgentInfo(String accountagency, String agencyname, String  agencyabbreviation , String productmode );
	
	List<AgentFundRecorderModel> findAgentFundRecorderByAgentInfo(String agencyLimited, String agentName, String localLimited,
			String agencyacount,String bussinessMode,String stranctionMode,String channelLimited,String agencyabbreviate,String ocrManufacturingDate);
	
	List<AuthoryInfoModel> findAuthoryInfo(String abbreviation);
}
