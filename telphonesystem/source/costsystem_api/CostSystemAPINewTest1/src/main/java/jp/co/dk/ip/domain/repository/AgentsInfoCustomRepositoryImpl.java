package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentInfoModel;;

/**
 * . agents custom repository
 */
public class AgentsInfoCustomRepositoryImpl implements AgentsInfoCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentInfoModel> findInfoAgentCapitalByAgentInfo(
			String accountagency,
			String agencyname,
			String agencyabbreviation,
			String productmode) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " * "
                + "FROM "
                + " agents a "
                + "WHERE "
                + " a.business_mode = '1' ";
		if (!Objects.equals(accountagency, Const.EMPTY_STRING) && !Objects.equals(accountagency, null)) {
			sql += "         AND a.agent_id = :accountagency ";
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}

		// sql injection preventing
		accountagency = Utils.escapeSql(accountagency);
		agencyname = Utils.escapeSql(agencyname);
		agencyabbreviation = Utils.escapeSql(agencyabbreviation);
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.AgentInfoModel");
		if (!Objects.equals(accountagency, Const.EMPTY_STRING) && !Objects.equals(accountagency, null)) {
			query.setParameter("agentId", accountagency);
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			query.setParameter("agentName", agencyname);
		}
		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
			query.setParameter("agentReduceName", agencyabbreviation);
		}
		List<AgentInfoModel> list = query.getResultList();
		return list;
	}
}
