package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.AuthoryInfoModel;
import jp.co.dk.ip.domain.model.StockInfoModel;
import lombok.Getter;
import lombok.Setter;

/**
 * . authories
 */
@Getter
@Setter
@Entity
@Table(name = "authories")
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "Authories.AuthoryInfoModel", classes = {
		@ConstructorResult(targetClass = AuthoryInfoModel.class, columns = {
				@ColumnResult(name = "abbreviation", type = String.class),			
				@ColumnResult(name = "account_name", type = String.class),
				
				}) 
		})
public class Authories {

	/**
	 * . 渠道商ID
	 */
	@Id
	@Column(name = "authory_id")
	private String authoryId;

	/**
	 * . 渠道商名
	 */
	@Column(name = "authory_name")
	private String authoryName;

	/**
	 * . 简称
	 */
	@Column(name = "abbreviation")
	private String abbreviation;

	/**
	 * . 联络方式
	 */
	@Column(name = "tel")
	private String tel;

	/**
	 * . 电子邮件
	 */
	@Column(name = "mail")
	private String mail;

	/**
	 * . 省
	 */
	@Column(name = "province")
	private String province;

	/**
	 * . 市
	 */
	@Column(name = "city")
	private String city;

	/**
	 * . 地址
	 */
	@Column(name = "address")
	private String address;

	/**
	 * . 账户名
	 */
	@Column(name = "account_name")
	private String accountName;

	/**
	 * . 账户状态
	 */
	@Column(name = "acount_status")
	private String acountStatus;

	/**
	 * . 代理类型
	 */
	@Column(name = "authory_type")
	private String authoryType;

	/**
	 * . 业务模式
	 */
	@Column(name = "business_mode")
	private String businessMode;

	/**
	 * . 接口状态
	 */
	@Column(name = "interface_status")
	private String interfaceStatus;

	/**
	 * . 登录密码
	 */
	@Column(name = "log_ps")
	private String logPs;

	/**
	 * . 支付密码
	 */
	@Column(name = "pay_ps")
	private String payPs;

	/**
	 * . 通知状态
	 */
	@Column(name = "info_status")
	private String infoStatus;

	/**
	 * . 优先级
	 */
	@Column(name = "prority")
	private String prority;

	/**
	 * . 分流比
	 */
	@Column(name = "split_flow")
	private String splitFlow;

	/**
	 * . 核成本
	 */
	@Column(name = "cost")
	private Double cost;

	/**
	 * . 核价格
	 */
	@Column(name = "price")
	private Double price;
	
    /**.
     * 渠道商发送地址
     */
    @Column(name = "call_back_url")
    private String callBackUrl;

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
