package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.APIAuthory001FakeReq;
import jp.co.dk.ip.application.response.APIAuthory001FakeRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.service.common.OrderPostService;

/**
 * authory data post 001 API Service
 */
@Service
@Transactional
public class APIAuthory001FakeService {

    private static final Logger logger = LoggerFactory.getLogger(APIAuthory001FakeService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    /**
     * authory data reception process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIAuthory001FakeRes clientDataProcess(APIAuthory001FakeReq apiAuthory001FakeReq) throws Exception {

        logger.debug("APIAuthory001FakeService clientDataProcess");

        try {
        	APIAuthory001FakeRes apiAuthory001FakeRes = new APIAuthory001FakeRes();
        	
			// MD5 validation
        	String md5Sign = Const.EMPTY_STRING;
        	md5Sign = OrderPostService.getAuthory001PostSign(
					apiAuthory001FakeReq.getAmount(),
					apiAuthory001FakeReq.getCallbackurl(),
					apiAuthory001FakeReq.getMerid(),
					apiAuthory001FakeReq.getOrderid(),
					apiAuthory001FakeReq.getRechargeno(),
					apiAuthory001FakeReq.getTs(),
					apiAuthory001FakeReq.getType());
			if (!Objects.equals(md5Sign, apiAuthory001FakeReq.getSign())) {
				// response error
				// {"code":"001","msg":"提交失败" ,"orderid":"xxxx","orderno":""}
				apiAuthory001FakeRes.setCode(Const.AUTHORY_RES_CODE.CODE_001);
				apiAuthory001FakeRes.setMsg(Const.AUTHORY_RES_CODE.MSG_FAILURE);
				apiAuthory001FakeRes.setOrderid(apiAuthory001FakeReq.getOrderid());
				apiAuthory001FakeRes.setOrderno(apiAuthory001FakeReq.getMerid());
				return apiAuthory001FakeRes;
			}
        	
        	apiAuthory001FakeRes.setCode(Const.AUTHORY_RES_CODE.CODE_000);
        	apiAuthory001FakeRes.setMsg(Const.AUTHORY_RES_CODE.MSG_SUCCESS);
        	apiAuthory001FakeRes.setOrderid(apiAuthory001FakeReq.getOrderid());
        	apiAuthory001FakeRes.setOrderno(apiAuthory001FakeReq.getMerid());
			return apiAuthory001FakeRes;
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAUTHORY + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAUTHORY + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAUTHORY + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAuthory001FakeRes getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("APIAuthory001FakeService getErrRes");

        APIAuthory001FakeRes apiClientRes = new APIAuthory001FakeRes();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiClientRes.setResultCode(resultCode);
        apiClientRes.setMessageCode(errorCode);
        apiClientRes.setMessage(errMsg);
        return apiClientRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIClientRes response class
     */
    public APIAuthory001FakeRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAuthory001FakeService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAuthory001FakeRes setErrRes(CustomHandleException e) {

        logger.debug("APIAuthory001FakeService setErrRes");

        APIAuthory001FakeRes apiClientRes = new APIAuthory001FakeRes();
        apiClientRes.setResultCode(e.getErrorMessage().getResultCode());
        apiClientRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiClientRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiClientRes.setMessage(e.getErrorMessage().getMessage());
        return apiClientRes;
    }
}
