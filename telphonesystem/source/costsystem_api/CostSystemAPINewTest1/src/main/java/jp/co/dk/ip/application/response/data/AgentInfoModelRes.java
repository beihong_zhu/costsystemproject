package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class AgentInfoModelRes {

	/**
	 * . 账户名
	 */
	private String accountagency;

	/**
	 * . 简称
	 */
	private String agecnyabbreviate;

	/**
	 * . 省
	 */

	private String province;

	/**
	 * . 类型
	 */
	private String type;

	/**
	 * . 模式
	 */
	private String mode;

	/**
	 * . 可用资金
	 */
	private String balance;

	/**
	 * . 授信额度
	 */
	private String use;

	/**
	 * . 状态
	 */
	private String status;
    
   

}
