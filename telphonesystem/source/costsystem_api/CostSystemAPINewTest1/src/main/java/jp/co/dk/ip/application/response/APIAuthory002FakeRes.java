package jp.co.dk.ip.application.response;

import jp.co.dk.ip.application.response.data.BaseRes;
import jp.co.dk.ip.application.response.data.DataRes;
import lombok.Getter;
import lombok.Setter;

/**
 * client data post 002 API Response
 */
@Getter
@Setter
public class APIAuthory002FakeRes extends BaseRes {

    // code
    private Integer code;
    
    // response data
    private DataRes data;
    
    // message
    private String message;
}
