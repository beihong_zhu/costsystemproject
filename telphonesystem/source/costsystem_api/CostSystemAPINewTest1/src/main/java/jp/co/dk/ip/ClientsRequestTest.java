package jp.co.dk.ip;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.CountDownLatch;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;

import jp.co.dk.ip.application.request.APIAuthory001FakeReq;
import jp.co.dk.ip.common.Const;

public class ClientsRequestTest {
	
	public static void attendanceOption1(String url, String input) throws UnsupportedOperationException, IOException {

		try {
			CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
			client.start();
			final HttpPost post = new HttpPost(url);
			APIAuthory001FakeReq apiClientReq = new APIAuthory001FakeReq();
			apiClientReq.setMerid(input);
			// 设置请求头 这里根据个人来定义
			post.addHeader("Content-type", "application/json; charset=utf-8");
			post.setHeader("Accept", "application/json");

			StringEntity stringEntity = new StringEntity(apiClientReq.toString());
			post.setEntity(stringEntity);
			// 执行
			try {
				client.execute(post, null);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public static void attendanceOption2(String url, String input) throws UnsupportedOperationException, IOException {
	
		CloseableHttpAsyncClient httpAsyncClient = HttpAsyncClients.createDefault();
		httpAsyncClient.start();

		final CountDownLatch latch = new CountDownLatch(1);

		final HttpPost post = new HttpPost(url);

		APIAuthory001FakeReq apiClientReq = new APIAuthory001FakeReq();
		apiClientReq.setMerid(input);
		// 设置请求头 这里根据个人来定义
		post.addHeader("Content-type", "application/json; charset=utf-8");
		post.setHeader("Accept", "application/json");

		StringEntity stringEntity = null;
		stringEntity = new StringEntity(apiClientReq.toString());
		post.setEntity(stringEntity);

		System.out.println(" caller thread id is : " + Thread.currentThread().getId());

		httpAsyncClient.execute(post, new FutureCallback<HttpResponse>() {
			@Override
			public void completed(final HttpResponse response) {
				latch.countDown();
				System.out.println(" callback thread id is : " + Thread.currentThread().getId());
				System.out.println(post.getRequestLine() + "->" + response.getStatusLine());
				try {
					String content = EntityUtils.toString(response.getEntity(), "UTF-8");
					System.out.println(" response content is : " + content);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void failed(final Exception ex) {
				latch.countDown();
				System.out.println(post.getRequestLine() + "->" + ex);
				System.out.println(" callback thread id is : " + Thread.currentThread().getId());
			}

			@Override
			public void cancelled() {
				latch.countDown();
				System.out.println(post.getRequestLine() + " cancelled");
				System.out.println(" callback thread id is : " + Thread.currentThread().getId());
			}
		});
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try {
			httpAsyncClient.close();
		} catch (IOException ignore) {

		}
	}

	public static void main(String[] args) throws UnsupportedOperationException, IOException {

		String url = Const.EMPTY_STRING;
		url = "http://192.168.3.2:8080/api/APICLIENT";
		
		String input = Const.EMPTY_STRING;
		input = "aaa";
		
		attendanceOption1(url, input);
		// attendanceOption2(url, input);
	}
}
