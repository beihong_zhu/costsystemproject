package jp.co.dk.ip.application.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIAgentOrderResultCallBackFakeReq;
import jp.co.dk.ip.application.response.APIAgentOrderResultCallBackFakeRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.APIAgentOrderResultCallBackFakeService;

/**
  * agent data post call back Controller
 */
@RestController
@RequestMapping("/api")
public class APIAgentOrderResultCallBackFakeController {

    private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderResultCallBackFakeController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private APIAgentOrderResultCallBackFakeService apiAgentCallBackFakeService;

    @PostMapping("/APIAGENTOrderResultCALLBACKFAKE")
	private APIAgentOrderResultCallBackFakeRes agentDataProcess(@RequestBody @Valid APIAgentOrderResultCallBackFakeReq apiAgentCallBackFakeReq, BindingResult bindingResult) {
    	
        logger.debug("APIAgentFakeRes agentDataProcess");

        APIAgentOrderResultCallBackFakeRes apiAgentCallBackFakeRes = new APIAgentOrderResultCallBackFakeRes();

        // 单项目验证
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().forEach(fieldError -> {
                String fieldName = MapSort.getMapValue(fieldError.getField());
                String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
                logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
                logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
            });
            return apiAgentCallBackFakeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
        }

        try {
        	// response获取
        	apiAgentCallBackFakeRes = apiAgentCallBackFakeService.agentDataProcess(apiAgentCallBackFakeReq);
        } catch (CustomHandleException e) {
            logger.error(Const.ERRRES_APIAGENT + e.getMessage(), e);
            apiAgentCallBackFakeRes = apiAgentCallBackFakeService.setErrRes(e);
        } catch (Exception e) {
            logger.error(Const.ERRRES_APIAGENT + e.getMessage(), e);
            apiAgentCallBackFakeRes = apiAgentCallBackFakeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000016, Const.ERRRES_APIAGENTORDERRESULT);
        }

        return apiAgentCallBackFakeRes;
    }
}
