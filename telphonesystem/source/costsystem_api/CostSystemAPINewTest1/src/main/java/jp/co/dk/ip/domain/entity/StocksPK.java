package jp.co.dk.ip.domain.entity;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * . stocks pk
 */
@SuppressWarnings("serial")
@Getter
@Setter
@EqualsAndHashCode
public class StocksPK implements Serializable {

	/**
	 * . 商品ID
	 */
	private String productId;

	/**
	 * . 渠道商ID
	 */
	private String authoryId;
}
