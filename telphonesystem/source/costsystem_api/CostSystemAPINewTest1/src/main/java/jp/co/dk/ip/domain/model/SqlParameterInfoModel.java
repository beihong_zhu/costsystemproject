package jp.co.dk.ip.domain.model;

import lombok.Getter;
import lombok.Setter;

/**.
 * SQL文パラメータ
 */
@Getter
@Setter
public class SqlParameterInfoModel {

    // SQL文のパラメータ
    private String resStr;

    // SQL文のパラメータ数
    private int maxNum;
}
