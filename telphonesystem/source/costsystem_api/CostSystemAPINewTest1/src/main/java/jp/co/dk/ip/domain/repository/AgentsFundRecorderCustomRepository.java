package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.AgentFundRecorderModel;;

/**
 * . agents custom repository
 */
public interface AgentsFundRecorderCustomRepository {

	List<AgentFundRecorderModel> findAgentFundRecorderByAgentInfo(String agencyLimited, String agentName, String localLimited,
	String agencyacount,String bussinessMode,String stranctionMode,String channelLimited,String agencyabbreviate,String ocrManufacturingDate);
}
