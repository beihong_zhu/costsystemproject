package jp.co.dk.ip.domain.repository;


import jp.co.dk.ip.domain.entity.Stocks;
import jp.co.dk.ip.domain.entity.StocksPK;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * . stocks repository
 */
@Repository
public interface StocksRepository extends JpaRepository<Stocks, StocksPK>, StocksCustomRepository {

	// @Lock(value = LockModeType.PESSIMISTIC_WRITE)
	List<Stocks> findByProductIdAndStatusAndIsDeleted(String productId, Integer status, int isDeleted);
	List<Stocks> findByIsDeleted(int isDeleted);
}
