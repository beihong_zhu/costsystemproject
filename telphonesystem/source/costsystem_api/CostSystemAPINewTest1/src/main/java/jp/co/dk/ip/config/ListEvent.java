package jp.co.dk.ip.config;

import java.util.List;

import org.springframework.context.ApplicationEvent;

import jp.co.dk.ip.application.request.APIAuthory001FakeReq;
import lombok.Getter;
import lombok.Setter;

/**
 * 自定义事件
 * @author shengwu ni
 * @date 2018/07/05
 */
@SuppressWarnings("serial")
@Getter
@Setter
public class ListEvent extends ApplicationEvent {

	private List<APIAuthory001FakeReq> apiClientReqList;

    public ListEvent(Object source, List<APIAuthory001FakeReq> apiClientReqList) {
        super(source);
        this.apiClientReqList = apiClientReqList;
    }
}
