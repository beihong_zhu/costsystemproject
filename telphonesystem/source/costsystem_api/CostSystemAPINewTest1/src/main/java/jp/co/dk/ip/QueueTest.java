package jp.co.dk.ip;

import java.util.*;

public class QueueTest {
	ArrayList<Object> arraylist = new ArrayList<Object>();
	Iterator<Object> ite = arraylist.iterator();

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner sanner = new Scanner(System.in);
		QueueTest test = new QueueTest();
		while (true) {
			System.out.println("请输入,取消请按N");
			String s = sanner.next();
			if (s.equals("N") || s.equals("n")) {
				break;
			} else {
				test.jinZhan(s);
			}
		}
		while (test.ite.hasNext()) {
			String str = (String) test.chuZhan();
			System.out.println(str);
		}
	}

	private void jinZhan(Object obj) {
		arraylist.add(obj);
	}

	private Object chuZhan() {
		if (ite.hasNext()) {
			// 这个是队列  入栈
			Object obj = arraylist.get(0);
			// 这个是队列  出栈
			// Object obj = arraylist.get(arraylist.size() - 1);
			arraylist.remove(0);
			return obj;
		} else {
			return null;
		}
	}

	private int size() {
		return arraylist.size();
	}
}
