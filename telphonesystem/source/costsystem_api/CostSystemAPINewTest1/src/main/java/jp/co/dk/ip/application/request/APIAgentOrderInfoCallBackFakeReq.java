package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception call back request
 */
@Getter
@Setter
public class APIAgentOrderInfoCallBackFakeReq extends BaseReq {

    // 账号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String corpId;
    
    // 代理商流水号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 50, message = "{ECT010}")
    private String reqId;
    
    // 平台交易单号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String orderId;
    
    // 实付金额
    @NotEmpty(message = "{ECT008}")
    @Length(max = 15, message = "{ECT010}")
    private String price;
    
    // 运营商：1-移动，2-联通，3-电信，4-中石化
    @NotEmpty(message = "{ECT008}")
    @Length(max = 1, message = "{ECT010}")
    private String status;
    
    // 签名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String sign;
}