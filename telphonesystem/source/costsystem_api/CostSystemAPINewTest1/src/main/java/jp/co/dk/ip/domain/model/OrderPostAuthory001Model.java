package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.domain.model.data.OrderPostAuthory001Req;
import jp.co.dk.ip.domain.model.data.OrderResultPostReq;
import lombok.Getter;
import lombok.Setter;

/**.
 * order post request Info
 */
@Getter
@Setter
public class OrderPostAuthory001Model {

    // order post request
    private OrderPostAuthory001Req orderPostAuthory001Req;
    
    // order post url
    private String url;
}