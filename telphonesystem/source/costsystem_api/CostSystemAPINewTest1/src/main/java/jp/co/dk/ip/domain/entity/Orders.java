package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import lombok.Getter;
import lombok.Setter;

/**
 * . orders
 */
@Getter
@Setter
@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener.class)
public class Orders {

	/**
	 * . 订单编号
	 */
	@Id
	@Column(name = "order_id")
	private String orderId;

	/**
	 * . 代理商账号
	 */
	@Column(name = "corp_id")
	private String corpId;

	/**
	 * . 代理商流水号
	 */
	@Column(name = "req_id")
	private String reqId;

	/**
	 * . 渠道商流水号
	 */
	@Column(name = "bill_id")
	private String billId;

	/**
	 * . 时间戳
	 */
	@Column(name = "ts")
	private String ts;

	/**
	 * . 面值
	 */
	@Column(name = "money")
	private Integer money;

	/**
	 * . 运营商
	 */
	@Column(name = "sp_id")
	private String spId;

	/**
	 * . 省份编码
	 */
	@Column(name = "prov_id")
	private String provId;

	/**
	 * . 充值号码
	 */
	@Column(name = "number")
	private String number;

	/**
	 * . 回调地址
	 */
	@Column(name = "ret_url")
	private String retUrl;

	/**
	 * . 状态
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * . 价格
	 */
	@Column(name = "price")
	private Double price;

	/**
	 * . 方式
	 */
	@Column(name = "order_form")
	private String orderForm;

	/**
	 * . 类型
	 */
	@Column(name = "order_type")
	private String orderType;

	/**
	 * . 参考凭证
	 */
	@Column(name = "ref_id")
	private String refId;

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
