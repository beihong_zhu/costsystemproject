package jp.co.dk.ip.application.request;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import jp.co.dk.ip.application.request.data.BaseReq;
import lombok.Getter;
import lombok.Setter;

/**
 * agent data reception call back request
 */
@Getter
@Setter
public class APIAgentOrderResultCallBackFakeReq extends BaseReq {

    // 账号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String corpId;
    
    // 代理商流水号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 50, message = "{ECT010}")
    private String reqId;
    
    // 平台交易单号
    @NotEmpty(message = "{ECT008}")
    @Length(max = 20, message = "{ECT010}")
    private String orderId;
    
    // 充值结果
    @NotEmpty(message = "{ECT008}")
    @Length(max = 5, message = "{ECT010}")
    private String result;
    
    // 签名
    @NotEmpty(message = "{ECT008}")
    @Length(max = 32, message = "{ECT010}")
    private String sign;
}