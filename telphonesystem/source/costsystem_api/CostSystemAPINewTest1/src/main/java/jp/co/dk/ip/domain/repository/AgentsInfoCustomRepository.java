package jp.co.dk.ip.domain.repository;

import java.util.List;

import jp.co.dk.ip.domain.model.AgentInfoModel;;

/**
 * . agents custom repository
 */
public interface AgentsInfoCustomRepository {

	List<AgentInfoModel> findInfoAgentCapitalByAgentInfo(
			String accountAgency,
			String agencyName,
			String agencyAbbreviation,
			String productMode);
}
