package jp.co.dk.ip.common.enums;

/**
 * 请求状态枚举类
 */
public enum StateCodeEnum {
    SUCCESS(200, "处理完成！"),
    ERRORSIGN(400, "接口校验失败"),
    ERRORPOSTUSER(401, "用户验证失败，请校验客户标识是否正确"),
    ERRORPOSTPASS(402, "用户验证失败，请校验客户密码是否正确"),
    ERRORPOSTNOROW(403, "门店取号表未设置，请联系管理员!"),
    ERRORSTOREPRODUCT(404, "商品主档版本信息未找到，请联系中台管理员!"),
    ERROR(500, "业务处理错误"),
    UNAVAILABLE(503, "认证失败"),

    // 日期
    MONDAY(1, "周一"),
    TUESDAY(2, "周二"),
    WEDNESDAY(3, "周三"),
    THURSDAY(4, "周四"),
    FRIDAY(5, "周五"),
    SATURDAY(6, "周六"),
    SUNDAY(7, "周日")
    ;

    /**
     * 其他自定义业务异常
     */

    private String msg;
    private int code;

    public String msg() {
        return msg;
    }

    public int getCode() {
        return code;
    }

    StateCodeEnum(int code, String msg) {
        this.msg = msg;
        this.code = code;
    }
}
