package jp.co.dk.ip.application.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIAuthory001FakeReq;
import jp.co.dk.ip.application.response.APIAuthory001FakeRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.APIAuthory001FakeService;

/**
  * client data post 001 API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAuthory001FakeController {

    private static final Logger logger = LoggerFactory.getLogger(APIAuthory001FakeController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private APIAuthory001FakeService apiAuthory001FakeService;

    @PostMapping("/APIAUTHORY001FAKE")
	private APIAuthory001FakeRes clientDataProcess(@RequestBody @Valid APIAuthory001FakeReq apiAuthory001FakeReq, BindingResult bindingResult) {
    	
        logger.debug("APIAuthory001FakeRes clientDataProcess");

        APIAuthory001FakeRes apiAuthory001FakeRes = new APIAuthory001FakeRes();

        // 单项目验证
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().forEach(fieldError -> {
                String fieldName = MapSort.getMapValue(fieldError.getField());
                String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
                logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
                logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
            });
            return apiAuthory001FakeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
        }

        try {
        	// response获取
        	apiAuthory001FakeRes = apiAuthory001FakeService.clientDataProcess(apiAuthory001FakeReq);
        } catch (CustomHandleException e) {
            logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
            apiAuthory001FakeRes = apiAuthory001FakeService.setErrRes(e);
        } catch (Exception e) {
            logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
            apiAuthory001FakeRes = apiAuthory001FakeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000005, Const.ERRRES_API0101);
        }

        return apiAuthory001FakeRes;
    }
}
