package jp.co.dk.ip.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
@AllArgsConstructor
public class AgentInfoCapitalInfoModel {

	/**
	 * . 账户名
	 */
	private String agencyName;

	/**
	 * . 简称
	 */
	private String agecnyAbbreviate;

	/**
	 * . 省
	 */
	private String agencyMode;

	/**
	 * . 市
	 */
	private String agencyProvince;

	/**
	 * . 模式
	 */
	private String agencyUseless;

	/**
	 * . 可用余额
	 */
	private String agenyCredit;

	/**
	 * . 可用与否
	 */
	private String agencyType;

	/**
	 * . 授信额度
	 */
	private String agenyTime;

	/**
	 * . 创建时间
	 */
}
