package jp.co.dk.ip.domain.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.StockInfoModel;
import jp.co.dk.ip.domain.model.ProductInfoModel;

/**
 * . 渠道商信息取得
 */
@SuppressWarnings("unchecked")
public class StocksCustomRepositoryImpl implements StocksCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<StockInfoModel> findStockByStockInfo(
			String channelNumber,
			String channelAbbre,
			String productMode,
			String productStatus) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " s.product_id, "
                + " s.authory_id, "
                + " s.product_name, "
                + " s.product_mode, "
                + " s.operator, "
                + " s.account_control, "
                + " s.num_type, "
                + " s.province, "
                + " s.city, "
                + " s.value, "
                + " s.name, "
                + " s.disaccount, "
                + " s.status, "
                + " a.prority, "
                + " a.split_flow "
                + "FROM "
                + " stocks s "
                + "LEFT JOIN authories a "
                + "ON s.authory_id = a.authory_id "
                + "AND a.is_deleted = '0' "
                + "WHERE "
                + " s.is_deleted = '0' ";
		if (null != channelNumber) {
			sql += "         AND s.authory_id = :channelNumber ";
		}
		if (null != channelAbbre) {
			sql += "         AND s.operator = :channelAbbre ";
		}
		if (null != productMode) {
			sql += "         AND s.product_mode = :productMode ";
		}
		if (null != productStatus) {
			sql += "         AND s.status = :productStatus ";
		}
		
		// sql injection preventing
		channelNumber = Utils.escapeSql(channelNumber);
		channelAbbre = Utils.escapeSql(channelAbbre);
		productMode = Utils.escapeSql(productMode);
		productStatus = Utils.escapeSql(productStatus);

		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Stocks.StockInfoModel");
		if (null != channelNumber) {
			query.setParameter("channelNumber", channelNumber);
		}
		if (null != channelAbbre) {
			query.setParameter("channelAbbre", channelAbbre);
		}
		if (null != productMode) {
			query.setParameter("productMode", productMode);
		}
		if (null != productStatus) {
			query.setParameter("productStatus", productStatus);
		}
		List<StockInfoModel> list = query.getResultList();
		return list;
	}
	
	
	@Override
	public List<ProductInfoModel> findStockByProductInfo(
			String aggencyLimited,
			String localLimited,
			String channelLimited,
			String ocrManufacturingDate) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " s.product_id, "
                + " s.authory_id, "
                + " s.product_name, "
                + " s.product_mode, "
                + " s.operator, "
                + " s.account_control, "
                + " s.num_type, "
                + " s.province, "
                + " s.city, "
                + " s.value, "
                + " s.name, "
                + " s.disaccount, "
                + " s.status, "
                + " a.prority, "
                + " a.split_flow "
                + "FROM "
                + " stocks s "
                + "LEFT JOIN authories a "
                + "ON s.authory_id = a.authory_id "
                + "AND a.is_deleted = '0' "
                + "WHERE "
                + " s.is_deleted = '0' ";
		if (null != aggencyLimited) {
			sql += "         AND s.authory_id = :channelNumber ";
		}
		if (null != localLimited) {
			sql += "         AND s.operator = :channelAbbre ";
		}
		if (null != channelLimited) {
			sql += "         AND s.product_mode = :productMode ";
		}
		if (null != ocrManufacturingDate) {
			sql += "         AND s.status = :productStatus ";
		}
		
		// sql injection preventing
		aggencyLimited = Utils.escapeSql(aggencyLimited);
		localLimited = Utils.escapeSql(localLimited);
		channelLimited = Utils.escapeSql(channelLimited);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);

		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Stocks.ProductInfoModel");
		if (null != aggencyLimited) {
			query.setParameter("aggencyLimited", aggencyLimited);
		}
		if (null != localLimited) {
			query.setParameter("localLimited", localLimited);
		}
		if (null != channelLimited) {
			query.setParameter("channelLimited", channelLimited);
		}
		if (null != ocrManufacturingDate) {
			query.setParameter("ocrManufacturingDate", ocrManufacturingDate);
		}
		List<ProductInfoModel> list = query.getResultList();
		return list;
	}
}
