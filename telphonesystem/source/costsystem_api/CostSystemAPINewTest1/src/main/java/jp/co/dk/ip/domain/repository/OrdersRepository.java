package jp.co.dk.ip.domain.repository;

import jp.co.dk.ip.domain.entity.Orders;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**.
 * orders repository
 */
@Repository
public interface OrdersRepository extends JpaRepository<Orders, String>, OrdersCustomRepository {

    // @Lock(value = LockModeType.PESSIMISTIC_WRITE)
	List<Orders> findByStatusAndIsDeleted(String status, int isDeleted);
    
    // @Lock(value = LockModeType.PESSIMISTIC_WRITE)
    Orders findByOrderIdAndIsDeleted(String orderId, int isDeleted);
}
