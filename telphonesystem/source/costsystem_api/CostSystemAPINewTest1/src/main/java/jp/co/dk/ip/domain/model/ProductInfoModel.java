package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class ProductInfoModel {

    // 代理商ID
    private String aggencyLimited;
    
    // 代理商流水号
    private String localLimited;

    // 代理商账户简称
	private String channelLimited;
    
    // 交易金额API0112Service.java
    private String ocrManufacturingDate;
    
   

    /**.
     * CSV Field
     *
     * @return String CSV Field
     */
    public String toRow() {
        return String.format(
                Utils.combinationString(Const.MANAGEMENTFILE.S_MARK, Const.MANAGEMENTFILE.USER_NUM_13),
                this.aggencyLimited,
                this.localLimited,
                this.channelLimited,
                this.ocrManufacturingDate
              
        		);
    }
}
