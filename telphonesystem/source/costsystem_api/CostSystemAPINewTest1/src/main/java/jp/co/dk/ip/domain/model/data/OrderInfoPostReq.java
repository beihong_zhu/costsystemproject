package jp.co.dk.ip.domain.model.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * Agent Order Info
 */
@Getter
@Setter
public class OrderInfoPostReq {

    // 结果代码(最大长度:4 必填:Y)
    private String retCode;
    
    // 平台交易单号(最大长度:20 必填:N)
    private String orderId;
    
    // 价格（以元为单位，精确到小数点后4位）(最大长度:15 必填:N)
    private String price;
    
    // 签名验证
    private String sign;
}
