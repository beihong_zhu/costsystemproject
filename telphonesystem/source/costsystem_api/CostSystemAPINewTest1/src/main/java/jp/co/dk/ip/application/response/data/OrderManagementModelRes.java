package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class OrderManagementModelRes {

    // 代理名字
    private String agencyname;
    
    // 代理流水
    private String agecnylimited;

    // 本地流水
	private String locallimited;
    
    // 渠道流水
    private String channellimited;
    
    // 商品名
    private String productname;
    
    // 电话号码
    private String telnumber;
    
    // 面值
    private String value;
    
    // 状态
    private String status;
    
    // 时间
    private String time;
    
    
}
