package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
@AllArgsConstructor
public class OnShelfProductModel {

	// 代理简称
    private String AgentReduceName;
    
    // 商品名称    
	private String productName;
    
    // 商品类型 
    private String productType;
    
    // 运营商
    private String spId;
    
    // 面额   
    private String value;
   
    // 价格
    private String price;
    
    // 折扣
    private String disaccount;
    
    // 检查价格
    private String checkPrice;
    
    // 检查成本
    private String checkCost;
    
    // 状态
    private String status;
    

    /**.
     * CSV Field
     *
     * @return String CSV Field
     */
    public String toRow() {
        return String.format(
                Utils.combinationString(Const.MANAGEMENTFILE.S_MARK, Const.MANAGEMENTFILE.USER_NUM_13),
                this.AgentReduceName,
                this.productName,
                this.productType,
                this.spId,
                this.value,
                this.price,
                this.disaccount,
                this.checkPrice,
                this.checkCost,
                this.status
        		);
    }
}
