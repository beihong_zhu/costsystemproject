package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class AgentInfo {

    // 账户名
    private String accountagency;
    
    // 代理商流水号
    private String agencyname;

    // 代理商账户简称
	private String agecnyabbreviate;
    
    // 省
    private String province;
    
    // 模式
    private String mode;
    
    // 类型
    private String type;
    
    // 可用资金
    private String balance;
    
    // 可用资金
    private String useless;
    
    // 授信额度
    private String credit;
    
    // 状态
    private String status;
    
    // 失败平均时长
    private String time;
    
   
}
