package jp.co.dk.ip.application.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.dk.ip.application.request.APIAuthory002FakeReq;
import jp.co.dk.ip.application.response.APIAuthory002FakeRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.MapSort;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.domain.service.APIAuthory002FakeService;

/**
  * client data post 002 API Controller
 */
@RestController
@RequestMapping("/api")
public class APIAuthory002FakeController {

    private static final Logger logger = LoggerFactory.getLogger(APIAuthory002FakeController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private APIAuthory002FakeService apiAuthory002FakeService;

    @PostMapping("/APIAUTHORY002FAKE")
	private APIAuthory002FakeRes clientDataProcess(@RequestBody @Valid APIAuthory002FakeReq apiAuthory002FakeReq, BindingResult bindingResult) {
    	
        logger.debug("APIAuthory002FakeRes clientDataProcess");

        APIAuthory002FakeRes apiAuthory002FakeRes = new APIAuthory002FakeRes();

        // 单项目验证
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().forEach(fieldError -> {
                String fieldName = MapSort.getMapValue(fieldError.getField());
                String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + fieldError.getDefaultMessage();
                logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + fieldError.getCode());
                logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
            });
            return apiAuthory002FakeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000001, Const.FIELD_ERROR);
        }

        try {
        	// response获取
        	apiAuthory002FakeRes = apiAuthory002FakeService.clientDataProcess(apiAuthory002FakeReq);
        } catch (CustomHandleException e) {
            logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
            apiAuthory002FakeRes = apiAuthory002FakeService.setErrRes(e);
        } catch (Exception e) {
            logger.error(Const.ERRRES_APIAUTHORY + e.getMessage(), e);
            apiAuthory002FakeRes = apiAuthory002FakeService.getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.E000005, Const.ERRRES_API0101);
        }

        return apiAuthory002FakeRes;
    }
}
