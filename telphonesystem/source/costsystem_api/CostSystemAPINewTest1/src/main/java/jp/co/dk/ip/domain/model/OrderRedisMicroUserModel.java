package jp.co.dk.ip.domain.model;

import lombok.Getter;
import lombok.Setter;

/**
 * . orders redis micro user
 */
@Getter
@Setter
public class OrderRedisMicroUserModel {

	// 客户端账号
	private String corpId;
	
    // 代理商流水号
    private String reqId;
	
	// 用户名
	private String username;

	// 密码
	private String password;

	// 用户手机号
	private String tel;

	// 时间戳，格式为：yyyyMMddHHmmss
	private String ts;

	// 用户性别
	private String gender;

	// 顾客账号
	private String accountId;

	// 充值金额
	private String amount;
	
	// 充值号码
	private String number;

	// 充值余额
	private String balance;

	// 充值状态（1-成功，0-失败）
	private String chargeStatus;

	// 充值返金
	private String returnMoney;
	
    // lock (1-lock, 0-unlock)
    private String lock;
    
    // run (1-run, 0-no run)
    private String isRun;
}
