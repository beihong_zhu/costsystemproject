package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.ConstructorResult;
import javax.persistence.ColumnResult;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.SysUserInfoModel;
import lombok.Getter;
import lombok.Setter;

/**
 * . system users
 */
@Getter
@Setter
@Entity
@Table(name = "sysusers")
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "SysUsers.SysUserInfoModel", classes = {
		@ConstructorResult(targetClass = SysUserInfoModel.class, columns = {
				@ColumnResult(name = "user_id", type = String.class),
				@ColumnResult(name = "user_name", type = String.class),
				@ColumnResult(name = "company_name", type = String.class),
				@ColumnResult(name = "face_set_token", type = String.class),
				@ColumnResult(name = "authority_id", type = String.class),
				@ColumnResult(name = "mail_address", type = String.class),
				@ColumnResult(name = "is_login", type = String.class) }) })
public class SysUsers {

	/**
	 * . 用户ID
	 */
	@Id
	@Column(name = "user_id")
	private String userId;

	/**
	 * . 用户名
	 */
	@Column(name = "user_name")
	private String userName;

	/**
	 * . 公司名
	 */
	@Column(name = "company_name")
	private String companyName;

	/**
	 * . 脸部face set token
	 */
	@Column(name = "face_set_token")
	private String faceSetToken;

	/**
	 * . 权限ID
	 */
	@Column(name = "authority_id")
	private String authorityId;

	/**
	 * . 邮箱地址
	 */
	@Column(name = "mail_address")
	private String mailAddress;

	/**
	 * . 登录flg
	 */
	@Column(name = "is_login")
	private int isLogin;

	/**
	 * . 删除flg
	 */
	@Column(name = "is_deleted")
	private int isDeleted;

	/**
	 * . 登录用户
	 */
	@CreatedBy
	@Column(name = "created_by", updatable = false)
	private String createdBy;

	/**
	 * . 登录日
	 */
	@CreatedDate
	@Column(name = "created_datetime", updatable = false)
	private Date createdDatetime;

	/**
	 * . 更新用户
	 */
	@LastModifiedBy
	@Column(name = "updated_by")
	private String updatedBy;

	/**
	 * . 更新日
	 */
	@LastModifiedDate
	@Column(name = "updated_datetime")
	private Date updatedDatetime;

	@PrePersist
	public void preInsert() {
		this.isDeleted = Const.COUNT_0;
	}
}
