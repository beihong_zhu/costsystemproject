package jp.co.dk.ip.domain.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import jp.co.dk.ip.domain.model.data.Face;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * 人脸识别 detect
 */
@Getter
@Setter
@AllArgsConstructor
public class FaceDetectModel {

    // request id
	@SerializedName("request_id")
    private String requestId;

    // time used
	@SerializedName("time_used")
    private Integer timeUsed;

    // faces
	@SerializedName("faces")
    private List<Face> faces;

    // image id
	@SerializedName("image_id")
    private String imageId;
	
    // face num
	@SerializedName("face_num")
    private Integer faceNum;
}
