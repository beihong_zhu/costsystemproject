package jp.co.dk.ip.domain.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**.
 * 登録状況情報
 */
@Getter
@Setter
public class ResInfoModel implements Serializable {

    // エラーコード
    private String errCode;

    // エラーコードパラメータ
    private String[] errContent;

    // 全データ出力情報
    private List<List<String>>[] exportListInfo;
}