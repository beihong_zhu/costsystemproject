package jp.co.dk.ip.domain.service.common;

import java.util.Objects;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.entity.Orders;
import jp.co.dk.ip.domain.model.OrderPostAgentModel;
import jp.co.dk.ip.domain.model.data.OrderPostReq;
import jp.co.dk.ip.domain.repository.OrdersRepository;

@Service
public class OrderPostService {
	
	/**
	 * get agent post sign
	 * 
	 * @return agent response
	 */
	public String getAgentOrderInfoPostSign(
			String corpId,
			String reqId,
			String orderId,
			String price,
			String status
			) {
		
		// MD5 sign setting
		String postSign = Const.EMPTY_STRING;
		String postMd5Sign = Const.EMPTY_STRING;
		postSign = 
    			Const.AGENT_ORDER_INFO_CALL_BACK_API.CORPID + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + corpId
    			+ Const.AGENT_ORDER_INFO_CALL_BACK_API.AND + Const.AGENT_ORDER_INFO_CALL_BACK_API.REQID + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + reqId
    			+ Const.AGENT_ORDER_INFO_CALL_BACK_API.AND + Const.AGENT_ORDER_INFO_CALL_BACK_API.ORDERID  + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + orderId
    			+ Const.AGENT_ORDER_INFO_CALL_BACK_API.AND + Const.AGENT_ORDER_INFO_CALL_BACK_API.PRICE + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + price
    			+ Const.AGENT_ORDER_INFO_CALL_BACK_API.AND + Const.AGENT_ORDER_INFO_CALL_BACK_API.STATUS + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + status;
		postMd5Sign = Utils.stringToMD5(postSign);
		return postMd5Sign;
	}
	
	/**
	 * get agent post sign
	 * 
	 * @return agent response
	 */
	public String getAgentOrderResultPostSign(
			String corpId,
			String reqId,
			String orderId,
			String result
			) {
		
		// MD5 sign setting
		String postSign = Const.EMPTY_STRING;
		String postMd5Sign = Const.EMPTY_STRING;
		postSign = 
    			Const.AGENT_ORDER_RESULT_CALL_BACK_API.CORPID + Const.AGENT_ORDER_RESULT_CALL_BACK_API.EQUAL + corpId
    			+ Const.AGENT_ORDER_RESULT_CALL_BACK_API.AND + Const.AGENT_ORDER_RESULT_CALL_BACK_API.REQID + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + reqId
    			+ Const.AGENT_ORDER_RESULT_CALL_BACK_API.AND + Const.AGENT_ORDER_RESULT_CALL_BACK_API.ORDERID  + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + orderId
    			+ Const.AGENT_ORDER_RESULT_CALL_BACK_API.AND + Const.AGENT_ORDER_RESULT_CALL_BACK_API.RESULT + Const.AGENT_ORDER_INFO_CALL_BACK_API.EQUAL + result;
		postMd5Sign = Utils.stringToMD5(postSign);
		return postMd5Sign;
	}
	
	/**
	 * get authory 001 post sign
	 * 
	 * @return agent response
	 */
	public static String getAuthory001PostSign(
			String amount,
			String callbackurl,
			String merId,
			String orderId,
			String rechargeNo,
			String ts,
			String type
			) {
		
		// MD5 validation
    	String originalSign = Const.EMPTY_STRING;
    	String md5Sign = Const.EMPTY_STRING;
    	originalSign = 
    			Const.AUTHORY_001_CHARGE_API.AMOUNT + Const.AUTHORY_001_CHARGE_API.EQUAL + amount
    			+ Const.AUTHORY_001_CHARGE_API.AND + Const.AUTHORY_001_CHARGE_API.CALLBACKURL + Const.AUTHORY_001_CHARGE_API.EQUAL + callbackurl
    			+ Const.AUTHORY_001_CHARGE_API.AND + Const.AUTHORY_001_CHARGE_API.MERID + Const.AUTHORY_001_CHARGE_API.EQUAL + merId
    			+ Const.AUTHORY_001_CHARGE_API.AND + Const.AUTHORY_001_CHARGE_API.ORDERID + Const.AUTHORY_001_CHARGE_API.EQUAL + orderId
    			+ Const.AUTHORY_001_CHARGE_API.AND + Const.AUTHORY_001_CHARGE_API.RECHARGENO + Const.AUTHORY_001_CHARGE_API.EQUAL + rechargeNo
    			+ Const.AUTHORY_001_CHARGE_API.AND + Const.AUTHORY_001_CHARGE_API.TS + Const.AUTHORY_001_CHARGE_API.EQUAL + ts
    			+ Const.AUTHORY_001_CHARGE_API.AND + Const.AUTHORY_001_CHARGE_API.TYPE + Const.AUTHORY_001_CHARGE_API.EQUAL + type;
		md5Sign = Utils.stringToMD5(originalSign);
		return md5Sign;
	}
	
	/**
	 * get authory 002 post sign
	 * 
	 * @return agent response
	 */
	public static String getAuthory002PostSign(
			String callbackUrl,
			String companyId,
			String orderId,
			String phone,
			String productId,
			String type
			) {
		
		// MD5 validation
    	String originalSign = Const.EMPTY_STRING;
    	String md5Sign = Const.EMPTY_STRING;
    	originalSign = 
    			Const.AUTHORY_002_CHARGE_API.CALLBACKURL + Const.AUTHORY_002_CHARGE_API.EQUAL + callbackUrl
    			+ Const.AUTHORY_002_CHARGE_API.AND + Const.AUTHORY_002_CHARGE_API.COMPANYID + Const.AUTHORY_002_CHARGE_API.EQUAL + companyId
    			+ Const.AUTHORY_002_CHARGE_API.AND + Const.AUTHORY_002_CHARGE_API.ORDERID + Const.AUTHORY_002_CHARGE_API.EQUAL + orderId
    			+ Const.AUTHORY_002_CHARGE_API.AND + Const.AUTHORY_002_CHARGE_API.PHONE + Const.AUTHORY_002_CHARGE_API.EQUAL + phone
    			+ Const.AUTHORY_002_CHARGE_API.AND + Const.AUTHORY_002_CHARGE_API.PRODUCTID + Const.AUTHORY_002_CHARGE_API.EQUAL + productId;
    	if (Objects.equals(type, null) && Objects.equals(type, Const.EMPTY_STRING)) {
    		originalSign = originalSign + Const.AUTHORY_002_CHARGE_API.AND + Const.AUTHORY_002_CHARGE_API.TYPE + Const.AUTHORY_002_CHARGE_API.EQUAL + type;
    	}		
		md5Sign = Utils.stringToMD5(originalSign);
		return md5Sign;
	}
}