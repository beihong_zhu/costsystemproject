package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.AgentCapitalInfoModel;
import jp.co.dk.ip.domain.model.AgentFundRecorderModel;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import jp.co.dk.ip.domain.model.OnShelfProductModel;

/**
 * . agents custom repository
 */
public class AgentsCustomRepositoryImpl implements AgentsCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<AgentCapitalInfoModel> findAgentCapitalByAgentInfo(String agentId, String agentName, String agentReduceName) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " a.agent_id, "
                + " a.agent_reduce_name, "
                + " a.province, "
                + " a.city, "
                + " a.business_mode, "
                + " ct.capital, "
                + " a.interface_status, "
                + " a.credit_limit, "
                + " a.created_datetime "
                + "FROM "
                + " agents a "
                + "WHERE "
                + " a.is_deleted = :agentReduceName ";
		if (!Objects.equals(agentId, Const.EMPTY_STRING) && !Objects.equals(agentId, null)) {
			sql += "         AND a.agent_id = :agentId ";
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}

		// sql injection preventing
		agentId = Utils.escapeSql(agentId);
		agentName = Utils.escapeSql(agentName);
		agentReduceName = Utils.escapeSql(agentReduceName);
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.AgentCapitalInfoModel");
		if (!Objects.equals(agentId, Const.EMPTY_STRING) && !Objects.equals(agentId, null)) {
			query.setParameter("agentId", agentId);
		} 
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			query.setParameter("agentName", agentName);
		}
		if (!Objects.equals(agentReduceName, Const.EMPTY_STRING) && !Objects.equals(agentReduceName, null)) {
			query.setParameter("agentReduceName", agentReduceName);
		}
		List<AgentCapitalInfoModel> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AgentInfoModel> findInfoAgentInfoByAgentInfo(String accountagency, String agencyname, String  agencyabbreviation , String agency ) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + "a.account_name,"
                + "a.account_reduce_name,"
                + "a.province,"
                + "a.agent_type,"
                + "a.business_mode,"
                + "a.acount_status,"
                + "o.capital,"
                + "o.facility,"
                + "FROM "
                + " agents a "               
                + "LEFT JOIN capital_total o "
                + "ON a.agent_id = o.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " a.acount_status = :agency";
		if (!Objects.equals(accountagency, Const.EMPTY_STRING) && !Objects.equals(accountagency, null)) {
			sql += "         AND a.agent_id = :accountagency ";
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}
		

		// sql injection preventing
		accountagency = Utils.escapeSql(accountagency);
		agencyname = Utils.escapeSql(agencyname);
		agencyabbreviation = Utils.escapeSql(agencyabbreviation);
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.AgentInfoModel");
		if (!Objects.equals(accountagency, Const.EMPTY_STRING) && !Objects.equals(accountagency, null)) {
			query.setParameter("agentId", accountagency);
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			query.setParameter("agentName", agencyname);
		}
		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
			query.setParameter("agentReduceName", agencyabbreviation);
		}
		List<AgentInfoModel> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AgentInfoModel> findInfoAgentCapitalByAgentInfo(
			String agentAccount,
			String agentName,
			String agentNumber,
			String transactionalNumber,
		    String requestNumber,
		    String transactionalMethod,
		    String transactionalType,
		    String timeScope
			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " * "
                + "FROM "
                + " agents a "
                + "WHERE "
                + " a.business_mode = '1' ";
//		if (!Objects.equals(accountagency, Const.EMPTY_STRING) && !Objects.equals(accountagency, null)) {
//			sql += "         AND a.agent_id = :accountagency ";
//		}
//		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
//			sql += "         AND a.agent_name = :agentName ";
//		}
//		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
//			sql += "         AND a.agent_reduce_name = :agentReduceName ";
//		}
//
//		// sql injection preventing
//		accountagency = Utils.escapeSql(accountagency);
//		agencyname = Utils.escapeSql(agencyname);
//		agencyabbreviation = Utils.escapeSql(agencyabbreviation);
//		
//		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.AgentInfoModel");
//		if (!Objects.equals(accountagency, Const.EMPTY_STRING) && !Objects.equals(accountagency, null)) {
//			query.setParameter("agentId", accountagency);
//		}
//		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
//			query.setParameter("agentName", agencyname);
//		}
//		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
//			query.setParameter("agentReduceName", agencyabbreviation);
//		}
		List<AgentInfoModel> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AgentFundRecorderModel> findAgentFundRecorderByAgentInfo(String agencyLimited, String agentName, String localLimited,
			String agencyacount,String bussinessMode,String stranctionMode,String channelLimited,String agencyabbreviate,String ocrManufacturingDate) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " a.agent_id, "
                + " a.agent_reduce_name, "
                + " a.province, "
                + " a.city, "
                + " a.business_mode, "
                + " ct.capital, "
                + " a.interface_status, "
                + " a.credit_limit, "
                + " a.created_datetime "
                + "FROM "
                + " agents a "
                + "WHERE "
                + " a.is_deleted = :agentReduceName ";
		if (!Objects.equals(agencyLimited, Const.EMPTY_STRING) && !Objects.equals(agencyLimited, null)) {
			sql += "         AND a.agent_id = :agencyLimited ";
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(localLimited, Const.EMPTY_STRING) && !Objects.equals(localLimited, null)) {
			sql += "         AND a.agent_reduce_name = :localLimited ";
		}
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			sql += "         AND a.agent_reduce_name = :agencyacount ";
		}
		
		if (!Objects.equals(bussinessMode, Const.EMPTY_STRING) && !Objects.equals(bussinessMode, null)) {
			sql += "         AND a.agent_reduce_name = :bussinessMode ";
		}
		
		if (!Objects.equals(stranctionMode, Const.EMPTY_STRING) && !Objects.equals(stranctionMode, null)) {
			sql += "         AND a.agent_reduce_name = :stranctionMode ";
		}
		
		if (!Objects.equals(channelLimited, Const.EMPTY_STRING) && !Objects.equals(channelLimited, null)) {
			sql += "         AND a.agent_reduce_name = :channelLimited ";
		}
		
		if (!Objects.equals(agencyabbreviate, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviate, null)) {
			sql += "         AND a.agent_reduce_name = :agencyabbreviate ";
		}
		
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			sql += "         AND a.agent_reduce_name = :ocrManufacturingDate ";
		}
		
				
		// sql injection preventing
		agencyLimited = Utils.escapeSql(agencyLimited);
		agentName = Utils.escapeSql(agentName);
		agencyacount = Utils.escapeSql(agencyacount);
		bussinessMode = Utils.escapeSql(bussinessMode);
		stranctionMode = Utils.escapeSql(stranctionMode);
		channelLimited = Utils.escapeSql(channelLimited);
		agencyabbreviate = Utils.escapeSql(agencyabbreviate);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);
		
		
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.AgentFundRecorderModel");
		if (!Objects.equals(agencyLimited, Const.EMPTY_STRING) && !Objects.equals(agencyLimited, null)) {
			query.setParameter("agencyLimited", agencyLimited);
		}
		if (!Objects.equals(agentName, Const.EMPTY_STRING) && !Objects.equals(agentName, null)) {
			query.setParameter("agentName", agentName);
		}
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			query.setParameter("agencyacount", agencyacount);
		}
		
		if (!Objects.equals(bussinessMode, Const.EMPTY_STRING) && !Objects.equals(bussinessMode, null)) {
			query.setParameter("bussinessMode", bussinessMode);
		}
		
		if (!Objects.equals(stranctionMode, Const.EMPTY_STRING) && !Objects.equals(stranctionMode, null)) {
			query.setParameter("stranctionMode", stranctionMode);
		}
		
		if (!Objects.equals(channelLimited, Const.EMPTY_STRING) && !Objects.equals(channelLimited, null)) {
			query.setParameter("channelLimited", channelLimited);
		}
		
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			query.setParameter("agencyacount", agencyacount);
		}
		
		if (!Objects.equals(agencyabbreviate, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviate, null)) {
			query.setParameter("agencyabbreviate", agencyabbreviate);
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			query.setParameter("ocrManufacturingDate", ocrManufacturingDate);
		}
		

		List<AgentFundRecorderModel> list = query.getResultList();
		return list;
	}
	

	@Override
	public List<OnShelfProductModel> findStockByOnshelfInfo(
			String agency,
			String agencyname,
			String agencyID,
			String producttype,
			String productstatus,
			String provideservice,
			String numbertype,
			String productname,
			String value) 
	{
		
		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
        		+ " o.agency, "
                + " o.agent_id, "
                + " o.authory_id, "
                + " cs.product_name, "
                + " cs.product_type, "
                + " cs.value, "
                + " cs.price, "
                + " cs.disaccount, "
                + " cs.status, "
                + " cs.check_price, "
                + " cs.check_cost"
                + "FROM "
                + " agent_product_price cs "
                + "LEFT JOIN agents o "
                + "ON cs.agent_id = o.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (null != agency) {
			sql += "         AND o.agency = :agency ";
		}
		if (null != agencyname) {
			sql += "         AND o.agent_name = :agencyname ";
		}
		if (null != agencyID) {
			sql += "         AND o.agent_id = :agencyID ";
		}
		if (null != producttype) {
			sql += "         AND cs.product_type = :producttype ";
		}
		if (null != productstatus) {
			sql += "         AND cs.status = :productstatus ";
		}
		if (null != provideservice) {
			sql += "         AND cs.provide_service = :provideservice ";
		}
		if (null != numbertype) {
			sql += "         AND cs.number_type = :numbertype ";
		}
		if (null != productname) {
			sql += "         AND cs.product_name = :productname ";
		}
		if (null != value) {
			sql += "         AND cs.value = :value ";
		}
		// sql injection preventing
		agency = Utils.escapeSql(agency);
		agencyname = Utils.escapeSql(agencyname);
		agencyID = Utils.escapeSql(agencyID);
		producttype = Utils.escapeSql(producttype);
		productstatus = Utils.escapeSql(productstatus);
		provideservice = Utils.escapeSql(provideservice);
		numbertype = Utils.escapeSql(numbertype);
		productname = Utils.escapeSql(productname);
		value = Utils.escapeSql(value);

		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Agents.OnShelfProductModel");
		if (null != agency) {
			query.setParameter("agency", agency);
		}
		if (null != agencyname) {
			query.setParameter("agencyname", agencyname);
		}
		if (null != agencyID) {
			query.setParameter("agencyID", agencyID);
		}
		if (null != producttype) {
			query.setParameter("producttype", producttype);
		}
		if (null != productstatus) {
			query.setParameter("productstatus", productstatus);
		}
		if (null != provideservice) {
			query.setParameter("provideservice", provideservice);
		}
		if (null != numbertype) {
			query.setParameter("numbertype", numbertype);
		}
		if (null != productname) {
			query.setParameter("productname", productname);
		}
		if (null != value) {
			query.setParameter("value", value);
		}
		List<OnShelfProductModel> list = query.getResultList();
		return list;
	}
}
