package jp.co.dk.ip.domain.model;

import java.io.Serializable;
import java.util.List;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;

import lombok.Getter;
import lombok.Setter;

/**.
 * 登録状況情報
 */
@Getter
@Setter
public class HttpModel implements Serializable {

    // http post
    private HttpPost httpPost;

    // エラーコードパラメータ
    private CloseableHttpAsyncClient client;
}