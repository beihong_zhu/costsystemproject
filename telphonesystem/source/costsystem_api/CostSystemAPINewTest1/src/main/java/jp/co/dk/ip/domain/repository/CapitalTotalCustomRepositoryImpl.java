package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.util.StringUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.CapitalTotalInfoModel;
import jp.co.dk.ip.domain.model.CapitalReducePlusModel;


/**
 * . capital repository
 */
public class CapitalTotalCustomRepositoryImpl implements CapitalTotalCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<CapitalTotalInfoModel> findCapitalByCapitalInfo(
			String agencyabbreviation,
			String value,
			String provideSevice,
			String ocrManufacturingDate
			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
			sql += "         AND o.corp_id = :agencyabbreviation ";
		}
		if (!Objects.equals(value, Const.EMPTY_STRING) && !Objects.equals(value, null)) {
			sql += "         AND a.agent_name = :value ";
		}
		if (!Objects.equals(provideSevice, Const.EMPTY_STRING) && !Objects.equals(provideSevice, null)) {
			sql += "         AND a.agent_reduce_name = :provideSevice ";
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			sql += "         AND o.req_id = :ocrManufacturingDate ";
		}
	

		// sql injection preventing
		agencyabbreviation = Utils.escapeSql(agencyabbreviation);
		value = Utils.escapeSql(value);
		provideSevice = Utils.escapeSql(provideSevice);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);


		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "CapitalStatus.CapitalInfoModel");
		if (!Objects.equals(agencyabbreviation, Const.EMPTY_STRING) && !Objects.equals(agencyabbreviation, null)) {
			query.setParameter("agencyabbreviation", agencyabbreviation);
		}
		if (!Objects.equals(value, Const.EMPTY_STRING) && !Objects.equals(value, null)) {
			query.setParameter("value", value);
		}
		if (!Objects.equals(provideSevice, Const.EMPTY_STRING) && !Objects.equals(provideSevice, null)) {
			query.setParameter("provideSevice", provideSevice);
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			query.setParameter("ocrManufacturingDate", ocrManufacturingDate);
		}
	

		List<CapitalTotalInfoModel> list = query.getResultList();
		return list;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CapitalReducePlusModel> findCapitalPlusReduceByCapitalInfo(
			String agencyacount,
			String agencyname,
			String applymode,
			String status,
			String ocrManufacturingDate
			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			sql += "         AND o.corp_id = :agencyacount ";
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			sql += "         AND a.agent_name = :agencyname ";
		}
		if (!Objects.equals(applymode, Const.EMPTY_STRING) && !Objects.equals(applymode, null)) {
			sql += "         AND a.agent_reduce_name = :applymode ";
		}
		
		if (!Objects.equals(status, Const.EMPTY_STRING) && !Objects.equals(status, null)) {
			sql += "         AND a.agent_reduce_name = :status ";
		}
		
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			sql += "         AND o.req_id = :ocrManufacturingDate ";
		}
	

		// sql injection preventing
		agencyacount = Utils.escapeSql(agencyacount);
		agencyname = Utils.escapeSql(agencyname);
		applymode = Utils.escapeSql(applymode);
		status = Utils.escapeSql(status);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);


		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "CapitalStatus.CapitalInfoModel");
		if (!Objects.equals(agencyacount, Const.EMPTY_STRING) && !Objects.equals(agencyacount, null)) {
			query.setParameter("agencyacount", agencyacount);
		}
		if (!Objects.equals(agencyname, Const.EMPTY_STRING) && !Objects.equals(agencyname, null)) {
			query.setParameter("agencyname", agencyname);
		}
		if (!Objects.equals(applymode, Const.EMPTY_STRING) && !Objects.equals(applymode, null)) {
			query.setParameter("applymode", applymode);
		}
		if (!Objects.equals(status, Const.EMPTY_STRING) && !Objects.equals(status, null)) {
			query.setParameter("status", status);
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			query.setParameter("ocrManufacturingDate", ocrManufacturingDate);
		}
	
		List<CapitalReducePlusModel> list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CapitalReducePlusModel> findCapitalZeroByCapitalInfo(
			String zero,
			String ocrManufacturingDate
			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(zero, Const.EMPTY_STRING) && !Objects.equals(zero, null)) {
			sql += "         AND o.corp_id = :zero ";
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			sql += "         AND a.agent_name = :ocrManufacturingDate ";
		}
	
	

		// sql injection preventing
		zero = Utils.escapeSql(zero);
		ocrManufacturingDate = Utils.escapeSql(ocrManufacturingDate);
		

		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "CapitalStatus.CapitalInfoModel");
		if (!Objects.equals(zero, Const.EMPTY_STRING) && !Objects.equals(zero, null)) {
			query.setParameter("zero", zero);
		}
		if (!Objects.equals(ocrManufacturingDate, Const.EMPTY_STRING) && !Objects.equals(ocrManufacturingDate, null)) {
			query.setParameter("ocrManufacturingDate", ocrManufacturingDate);
		}

	
		List<CapitalReducePlusModel> list = query.getResultList();
		return list;
	}
}
