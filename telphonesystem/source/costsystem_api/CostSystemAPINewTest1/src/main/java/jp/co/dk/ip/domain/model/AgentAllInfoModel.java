package jp.co.dk.ip.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
@AllArgsConstructor
public class AgentAllInfoModel {

	/**
	 * . 账户名
	 */
	private String agentName;

	/**
	 * . 简称
	 */
	private String agentReduceName;

	/**
	 * . 省
	 */
	private String province;

	/**
	 * . 市
	 */
	
	private String type;

	/**
	 * . 类型
	 */
	
	private String city;

	/**
	 * . 模式
	 */
	private String businessMode;

	/**
	 * . 可用余额
	 */
	private String capital;

	/**
	 * . 可用与否
	 */
	private String interfaceStatus;

	/**
	 * . 授信额度
	 */
	private String creditLimit;

	/**
	 * . 创建时间
	 */
	private String createdDatetime;
	
	/**
	 * . 状态
	 */
	private String status;
	
	
}
