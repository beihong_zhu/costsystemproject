package jp.co.dk.ip.domain.service;

import java.util.Locale;
import java.util.Objects;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.PessimisticLockingFailureException;
import org.springframework.stereotype.Service;

import jp.co.dk.ip.application.request.APIAgentOrderInfoCallBackFakeReq;
import jp.co.dk.ip.application.response.APIAgentOrderInfoCallBackFakeRes;
import jp.co.dk.ip.application.response.data.ErrorRes;
import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.CustomHandleException;
import jp.co.dk.ip.common.SettingContext;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.service.common.OrderPostService;

/**
 * agent data post  call back Service
 */
@Service
@Transactional
public class APIAgentOrderInfoCallBackFakeService {

    private static final Logger logger = LoggerFactory.getLogger(APIAgentOrderInfoCallBackFakeService.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SettingContext settingContext;
    
    @Autowired
    private OrderPostService orderPostService;
    
    /**
     * agent data post process
     *
     * @param pictureMultipartFile face picture
     * @return clients result
     */
	public APIAgentOrderInfoCallBackFakeRes agentDataProcess(APIAgentOrderInfoCallBackFakeReq apiAgentCallBackFakeReq) throws Exception {

        logger.debug("APIAgentCallBackFakeService agentDataProcess");

        try {
        	APIAgentOrderInfoCallBackFakeRes apiAgentCallBackFakeRes = new APIAgentOrderInfoCallBackFakeRes();
        	
			// MD5 sign validation
			String postMd5Sign = Const.EMPTY_STRING;
			postMd5Sign = orderPostService.getAgentOrderInfoPostSign(
					apiAgentCallBackFakeReq.getCorpId(),
					apiAgentCallBackFakeReq.getReqId(),
					apiAgentCallBackFakeReq.getOrderId(),
					apiAgentCallBackFakeReq.getPrice(),
					apiAgentCallBackFakeReq.getStatus());
			if (!Objects.equals(postMd5Sign, apiAgentCallBackFakeReq.getSign())) {
				// response error
	        	apiAgentCallBackFakeRes.setResult(Const.AGENT_CHARGE_API.FAILURE);
				return apiAgentCallBackFakeRes;
			}
        	
        	apiAgentCallBackFakeRes.setResult(Const.AGENT_CHARGE_API.SUCCESS);
			return apiAgentCallBackFakeRes;
        } catch (PessimisticLockingFailureException plfEx) {
            // DB排他
            logger.error(plfEx.getMessage(), plfEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000048,
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAUTHORY + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_3, Const.RESULT_CNT_0, Const.E000048, message);
            throw new CustomHandleException(errorRes);
        } catch (DataAccessException daEx) {
            logger.error(daEx.getMessage(), daEx);
            ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
            message = messageSource.getMessage(
            		Const.E000015, 
            		new String[] {},
                    Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAUTHORY + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000015, message);
            throw new CustomHandleException(errorRes);
		} catch (RuntimeException runEx) {
            logger.error(runEx.getMessage(), runEx);
			ErrorRes errorRes = new ErrorRes();
            String message = Const.EMPTY_STRING;
			message = messageSource.getMessage(
					Const.E000005, 
					new String[] {},
					Locale.forLanguageTag(settingContext.getLangZh()));
            logger.error(Const.ERRRES_APIAUTHORY + Const.ERROR_MSG + message);
            errorRes = Utils.customHandleException(Const.RESULT_CODE_9, Const.RESULT_CNT_0, Const.E000005, message);
            throw new CustomHandleException(errorRes);	
		}
    }
    
    /**.
     * 错误信息取得
     *
     * @param param 错误参数
     * @param resultCode 返回code
     * @param errorCode 错误code
     * @param logError 错误log说明
     * @return response class
     */
    public APIAgentOrderInfoCallBackFakeRes getErrRes(String[] param, String resultCode, String errorCode, String logError) {

        logger.debug("APIAgentFakeService getErrRes");

        APIAgentOrderInfoCallBackFakeRes apiAgentCallBackFakeRes = new APIAgentOrderInfoCallBackFakeRes();
        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        logger.error(logError + Const.ERROR_CODE + errorCode);
        logger.error(logError + Const.ERROR_MSG + errMsg);
        apiAgentCallBackFakeRes.setResultCode(resultCode);
        apiAgentCallBackFakeRes.setMessageCode(errorCode);
        apiAgentCallBackFakeRes.setMessage(errMsg);
        return apiAgentCallBackFakeRes;
    }
    
    /**.
     * 单项目错误信息取得
     *
     * @param param 错误参数
     * @param errorCode 错误code
     * @param fieldName 字段中文名
     * @return APIClientRes response class
     */
    public APIAgentOrderInfoCallBackFakeRes getReqErr(String[] param, String errorCode, String fieldName) {

        logger.debug("APIAgentFakeService getReqErr");

        String errMsg = messageSource.getMessage(errorCode, param, Locale.forLanguageTag(settingContext.getLangZh()));
        String msg = Const.BRACKET_LEFT + fieldName + Const.BRACKET_RIGHT + errMsg;
        logger.error(Const.FIELD_ERROR + Const.ERROR_CODE + errorCode);
        logger.error(Const.FIELD_ERROR + Const.ERROR_MSG + msg);
        return getErrRes(new String[] {}, Const.RESULT_CODE_9, Const.EIF001, Const.FIELD_ERROR);
    }

    /**.
     * 错误信息设定
     *
     * @param e 自定义错误获取类
     * @return response class
     */
    public APIAgentOrderInfoCallBackFakeRes setErrRes(CustomHandleException e) {

        logger.debug("APIAgentFakeService setErrRes");

        APIAgentOrderInfoCallBackFakeRes apiAgentCallBackFakeRes = new APIAgentOrderInfoCallBackFakeRes();
        apiAgentCallBackFakeRes.setResultCode(e.getErrorMessage().getResultCode());
        apiAgentCallBackFakeRes.setResultCnt(e.getErrorMessage().getResultCnt());
        apiAgentCallBackFakeRes.setMessageCode(e.getErrorMessage().getMessageCode());
        apiAgentCallBackFakeRes.setMessage(e.getErrorMessage().getMessage());
        return apiAgentCallBackFakeRes;
    }
}
