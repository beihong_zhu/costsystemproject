package jp.co.dk.ip.domain.model.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * order result post
 */
@Getter
@Setter
public class OrderResultPostReq {

    // 账号
    private String corpid;
    
    // 代理商流水号
    private String reqid;
    
    // 平台交易单号
    private String orderid;
    
    // 时间戳
    private String ts;
    
    // 实付金额
    private String price;
    
    // 运营商：1-移动，2-联通，3-电信，4-中石化
    private String spId;
    
    // 省份编码（详见附录）
    private String refid;
    
    // 签名
    private String sign;
}