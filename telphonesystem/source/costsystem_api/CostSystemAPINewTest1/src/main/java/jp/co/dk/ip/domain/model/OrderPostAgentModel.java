package jp.co.dk.ip.domain.model;

import jp.co.dk.ip.domain.model.data.OrderPostReq;
import lombok.Getter;
import lombok.Setter;

/**.
 * order post request Info
 */
@Getter
@Setter
public class OrderPostAgentModel {

    // order post request
    private OrderPostReq orderPostReq;
    
    // order post url
    private String url;
}