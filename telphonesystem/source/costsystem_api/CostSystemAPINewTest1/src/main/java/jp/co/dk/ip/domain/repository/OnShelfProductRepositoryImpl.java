package jp.co.dk.ip.domain.repository;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.util.StringUtils;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.common.Utils;
import jp.co.dk.ip.domain.model.OnShelfProductModel;
import jp.co.dk.ip.domain.model.CapitalInfoModel;

/**
 * . orders repository
 */
public class OnShelfProductRepositoryImpl implements OnShelfProductRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<OnShelfProductModel> findOrderSummaryByOrderInfo(
			String bussiness,
			String bussitype,
			String privince,
			String agency,
			String producttype,
			String productstatus,
			String numbertype,
			String provideservice,
			String numbermode,
			String agencyname,
			String agencyID,
			String productname,
			String value
		
			) {

		String sql = Const.EMPTY_STRING;
        sql = "SELECT "
                + " o.req_id, "
                + " o.order_id, "
                + " a.agent_reduce_name, "
                + " o.order_form, "
                + " CONCAT(o.sp_id, '_', o.money) as product_name, "
                + " cs.capital, "
                + " o.price, "
                + " o.status, "
                + " o.created_datetime "
                + "FROM "
                + " capital_status cs "
                + "LEFT JOIN orders o "
                + "ON cs.order_id = o.order_id "
                + "AND o.is_deleted = '0' "
                + "LEFT JOIN agents a "
                + "ON cs.agent_id = a.agent_id "
                + "AND a.is_deleted = '0' "
                + "LEFT JOIN capital_total ct "
                + "ON cs.agent_id = ct.agent_id "
                + "AND o.is_deleted = '0' "
                + "WHERE "
                + " cs.is_deleted = '0' ";
		if (!Objects.equals(bussiness, Const.EMPTY_STRING) && !Objects.equals(bussiness, null)) {
			sql += "         AND a.agent_name = :agentName ";
		}
		if (!Objects.equals(bussitype, Const.EMPTY_STRING) && !Objects.equals(bussitype, null)) {
			sql += "         AND a.agent_reduce_name = :agentReduceName ";
		}
		if (!Objects.equals(privince, Const.EMPTY_STRING) && !Objects.equals(privince, null)) {
			sql += "         AND o.money = :money ";
		}
		if ((!Objects.equals(agency, Const.EMPTY_STRING) && !Objects.equals(agency, null)) &&
				(!Objects.equals(producttype, Const.EMPTY_STRING) && !Objects.equals(producttype, null))) {
			sql += "         AND o.created_datetime between :startTime and :endTime";
		}

		// sql injection preventing
		bussiness = Utils.escapeSql(bussiness);
		bussitype = Utils.escapeSql(bussitype);
		privince = Utils.escapeSql(privince);
		producttype = Utils.escapeSql(producttype);
		productstatus = Utils.escapeSql(productstatus);

		// time setting
		String strStartTime = Const.EMPTY_STRING;
		String strEndTime = Const.EMPTY_STRING;
        if (!StringUtils.isEmpty(strStartTime)) {
        	strStartTime = Utils.setDateString(strStartTime);
        }
        if (!StringUtils.isEmpty(strEndTime)) {
        	strEndTime = Utils.setDateString(strEndTime);
        }
		
		// sql param setting
		Query query = entityManager.createNativeQuery(sql, "Orders.AgentSummaryModel");
		if (!Objects.equals(bussiness, Const.EMPTY_STRING) && !Objects.equals(bussiness, null)) {
			query.setParameter("bussiness", bussiness);
		}
		if (!Objects.equals(bussitype, Const.EMPTY_STRING) && !Objects.equals(bussitype, null)) {
			query.setParameter("bussitype", bussitype);
		}
		if (!Objects.equals(privince, Const.EMPTY_STRING) && !Objects.equals(privince, null)) {
			query.setParameter("privince", privince);
		}
		if ((!Objects.equals(strStartTime, Const.EMPTY_STRING) && !Objects.equals(strStartTime, null)) &&
				(!Objects.equals(strEndTime, Const.EMPTY_STRING) && !Objects.equals(strEndTime, null))) {
			query.setParameter("strStartTime", strStartTime);
			query.setParameter("strEndTime", strEndTime);
		}
		List<OnShelfProductModel> list = query.getResultList();
		return list;
	}
}
