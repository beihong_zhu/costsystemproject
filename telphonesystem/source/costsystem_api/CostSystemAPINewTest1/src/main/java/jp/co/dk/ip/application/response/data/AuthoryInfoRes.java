package jp.co.dk.ip.application.response.data;

import lombok.Getter;
import lombok.Setter;

/**.
 * CSV Info
 */
@Getter
@Setter
public class AuthoryInfoRes {


	/**
	 * . 渠道商名
	 */
	private String authoryName;

	/**
	 * . 简称
	 */
	private String abbreviation;
}
