package jp.co.dk.ip.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jp.co.dk.ip.common.Const;
import jp.co.dk.ip.domain.model.AgentCapitalInfoModel;
import jp.co.dk.ip.domain.model.AgentInfoModel;
import lombok.Getter;
import lombok.Setter;

/**.
 * agents
 */
@Getter
@Setter
@Entity
@Table(name = "agents")
@EntityListeners(AuditingEntityListener.class)
@SqlResultSetMapping(name = "Agents.AgentInfoModel", classes = {
		@ConstructorResult(targetClass = AgentInfoModel.class, columns = {
				@ColumnResult(name = "agent_id", type = String.class),
				@ColumnResult(name = "agent_reduce_name", type = String.class),
				@ColumnResult(name = "province", type = String.class),
				@ColumnResult(name = "city", type = String.class),
				@ColumnResult(name = "business_mode", type = String.class),
				@ColumnResult(name = "capital", type = String.class),
				@ColumnResult(name = "interface_status", type = String.class),
				@ColumnResult(name = "credit_limit", type = String.class),
				@ColumnResult(name = "created_datetime", type = String.class)
				})
		})

public class Agents {
	
    /**.
     * 代理商ID
     */
    @Id
    @Column(name = "agent_id")
    private String agentId;
   

    /**.
     * 代理商
     */
    @Column(name = "agent_name")
    private String AgentName;
    

    /**.
     * 代理简称
     */
    @Column(name = "agent_reduce_name")
    private String AgentReduceName;

    /**.
     * 电话
     */
    @Column(name = "tel")
    private String Tel;
    
    /**.
     * 邮件
     */
    @Column(name = "mail")
    private String Mail;

    /**.
     * 省
     */
    @Column(name = "province")
    private String province;

    /**.
     * 市
     */
    @Column(name = "city")
    private String city;
    
    /**.
     * 地址						
     */
    @Column(name = "address")
    private String Address;
    
    /**.
     * 账户名
     */
    @Column(name = "account_name")
    private String AccountName;
    
    /**.
     * 账户状态
     */
    @Column(name = "acount_status")
    private String AcountStatus;
    
    /**.
     * 代理类型
     */
    @Column(name = "agent_type")
    private String AgentType;
    
    /**.
     * 业务模式
     */
    @Column(name = "business_mode")
    private String BusinessMode;
    
    /**.
     * 接口状态
     */
    @Column(name = "interface_status")
    private String interfaceStatus;
    
    /**.
     * 登录密码
     */
    @Column(name = "log_ps")
    private String logPs;
    
    /**.
     * 支付密码
     */
    @Column(name = "pay_ps")
    private String payPs;
    
    /**.
     * 通知状态
     */
    @Column(name = "info_status")
    private String infoStatus;
    
    /**.
     * 信用额度
     */
    @Column(name = "credit_limit")
    private String creditLimit;    
    
    /**.
     * 代理商回调地址
     */
    @Column(name = "call_back_url")
    private String callBackUrl;
    
    /**.
     * 删除flg
     */
    @Column(name = "is_deleted")
    private int isDeleted;
    
    /**.
     * 登录用户
     */
    @CreatedBy
    @Column(name = "created_by", updatable = false)
    private String createdBy;

    /**.
     * 登录日
     */
    @CreatedDate
    @Column(name = "created_datetime", updatable = false)
    private Date createdDatetime;

    /**.
     * 更新用户
     */
    @LastModifiedBy
    @Column(name = "updated_by")
    private String updatedBy;

    /**.
     * 更新日
     */
    @LastModifiedDate
    @Column(name = "updated_datetime")
    private Date updatedDatetime;

    @PrePersist
    public void preInsert() {
        this.isDeleted = Const.COUNT_0;
    }
}
