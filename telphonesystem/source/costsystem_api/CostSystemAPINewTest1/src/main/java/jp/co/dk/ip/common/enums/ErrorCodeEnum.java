package jp.co.dk.ip.common.enums;

/**
 * 错误码枚举
 */
//@Configuration
public enum  ErrorCodeEnum {

    SYS0001("000001","系统异常"),

    //=========== UMS相关 start ===========
    UMS0001("010001","请输入用户名或密码"),
    UMS0002("010002","用户名不存在"),
    UMS0003("010003","登录失败次数过多，账号暂时冻结"),
    UMS0004("010004","密码错误"),

    UMS0005("010005","旧密码或新密码不能为空"),
    UMS0006("010006","旧密码错误"),
    UMS0007("010007","密码长度至少为6位，最多为16个字符"),
    UMS0008("010008","不符合密码规则"),
    UMS0009("010009","验证码错误");


    //=========== UMS相关 end   ===========

//    //=========== 美团相关 start   ===========

//    //=========== 美团相关 end   ===========
//    @Autowired
//    private ITbSysMsgService iTbSysMsgService;
//
//    @Bean("MapMsg")
//    public Map<String,String> MapMsg(){
//        return  iTbSysMsgService.MsgMap("pro");
//    }



    private String code;
    private String msg;

    public String msg() {
        return msg;
    }

    public String code() {
        return code;
    }

    ErrorCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static ErrorCodeEnum getEnum(String code) {
        for (ErrorCodeEnum ele : ErrorCodeEnum.values()) {
            if (ele.code().equals(code)) {
                return ele;
            }
        }
        return null;
    }
}
