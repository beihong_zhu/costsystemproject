package jp.co.dk.ip.domain.model.data;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**.
 * 人脸识别 detect data 人脸参数
 */
@Getter
@Setter
@AllArgsConstructor
public class FaceRectangle {

    // top
	@SerializedName("top")
    private Integer top;

    // left
	@SerializedName("left")
    private Integer left;

    // width
	@SerializedName("width")
    private Integer width;

    // height
	@SerializedName("height")
    private Integer height;
}
