package jp.co.dk.ip.common;

import java.io.IOException;
import java.nio.CharBuffer;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.IOControl;
import org.apache.http.nio.client.methods.AsyncCharConsumer;
import org.apache.http.nio.client.methods.HttpAsyncMethods;
import org.apache.http.protocol.HttpContext;

public class HttpUtils {

	private static CloseableHttpAsyncClient httpclient;

	static {
		httpclient = HttpAsyncClients.createDefault();;
		httpclient.start();
	}

	public static void invoke(String url) {
		httpclient.execute(HttpAsyncMethods.createGet(url),
		new AsyncCharConsumer<Boolean>() {
			@Override
			protected void onCharReceived(CharBuffer buf,
					IOControl ioctrl) throws IOException {
			}

			@Override			
			protected Boolean buildResult(HttpContext arg0)
					throws Exception {
				return Boolean.TRUE;
			}

			@Override
			protected void onResponseReceived(HttpResponse arg0)
					throws HttpException, IOException {
			}
		}, null);
	}
}