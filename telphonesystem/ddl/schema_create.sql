USE cost_system_db;
SET character_set_server=utf8;
SET SQL_SAFE_UPDATES = 0;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tables
-- ----------------------------

DROP TABLE IF EXISTS sysusers;
CREATE TABLE sysusers (
  user_id varchar(256) NOT NULL,
  user_name varchar(60) NOT NULL,
  company_name varchar(100),
  face_set_token varchar(1000),
  authority_id varchar(10),
  mail_address varchar(300),
  is_login tinyint(1) NOT NULL,
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (user_id),
  UNIQUE KEY idx_users_01 (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS agent_product_price;
CREATE TABLE agent_product_price (
  agent_id varchar(256) NOT NULL,
  product_id varchar(256) NOT NULL,
  value int(8),
  price decimal(8,3),
  sp_id varchar(256) NOT NULL,
  status tinyint(1) NOT NULL,
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (agent_id,product_id),
  UNIQUE KEY idx_users_01 (agent_id,product_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS agents;
CREATE TABLE agents (
  agent_id varchar(256) NOT NULL,
  agent_name varchar(256) NOT NULL,
  agent_reduce_name varchar(10),
  price decimal(8,3),
  tel varchar(11),
  mail varchar(100),
  province varchar(10),
  city varchar(10),
  address varchar(100),
  account_name varchar(10),
  acount_status varchar(1) NOT NULL,
  agent_type varchar(1) NOT NULL,
  business_mode varchar(1) NOT NULL,
  interface_status varchar(1) NOT NULL,
  log_ps varchar(10),
  pay_ps varchar(10),
  info_status varchar(1) NOT NULL,
  credit_limit varchar(1) NOT NULL,
  call_back_url varchar(100),
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (agent_id),
  UNIQUE KEY idx_users_01 (agent_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS authories;
CREATE TABLE authories (
  authory_id varchar(256) NOT NULL,
  authory_name varchar(256) NOT NULL,
  abbreviation varchar(10),
  tel varchar(11),
  mail varchar(100),
  province varchar(10),
  city varchar(10),
  address varchar(100),
  account_name varchar(10),
  acount_status varchar(1) NOT NULL,
  authory_type varchar(1) NOT NULL,
  business_mode varchar(1) NOT NULL,
  interface_status varchar(1) NOT NULL,
  log_ps varchar(10),
  pay_ps varchar(10),
  info_status varchar(1) NOT NULL,
  prority varchar(1) NOT NULL,
  split_flow varchar(1) NOT NULL,
  cost decimal(8,3),
  price decimal(8,3),
  call_back_url varchar(100),
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (authory_id),
  UNIQUE KEY idx_users_01 (authory_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS capital_status;
CREATE TABLE capital_status (
  order_id varchar(256) NOT NULL,
  agent_id varchar(256) NOT NULL,
  money int(8),
  price decimal(8,3),
  capital_status varchar(1) NOT NULL,
  capital decimal(8,3),
  capital_warn varchar(5),
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (order_id,agent_id),
  UNIQUE KEY idx_users_01 (order_id,agent_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS capital_total;
CREATE TABLE capital_total (
  agent_id varchar(256) NOT NULL,
  capital_status varchar(1) NOT NULL,
  capital decimal(8,3),
  capital_warn varchar(5),
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (agent_id),
  UNIQUE KEY idx_users_01 (agent_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
  order_id varchar(256) NOT NULL,
  corp_id varchar(256) NOT NULL,
  req_id varchar(256) NOT NULL,
  bill_id varchar(256) NOT NULL,
  ts varchar(20) NOT NULL,
  money int(8),
  sp_id varchar(256) NOT NULL,
  prov_id varchar(256) NOT NULL,
  number varchar(11) NOT NULL,
  ret_url varchar(100) NOT NULL,
  status tinyint(1) NOT NULL,
  price decimal(8,3),
  order_form varchar(1) NOT NULL,
  order_type varchar(1) NOT NULL,
  ref_id varchar(256) NOT NULL,
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (order_id),
  UNIQUE KEY idx_users_01 (order_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS stocks;
CREATE TABLE stocks (
  product_id varchar(256) NOT NULL,
  authory_id varchar(256) NOT NULL,
  product_name varchar(100),
  product_mode varchar(1) NOT NULL,
  operator varchar(100),
  account_control varchar(1) NOT NULL,
  num_type varchar(1) NOT NULL,
  province varchar(10),
  city varchar(10),
  value int(8),
  name varchar(100),
  disaccount varchar(10),
  status tinyint(1) NOT NULL,
  is_deleted tinyint(1) NOT NULL,
  created_by varchar(256),
  created_datetime datetime,
  updated_by varchar(256),
  updated_datetime datetime,
  PRIMARY KEY (product_id,authory_id),
  UNIQUE KEY idx_users_01 (product_id,authory_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
