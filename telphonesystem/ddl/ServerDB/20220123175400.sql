/*
MySQL Backup
Database: cost_system_db
Backup Time: 2022-01-23 17:54:51
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `cost_system_db`.`agent_product_price`;
DROP TABLE IF EXISTS `cost_system_db`.`agents`;
DROP TABLE IF EXISTS `cost_system_db`.`authories`;
DROP TABLE IF EXISTS `cost_system_db`.`capital_status`;
DROP TABLE IF EXISTS `cost_system_db`.`capital_total`;
DROP TABLE IF EXISTS `cost_system_db`.`orders`;
DROP TABLE IF EXISTS `cost_system_db`.`stocks`;
DROP TABLE IF EXISTS `cost_system_db`.`sysusers`;
CREATE TABLE `agent_product_price` (
  `agent_id` varchar(256) NOT NULL COMMENT '代理商ID',
  `product_id` varchar(256) NOT NULL COMMENT '商品ID',
  `product_name` varchar(256) NOT NULL COMMENT '商品名称',
  `sp_id` varchar(256) NOT NULL COMMENT '代理商流水',
  `value` int NOT NULL COMMENT '商品面额',
  `price` decimal(8,3) NOT NULL COMMENT '商品价格',
  `disaccount` int DEFAULT NULL,
  `product_type` int DEFAULT NULL COMMENT '产品类型 1:话费快充 2:话费慢充',
  `check_price` tinyint(1) DEFAULT NULL,
  `check_cost` tinyint(1) DEFAULT NULL,
  `status` int DEFAULT NULL COMMENT '商品上下架状态 1:上架 0:下架',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`agent_id`,`product_id`),
  UNIQUE KEY `idx_users_01` (`agent_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='代理商商品价格表';
CREATE TABLE `agents` (
  `agent_id` varchar(256) NOT NULL COMMENT '代理商ID',
  `agent_name` varchar(256) NOT NULL COMMENT '代理商名称',
  `agent_reduce_name` varchar(10) DEFAULT NULL COMMENT '代理商简称',
  `price` decimal(8,3) DEFAULT NULL COMMENT '价格',
  `tel` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `mail` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `province` varchar(10) DEFAULT NULL COMMENT '省份',
  `city` varchar(10) DEFAULT NULL COMMENT '城市',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `account_name` varchar(10) DEFAULT NULL COMMENT '代理商账户名',
  `acount_status` varchar(1) NOT NULL COMMENT '代理商状态 1:开启 0:关闭',
  `agent_type` varchar(1) NOT NULL COMMENT '代理商类型',
  `business_mode` varchar(1) NOT NULL COMMENT '商业模式',
  `interface_status` varchar(1) NOT NULL COMMENT '代理商接口状态 1:开启 0:关闭',
  `log_ps` varchar(10) DEFAULT NULL COMMENT '无效字段',
  `pay_ps` varchar(10) DEFAULT NULL COMMENT '无效字段',
  `info_status` varchar(1) NOT NULL COMMENT '代理商信用状态 1:实付 0:信用',
  `credit_limit` varchar(1) NOT NULL COMMENT '信用额度',
  `call_back_url` varchar(100) DEFAULT NULL COMMENT '代理商回调url',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`agent_id`),
  UNIQUE KEY `idx_users_01` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='代理商信息表';
CREATE TABLE `authories` (
  `authory_id` varchar(256) NOT NULL COMMENT '渠道商ID',
  `authory_name` varchar(256) NOT NULL COMMENT '渠道商ID',
  `abbreviation` varchar(10) DEFAULT NULL COMMENT '渠道商简称',
  `tel` varchar(11) DEFAULT NULL COMMENT '电话',
  `mail` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `province` varchar(10) DEFAULT NULL COMMENT '省份',
  `city` varchar(10) DEFAULT NULL COMMENT '城市',
  `address` varchar(100) DEFAULT NULL COMMENT '地址',
  `account_name` varchar(10) DEFAULT NULL COMMENT '渠道商账户名',
  `acount_status` varchar(1) NOT NULL COMMENT '渠道商状态 1:开启 0:关闭',
  `authory_type` varchar(1) NOT NULL COMMENT '渠道商类型',
  `business_mode` varchar(1) NOT NULL COMMENT '商业模式',
  `interface_status` varchar(1) NOT NULL COMMENT '渠道商接口状态 1:开启 0:关闭',
  `log_ps` varchar(10) DEFAULT NULL COMMENT '无效字段',
  `pay_ps` varchar(10) DEFAULT NULL COMMENT '无效字段',
  `info_status` varchar(1) NOT NULL COMMENT '渠道商信用状态 1:实付 0:信用',
  `prority` varchar(1) NOT NULL COMMENT '渠道商优先级',
  `split_flow` varchar(1) NOT NULL COMMENT '渠道商分流比',
  `cost` decimal(8,3) DEFAULT NULL COMMENT '渠道商费用',
  `price` decimal(8,3) DEFAULT NULL COMMENT '渠道商价格',
  `call_back_url` varchar(100) DEFAULT NULL COMMENT '渠道商回调url',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`authory_id`),
  UNIQUE KEY `idx_users_01` (`authory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='渠道商信息表';
CREATE TABLE `capital_status` (
  `order_id` varchar(256) NOT NULL COMMENT '订单ID',
  `agent_id` varchar(256) NOT NULL COMMENT '代理商ID',
  `money` int DEFAULT NULL COMMENT '面额',
  `price` decimal(8,3) DEFAULT NULL COMMENT '价格',
  `capital_status` varchar(1) NOT NULL COMMENT '资金状态',
  `capital` decimal(8,3) DEFAULT NULL COMMENT '资金量',
  `capital_warn` varchar(5) DEFAULT NULL COMMENT '资金警戒值',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`order_id`,`agent_id`),
  UNIQUE KEY `idx_users_01` (`order_id`,`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='资金记录表';
CREATE TABLE `capital_total` (
  `agent_id` varchar(256) NOT NULL COMMENT '代理商ID',
  `capital_status` varchar(1) NOT NULL COMMENT '资金状态',
  `capital` decimal(8,3) DEFAULT NULL COMMENT '资金量',
  `plus_capital` varchar(256) DEFAULT NULL,
  `capital_warn` varchar(5) DEFAULT NULL COMMENT '资金警戒值',
  `facility` decimal(8,3) DEFAULT NULL COMMENT '授信额度',
  `profit` decimal(8,3) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`agent_id`),
  UNIQUE KEY `idx_users_01` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='资金总额表';
CREATE TABLE `orders` (
  `order_id` varchar(256) NOT NULL COMMENT '订单ID',
  `corp_id` varchar(256) NOT NULL COMMENT '代理商ID',
  `req_id` varchar(256) NOT NULL COMMENT '代理商流水',
  `bill_id` varchar(256) NOT NULL COMMENT '渠道商流水',
  `ts` varchar(20) NOT NULL COMMENT '订单时间戳',
  `money` int DEFAULT NULL COMMENT '面额',
  `sp_id` varchar(256) NOT NULL COMMENT '代理商流水',
  `prov_id` varchar(256) NOT NULL COMMENT '无效字段',
  `number` varchar(11) NOT NULL COMMENT '充值手机号',
  `ret_url` varchar(100) NOT NULL COMMENT '代理商回调地址',
  `status` varchar(256) NOT NULL COMMENT '订单状态',
  `price` decimal(8,3) DEFAULT NULL COMMENT '价格',
  `order_form` varchar(1) NOT NULL COMMENT '订单方式',
  `order_type` varchar(1) NOT NULL COMMENT '订单类型',
  `ref_id` varchar(256) NOT NULL COMMENT '无效字段',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `idx_users_01` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='订单信息表';
CREATE TABLE `stocks` (
  `product_id` varchar(256) NOT NULL COMMENT '商品ID',
  `authory_id` varchar(256) NOT NULL COMMENT '渠道商ID',
  `product_name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `product_mode` varchar(100) NOT NULL COMMENT '商品模式',
  `operator` varchar(100) DEFAULT NULL COMMENT '操作者',
  `account_control` varchar(1) NOT NULL COMMENT '账户控制',
  `num_type` varchar(1) NOT NULL COMMENT '数字类型',
  `province` varchar(10) DEFAULT NULL COMMENT '省份',
  `city` varchar(10) DEFAULT NULL COMMENT '城市',
  `value` varchar(8) DEFAULT NULL COMMENT '面额',
  `price` varchar(8) DEFAULT NULL COMMENT '价格',
  `disaccount` varchar(10) DEFAULT NULL COMMENT '折扣',
  `name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `priority` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL COMMENT '商品状态',
  `check_price` tinyint(1) DEFAULT NULL COMMENT '价格',
  `check_cost` varchar(1) DEFAULT NULL COMMENT '费用',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`product_id`,`authory_id`),
  UNIQUE KEY `idx_users_01` (`product_id`,`authory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='库存信息表';
CREATE TABLE `sysusers` (
  `user_id` varchar(256) NOT NULL COMMENT '用户ID',
  `user_name` varchar(60) NOT NULL COMMENT '用户名',
  `company_name` varchar(100) DEFAULT NULL COMMENT '公司名称',
  `faceset_token` varchar(1000) DEFAULT NULL COMMENT '脸部识别token',
  `authority_id` varchar(10) DEFAULT NULL COMMENT '验证ID',
  `mail_address` varchar(300) DEFAULT NULL COMMENT '邮箱地址',
  `is_login` tinyint(1) NOT NULL COMMENT '是否登录',
  `is_deleted` tinyint(1) NOT NULL COMMENT '删除flg',
  `created_by` varchar(256) DEFAULT NULL COMMENT '创建用户',
  `created_datetime` datetime DEFAULT NULL COMMENT '创建日期',
  `updated_by` varchar(256) DEFAULT NULL COMMENT '更新用户',
  `updated_datetime` datetime DEFAULT NULL COMMENT '更新日期',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `idx_users_01` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户信息表';
BEGIN;
LOCK TABLES `cost_system_db`.`agent_product_price` WRITE;
DELETE FROM `cost_system_db`.`agent_product_price`;
INSERT INTO `cost_system_db`.`agent_product_price` (`agent_id`,`product_id`,`product_name`,`sp_id`,`value`,`price`,`disaccount`,`product_type`,`check_price`,`check_cost`,`status`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('1', '1_100', '', '1', 100, 98.000, 98, 1, 1, 1, 1, 0, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`agents` WRITE;
DELETE FROM `cost_system_db`.`agents`;
INSERT INTO `cost_system_db`.`agents` (`agent_id`,`agent_name`,`agent_reduce_name`,`price`,`tel`,`mail`,`province`,`city`,`address`,`account_name`,`acount_status`,`agent_type`,`business_mode`,`interface_status`,`log_ps`,`pay_ps`,`info_status`,`credit_limit`,`call_back_url`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('1', '天融信有限公司', '天融信', 98.000, '18912345678', '423318232@qq.com', '日本', '东京', '木黑区', '12345678', '1', '1', '1', '1', '1', '1', '1', '1', '1', 0, '12345678', NULL, NULL, NULL),('1234', '张毅', '张', 98.000, '18940854053', '423318232@qq.com', '日本', '东京', '目黑', '12345678', '1', '1', '1', '1', '1', '1', '1', '1', '1', 0, '11111111', '2021-12-03 23:50:00', NULL, NULL),('22222222', '天融信有限公司', '天融信', 98.000, '18940854053', '423318232', '日本', '东京', '目黑去', '12345678', '1', '1', '1', '1', '1', '1', '1', '1', '1', 0, '12345678', NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`authories` WRITE;
DELETE FROM `cost_system_db`.`authories`;
INSERT INTO `cost_system_db`.`authories` (`authory_id`,`authory_name`,`abbreviation`,`tel`,`mail`,`province`,`city`,`address`,`account_name`,`acount_status`,`authory_type`,`business_mode`,`interface_status`,`log_ps`,`pay_ps`,`info_status`,`prority`,`split_flow`,`cost`,`price`,`call_back_url`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('1', '张毅', '张', '090495', '22', NULL, NULL, NULL, '皇城黄', '可', '可', '预', '热', '热', '  rtrt', '热', '热', '分', 44.000, 33.000, '333', 0, NULL, NULL, NULL, NULL),('22222222', '张毅', '张', '09049521721', '22', NULL, NULL, NULL, '皇城黄', '可', '可', '预', '热', '热', 'rtrt', '热', '1', '分', 44.000, 98.000, '333', 0, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`capital_status` WRITE;
DELETE FROM `cost_system_db`.`capital_status`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`capital_total` WRITE;
DELETE FROM `cost_system_db`.`capital_total`;
INSERT INTO `cost_system_db`.`capital_total` (`agent_id`,`capital_status`,`capital`,`plus_capital`,`capital_warn`,`facility`,`profit`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('1', '1', 100.000, NULL, '4', 10000.000, NULL, 0, NULL, NULL, NULL, NULL),('1234', '1', 100.000, '50', '4', 10000.000, NULL, 0, NULL, '2021-11-20 23:41:29', NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`orders` WRITE;
DELETE FROM `cost_system_db`.`orders`;
INSERT INTO `cost_system_db`.`orders` (`order_id`,`corp_id`,`req_id`,`bill_id`,`ts`,`money`,`sp_id`,`prov_id`,`number`,`ret_url`,`status`,`price`,`order_form`,`order_type`,`ref_id`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('123456', '22222222', '1234', '12345', '2021', 100, '111', '22222222', '111', '1111', '成功', 98.000, '1', '2', '2', 0, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`stocks` WRITE;
DELETE FROM `cost_system_db`.`stocks`;
INSERT INTO `cost_system_db`.`stocks` (`product_id`,`authory_id`,`product_name`,`product_mode`,`operator`,`account_control`,`num_type`,`province`,`city`,`value`,`price`,`disaccount`,`name`,`priority`,`status`,`check_price`,`check_cost`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('2', '1', '电信', '快', '11', '1', '1', '辽宁', '大连', '100', '98', '98', '电话费', 1, 1, 1, '1', 0, NULL, NULL, NULL, NULL);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `cost_system_db`.`sysusers` WRITE;
DELETE FROM `cost_system_db`.`sysusers`;
INSERT INTO `cost_system_db`.`sysusers` (`user_id`,`user_name`,`company_name`,`faceset_token`,`authority_id`,`mail_address`,`is_login`,`is_deleted`,`created_by`,`created_datetime`,`updated_by`,`updated_datetime`) VALUES ('1', '张毅', 'suyuan', 'e616977d73bab9c47040a7f572f7866e', '1', 'BeiJing', 1, 0, '朱倍宏', '2021-04-07 17:01:00', '朱倍宏', '2021-04-09 18:07:02'),('2', '朱倍宏', 'suyuan', '1b9b37bbad9e5398c0179bb5a9ddd89c', '1', 'BeiJing', 1, 0, '朱倍宏', '2021-04-07 17:01:00', '朱倍宏', '2021-04-09 17:47:00');
UNLOCK TABLES;
COMMIT;
